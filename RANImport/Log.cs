﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace RANImport
{
    static class Log
    {
        public static RichTextBox TextBox { get; set; }
        static bool newLine;
        public static void Append(string log)
        {
            TextBox.AppendText(TextBox.TextLength > 0 ? "\r\n" + log+ "..." : log + "...");
            newLine = false;
        }
        public static void Done(bool res, string text = null)
        {
            TextBox.SelectionColor = res ? Color.LimeGreen : Color.Red;
            TextBox.AppendText((newLine ? "\r\n" : "") + (res ? "Done" : "Error"));
            TextBox.SelectionColor = SystemColors.Control;
            if (text != null) TextBox.AppendText(": " + text + ".");
            newLine = true;
        }
        public static void Done(string text)
        {
            TextBox.SelectionColor = text == "" ? Color.LimeGreen : Color.Red;
            TextBox.AppendText((newLine ? "\r\n" : "") + (text == "" ? "Done" : "Error"));
            TextBox.SelectionColor = SystemColors.Control;
            if(text != "") TextBox.AppendText(": " + text + ".");
            newLine = true;
        }
    }
}
