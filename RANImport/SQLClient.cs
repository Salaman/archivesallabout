﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Data.Sql;
using Microsoft.Win32;
using System.Threading;
namespace RANImport
{
    class SQLClient
    {
        public SqlConnection Connection { get; set; }

        public SQLClient()
        {

        }

        public void Connect(string server, string login, string password)
        {
            Log.Append("Подключение к базе данных");
            Connection = new SqlConnection($"Server={server}; Initial Catalog=ArchiveFund; User ID={login}; Password={password}; Connect Timeout=3;");
            try
            {
                Connection.Open();
                Log.Done(Connection.State == ConnectionState.Open);
            }
            catch (Exception ex)
            {
                Log.Done(Connection.State == ConnectionState.Open, ex.Message);
            }
        }

        public void Disconnect()
        {
            Log.Append("Отключение от базы данных");
            Connection.Close();
            Log.Done(Connection.State == ConnectionState.Closed);

        }

        public void Import(DataSet Database)
        {
            SetFK("tblARCHIVE", "FK_tblARCHIVE_tblSUBJECT_CL", false);
            SetFK("tblFUND", "FK_tblFUND_tblSECURITY_REASON", false);
            SetFK("tblFUND_RENAME", "FK_tblFUND_RENAME_tblFUND", false);

            DataTable Archive = Database.Tables["Archive"];

            Log.Append("Определение субъекта");
            long ISN_SUBJECT = (long)execScalar($"select ISN_SUBJECT from tblSUBJECT_CL where NAME like '%{Archive.Rows[0]["CITY"].ToString().Replace("г. ", "")}%'");
            Log.Done(ISN_SUBJECT != 0);

            Log.Append($"Импорт архива \"{Archive.Rows[0]["NAME"]}\"");
            foreach (DataRow r in Archive.Rows)
                Log.Done(exec("insert into tblARCHIVE values(" +
                                $"'00000000-0100-0000-0000-{i(r["ID"]):000000000000}'," +
                                "'12345678-9012-3456-7890-123456789012'," +
                                $"cast('{r["IDATE"]}' as datetime)," +
                                "'0253E5AE-57EE-4D21-AAD9-15EBECA3E854'," +
                                "0," +
                                $"1{i(r["ID"]):0000000000}," +
                                $"{ISN_SUBJECT}," +
                                "NULL," +
                                "NULL," +
                                $"'{r["CODENAME"]}'," +
                                $"'{r["SHORTNAME"]}'," +
                                $"'{r["NAME"]}'," +
                                $"'{r["IND"]},  {r["CITY"]},  {r["ADRES"]},  {r["HOUSE"]}'," +
                                $"'{r["WATERMARK"]}'," +
                                "'b'," +
                                "'a')"));



            DataTable Fund = Database.Tables["Fund"];

            foreach (DataRow r in Fund.Rows)
            {
                Log.Append($"Импорт фонда {r["LITER1"]}{r["FONDNOM"]}{r["LITER"]}");
                Log.Done(exec("insert into tblFUND values(" +
                    $"'{r["GUID"]}'," +
                    "'12345678-9012-3456-7890-123456789012'," +
                    $"cast('{r["IDATE"]}' as datetime)," +
                    "'FFBEE326-AC8B-4073-8C7F-15EBE6758D16'," +
                    "0," +
                    $"1{i(r["ID_SPHINX"]):0000000000}," +
                    $"1{i(r["IDA"]):0000000000}," +
                    $"'{(s(r["LITER1"]).Length > 0 ? s(r["LITER1"])[0] : '\0')}'," +
                    $"'{s(r["FONDNOM"])}'," +
                    $"'{(s(r["LITER"]).Length > 0 ? s(r["LITER"])[0] : '\0')}'," +
                    "1," +
                    $"'{(i(r["KATFOND"]) == 0 ? 'd' : i(r["KATFOND"]) == 1 ? 'a' : i(r["KATFOND"]) == 2 ? 'b' : 'c')}'," +
                    "3," +
                    $"'{(i(r["TYPFOND"]) == 0 ? 'a' : i(r["TYPFOND"]) == 1 ? 'd' : 'c')}'," +
                    $"'{(s(r["SHORTNAME"]).Length > 0 ? r["SHORTNAME"] : r["FONDNAME"])}'," +
                    $"'{r["FONDNAME"]}'," +
                    $"{i(r["OPISCOUNT"])}," +
                    $"{i(r["OPISCOUNT"])}," +
                    $"{i(r["BEGDOC"])}," +
                    "'N'," +
                    $"{i(r["ENDDOC"])}," +
                    "'N'," +
                    $"{i(s(r["FIRSTDATE"]).Length < 3 ? null : s(r["FIRSTDATE"]).Substring(s(r["FIRSTDATE"]).Length - 4))}," +
                    $"{i(s(r["LASTCHECK"]).Length < 3 ? null : s(r["LASTCHECK"]).Substring(s(r["LASTCHECK"]).Length - 4))}," +
                    "null," +
                    "'N'," +
                    "'N'," +
                    $"'{r["OBOZRENIE"]}'," +
                    "'a'," +
                    "'a'," +
                    "null," +
                    "null," +
                    "'N'," +
                    "0," +
                    "'N'," +
                    "'N'," +
                    $"'{(s(r["RENAMES"]).Length > 3 ? 'Y' : 'N')}'," +
                    "null," +
                    "'a'," +
                    $"{(i(r["SECRET"]) == 0 ? 1 : 2)}," +
                    $"'{(i(r["SECRET"]) == 0 ? new char?('o') : null)}'," +
                    "null," +
                    "null," +
                    "null," +
                    "null," +
                    "null," +
                    "null," +
                    "null," +
                    "null," +
                    "0," +
                    "'N'," +
                    "'N'," +
                    "'T'," +
                    "0," +
                    "0," +
                    $"'{r["SOURCE2"]}'," +
                    $"'{r["SHORTHELP"]}'," +
                    "null," +
                    $"'{r["HISTORY"]}'," +
                    "null," +
                    "'a'," +
                    "null," +
                    "null)"));

                if (s(r["RENAMES"]).Length > 3)
                {
                    List<string> renames = s(r["RENAMES"]).Split(new char[] { '\r' }).ToList();
                    renames = renames.Select(s => s.Trim()).ToList();




                    //Guid ID = Guid.NewGuid(); //Новый ID для eqDocStatesLOG. Положение: Сохранен
                    //Log.Append($"Добавление вспомогательной информации {r["LITER1"]}{r["FONDNOM"]}{r["LITER"]} : eqDocStatesLOG");
                    //Log.Done(exec("insert into eqDocStatesLOG values(" +
                    //    $"'{ID}', " +
                    //    "'BC6013A9-19E8-42AD-9754-CE044BF94520', " +
                    //    "'12345678-9012-3456-7890-123456789012', " +
                    //    "'FFBEE326-AC8B-4073-8C7F-15EBE6758D16', " +
                    //    $"cast('{DateTime.Now}' as datetime), " +
                    //    "0, " +
                    //    "'852437A4-49C3-436D-B388-15EBE6743C69', " +
                    //    "'Администратор С.', " +
                    //    "'Сохранен')"));



                    int n = 0; //SpecRowNum
                    foreach (string rename in renames)
                    {
                        string firstPart = rename.Substring(0, rename.IndexOf(' '));
                        int startYear = int.Parse(firstPart.Split(new char[] { '-' })[0]);
                        int endYear = firstPart.Last() == '-' ? 0 : int.Parse(firstPart.Split(new char[] { '-' })[1]);
                        string name = rename.Substring(rename.IndexOf(' ')).Trim();

                        long lastISN = (long)getLastISN("tblFUND_RENAME", "ISN_FUND_RENAME") + 1;
                        string renameID = $"00000000-0100-0000-0000-00{ lastISN.ToString().Substring(1):0000000000}";

                        Log.Append($"Импорт переименования фонда {r["LITER1"]}{r["FONDNOM"]}{r["LITER"]}");
                        Log.Done(exec("insert into tblFUND_RENAME values(" +
                        $"'{renameID}'," +
                        "'12345678-9012-3456-7890-123456789012'," +
                        $"cast('{r["IDATE"]}' as datetime)," +
                        $"'{r["GUID"]}'," +
                        $"{n}," +
                        $"{lastISN}," +
                        $"1{i(r["ID_SPHINX"]):0000000000}," +
                         $"{startYear}0101," +
                        "'N'," +
                        (endYear == 0 ? "'\0'," : $"{endYear}1231,") +
                        "'N'," +
                        $"'{(s(r["LITER1"]).Length > 0 ? s(r["LITER1"])[0] : '\0')}'," +
                        $"'{s(r["FONDNOM"])}'," +
                        $"'{(s(r["LITER"]).Length > 0 ? s(r["LITER"])[0] : '\0')}'," +
                        $"'{name}'," +
                        $"'{name}'," +
                        "''," +
                        "'N')"));
                        n++;

                        //Log.Append($"Добавление вспомогательной информации {r["LITER1"]}{r["FONDNOM"]}{r["LITER"]} : eqDocFieldLog");
                        //Log.Done(exec("insert into eqDocFieldLog values(" +
                        //$"cast('{DateTime.Now:yyyy-MM-dd hh:mm:ss.fff}' as datetime), " +
                        //$"'{r["GUID"]}', " +
                        //$"'58C9B317-8940-4183-96A8-54E32153CF31', " +
                        //$"'12345678-9012-3456-7890-123456789012', " +
                        //"'\0', " +
                        //"'\0', " +
                        //$"{n}, " +
                        //$"'{renameID}', " +
                        //$"'{ID}', " +
                        //"'Администратор С.', " +
                        //"'Примечание', " +
                        //$"'852437A4-49C3-436D-B388-15EBE6743C69')"));
                        
                    }
                }
            }

            SetFK("tblARCHIVE", "FK_tblARCHIVE_tblSUBJECT_CL", true);
            SetFK("tblFUND", "FK_tblFUND_tblSECURITY_REASON", true);
            SetFK("tblFUND_RENAME", "FK_tblFUND_RENAME_tblFUND", true);
        }

        //Преобразования
        string s(object o)
        {
            return o.ToString();
        }
        int i(object o)
        {
            return Convert.ToInt32(o);
        }

        void SetFK(string table, string fkey, bool flag)
        {
            exec($"alter table {table} {(flag ? "check" : "nocheck")} constraint {fkey}");
        }

        object getLastISN(string table, string column)
        {
            object obj = execScalar($"select top 1 {column} from {table} order by {column} desc");

            if (obj != null)
            {
                return obj;
            }
            else
            {
                return 10000000000;
            }
        }

        public string[] GetDBNames()
        {
            Log.Append("Получение списка SQL-серверов");

            string machineName = Environment.MachineName;
            List<string> list = new List<string>();

            var registryViewArray = new[] { RegistryView.Registry32, RegistryView.Registry64 };

            foreach (var registryView in registryViewArray)
            {
                using (var hklm = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, registryView))
                using (var key = hklm.OpenSubKey(@"SOFTWARE\Microsoft\Microsoft SQL Server"))
                {
                    var instances = (string[])key?.GetValue("InstalledInstances");
                    if (instances != null)
                    {
                        foreach (var element in instances)
                        {
                            if (element == "MSSQLSERVER")
                                list.Add(machineName);
                            else
                                list.Add(machineName + @"\" + element);

                        }
                    }
                }
            }

            Log.Done(list.Count > 0);

            return list.ToArray();
        }

        string exec(string command1, string command2 = null, string command3 = null)
        {
            try
            {
                SqlCommand cmd = new SqlCommand(command1, Connection);
                cmd.CommandTimeout = 1000;
                cmd.ExecuteNonQuery();
                if (command2 != null)
                {
                    SqlCommand cmd2 = new SqlCommand(command2, Connection);
                    cmd2.CommandTimeout = 1000;
                    cmd2.ExecuteNonQuery();
                }
                if (command3 != null)
                {
                    SqlCommand cmd3 = new SqlCommand(command3, Connection);
                    cmd3.CommandTimeout = 1000;
                    cmd3.ExecuteNonQuery();
                }
                return "";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }

        object execScalar(string command)
        {
            object val = null;
            try
            {
                SqlCommand cmd = new SqlCommand(command, Connection);
                val = cmd.ExecuteScalar();
            }
            catch (Exception)
            {
                return null;
            }
            return val;
        }

    }
}
