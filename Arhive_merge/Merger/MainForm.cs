﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using System.Windows;
namespace Merger
{
    public partial class MainForm : Form
    {
        XmlManager settings;
        dbManager dbManager;
        int errCount;
        public MainForm()
        {
            InitializeComponent();

            settings = new XmlManager(this).Load();
            dbManager = new dbManager(OnLog);

            #region Настройка цветов
            _Database1.DrawItem += OnDrawComboBoxItem;
            _Database2.DrawItem += OnDrawComboBoxItem;

            #endregion

            #region Перевод каретки в конец строки
            //_Server.Click += OnSelect;
            OnSelect(_Server, new EventArgs());
            //_Login.Click += OnSelect;
            OnSelect(_Login, new EventArgs());
            //_Password.Click += OnSelect;
            OnSelect(_Password, new EventArgs());
            #endregion

            #region Сброс фокуса
            Load += (s, a) => ActiveControl = null;
            _Connect.Click += (s, a) => ActiveControl = null;
            _Merge.Click += (s, a) => ActiveControl = null;
            _Log.Enter += (s, a) => ActiveControl = null;
            _Database1.SelectedIndexChanged += (s, a) => ActiveControl = null;
            _Database2.SelectedIndexChanged += (s, a) => ActiveControl = null;
            #endregion Сброс фокуса

            #region Перемещение окна за любой участок
            MouseDown += OnMouseDown; MouseMove += OnMouseMove;
            foreach (Control c in Controls)
            {
                if (!(c is ComboBox) && c.Name != "_Server" && c.Name != "_Login" && c.Name != "_Password")
                {
                    c.MouseDown += OnMouseDown;
                    c.MouseMove += OnMouseMove;
                }
            }
            #endregion

            #region Стрелка
            Paint += (s, a) =>
            {
                Graphics g = _Arrow.CreateGraphics();
                Pen pen = new Pen(_Connect.BackColor, 11);
                pen.CustomEndCap = new AdjustableArrowCap(3, 3);
                g.DrawLine(pen, 0, _Arrow.Height/2, _Arrow.Width, _Arrow.Height/2);
            };
            #endregion

            #region Выход
            _Exit.Click += (s, a) => { settings.Save(); Application.Exit(); };
            #endregion
        }

        void _Connect_Click(object sender, EventArgs e)
        {
            if (dbManager.ConnectTo(_Server.Text, _Login.Text, _Password.Text))
            {
                _Status.BackColor = Color.LimeGreen;
                dbManager.FillComboBoxes(new ComboBox[] { _Database1, _Database2 });
                _Database1.Enabled = true; _Database2.Enabled = true;
                _Merge.Enabled = true;
                AcceptButton = _Merge;
                _Progress.Value = 0;
            }
            else
            {
                _Status.BackColor = Color.Red;
                _Database1.Items.Clear(); _Database2.Items.Clear();
                _Database1.Enabled = false; _Database2.Enabled = false;
                _Merge.Enabled = false;
                AcceptButton = _Connect;
                _Progress.Value = 0;
            }
        }


        void _Merge_Click(object sender, EventArgs e)
        {
            _Progress.Value = 0;
            errCount = 0;
            _Merge.Enabled = false;
            _Connect.Enabled = false;
            dbManager.MergeAsync(_Database2.Text, _Database1.Text, _Database2.Text.Trim(new char[] { '[', ']' }) + "1");
        }
        //int c = 0; //для подсчета операций для ProgressBar
        void OnLog(object sender, LabelEditEventArgs e)
        {
            Invoke((MethodInvoker)(() =>
            {
                string d = "Done.";
                string er = "Error.";
                string compl = "completed.";
                _Log.AppendText(e.Label);

                if (e.Label.Contains(d))
                {
                    _Log.Select(_Log.Text.LastIndexOf(d), d.Length);
                    _Log.SelectionColor = Color.LimeGreen;
                    //_Log.AppendText(c++.ToString()); //для подсчета операций для ProgressBar
                    _Progress.Increment(1);

                }
                else if (e.Label.Contains(er))
                {
                    _Log.Select(_Log.Text.LastIndexOf(er), er.Length);
                    _Log.SelectionColor = Color.Red;
                    _Progress.Increment(1);
                    errCount++;
                }
                else if (e.Label.Contains(compl))
                {
                    _Log.Select(_Log.Text.LastIndexOf(compl), compl.Length);
                    _Log.SelectionColor = Color.LimeGreen;
                    _Progress.Increment(1);
                    _Log.Select(_Log.TextLength, 0);
                    _Log.SelectionColor = SystemColors.Control;
                    _Log.AppendText($" Errors: {errCount.ToString()}.\r\n");
                    _Merge.Enabled = true;
                    _Connect.Enabled = true;
                }
                Update();
            }
        ));
        }

        void OnSelect(object sender, EventArgs e)
        {
            AcceptButton = _Connect;
            TextBox tbx = sender as TextBox;
            tbx.Select(tbx.TextLength, 0);
        }
        private void _Log_TextChanged(object sender, EventArgs e)
        {
            _Log.ScrollToCaret();
        }
        void OnDrawComboBoxItem(object sender, DrawItemEventArgs e)
        {
            if (e.Index < 0) return;

            ComboBox combo = sender as ComboBox;
            if ((e.State & DrawItemState.Selected) == DrawItemState.Selected)
                e.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(120, 120, 120)), e.Bounds);
            else
                e.Graphics.FillRectangle(new SolidBrush(combo.BackColor), e.Bounds);

            e.Graphics.DrawString(combo.Items[e.Index].ToString(), e.Font,
                                  new SolidBrush(combo.ForeColor),
                                  new Point(e.Bounds.X, e.Bounds.Y));

        }
        #region Перемещение окна за любой участок (обработчики)
        int x = 0, y = 0;
        void OnMouseDown(object sender, MouseEventArgs e) { x = e.X; y = e.Y; }
        void OnMouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                Location = new Point(Location.X + (e.X - x), Location.Y + (e.Y - y));
        }
        #endregion
    }
}
