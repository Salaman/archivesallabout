﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data;
namespace Merger
{
    class PrimaryKey
    {
        EventHandler<LabelEditEventArgs> log;

        public string Name { get; set; }
        public string Columns { get; set; }
        public string Type { get; set; }
        public bool Exists { get; set; }

        public string TableName { get; set; }
        public string Database { get; set; }
        public SqlConnection Connection { get; set; }
        public PrimaryKey(string tableName, string database, SqlConnection connection, EventHandler<LabelEditEventArgs> upperLog)
        {
            log += upperLog;

            TableName = tableName;
            Database = database;
            Connection = connection;

            var res = GetPK();

            Name = res.Item1;
            Columns = res.Item2;
            Type = res.Item3;

        }
        public PrimaryKey(PrimaryKey primaryKey, string database, EventHandler<LabelEditEventArgs> upperLog)
        {
            log += upperLog;

            TableName = primaryKey.TableName;
            Database = database;
            Connection = primaryKey.Connection;

            Name = primaryKey.Name;
            Columns = primaryKey.Columns;
            Type = primaryKey.Type;
            Exists = primaryKey.Exists;
        }
        Tuple<string, string, string> GetPK()
        {
            #region in SQL
            //select pk.[name] as pk_name, substring(column_names, 1, len(column_names)-1) as [columns], pk.[type_desc]
            //from sys.tables tab
            //inner join sys.indexes pk
            //on tab.object_id = pk.object_id
            //and pk.is_primary_key = 1
            //cross apply (select col.[name] + ', '
            //from sys.index_columns ic
            //inner join sys.columns col
            //on ic.object_id = col.object_id
            //and ic.column_id = col.column_id
            //where ic.object_id = tab.object_id
            //and ic.index_id = pk.index_id
            //order by col.column_id
            //for xml path ('') ) D(column_names)
            //where tab.name = 'eqDocFieldLog'
            #endregion
            Log($"Get PK for {TableName}...");

            var resList = exec3($"use {Database} " +
                $"select pk.[name] as pk_name, substring(column_names, 1, len(column_names)-1) as [columns], pk.[type_desc] " +
                $"from sys.tables tab " +
                $"inner join sys.indexes pk " +
                $"on tab.object_id = pk.object_id " +
                $"and pk.is_primary_key = 1 " +
                $"cross apply (select col.[name] + ', ' " +
                $"from sys.index_columns ic " +
                $"inner join sys.columns col " +
                $"on ic.object_id = col.object_id " +
                $"and ic.column_id = col.column_id " +
                $"where ic.object_id = tab.object_id " +
                $"and ic.index_id = pk.index_id " +
                $"order by col.column_id " +
                $"for xml path ('') ) D (column_names) " +
                $"where tab.name = '{TableName}' " +
                $"use master");
            Exists = resList.Count > 0 ? true : false;
            var res = Exists ? resList.First() : new Tuple<string, string, string>("", "", "");

            return res;
        }
        // создание PK + индекс для таблицы, чтобы индекс был Clustered (?всегда ли так?)
        public void Create()
        {
            //    ALTER TABLE[dbo].[tblFILE_CONTENTS2]
            //ADD CONSTRAINT[PK_tblFILE_CONTENTS2] PRIMARY KEY NONCLUSTERED([ID] ASC);
            if (Exists)
            {
                Log($"Create {Name}...");
                exec($"use {Database}",
                     $"alter table {TableName} " +
                     $"add constraint {Name} primary key {Type} ({Columns});",
                     $"use master");
            }
        }
        public void Delete()
        {
            if (Exists)
            {
                Log($"Delete {Name}...");
                exec($"alter table {Database}.dbo.{TableName} drop constraint {Name}");
            }
        }

        DataTable execDataTable(string command)
        {
            DataTable res = new DataTable();
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(command, Connection);
                da.Fill(res);
                Log("Done.\r\n");
            }
            catch (Exception ex)
            {
                Log($"Error.\r\nError: {ex.Message}\r\n");
            }
            return res;
        }
        List<Tuple<string, string, string>> exec3(string command)
        {
            List<Tuple<string, string, string>> res = new List<Tuple<string, string, string>>();
            DataTable t = execDataTable(command);
            foreach (DataRow r in t.Rows)
                res.Add(Tuple.Create(r.ItemArray[0].ToString(), r.ItemArray[1].ToString(), r.ItemArray[2].ToString()));

            return res;
        }

        bool exec(string command1, string command2 = null, string command3 = null)
        {
            try
            {
                SqlCommand cmd = new SqlCommand(command1, Connection);
                cmd.CommandTimeout = 1000;
                cmd.ExecuteNonQuery();
                if (command2 != null)
                {
                    SqlCommand cmd2 = new SqlCommand(command2, Connection);
                    cmd2.CommandTimeout = 1000;
                    cmd2.ExecuteNonQuery();
                }
                if (command3 != null)
                {
                    SqlCommand cmd3 = new SqlCommand(command3, Connection);
                    cmd3.CommandTimeout = 1000;
                    cmd3.ExecuteNonQuery();
                }
                Log("Done.\r\n");
                return true;
            }
            catch (Exception ex)
            {
                Log($"Error.\r\nError: {ex.Message}\r\n");
                return false;
            }

        }
        void Log(string logText)
        {
            log(this, new LabelEditEventArgs(Convert.ToInt32(true), logText));
        }
    }

    class ForeignKeys
    {
        EventHandler<LabelEditEventArgs> log;

        public List<ForeignKey> Items { get; set; } = new List<ForeignKey>();

        public string TableName { get; set; }
        public string Database { get; set; }
        public SqlConnection Connection { get; set; }
        public ForeignKeys(string tableName, string database, SqlConnection connection, EventHandler<LabelEditEventArgs> upperLog)
        {
            log += upperLog;

            TableName = tableName;
            Database = database;
            Connection = connection;

            Items = GetForeignKeys();
        }
        public ForeignKeys(ForeignKeys foreignKeys, string database, EventHandler<LabelEditEventArgs> upperLog)
        {
            log += upperLog;

            TableName = foreignKeys.TableName;
            Database = database;
            Connection = foreignKeys.Connection;

            foreach (var f in foreignKeys)
                Items.Add(new ForeignKey(f.Name, f.PKTable, f.PKColumn, f.FKTable, f.FKColumn, Database, Connection, log));
        }

        List<ForeignKey> GetForeignKeys()
        {
            List<ForeignKey> res = new List<ForeignKey>();

            #region in SQL
            //    SELECT obj.name AS FK_NAME,
            //    tab2.name AS[PKTable],
            //    col2.name AS[PKColumn],
            //    tab1.name AS[FKTable],
            //    col1.name AS[FKColumn]
            //FROM sys.foreign_key_columns fkc
            //INNER JOIN sys.objects obj
            //    ON obj.object_id = fkc.constraint_object_id
            //INNER JOIN sys.tables tab1
            //    ON tab1.object_id = fkc.parent_object_id
            //INNER JOIN sys.schemas sch
            //    ON tab1.schema_id = sch.schema_id
            //INNER JOIN sys.columns col1
            //    ON col1.column_id = parent_column_id AND col1.object_id = tab1.object_id
            //INNER JOIN sys.tables tab2
            //    ON tab2.object_id = fkc.referenced_object_id
            //INNER JOIN sys.columns col2
            //    ON col2.column_id = referenced_column_id AND col2.object_id = tab2.object_id
            //INNER JOIN sys.[types] TY ON col1.[system_type_id] = TY.[system_type_id] AND col1.[user_type_id] = TY.[user_type_id]
            //where tab1.name = 'tblARCHIVE'
            #endregion

            Log($"Get FK for {TableName}...");
            var temp = exec5($"use {Database} " +
                $"SELECT  obj.name AS FK_NAME, tab2.name AS [PKTable], col2.name AS [PKColumn], " +
                $"tab1.name AS [FKTable],col1.name AS [FKColumn] " +
                $"FROM sys.foreign_key_columns fkc " +
                $"INNER JOIN sys.objects obj " +
                $"ON obj.object_id = fkc.constraint_object_id " +
                $"INNER JOIN sys.tables tab1 " +
                $"ON tab1.object_id = fkc.parent_object_id " +
                $"INNER JOIN sys.schemas sch " +
                $"ON tab1.schema_id = sch.schema_id " +
                $"INNER JOIN sys.columns col1 " +
                $"ON col1.column_id = parent_column_id AND col1.object_id = tab1.object_id " +
                $"INNER JOIN sys.tables tab2 " +
                $"ON tab2.object_id = fkc.referenced_object_id " +
                $"INNER JOIN sys.columns col2 " +
                $"ON col2.column_id = referenced_column_id AND col2.object_id = tab2.object_id " +
                $"INNER JOIN sys.[types] TY ON col1.[system_type_id] = TY.[system_type_id] AND col1.[user_type_id] = TY.[user_type_id] " +
                $"where tab1.name = '{TableName}' " +
                $"use master");


            foreach (var t in temp)
                res.Add(new ForeignKey(t.Item1, t.Item2, t.Item3, t.Item4, t.Item5, Database, Connection, log));

            return res;
        }

        //создание всех Foreign keys для таблицы
        public void Create()
        {
            foreach (var f in Items)
                f.Create();
        }
        public void Delete()
        {
            foreach (var f in Items)
                f.Delete();
        }

        public IEnumerator<ForeignKey> GetEnumerator()
        {
            return Items.GetEnumerator();
        }

        DataTable execDataTable(string command)
        {
            DataTable res = new DataTable();
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(command, Connection);
                da.Fill(res);
                Log("Done.\r\n");
            }
            catch (Exception ex)
            {
                Log($"Error.\r\nError: {ex.Message}\r\n");
            }
            return res;
        }
        List<Tuple<string, string, string, string, string>> exec5(string command)
        {
            List<Tuple<string, string, string, string, string>> res = new List<Tuple<string, string, string, string, string>>();
            DataTable t = execDataTable(command);
            foreach (DataRow r in t.Rows)
                res.Add(Tuple.Create(r.ItemArray[0].ToString(), r.ItemArray[1].ToString(),
                                     r.ItemArray[2].ToString(), r.ItemArray[3].ToString(), r.ItemArray[4].ToString()));

            return res;
        }

        void Log(string logText)
        {
            log(this, new LabelEditEventArgs(Convert.ToInt32(true), logText));
        }
    }
    class ForeignKey
    {
        EventHandler<LabelEditEventArgs> log;

        public string Name { get; set; }
        public string PKTable { get; set; }
        public string PKColumn { get; set; }
        public string FKTable { get; set; }
        public string FKColumn { get; set; }

        public string Database { get; set; }
        public SqlConnection Connection { get; set; }
        public ForeignKey(string fkName, string pkTable, string pkColumn, string fkTable, string fkColumn,
            string database, SqlConnection connection, EventHandler<LabelEditEventArgs> upperLog)
        {
            log += upperLog;

            Database = database;
            Connection = connection;

            Name = fkName;
            PKTable = pkTable;
            PKColumn = pkColumn;
            FKTable = fkTable;
            FKColumn = fkColumn;
        }
        public void Create()
        {
            Log($"Create {Name}...");
            exec($"use {Database}",
                 $"alter table {FKTable} " +
                 $"add constraint {Name} " +
                 $"foreign key ({FKColumn}) references {PKTable}({PKColumn})",
                 $"use master");

        }

        public void Delete()
        {
            Log($"Delete {Name}...");
            exec($"alter table {Database}.dbo.{FKTable} drop constraint {Name}");
        }

        bool exec(string command1, string command2 = null, string command3 = null)
        {
            try
            {
                SqlCommand cmd = new SqlCommand(command1, Connection);
                cmd.CommandTimeout = 1000;
                cmd.ExecuteNonQuery();
                if (command2 != null)
                {
                    SqlCommand cmd2 = new SqlCommand(command2, Connection);
                    cmd2.CommandTimeout = 1000;
                    cmd2.ExecuteNonQuery();
                }
                if (command3 != null)
                {
                    SqlCommand cmd3 = new SqlCommand(command3, Connection);
                    cmd3.CommandTimeout = 1000;
                    cmd3.ExecuteNonQuery();
                }
                Log("Done.\r\n");
                return true;
            }
            catch (Exception ex)
            {
                Log($"Error.\r\nError: {ex.Message}\r\n");
                return false;
            }

        }
        void Log(string logText)
        {
            log(this, new LabelEditEventArgs(Convert.ToInt32(true), logText));
        }
    }
    class Indexes
    {
        EventHandler<LabelEditEventArgs> log;

        public List<Index> Items { get; set; } = new List<Index>();


        public string TableName { get; set; }
        public string Database { get; set; }
        public SqlConnection Connection { get; set; }
        public Indexes(string tableName, string database, SqlConnection connection, EventHandler<LabelEditEventArgs> upperLog)
        {
            log += upperLog;

            TableName = tableName;
            Database = database;
            Connection = connection;

            Items = GetIndexes();

        }
        public Indexes(Indexes indexes, string database, EventHandler<LabelEditEventArgs> upperLog)
        {
            log += upperLog;

            TableName = indexes.TableName;
            Database = database;
            Connection = indexes.Connection;

            foreach (var i in indexes)
                Items.Add(new Index(i.TableName, i.Name, i.Columns, i.Type, i.Unique, Database, Connection, log));
        }

        List<Index> GetIndexes()
        {
            #region in SQL
            //select t.[name] as table_view, i.[name] as index_name, substring(column_names, 1, len(column_names)-1) as [columns],
            //case when i.[type] = 1 then 'CLUSTERED' when i.[type] = 2 then 'NONCLUSTERED' end as _type,
            //   case when i.is_unique = 1 then 'UNIQUE' else '' end as [unique]

            //   from sys.objects t
            //   inner join sys.indexes i on t.object_id = i.object_id
            //   cross apply (select col.[name] + ', '
            //           from sys.index_columns ic
            //               inner join sys.columns col
            //                   on ic.object_id = col.object_id
            //                   and ic.column_id = col.column_id
            //           where ic.object_id = t.object_id
            //               and ic.index_id = i.index_id
            //                   order by key_ordinal
            //                   for xml path ('') ) D(column_names)
            //   where t.is_ms_shipped<> 1
            //   and index_id > 0
            //   and t.type = 'U'
            //   and t.name not like 'eq%'
            //   and i.name not like 'PK%'
            //   and t.name = 'tblARCHIVE'
            //   order by t.name
            #endregion
            List<Index> res = new List<Index>();

            Log($"Get IX for {TableName}...");

            List<Tuple<string, string, string, string, string>> resList = exec5($"use {Database} " +
                $"select t.[name] as table_view, i.[name] as index_name, substring(column_names, 1, len(column_names)-1) as [columns], " +
                $"case when i.[type] = 1 then 'CLUSTERED' when i.[type] = 2 then 'NONCLUSTERED' end as _type, " +
                $"case when i.is_unique = 1 then 'UNIQUE' else '' end as [unique] " +
                $"from sys.objects t " +
                $"inner join sys.indexes i on t.object_id = i.object_id " +
                $"cross apply (select col.[name] + ', ' " +
                $"from sys.index_columns ic " +
                $"inner join sys.columns col " +
                $"on ic.object_id = col.object_id " +
                $"and ic.column_id = col.column_id " +
                $"where ic.object_id = t.object_id " +
                $"and ic.index_id = i.index_id " +
                $"order by key_ordinal " +
                $"for xml path ('') ) D (column_names) " +
                $"where t.is_ms_shipped <> 1 " +
                $"and index_id > 0 " +
                $"and t.type = 'U' " +
                $"and t.name not like 'eq%' " +
                $"and i.name not like 'PK%' " +
                $"and t.name = '{TableName}' " +
                $"order by t.name " +
                $"use master");

            foreach (var e in resList)
                res.Add(new Index(e.Item1, e.Item2, e.Item3, e.Item4, e.Item5, Database, Connection, log));
            return res;
        }

        //создание всех индексов для таблицы
        public void Create()
        {
            foreach (var i in Items)
                i.Create();
        }

        public void Delete()
        {
            foreach (var i in Items)
                i.Delete();
        }

        public IEnumerator<Index> GetEnumerator()
        {
            return Items.GetEnumerator();
        }

        DataTable execDataTable(string command)
        {
            DataTable res = new DataTable();
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(command, Connection);
                da.Fill(res);
                Log("Done.\r\n");
            }
            catch (Exception ex)
            {
                Log($"Error.\r\nError: {ex.Message}\r\n");
            }
            return res;
        }
        List<Tuple<string, string, string, string, string>> exec5(string command)
        {
            List<Tuple<string, string, string, string, string>> res = new List<Tuple<string, string, string, string, string>>();
            DataTable t = execDataTable(command);
            foreach (DataRow r in t.Rows)
                res.Add(Tuple.Create(r.ItemArray[0].ToString(), r.ItemArray[1].ToString(),
                                     r.ItemArray[2].ToString(), r.ItemArray[3].ToString(), r.ItemArray[4].ToString()));

            return res;
        }

        bool exec(string command1, string command2 = null, string command3 = null, bool log = true)
        {
            try
            {
                SqlCommand cmd = new SqlCommand(command1, Connection);
                cmd.CommandTimeout = 1000;
                cmd.ExecuteNonQuery();
                if (command2 != null)
                {
                    SqlCommand cmd2 = new SqlCommand(command2, Connection);
                    cmd2.CommandTimeout = 1000;
                    cmd2.ExecuteNonQuery();
                }
                if (command3 != null)
                {
                    SqlCommand cmd3 = new SqlCommand(command3, Connection);
                    cmd3.CommandTimeout = 1000;
                    cmd3.ExecuteNonQuery();
                }
                if (log) Log("Done.\r\n");
                return true;
            }
            catch (Exception ex)
            {
                if (log) Log($"Error.\r\nError: {ex.Message}\r\n");
                return false;
            }

        }

        void Log(string logText)
        {
            log(this, new LabelEditEventArgs(Convert.ToInt32(true), logText));
        }
    }
    class Index
    {
        EventHandler<LabelEditEventArgs> log;

        public string TableName { get; set; }
        public string Name { get; set; }
        public string Columns { get; set; } // тут может быть и несколько полей (есть такой sql запрос, который возвращает эти поля через запятую)
        public string Type { get; set; }
        public string Unique { get; set; }

        public string Database { get; set; }
        public SqlConnection Connection { get; set; }
        public Index(string tableName, string name, string columns, string type, string unique,
            string database, SqlConnection connection, EventHandler<LabelEditEventArgs> upperLog)
        {
            log += upperLog;

            TableName = tableName;
            Name = name;
            Columns = columns;
            Type = type;
            Unique = unique;

            Database = database;
            Connection = connection;

        }

        public void Create()
        {
            #region in SQL
            //CREATE NONCLUSTERED INDEX[indtblCLS_index]
            //ON[dbo].[tblCLS]([Deleted] ASC, [ISN_CLS] ASC, [ISN_HIGH_CLS] ASC);

            //CREATE UNIQUE NONCLUSTERED INDEX[IX_tblCLS]
            //ON[dbo].[tblCLS]([ISN_CLS] ASC);
            #endregion
            Log($"Create {Name}...");
            exec($"create {Unique} {Type} index {Name} on {Database}.dbo.{TableName} ({Columns})");

        }
        public void Delete()
        {
            Log($"Delete {Name}...");
            exec($"use {Database} ",
                 $"drop index {Name} on {TableName}",
                 $"use master");
        }
        bool exec(string command1, string command2 = null, string command3 = null)
        {
            try
            {
                SqlCommand cmd = new SqlCommand(command1, Connection);
                cmd.CommandTimeout = 1000;
                cmd.ExecuteNonQuery();
                if (command2 != null)
                {
                    SqlCommand cmd2 = new SqlCommand(command2, Connection);
                    cmd2.CommandTimeout = 1000;
                    cmd2.ExecuteNonQuery();
                }
                if (command3 != null)
                {
                    SqlCommand cmd3 = new SqlCommand(command3, Connection);
                    cmd3.CommandTimeout = 1000;
                    cmd3.ExecuteNonQuery();
                }
                Log("Done.\r\n");
                return true;
            }
            catch (Exception ex)
            {
                Log($"Error.\r\nError: {ex.Message}\r\n");
                return false;
            }

        }
        void Log(string logText)
        {
            log(this, new LabelEditEventArgs(Convert.ToInt32(true), logText));
        }
    }


    class IndexF
    {
        EventHandler<LabelEditEventArgs> log;

        public string TableName { get; set; }

        public string PK { get; set; } = "";
        public string Columns { get; set; } = "";
        public bool Exists { get; set; } = false;

        public string Database { get; set; }
        public SqlConnection Connection { get; set; }
        public IndexF(string tableName, string database, SqlConnection connection, EventHandler<LabelEditEventArgs> upperLog)
        {
            log += upperLog;

            TableName = tableName;

            Database = database;
            Connection = connection;

            GetIndexF();
        }
        public IndexF(IndexF indexf, string database, EventHandler<LabelEditEventArgs> upperLog)
        {
            log += upperLog;

            Database = database;
            Connection = indexf.Connection;
            TableName = indexf.TableName;

            PK = indexf.PK;
            Columns = indexf.Columns;
            Exists = indexf.Exists;
        }
        void GetIndexF()
        {
            #region in SQL
            //SELECT i.name AS UniqueIdxName, cl.name AS ColumnName
            //FROM sys.tables t
            //INNER JOIN sys.fulltext_indexes fi ON t.[object_id] = fi.[object_id]
            //INNER JOIN sys.fulltext_index_columns ic ON ic.[object_id] = t.[object_id]
            //INNER JOIN sys.columns cl ON ic.column_id = cl.column_id AND ic.[object_id] = cl.[object_id]
            //INNER JOIN sys.fulltext_catalogs c ON fi.fulltext_catalog_id = c.fulltext_catalog_id
            //INNER JOIN sys.indexes i ON fi.unique_index_id = i.index_id AND fi.[object_id] = i.[object_id]
            //WHERE t.name = 'tblARCHIVE'
            #endregion
            Log($"Get IXF for {TableName}...");
            List<Tuple<string, string>> temp =
                exec2($"use {Database} " +
                               $"SELECT i.name AS UniqueIdxName, cl.name AS ColumnName " +
                               $"FROM sys.tables t " +
                               $"INNER JOIN sys.fulltext_indexes fi ON t.[object_id] = fi.[object_id] " +
                               $"INNER JOIN sys.fulltext_index_columns ic ON ic.[object_id] = t.[object_id] " +
                               $"INNER JOIN sys.columns cl ON ic.column_id = cl.column_id AND ic.[object_id] = cl.[object_id] " +
                               $"INNER JOIN sys.fulltext_catalogs c ON fi.fulltext_catalog_id = c.fulltext_catalog_id " +
                               $"INNER JOIN sys.indexes i ON fi.unique_index_id = i.index_id AND fi.[object_id] = i.[object_id] " +
                               $"WHERE t.name = '{TableName}' " +
                               $"use master");

            foreach (var c in temp)
            {
                PK = c.Item1;
                Columns += c.Item2 + " LANGUAGE 0, ";
            }

            if (PK != "" && Columns != "") Exists = true;
            else return;
            Columns = Columns.Trim(new char[] { ',', ' ' });
        }

        public void Create()
        {
            #region in SQL
            //Log($"Create IX_FULLTEXT...");
            //exec($"CREATE FULLTEXT INDEX ON [dbo].[tblAuthorizedDep] " +
            //     $"([ShortName] LANGUAGE 0, [FullName] LANGUAGE 0, [Address] LANGUAGE 0, [District] LANGUAGE 0) " +
            //     $"KEY INDEX [PK_tblAuthorizedDep] " +
            //     $"ON [SearchCatalog];");
            #endregion
            if (Exists)
            {
                Log($"Create IXF_{TableName}...");
                exec($"use {Database}",
                     $"CREATE FULLTEXT INDEX ON {TableName} " +
                     $"({Columns}) " +
                     $"KEY INDEX {PK} " +
                     $"ON [SearchCatalog]",
                     $"use master");
            }
        }
        public void Delete()
        {
            Log($"Delete IXF_{TableName}...");
            exec($"use {Database} ",
                 $"drop fulltext index on {TableName}",
                 $"use master");
        }

        DataTable execDataTable(string command)
        {
            DataTable res = new DataTable();
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(command, Connection);
                da.Fill(res);
                Log("Done.\r\n");
            }
            catch (Exception ex)
            {
                Log($"Error.\r\nError: {ex.Message}\r\n");
            }
            return res;
        }
        List<Tuple<string, string>> exec2(string command)
        {
            List<Tuple<string, string>> res = new List<Tuple<string, string>>();
            DataTable t = execDataTable(command);
            foreach (DataRow r in t.Rows)
                res.Add(Tuple.Create(r.ItemArray[0].ToString(), r.ItemArray[1].ToString()));

            return res;
        }


        bool exec(string command1, string command2 = null, string command3 = null)
        {
            try
            {
                SqlCommand cmd = new SqlCommand(command1, Connection);
                cmd.CommandTimeout = 1000;
                cmd.ExecuteNonQuery();
                if (command2 != null)
                {
                    SqlCommand cmd2 = new SqlCommand(command2, Connection);
                    cmd2.CommandTimeout = 1000;
                    cmd2.ExecuteNonQuery();
                }
                if (command3 != null)
                {
                    SqlCommand cmd3 = new SqlCommand(command3, Connection);
                    cmd3.CommandTimeout = 1000;
                    cmd3.ExecuteNonQuery();
                }
                Log("Done.\r\n");
                return true;
            }
            catch (Exception ex)
            {
                Log($"Error.\r\nError: {ex.Message}\r\n");
                return false;
            }

        }
        void Log(string logText)
        {
            log(this, new LabelEditEventArgs(Convert.ToInt32(true), logText));
        }
    }

}
