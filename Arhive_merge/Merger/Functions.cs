﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Data;
namespace Merger
{
    class Functions
    {
        public event EventHandler<LabelEditEventArgs> log;

        public List<Function> Items { get; set; } = new List<Function>();

        public string Database { get; set; }
        public SqlConnection Connection { get; set; }
        public Functions(string database, SqlConnection connection, EventHandler<LabelEditEventArgs> upperLog)
        {
            log += upperLog;

            Database = database;
            Connection = connection;

            Items = GetFunctions();
        }
        public Functions(Functions functions, string database, EventHandler<LabelEditEventArgs> upperLog)
        {
            log += upperLog;

            Database = database;
            Connection = functions.Connection;

            foreach (var f in functions)
                Items.Add(new Function(f.Name, f.Definition, Database, Connection, log));
        }

        public void Create()
        {
            foreach (var f in Items)
                f.Create();
        }

        List<Function> GetFunctions()
        {
            #region in SQL
            //select o.name, m.definition
            //from sys.objects as o
            //inner join sys.schemas as s on o.schema_id = s.schema_id
            //inner join sys.sql_modules as m on o.object_id = m.object_id
            //where o.type <> 'V' order by type_desc, o.name
            #endregion

            Log($"Get functions from {Database}...");
            Dictionary<string, string> res = execDictionary($"use {Database} " +
                $"select o.name, m.definition " +
                $"from sys.objects as o " +
                $"inner join sys.schemas as s on o.schema_id = s.schema_id " +
                $"inner join sys.sql_modules as m on o.object_id = m.object_id " +
                $"where o.type <> 'V' order by type_desc, o.name " +
                $"use master");

            List<Function> temp = new List<Function>();
            foreach (var f in res)
                temp.Add(new Function(f.Key, f.Value, Database, Connection, log));
            return temp;
        }

        public IEnumerator<Function> GetEnumerator()
        {
            return Items.GetEnumerator();
        }

        DataTable execDataTable(string command)
        {
            DataTable res = new DataTable();
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(command, Connection);
                da.Fill(res);
                Log("Done.\r\n");
            }
            catch (Exception ex)
            {
                Log($"Error.\r\nError: {ex.Message}\r\n");
            }
            return res;
        }
        Dictionary<string, string> execDictionary(string command)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            DataTable t = execDataTable(command);
            foreach (DataRow r in t.Rows)
                result.Add(r.ItemArray[0].ToString(), r.ItemArray[1].ToString());
            return result;
        }
        void Log(string logText)
        {
            log(this, new LabelEditEventArgs(Convert.ToInt32(true), logText));
        }

    }
    class Function
    {
        public event EventHandler<LabelEditEventArgs> log;

        public string Name { get; set; }
        public string Definition { get; set; }

        public string Database { get; set; }
        public SqlConnection Connection { get; set; }
        public Function(string name, string definition, string database, SqlConnection connection, EventHandler<LabelEditEventArgs> upperLog)
        {
            log += upperLog;

            Database = database;
            Connection = connection;

            Name = name;
            Definition = definition;
        }


        public void Create()
        {

            Log($"Create {Name}...");
            exec($"use {Database}", Definition, $"use master");
        }

        bool exec(string command1, string command2 = null, string command3 = null, bool log = true)
        {
            try
            {
                SqlCommand cmd = new SqlCommand(command1, Connection);
                cmd.CommandTimeout = 1000;
                cmd.ExecuteNonQuery();
                if (command2 != null)
                {
                    SqlCommand cmd2 = new SqlCommand(command2, Connection);
                    cmd2.CommandTimeout = 1000;
                    cmd2.ExecuteNonQuery();
                }
                if (command3 != null)
                {
                    SqlCommand cmd3 = new SqlCommand(command3, Connection);
                    cmd3.CommandTimeout = 1000;
                    cmd3.ExecuteNonQuery();
                }
                if (log) Log("Done.\r\n");
                return true;
            }
            catch (Exception ex)
            {
                if (log) Log($"Error.\r\nError: {ex.Message}\r\n");
                return false;
            }

        }
        void Log(string logText)
        {
            log(this, new LabelEditEventArgs(Convert.ToInt32(true), logText));
        }

    }
}
