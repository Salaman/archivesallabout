﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data;
namespace Merger
{

    class Dependencies
    {
        public event EventHandler<LabelEditEventArgs> log;

        public List<Dependence> Items { get; set; } = new List<Dependence>();

        public string TableName { get; set; }
        public string Database { get; set; }
        public SqlConnection Connection { get; set; }
        public Dependencies(string tableName, bool hasISN, string database, SqlConnection connection, EventHandler<LabelEditEventArgs> upperLog)
        {
            log += upperLog;

            Database = database;
            Connection = connection;

            TableName = tableName;

            Items = hasISN ? GetDependencies(TableName) : new List<Dependence>();
        }
        public Dependencies(Dependencies dependencies, string database, EventHandler<LabelEditEventArgs> upperLog)
        {
            log += upperLog;

            Database = database;
            Connection = dependencies.Connection;

            TableName = dependencies.TableName;

            foreach (var d in dependencies)
                Items.Add(new Dependence(d.FKName, d.PKTable, d.PKColumn, d.FKTable, d.FKColumn, Database, Connection, log));
        }

        List<Dependence> GetDependencies(string tableName)
        {
            Log($"Get Dependencies for {tableName}...");
            List<Tuple<string, string, string, string, string>> res = exec5($"use {Database} " +
                            $"create table #temp (pkq varchar(128),pko varchar(128),PKTable varchar(128),PKColumn varchar(128), " +
                            $"fkq varchar(128),fko varchar(128),FKTable varchar(128),FKColumn varchar(128), " +
                            $"k varchar(128),u varchar(128), d varchar(128), FKName varchar(128),PK varchar(128), def varchar(128)) " +
                            $"insert into #temp exec sp_fkeys '{tableName}' " +
                            $"select FKName, 'dbo.' + PKTable, PKColumn, 'dbo.' + FKTable, FKColumn from #temp " +
                            $"drop table #temp " +
                            $"use master");

            List<Dependence> lst = new List<Dependence>();
            foreach (var t in res)
                lst.Add(new Dependence(t.Item1, t.Item2, t.Item3, t.Item4, t.Item5, Database, Connection, log));

            return lst;
        }

        public IEnumerator<Dependence> GetEnumerator()
        {
            return Items.GetEnumerator();
        }

        DataTable execDataTable(string command)
        {
            DataTable res = new DataTable();
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(command, Connection);
                da.Fill(res);
                Log("Done.\r\n");
            }
            catch (Exception ex)
            {
                Log($"Error.\r\nError: {ex.Message}\r\n");
            }
            return res;
        }
        List<Tuple<string, string, string, string, string>> exec5(string command)
        {
            List<Tuple<string, string, string, string, string>> res = new List<Tuple<string, string, string, string, string>>();
            DataTable t = execDataTable(command);
            foreach (DataRow r in t.Rows)
                res.Add(Tuple.Create(r.ItemArray[0].ToString(), r.ItemArray[1].ToString(), r.ItemArray[2].ToString(),
                    r.ItemArray[3].ToString(), r.ItemArray[4].ToString()));

            return res;
        }

        void Log(string logText)
        {
            log(this, new LabelEditEventArgs(Convert.ToInt32(true), logText));
        }
    }

    class Dependence
    {
        public event EventHandler<LabelEditEventArgs> log;

        public string FKName { get; set; }
        public string PKTable { get; set; }
        public string PKColumn { get; set; }
        public string FKTable { get; set; }
        public string FKColumn { get; set; }

        public bool Cascade { get; set; }
        public string TriggerName { get; set; } = "";
        public bool IsSelf
        {
            get { return PKTable == FKTable; }
        }
        public int RowsCount
        {
            get
            {
                return Convert.ToInt32(execScalar($"use {Database} select count(*) from {PKTable.Replace("dbo.","")} use master"));
            }
        }
        public string Database { get; set; }
        public SqlConnection Connection { get; set; }
        public Dependence(string fkName, string pkTable, string pkColumn, string fkTable, string fkColumn, string database, SqlConnection connection, EventHandler<LabelEditEventArgs> upperLog)
        {
            log += upperLog;

            Database = database;
            Connection = connection;

            FKName = fkName;
            PKTable = pkTable;
            PKColumn = pkColumn;
            FKTable = fkTable;
            FKColumn = fkColumn;

            TriggerName = FKName.Replace("FK_", "TR_");
        }

        public void SetCascade(bool on)
        {
            if (!IsSelf)
            {
                    Log($"Cascade {(on ? "on" : "off")}: {FKName}...");
                    if (exec($"alter table {Database}.{FKTable} " +
                           $"drop constraint {FKName}",
                           $"alter table {Database}.{FKTable} " +
                           $"add constraint {FKName} " +
                           $"foreign key ({FKColumn}) references {Database}.{PKTable}({PKColumn}) " +
                           $"on update " + (on ? "cascade" : "no action"))) Cascade = on;
            }
            else
            {
                if (PKColumn.StartsWith("ISN"))
                {
                    if (on)
                    {
                        if (IsSelf && PKColumn == "ISN_LOCATION" && RowsCount == 1)
                            Delete();
                        else
                        {
                            Delete();
                            CreateTrigger();
                        }
                    }
                    else
                    {
                        if (IsSelf && PKColumn == "ISN_LOCATION" && RowsCount == 1)
                            Create();
                        else
                        {
                            DeleteTrigger();
                            Create();
                        }
                    }
                }

            }

            #region in SQL
            //create trigger TR_tblDOCUMENT_STATS_DocID_tblFUND_ID on tblFUND after update as
            //if update(ID) begin
            //create table #temp(ID1 uniqueidentifier, ID1_NEW uniqueidentifier, ID2 uniqueidentifier)
            //insert into #temp 
            //select d.ID, i.ID , ds.ID from deleted as d
            //left join tblDOCUMENT_STATS as ds on ds.DocID = d.ID
            //left join inserted as i on i.ISN_FUND = d.ISN_FUND
            //update tblDOCUMENT_STATS set DocID = ID1_NEW from #temp where tblDOCUMENT_STATS.ID = #temp.ID2 
            //drop table #temp 
            //end
            #endregion
            //if (on)
            //{
            //    SetRefID(FKTable.Replace("dbo.", ""), FKColumn, PKTable.Replace("dbo.", ""), PKColumn, "ID", on);
            //}
            //else
            //{
            //    Log($"Delete TR_{FKTable.Replace("dbo.", "")}_{FKColumn}_{PKTable.Replace("dbo.", "")}_{PKColumn}...");
            //    exec($"use {Database}",
            //         $"drop trigger TR_{FKTable.Replace("dbo.", "")}_{FKColumn}_{PKTable.Replace("dbo.", "")}_{PKColumn}",
            //         $"use master");
            //}
        }

        void SetRefID(string dtable, string dcolumn, string table, string column, string field, bool on)
        {
            string name = $"TR_{dtable}_{dcolumn}_{table}_{column}";

            string type;
            if (dcolumn.StartsWith("ISN")) type = "bigint";
            else type = "uniqueidentifier";
            #region help SQL
            //select f.ID, ds.DocID , ds.ID from tableName as f
            //left join tblDOCUMENT_STATS as ds on ds.DocID = f.ID
            #endregion
            #region in SQL
            //create trigger TR_tblDOCUMENT_STATS_DocID_tblFUND_ID on tblFUND after update as
            //if update(ID) begin
            //create table #temp(ID1 uniqueidentifier, ID1_NEW uniqueidentifier, ID2 uniqueidentifier)
            //insert into #temp 
            //select d.ID, i.ID , ds.ID from deleted as d
            //left join tblDOCUMENT_STATS as ds on ds.DocID = d.ID
            //left join inserted as i on i.ISN_FUND = d.ISN_FUND
            //update tblDOCUMENT_STATS set DocID = ID1_NEW from #temp where tblDOCUMENT_STATS.ID = #temp.ID2 
            //drop table #temp 
            //end
            #endregion
            if (on)
            {
                Log($"Create {name}...");
                exec($"use {Database}",
                     $"create trigger {name} on {table} after update as " +
                     $"if update({column}) begin " +
                     $"create table #temp(ID1 {type}, ID1_NEW {type}, ID2 uniqueidentifier) " +
                     $"insert into #temp " +
                     $"select d.{column}, i.{column} , ds.ID from deleted as d " +
                     $"left join {dtable} as ds on ds.{dcolumn} = d.{column} " +
                     $"left join inserted as i on i.{field} = d.{field} " +
                     $"update {dtable} set {dcolumn} = ID1_NEW from #temp where {dtable}.ID = #temp.ID2 " +
                     $"drop table #temp " +
                     $"end",
                     $"use master");
            }
            else
            {
                Log($"Delete {name}...");
                exec($"use {Database}", $"drop trigger {name}", "use master");
            }
        }



        public void Create()
        {
            Log($"Create {FKName}...");
            exec($"alter table {Database}.{FKTable} " +
                 $"add constraint {FKName} " +
                 $"foreign key ({FKColumn}) references {Database}.{PKTable}({PKColumn})");
        }
        public void Delete()
        {
            Log($"Delete {FKName}...");
            exec($"alter table {Database}.{FKTable} " +
                 $"drop constraint {FKName}");
        }
        void CreateTrigger()
        {
            Log($"Create {TriggerName}...");

            exec($"use {Database}",
                $"create trigger {TriggerName} on {PKTable} after update as " +
                $"if update({PKColumn}) begin " +
                $"create table #temp({FKColumn} bigint, ID uniqueidentifier) " +
                $"insert into #temp " +
                $"select i.{PKColumn}, b.ID from deleted as a " +
                $"left join {PKTable} as b on b.{FKColumn} = a.{PKColumn} " +
                $"left join inserted as i on i.ID = a.ID " +
                $"where b.ID is not null " +
                $"update {PKTable} set {FKColumn} = #temp.{FKColumn} from #temp " +
                $"where {PKTable}.ID = #temp.ID " +
                $"drop table #temp " +
                $"end",
                $"use master");
        }
        void DeleteTrigger()
        {
            Log($"Delete {TriggerName}...");
            exec($"use {Database}",
                 $"drop trigger {TriggerName}",
                 $"use master");
        }

        bool exec(string command1, string command2 = null, string command3 = null)
        {
            try
            {
                SqlCommand cmd = new SqlCommand(command1, Connection);
                cmd.CommandTimeout = 1000;
                cmd.ExecuteNonQuery();
                if (command2 != null)
                {
                    SqlCommand cmd2 = new SqlCommand(command2, Connection);
                    cmd2.CommandTimeout = 1000;
                    cmd2.ExecuteNonQuery();
                }
                if (command3 != null)
                {
                    SqlCommand cmd3 = new SqlCommand(command3, Connection);
                    cmd3.CommandTimeout = 1000;
                    cmd3.ExecuteNonQuery();
                }
                Log("Done.\r\n");
                return true;
            }
            catch (Exception ex)
            {
                Log($"Error.\r\nError: {ex.Message}\r\n");
                return false;
            }

        }
        object execScalar(string command)
        {
            object val = null;
            try
            {
                SqlCommand cmd = new SqlCommand(command, Connection);
                val = cmd.ExecuteScalar();
            }
            catch (Exception ex)
            {
                Log($"Error: {ex.Message}");
            }
            return val;
        }
        void Log(string logText)
        {
            log(this, new LabelEditEventArgs(Convert.ToInt32(true), logText));
        }
    }

}
