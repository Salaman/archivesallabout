﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Windows.Forms;
using System.IO;
namespace Merger
{
    public class XmlManager
    {
        XmlDocument doc;
        MainForm form;
        Control.ControlCollection controls;
        string path = Application.StartupPath + "\\settings.xml";
        public XmlManager(MainForm form)
        {
            this.form = form;
            controls = form.Controls;
        }
        public void Save()
        {
            doc = new XmlDocument();
            XmlDeclaration xmlDeclaration = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlElement root = doc.CreateElement("Settings");
            doc.AppendChild(root);
            doc.InsertBefore(xmlDeclaration, root);

            foreach (TextBox t in controls.OfType<TextBox>().Where(o => o.Name != "_Log"))
            {
                XmlElement element = doc.CreateElement(t.Name);
                XmlText text = doc.CreateTextNode(t.Text);
                element.AppendChild(text);
                root.AppendChild(element);
            }
            doc.Save(path);
        }
        public XmlManager Load()
        {
            doc = new XmlDocument();
            if (File.Exists(path))
            {
                doc.Load(path);
                foreach (TextBox t in controls.OfType<TextBox>().Where(o => o.Name != "_Log"))
                    t.Text = doc.SelectSingleNode($"/Settings/{t.Name}").InnerText;
            }
            else Save();
            return this;
        }
    }
}
