﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Data;
namespace Merger
{
    class Tables
    {
        public event EventHandler<LabelEditEventArgs> log;
        public List<Table> Items { get; set; } = new List<Table>();
        public int Count { get { return Items.Count; } }
        public List<Tuple<string, string, string, string>> DefaultValues { get; set; }

        public string Database { get; set; }
        public SqlConnection Connection { get; set; }
        public Tables(string database, SqlConnection connection, EventHandler<LabelEditEventArgs> upperLog)
        {
            log += upperLog;

            Database = database;
            Connection = connection;

        }

        public Tables(Tables tables, string database, EventHandler<LabelEditEventArgs> upperLog)
        {
            log += upperLog;

            Database = database;
            Connection = tables.Connection;

            foreach (var t in tables)
                Add(t.FullName, t.Dependencies, t.PrimaryKey, t.IndexF, t.Indexes, t.ForeignKeys);
        }

        public void Add(string fullTableName,
            Dependencies dependencies = null, PrimaryKey primaryKey = null, IndexF indexf = null, Indexes indexes = null, ForeignKeys foreignKeys = null)
        {
            if (primaryKey == null) Log($"Get Table {fullTableName.Replace("dbo.", "")}...Done.\r\n");
            Items.Add(new Table(fullTableName, Database, Connection, log, dependencies, primaryKey, indexf, indexes, foreignKeys));
        }

        public void Except(string name)
        {
            if (name.EndsWith("*"))
            {
                name = name.Remove(name.Length - 1);
                Items.RemoveAll(t => t.Name.Contains(name));
            }
            else
                Items.RemoveAll(t => t.Name == name);
        }
        public List<Tuple<string, string, string, string>> GetDefaultValues()
        {
            //in SQL
            //SELECT SO.NAME AS "Table Name", SC.NAME AS "Column Name",COL.DATA_TYPE, SM.TEXT AS "Default Value"
            //FROM dbo.sysobjects SO INNER JOIN dbo.syscolumns SC ON SO.id = SC.id
            //LEFT JOIN dbo.syscomments SM ON SC.cdefault = SM.id
            //LEFT JOIN INFORMATION_SCHEMA.COLUMNS COL ON SO.NAME = COL.TABLE_NAME and SC.NAME = COL.COLUMN_NAME
            //WHERE SO.xtype = 'U' and SM.TEXT is not null
            //ORDER BY SO.[name], SC.colid

            Log($"Get default values for {Database}...");
            DefaultValues = exec4($"use {Database} " +
                                  $"SELECT SO.NAME, SC.NAME, COL.DATA_TYPE, SM.TEXT " +
                                  $"FROM dbo.sysobjects SO INNER JOIN dbo.syscolumns SC ON SO.id = SC.id " +
                                  $"LEFT JOIN dbo.syscomments SM ON SC.cdefault = SM.id " +
                                  $"LEFT JOIN INFORMATION_SCHEMA.COLUMNS COL ON SO.NAME = COL.TABLE_NAME and SC.NAME = COL.COLUMN_NAME " +
                                  $"WHERE SO.xtype = 'U' and SM.TEXT is not null " +
                                  $"ORDER BY SO.[name], SC.colid " +
                                  $"use master");
            return DefaultValues;
        }
        public void SetDefaultValues()
        {
            foreach (var v in DefaultValues)
            {
                if (v.Item1 == "eqGroups" || v.Item1 == "eqRoles")
                    continue;
                this[v.Item1].SetDefault(v.Item2, v.Item3, v.Item4);
            }
        }
        public void CreateConstraints()
        {
            exec($"use {Database}", null, null, false);

            Log($"Create SearchCatalog...");
            exec($"CREATE FULLTEXT CATALOG [SearchCatalog] " +
                 $"WITH ACCENT_SENSITIVITY = ON " +
                 $"AUTHORIZATION [dbo]");
            exec("use master", null, null, false);

            foreach (var i in Items)
                i.PrimaryKey.Create();
            foreach (var i in Items)
                i.IndexF.Create();
            foreach (var i in Items)
                i.Indexes.Create();
            foreach (var i in Items)
            {
                i.ForeignKeys.Create();
            }
        }

        public void IdentityOff()
        {
            foreach (var t in Items)
            {
                if (t.Name == "tblSECURITY_REASON")
                {
                    foreach (var d in t.Dependencies) d.Delete();
                    foreach (var i in t.Indexes) i.Delete();

                    Log($"Identity off: {t.Name}...");
                    t.IdentityOff();


                    foreach (var i in t.Indexes) i.Create();
                    foreach (var d in t.Dependencies) d.Create();
                }
            }
        }

        #region SetDefault
        //на всякий случай , если из-за подвязок не получится дать полю значение по умолчанию
        //public void SetDefaults()
        //{
        //    foreach (var t in Items)
        //    {
        //        if (t.Name == "eqDocStatesLOG")
        //        {
        //            foreach (var d in t.Dependencies) d.Delete();
        //            foreach (var i in t.Indexes) i.Delete();

        //            Log($"Set Default: EventTime = (getdate())...");
        //            t.SetDefault("EventTime", "datetime", "(getdate())");


        //            foreach (var i in t.Indexes) i.Create();
        //            foreach (var d in t.Dependencies) d.Create();
        //        }
        //    }
        //}
        #endregion

        public IEnumerator<Table> GetEnumerator()
        {
            return Items.GetEnumerator();
        }
        public Table this[int i]
        {
            get
            {
                return Items[i];
            }
        }
        public Table this[string name]
        {
            get
            {
                return Items.Find(t => t.Name == name);
            }
        }
        bool exec(string command1, string command2 = null, string command3 = null, bool log = true)
        {
            try
            {
                SqlCommand cmd = new SqlCommand(command1, Connection);
                cmd.CommandTimeout = 1000;
                cmd.ExecuteNonQuery();
                if (command2 != null)
                {
                    SqlCommand cmd2 = new SqlCommand(command2, Connection);
                    cmd2.CommandTimeout = 1000;
                    cmd2.ExecuteNonQuery();
                }
                if (command3 != null)
                {
                    SqlCommand cmd3 = new SqlCommand(command3, Connection);
                    cmd3.CommandTimeout = 1000;
                    cmd3.ExecuteNonQuery();
                }
                if (log) Log("Done.\r\n");
                return true;
            }
            catch (Exception ex)
            {
                if (log) Log($"Error.\r\nError: {ex.Message}\r\n");
                return false;
            }

        }
        DataTable execDataTable(string command)
        {
            DataTable res = new DataTable();
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(command, Connection);
                da.Fill(res);
                Log("Done.\r\n");
            }
            catch (Exception ex)
            {
                Log($"Error.\r\nError: {ex.Message}\r\n");
            }
            return res;
        }
        List<Tuple<string, string, string>> exec3(string command)
        {
            List<Tuple<string, string, string>> res = new List<Tuple<string, string, string>>();
            DataTable t = execDataTable(command);
            foreach (DataRow r in t.Rows)
                res.Add(Tuple.Create(r.ItemArray[0].ToString(), r.ItemArray[1].ToString(), r.ItemArray[2].ToString()));

            return res;
        }
        List<Tuple<string, string, string, string>> exec4(string command)
        {
            List<Tuple<string, string, string, string>> res = new List<Tuple<string, string, string, string>>();
            DataTable t = execDataTable(command);
            foreach (DataRow r in t.Rows)
                res.Add(Tuple.Create(r.ItemArray[0].ToString(), r.ItemArray[1].ToString(), r.ItemArray[2].ToString(),
                    r.ItemArray[3].ToString()));

            return res;
        }

        void Log(string logText)
        {
            log(this, new LabelEditEventArgs(Convert.ToInt32(true), logText));
        }
    }

    class Table
    {
        public event EventHandler<LabelEditEventArgs> log;

        public PrimaryKey PrimaryKey { get; set; }
        public ForeignKeys ForeignKeys { get; set; }
        public Indexes Indexes { get; set; }
        public IndexF IndexF { get; set; }

        public string Name { get; set; }
        public string FullName { get; set; }
        public bool HasISN
        {
            get
            {
                return Convert.ToBoolean(execScalar($"select count(1) from " +
                    $"{Database}.information_schema.columns where TABLE_NAME = '{Name}' and COLUMN_NAME = '{GetCorrectISN()}'"));
            }
        }
        public bool HasID
        {
            get
            {
                return Convert.ToBoolean(execScalar($"select count(1) from " +
                    $"{Database}.information_schema.columns where TABLE_NAME = '{Name}' and COLUMN_NAME = 'ID'"));
            }
        }
        public string ISN { get; set; } = "";

        public long FirstISN { get; set; }
        public long LastISN
        {
            get
            {
                if (HasISN)
                {
                    try
                    {
                        object res = execScalar($"select max({ISN}) from {Database}.{FullName}");
                        return Convert.ToInt64(res);
                    }
                    catch (Exception)
                    {
                        return 0;
                    }

                }
                else return 0;
            }
        }

        public Dependencies Dependencies { get; set; } = null;

        public SqlConnection Connection { get; set; }
        public string Database { get; set; }
        public Table(string tableFullName, string database, SqlConnection connection, EventHandler<LabelEditEventArgs> upperLog,
            Dependencies dependencies = null, PrimaryKey primaryKey = null, IndexF indexf = null, Indexes indexes = null, ForeignKeys foreignKeys = null)
        {
            log += upperLog;
            FullName = tableFullName;
            Name = FullName.Replace("dbo.", "");

            Database = database;
            Connection = connection;


            if (HasISN) ISN = GetCorrectISN();



            Dependencies = dependencies == null ? new Dependencies(Name, HasISN, Database, Connection, log) :
                                                  new Dependencies(dependencies, Database, log);

            PrimaryKey = primaryKey == null ? new PrimaryKey(Name, Database, Connection, log) :
                                              new PrimaryKey(primaryKey, Database, log);

            IndexF = indexf == null ? new IndexF(Name, Database, Connection, log) :
                                      new IndexF(indexf, Database, log);

            Indexes = indexes == null ? new Indexes(Name, Database, Connection, log) :
                                        new Indexes(indexes, Database, log);

            ForeignKeys = foreignKeys == null ? new ForeignKeys(Name, Database, Connection, log) :
                                                new ForeignKeys(foreignKeys, Database, log);

        }

        public void IdentityOff()
        {
            exec($"use {Database}", null, null, false);

            exec($"alter table {Name} add {ISN}_N bigint", null, null, false);

            exec($"update {Name} set {ISN}_N = {ISN}", null, null, false);

            exec($"alter table {Name} drop column {ISN}");

            exec($"exec sp_rename '{Name}.{ISN}_N', '{ISN}', 'COLUMN'", null, null, false);

            exec("use master", null, null, false);
        }

        public void SetDefault(string field, string type, string value)
        {
            string field1 = field;
            if (field == "RollBack") field1 = "[RollBack]";
            if (field == "Open") field1 = "[Open]";
            if (type == "nvarchar") type = "nvarchar(255)";
            if (type == "varchar") type = "varchar(500)";

            Log($"Set default in {Name}: {field} = {value}...");
            exec($"use {Database}", null, null, false);

            exec($"alter table {Name} add {field}_N {type} not null default {value} ", null, null, false);

            exec($"update {Name} set {field}_N = {field1}", null, null, false);

            exec($"alter table {Name} drop column {field1}");// тут была проверка Done

            exec($"exec sp_rename '{Name}.{field}_N', '{field}', 'COLUMN'", null, null, false);

            exec("use master", null, null, false);
        }
        public void ChangeISN()
        {
            if (HasISN)
            {
                Log($"Change {Name}.{ISN}...");
                exec($"declare @i bigint = {FirstISN} " +
                     $"update {Database}.{FullName} set @i = {ISN} = @i+1");
            }
        }
        public void ChangeID() //gg
        {
            if (Name == "tblINVENTORY_STRUCTURE")
            {
                exec($"update {Database}.{FullName} set ID = newid() where DocID = '00000000-0100-0000-0000-000000000000'");
                return;
            }
            
            if (HasID)
            {
                Log($"Change {Name}.ID...");
                exec($"update {Database}.{FullName} set ID = newid()");
            }
        }

        public void SetCascade(bool on)
        {
            SetRefsID(on);

            foreach (var d in Dependencies)
                d.SetCascade(on);
        }


        public void CopyTo(string database)
        {
            Log($"Copy {Name}...");
            exec($"use {database} select * into {FullName} from {Database}.{FullName} use master");
        }

        void SetRefsID(bool on)
        {
            #region help SQL
            //select t.name, c.name from sys.columns c
            //left join sys.tables t on c.object_id = t.object_id
            //where
            //t.name is not null
            //and t.name not like 'eq%'
            //and c.name = 'DocID'
            //order by t.name
            #endregion
            #region find by value in db
            //DECLARE @SearchStrTableName nvarchar(255), @SearchStrColumnName nvarchar(255), @SearchStrColumnValue nvarchar(255), @SearchStrInXML bit, @FullRowResult bit, @FullRowResultRows int
            //SET @SearchStrColumnValue = '%value%' /* use LIKE syntax */
            //SET @FullRowResult = 1
            //SET @FullRowResultRows = 3
            //SET @SearchStrTableName = NULL /* NULL for all tables, uses LIKE syntax */
            //SET @SearchStrColumnName = NULL /* NULL for all columns, uses LIKE syntax */
            //SET @SearchStrInXML = 0 /* Searching XML data may be slow */

            //IF OBJECT_ID('tempdb..#Results') IS NOT NULL DROP TABLE #Results
            //CREATE TABLE #Results (TableName nvarchar(128), ColumnName nvarchar(128), ColumnValue nvarchar(max),ColumnType nvarchar(20))

            //SET NOCOUNT ON

            //DECLARE @TableName nvarchar(256) = '',@ColumnName nvarchar(128),@ColumnType nvarchar(20), @QuotedSearchStrColumnValue nvarchar(110), @QuotedSearchStrColumnName nvarchar(110)
            //SET @QuotedSearchStrColumnValue = QUOTENAME(@SearchStrColumnValue, '''')
            //DECLARE @ColumnNameTable TABLE(COLUMN_NAME nvarchar(128), DATA_TYPE nvarchar(20))

            //WHILE @TableName IS NOT NULL
            //BEGIN
            //    SET @TableName =
            //    (
            //        SELECT MIN(QUOTENAME(TABLE_SCHEMA) + '.' + QUOTENAME(TABLE_NAME))
            //        FROM INFORMATION_SCHEMA.TABLES
            //     WHERE       TABLE_TYPE = 'BASE TABLE'
            //            AND TABLE_NAME LIKE COALESCE(@SearchStrTableName, TABLE_NAME)
            //            AND QUOTENAME(TABLE_SCHEMA) +'.' + QUOTENAME(TABLE_NAME) > @TableName
            //            AND OBJECTPROPERTY(OBJECT_ID(QUOTENAME(TABLE_SCHEMA) +'.' + QUOTENAME(TABLE_NAME)), 'IsMSShipped') = 0
            //    )
            //    IF @TableName IS NOT NULL
            //    BEGIN
            //        DECLARE @sql VARCHAR(MAX)
            //        SET @sql = 'SELECT QUOTENAME(COLUMN_NAME),DATA_TYPE
            //                FROM INFORMATION_SCHEMA.COLUMNS
            //             WHERE       TABLE_SCHEMA = PARSENAME(''' + @TableName + ''', 2)
            //                AND TABLE_NAME = PARSENAME(''' + @TableName + ''', 1)
            //                AND DATA_TYPE IN(' + CASE WHEN ISNUMERIC(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@SearchStrColumnValue,' % ',''),'_',''),'[',''),']',''),' - ','')) = 1 THEN '''tinyint'', ''int'', ''smallint'', ''bigint'', ''numeric'', ''decimal'', ''smallmoney'', ''money'', ' ELSE '' END + '''char'', ''varchar'', ''nchar'', ''nvarchar'', ''timestamp'', ''uniqueidentifier''' + CASE @SearchStrInXML WHEN 1 THEN ', ''xml''' ELSE '' END + ')
            //                AND COLUMN_NAME LIKE COALESCE(' + CASE WHEN @SearchStrColumnName IS NULL THEN 'NULL' ELSE '''' + @SearchStrColumnName + '''' END  + ',COLUMN_NAME)'
            //        INSERT INTO @ColumnNameTable
            //        EXEC(@sql)
            //        WHILE EXISTS(SELECT TOP 1 COLUMN_NAME FROM @ColumnNameTable)
            //        BEGIN
            //            PRINT @ColumnName
            //            SELECT TOP 1 @ColumnName = COLUMN_NAME,@ColumnType = DATA_TYPE FROM @ColumnNameTable
            //            SET @sql = 'SELECT ''' + @TableName + ''',''' + @ColumnName + ''',' + CASE @ColumnType WHEN 'xml' THEN 'LEFT(CAST(' + @ColumnName + ' AS nvarchar(MAX)), 4096),'''
            //            WHEN 'timestamp' THEN 'master.dbo.fn_varbintohexstr(' + @ColumnName + '),'''
            //            ELSE 'LEFT(' + @ColumnName + ', 4096),''' END + @ColumnType + ''' 
            //                    FROM ' + @TableName + '(NOLOCK) ' +
            //                    ' WHERE ' + CASE @ColumnType WHEN 'xml' THEN 'CAST(' + @ColumnName + ' AS nvarchar(MAX))'
            //                    WHEN 'timestamp' THEN 'master.dbo.fn_varbintohexstr(' + @ColumnName + ')'
            //                    ELSE @ColumnName END + ' LIKE ' + @QuotedSearchStrColumnValue
            //            INSERT INTO #Results
            //            EXEC(@sql)
            //            IF @@ROWCOUNT > 0 IF @FullRowResult = 1
            //            BEGIN
            //                SET @sql = 'SELECT TOP ' + CAST(@FullRowResultRows AS VARCHAR(3)) + ' ''' + @TableName + ''' AS [TableFound],''' + @ColumnName + ''' AS [ColumnFound],''FullRow>'' AS [FullRow>],*' +
            //                    ' FROM ' + @TableName + ' (NOLOCK) ' +
            //                    ' WHERE ' + CASE @ColumnType WHEN 'xml' THEN 'CAST(' + @ColumnName + ' AS nvarchar(MAX))'
            //                    WHEN 'timestamp' THEN 'master.dbo.fn_varbintohexstr(' + @ColumnName + ')'
            //                    ELSE @ColumnName END + ' LIKE ' + @QuotedSearchStrColumnValue
            //                EXEC(@sql)
            //            END
            //            DELETE FROM @ColumnNameTable WHERE COLUMN_NAME = @ColumnName
            //        END
            //    END
            //END
            //SET NOCOUNT OFF

            //SELECT TableName, ColumnName, ColumnValue, ColumnType, COUNT(*) AS Count FROM #Results
            //GROUP BY TableName, ColumnName, ColumnValue, ColumnType
            #endregion

            //tblDOCUMENT_STATS
            if (Name == "tblARCHIVE") SetRefID("tblDOCUMENT_STATS", "DocID", Name, "ID", ISN, on);
            if (Name == "tblFUND") SetRefID("tblDOCUMENT_STATS", "DocID", Name, "ID", ISN, on);
            if (Name == "tblINVENTORY") SetRefID("tblDOCUMENT_STATS", "DocID", Name, "ID", ISN, on);
            if (Name == "tblUNIT") SetRefID("tblDOCUMENT_STATS", "DocID", Name, "ID", ISN, on);
            if (Name == "tblDOCUMENT") SetRefID("tblDOCUMENT_STATS", "DocID", Name, "ID", ISN, on);

            ////tblARCHIVE_PASSPORT
            if (Name == "tblARCHIVE") SetRefID("tblARCHIVE_PASSPORT", "DocID", Name, "ID", ISN, on);
            ////tblARCHIVE_STATS
            if (Name == "tblARCHIVE_PASSPORT") SetRefID("tblARCHIVE_STATS", "DocID", Name, "ID", ISN, on);
            ////tblARCHIVE_STORAGE
            if (Name == "tblLOCATION") SetRefID("tblARCHIVE_STORAGE", "DocID", Name, "ID", ISN, on);
            ////tblConstantsSpec
            if (Name == "tblConstants") SetRefID("tblConstantsSpec", "DocID", Name, "ID", "CreationDateTime", on);//Не ISN
            ////tblDEPOSIT_DOC_TYPE
            if (Name == "tblDEPOSIT") SetRefID("tblDEPOSIT_DOC_TYPE", "DocID", Name, "ID", ISN, on);
            ////tblFUND_CHECK
            if (Name == "tblFUND") SetRefID("tblFUND_CHECK", "DocID", Name, "ID", ISN, on);
            ////tblFUND_COLLECTION_REASONS
            if (Name == "tblFUND") SetRefID("tblFUND_COLLECTION_REASONS", "DocID", Name, "ID", ISN, on);
            ////tblFUND_CREATOR
            if (Name == "tblFUND") SetRefID("tblFUND_CREATOR", "DocID", Name, "ID", ISN, on);
            ////tblFUND_DOC_TYPE
            if (Name == "tblFUND") SetRefID("tblFUND_DOC_TYPE", "DocID", Name, "ID", ISN, on);
            ////tblFUND_INCLUSION
            if (Name == "tblFUND") SetRefID("tblFUND_INCLUSION", "DocID", Name, "ID", ISN, on);
            ////tblFUND_OAF
            if (Name == "tblFUND") SetRefID("tblFUND_OAF", "DocID", Name, "ID", ISN, on);
            ////tblFUND_OAF_REASON
            if (Name == "tblFUND") SetRefID("tblFUND_OAF_REASON", "DocID", Name, "ID", ISN, on);
            ////tblFUND_PAPER_CLS
            if (Name == "tblFUND") SetRefID("tblFUND_PAPER_CLS", "DocID", Name, "ID", ISN, on);
            ////tblFUND_PUBLICATION
            if (Name == "tblFUND") SetRefID("tblFUND_PUBLICATION", "DocID", Name, "ID", ISN, on);
            ////tblFUND_RECEIPT_REASON
            if (Name == "tblFUND") SetRefID("tblFUND_RECEIPT_REASON", "DocID", Name, "ID", ISN, on);
            ////tblFUND_RECEIPT_SOURCE
            if (Name == "tblFUND") SetRefID("tblFUND_RECEIPT_SOURCE", "DocID", Name, "ID", ISN, on);
            ////tblFUND_RENAME
            if (Name == "tblFUND") SetRefID("tblFUND_RENAME", "DocID", Name, "ID", ISN, on);
            ////tblINVENTORY_CHECK
            if (Name == "tblINVENTORY") SetRefID("tblINVENTORY_CHECK", "DocID", Name, "ID", ISN, on);
            ////tblINVENTORY_CLS_ATTR
            if (Name == "tblINVENTORY") SetRefID("tblINVENTORY_CLS_ATTR", "DocID", Name, "ID", ISN, on);
            ////tblINVENTORY_DOC_STORAGE
            if (Name == "tblINVENTORY") SetRefID("tblINVENTORY_DOC_STORAGE", "DocID", Name, "ID", ISN, on);
            ////tblINVENTORY_DOC_TYPE
            if (Name == "tblINVENTORY") SetRefID("tblINVENTORY_DOC_TYPE", "DocID", Name, "ID", ISN, on);
            ////tblINVENTORY_GROUPING_ATTRIBUTE
            if (Name == "tblINVENTORY") SetRefID("tblINVENTORY_GROUPING_ATTRIBUTE", "DocID", Name, "ID", ISN, on);
            ////tblINVENTORY_PAPER_CLS
            if (Name == "tblINVENTORY") SetRefID("tblINVENTORY_PAPER_CLS", "DocID", Name, "ID", ISN, on);
            ////tblINVENTORY_REQUIRED_WORK
            if (Name == "tblINVENTORY") SetRefID("tblINVENTORY_REQUIRED_WORK", "DocID", Name, "ID", ISN, on);
            ////tblINVENTORY_STRUCTURE
            if (Name == "tblINVENTORY") SetRefID("tblINVENTORY_STRUCTURE", "DocID", Name, "ID", ISN, on);
            ////tblINVENTORY_STRUCTURE2
            if (Name == "tblINVENTORY") SetRefID("tblINVENTORY_STRUCTURE", "ID", Name, "ID", ISN, on);
            ////tblINVENTORY_STRUCTURE3
            if (Name == "tblINVENTORY") SetRefID("tblINVENTORY_STRUCTURE", ISN, Name, ISN, "ID", on);//gg
            ////tblORGANIZ_RENAME
            if (Name == "tblORGANIZ_CL") SetRefID("tblORGANIZ_RENAME", "DocID", Name, "ID", ISN, on);
            ////tblREF_ACT
            if (Name == "tblARCHIVE") SetRefID("tblREF_ACT", "DocID", Name, "ID", ISN, on);
            if (Name == "tblFUND") SetRefID("tblREF_ACT", "DocID", Name, "ID", ISN, on);
            if (Name == "tblINVENTORY") SetRefID("tblREF_ACT", "DocID", Name, "ID", ISN, on);
            if (Name == "tblUNIT") SetRefID("tblREF_ACT", "DocID", Name, "ID", ISN, on);
            if (Name == "tblDOCUMENT") SetRefID("tblREF_ACT", "DocID", Name, "ID", ISN, on);

            if (Name == "tblARCHIVE") SetRefID("tblREF_ACT", "ISN_OBJ", Name, ISN, "ID", on);
            if (Name == "tblFUND") SetRefID("tblREF_ACT", "ISN_OBJ", Name, ISN, "ID", on);
            if (Name == "tblINVENTORY") SetRefID("tblREF_ACT", "ISN_OBJ", Name, ISN, "ID", on);
            if (Name == "tblUNIT") SetRefID("tblREF_ACT", "ISN_OBJ", Name, ISN, "ID", on);
            if (Name == "tblDOCUMENT") SetRefID("tblREF_ACT", "ISN_OBJ", Name, ISN, "ID", on);
            ////tblREF_CLS
            if (Name == "tblARCHIVE") SetRefID("tblREF_CLS", "DocID", Name, "ID", ISN, on);
            if (Name == "tblFUND") SetRefID("tblREF_CLS", "DocID", Name, "ID", ISN, on);
            if (Name == "tblINVENTORY") SetRefID("tblREF_CLS", "DocID", Name, "ID", ISN, on);
            if (Name == "tblUNIT") SetRefID("tblREF_CLS", "DocID", Name, "ID", ISN, on);
            if (Name == "tblDOCUMENT") SetRefID("tblREF_CLS", "DocID", Name, "ID", ISN, on);

            if (Name == "tblARCHIVE") SetRefID("tblREF_CLS", "ISN_OBJ", Name, ISN, "ID", on);
            if (Name == "tblFUND") SetRefID("tblREF_CLS", "ISN_OBJ", Name, ISN, "ID", on);
            if (Name == "tblINVENTORY") SetRefID("tblREF_CLS", "ISN_OBJ", Name, ISN, "ID", on);
            if (Name == "tblUNIT") SetRefID("tblREF_CLS", "ISN_OBJ", Name, ISN, "ID", on);
            if (Name == "tblDOCUMENT") SetRefID("tblREF_CLS", "ISN_OBJ", Name, ISN, "ID", on);
            ////tblREF_FEATURE
            if (Name == "tblARCHIVE") SetRefID("tblREF_FEATURE", "DocID", Name, "ID", ISN, on);
            if (Name == "tblFUND") SetRefID("tblREF_FEATURE", "DocID", Name, "ID", ISN, on);
            if (Name == "tblINVENTORY") SetRefID("tblREF_FEATURE", "DocID", Name, "ID", ISN, on);
            if (Name == "tblUNIT") SetRefID("tblREF_FEATURE", "DocID", Name, "ID", ISN, on);
            if (Name == "tblDOCUMENT") SetRefID("tblREF_FEATURE", "DocID", Name, "ID", ISN, on);

            if (Name == "tblARCHIVE") SetRefID("tblREF_FEATURE", "ISN_OBJ", Name, ISN, "ID", on);
            if (Name == "tblFUND") SetRefID("tblREF_FEATURE", "ISN_OBJ", Name, ISN, "ID", on);
            if (Name == "tblINVENTORY") SetRefID("tblREF_FEATURE", "ISN_OBJ", Name, ISN, "ID", on);
            if (Name == "tblUNIT") SetRefID("tblREF_FEATURE", "ISN_OBJ", Name, ISN, "ID", on);
            if (Name == "tblDOCUMENT") SetRefID("tblREF_FEATURE", "ISN_OBJ", Name, ISN, "ID", on);
            ////tblREF_FILE
            if (Name == "tblARCHIVE") SetRefID("tblREF_FILE", "DocID", Name, "ID", ISN, on);
            if (Name == "tblFUND") SetRefID("tblREF_FILE", "DocID", Name, "ID", ISN, on);
            if (Name == "tblINVENTORY") SetRefID("tblREF_FILE", "DocID", Name, "ID", ISN, on);
            if (Name == "tblUNIT") SetRefID("tblREF_FILE", "DocID", Name, "ID", ISN, on);
            if (Name == "tblDOCUMENT") SetRefID("tblREF_FILE", "DocID", Name, "ID", ISN, on);

            if (Name == "tblARCHIVE") SetRefID("tblREF_FILE", "ISN_OBJ", Name, ISN, "ID", on);
            if (Name == "tblFUND") SetRefID("tblREF_FILE", "ISN_OBJ", Name, ISN, "ID", on);
            if (Name == "tblINVENTORY") SetRefID("tblREF_FILE", "ISN_OBJ", Name, ISN, "ID", on);
            if (Name == "tblUNIT") SetRefID("tblREF_FILE", "ISN_OBJ", Name, ISN, "ID", on);
            if (Name == "tblDOCUMENT") SetRefID("tblREF_FILE", "ISN_OBJ", Name, ISN, "ID", on);
            ////tblREF_LANGUAGE
            if (Name == "tblARCHIVE") SetRefID("tblREF_LANGUAGE", "DocID", Name, "ID", ISN, on);
            if (Name == "tblFUND") SetRefID("tblREF_LANGUAGE", "DocID", Name, "ID", ISN, on);
            if (Name == "tblINVENTORY") SetRefID("tblREF_LANGUAGE", "DocID", Name, "ID", ISN, on);
            if (Name == "tblUNIT") SetRefID("tblREF_LANGUAGE", "DocID", Name, "ID", ISN, on);
            if (Name == "tblDOCUMENT") SetRefID("tblREF_LANGUAGE", "DocID", Name, "ID", ISN, on);

            if (Name == "tblARCHIVE") SetRefID("tblREF_LANGUAGE", "ISN_OBJ", Name, ISN, "ID", on);
            if (Name == "tblFUND") SetRefID("tblREF_LANGUAGE", "ISN_OBJ", Name, ISN, "ID", on);
            if (Name == "tblINVENTORY") SetRefID("tblREF_LANGUAGE", "ISN_OBJ", Name, ISN, "ID", on);
            if (Name == "tblUNIT") SetRefID("tblREF_LANGUAGE", "ISN_OBJ", Name, ISN, "ID", on);
            if (Name == "tblDOCUMENT") SetRefID("tblREF_LANGUAGE", "ISN_OBJ", Name, ISN, "ID", on);
            ////tblREF_LOCATION
            if (Name == "tblARCHIVE") SetRefID("tblREF_LOCATION", "DocID", Name, "ID", ISN, on);
            if (Name == "tblFUND") SetRefID("tblREF_LOCATION", "DocID", Name, "ID", ISN, on);
            if (Name == "tblINVENTORY") SetRefID("tblREF_LOCATION", "DocID", Name, "ID", ISN, on);
            if (Name == "tblUNIT") SetRefID("tblREF_LOCATION", "DocID", Name, "ID", ISN, on);
            if (Name == "tblDOCUMENT") SetRefID("tblREF_LOCATION", "DocID", Name, "ID", ISN, on);

            if (Name == "tblARCHIVE") SetRefID("tblREF_LOCATION", "ISN_OBJ", Name, ISN, "ID", on);
            if (Name == "tblFUND") SetRefID("tblREF_LOCATION", "ISN_OBJ", Name, ISN, "ID", on);
            if (Name == "tblINVENTORY") SetRefID("tblREF_LOCATION", "ISN_OBJ", Name, ISN, "ID", on);
            if (Name == "tblUNIT") SetRefID("tblREF_LOCATION", "ISN_OBJ", Name, ISN, "ID", on);
            if (Name == "tblDOCUMENT") SetRefID("tblREF_LOCATION", "ISN_OBJ", Name, ISN, "ID", on);
            ////tblREF_QUESTION
            if (Name == "tblARCHIVE") SetRefID("tblREF_QUESTION", "DocID", Name, "ID", ISN, on);
            if (Name == "tblFUND") SetRefID("tblREF_QUESTION", "DocID", Name, "ID", ISN, on);
            if (Name == "tblINVENTORY") SetRefID("tblREF_QUESTION", "DocID", Name, "ID", ISN, on);
            if (Name == "tblUNIT") SetRefID("tblREF_QUESTION", "DocID", Name, "ID", ISN, on);
            if (Name == "tblDOCUMENT") SetRefID("tblREF_QUESTION", "DocID", Name, "ID", ISN, on);

            if (Name == "tblARCHIVE") SetRefID("tblREF_QUESTION", "ISN_OBJ", Name, ISN, "ID", on);
            if (Name == "tblFUND") SetRefID("tblREF_QUESTION", "ISN_OBJ", Name, ISN, "ID", on);
            if (Name == "tblINVENTORY") SetRefID("tblREF_QUESTION", "ISN_OBJ", Name, ISN, "ID", on);
            if (Name == "tblUNIT") SetRefID("tblREF_QUESTION", "ISN_OBJ", Name, ISN, "ID", on);
            if (Name == "tblDOCUMENT") SetRefID("tblREF_QUESTION", "ISN_OBJ", Name, ISN, "ID", on);
            ////tblServiceLog
            if (Name == "tblARCHIVE") SetRefID("tblServiceLog", "DocID", Name, "ID", ISN, on);
            if (Name == "tblFUND") SetRefID("tblServiceLog", "DocID", Name, "ID", ISN, on);
            if (Name == "tblINVENTORY") SetRefID("tblServiceLog", "DocID", Name, "ID", ISN, on);
            if (Name == "tblUNIT") SetRefID("tblServiceLog", "DocID", Name, "ID", ISN, on);
            if (Name == "tblDOCUMENT") SetRefID("tblServiceLog", "DocID", Name, "ID", ISN, on);
            ////tblUNDOCUMENTED_PERIOD
            if (Name == "tblFUND") SetRefID("tblUNDOCUMENTED_PERIOD", "DocID", Name, "ID", ISN, on);
            ////tblUNIT_ELECTRONIC
            if (Name == "tblUNIT") SetRefID("tblUNIT_ELECTRONIC", "DocID", Name, "ID", ISN, on);
            ////tblUNIT_FOTO
            if (Name == "tblUNIT") SetRefID("tblUNIT_FOTO", "DocID", Name, "ID", ISN, on);
            ////tblUNIT_FOTO_EX
            if (Name == "tblUNIT") SetRefID("tblUNIT_FOTO_EX", "DocID", Name, "ID", ISN, on);
            ////tblUNIT_MICROFORM
            if (Name == "tblUNIT") SetRefID("tblUNIT_MICROFORM", "DocID", Name, "ID", ISN, on);
            ////tblUNIT_MOVIE
            if (Name == "tblUNIT") SetRefID("tblUNIT_MOVIE", "DocID", Name, "ID", ISN, on);
            ////tblUNIT_MOVIE_EX
            if (Name == "tblUNIT") SetRefID("tblUNIT_MOVIE_EX", "DocID", Name, "ID", ISN, on);
            ////tblUNIT_NTD
            if (Name == "tblUNIT") SetRefID("tblUNIT_NTD", "DocID", Name, "ID", ISN, on);
            ////tblUNIT_PHONO
            if (Name == "tblUNIT") SetRefID("tblUNIT_PHONO", "DocID", Name, "ID", ISN, on);
            ////tblUNIT_REQUIRED_WORK
            if (Name == "tblUNIT") SetRefID("tblUNIT_REQUIRED_WORK", "DocID", Name, "ID", ISN, on);
            ////tblUNIT_STATE
            if (Name == "tblUNIT") SetRefID("tblUNIT_STATE", "DocID", Name, "ID", ISN, on);
            ////tblUNIT_VIDEO
            if (Name == "tblUNIT") SetRefID("tblUNIT_VIDEO", "DocID", Name, "ID", ISN, on);
            ////tblUNIT_VIDEO_EX
            if (Name == "tblUNIT") SetRefID("tblUNIT_VIDEO_EX", "DocID", Name, "ID", ISN, on);
            ////tblUNIT_WORK
            if (Name == "tblUNIT") SetRefID("tblUNIT_WORK", "DocID", Name, "ID", ISN, on);

        }

        void SetRefID(string dtable, string dcolumn, string table, string column, string field, bool on)
        {
            string name = $"TR_{dtable}_{dcolumn}_{table}_{column}";

            string type;
            if (dcolumn.StartsWith("ISN")) type = "bigint";
            else type = "uniqueidentifier";
            #region help SQL
            //select f.ID, ds.DocID , ds.ID from tableName as f
            //left join tblDOCUMENT_STATS as ds on ds.DocID = f.ID
            #endregion
            #region in SQL
            //create trigger TR_tblDOCUMENT_STATS_DocID_tblFUND_ID on tblFUND after update as
            //if update(ID) begin
            //create table #temp(ID1 uniqueidentifier, ID1_NEW uniqueidentifier, ID2 uniqueidentifier)
            //insert into #temp 
            //select d.ID, i.ID , ds.ID from deleted as d
            //left join tblDOCUMENT_STATS as ds on ds.DocID = d.ID
            //left join inserted as i on i.ISN_FUND = d.ISN_FUND
            //update tblDOCUMENT_STATS set DocID = ID1_NEW from #temp where tblDOCUMENT_STATS.ID = #temp.ID2 
            //drop table #temp 
            //end
            #endregion
            if (on)
            {
                Log($"Create {name}...");
                exec($"use {Database}",
                     $"create trigger {name} on {table} after update as " +
                     $"if update({column}) begin " +
                     $"create table #temp(ID1 {type}, ID1_NEW {type}, ID2 uniqueidentifier) " +
                     $"insert into #temp " +
                     $"select d.{column}, i.{column} , ds.ID from deleted as d " +
                     $"left join {dtable} as ds on ds.{dcolumn} = d.{column} " +
                     $"left join inserted as i on i.{field} = d.{field} " +
                     $"update {dtable} set {dcolumn} = ID1_NEW from #temp where {dtable}.ID = #temp.ID2 " +
                     $"drop table #temp " +
                     $"end",
                     $"use master");
            }
            else
            {
                Log($"Delete {name}...");
                exec($"use {Database}", $"drop trigger {name}", "use master");
            }
        }

        string GetCorrectISN()
        {
            if (Name.EndsWith("_CL"))
                return "ISN_" + Name.Replace("tbl", "").Replace("_CL", "").Replace("INV_", "").Replace("DECL_", "");
            else if (Name == "tblFILE_CONTENTS2") return "ISN_REF_FILE";
            else if (Name == "tblFUND_INCLUSION") return "ISN_INCLUSION";
            else if (Name == "tblFUND_OAF") return "ISN_OAF";
            else if (Name == "tblARCHIVE_PASSPORT") return "ISN_PASSPORT";
            else if (Name == "tblINVENTORY_STRUCTURE") return "ISN_INVENTORY_CLS";
            else if (Name == "tblTREE_SUPPORT") return "ISN";
            else if (Name == "tblDOCUMENT") return "ISN_DOCUM";
            else return "ISN_" + Name.Replace("tbl", "");
        }


        bool exec(string command1, string command2 = null, string command3 = null, bool log = true)
        {
            try
            {
                SqlCommand cmd = new SqlCommand(command1, Connection);
                cmd.CommandTimeout = 1000;
                cmd.ExecuteNonQuery();
                if (command2 != null)
                {
                    SqlCommand cmd2 = new SqlCommand(command2, Connection);
                    cmd2.CommandTimeout = 1000;
                    cmd2.ExecuteNonQuery();
                }
                if (command3 != null)
                {
                    SqlCommand cmd3 = new SqlCommand(command3, Connection);
                    cmd3.CommandTimeout = 1000;
                    cmd3.ExecuteNonQuery();
                }
                if (log) Log("Done.\r\n");
                return true;
            }
            catch (Exception ex)
            {
                if (log) Log($"Error.\r\nError: {ex.Message}\r\n");
                return false;
            }

        }
        object execScalar(string command)
        {
            object val = null;
            try
            {
                SqlCommand cmd = new SqlCommand(command, Connection);
                val = cmd.ExecuteScalar();
            }
            catch (Exception ex)
            {
                Log($"Error: {ex.Message}");
            }
            return val;
        }
        void Log(string logText)
        {
            log(this, new LabelEditEventArgs(Convert.ToInt32(true), logText));
        }

    }
}
