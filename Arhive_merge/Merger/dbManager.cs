﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Threading.Tasks;
using System.IO;

namespace Merger
{
    public class dbManager
    {
        public event EventHandler<LabelEditEventArgs> log;

        public SqlConnection Connection { get; private set; }
        public dbManager(string serverName, string login, string password, EventHandler<LabelEditEventArgs> logUpperFunc)
        {
            log += logUpperFunc;
            ConnectTo(serverName, login, password);
        }
        public dbManager(EventHandler<LabelEditEventArgs> logFunc)
        {
            log += logFunc;
            Connection = new SqlConnection();
        }



        public bool ConnectTo(string serverName, string login, string password)
        {
            string connectionString = $"Server={serverName};User ID={login};Password={password};Connect Timeout=3;";
            Connection = new SqlConnection(connectionString);
            Log($"Connecting to {serverName}...");
            try
            {
                Connection.Open();
                Log("Done.\r\nStatus: connected\r\n");
            }
            catch (Exception ex)
            {
                Log("Error.\r\n");
                Log(ex.Message);
                Log("\r\nStatus: disconnected\r\n");
            }
            return Connection.State == ConnectionState.Open;
        }
        public List<string> GetDBNames()
        {
            List<string> dbList = new List<string>();
            if (Connection.State == ConnectionState.Open)
            {
                SqlCommand cmd = new SqlCommand("select name FROM sys.databases;", Connection);
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataSet set = new DataSet();
                adapter.Fill(set);
                foreach (DataRow r in set.Tables[0].Rows)
                    dbList.Add($"[{r.ItemArray.FirstOrDefault().ToString()}]");

                dbList.RemoveAll(name => name == "[master]" || name == "[model]" || name == "[msdb]" || name == "[tempdb]");
                dbList.Sort();
            }
            return dbList;
        }
        public void FillComboBoxes(ComboBox[] comboBoxes)
        {
            try
            {
                Random rnd = new Random();
                foreach (ComboBox c in comboBoxes)
                {
                    c.Items.Clear();
                    c.Items.AddRange(GetDBNames().ToArray());
                    if (c.Items.Count > 0) c.SelectedItem = c.Items[0];//rnd.Next(0, c.Items.Count)
                    else Log($"Error.\r\nError: Databases is missing\r\n");
                }

            }
            catch (Exception ex)
            {
                Log(ex.Message);

            }
        }
        public void Disconnect()
        {
            if (Connection != null) Connection.Close();
        }




        #region old tables (not all)
        //tables_db1.Add("dbo.tblARCHIVE");
        //tables_db1.Add("dbo.tblFUND");
        //tables_db1.Add("dbo.tblINVENTORY");
        //tables_db1.Add("dbo.tblUNIT");
        //tables_db1.Add("dbo.tblDOCUMENT");
        //tables_db1.Add("dbo.tblDOCUMENT_STATS");
        //tables_db1.Add("dbo.tblACT_TYPE_CL");
        //tables_db1.Add("dbo.tblACT");
        //tables_db1.Add("dbo.tblDEPOSIT_DOC_TYPE");
        //tables_db1.Add("dbo.tblORGANIZ_CL");
        //tables_db1.Add("dbo.tblTREE_SUPPORT");
        //tables_db1.Add("dbo.tblWORK_CL");
        //tables_db1.Add("dbo.tblUNIT_WORK");
        //tables_db1.Add("dbo.tblLOCATION");
        //tables_db1.Add("dbo.tblARCHIVE_STORAGE");
        //tables_db1.Add("dbo.tblINVENTORY_STRUCTURE");
        //tables_db1.Add("dbo.tblDOC_KIND_CL");
        //tables_db1.Add("dbo.tblUNIT_VIDEO");
        //tables_db1.Add("dbo.tblARCHIVE_PASSPORT");
        //tables_db1.Add("dbo.tblDOC_TYPE_CL");
        //tables_db1.Add("dbo.tblARCHIVE_STATS");
        //tables_db1.Add("dbo.tblServiceLog");
        //tables_db1.Add("dbo.tblSTATE_CL");
        //tables_db1.Add("dbo.tblUNIT_STATE");
        //tables_db1.Add("dbo.tblUNIT_REQUIRED_WORK");
        //tables_db1.Add("dbo.tblFUND_RENAME");
        //tables_db1.Add("dbo.tblUNIT_PHONO");
        //tables_db1.Add("dbo.tblREF_QUESTION");
        //tables_db1.Add("dbo.tblCLS");
        //tables_db1.Add("dbo.tblREF_CLS");
        //tables_db1.Add("dbo.tblQUESTION");
        //tables_db1.Add("dbo.tblREF_FILE");
        //tables_db1.Add("dbo.tblRECEIPT_SOURCE_CL");
        //tables_db1.Add("dbo.tblFUND_RECEIPT_SOURCE");
        //tables_db1.Add("dbo.tblUNIT_FOTO_EX");
        //tables_db1.Add("dbo.tblUNIT_NTD");
        //tables_db1.Add("dbo.tblRECEIPT_REASON_CL");
        //tables_db1.Add("dbo.tblFUND_RECEIPT_REASON");
        //tables_db1.Add("dbo.tblUNIT_MOVIE_EX");
        //tables_db1.Add("dbo.tblUNIT_VIDEO_EX");
        //tables_db1.Add("dbo.tblPUBLICATION_CL");
        //tables_db1.Add("dbo.tblFUND_PUBLICATION");
        //tables_db1.Add("dbo.tblPAPER_CLS");
        //tables_db1.Add("dbo.tblFUND_PAPER_CLS");
        //tables_db1.Add("dbo.tblOAF_REASON_CL");
        //tables_db1.Add("dbo.tblFUND_OAF_REASON");
        //tables_db1.Add("dbo.tblUNIT_MOVIE");
        //tables_db1.Add("dbo.tblFUND_OAF");
        //tables_db1.Add("dbo.tblCITIZEN_CL");
        //tables_db1.Add("dbo.tblFUND_INCLUSION");
        //tables_db1.Add("dbo.tblFUND_DOC_TYPE");
        //tables_db1.Add("dbo.tblUNIT_MICROFORM");
        //tables_db1.Add("dbo.tblFUND_CREATOR");
        //tables_db1.Add("dbo.tblCOLLECTION_REASON_CL");
        //tables_db1.Add("dbo.tblFUND_COLLECTION_REASONS");
        //tables_db1.Add("dbo.tblFUND_CHECK");
        //tables_db1.Add("dbo.tblORGANIZ_RENAME");
        //tables_db1.Add("dbo.tblREF_LOCATION");
        //tables_db1.Add("dbo.tblUNIT_FOTO");
        //tables_db1.Add("dbo.tblLANGUAGE_CL");
        //tables_db1.Add("dbo.tblREF_LANGUAGE");
        //tables_db1.Add("dbo.tblFEATURE");
        //tables_db1.Add("dbo.tblREF_FEATURE");
        //tables_db1.Add("dbo.tblUNIT_ELECTRONIC");
        //tables_db1.Add("dbo.rptLOCATION");
        //tables_db1.Add("dbo.tblDEPOSIT");
        //tables_db1.Add("dbo.rptGuide");
        //tables_db1.Add("dbo.rptFUND_UNIT_REG_STATS");
        //tables_db1.Add("dbo.rptFUND_PAPER");
        //tables_db1.Add("dbo.rptFUND_GUIDE");
        //tables_db1.Add("dbo.rptCLS_STRUCTURE");
        //tables_db1.Add("dbo.tblDECL_COMMISSION_CL");
        //tables_db1.Add("dbo.tblABSENCE_REASON_CL");
        //tables_db1.Add("dbo.tblUNDOCUMENTED_PERIOD");
        //tables_db1.Add("dbo.tblAuthorizedDep");
        //tables_db1.Add("dbo.tblREF_ACT");
        //tables_db1.Add("dbo.tblINV_REQUIRED_WORK_CL");
        //tables_db1.Add("dbo.tblINVENTORY_REQUIRED_WORK");
        //tables_db1.Add("dbo.tblPAPER_CLS_INV");
        //tables_db1.Add("dbo.tblINVENTORY_PAPER_CLS");
        //tables_db1.Add("dbo.tblGROUPING_ATTRIBUTE_CL");
        //tables_db1.Add("dbo.tblINVENTORY_GROUPING_ATTRIBUTE");
        //tables_db1.Add("dbo.tblINVENTORY_DOC_TYPE");
        //tables_db1.Add("dbo.tblSTORAGE_MEDIUM_CL");
        //tables_db1.Add("dbo.tblINVENTORY_DOC_STORAGE");
        //tables_db1.Add("dbo.tblINVENTORY_CLS_ATTR");
        //tables_db1.Add("dbo.tblFILE_CONTENTS2");
        //tables_db1.Add("dbo.tblINVENTORY_CHECK");
        //tables_db1.Add("dbo.tblSUBJECT_CL");
        //tables_db1.Add("dbo.tblPERIOD");
        //tables_db1.Add("dbo.tblSECURLEVEL");
        //tables_db1.Add("dbo.tblSECURITY_REASON"); //эту таблицу не объединяем потому что она с identity
        //tables_db1.Add("dbo.tblREPRODUCTION_METHOD_CL");
        #endregion

        //хвала этой функции !
        public void Merge(string db1, string db2, string new_db)
        {
            if (Connection.State == ConnectionState.Open)
            {
                CreateNewDB(new_db);

                //Initialization
                List<string> allTables = exec1($"use {db1} " +
                    $"SELECT 'dbo.' + TABLE_NAME FROM INFORMATION_SCHEMA.TABLES where TABLE_TYPE = 'BASE TABLE' use master", false);

                Functions functions_db1 = new Functions(db1, Connection, log);
                Functions functions_newdb = new Functions(functions_db1, new_db, log);

                Views views_db1 = new Views(db1, Connection, log);
                Views views_newdb = new Views(views_db1, new_db, log);

                ExtendedProperties exProps_db1 = new ExtendedProperties(db1, Connection, log);
                ExtendedProperties exProps_newdb = new ExtendedProperties(exProps_db1, new_db, log);

                Tables tables_db1 = new Tables(db1, Connection, log);
                foreach (var t in allTables) tables_db1.Add(t);
                tables_db1.IdentityOff();
                

                Tables tables_newdb = new Tables(tables_db1, new_db, log);

                Tables tables_db2 = new Tables(tables_db1, db2, log);
                tables_db2.IdentityOff();



                //Copy tables of db1 to new_db
                foreach (var t in tables_db1)
                {
                    t.CopyTo(new_db);
                    tables_db2[t.Name].FirstISN = t.LastISN;
                }



                //Except eq*
                tables_db2.Except("eq*");
                //Except tblARCHIVE
                tables_db2.Except("tblARCHIVE");


                //Equals
                tables_db2.Except("tblConstants");
                tables_db2.Except("tblConstantsSpec");
                tables_db2.Except("tblAuthorizedDep");
                tables_db2.Except("tblDataExport");
                tables_db2.Except("tblDECL_COMMISSION_CL");
                tables_db2.Except("tblFILE_CONTENTS2");
                tables_db2.Except("tblINVENTORY_CLS_ATTR");
                tables_db2.Except("tblQUESTION");
                tables_db2.Except("tblREF_QUESTION");
                tables_db2.Except("tblService");
                tables_db2.Except("tblServiceLog");
                tables_db2.Except("tblTREE_SUPPORT");
                tables_db2.Except("tblUNIT_ELECTRONIC");
                tables_db2.Except("tblUNIT_FOTO_EX");
                tables_db2.Except("tblUNIT_MOVIE");
                tables_db2.Except("tblUNIT_MOVIE_EX");
                tables_db2.Except("tblUNIT_PHONO");
                tables_db2.Except("tblUNIT_VIDEO");
                tables_db2.Except("tblUNIT_VIDEO_EX");
                tables_db2.Except("tblUNIT_WORK");
                //Other
                //tables_db2.Except("");
                //tables_db2.Except("");
                //tables_db2.Except("");
                //tables_db2.Except("");
                //tables_db2.Except("");
                //tables_db2.Except("");
                //tables_db2.Except("");


                //Changing db2
                foreach (Table t in tables_db2)
                {
                    t.SetCascade(true);
                    t.ChangeID();
                    t.ChangeISN();
                    t.SetCascade(false);
                }



                //Union
                foreach (var t in tables_db2)
                {
                    Log($"UNION {t.Name}...");
                    exec($"insert into {tables_newdb[t.Name].Database}.{tables_newdb[t.Name].FullName} select * from {t.Database}.{t.FullName}");
                }

                #region find by value in db
                //DECLARE @SearchStrTableName nvarchar(255), @SearchStrColumnName nvarchar(255), @SearchStrColumnValue nvarchar(255), @SearchStrInXML bit, @FullRowResult bit, @FullRowResultRows int
                //SET @SearchStrColumnValue = '%value%' /* use LIKE syntax */
                //SET @FullRowResult = 1
                //SET @FullRowResultRows = 3
                //SET @SearchStrTableName = NULL /* NULL for all tables, uses LIKE syntax */
                //SET @SearchStrColumnName = NULL /* NULL for all columns, uses LIKE syntax */
                //SET @SearchStrInXML = 0 /* Searching XML data may be slow */

                //IF OBJECT_ID('tempdb..#Results') IS NOT NULL DROP TABLE #Results
                //CREATE TABLE #Results (TableName nvarchar(128), ColumnName nvarchar(128), ColumnValue nvarchar(max),ColumnType nvarchar(20))

                //SET NOCOUNT ON

                //DECLARE @TableName nvarchar(256) = '',@ColumnName nvarchar(128),@ColumnType nvarchar(20), @QuotedSearchStrColumnValue nvarchar(110), @QuotedSearchStrColumnName nvarchar(110)
                //SET @QuotedSearchStrColumnValue = QUOTENAME(@SearchStrColumnValue, '''')
                //DECLARE @ColumnNameTable TABLE(COLUMN_NAME nvarchar(128), DATA_TYPE nvarchar(20))

                //WHILE @TableName IS NOT NULL
                //BEGIN
                //    SET @TableName =
                //    (
                //        SELECT MIN(QUOTENAME(TABLE_SCHEMA) + '.' + QUOTENAME(TABLE_NAME))
                //        FROM INFORMATION_SCHEMA.TABLES
                //     WHERE       TABLE_TYPE = 'BASE TABLE'
                //            AND TABLE_NAME LIKE COALESCE(@SearchStrTableName, TABLE_NAME)
                //            AND QUOTENAME(TABLE_SCHEMA) +'.' + QUOTENAME(TABLE_NAME) > @TableName
                //            AND OBJECTPROPERTY(OBJECT_ID(QUOTENAME(TABLE_SCHEMA) +'.' + QUOTENAME(TABLE_NAME)), 'IsMSShipped') = 0
                //    )
                //    IF @TableName IS NOT NULL
                //    BEGIN
                //        DECLARE @sql VARCHAR(MAX)
                //        SET @sql = 'SELECT QUOTENAME(COLUMN_NAME),DATA_TYPE
                //                FROM INFORMATION_SCHEMA.COLUMNS
                //             WHERE       TABLE_SCHEMA = PARSENAME(''' + @TableName + ''', 2)
                //                AND TABLE_NAME = PARSENAME(''' + @TableName + ''', 1)
                //                AND DATA_TYPE IN(' + CASE WHEN ISNUMERIC(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@SearchStrColumnValue,' % ',''),'_',''),'[',''),']',''),' - ','')) = 1 THEN '''tinyint'', ''int'', ''smallint'', ''bigint'', ''numeric'', ''decimal'', ''smallmoney'', ''money'', ' ELSE '' END + '''char'', ''varchar'', ''nchar'', ''nvarchar'', ''timestamp'', ''uniqueidentifier''' + CASE @SearchStrInXML WHEN 1 THEN ', ''xml''' ELSE '' END + ')
                //                AND COLUMN_NAME LIKE COALESCE(' + CASE WHEN @SearchStrColumnName IS NULL THEN 'NULL' ELSE '''' + @SearchStrColumnName + '''' END  + ',COLUMN_NAME)'
                //        INSERT INTO @ColumnNameTable
                //        EXEC(@sql)
                //        WHILE EXISTS(SELECT TOP 1 COLUMN_NAME FROM @ColumnNameTable)
                //        BEGIN
                //            PRINT @ColumnName
                //            SELECT TOP 1 @ColumnName = COLUMN_NAME,@ColumnType = DATA_TYPE FROM @ColumnNameTable
                //            SET @sql = 'SELECT ''' + @TableName + ''',''' + @ColumnName + ''',' + CASE @ColumnType WHEN 'xml' THEN 'LEFT(CAST(' + @ColumnName + ' AS nvarchar(MAX)), 4096),'''
                //            WHEN 'timestamp' THEN 'master.dbo.fn_varbintohexstr(' + @ColumnName + '),'''
                //            ELSE 'LEFT(' + @ColumnName + ', 4096),''' END + @ColumnType + ''' 
                //                    FROM ' + @TableName + '(NOLOCK) ' +
                //                    ' WHERE ' + CASE @ColumnType WHEN 'xml' THEN 'CAST(' + @ColumnName + ' AS nvarchar(MAX))'
                //                    WHEN 'timestamp' THEN 'master.dbo.fn_varbintohexstr(' + @ColumnName + ')'
                //                    ELSE @ColumnName END + ' LIKE ' + @QuotedSearchStrColumnValue
                //            INSERT INTO #Results
                //            EXEC(@sql)
                //            IF @@ROWCOUNT > 0 IF @FullRowResult = 1
                //            BEGIN
                //                SET @sql = 'SELECT TOP ' + CAST(@FullRowResultRows AS VARCHAR(3)) + ' ''' + @TableName + ''' AS [TableFound],''' + @ColumnName + ''' AS [ColumnFound],''FullRow>'' AS [FullRow>],*' +
                //                    ' FROM ' + @TableName + ' (NOLOCK) ' +
                //                    ' WHERE ' + CASE @ColumnType WHEN 'xml' THEN 'CAST(' + @ColumnName + ' AS nvarchar(MAX))'
                //                    WHEN 'timestamp' THEN 'master.dbo.fn_varbintohexstr(' + @ColumnName + ')'
                //                    ELSE @ColumnName END + ' LIKE ' + @QuotedSearchStrColumnValue
                //                EXEC(@sql)
                //            END
                //            DELETE FROM @ColumnNameTable WHERE COLUMN_NAME = @ColumnName
                //        END
                //    END
                //END
                //SET NOCOUNT OFF

                //SELECT TableName, ColumnName, ColumnValue, ColumnType, COUNT(*) AS Count FROM #Results
                //GROUP BY TableName, ColumnName, ColumnValue, ColumnType
                #endregion

                //Creating constraints, views, funtions, extended properties and etc...
                tables_newdb.DefaultValues = tables_db1.GetDefaultValues();
                tables_newdb.SetDefaultValues();
                tables_newdb.CreateConstraints();
                //tables_newdb["eqDocStatesLOG"].SetDefault("EventTime", "datetime", "(getdate())");
                functions_newdb.Create();
                views_newdb.Create();
                exProps_newdb.Create();



                Log($"Merge {db2}->{db1} completed.");
                Log($"New database is {new_db}.\r\n");
            }

        }
        public async void MergeAsync(string db1, string db2, string new_db)
        {
            await Task.Run(() => Merge(db1, db2, new_db));
        }

        bool CreateNewDB(string name)
        {
            if (Exists(name)) DeleteDatabase(name);

            Log($"Creating the database {name}...");
            return exec($"create database {name}");
        }
        bool DeleteDatabase(string name)
        {
            Log($"Deleting the database {name}...");
            return exec($"drop database {name}");
        }
        bool Exists(string dbName)
        {
            return GetDBNames().Exists(name => name == dbName);
        }


        DataTable execDataTable(string command, bool log = true)
        {
            DataTable res = new DataTable();
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(command, Connection);
                da.Fill(res);
                if (log) Log("Done.\r\n");
            }
            catch (Exception ex)
            {
                if (log) Log($"Error.\r\nError: {ex.Message}\r\n");
            }
            return res;
        }
        object execScalar(string command)
        {
            object val = null;
            try
            {
                SqlCommand cmd = new SqlCommand(command, Connection);
                val = cmd.ExecuteScalar();
            }
            catch (Exception ex)
            {
                Log($"Error: {ex.Message}");
            }
            return val;
        }
        List<string> exec1(string command, bool log = true)
        {
            List<string> result = new List<string>();

            foreach (DataRow r in execDataTable(command, log).Rows)
                result.Add(r.ItemArray[0].ToString());

            return result;
        }
        Dictionary<string, string> execDictionary(string command)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            DataTable t = execDataTable(command);
            foreach (DataRow r in t.Rows)
                result.Add(r.ItemArray[0].ToString(), r.ItemArray[1].ToString());
            return result;
        }
        List<Tuple<string, string, string>> execTuple(string command)
        {
            List<Tuple<string, string, string>> res = new List<Tuple<string, string, string>>();
            DataTable t = execDataTable(command);
            foreach (DataRow r in t.Rows)
                res.Add(Tuple.Create(r.ItemArray[0].ToString(), r.ItemArray[1].ToString(), r.ItemArray[2].ToString()));

            return res;
        }


        bool exec(string command1, string command2 = null, string command3 = null, bool log = true)
        {
            try
            {
                SqlCommand cmd = new SqlCommand(command1, Connection);
                cmd.CommandTimeout = 1000;
                cmd.ExecuteNonQuery();
                if (command2 != null)
                {
                    SqlCommand cmd2 = new SqlCommand(command2, Connection);
                    cmd2.CommandTimeout = 1000;
                    cmd2.ExecuteNonQuery();
                }
                if (command3 != null)
                {
                    SqlCommand cmd3 = new SqlCommand(command3, Connection);
                    cmd3.CommandTimeout = 1000;
                    cmd3.ExecuteNonQuery();
                }
                if (log) Log("Done.\r\n");
                return true;
            }
            catch (Exception ex)
            {
                if (log) Log($"Error.\r\nError: {ex.Message}\r\n");
                return false;
            }

        }
        void Log(string logText)
        {
            log(this, new LabelEditEventArgs(Convert.ToInt32(true), logText));
        }
    }
}