﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Data;

namespace Merger
{
    class ExtendedProperties
    {
        public event EventHandler<LabelEditEventArgs> log;

        public List<ExtendedProperty> Items { get; set; } = new List<ExtendedProperty>();

        public string Database { get; set; }
        public SqlConnection Connection { get; set; }
        public ExtendedProperties(string database, SqlConnection connection, EventHandler<LabelEditEventArgs> upperLog)
        {
            log += upperLog;

            Database = database;
            Connection = connection;

            Items = GetExtendedProperties();
        }
        public ExtendedProperties(ExtendedProperties extendedProperties, string database, EventHandler<LabelEditEventArgs> upperLog)
        {
            log += upperLog;

            Database = database;
            Connection = extendedProperties.Connection;

            foreach (var p in extendedProperties)
                Items.Add(new ExtendedProperty(p.Name, p.Value,
                    p.Level0type, p.Level0name,
                    p.Level1type, p.Level1name,
                    p.Level2type, p.Level2name,
                    Database, Connection, log));
        }

        public void Create()
        {
            foreach (var f in Items)
                f.Create();
        }

        List<ExtendedProperty> GetExtendedProperties()
        {
            #region in SQL
            //SELECT ep.name, value,

            // 'SCHEMA' + CASE WHEN ob.parent_object_id > 0 THEN '/TABLE'ELSE '' END + '/' +
            // CASE WHEN ob.type IN('TF','FN','IF','FS','FT') THEN 'FUNCTION'
            // WHEN ob.type IN('P', 'PC','RF','X') then 'PROCEDURE'
            // WHEN ob.type IN('U','IT') THEN 'TABLE'
            // WHEN ob.type = 'SQ' THEN 'QUEUE'
            // ELSE(ob.type_desc) end
            //+ CASE WHEN col.column_id IS NULL THEN '' ELSE '/COLUMN' END AS thing,

            // CASE WHEN ob.parent_object_id > 0
            // THEN OBJECT_SCHEMA_NAME(ob.parent_object_id)
            // +'.' + OBJECT_NAME(ob.parent_object_id) + '/' + ob.name
            // ELSE OBJECT_SCHEMA_NAME(ob.object_id)+'/' + ob.name END
            //   + CASE WHEN ep.minor_id > 0 THEN '/' + col.name ELSE '' END AS path


            //       FROM sys.extended_properties ep
            // inner join sys.objects ob ON ep.major_id = ob.OBJECT_ID AND class=1
            // LEFT outer join sys.columns col
            // ON ep.major_id= col.Object_id AND class=1 
            // AND ep.minor_id=col.column_id

            #endregion
            List<ExtendedProperty> res = new List<ExtendedProperty>();

            Log($"Get extended properties from {Database}...");

            List<Tuple<string, string, string, string>> temp = exec4($"use {Database} " +
                $"SELECT ep.name, value, " +
                $"'SCHEMA' + CASE WHEN ob.parent_object_id > 0 THEN '/TABLE'ELSE '' END + '/' + " +
                $"CASE WHEN ob.type IN('TF','FN','IF','FS','FT') THEN 'FUNCTION' " +
                $"WHEN ob.type IN('P', 'PC','RF','X') then 'PROCEDURE' " +
                $"WHEN ob.type IN('U','IT') THEN 'TABLE' " +
                $"WHEN ob.type = 'SQ' THEN 'QUEUE' " +
                $"ELSE(ob.type_desc) end " +
                $"+ CASE WHEN col.column_id IS NULL THEN '' ELSE '/COLUMN' END AS thing, " +
                $"CASE WHEN ob.parent_object_id > 0 " +
                $"THEN OBJECT_SCHEMA_NAME(ob.parent_object_id) " +
                $"+'.' + OBJECT_NAME(ob.parent_object_id) + '/' + ob.name " +
                $"ELSE OBJECT_SCHEMA_NAME(ob.object_id)+'/' + ob.name END " +
                $"+ CASE WHEN ep.minor_id > 0 THEN '/' + col.name ELSE '' END AS path " +
                $"FROM sys.extended_properties ep " +
                $"inner join sys.objects ob ON ep.major_id = ob.OBJECT_ID AND class=1 " +
                $"LEFT outer join sys.columns col " +
                $"ON ep.major_id= col.Object_id AND class=1 " +
                $"AND ep.minor_id=col.column_id " +
                $"use master");
            List<Tuple<string, string, string, string>> temp2 = new List<Tuple<string, string, string, string>>();
            //Обработка temp по p.Item2 (value in SQL)
            foreach (var p in temp)
            {
                if (p.Item2 == "System.Byte[]")
                {
                    string newItem2 = "";
                    if (p.Item1 == "MS_DefaultView") newItem2 = "0x02";
                    else if (p.Item1 == "MS_Orientation") newItem2 = "0x00";
                    else if (p.Item1 == "MS_TabularCharSet") newItem2 = "0xCC";
                    else if (p.Item1 == "MS_TabularFamily") newItem2 = "0x31";
                    temp2.Add(new Tuple<string, string, string, string>(p.Item1, newItem2, p.Item3, p.Item4));
                }
                else if (p.Item2 == "") temp2.Add(new Tuple<string, string, string, string>(p.Item1, "NULL", p.Item3, p.Item4));
                else if (IsDigit(p.Item2)) temp2.Add(new Tuple<string, string, string, string>(p.Item1, p.Item2, p.Item3, p.Item4));
                else temp2.Add(new Tuple<string, string, string, string>(p.Item1, $@"N'{p.Item2}'", p.Item3, p.Item4));

            }

            foreach (var p in temp2)
            {
                List<string> thing = p.Item3.Split('/').ToList();
                List<string> path = p.Item4.Split('/').ToList();
                res.Add(new ExtendedProperty(p.Item1, p.Item2,
                    thing[0], path[0],
                    thing[1], path[1],
                    thing.Count == 3 ? thing[2] : "",
                    path.Count == 3 ? path[2] : "",
                    Database, Connection, log));
            }

            return res;
        }

        bool IsDigit(string str)
        {
            return int.TryParse(str, out int t);
        }

        public IEnumerator<ExtendedProperty> GetEnumerator()
        {
            return Items.GetEnumerator();
        }
        DataTable execDataTable(string command)
        {
            DataTable res = new DataTable();
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(command, Connection);
                da.Fill(res);
                Log("Done.\r\n");
            }
            catch (Exception ex)
            {
                Log($"Error.\r\nError: {ex.Message}\r\n");
            }
            return res;
        }
        List<Tuple<string, string, string, string>> exec4(string command)
        {
            List<Tuple<string, string, string, string>> res = new List<Tuple<string, string, string, string>>();
            DataTable t = execDataTable(command);
            foreach (DataRow r in t.Rows)
                res.Add(Tuple.Create(r.ItemArray[0].ToString(), r.ItemArray[1].ToString(), r.ItemArray[2].ToString(),
                    r.ItemArray[3].ToString()));

            return res;
        }

        Dictionary<string, string> execDictionary(string command)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            DataTable t = execDataTable(command);
            foreach (DataRow r in t.Rows)
                result.Add(r.ItemArray[0].ToString(), r.ItemArray[1].ToString());
            return result;
        }
        void Log(string logText)
        {
            log(this, new LabelEditEventArgs(Convert.ToInt32(true), logText));
        }

    }
    class ExtendedProperty
    {
        public event EventHandler<LabelEditEventArgs> log;

        public string Name { get; set; }
        public string Value { get; set; }

        public string Level0type { get; set; }
        public string Level0name { get; set; }

        public string Level1type { get; set; }
        public string Level1name { get; set; }

        public string Level2type { get; set; }
        public string Level2name { get; set; }

        public string Database { get; set; }
        public SqlConnection Connection { get; set; }
        public ExtendedProperty(string name, string value,
                                string level0type, string level0name,
                                string level1type, string level1name,
                                string level2type, string level2name,
                                string database, SqlConnection connection, EventHandler<LabelEditEventArgs> upperLog)
        {
            log += upperLog;

            Database = database;
            Connection = connection;

            Name = name;
            Value = value;

            Level0type = level0type;
            Level0name = level0name;

            Level1type = level1type;
            Level1name = level1name;

            Level2type = level2type;
            Level2name = level2name;

        }
        public void Create()
        {
            #region in SQL
            //EXECUTE sp_addextendedproperty @name = N'MS_ColumnOrder', @value = 0,
            //@level0type = N'SCHEMA', @level0name = N'dbo', 
            //@level1type = N'TABLE', @level1name = N'eqRolesToolBarDenied',
            //@level2type = N'COLUMN', @level2name = N'DocTypeID';
            #endregion
            Log($"Create {Name}...");
            exec($"use {Database}", null, null, false);
            exec($"EXECUTE sp_addextendedproperty @name = N'{Name}', @value = {Value} " +
                 $", @level0type = N'{Level0type}', @level0name = N'{Level0name}' " +
                 $", @level1type = N'{Level1type}', @level1name = N'{Level1name}'" +
                 $"{(Level2type != "" ? $" , @level2type = N'{Level2type}', @level2name = N'{Level2name}'" : "")}");
            exec($"use master", null, null, false);
        }

        bool exec(string command1, string command2 = null, string command3 = null, bool log = true)
        {
            try
            {
                SqlCommand cmd = new SqlCommand(command1, Connection);
                cmd.CommandTimeout = 1000;
                cmd.ExecuteNonQuery();
                if (command2 != null)
                {
                    SqlCommand cmd2 = new SqlCommand(command2, Connection);
                    cmd2.CommandTimeout = 1000;
                    cmd2.ExecuteNonQuery();
                }
                if (command3 != null)
                {
                    SqlCommand cmd3 = new SqlCommand(command3, Connection);
                    cmd3.CommandTimeout = 1000;
                    cmd3.ExecuteNonQuery();
                }
                if (log) Log("Done.\r\n");
                return true;
            }
            catch (Exception ex)
            {
                if (log) Log($"Error.\r\nError: {ex.Message}\r\n");
                return false;
            }

        }
        void Log(string logText)
        {
            log(this, new LabelEditEventArgs(Convert.ToInt32(true), logText));
        }

    }
}

