﻿namespace Merger
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.label1 = new System.Windows.Forms.Label();
            this._Connect = new System.Windows.Forms.Button();
            this._Server = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this._Login = new System.Windows.Forms.TextBox();
            this._Password = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this._Merge = new System.Windows.Forms.Button();
            this._Database1 = new System.Windows.Forms.ComboBox();
            this._Database2 = new System.Windows.Forms.ComboBox();
            this._Exit = new System.Windows.Forms.Button();
            this._Status = new System.Windows.Forms.PictureBox();
            this._Log = new System.Windows.Forms.RichTextBox();
            this._Progress = new System.Windows.Forms.ProgressBar();
            this._Arrow = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this._Status)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._Arrow)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.SystemColors.Control;
            this.label1.Location = new System.Drawing.Point(47, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Server:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _Connect
            // 
            this._Connect.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this._Connect.FlatAppearance.BorderSize = 0;
            this._Connect.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(110)))), ((int)(((byte)(110)))));
            this._Connect.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(120)))), ((int)(((byte)(120)))));
            this._Connect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._Connect.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._Connect.ForeColor = System.Drawing.SystemColors.Control;
            this._Connect.Location = new System.Drawing.Point(12, 179);
            this._Connect.Name = "_Connect";
            this._Connect.Size = new System.Drawing.Size(430, 83);
            this._Connect.TabIndex = 4;
            this._Connect.TabStop = false;
            this._Connect.Text = "Connect";
            this._Connect.UseVisualStyleBackColor = false;
            this._Connect.Click += new System.EventHandler(this._Connect_Click);
            // 
            // _Server
            // 
            this._Server.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this._Server.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Server.Cursor = System.Windows.Forms.Cursors.Default;
            this._Server.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._Server.ForeColor = System.Drawing.SystemColors.Control;
            this._Server.Location = new System.Drawing.Point(142, 30);
            this._Server.Name = "_Server";
            this._Server.Size = new System.Drawing.Size(738, 33);
            this._Server.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.SystemColors.Control;
            this.label2.Location = new System.Drawing.Point(60, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 25);
            this.label2.TabIndex = 4;
            this.label2.Text = "Login:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _Login
            // 
            this._Login.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this._Login.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Login.Cursor = System.Windows.Forms.Cursors.Default;
            this._Login.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._Login.ForeColor = System.Drawing.SystemColors.Control;
            this._Login.Location = new System.Drawing.Point(142, 86);
            this._Login.Name = "_Login";
            this._Login.Size = new System.Drawing.Size(738, 33);
            this._Login.TabIndex = 2;
            // 
            // _Password
            // 
            this._Password.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this._Password.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Password.Cursor = System.Windows.Forms.Cursors.Default;
            this._Password.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._Password.ForeColor = System.Drawing.SystemColors.Control;
            this._Password.Location = new System.Drawing.Point(142, 129);
            this._Password.Name = "_Password";
            this._Password.Size = new System.Drawing.Size(738, 33);
            this._Password.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.SystemColors.Control;
            this.label3.Location = new System.Drawing.Point(11, 131);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(133, 25);
            this.label3.TabIndex = 6;
            this.label3.Text = "Password:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _Merge
            // 
            this._Merge.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this._Merge.Enabled = false;
            this._Merge.FlatAppearance.BorderSize = 0;
            this._Merge.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(110)))), ((int)(((byte)(110)))));
            this._Merge.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(120)))), ((int)(((byte)(120)))));
            this._Merge.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._Merge.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._Merge.ForeColor = System.Drawing.SystemColors.Control;
            this._Merge.Location = new System.Drawing.Point(450, 179);
            this._Merge.Name = "_Merge";
            this._Merge.Size = new System.Drawing.Size(430, 85);
            this._Merge.TabIndex = 8;
            this._Merge.TabStop = false;
            this._Merge.Text = "Merge";
            this._Merge.UseVisualStyleBackColor = false;
            this._Merge.Click += new System.EventHandler(this._Merge_Click);
            // 
            // _Database1
            // 
            this._Database1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this._Database1.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this._Database1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._Database1.Enabled = false;
            this._Database1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._Database1.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._Database1.ForeColor = System.Drawing.SystemColors.Control;
            this._Database1.FormattingEnabled = true;
            this._Database1.Location = new System.Drawing.Point(10, 277);
            this._Database1.Name = "_Database1";
            this._Database1.Size = new System.Drawing.Size(406, 34);
            this._Database1.Sorted = true;
            this._Database1.TabIndex = 4;
            // 
            // _Database2
            // 
            this._Database2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this._Database2.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this._Database2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._Database2.Enabled = false;
            this._Database2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._Database2.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._Database2.ForeColor = System.Drawing.SystemColors.Control;
            this._Database2.FormattingEnabled = true;
            this._Database2.Location = new System.Drawing.Point(479, 277);
            this._Database2.Name = "_Database2";
            this._Database2.Size = new System.Drawing.Size(401, 34);
            this._Database2.Sorted = true;
            this._Database2.TabIndex = 5;
            // 
            // _Exit
            // 
            this._Exit.FlatAppearance.BorderSize = 0;
            this._Exit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(110)))), ((int)(((byte)(110)))));
            this._Exit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(120)))), ((int)(((byte)(120)))));
            this._Exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._Exit.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._Exit.ForeColor = System.Drawing.SystemColors.Control;
            this._Exit.Location = new System.Drawing.Point(829, 1);
            this._Exit.Name = "_Exit";
            this._Exit.Size = new System.Drawing.Size(64, 24);
            this._Exit.TabIndex = 13;
            this._Exit.TabStop = false;
            this._Exit.Text = "X";
            this._Exit.UseVisualStyleBackColor = true;
            // 
            // _Status
            // 
            this._Status.Location = new System.Drawing.Point(10, 177);
            this._Status.Name = "_Status";
            this._Status.Size = new System.Drawing.Size(434, 87);
            this._Status.TabIndex = 14;
            this._Status.TabStop = false;
            // 
            // _Log
            // 
            this._Log.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this._Log.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this._Log.Cursor = System.Windows.Forms.Cursors.Default;
            this._Log.DetectUrls = false;
            this._Log.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._Log.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._Log.ForeColor = System.Drawing.SystemColors.Control;
            this._Log.Location = new System.Drawing.Point(0, 358);
            this._Log.MaxLength = 0;
            this._Log.Name = "_Log";
            this._Log.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this._Log.Size = new System.Drawing.Size(892, 557);
            this._Log.TabIndex = 15;
            this._Log.Text = "";
            this._Log.TextChanged += new System.EventHandler(this._Log_TextChanged);
            // 
            // _Progress
            // 
            this._Progress.Location = new System.Drawing.Point(10, 317);
            this._Progress.MarqueeAnimationSpeed = 200;
            this._Progress.Maximum = 2801;
            this._Progress.Name = "_Progress";
            this._Progress.Size = new System.Drawing.Size(870, 35);
            this._Progress.Step = 1;
            this._Progress.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this._Progress.TabIndex = 16;
            // 
            // _Arrow
            // 
            this._Arrow.Location = new System.Drawing.Point(423, 277);
            this._Arrow.Name = "_Arrow";
            this._Arrow.Size = new System.Drawing.Size(50, 34);
            this._Arrow.TabIndex = 17;
            this._Arrow.TabStop = false;
            // 
            // MainForm
            // 
            this.AcceptButton = this._Connect;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.ClientSize = new System.Drawing.Size(892, 915);
            this.Controls.Add(this._Arrow);
            this.Controls.Add(this._Progress);
            this.Controls.Add(this._Log);
            this.Controls.Add(this._Server);
            this.Controls.Add(this._Login);
            this.Controls.Add(this._Exit);
            this.Controls.Add(this._Database2);
            this.Controls.Add(this._Database1);
            this.Controls.Add(this._Merge);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this._Connect);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._Password);
            this.Controls.Add(this._Status);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            ((System.ComponentModel.ISupportInitialize)(this._Status)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._Arrow)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button _Connect;
        private System.Windows.Forms.TextBox _Server;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox _Login;
        private System.Windows.Forms.TextBox _Password;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button _Merge;
        private System.Windows.Forms.ComboBox _Database1;
        private System.Windows.Forms.ComboBox _Database2;
        private System.Windows.Forms.Button _Exit;
        private System.Windows.Forms.PictureBox _Status;
        private System.Windows.Forms.RichTextBox _Log;
        private System.Windows.Forms.ProgressBar _Progress;
        private System.Windows.Forms.PictureBox _Arrow;
    }
}

