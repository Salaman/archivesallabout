select a.name, COUNT(f.ISN_FUND) as FUNDCOUNT from tblARCHIVE as a -- 1.COUNT(f.ISN_FUND) 2.COUNT(i.ISN_INVENTORY) 3.COUNT(u.ISN_UNIT) 4.COUNT(d.ISN_DOCUM)
left join tblFUND as f on a.ISN_ARCHIVE = f.ISN_ARCHIVE and f.Deleted = 0 --join alternately : first only tblFUND, then tblFUND and tblINVENTORY etc...
--left join tblINVENTORY as i on i.ISN_FUND = f.ISN_FUND and i.Deleted = 0
--left join tblUNIT as u on u.ISN_INVENTORY = i.ISN_INVENTORY and u.Deleted = 0
--left join tblDOCUMENT as d on d.ISN_UNIT = u.ISN_UNIT and d.Deleted = 0
where a.Deleted = 0
group by a.ISN_ARCHIVE, a.name order by a.name ASC


-- count all (alternately) for all ARCHIVES