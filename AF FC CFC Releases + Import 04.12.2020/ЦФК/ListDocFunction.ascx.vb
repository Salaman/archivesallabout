﻿Partial Public Class ListDocFunction
    Inherits eqBaseUserControl

    Public Event FunctionChanged(ByVal sender As Object, ByVal e As EventArgs)

    Public Property FunctionReadOnly() As Boolean
        Get
            Return ViewState("FunctionReadOnly")
        End Get
        Set(ByVal value As Boolean)
            ViewState("FunctionReadOnly") = value
        End Set
    End Property

    Public Property FunctionID() As Guid
        Get
            Return ViewState("FunctionID")
        End Get
        Set(ByVal value As Guid)
            ViewState("FunctionID") = value
        End Set
    End Property

    Public Property FunctionName() As String
        Get
            Return ViewState("FunctionName")
        End Get
        Set(ByVal value As String)
            ViewState("FunctionName") = value
        End Set
    End Property

    Public Property FunctionType() As String
        Get
            Return ViewState("FunctionType")
        End Get
        Set(ByVal value As String)
            ViewState("FunctionType") = value
            If FiltersVeriflyed Then
                VeriflyFunction()
            End If
        End Set
    End Property

    Public Property FunctionEnabled() As Boolean
        Get
            If ViewState("FunctionEnabled") IsNot Nothing Then
                Return ViewState("FunctionEnabled")
            Else
                Return True
            End If
        End Get
        Set(ByVal value As Boolean)
            ViewState("FunctionEnabled") = value
            If FiltersVeriflyed Then
                VeriflyFunction()
            End If
        End Set
    End Property

    Public Property ParameterName() As String
        Get
            Return ViewState("ParameterName")
        End Get
        Set(ByVal value As String)
            ViewState("ParameterName") = value
        End Set
    End Property

    Public Property ParameterValue() As String
        Get
            Return ViewState("FunctionValue")
        End Get
        Set(ByVal value As String)
            ViewState("FunctionValue") = value
            If FiltersVeriflyed Then
                VeriflyFunction()
            End If
        End Set
    End Property

    Public Property ParameterValuesDataTable() As DataTable
        Get
            Return ViewState("ParameterValues")
        End Get
        Set(ByVal value As DataTable)
            ViewState("ParameterValues") = value
            If FiltersVeriflyed Then
                VeriflyFunction()
            End If
        End Set
    End Property

    Public Property ParameterDefaultValue() As String
        Get
            Return ViewState("ParameterDefaultValue")
        End Get
        Set(ByVal value As String)
            ViewState("ParameterDefaultValue") = value
            If FiltersVeriflyed Then
                VeriflyFunction()
            End If
        End Set
    End Property

    Public Overrides Sub Focus()
        FunctionTextBox.Focus()
    End Sub

    Dim FiltersVeriflyed As Boolean

    Public Sub VeriflyFunction()
        FunctionDropDownList.Visible = False
        FunctionTextBox.Visible = False
        FunctionCheckBox.Visible = False
        FunctionCalendarextender.Enabled = False
        FunctionFilteredTextBoxExtender.Enabled = False
        If ParameterValuesDataTable IsNot Nothing Then
            FunctionDropDownList.Visible = True
            FunctionDropDownList.Items.Clear()
            FunctionDropDownList.Items.Add(String.Empty)
            For Each R In ParameterValuesDataTable.Rows
                If ParameterValuesDataTable.Columns.Count > 0 Then
                    Dim ddItem As New ListItem
                    ddItem.Value = R(0).ToString
                    If ParameterValuesDataTable.Columns.Count = 1 Then
                        ddItem.Text = R(0).ToString
                    Else
                        Dim pText As New StringBuilder
                        For cIndex As Integer = 1 To ParameterValuesDataTable.Columns.Count - 1
                            If pText.Length > 0 Then
                                pText.Append(" | ")
                            End If
                            pText.Append(R(cIndex).ToString)
                        Next
                        ddItem.Text = pText.ToString
                    End If
                    FunctionDropDownList.Items.Add(ddItem)
                    If ParameterValue IsNot Nothing Then
                        If ddItem.Value = ParameterValue Then
                            FunctionDropDownList.SelectedValue = ParameterValue
                        End If
                    ElseIf ParameterDefaultValue IsNot Nothing Then
                        If ddItem.Value = ParameterDefaultValue Then
                            FunctionDropDownList.SelectedValue = ParameterDefaultValue
                        End If
                    End If
                End If
            Next
        Else
            Select Case FunctionType
                Case "decimal", "money", "numeric", "smallmoney", "float", "real"
                    FunctionTextBox.Visible = True
                    FunctionTextBox.Text = ParameterValue
                    FunctionFilteredTextBoxExtender.Enabled = True
                    FunctionFilteredTextBoxExtender.ValidChars = "."
                    FunctionFilteredTextBoxExtender.FilterType = AjaxControlToolkit.FilterTypes.Custom Or AjaxControlToolkit.FilterTypes.Numbers
                Case "bigint", "int", "smallint", "tinyint"
                    FunctionTextBox.Visible = True
                    FunctionTextBox.Text = ParameterValue
                    FunctionFilteredTextBoxExtender.Enabled = True
                    FunctionFilteredTextBoxExtender.FilterType = AjaxControlToolkit.FilterTypes.Numbers
                Case "char", "varchar", "nchar", "nvarchar", "text", "ntext"
                    FunctionTextBox.Visible = True
                    FunctionTextBox.Text = ParameterValue
                Case "uniqueidentifier"
                    FunctionTextBox.Visible = True
                    FunctionTextBox.Text = ParameterValue
                Case "date", "datetime", "smalldatetime"
                    FunctionTextBox.Visible = True
                    FunctionTextBox.Text = ParameterValue
                    FunctionCalendarextender.Enabled = True
                Case "bit"
                    FunctionCheckBox.Visible = True
                    Boolean.TryParse(ParameterValue, FunctionCheckBox.Checked)
                Case "binary", "varbinary", "image", "timestamp", "sql_variant"
                    FunctionCheckBox.Visible = True
                    Boolean.TryParse(ParameterValue, FunctionCheckBox.Checked)
            End Select
        End If
        FiltersVeriflyed = True
    End Sub

#Region "Page Events"

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        VeriflyFunction()
        ' Visiblites
        If FunctionReadOnly Then
            FunctionDropDownList.Visible = False
            FunctionLinkButton.Visible = False
            FunctionTextBox.Visible = False
            FunctionCheckBox.Visible = False
            FunctionCalendarextender.Enabled = False
            FunctionFilteredTextBoxExtender.Enabled = False

            FunctionNameLabel.Visible = True
            FunctionNameLabel.Text = FunctionName
            FunctionLabel.Visible = True
            FunctionLabel.Text = ParameterValue
        Else
            FunctionNameLabel.Visible = True
            FunctionLabel.Visible = False

            FunctionLinkButton.Text = FunctionName
        End If
        ' Param
        If ParameterName = String.Empty Then
            tdParameter.Visible = False
        Else
            tdParameter.Visible = True
        End If
        ' Style
        If FunctionReadOnly Then
            tableFunction.Attributes("class") = "MenuSubItemStyle"
        ElseIf FunctionEnabled Then
            tableFunction.Attributes("class") = "MenuItemStyle MenuItemStyleSelected"
        Else
            tableFunction.Attributes("class") = "MenuItemStyle"
        End If
    End Sub

#End Region

#Region "Events Change"

    Private Sub FunctionCheckBox_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FunctionCheckBox.CheckedChanged
        ParameterValue = FunctionCheckBox.Checked.ToString
        FunctionEnabled = True
        RaiseEvent FunctionChanged(Me, EventArgs.Empty)
    End Sub

    Private Sub FunctionTextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles FunctionTextBox.TextChanged
        ParameterValue = FunctionTextBox.Text
        FunctionEnabled = True
        RaiseEvent FunctionChanged(Me, EventArgs.Empty)
    End Sub

    Private Sub FunctionDropDownList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FunctionDropDownList.SelectedIndexChanged
        ParameterValue = FunctionDropDownList.SelectedValue
        FunctionEnabled = True
        RaiseEvent FunctionChanged(Me, EventArgs.Empty)
    End Sub

#End Region

    Private Sub FunctionLinkButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles FunctionLinkButton.Click
        FunctionEnabled = Not FunctionEnabled
        RaiseEvent FunctionChanged(Me, EventArgs.Empty)
    End Sub

End Class