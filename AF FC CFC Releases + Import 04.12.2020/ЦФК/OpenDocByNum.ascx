﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="OpenDocByNum.ascx.vb"
    Inherits="WebApplication.OpenDocByNum" %>
<asp:UpdatePanel ID="UpdatePanelOpenDocByNum" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <table width="100%" cellpadding="0" cellspacing="0" style="height: 29px;" class="LogicBlock">
            <tr>
                <td>
                    <eq:eqTextBox ID="DocNum" runat="server" AutoPostBack="true" />
                </td>
                <td style="width: 20px;">
                    <asp:ImageButton ID="OpenDoc" runat="server" ImageAlign="AbsMiddle" AlternateText="Искать документ" />
                </td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>
