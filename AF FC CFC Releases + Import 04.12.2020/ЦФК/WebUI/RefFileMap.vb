﻿Imports System.IO
Imports System.Data
Imports System.Data.SqlClient

<Microsoft.VisualBasic.HideModuleName()> _
Public Module RefFileMap

    Const PATH_PATTERN = "{0:yyyyMMdd_HHmmss}_{1}"
    Const DB_STORAGE_GUID = "84E12153-532F-46AD-A0B7-0541450978E5"

#Region "Старая иерархическая версия"

    ' ID                Идентификатор файла
    ' TransferTime      Время передачи файла
    ' Kind              Тип объекта учета (фонд, опись ...)
    ' Storage           Дополнительно задает подкаталог для некоторых объектов учета
    ' Crumbs            Крошилово для определения пути
    ' FileName          Имя файла

    <Obsolete("Старая иерархическая версия", True)> _
    Public Function GetFileSystemAttachFileName(ByVal ID As Guid, ByVal TransferTime As Date, ByVal Kind As Integer, ByVal GrStorage As Object, ByVal Crumbs As WebUI.ArchiveCrumbs, Optional ByVal FileName As String = "") As String
        Dim FilePath As String = My.Settings.StoragePath
#If GR Then
        If String.IsNullOrEmpty(FilePath) Then
            FilePath = "C:\Госреестр-Файлы"
        End If
#Else
        If String.IsNullOrEmpty(FilePath) Then
            FilePath = "C:\ArchiveFundStorage"
        End If
#End If

        If Crumbs.ISN_ARCHIVE IsNot Nothing Then
            Dim AppSettings = Crumbs.ISN_ARCHIVE.AppSettings

            Dim cmd As New SqlCommand("select Text from tblConstantsSpec where ID = @ID")
            cmd.Parameters.AddWithValue("@ID", New Guid(DB_STORAGE_GUID))
            Dim dt = AppSettings.GetDataTable(cmd)

            If dt.Rows.Count > 0 Then
                FilePath = dt.Rows(0)("Text")
            End If

            If Crumbs.ISN_AuthorizedDep IsNot Nothing Then
                cmd = New SqlCommand("select CreationDateTime, ShortName from tblAuthorizedDep where ID = @ID")
                cmd.Parameters.AddWithValue("@ID", Crumbs.ISN_AuthorizedDep.DocID)
                dt = AppSettings.GetDataTable(cmd)

                BuildPath(FilePath, String.Format(PATH_PATTERN, dt.Rows(0)(0), Crumbs.ISN_AuthorizedDep.DocID.ToString("N")))
                If FileName = "" Then
                    FilePath = Directory.GetDirectories(FilePath).First()
                Else
                    If dt.Rows(0)(1).ToString <> "" Then
                        BuildPath(FilePath, dt.Rows(0)(1))
                    Else
                        BuildPath(FilePath, "Без имени")
                    End If
                End If
            End If

            cmd = New SqlCommand("select CreationDateTime, NAME_SHORT from tblARCHIVE where ID = @ID")
            cmd.Parameters.AddWithValue("@ID", Crumbs.ISN_ARCHIVE.DocID)
            dt = AppSettings.GetDataTable(cmd)

            BuildPath(FilePath, String.Format(PATH_PATTERN, dt.Rows(0)(0), Crumbs.ISN_ARCHIVE.DocID.ToString("N")))
            If FileName = "" Then
                FilePath = Directory.GetDirectories(FilePath).First()
            Else
                If dt.Rows(0)(1).ToString <> "" Then
                    BuildPath(FilePath, dt.Rows(0)(1))
                Else
                    BuildPath(FilePath, "Без имени")
                End If
            End If

            Select Case Kind
                Case 700
                    BuildPath(FilePath, "Архив")
                Case 701
                    BuildPath(FilePath, "Фонды")
                    If IsDBNull(GrStorage) OrElse GrStorage <> 100 Then
                        BuildPath(FilePath, "НСА")
                    End If
                Case 702
                    BuildPath(FilePath, "Описи")
                    If IsDBNull(GrStorage) OrElse GrStorage <> 100 Then
                        BuildPath(FilePath, "НСА")
                    End If
                Case 703, 704
                    BuildPath(FilePath, "Единицы хранения и учета")
                    If IsDBNull(GrStorage) OrElse GrStorage <> 100 Then
                        BuildPath(FilePath, "НСА")
                    End If
                Case 705
                    BuildPath(FilePath, "Документы")
                    If IsDBNull(GrStorage) OrElse GrStorage <> 100 Then
                        BuildPath(FilePath, "НСА")
                    End If
                Case 706
                    BuildPath(FilePath, "Акты")
                Case 709
                    BuildPath(FilePath, "Документы Госреестр")
                Case Else
                    BuildPath(FilePath, "Другое")
            End Select
            BuildPath(FilePath, String.Format(PATH_PATTERN, TransferTime, ID.ToString("N")))
            If FileName = "" Then
                FilePath = Directory.GetFiles(FilePath).First()
            Else
                BuildPath(FilePath, FileName)
            End If
        End If

        Return FilePath
    End Function

#End Region

    Public ReadOnly Property StorageFolder() As String
        Get
            If String.IsNullOrEmpty(_StorageFolder) Then
                Dim AppSettings = DirectCast(HttpContext.Current.Session("AppSettings"), eqAppSettings)
                Dim StorageCommand As New SqlCommand("select Text from tblConstantsSpec where ID = @ID")
                StorageCommand.Parameters.AddWithValue("@ID", New Guid(DB_STORAGE_GUID))
                Dim StorageResult = AppSettings.ExecCommandScalar(StorageCommand)

                If StorageResult IsNot Nothing Then
                    _StorageFolder = StorageResult.ToString
                Else
                    _StorageFolder = My.Settings.StoragePath
                End If
            End If
            Return _StorageFolder
        End Get
    End Property
    Private _StorageFolder As String

    Public Function GetFileSystemAttachFileName(ByVal ID As Guid, ByVal TransferTime As Date, ByVal Kind As Integer, ByVal GrStorage As Object, Optional ByVal FileName As String = "") As String
        Dim FilePath As String = StorageFolder

        If String.IsNullOrEmpty(FilePath) Then
            FilePath = "C:\ArchiveFundStorage"
        End If
        BuildPath(FilePath, String.Format(PATH_PATTERN, TransferTime, ID.ToString("N")))
        If FileName = "" Then
            FilePath = Directory.GetFiles(FilePath).First()
        Else
            BuildPath(FilePath, FileName)
        End If

        Return FilePath
    End Function
    
    'Для Госреестра упрощенно без ISN и сложных путей NataliaLyalina 2012-12
    Public Function GetFileSystemAttachFileNameGR(ByVal ID As Guid, ByVal TransferTime As Date, ByVal Kind As Integer, ByVal SUBJECT As String, ByVal ARCHIVE As String, ByVal DOC_ATTR As String, ByVal FilePath As String, Optional ByVal FileName As String = "") As String
        '  Dim FilePath As String = My.Settings.StoragePath

#If GR Then
        If String.IsNullOrEmpty(FilePath) Then
            FilePath = "C:\Госреестр-Файлы"
        End If
#Else

#End If

        'Надо определить имя файла по ID, это надо когда мы открываем файл

        BuildPath(FilePath, SUBJECT)
        BuildPath(FilePath, ARCHIVE)
        'Select Case Kind
        '    Case 709
        '        BuildPath(FilePath, "Документы Госреестр")
        '    Case 710
        '        BuildPath(FilePath, "Экспертная оценка")
        '    Case Else
        '        BuildPath(FilePath, "Другое")
        'End Select
        FilePath = Path.Combine(FilePath, DOC_ATTR)
        ' BuildPath(FilePath, DOC_ATTR)

        If FileName = "" Then
            FilePath = Directory.GetFiles(FilePath).First()
        Else
            BuildPath(FilePath, FileName)
        End If


        Return FilePath
    End Function

    Private Sub BuildPath(ByRef LocalPath As String, ByVal AddPath As String)
        For Each c In Path.GetInvalidPathChars.Concat(Path.GetInvalidFileNameChars).Distinct
            AddPath = AddPath.Replace(c, "_")
        Next
        LocalPath = Path.Combine(LocalPath, AddPath)
    End Sub
    
End Module