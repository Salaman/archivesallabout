﻿Imports System.IO

Partial Public Class RefFile
    Inherits BaseCompositeControl

    Public Event Requesting(ByVal sender As Object, ByVal e As RefFileArgs)

    Public Property Mode() As RefFiledMode
        Get
            Return _Mode
        End Get
        Set(ByVal value As RefFiledMode)
            _Mode = value
        End Set
    End Property
    Private _Mode As RefFiledMode = RefFiledMode.DataBase

    Protected Overrides Sub CreateChildControls()
        Dim FileLink As New LinkButton With {.ID = "FileLink", .Text = "Загрузить"}
        AddHandler FileLink.Click, AddressOf FileLink_Click
        Controls.Add(FileLink)
    End Sub

    Public ReadOnly Property FileLink() As LinkButton
        Get
            Return DirectCast(FindControl("FileLink"), LinkButton)
        End Get
    End Property

    Public Property Value() As Guid
        Get
            If ViewState("ID") Is Nothing Then
                Return Guid.Empty
            Else
                Return ViewState("ID")
            End If
        End Get
        Set(ByVal value As Guid)
            ViewState("ID") = value
        End Set
    End Property

    Private Sub FileLink_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try 'добавила обработку ошибок Natalia Lyalina 12-2012
            Dim Args As New RefFileArgs
            Args.ID = Value
            RaiseEvent Requesting(Me, Args)
            Select Case Mode
                Case RefFiledMode.DataBase
                    Using dc As New RefFileDataContext(AppSettings.Cnn)
                        Dim filContent = dc.tblFILE_CONTENTS2s.FirstOrDefault(Function(F) F.ID = Value)
                        BasePage.AttachFile("File" & filContent.FILE_EXTENSION, filContent.CONTENTS.ToArray)
                    End Using
                Case RefFiledMode.FileSystem
                    BasePage.AttachFile(Path.ChangeExtension(Path.GetFileName(Args.FileName), Path.GetExtension(Args.FileName)), My.Computer.FileSystem.ReadAllBytes(Args.FileName))
            End Select
        Catch ex As Exception
            AddMessage(ex.Message)
        End Try
    End Sub

End Class

Public Enum RefFiledMode
    DataBase
    FileSystem
End Enum

Public Class RefFileArgs
    Inherits EventArgs

    Public ID As Guid
    Public TransferTime As Date
    Public FileName As String
    Public Cancel As Boolean
End Class