﻿Namespace WebUI

    Public Class ArchiveCrumbs
        Inherits CompositeControl

        Private _ISN_AuthorizedDep_Control As eqDocument = Nothing
        Private _ISN_ARCHIVE_Control As eqDocument = Nothing
        Private _ID_ARCHIVE_Control As eqDocument = Nothing
        Private _ISN_FUND_Control As eqDocument = Nothing
        Private _ISN_INVENTORY_Control As eqDocument = Nothing
        Private _ISN_UNIT_Control As eqDocument = Nothing
        Private _ISN_GR_DOCUMENT_Control As eqDocument = Nothing
        Private _ISN_GR_DOC_DESCRIPT_Control As eqDocument = Nothing
        Private _Level As ArchiveCrumbsLevel = ArchiveCrumbsLevel.DEP

        Public Enum ArchiveCrumbsLevel
            DEP
            ARCHIVE
            GR_DOCUMENT
            GR_DOC_DESCRIPT
            FUND
            ACT
            INVENTORY
            UNIT
        End Enum

        Public Property Level() As ArchiveCrumbsLevel
            Get
                Return _Level
            End Get
            Set(ByVal value As ArchiveCrumbsLevel)
                _Level = value
                RecreateChildControls()
            End Set
        End Property

        Public ReadOnly Property ISN_AuthorizedDep() As eqDocument
            Get
                Return _ISN_AuthorizedDep_Control
            End Get
        End Property

        Public ReadOnly Property ISN_ARCHIVE() As eqDocument
            Get
                Return _ISN_ARCHIVE_Control
            End Get
        End Property
        Public ReadOnly Property ID_ARCHIVE() As eqDocument
            Get
                Return _ID_ARCHIVE_Control
            End Get
        End Property
        Public ReadOnly Property ISN_FUND() As eqDocument
            Get
                Return _ISN_FUND_Control
            End Get
        End Property

        Public ReadOnly Property ISN_INVENTORY() As eqDocument
            Get
                Return _ISN_INVENTORY_Control
            End Get
        End Property

        Public ReadOnly Property ISN_UNIT() As eqDocument
            Get
                Return _ISN_UNIT_Control
            End Get
        End Property
        Public ReadOnly Property ISN_GR_DOCUMENT() As eqDocument
            Get
                Return _ISN_GR_DOCUMENT_Control
            End Get
        End Property
        Public ReadOnly Property ISN_GR_DOC_DESCRIPT() As eqDocument
            Get
                Return _ISN_GR_DOC_DESCRIPT_Control
            End Get
        End Property

        Public ReadOnly Property Names(ByVal Level As ArchiveCrumbsLevel) As String
            Get
                If _Names Is Nothing Then
                    _Names = New Dictionary(Of ArchiveCrumbsLevel, String)
                    If ISN_AuthorizedDep IsNot Nothing Then
                        _Names.Add(ArchiveCrumbsLevel.DEP, ISN_AuthorizedDep.DataSetHeaderDataRow("ShortName"))
                    End If
                    If ISN_ARCHIVE IsNot Nothing Then
                        _Names.Add(ArchiveCrumbsLevel.ARCHIVE, ISN_ARCHIVE.DataSetHeaderDataRow("NAME_SHORT"))
                    End If
                    If ISN_FUND IsNot Nothing Then
                        _Names.Add(ArchiveCrumbsLevel.FUND, ISN_FUND.DataSetHeaderDataRow("FUND_NAME_SHORT"))
                    End If
                    If ISN_INVENTORY IsNot Nothing Then
                        _Names.Add(ArchiveCrumbsLevel.INVENTORY, ISN_INVENTORY.DataSetHeaderDataRow("INVENTORY_NAME"))
                    End If
                    If ISN_UNIT IsNot Nothing Then
                        _Names.Add(ArchiveCrumbsLevel.UNIT, ISN_UNIT.DataSetHeaderDataRow("NAME"))
                    End If
                    If ISN_GR_DOCUMENT IsNot Nothing Then
                        _Names.Add(ArchiveCrumbsLevel.GR_DOCUMENT, ISN_GR_DOCUMENT.DataSetHeaderDataRow("NAME"))
                    End If
                    If ISN_GR_DOC_DESCRIPT IsNot Nothing Then
                        _Names.Add(ArchiveCrumbsLevel.GR_DOC_DESCRIPT, ISN_GR_DOC_DESCRIPT.DataSetHeaderDataRow("NAME"))
                    End If
                    If ID_ARCHIVE IsNot Nothing Then
                        _Names.Add(ArchiveCrumbsLevel.ARCHIVE, ID_ARCHIVE.DataSetHeaderDataRow("NAME_SHORT"))
                    End If
                End If
                If _Names.ContainsKey(Level) Then
                    Return _Names(Level)
                Else
                    Return ""
                End If
            End Get
        End Property
        Private _Names As Dictionary(Of ArchiveCrumbsLevel, String) = Nothing

        Protected Overrides Sub CreateChildControls()
            Controls.Clear()

#If FC Or CFC Then
            _ISN_AuthorizedDep_Control = New eqDocument
            _ISN_AuthorizedDep_Control.ID = "ISN_AuthorizedDep"
            _ISN_AuthorizedDep_Control.DocTypeURL = "AuthorizedDep.ascx"
            _ISN_AuthorizedDep_Control.ListDocMode = eqBaseContainer.ListDocModes.NewWindowAndSelect
            _ISN_AuthorizedDep_Control.LoadDocMode = eqBaseContainer.LoadDocModes.NewWindow
            _ISN_AuthorizedDep_Control.ShowingFields = "ShortName, Address"
            _ISN_AuthorizedDep_Control.ShowingFieldsFormat = "{0}"
            _ISN_AuthorizedDep_Control.KeyName = "ISN_AuthorizedDep"
            Controls.Add(_ISN_AuthorizedDep_Control)
#End If

            If Level >= ArchiveCrumbsLevel.ARCHIVE Then
                _ISN_ARCHIVE_Control = New eqDocument
                _ISN_ARCHIVE_Control.ID = "ISN_ARCHIVE"
                _ISN_ARCHIVE_Control.DocTypeURL = "ARCHIVE.ascx"
                _ISN_ARCHIVE_Control.ListDocMode = eqBaseContainer.ListDocModes.NewWindowAndSelect
                _ISN_ARCHIVE_Control.LoadDocMode = IIf(Level > ArchiveCrumbsLevel.ARCHIVE, eqBaseContainer.LoadDocModes.NewWindow, eqBaseContainer.LoadDocModes.NewWindowAndSelect)
                _ISN_ARCHIVE_Control.ShowingFields = "NAME_SHORT, ISN_ARCHIVE, ISN_AuthorizedDep"
                _ISN_ARCHIVE_Control.ShowingFieldsFormat = "Архив {0}"
                _ISN_ARCHIVE_Control.KeyName = "ISN_ARCHIVE"
                Controls.Add(_ISN_ARCHIVE_Control)
            End If

            If Level >= ArchiveCrumbsLevel.FUND Then
                _ISN_FUND_Control = New eqDocument
                _ISN_FUND_Control.ID = "ISN_FUND"
                _ISN_FUND_Control.DocTypeURL = "FUND.ascx"
                _ISN_FUND_Control.ListDocMode = eqBaseContainer.ListDocModes.NewWindowAndSelect
                _ISN_FUND_Control.LoadDocMode = IIf(Level > ArchiveCrumbsLevel.FUND, eqBaseContainer.LoadDocModes.NewWindow, eqBaseContainer.LoadDocModes.NewWindowAndSelect)
                _ISN_FUND_Control.ShowingFields = "FUND_NAME_SHORT, ISN_ARCHIVE, ISN_FUND, FUND_NUM"
                _ISN_FUND_Control.ShowingFieldsFormat = "Фонд №{3} {0}"
                _ISN_FUND_Control.KeyName = "ISN_FUND"
                Controls.Add(_ISN_FUND_Control)
            End If

            If Level >= ArchiveCrumbsLevel.INVENTORY Then
                _ISN_INVENTORY_Control = New eqDocument
                _ISN_INVENTORY_Control.ID = "ISN_INVENTORY"
                _ISN_INVENTORY_Control.DocTypeURL = "INVENTORY.ascx"
                _ISN_INVENTORY_Control.ListDocMode = eqBaseContainer.ListDocModes.NewWindowAndSelect
                _ISN_INVENTORY_Control.LoadDocMode = IIf(Level > ArchiveCrumbsLevel.INVENTORY, eqBaseContainer.LoadDocModes.NewWindow, eqBaseContainer.LoadDocModes.NewWindowAndSelect)
                _ISN_INVENTORY_Control.ShowingFields = "INVENTORY_NAME, ISN_FUND, ISN_INVENTORY, FUND_NUM, INVENTORY_NUM"
                _ISN_INVENTORY_Control.ShowingFieldsFormat = "Опись №{4} {0}"
                _ISN_INVENTORY_Control.KeyName = "ISN_INVENTORY"
                Controls.Add(_ISN_INVENTORY_Control)
            End If

            If Level >= ArchiveCrumbsLevel.UNIT Then
                _ISN_UNIT_Control = New eqDocument
                _ISN_UNIT_Control.ID = "ISN_UNIT"
                _ISN_UNIT_Control.DocTypeURL = "UNIT.ascx"
                _ISN_UNIT_Control.ListDocMode = eqBaseContainer.ListDocModes.NewWindowAndSelect
                _ISN_UNIT_Control.LoadDocMode = eqBaseContainer.LoadDocModes.NewWindowAndSelect
                _ISN_UNIT_Control.ShowingFields = "NAME, ISN_INVENTORY, FUND_NUM, INVENTORY_NUM, UNIT_NUM"
                _ISN_UNIT_Control.ShowingFieldsFormat = "Ед.хр./Ед.уч № {4} {0}"
                _ISN_UNIT_Control.KeyName = "ISN_UNIT"
                Controls.Add(_ISN_UNIT_Control)
            End If

            If Level >= ArchiveCrumbsLevel.GR_DOCUMENT Then
                _ISN_GR_DOCUMENT_Control = New eqDocument
                _ISN_GR_DOCUMENT_Control.ID = "ISN_GR_DOCUMENT"
                _ISN_GR_DOCUMENT_Control.DocTypeURL = "GR_DOCUMENT.ascx"
                _ISN_GR_DOCUMENT_Control.ListDocMode = eqBaseContainer.ListDocModes.NewWindowAndSelect
                _ISN_GR_DOCUMENT_Control.LoadDocMode = IIf(Level > ArchiveCrumbsLevel.ARCHIVE, eqBaseContainer.LoadDocModes.NewWindow, eqBaseContainer.LoadDocModes.NewWindowAndSelect)
                _ISN_GR_DOCUMENT_Control.ShowingFields = "NAME, ISN_DOCUMENT, ISN_ARCHIVE"
                _ISN_GR_DOCUMENT_Control.ShowingFieldsFormat = "Госреестр документ {0}"
                _ISN_GR_DOCUMENT_Control.KeyName = "ISN_DOCUMENT"
                Controls.Add(_ISN_GR_DOCUMENT_Control)
            End If
            If Level >= ArchiveCrumbsLevel.GR_DOC_DESCRIPT Then
                _ISN_GR_DOC_DESCRIPT_Control = New eqDocument
                _ISN_GR_DOC_DESCRIPT_Control.ID = "ISN_GR_DOC_DESCRIPT"
                _ISN_GR_DOC_DESCRIPT_Control.DocTypeURL = "GR_DOC_DESCRIPT.ascx"
                _ISN_GR_DOC_DESCRIPT_Control.ListDocMode = eqBaseContainer.ListDocModes.NewWindowAndSelect
                _ISN_GR_DOC_DESCRIPT_Control.LoadDocMode = IIf(Level > ArchiveCrumbsLevel.ARCHIVE, eqBaseContainer.LoadDocModes.NewWindow, eqBaseContainer.LoadDocModes.NewWindowAndSelect)
                _ISN_GR_DOC_DESCRIPT_Control.ShowingFields = "NAME, DOC_DESCRIPT, ISN_DOCUMENT, ISN_ARCHIVE"
                _ISN_GR_DOC_DESCRIPT_Control.ShowingFieldsFormat = "Госреестр документ {0}"
                _ISN_GR_DOC_DESCRIPT_Control.KeyName = "ISN_GR_DOC_DESCRIPT"
                Controls.Add(_ISN_GR_DOC_DESCRIPT_Control)
            End If
        End Sub

        Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
#If CFC Then
            If _ISN_AuthorizedDep_Control IsNot Nothing Then
                writer.WriteEncodedText("Уполномоченный орган: ")
                _ISN_AuthorizedDep_Control.RenderControl(writer)
            End If
#End If
            If _ISN_ARCHIVE_Control IsNot Nothing Then
                If _ISN_AuthorizedDep_Control IsNot Nothing Then
                    If _ISN_ARCHIVE_Control.Visible Then
                        writer.WriteEncodedText(" >> ")
                    End If
                End If
                _ISN_ARCHIVE_Control.RenderControl(writer)
            End If

            If _ISN_FUND_Control IsNot Nothing Then
                If _ISN_FUND_Control.Visible Then
                    writer.WriteEncodedText(" >> ")
                End If
                _ISN_FUND_Control.RenderControl(writer)
            End If

            If _ISN_INVENTORY_Control IsNot Nothing Then
                If _ISN_INVENTORY_Control.Visible Then
                    writer.WriteEncodedText(" >> ")
                End If
                _ISN_INVENTORY_Control.RenderControl(writer)
            End If

            If _ISN_UNIT_Control IsNot Nothing Then
                If _ISN_UNIT_Control.Visible Then
                    writer.WriteEncodedText(" >> ")
                End If
                _ISN_UNIT_Control.RenderControl(writer)
            End If
        End Sub

        Protected Overrides Sub OnPreRender(ByVal e As System.EventArgs)
            MyBase.OnPreRender(e)

            If _ISN_AuthorizedDep_Control IsNot Nothing AndAlso _ISN_ARCHIVE_Control IsNot Nothing Then
                If Not IsDBNull(_ISN_ARCHIVE_Control.DataSetHeaderDataRow("ISN_AuthorizedDep")) Then
                    _ISN_AuthorizedDep_Control.KeyValue = _ISN_ARCHIVE_Control.DataSetHeaderDataRow("ISN_AuthorizedDep")
                End If
            End If
        End Sub

    End Class

End Namespace
