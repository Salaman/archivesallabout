﻿Public Class BasePage
    Inherits eqBasePage

    Public Overrides Function GetDocURL(ByVal DocTypeURL As String, ByVal DocID As System.Guid, ByVal Modal As Boolean, Optional ByVal OptionalQueryString As String = "") As String
        PasstrouthQueryString(DocTypeURL, OptionalQueryString)
        Return MyBase.GetDocURL(DocTypeURL, DocID, Modal, OptionalQueryString)
    End Function

    Public Overrides Function GetListDocURL(ByVal DocTypeURL As String, ByVal Modal As Boolean, Optional ByVal OptionalQueryString As String = "") As String
        PasstrouthQueryString(DocTypeURL, OptionalQueryString)
        Return MyBase.GetListDocURL(DocTypeURL, Modal, OptionalQueryString)
    End Function

    Public Overrides Function GetNewDocURL(ByVal DocTypeURL As String, ByVal Modal As Boolean, Optional ByVal OptionalQueryString As String = "") As String
        PasstrouthQueryString(DocTypeURL, OptionalQueryString)
        Return MyBase.GetNewDocURL(DocTypeURL, Modal, OptionalQueryString)
    End Function

    Private Sub PasstrouthQueryString(ByVal DocTypeURL As String, ByRef OptionalQueryString As String)
        If DocTypeURL = Me.DocTypeURL And String.IsNullOrEmpty(OptionalQueryString) Then
            Dim SystemKeys = New List(Of String)(New String() {"DOCID", "LISTDOC", "DOCTYPEURL", "PARENTID", "LOCKID"})
            Dim ListDoc = Request.QueryString("LISTDOC")
            If ListDoc IsNot Nothing AndAlso ListDoc.ToUpper = "TRUE" Then
                SystemKeys.Add("LISTID")
            End If
            For Each key In Request.QueryString.AllKeys
                If Not SystemKeys.Contains(key.ToUpper) Then
                    OptionalQueryString += "&" & key & "=" & Request.QueryString(key)
                End If
            Next
        End If
    End Sub

End Class
