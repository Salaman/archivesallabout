﻿Public MustInherit Class BaseListDoc
    Inherits eqBaseListDoc

#Region "Свойства"

    Protected ReadOnly Property MyBaseContainer() As BaseContainer
        Get
            Return DirectCast(Me.BaseContainer, BaseContainer)
        End Get
    End Property

#End Region

#Region "События"

    Private Sub Page_ModalDialogClosed1(ByVal Argument As Equipage.WebUI.eqModalDialogArgument) Handles Me.ModalDialogClosed
        Select Case Argument.Argument
            Case "SetupView", "SelectView", "SetupViewSimple"
                MyBaseContainer.ListSorting.Clear()
                Refresh()
        End Select
    End Sub

    Private Sub Page_PreRender1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Select Case DocTypeURL
            Case "FUND.ascx"
                SetTitle("Список фондов")
            Case "INVENTORY.ascx"
                SetTitle("Список описей")
            Case "INVENTORYSTRUCTURE.ascx"
                SetTitle("Разделы описи")
            Case "UNIT.ascx", "UNIT2.ascx"
                SetTitle("Список ед.хр./ед.уч.")
            Case "DEPOSIT.ascx"
                SetTitle("Список россыпей")
            Case Else
                SetTitle("Список " & DocType.DocTypeName)
        End Select
    End Sub

#End Region

#Region "Методы"

    Private Sub ViewRefresh()
        LoadListDocMenu()
        SetTitle()
        MyBase.LoadListDocDataTable()
        SetListDocData()
    End Sub

    Private Sub OverrideSorting(ByVal OrderStringBuilder As Text.StringBuilder)
        ' Reset Order
        OrderStringBuilder.Remove(0, OrderStringBuilder.Length)
        ' User sorting
        For Each R In MyBaseContainer.ListSorting
            Dim FieldID = DirectCast(R.First, Guid)
            Dim GGR = ViewGrouppingFields.FirstOrDefault(Function(GR) GR("FieldID") = FieldID)
            If GGR Is Nothing Then
                Dim FieldAlias = "[" & FieldID.ToString("n") & "]"
                Dim FieldVariable = "@" & FieldID.ToString("n")
                Dim FieldInfo = DocType.GetFieldInfo(FieldID)
                Dim ListDocFieldInfo = DocType.GetListDocFieldInfo(FieldID)
                Dim FieldTable = "[" & FieldInfo("TableName") & "]"
                Dim FieldName = "[" & FieldInfo("FieldName") & "]"
                Dim FieldFullName = FieldTable & "." & FieldName
                ' Order
                Dim SortDirection As String = R.Second.ToString
                ' Write
                If OrderStringBuilder.Length = 0 Then
                    OrderStringBuilder.AppendLine("ORDER BY")
                    OrderStringBuilder.Append("   ")
                Else
                    OrderStringBuilder.Append("   ,")
                End If
                ' DocTypeURL
                If ListDocFieldInfo("DocTypeURL").ToString <> "" AndAlso ListDocFieldInfo("ShowingFields").ToString <> "" Then
                    Dim FieldDocTypeURL = ListDocFieldInfo("DocTypeURL").ToString
                    Dim FieldDocType = AppSettings.GetDocType(FieldDocTypeURL)
                    Dim FieldShowingFields = ListDocFieldInfo("ShowingFields").ToString
                    Dim FieldShowingField = FieldShowingFields.Split(New Char() {";", ","}).SingleOrDefault
                    If FieldShowingField IsNot Nothing Then
                        Dim FieldShowingFieldAlias = "[" & FieldID.ToString("n") & "_" & FieldDocType.GetHeaderTableName & "_" & FieldShowingField & "]"
                        FieldAlias = FieldShowingFieldAlias
                    End If
                End If
                ' SourceTable
                If ListDocFieldInfo("SourceTable").ToString <> "" AndAlso ListDocFieldInfo("KeyField").ToString <> "" AndAlso ListDocFieldInfo("DisplayField").ToString <> "" Then
                    Dim SourceTable = ListDocFieldInfo("SourceTable").ToString
                    Dim DisplayField = ListDocFieldInfo("DisplayField").ToString
                    Dim DisplayFieldAlias = "[" & FieldID.ToString("n") & "_" & SourceTable & "_" & DisplayField & "]"
                    FieldAlias = DisplayFieldAlias
                End If
                ' Apply
                OrderStringBuilder.AppendLine(FieldAlias & " " & SortDirection)
            End If
        Next
        ' Group sorting
        For Each GR As DataRowView In ViewGrouppingFields
            Dim FieldID = DirectCast(GR("FieldID"), Guid)
            Dim LS = MyBaseContainer.ListSorting.FirstOrDefault(Function(F) F.First = FieldID)
            If LS Is Nothing Then
                Dim FieldAlias = "[" & FieldID.ToString("n") & "]"
                Dim FieldVariable = "@" & FieldID.ToString("n")
                Dim FieldInfo = DocType.GetFieldInfo(FieldID)
                Dim ListDocFieldInfo = DocType.GetListDocFieldInfo(FieldID)
                Dim FieldTable = "[" & FieldInfo("TableName") & "]"
                Dim FieldName = "[" & FieldInfo("FieldName") & "]"
                Dim FieldFullName = FieldTable & "." & FieldName
                ' Order
                Dim SortDirection As String = "ASC"
                Dim R = ViewSortingFields.FirstOrDefault(Function(SR) SR("FieldID") = FieldID)
                If R IsNot Nothing Then
                    SortDirection = R("SortDirection").ToString
                End If
                ' Write
                If OrderStringBuilder.Length = 0 Then
                    OrderStringBuilder.AppendLine("ORDER BY")
                    OrderStringBuilder.Append("   ")
                Else
                    OrderStringBuilder.Append("   ,")
                End If
                ' DocTypeURL
                If ListDocFieldInfo("DocTypeURL").ToString <> "" AndAlso ListDocFieldInfo("ShowingFields").ToString <> "" Then
                    Dim FieldDocTypeURL = ListDocFieldInfo("DocTypeURL").ToString
                    Dim FieldDocType = AppSettings.GetDocType(FieldDocTypeURL)
                    Dim FieldShowingFields = ListDocFieldInfo("ShowingFields").ToString
                    Dim FieldShowingField = FieldShowingFields.Split(New Char() {";", ","}).SingleOrDefault
                    If FieldShowingField IsNot Nothing Then
                        Dim FieldShowingFieldAlias = "[" & FieldID.ToString("n") & "_" & FieldDocType.GetHeaderTableName & "_" & FieldShowingField & "]"
                        FieldAlias = FieldShowingFieldAlias
                    End If
                End If
                ' SourceTable
                If ListDocFieldInfo("SourceTable").ToString <> "" AndAlso ListDocFieldInfo("KeyField").ToString <> "" AndAlso ListDocFieldInfo("DisplayField").ToString <> "" Then
                    Dim SourceTable = ListDocFieldInfo("SourceTable").ToString
                    Dim DisplayField = ListDocFieldInfo("DisplayField").ToString
                    Dim DisplayFieldAlias = "[" & FieldID.ToString("n") & "_" & SourceTable & "_" & DisplayField & "]"
                    FieldAlias = DisplayFieldAlias
                End If
                ' Apply
                OrderStringBuilder.AppendLine(FieldAlias & " " & SortDirection)
            End If
        Next
        For Each R As DataRowView In ViewSortingFields
            Dim FieldID = DirectCast(R("FieldID"), Guid)
            Dim LS = MyBaseContainer.ListSorting.FirstOrDefault(Function(F) F.First = FieldID)
            If LS Is Nothing Then
                Dim GGR = ViewGrouppingFields.FirstOrDefault(Function(GR) GR("FieldID") = FieldID)
                If GGR Is Nothing Then
                    Dim FieldAlias = "[" & FieldID.ToString("n") & "]"
                    Dim FieldVariable = "@" & FieldID.ToString("n")
                    Dim FieldInfo = DocType.GetFieldInfo(FieldID)
                    Dim ListDocFieldInfo = DocType.GetListDocFieldInfo(FieldID)
                    Dim FieldTable = "[" & FieldInfo("TableName") & "]"
                    Dim FieldName = "[" & FieldInfo("FieldName") & "]"
                    Dim FieldFullName = FieldTable & "." & FieldName
                    ' Order
                    Dim SortDirection As String = R("SortDirection").ToString
                    ' Write
                    If OrderStringBuilder.Length = 0 Then
                        OrderStringBuilder.AppendLine("ORDER BY")
                        OrderStringBuilder.Append("   ")
                    Else
                        OrderStringBuilder.Append("   ,")
                    End If
                    ' DocTypeURL
                    If ListDocFieldInfo("DocTypeURL").ToString <> "" AndAlso ListDocFieldInfo("ShowingFields").ToString <> "" Then
                        Dim FieldDocTypeURL = ListDocFieldInfo("DocTypeURL").ToString
                        Dim FieldDocType = AppSettings.GetDocType(FieldDocTypeURL)
                        Dim FieldShowingFields = ListDocFieldInfo("ShowingFields").ToString
                        Dim FieldShowingField = FieldShowingFields.Split(New Char() {";", ","}).SingleOrDefault
                        If FieldShowingField IsNot Nothing Then
                            Dim FieldShowingFieldAlias = "[" & FieldID.ToString("n") & "_" & FieldDocType.GetHeaderTableName & "_" & FieldShowingField & "]"
                            FieldAlias = FieldShowingFieldAlias
                        End If
                    End If
                    ' SourceTable
                    If ListDocFieldInfo("SourceTable").ToString <> "" AndAlso ListDocFieldInfo("KeyField").ToString <> "" AndAlso ListDocFieldInfo("DisplayField").ToString <> "" Then
                        Dim SourceTable = ListDocFieldInfo("SourceTable").ToString
                        Dim DisplayField = ListDocFieldInfo("DisplayField").ToString
                        Dim DisplayFieldAlias = "[" & FieldID.ToString("n") & "_" & SourceTable & "_" & DisplayField & "]"
                        FieldAlias = DisplayFieldAlias
                    End If
                    ' Apply
                    OrderStringBuilder.AppendLine(FieldAlias & " " & SortDirection)
                End If
            End If
        Next
    End Sub

#End Region

#Region "Ядро"

    Public Overrides Property TreeFilterMode() As Equipage.WebUI.eqBaseListDoc.TreeFilterModes
        Get
            Return TreeFilterModes.MatchesAndTreeAfter
        End Get
        Set(ByVal value As Equipage.WebUI.eqBaseListDoc.TreeFilterModes)
            MyBase.TreeFilterMode = value
        End Set
    End Property

    Public Overrides Sub AppendAdditionalStrings(ByVal SelectStringBuilder As System.Text.StringBuilder, ByVal FromStringBuilder As System.Text.StringBuilder, ByVal WhereStringBuilder As System.Text.StringBuilder, ByVal OrderStringBuilder As System.Text.StringBuilder, ByVal SqlCommand As System.Data.SqlClient.SqlCommand)
        ' Модифицируем запрос в зависимости от типа документа и параметров URL
        Select Case DocTypeURL
            Case "ACT.ascx"
                If Not String.IsNullOrEmpty(Request.QueryString("FUND")) Then
                    FromStringBuilder.AppendLine("JOIN tblFUND ON tblFUND.ISN_FUND=[" & DocType.GetHeaderTableName & "].ISN_FUND")
                    WhereStringBuilder.AppendLine("AND tblFUND.ID=@FUND")
                    SqlCommand.Parameters.AddWithValue("@FUND", New Guid(Request.QueryString("FUND")))
                End If
                If Not String.IsNullOrEmpty(Request.QueryString("INVENTORY")) Then
                    FromStringBuilder.AppendLine("JOIN tblFUND ON tblFUND.ISN_FUND=[" & DocType.GetHeaderTableName & "].ISN_FUND")
                    FromStringBuilder.AppendLine("JOIN tblINVENTORY ON tblINVENTORY.ISN_FUND=tblFUND.ISN_FUND")
                    WhereStringBuilder.AppendLine("AND tblINVENTORY.ID=@INVENTORY")
                    SqlCommand.Parameters.AddWithValue("@INVENTORY", New Guid(Request.QueryString("INVENTORY")))
                End If
                If Not String.IsNullOrEmpty(Request.QueryString("DEPOSIT")) Then
                    FromStringBuilder.AppendLine("JOIN tblFUND ON tblFUND.ISN_FUND=[" & DocType.GetHeaderTableName & "].ISN_FUND")
                    FromStringBuilder.AppendLine("JOIN tblDEPOSIT ON tblDEPOSIT.ISN_FUND=tblFUND.ISN_FUND")
                    WhereStringBuilder.AppendLine("AND tblDEPOSIT.ID=@DEPOSIT")
                    SqlCommand.Parameters.AddWithValue("@DEPOSIT", New Guid(Request.QueryString("DEPOSIT")))
                End If
                If Not String.IsNullOrEmpty(Request.QueryString("UNIT")) Then
                    FromStringBuilder.AppendLine("JOIN tblFUND ON tblFUND.ISN_FUND=[" & DocType.GetHeaderTableName & "].ISN_FUND")
                    FromStringBuilder.AppendLine("JOIN tblINVENTORY ON tblINVENTORY.ISN_FUND=tblFUND.ISN_FUND")
                    FromStringBuilder.AppendLine("JOIN tblUNIT ON tblUNIT.ISN_INVENTORY=tblINVENTORY.ISN_INVENTORY")
                    WhereStringBuilder.AppendLine("AND tblUNIT.ID=@UNIT")
                    SqlCommand.Parameters.AddWithValue("@UNIT", New Guid(Request.QueryString("UNIT")))
                End If
                Select Case Request.QueryString("DocTypeURL")
                    Case "INVENTORY.ascx"
                        WhereStringBuilder.AppendLine("AND [" & DocType.GetHeaderTableName & "].ACT_OBJ=702")
                    Case "UNIT.ascx"
                        WhereStringBuilder.AppendLine("AND ([" & DocType.GetHeaderTableName & "].ACT_OBJ=703")
                        WhereStringBuilder.AppendLine("OR [" & DocType.GetHeaderTableName & "].ACT_OBJ=704)")
                    Case "DEPOSIT.ascx"
                        WhereStringBuilder.AppendLine("AND [" & DocType.GetHeaderTableName & "].ACT_OBJ=707")
                End Select
                If Not String.IsNullOrEmpty(Request.QueryString("ACT_OBJ")) Then
                    WhereStringBuilder.AppendLine("AND [" & DocType.GetHeaderTableName & "].ACT_OBJ=" & Request.QueryString("ACT_OBJ"))
                End If
            Case "DOCUMENT.ascx"
                If Not String.IsNullOrEmpty(Request.QueryString("UNIT")) Then
                    FromStringBuilder.AppendLine("JOIN tblUNIT ON tblUNIT.ISN_UNIT=[" & DocType.GetHeaderTableName & "].ISN_UNIT")
                    WhereStringBuilder.AppendLine("AND tblUNIT.ID=@UNIT")
                    SqlCommand.Parameters.AddWithValue("@UNIT", New Guid(Request.QueryString("UNIT")))
                End If
            Case "DEPOSIT.ascx"
                If Not String.IsNullOrEmpty(Request.QueryString("FUND")) Then
                    FromStringBuilder.AppendLine("JOIN tblFUND ON tblFUND.ISN_FUND=[" & DocType.GetHeaderTableName & "].ISN_FUND")
                    WhereStringBuilder.AppendLine("AND tblFUND.ID=@FUND")
                    SqlCommand.Parameters.AddWithValue("@FUND", New Guid(Request.QueryString("FUND")))
                End If
            Case "UNIT.ascx"
                If Not String.IsNullOrEmpty(Request.QueryString("INVENTORY")) Then
                    FromStringBuilder.AppendLine("JOIN tblINVENTORY ON tblINVENTORY.ISN_INVENTORY=[" & DocType.GetHeaderTableName & "].ISN_INVENTORY")
                    WhereStringBuilder.AppendLine("AND tblINVENTORY.ID=@INVENTORY")
                    SqlCommand.Parameters.AddWithValue("@INVENTORY", New Guid(Request.QueryString("INVENTORY")))
                End If
                If Not String.IsNullOrEmpty(Request.QueryString("FUND")) Then
                    FromStringBuilder.AppendLine("JOIN tblINVENTORY ON tblINVENTORY.ISN_INVENTORY=[" & DocType.GetHeaderTableName & "].ISN_INVENTORY")
                    FromStringBuilder.AppendLine("JOIN tblFUND ON tblFUND.ISN_FUND=tblINVENTORY.ISN_FUND")
                    WhereStringBuilder.AppendLine("AND tblFUND.ID=@FUND")
                    SqlCommand.Parameters.AddWithValue("@FUND", New Guid(Request.QueryString("FUND")))
                End If
                If Not String.IsNullOrEmpty(Request.QueryString("INVENTORYSTRUCTURE")) Then
                    FromStringBuilder.AppendLine("JOIN tblINVENTORY_STRUCTURE ON tblINVENTORY_STRUCTURE.ISN_INVENTORY_CLS=[" & DocType.GetHeaderTableName & "].ISN_INVENTORY_CLS")
                    WhereStringBuilder.AppendLine("AND tblINVENTORY_STRUCTURE.ID=@INVENTORYSTRUCTURE")
                    SqlCommand.Parameters.AddWithValue("@INVENTORYSTRUCTURE", New Guid(Request.QueryString("INVENTORYSTRUCTURE")))
                End If
                If BasePage.IsModalDialog AndAlso BasePage.ModalDialogArgument.ArgumentObject IsNot Nothing AndAlso BasePage.ModalDialogArgument.ArgumentObject.ToString.StartsWith("UNIT_KIND=") Then
                    WhereStringBuilder.AppendLine("AND [" & DocType.GetHeaderTableName & "].UNIT_KIND<>@UNIT_KIND AND [" & DocType.GetHeaderTableName & "].ISN_HIGH_UNIT IS NULL")
                    SqlCommand.Parameters.AddWithValue("@UNIT_KIND", BasePage.ModalDialogArgument.ArgumentObject.ToString.Substring("UNIT_KIND=".Length))
                End If
            Case "UNIT2.ascx"
                If Not String.IsNullOrEmpty(Request.QueryString("INVENTORY")) Then
                    FromStringBuilder.AppendLine("JOIN tblINVENTORY ON tblINVENTORY.ISN_INVENTORY=[" & DocType.GetHeaderTableName & "].ISN_INVENTORY")
                    WhereStringBuilder.AppendLine("AND tblINVENTORY.ID=@INVENTORY")
                    SqlCommand.Parameters.AddWithValue("@INVENTORY", New Guid(Request.QueryString("INVENTORY")))
                End If
                If Not String.IsNullOrEmpty(Request.QueryString("FUND")) Then
                    FromStringBuilder.AppendLine("JOIN tblINVENTORY ON tblINVENTORY.ISN_INVENTORY=[" & DocType.GetHeaderTableName & "].ISN_INVENTORY")
                    FromStringBuilder.AppendLine("JOIN tblFUND ON tblFUND.ISN_FUND=tblINVENTORY.ISN_FUND")
                    WhereStringBuilder.AppendLine("AND tblFUND.ID=@FUND")
                    SqlCommand.Parameters.AddWithValue("@FUND", New Guid(Request.QueryString("FUND")))
                End If
                If Not String.IsNullOrEmpty(Request.QueryString("INVENTORYSTRUCTURE")) Then
                    FromStringBuilder.AppendLine("JOIN tblINVENTORY_STRUCTURE ON tblINVENTORY_STRUCTURE.ISN_INVENTORY_CLS=[" & DocType.GetHeaderTableName & "].ISN_INVENTORY_CLS")
                    WhereStringBuilder.AppendLine("AND tblINVENTORY_STRUCTURE.ID=@INVENTORYSTRUCTURE")
                    SqlCommand.Parameters.AddWithValue("@INVENTORYSTRUCTURE", New Guid(Request.QueryString("INVENTORYSTRUCTURE")))
                End If
                If BasePage.IsModalDialog AndAlso BasePage.ModalDialogArgument.ArgumentObject IsNot Nothing AndAlso BasePage.ModalDialogArgument.ArgumentObject.ToString.StartsWith("UNIT_KIND=") Then
                    WhereStringBuilder.AppendLine("AND [" & DocType.GetHeaderTableName & "].UNIT_KIND<>@UNIT_KIND AND [" & DocType.GetHeaderTableName & "].ISN_HIGH_UNIT IS NULL")
                    SqlCommand.Parameters.AddWithValue("@UNIT_KIND", BasePage.ModalDialogArgument.ArgumentObject.ToString.Substring("UNIT_KIND=".Length))
                End If
            Case "INVENTORY.ascx"
                If Not String.IsNullOrEmpty(Request.QueryString("FUND")) Then
                    FromStringBuilder.AppendLine("JOIN tblFUND ON tblFUND.ISN_FUND=[" & DocType.GetHeaderTableName & "].ISN_FUND")
                    WhereStringBuilder.AppendLine("AND tblFUND.ID=@FUND")
                    SqlCommand.Parameters.AddWithValue("@FUND", New Guid(Request.QueryString("FUND")))
                End If
            Case "INVENTORY_ACT.ascx"
                If Not String.IsNullOrEmpty(Request.QueryString("FUND")) Then
                    FromStringBuilder.AppendLine("JOIN tblFUND ON tblFUND.ISN_FUND=[" & DocType.GetHeaderTableName & "].ISN_FUND")
                    WhereStringBuilder.AppendLine("AND tblFUND.ID=@FUND")
                    SqlCommand.Parameters.AddWithValue("@FUND", New Guid(Request.QueryString("FUND")))
                End If
            Case "INVENTORYSTRUCTURE.ascx"
                If Not String.IsNullOrEmpty(Request.QueryString("INVENTORY")) Then
                    FromStringBuilder.AppendLine("LEFT JOIN tblARCHIVE ON tblARCHIVE.ISN_ARCHIVE=[" & DocType.GetHeaderTableName & "].ISN_ARCHIVE_ROOT")
                    FromStringBuilder.AppendLine("LEFT JOIN tblFUND ON tblFUND.ISN_FUND=[" & DocType.GetHeaderTableName & "].ISN_FUND_ROOT")
                    FromStringBuilder.AppendLine("LEFT JOIN tblINVENTORY ON tblINVENTORY.ISN_INVENTORY=[" & DocType.GetHeaderTableName & "].ISN_INVENTORY_ROOT")
                    FromStringBuilder.AppendLine("LEFT JOIN tblFUND refFUND ON refFUND.ISN_ARCHIVE=tblARCHIVE.ISN_ARCHIVE")
                    FromStringBuilder.AppendLine("LEFT JOIN tblINVENTORY refINVENTORY ON refINVENTORY.ISN_FUND=isnull(tblFUND.ISN_FUND,refFUND.ISN_FUND)")
                    WhereStringBuilder.AppendLine("AND (tblINVENTORY.ID=@INVENTORY OR refINVENTORY.ID=@INVENTORY)")
                    WhereStringBuilder.AppendLine("AND dbo.fn_GetArchiveID(isnull(tblINVENTORY.ISN_INVENTORY,refINVENTORY.ISN_INVENTORY)) = dbo.fn_GetArchiveID([" & DocType.GetHeaderTableName & "].ISN_INVENTORY_CLS)")
                    SqlCommand.Parameters.AddWithValue("@INVENTORY", New Guid(Request.QueryString("INVENTORY")))
                End If
                If Not String.IsNullOrEmpty(Request.QueryString("FUND")) Then
                    FromStringBuilder.AppendLine("LEFT JOIN tblARCHIVE ON tblARCHIVE.ISN_ARCHIVE=[" & DocType.GetHeaderTableName & "].ISN_ARCHIVE_ROOT")
                    FromStringBuilder.AppendLine("LEFT JOIN tblFUND ON tblFUND.ISN_FUND=[" & DocType.GetHeaderTableName & "].ISN_FUND_ROOT")
                    FromStringBuilder.AppendLine("LEFT JOIN tblFUND refFUND ON refFUND.ISN_ARCHIVE=tblARCHIVE.ISN_ARCHIVE")
                    WhereStringBuilder.AppendLine("AND (tblFUND.ID=@FUND OR refFUND.ID=@FUND)")
                    WhereStringBuilder.AppendLine("AND dbo.fn_GetArchiveID(isnull(tblFUND.ISN_FUND,refFUND.ISN_FUND)) = dbo.fn_GetArchiveID([" & DocType.GetHeaderTableName & "].ISN_INVENTORY_CLS)")
                    SqlCommand.Parameters.AddWithValue("@FUND", New Guid(Request.QueryString("FUND")))
                End If
                If Not String.IsNullOrEmpty(Request.QueryString("ARCHIVE")) Then
                    FromStringBuilder.AppendLine("LEFT JOIN tblARCHIVE ON tblARCHIVE.ISN_ARCHIVE=[" & DocType.GetHeaderTableName & "].ISN_ARCHIVE_ROOT")
                    WhereStringBuilder.AppendLine("AND tblARCHIVE.ID=@ARCHIVE")
                    WhereStringBuilder.AppendLine("AND dbo.fn_GetArchiveID(tblARCHIVE.ISN_ARCHIVE) = dbo.fn_GetArchiveID([" & DocType.GetHeaderTableName & "].ISN_INVENTORY_CLS)")
                    SqlCommand.Parameters.AddWithValue("@ARCHIVE", New Guid(Request.QueryString("ARCHIVE")))
                End If
                If BasePage.IsModalDialog AndAlso BasePage.ModalDialogArgument.Argument = "SelectDoc" Then
                    If BasePage.ModalDialogArgument.ArgumentObject IsNot Nothing Then
                        FromStringBuilder.AppendLine("LEFT JOIN tblARCHIVE ON tblARCHIVE.ISN_ARCHIVE=[" & DocType.GetHeaderTableName & "].ISN_ARCHIVE_ROOT")
                        FromStringBuilder.AppendLine("LEFT JOIN tblFUND ON tblFUND.ISN_FUND=[" & DocType.GetHeaderTableName & "].ISN_FUND_ROOT")
                        FromStringBuilder.AppendLine("LEFT JOIN tblINVENTORY ON tblINVENTORY.ISN_INVENTORY=[" & DocType.GetHeaderTableName & "].ISN_INVENTORY_ROOT")
                        FromStringBuilder.AppendLine("LEFT JOIN tblFUND refFUND ON refFUND.ISN_ARCHIVE=tblARCHIVE.ISN_ARCHIVE")
                        FromStringBuilder.AppendLine("LEFT JOIN tblINVENTORY refINVENTORY ON refINVENTORY.ISN_FUND=isnull(tblFUND.ISN_FUND,refFUND.ISN_FUND)")
                        WhereStringBuilder.AppendLine("AND (tblINVENTORY.ID=@INVENTORY OR refINVENTORY.ID=@INVENTORY)")
                        WhereStringBuilder.AppendLine("AND dbo.fn_GetArchiveID(isnull(tblINVENTORY.ISN_INVENTORY,refINVENTORY.ISN_INVENTORY)) = dbo.fn_GetArchiveID([" & DocType.GetHeaderTableName & "].ISN_INVENTORY_CLS)")
                        SqlCommand.Parameters.AddWithValue("@INVENTORY", New Guid(BasePage.ModalDialogArgument.ArgumentObject.ToString))
                    End If
                End If
            Case "FUND.ascx"
                If Not String.IsNullOrEmpty(Request.QueryString("ARCHIVE")) Then
                    FromStringBuilder.AppendLine("JOIN tblARCHIVE ON tblARCHIVE.ISN_ARCHIVE=[" & DocType.GetHeaderTableName & "].ISN_ARCHIVE")
                    WhereStringBuilder.AppendLine("AND tblARCHIVE.ID=@ARCHIVE")
                    SqlCommand.Parameters.AddWithValue("@ARCHIVE", New Guid(Request.QueryString("ARCHIVE")))
                End If
                If BasePage.IsModalDialog AndAlso BasePage.ModalDialogArgument.Argument = "SelectDoc" Then
                    If BasePage.ModalDialogArgument.ArgumentObject IsNot Nothing Then
                        WhereStringBuilder.AppendLine("AND ")
                        WhereStringBuilder.AppendLine(BasePage.ModalDialogArgument.ArgumentObject.ToString)
                    End If
                End If
            Case "DOC_KIND_CL.ascx"
                If BasePage.IsModalDialog AndAlso BasePage.ModalDialogArgument.Argument = "SelectDoc" Then
                    If BasePage.ModalDialogArgument.ArgumentObject IsNot Nothing Then
                        WhereStringBuilder.AppendLine("AND [" & DocType.GetHeaderTableName & "].ID IN (" & BasePage.ModalDialogArgument.ArgumentObject.ToString & ")")
                    End If
                End If
            Case "CLS701.ascx"
                If BasePage.IsModalDialog AndAlso BasePage.ModalDialogArgument.ArgumentObject IsNot Nothing Then
                    Select Case BasePage.ModalDialogArgument.ArgumentObject.ToString
                        Case "TEMATIK"
                            WhereStringBuilder.AppendLine("AND [" & DocType.GetHeaderTableName & "].ISN_HIGH_CLS = 35392")
                        Case "PERSONA"
                            WhereStringBuilder.AppendLine("AND [" & DocType.GetHeaderTableName & "].ISN_HIGH_CLS = 35393")
                        Case "GEOGRAF"
                            WhereStringBuilder.AppendLine("AND [" & DocType.GetHeaderTableName & "].ISN_HIGH_CLS = 35394")
                        Case "WORD"
                            WhereStringBuilder.AppendLine("AND [" & DocType.GetHeaderTableName & "].ISN_HIGH_CLS = 35395")
                    End Select
                End If
            Case "SUBJECT_CL.ascx"
                If Not String.IsNullOrEmpty(Request.QueryString("SUBJECT")) Then
                    ' FromStringBuilder.AppendLine("JOIN GR_tblFILE ON GR_tblFILE.ISN_FILE=[" & DocType.GetHeaderTableName & "].ISN_FILE")
                    'WhereStringBuilder.AppendLine("AND ID_FILE=@FILE")
                    'SqlCommand.Parameters.AddWithValue("@FILE", New Guid(Request.QueryString("FILE")))
                End If
            Case "GR_ARCHIVE.ascx"
                'If InStr(Request.QueryString.ToString, "ARCHIVE_LEVEL=a") > 0 Then
                If Not String.IsNullOrEmpty(Request.QueryString("ARCHIVE_LEVEL")) Then
                    WhereStringBuilder.AppendLine("AND ARCHIVE_LEVEL='a'")
                End If
                If InStr(Request.QueryString.ToString, "ARCHIVE_LEVEL_MUNICIPALITY") > 0 Then
                    WhereStringBuilder.AppendLine("AND ARCHIVE_LEVEL='c'")
                End If
            Case "ARCHIVE.ascx"
                If Not String.IsNullOrEmpty(Request.QueryString("ARCHIVE_LEVEL")) Then
                    WhereStringBuilder.AppendLine("AND ARCHIVE_LEVEL='a'")
                End If
                If InStr(Request.QueryString.ToString, "ARCHIVE_LEVEL_MUNICIPALITY") > 0 Then
                    WhereStringBuilder.AppendLine("AND ARCHIVE_LEVEL='c'")
                End If
            Case "GR_DOC_DESCRIPT_LIST.ascx"
                If Not String.IsNullOrEmpty(Request.QueryString("FILE")) Then
                    WhereStringBuilder.AppendLine("AND ID_FILE=@FILE")
                    SqlCommand.Parameters.AddWithValue("@FILE", New Guid(Request.QueryString("FILE")))
                End If
                If Not String.IsNullOrEmpty(Request.QueryString("GR_DOCUMENT")) Then
                    ' FromStringBuilder.AppendLine("JOIN GR_tblFILE ON GR_tblFILE.ISN_FILE=[" & DocType.GetHeaderTableName & "].ISN_FILE")
                    WhereStringBuilder.AppendLine("AND DocID=@GR_DOCUMENT")
                    SqlCommand.Parameters.AddWithValue("@GR_DOCUMENT", New Guid(Request.QueryString("GR_DOCUMENT")))
                End If
        End Select
        If MyBaseContainer IsNot Nothing Then
            OverrideSorting(OrderStringBuilder)
        End If
        MyBase.AppendAdditionalStrings(SelectStringBuilder, FromStringBuilder, WhereStringBuilder, OrderStringBuilder, SqlCommand)
    End Sub

    Public Overrides Sub LoadListDocDataTable()
        ' Переходим на представление
        If Not String.IsNullOrEmpty(Request.QueryString("ViewID")) Then
            If ViewState("ViewID") Is Nothing Then
                Dim ViewID = New Guid(Request.QueryString("ViewID"))
                LoadView(ViewID)
                ViewRefresh()
                ViewState("ViewID") = ViewID
            End If
        End If

        ' Для карточки ед.хр.уч. будем шаманить
        If DocType.GetHeaderTableName.ToUpper = "VUNIT" Then
            Dim UNIT_KIND_FiledID = New Guid("fe93a0ab-ddf2-4612-a98d-32b61794c333")
            Dim Filter1 = ViewFields.First(Function(F) F.Row("FieldID") = UNIT_KIND_FiledID)("Filter1")

            If Filter1 = "703" Then
                LoadView(New Guid("13C25E8D-7B6F-4962-81C5-15D515836442"))
                ViewRefresh()
                Exit Sub
            ElseIf Filter1 = "704" Then
                LoadView(New Guid("85752201-5C1F-472D-A826-15D51581BE9F"))
                ViewRefresh()
                Exit Sub
            End If
        End If

        ' Нормально загружаем данные
        MyBase.LoadListDocDataTable()
    End Sub

    Public Overrides Sub SetListDocData()
        ' Сохраняем загруженное представление в сессии
        If MyBaseContainer IsNot Nothing Then
            MyBaseContainer.ListNavigation.Clear()
            MyBaseContainer.ListNavigation.AddRange(ListDocDataTable.AsEnumerable.Select(Function(F) DirectCast(F("ID"), Guid)))
        End If
    End Sub

#End Region

End Class
