﻿Imports System.IO

Partial Public Class RefFileUpload
    Inherits BaseCompositeControl

    Public Property Mode() As RefFiledMode
        Get
            Return _Mode
        End Get
        Set(ByVal value As RefFiledMode)
            _Mode = value
        End Set
    End Property
    Private _Mode As RefFiledMode = RefFiledMode.DataBase

    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        Dim updPanel As New UpdatePanel
        Dim fileUpload As New FileUpload
        fileUpload.Attributes("onchange") = "this.parentElement.getElementsByTagName('*')[1].disabled = (this.value == '' ? 'disabled' : '')"
        updPanel.ContentTemplateContainer.Controls.Add(fileUpload)
        Dim uplButton As New Button
        uplButton.ID = "uplButton"
        uplButton.Text = "Добавить"
        uplButton.Attributes("disabled") = "disabled"
        AddHandler uplButton.Click, AddressOf uplButton_Click
        updPanel.ContentTemplateContainer.Controls.Add(uplButton)
        updPanel.Triggers.Add(New PostBackTrigger With {.ControlID = "uplButton"})
        Controls.Add(updPanel)
    End Sub

    Public Event Uploading(ByVal sender As Object, ByVal e As RefFileArgs)
    Public Event Uploaded(ByVal sender As Object, ByVal e As RefFileArgs)

    Private Sub uplButton_Click(ByVal sender As Object, ByVal e As EventArgs)
        If Request.Files.Count > 0 AndAlso Request.Files(0).FileName <> "" Then
            Dim Args As New RefFileArgs
            Args.ID = Guid.NewGuid
            Args.TransferTime = Now
            Args.FileName = Path.GetFileName(Request.Files(0).FileName)
            RaiseEvent Uploading(Me, Args)
            If Args.Cancel Then
                ' Clear Request.Files!
            Else
                Select Case Mode
                    Case RefFiledMode.DataBase
                        Using dc As New RefFileDataContext(AppSettings.Cnn)
                            Dim filContent As New tblFILE_CONTENTS2
                            filContent.ID = Args.ID
                            filContent.OwnerID = AppSettings.UserInfo.UserID
                            filContent.CreationDateTime = Now
                            filContent.DocID = filContent.ID
                            filContent.RowID = 0
                            filContent.ISN_REF_FILE = 1
                            Dim firstFile = dc.tblFILE_CONTENTS2s.FirstOrDefault
                            If firstFile IsNot Nothing Then
                                filContent.ISN_REF_FILE = dc.tblFILE_CONTENTS2s.Select(Function(F) F.ISN_REF_FILE).Max + 1
                            End If
                            dc.tblFILE_CONTENTS2s.InsertOnSubmit(filContent)
                            filContent.FILE_EXTENSION = Path.GetExtension(Request.Files(0).FileName)
                            Dim buff(Request.Files(0).ContentLength) As Byte
                            Request.Files(0).InputStream.Read(buff, 0, Request.Files(0).ContentLength)
                            filContent.CONTENTS = buff
                            dc.SubmitChanges()
                        End Using
                    Case RefFiledMode.FileSystem
                        If Not My.Computer.FileSystem.DirectoryExists(Path.GetDirectoryName(Args.FileName)) Then
                            My.Computer.FileSystem.CreateDirectory(Path.GetDirectoryName(Args.FileName))
                        End If
                        Request.Files(0).SaveAs(Args.FileName)
                End Select
                RaiseEvent Uploaded(Me, Args)
            End If
        Else
            AddMessage("Файл не выбран")
        End If
    End Sub

End Class
