﻿Imports System.Data.SqlClient
Imports WordReportGenerator.WordReportGenerator

Public MustInherit Class BaseContainer
    Inherits eqBaseContainer

#Region "Ядро"

    Public Overrides Property ShowSelectBox() As Boolean
        Get
            If ViewState("Cls") IsNot Nothing OrElse ViewState("Location") IsNot Nothing OrElse ViewState("Movement") IsNot Nothing OrElse ViewState("Displace") IsNot Nothing OrElse ViewState("ChooseInventoryStructure") IsNot Nothing Then
                Return True
            End If
            Return MyBase.ShowSelectBox
        End Get
        Set(ByVal value As Boolean)
            MyBase.ShowSelectBox = value
        End Set
    End Property

    Public Overrides Property DocCommandMode() As Equipage.WebUI.eqBaseContainer.DocCommandModes
        Get
            If ViewState("Cls") IsNot Nothing OrElse ViewState("Location") IsNot Nothing OrElse ViewState("Movement") IsNot Nothing OrElse Not String.IsNullOrEmpty(Request.QueryString("SingleAndMulti")) Then
                Return DocCommandModes.SingleAndMulti
            End If
            Return MyBase.DocCommandMode
        End Get
        Set(ByVal value As Equipage.WebUI.eqBaseContainer.DocCommandModes)
            MyBase.DocCommandMode = value
        End Set
    End Property

    Public Overrides Property InsertDocMode() As Equipage.WebUI.eqBaseContainer.InsertDocModes
        Get
            Return ChildDocModes.Self
        End Get
        Set(ByVal value As Equipage.WebUI.eqBaseContainer.InsertDocModes)
            MyBase.ChildDocMode = value
        End Set
    End Property

    Public Overrides Property ChildDocMode() As Equipage.WebUI.eqBaseContainer.ChildDocModes
        Get
            Return ChildDocModes.NewWindow
        End Get
        Set(ByVal value As Equipage.WebUI.eqBaseContainer.ChildDocModes)
            MyBase.ChildDocMode = value
        End Set
    End Property

    Public Overrides Sub LoadDocumentMenu()
        MyBase.LoadDocumentMenu()

        If BaseDoc.DOCS_NEW.Contains(DocTypeURL.ToLower) Then
            If _
                DocTypeURL = "FUND.ascx" Or _
                Not String.IsNullOrEmpty(Request.QueryString("FUND")) Or _
                Not String.IsNullOrEmpty(Request.QueryString("INVENTORY")) Or _
                Not String.IsNullOrEmpty(Request.QueryString("UNIT")) _
            Then
                AddMenuItem("Новый", "NewDoc", "Images/New.gif", AddAtIndex:=0)
            End If
            If _
                DocTypeURL = "UNIT.ascx" And String.IsNullOrEmpty(Request.QueryString("INVENTORY")) And String.IsNullOrEmpty(Request.QueryString("DEPOSIT")) _
            Then '' если при юнит открыт не из описи или россыпи, то создание нового юнита вызывало бы ошибку 
                RemoveMenuItem("NewDoc")
            End If
        End If
        If BaseDoc.DOCS_EDIT.Contains(DocTypeURL.ToLower) Then
            AddMenuItem("Редактировать", "Unlock", "Images/Execute.gif")
        End If
    End Sub

    Public Overrides Sub LoadListDocMenu()
        MyBase.LoadListDocMenu()

        If Request.QueryString("FUND") Is Nothing AndAlso Request.QueryString("INVENTORY") Is Nothing Then
            RemoveMenuItem("Movement")
        End If

        If BasePage.DocID <> Guid.Empty Then
            RemoveMenuItem("ShowDocumentHistory")
            AddMenuItem("Протокол", "ShowDocumentHistory", "Images/GotoLine.gif")
        End If
        Dim ShowNewDoc = False
        ' В списке документов показываем кнопку
        If _
            (DocTypeURL = "FUND.ascx") Or (ListDocControl.DocType.Category.CategoryName.ToUpper = "РАБОТА С БАЗОЙ" And _
                (Not String.IsNullOrEmpty(Request.QueryString("ARCHIVE")) Or _
                Not String.IsNullOrEmpty(Request.QueryString("FUND")) Or _
                Not String.IsNullOrEmpty(Request.QueryString("INVENTORY")) Or _
                Not String.IsNullOrEmpty(Request.QueryString("INVENTORYSTRUCTURE")) Or _
                Not String.IsNullOrEmpty(Request.QueryString("UNIT"))) _
            ) _
        Then ShowNewDoc = True
        ' В списке ед.хр. открытым из фонда скрываем
        If _
            (DocTypeURL = "UNIT.ascx") And _
                Not String.IsNullOrEmpty(Request.QueryString("FUND")) _
        Then ShowNewDoc = False
        ' В списке ед.хр. открытым из раздела, если это не раздел описи, скрываем
        If _
            (DocTypeURL = "UNIT.ascx") And _
                Not String.IsNullOrEmpty(Request.QueryString("INVENTORYSTRUCTURE")) _
        Then
            Dim InventoryCheckSQL = <sql>
                                        select count(ID) from vInventoryStructure
                                        where ID = @ID and (KIND = 702) and Deleted = 0
                                    </sql>
            Dim InventoryCheckCommand As New SqlCommand(InventoryCheckSQL) With {.CommandTimeout = 3600}
            InventoryCheckCommand.Parameters.AddWithValue("@ID", Request.QueryString("INVENTORYSTRUCTURE"))
            Dim InventoryCheckResult = AppSettings.ExecCommandScalar(InventoryCheckCommand)
            If InventoryCheckResult = 0 Then ShowNewDoc = False
        End If
        If AppSettings.UserSecurities.HasDocumentsAccessNew(ListDocControl.DocType.DocTypeID) _
            And DocTypeURL = "UNIT.ascx" _
            And Not String.IsNullOrEmpty(Request.QueryString("INVENTORY")) _
        Then
            AddMenuItem("Шаблон списка ед. хранения", "ExportUnitsExcelTemplate", "Images/Excel.gif", "Выгрузить шаблон списка ед. хранения", False, 5)
            AddMenuItem("Импорт списка ед. хранения", "ImportUnitsFromExcel", "Images/Excel.gif", "Загрузить список ед. хранения из файла", False, 6)
        End If
        If AppSettings.UserSecurities.HasDocumentsAccessNew(ListDocControl.DocType.DocTypeID) _
           And DocTypeURL = "DOCUMENT.ascx" _
           And Not String.IsNullOrEmpty(Request.QueryString("UNIT")) _
            Then
            AddMenuItem("Шаблон списка документов", "ExportUnitDocsExcelTemplate", "Images/Excel.gif", "Выгрузить шаблон списка документов", False, 5)
            AddMenuItem("Импорт списка документов", "ImportUnitDocsFromExcel", "Images/Excel.gif", "Загрузить список документов", False, 6)
        End If
        If ShowNewDoc AndAlso AppSettings.UserSecurities.HasDocumentsAccessNew(ListDocControl.DocType.DocTypeID) Then
            AddMenuItem("Новый", "NewDoc", "Images/New.gif")
        End If
        If ViewState("Cls") IsNot Nothing Then
            RemoveMenuItem("Cls")
            RemoveMenuItem("Location")
            RemoveMenuItem("Movement")
            RemoveMenuItem("NewDoc")
            RemoveMenuItem("ShowDocumentHistory")
            RemoveMenuItem("Excel")
            RemoveMenuItem("Reports")
            RemoveMenuItem("Displace")
            RemoveMenuItem("Recover")
            RemoveMenuItem("ChooseInventoryStructure")
            ' Personal
            AddMenuItem("Выйти из АНСА", "Cls", "Images/Execute.gif")
            AddMenuItem("Прикрепить", "Attach", "Images/Tick.gif")
        ElseIf ViewState("Location") IsNot Nothing Then
            RemoveMenuItem("Cls")
            RemoveMenuItem("Location")
            RemoveMenuItem("Movement")
            RemoveMenuItem("NewDoc")
            RemoveMenuItem("ShowDocumentHistory")
            RemoveMenuItem("Excel")
            RemoveMenuItem("Reports")
            RemoveMenuItem("Displace")
            RemoveMenuItem("Recover")
            RemoveMenuItem("ChooseInventoryStructure")
            ' Personal
            AddMenuItem("Выйти из Топографирования", "Location", "Images/Execute.gif")
            AddMenuItem("Прикрепить", "Attach", "Images/Tick.gif")
        ElseIf ViewState("Movement") IsNot Nothing Then
            RemoveMenuItem("Cls")
            RemoveMenuItem("Location")
            RemoveMenuItem("Movement")
            RemoveMenuItem("NewDoc")
            RemoveMenuItem("ShowDocumentHistory")
            RemoveMenuItem("Excel")
            RemoveMenuItem("Reports")
            RemoveMenuItem("Displace")
            RemoveMenuItem("Recover")
            RemoveMenuItem("ChooseInventoryStructure")
            ' Personal
            AddMenuItem("Выйти из Движения", "Movement", "Images/Execute.gif")
            AddMenuItem("Прикрепить", "Attach", "Images/Tick.gif")
        ElseIf ViewState("Displace") IsNot Nothing Then
            RemoveMenuItem("Cls")
            RemoveMenuItem("Location")
            RemoveMenuItem("Movement")
            RemoveMenuItem("NewDoc")
            RemoveMenuItem("ShowDocumentHistory")
            RemoveMenuItem("Excel")
            RemoveMenuItem("Reports")
            RemoveMenuItem("Displace")
            RemoveMenuItem("Recover")
            RemoveMenuItem("ChooseInventoryStructure")

            AddMenuItem("Выйти из Перемещения", "Displace", "Images/Execute.gif")
            AddMenuItem("Переместить", "DoDisplace", "Images/Tick.gif")
        ElseIf ViewState("ChooseInventoryStructure") IsNot Nothing Then
            RemoveMenuItem("Cls")
            RemoveMenuItem("Location")
            RemoveMenuItem("Movement")
            RemoveMenuItem("NewDoc")
            RemoveMenuItem("ShowDocumentHistory")
            RemoveMenuItem("Excel")
            RemoveMenuItem("Reports")
            RemoveMenuItem("Displace")
            RemoveMenuItem("Recover")
            RemoveMenuItem("Displace")
            RemoveMenuItem("ChooseInventoryStructure")

            AddMenuItem("Выйти из Прикрепления", "ChooseInventoryStructure", "Images/Execute.gif")
            AddMenuItem("Перейти к выбору раздела описи", "DoChooseInventoryStructure", "Images/Tick.gif")
        End If
    End Sub

    Public Overrides Sub ExecuteDocumentCommand(ByVal CommandName As String)
        Select Case CommandName
            Case "SaveDoc", "DeleteDoc"
                ' AW: Это справочник, с защитой (обработка здесь потому что есть не переопределяемые авто-документы)
                If DocControl IsNot Nothing AndAlso DocControl.DataSetHeaderDataRow.Table.Columns.Contains("PROTECTED") Then

                    If AppSettings.UserInfo.UserLogin <> "sa" Then
                        If DocControl.DataSetHeaderDataRow("PROTECTED").ToString = "Y" Then
                            AddMessage("Редактирование/удаление значений из стандартных справочников невозможно.")
                            Exit Sub
                        End If
                    End If
                End If
                MyBase.ExecuteDocumentCommand(CommandName)
            Case "Report"
                    Select Case DocTypeURL
                        Case "ARCHIVE.ascx"
                            Dim cmd As New SqlCommand("select ISN_ARCHIVE from tblARCHIVE where ID=@ID")
                            cmd.Parameters.AddWithValue("@ID", DocID)
                            Dim ISN_ARCHIVE = AppSettings.ExecCommandScalar(cmd)
                            Response.Redirect(BasePage.GetNewDocURL("WordReports.ascx", False, "&ISN_ARCHIVE=" & ISN_ARCHIVE))
                        Case "FUND.ascx"
                            Dim cmd As New SqlCommand("select ISN_FUND from tblFUND where ID=@ID")
                            cmd.Parameters.AddWithValue("@ID", DocID)
                            Dim ISN_FUND = AppSettings.ExecCommandScalar(cmd)
                            Response.Redirect(BasePage.GetNewDocURL("WordReports.ascx", False, "&ISN_FUND=" & ISN_FUND))
                        Case "INVENTORY.ascx"
                            Dim cmd As New SqlCommand("select ISN_INVENTORY from tblINVENTORY where ID=@ID")
                            cmd.Parameters.AddWithValue("@ID", DocID)
                            Dim ISN_INVENTORY = AppSettings.ExecCommandScalar(cmd)
                            Response.Redirect(BasePage.GetNewDocURL("WordReports.ascx", False, "&ISN_INVENTORY=" & ISN_INVENTORY))
                        Case "UNIT.ascx"
                            Dim cmd As New SqlCommand("select ISN_UNIT from tblUNIT where ID=@ID")
                            cmd.Parameters.AddWithValue("@ID", DocID)
                            Dim ISN_UNIT = AppSettings.ExecCommandScalar(cmd)
                            Response.Redirect(BasePage.GetNewDocURL("WordReports.ascx", False, "&ISN_UNIT=" & ISN_UNIT))
                        Case "LOCATION.ascx"
                            Dim cmd As New SqlCommand("select ISN_LOCATION from tblLOCATION where ID=@ID")
                            cmd.Parameters.AddWithValue("@ID", DocID)
                            Dim ISN_LOCATION = AppSettings.ExecCommandScalar(cmd)
                            Response.Redirect(BasePage.GetNewDocURL("WordReports.ascx", False, "&ISN_LOCATION=" & ISN_LOCATION))
                        Case "ARCHIVE_PASSPORT.ascx"
                            Dim cmd As New SqlCommand("select ISN_PASSPORT from tblARCHIVE_PASSPORT where ID=@ID")
                            cmd.Parameters.AddWithValue("@ID", DocID)
                            Dim ISN_PASSPORT = AppSettings.ExecCommandScalar(cmd)
                            Response.Redirect(BasePage.GetNewDocURL("WordReports.ascx", False, "&ISN_PASSPORT=" & ISN_PASSPORT))
                    End Select
            Case "ReportIndex"
                    Response.Redirect(BasePage.GetNewDocURL("WordReports.ascx", False, "ForIndex=" & DocTypeURL))
            Case "ReportIndexAsExcel"
                    MyBase.ExecuteDocumentCommand("Excel")
            Case "MakeReportArchList"
                    Dim ID_INVENTORY = Request.QueryString("INVENTORY")
                    If ID_INVENTORY IsNot Nothing Then
                        Dim cmd As New SqlCommand("select ISN_INVENTORY from tblINVENTORY where ID=@ID")
                        cmd.Parameters.AddWithValue("@ID", ID_INVENTORY)
                        Dim ISN_INVENTORY = AppSettings.ExecCommandScalar(cmd)

                        Dim SortOrder As String = ""
                        Dim AcceptedFieldNames As New List(Of String)(New String() {"NAME", "START_YEAR", "END_YEAR", "PAGE_COUNT", "NOTE"})

                        For Each R As DataRowView In ListDocControl.ViewSortingFields
                            Dim FieldID = DirectCast(R("FieldID"), Guid)
                            Dim GGR = ListDocControl.ViewGrouppingFields.FirstOrDefault(Function(GR) GR("FieldID") = FieldID)
                            If GGR Is Nothing Then
                                Dim FieldInfo = ListDocControl.DocType.GetFieldInfo(FieldID)
                                Dim ListDocFieldInfo = ListDocControl.DocType.GetListDocFieldInfo(FieldID)
                                Dim FieldName = "[" & FieldInfo("FieldName") & "]"
                                If AcceptedFieldNames.Contains(FieldInfo("FieldName")) Then
                                    SortOrder = SortOrder & "," & FieldName & " " & R("SortDirection").ToString
                                End If
                            End If
                        Next

                        Dim Report = FillReport_ArchList(AppSettings.Cnn, 4, ISN_INVENTORY, SortOrder)
                        BasePage.AttachFile(Report.FileName, Report.Data)
                    End If
            Case "Cls"
                    If LockCommand("Читатели") Then
                        Exit Sub
                    End If
                    If ViewState("Cls") Is Nothing Then
                        ViewState("Cls") = True
                    Else
                        ViewState("Cls") = Nothing
                    End If
                    ListDocControl.Refresh()
                    ListDocControl.LoadListDocMenu()
            Case "Location"
                    If LockCommand("Читатели") Then
                        Exit Sub
                    End If
                    If ViewState("Location") Is Nothing Then
                        ViewState("Location") = True
                    Else
                        ViewState("Location") = Nothing
                    End If
                    ListDocControl.Refresh()
                    ListDocControl.LoadListDocMenu()
            Case "Movement"
                    If LockCommand("Читатели") Then
                        Exit Sub
                    End If
                    If ViewState("Movement") Is Nothing Then
                        ViewState("Movement") = True
                    Else
                        ViewState("Movement") = Nothing
                    End If
                    ListDocControl.Refresh()
                    ListDocControl.LoadListDocMenu()
            Case "Attach"
                    Dim SelectedDocs = ListDocControl.SelectedDocuments
                    If SelectedDocs.Count = 0 Then
                        AddMessage("Выберите объекты учета для прикрепления")
                    Else
                        If ViewState("Cls") IsNot Nothing Then
                            BasePage.OpenModalDialogWindow(Me, "Cls.aspx?DocTypeURL=" & DocTypeURL, SelectedDocs)
                        ElseIf ViewState("Location") IsNot Nothing Then
                            BasePage.OpenModalDialogWindow(Me, "Location.aspx?DocTypeURL=" & DocTypeURL, SelectedDocs)
                        ElseIf ViewState("Movement") IsNot Nothing Then
                            Dim add As String
                            If Request.QueryString("FUND") IsNot Nothing Then
                                add = "&FUND=" & Request.QueryString("FUND")
                            Else
                                add = "&INVENTORY=" & Request.QueryString("INVENTORY")
                            End If
                            BasePage.OpenModalDialogWindow(Me, "Movement.aspx?DocTypeURL=" & DocTypeURL & add, SelectedDocs)
                        End If
                    End If
            Case "ChooseArchives"
                    Dim arch As String
                    Dim SelectedDocs = ListDocControl.SelectedDocuments
                    Dim sd As Guid
                    For Each SelectedDoc In SelectedDocs
                        Debug.Print(SelectedDoc.ToString)
                        arch = arch + SelectedDoc.ToString + "-"
                        sd = SelectedDoc
                    Next
                    BasePage.OpenWindow(BasePage.GetNewDocURL("FC_ExcelReports.ascx", True, "ARCH=1" & arch))
                    'Response.Redirect(BasePage.GetDocURL("FC_ExcelReports.ascx", sd, False, "ARCH=1" & arch))
            Case "ShowDocumentHistory"
                    If BasePage.IsListDoc Then
                        BasePage.OpenWindow(BasePage.GetNewDocURL("ShowDocumentHistory.ascx", True, "ForDocTypeURL=" & DocTypeURL))
                    Else
                        BasePage.OpenWindow(BasePage.GetNewDocURL("ShowDocumentHistory.ascx", True, "ForDocTypeURL=" & DocTypeURL & "&ForDocID=" & DocID.ToString))
                    End If
            Case "InventoryAct"
                    If Request.QueryString("FUND") IsNot Nothing Then
                        Response.Redirect(BasePage.GetListDocURL("INVENTORY_ACT.ascx", False, "FUND=" & Request.QueryString("FUND")))
                    Else
                        Response.Redirect(BasePage.GetListDocURL("INVENTORY_ACT.ascx", False))
                    End If
            Case "CalcAllFund"
                If Not LockCommand("Читатели") Then
                    'BasePage.AddMessage("Создавать ли резервную копию перед запуском пересчета?", "Резервное копирование", eqBasePage.AddMessageButtons.YesNoCancel)
                    BasePage.OpenModalDialogWindow(Me, "RecalcFund.aspx?DocTypeURL=" & DocTypeURL, "All", Nothing)
                End If
            Case "CalcAllInventory"
                    If Not LockCommand("Читатели") Then
                        BasePage.OpenModalDialogWindow(Me, "RecalcInventory.aspx?DocTypeURL=" & DocTypeURL, "All", Nothing)
                    End If
            Case "CalcAllUnit"
                    If Not LockCommand("Читатели") Then
                        BasePage.OpenModalDialogWindow(Me, "RecalcUnit.aspx?DocTypeURL=" & DocTypeURL, "All", Nothing)
                    End If
            Case "CalcAllDocument"
                    If Not LockCommand("Читатели") Then
                        Dim r As New Recalc(AppSettings)
                        Try
                            r.CalcDocument()
                        Catch ex As Exception
                            AddMessage("Не удалось выполнить расчет: >> " & ex.Message)
                        End Try
                    End If
            Case "Displace"
                    If LockCommand("Читатели") Then
                        Exit Sub
                    End If
                    If ViewState("Displace") Is Nothing Then
                        ViewState("Displace") = True
                    Else
                        ViewState("Displace") = Nothing
                    End If
                    ListDocControl.Refresh()
                    ListDocControl.LoadListDocMenu()

            Case "DoDisplace"
                    Dim SelectedDocs = ListDocControl.SelectedDocuments
                    If SelectedDocs.Count = 0 Then
                        AddMessage("Выберите объекты учета для прикрепления")
                    Else
                        If ViewState("Displace") IsNot Nothing Then
                            Dim urlParams As String = ""

                        'If DocTypeURL = "UNIT.ascx" Then
                        '    urlParams += GetUnitDisplacementUrlParams()
                        ' ElseIf DocTypeURL = "DOCUMENT.ascx" Then
                        urlParams += GetDisplacementUrlParams(DocTypeURL, SelectedDocs)
                        ' End If
                        BasePage.OpenModalDialogWindow(Me, "Displacement.aspx?DocTypeURL=" & DocTypeURL & urlParams, SelectedDocs)
                        End If
                    End If
            Case "Recover"
                    If LockCommand("Читатели") Then
                        Exit Sub
                    End If
                    Dim urlParams As String = ""
                    Select Case DocTypeURL
                        Case "DOCUMENT.ascx"
                            urlParams += "&UNIT=" + Request.QueryString("UNIT")
                        Case "UNIT.ascx"
                            urlParams += "&INVENTORY=" + Request.QueryString("INVENTORY")
                        Case "INVENTORY.ascx"
                            urlParams += "&FUND=" + Request.QueryString("FUND")
                        Case "FUND.ascx"
                            urlParams += "&ARCHIVE=" + Request.QueryString("ARCHIVE")
                            'Case "INVENTORYSTRUCTURE.ascx"
                            '    urlParams += "&INVENTORYSTRUCTURE=" + Request.QueryString("INVENTORYSTRUCTURE")
                        Case "INVENTORYSTRUCTURE.ascx"
                            If Request.QueryString("FUND") <> "" Then
                                urlParams += "&FUND=" + Request.QueryString("FUND")
                            End If
                            If Request.QueryString("INVENTORY") <> "" Then
                                urlParams += "&INVENTORY=" + Request.QueryString("INVENTORY")
                            End If
                        Case Else
                            urlParams += ""
                    End Select

                    'BasePage.OpenModalDialogWindow(Me, BasePage.GetListDocURL("View.ascx", True), "SelectView", CurrentViewDataSet) 'так открывают окно, перезагружающее список
                    BasePage.OpenModalDialogWindow(Me, "Recovery.aspx?DocTypeURL=" & DocTypeURL & urlParams, New eqModalDialogArgument("Recovery.aspx"))
                    'BasePage.OpenModalDialogWindow(Me,BasePage.GetListDocURL("Recovery.aspx",True),"SelectView
                    'ListDocControl.Refresh()

            Case "ChooseInventoryStructure"
                    If LockCommand("Читатели") Then
                        Exit Sub
                    End If
                    If ViewState("ChooseInventoryStructure") Is Nothing Then
                        ViewState("ChooseInventoryStructure") = True
                    Else
                        ViewState("ChooseInventoryStructure") = Nothing
                    End If
                    ListDocControl.Refresh()
                    ListDocControl.LoadListDocMenu()
            Case "DoChooseInventoryStructure"
                    Dim SelectedDocs = ListDocControl.SelectedDocuments
                    If SelectedDocs.Count = 0 Then
                        AddMessage("Выберите объекты учета для прикрепления")
                    Else

                        If ViewState("ChooseInventoryStructure") IsNot Nothing Then
                            BasePage.OpenModalDialogWindow(Me, "InventoryStructureChoose.aspx?INVENTORY=" & Request.QueryString("INVENTORY"), SelectedDocs)
                        End If

                    End If
            Case Else
                    MyBase.ExecuteDocumentCommand(CommandName)
        End Select
    End Sub

    ''' <summary>
    ''' Возвращает параметры URL для вызова окна перемещения юнита. Вчасности, там задается ограничение на фонд.
    ''' </summary>
    ''' <returns>Парамерты для url в окно перемещения "Displacement.aspx"</returns>
    ''' <remarks>Roman 15.07.13</remarks>

    Private Function GetDisplacementUrlParams(ByVal DocTypeURL As String, ByVal SelectedDocs As List(Of Guid)) As String
        Select Case DocTypeURL
            Case "UNIT.ascx"
                'Раскомментируйте , если понадобится ограничить описи в которые будет осуществляться перемещение ед. хр.
                'AppSettings.ExecCommand(New SqlCommand(<sql>create table #temp (ID uniqueidentifier)</sql>))'закомментировано в зависимости от вариантов
                'For Each doc As Guid In SelectedDocs
                '    AppSettings.ExecCommand(New SqlCommand($"insert into #temp values('{doc}')"))'01.12.2020 Саламан Андрей
                'Next


                'Dim cmdString = <sql>select F.ID from tblFUND as F
                '                     join tblINVENTORY as I 
                '                     on F.ISN_FUND = I.ISN_FUND
                '                     where I.ID = @IID</sql> 'Вариант 1 - Не рабочий 01.12.2020 Саламан Андрей

                'Dim cmdString = <sql>select F.ID from tblFUND as F
                '                        join (select i.ISN_FUND from tblINVENTORY as i
                '                           join tblUNIT as u on i.ISN_INVENTORY = u.ISN_INVENTORY
                '                           join #temp as t on u.ID = t.ID) as I 
                '                        on F.ISN_FUND=I.ISN_FUND 
                '                        drop table #temp</sql> Вариант 2 - только те фонды , из которых выбраны ед. хр. 01.12.2020 Саламан Андрей

                'Dim cmdString = <sql>select I.ID from tblINVENTORY as I
                '                        join tblUNIT as u on u.ISN_INVENTORY = I.ISN_INVENTORY
                '                        join #temp as t on t.ID = u.ID
                '                        drop table #temp</sql> 'Вариант 3 - только те описи, из которых выбраны ед. хр. 01.12.2020 Саламан Андрей

                Dim cmdString = <sql>select ID from tblINVENTORY where Deleted = 0</sql> 'Вариант 4 - Все описи. 01.12.2020 Саламан Андрей



                Dim cmd As New SqlCommand(cmdString)

                'ВСЕГДА дает Nothing - ошибка 01.12.2020 Саламан Андрей
                'cmd.Parameters.AddWithValue("@IID", Request.QueryString("INVENTORY"))

                Dim cmdResult = AppSettings.ExecCommandScalar(cmd) 'узнаем фонд, в котором лежит опись, из которой перемещаем юнит

                Return "&INVENTORY=" + cmdResult.ToString 'отобразим только описи того же фонда для перемещения 01.12.2020 Саламан Андрей
                'Return "&FUND=" + cmdResult.ToString 'отобразим только описи того же фонда для перемещения
            Case "DOCUMENT.ascx"
                'Dim cmdString = <sql>SELECT I.ID 
                '                FROM tblINVENTORY as I
                '                JOIN tblUNIT as U ON I.ISN_INVENTORY=U.ISN_INVENTORY
                '                WHERE U.ID = @UID </sql>'01.12.2020 Саламан Андрей
                Dim cmdString = <sql>select ID from tblINVENTORY where Deleted = 0</sql> '01.12.2020 Саламан Андрей

                Dim cmd As New SqlCommand(cmdString)
                'cmd.Parameters.AddWithValue("@UID", Request.QueryString("UNIT"))'01.12.2020 Саламан Андрей
                Dim cmdResult = AppSettings.ExecCommandScalar(cmd)

                Return "&INVENTORY=" + cmdResult.ToString
            Case Else
                Return ""
        End Select

    End Function


#End Region

#Region "Методы"

    Private Function LockCommand(ByVal RoleName As String) As Boolean
        Dim Roles = AppSettings.UserSecurities.Roles
        If Roles.Count = 1 AndAlso Roles.SingleOrDefault(Function(F) F("RoleName") = RoleName) IsNot Nothing Then
            AddMessage(String.Format("Команда не доступна для роли '{0}'", RoleName))
            Return True
        End If
        Return False
    End Function

#End Region

#Region "Работа со списками"

    Public ReadOnly Property ListID() As String
        Get
            If Request.QueryString("ListID") IsNot Nothing Then
                Return Request.QueryString("ListID")
            End If
            If ViewState("ListID") Is Nothing Then
                ViewState("ListID") = Guid.NewGuid
            End If
            Return ViewState("ListID").ToString
        End Get
    End Property

    Public ReadOnly Property ListNavigation() As List(Of Guid)
        Get
            If Session(ListID & "ListNavigation") Is Nothing Then
                Session(ListID & "ListNavigation") = New List(Of Guid)
            End If
            Return Session(ListID & "ListNavigation")
        End Get
    End Property

    Public ReadOnly Property ListSorting() As List(Of Pair)
        Get
            If Session(ListID & "ListSorting") Is Nothing Then
                Session(ListID & "ListSorting") = New List(Of Pair)
            End If
            Return Session(ListID & "ListSorting")
        End Get
    End Property

#End Region

End Class
