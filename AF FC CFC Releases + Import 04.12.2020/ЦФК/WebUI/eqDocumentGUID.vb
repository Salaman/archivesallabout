﻿Imports System.Data
Imports System.Data.SqlClient

Namespace WebUI_G

    ''' <summary>
    ''' Обновленный eqDocument для работы с полями идентификаторов типа GUID
    ''' </summary>
    ''' <remarks></remarks>
    Public Class eqDocument
        Inherits Equipage.WebUI.eqDocument

        ''' <summary>
        ''' Имя поля идентификатора int
        ''' </summary>
        ''' <remarks></remarks>
        Public Property KeyName() As String
            Get
                If _KeyName = "" Then
                    Return ID
                Else
                    Return _KeyName
                End If
            End Get
            Set(ByVal value As String)
                _KeyName = value
            End Set
        End Property
        Private _KeyName As String

        ''' <summary>
        ''' Значение поля идентификаторов int, автоматически транслируется в DocID
        ''' </summary>
        ''' <remarks></remarks>
        Public Property KeyValue() As System.Guid
            Get
                Dim DocType = AppSettings.GetDocType(DocTypeURL)
                Dim SqlCommand As New SqlClient.SqlCommand(String.Format("SELECT [{0}] FROM {1} WHERE ID=@ID", KeyName, DocType.GetHeaderTableName))
                SqlCommand.Parameters.AddWithValue("@ID", DocID)
                Dim DocumentDataTable = AppSettings.GetDataTable(SqlCommand)
                If DocumentDataTable IsNot Nothing AndAlso DocumentDataTable.Rows.Count > 0 Then
                    KeyValue = DocumentDataTable.Rows(0)(KeyName)
                Else
                    KeyValue = New System.Guid
                End If
            End Get
            Set(ByVal value As System.Guid)
                If Len(value.ToString) = 0 Then
                    DocID = Nothing
                Else
                    Dim DocType = AppSettings.GetDocType(DocTypeURL)
                    Dim SqlCommand As New SqlClient.SqlCommand(String.Format("SELECT ID FROM {1} WHERE [{0}]=@KeyValue", KeyName, DocType.GetHeaderTableName))
                    SqlCommand.Parameters.AddWithValue("@KeyValue", value.ToString)
                    Dim DocumentDataTable = AppSettings.GetDataTable(SqlCommand)
                    If DocumentDataTable IsNot Nothing AndAlso DocumentDataTable.Rows.Count > 0 Then
                        DocID = DocumentDataTable.Rows(0)("ID")
                    Else
                        DocID = Nothing
                    End If
                End If
            End Set
        End Property

        ''' <summary>
        ''' Включает проверку правельности вложенности ссылок
        ''' </summary>
        ''' <value></value>
        Public Property ValidateHierarchy() As Boolean
            Get
                Return _ValidateHierarchy
            End Get
            Set(ByVal value As Boolean)
                _ValidateHierarchy = value
            End Set
        End Property
        Private _ValidateHierarchy As Boolean = False

        Private Sub Page_SelectDoc(ByVal sender As Object, ByVal e As Equipage.WebUI.eqBaseContainer.SelectDocEventArgs) Handles Me.SelectDoc
            If ValidateHierarchy Then
                Dim DocType = AppSettings.GetDocType(DocTypeURL)
                Dim SQL As New StringBuilder

                SQL.AppendLine("WITH CTE AS")
                SQL.AppendLine("(")
                SQL.AppendLine("  SELECT ID, [" & KeyName & "], [" & ID & "]")
                SQL.AppendLine("  FROM [" & DocType.GetHeaderTableName & "]")
                SQL.AppendLine("  WHERE ID = @SelectedValue")
                SQL.AppendLine("UNION ALL")
                SQL.AppendLine("  SELECT T.ID, T.[" & KeyName & "], T.[" & ID & "]")
                SQL.AppendLine("  FROM [" & DocType.GetHeaderTableName & "] T")
                SQL.AppendLine("  INNER JOIN CTE ON CTE.[" & ID & "] = T.[" & KeyName & "]")
                SQL.AppendLine(")")
                SQL.AppendLine("SELECT CTE.*")
                SQL.AppendLine("FROM CTE")
                SQL.AppendLine("WHERE CTE.ID = @CurrentValue")

                Dim cmd As New SqlCommand(SQL.ToString)
                cmd.Parameters.AddWithValue("@SelectedValue", e.DocID)
                cmd.Parameters.AddWithValue("@CurrentValue", BaseDoc.DocID)
                Dim dt = AppSettings.GetDataTable(cmd)

                If dt.Rows.Count > 0 Then
                    e.Suppress = True
                    AddMessage("Ссылка на вложенный раздел!")
                End If
            End If
        End Sub

    End Class
End Namespace
