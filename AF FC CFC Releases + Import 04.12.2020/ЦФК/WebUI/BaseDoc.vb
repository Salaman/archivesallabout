﻿Imports System.Data.SqlClient

Public Class BaseDoc
    Inherits eqBaseDoc
    Implements IPostBackEventHandler

#Region "Права на документы"

#If FC Or CFC Then
    Public Shared DOCS_NEW As String() = New String() {}
    Public Shared DOCS_EDIT As String() = New String() {"archive.ascx", "authorizeddep.ascx"}
    Public Shared DOCS_CALC As String() = New String() {"fund.ascx", "inventory.ascx", "unit.ascx"}
#ElseIf GR Then
    Public Shared DOCS_NEW As String() = New String() {}
    Public Shared DOCS_EDIT As String() = New String() {"gr_document.ascx"}
    Public Shared DOCS_CALC As String() = New String() {}
#Else
    Public Shared DOCS_NEW As String() = New String() {"fund.ascx", "inventory.ascx", "unit.ascx", "document.ascx"}
    Public Shared DOCS_EDIT As String() = New String() {"act.ascx", "archive.ascx", "archive_passport.ascx", "fund.ascx", "inventory.ascx", "inventory_act.ascx", "inventorystructure.ascx", "deposit.ascx", "unit.ascx", "document.ascx", "question.ascx"}
    Public Shared DOCS_CALC As String() = New String() {"fund.ascx", "inventory.ascx", "unit.ascx"}
#End If

#End Region

#Region "Свойства"

    Protected ReadOnly Property MyBaseContainer() As BaseContainer
        Get
            Return DirectCast(Me.BaseContainer, BaseContainer)
        End Get
    End Property

#End Region

#Region "События"

    Private Sub Page_PreRender1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If DocStateInfo.StateID <> Guid.Empty Then
            Select Case DocTypeURL
                Case "FUND.ascx"
                    SetTitle(String.Format("Карточка фонда ""{1}"" : Cохранена", DocType.DocTypeName, DataSetHeaderDataRow("FUND_NAME_SHORT")))
                Case "INVENTORY.ascx"
                    SetTitle(String.Format("Карточка описи ""{1}"" : Cохранена", DocType.DocTypeName, DataSetHeaderDataRow("INVENTORY_NAME")))
                Case "DEPOSIT.ascx"
                    SetTitle(String.Format("Карточка россыпи ""{1}"" : Cохранена", DocType.DocTypeName, DataSetHeaderDataRow("DEPOSIT_NAME")))
                Case "UNIT.ascx"
                    SetTitle(String.Format("Карточка ед.уч./ед.хр. ""{1}"" : Cохранена", DocType.DocTypeName, DataSetHeaderDataRow("NAME")))
                Case "Service.ascx"
                    SetTitle("Обслуживание")
                Case Else
                    SetTitle("Карточка " & DocType.DocTypeName.ToLower & " Cохранена")
            End Select
        Else
            If DocType.DocToolBarRows.Count = 0 Then
                SetTitle(DocType.DocTypeName)
            Else
                SetTitle("Новая карточка " & DocType.DocTypeName)
            End If
        End If
        CheckSpecifications()
    End Sub

#End Region

#Region "Методы"

    Public Function LockCommand(ByVal RoleName As String) As Boolean
        Dim Roles = AppSettings.UserSecurities.Roles
        If Roles.Count = 1 AndAlso Roles.SingleOrDefault(Function(F) F("RoleName") = RoleName) IsNot Nothing Then
            AddMessage(String.Format("Команда не доступна для роли '{0}'", RoleName))
            Return True
        End If
        Return False
    End Function

#Region "Рекурсивный лог удаления"
    '//////////////////////////////////////////////////////////////////////////////////////////////////////////
    ' решение задачи рекурсивного проставления в лог сообщений об удалении дочерних контейнеров
    ' (запись об удалении базового контейнера туда прописывается в ф-ии DeleteDoc())
    Public Function setDocStatesLogDeletedRecursively(ByVal StatusText As String) As Boolean
        Return _setDocStatesLogDeletedRecursively(AppSettings, 0, DocID, DocType.DocTypeID, _getStatusIdDeleteByDocTypeId(DocType.DocTypeID), StatusText)
    End Function

    Private Shared Function _getChildIdListByDocTypeId(ByVal DocTypeId As Guid) As List(Of Guid)
        Dim ret As List(Of Guid) = New List(Of Guid)()
        Select Case DocTypeId.ToString().ToUpper()
            Case "C03E30F9-2AF1-4D7B-8F48-15EBE74B3946" '"ARCHIVE"
                ret.Add(New Guid("852437A4-49C3-436D-B388-15EBE6743C69")) '("FUND")
                ret.Add(New Guid("B3908728-D264-4612-BA7F-15EBECA3AE66")) '("ARCHIVE_PASSPORT")
            Case "852437A4-49C3-436D-B388-15EBE6743C69" '"FUND"
                ret.Add(New Guid("A734924C-177A-49B2-AB56-15EBE34DA478")) '("INVENTORY")
            Case "A734924C-177A-49B2-AB56-15EBE34DA478" '"INVENTORY"
                ret.Add(New Guid("E4D0643E-4DCC-42E3-9728-15EBE4D1B785")) '("UNIT")

                ret.Add(New Guid("C8615022-AE8E-4116-BDCC-15D51107FA73")) '("INVENTORY_STRUCTURE")

                '  по кактм-то причинам, DOCUMENT не удаляется при удалении UNIT (видимо, связь с UNIT - не главная)
                'Case "E4D0643E-4DCC-42E3-9728-15EBE4D1B785" '"UNIT"
                '   ret.Add(New Guid("D755C0EE-0754-4481-BBAC-15EBE7888580")) '("DOCUMENT")
        End Select
        Return ret
    End Function


    Private Shared Function _getStatusIdDeleteByDocTypeId(ByVal DocTypeId As Guid) As Guid
        'SELECT *
        'FROM eqDocStates
        'WHERE StateDescription = 'Удален'
        'AND DocTypeID = (
        '	SELECT ID
        '	FROM eqDocTypes
        '	WHERE Deleted = 0
        '	 AND DocURL = 'UNIT.ascx'
        ')
        Select Case DocTypeId.ToString().ToUpper()
            Case "E4D0643E-4DCC-42E3-9728-15EBE4D1B785" '"UNIT"
                Return New Guid("28462C8E-6809-4BFF-BC1D-15EBE4D1FE81")
            Case "D755C0EE-0754-4481-BBAC-15EBE7888580" '"DOCUMENT"
                Return New Guid("D9989330-703D-445C-A8FE-15EBE788B0C8")
            Case "C8615022-AE8E-4116-BDCC-15D51107FA73" '"INVENTORY_STRUCTURE"
                Return New Guid("D45C1A23-CA2F-4175-9631-15D51107DA91")
            Case "A734924C-177A-49B2-AB56-15EBE34DA478" '"INVENTORY"
                Return New Guid("D43F638C-1B32-42CE-A292-15EBE34E0740")
            Case "C03E30F9-2AF1-4D7B-8F48-15EBE74B3946" '"ARCHIVE"
                Return New Guid("58749535-2AC0-4C48-83B4-15EBE74B65C3")
            Case "852437A4-49C3-436D-B388-15EBE6743C69" '"FUND"
                Return New Guid("AC975C8E-6948-4BE5-9BC6-15EBE6758BAC")
            Case "B3908728-D264-4612-BA7F-15EBECA3AE66" '"ARCHIVE_PASSPORT"
                Return New Guid("6E1719E5-631A-4B23-8288-15EBECA3E794")
            Case Else
                Return New Guid()
        End Select
    End Function



    Private Shared Function _getTableNameByDocTypeId(ByVal DocTypeId As Guid) As String
        'SELECT *
        'FROM eqDocTypes 
        'WHERE DocURL LIKE 'UNIT.%'
        Select Case DocTypeId.ToString().ToUpper()
            Case "E4D0643E-4DCC-42E3-9728-15EBE4D1B785"
                Return "UNIT"
            Case "D755C0EE-0754-4481-BBAC-15EBE7888580"
                Return "DOCUMENT"
            Case "C8615022-AE8E-4116-BDCC-15D51107FA73"
                Return "INVENTORY_STRUCTURE"
            Case "A734924C-177A-49B2-AB56-15EBE34DA478"
                Return "INVENTORY"
            Case "C03E30F9-2AF1-4D7B-8F48-15EBE74B3946"
                Return "ARCHIVE"
            Case "852437A4-49C3-436D-B388-15EBE6743C69"
                Return "FUND"
            Case "B3908728-D264-4612-BA7F-15EBECA3AE66"
                Return "ARCHIVE_PASSPORT"
            Case Else
                Return ""
        End Select
    End Function


    Private Shared Function _setDocStatesLogDeletedRecursively(ByRef appS As eqAppSettings, ByRef sch As Integer, ByVal DocID As Guid, ByVal DocTypeID As Guid, ByVal StatusID As Guid, ByVal StatusText As String) As Boolean
        If sch > 0 Then 'возможность не проставлять статус для родительского элемента
            Dim sqlStrLog = <sql>
                         INSERT INTO eqDocStatesLOG (ID, DocID,  UserID, StatusID, EventTime,  [RollBack], DocTypeID, UserName, StatusName)
                         VALUES (NewId(), @DocID, dbo.CURRENT_USER_ID(), @StatusID, GetDate(), 0, @DocTypeID, dbo.fnGetUserName(dbo.CURRENT_USER_ID()), @StatusName)
                     </sql>
            Dim sqlCmdLog As New SqlCommand(sqlStrLog.Value)
            sqlCmdLog.Parameters.AddWithValue("@DocID", DocID)
            sqlCmdLog.Parameters.AddWithValue("@StatusID", StatusID)
            sqlCmdLog.Parameters.AddWithValue("@DocTypeID", DocTypeID)
            sqlCmdLog.Parameters.AddWithValue("@StatusName", StatusText)
            appS.ExecCommand(sqlCmdLog)
        End If

        Dim sqlXmlGetChildren = <sql>
                                    SELECT ID
                                    FROM tbl%ChildName%
                                    WHERE ISN_%ParentName% = (
                                        SELECT ISN_%ParentName% 
                                        FROM tbl%ParentName% 
                                        WHERE ID=@ID_Parent
                                    )
                                </sql>


        Dim ChildIdList As List(Of Guid) = _getChildIdListByDocTypeId(DocTypeID)
        'Dim ParentName As String = Me.DocType.DocTypeName

        For Each ChildId As Guid In ChildIdList
            Dim sqlStrGetChildren As String = sqlXmlGetChildren.Value.Replace("%ChildName%", _getTableNameByDocTypeId(ChildId)).Replace("%ParentName%", _getTableNameByDocTypeId(DocTypeID))
            Dim sqlCmdGetChildren As New SqlCommand(sqlStrGetChildren)
            sqlCmdGetChildren.Parameters.AddWithValue("@ID_Parent", DocID)
            Dim dt As DataTable = appS.GetDataTable(sqlCmdGetChildren)
            For Each row As DataRow In dt.Rows
                _setDocStatesLogDeletedRecursively(appS, sch + 1, row("ID"), ChildId, StatusID, StatusText)
            Next
        Next


        Return True
    End Function
    '//////////////////////////////////////////////////////////////////////////////////////////////////////////
#End Region


#End Region

#Region "Ядро"

    Public Overrides Function NewDoc() As Boolean
        SetLock()
        Return MyBase.NewDoc()
    End Function

    Public Overrides Function LoadDoc(ByVal DocID As System.Guid) As Boolean
        Return MyBase.LoadDoc(DocID)
    End Function

    Public Overrides Function SaveDoc() As Boolean
        ' Сбрасываем флаг сохранения истории
        Dim SaveDocHistoryFlagControl = BaseContainer.FindControl("SaveDocHistoryFlag")
        If SaveDocHistoryFlagControl IsNot Nothing Then
            Dim SaveDocHistoryFlag = DirectCast(SaveDocHistoryFlagControl, HiddenField)
            SaveDocHistoryFlag.Value = "0"
        End If

        If DOCS_EDIT.Contains(DocTypeURL.ToLower) Then
            If GetLock(LockID) Then
                If MyBase.SaveDoc() Then
                    SetLock(LockID)

                    ' AW: это говнокод кношно - но работает зае6iсь
                    If DocTypeURL.ToLower = "fund.ascx" AndAlso DataSetHeaderDataRow("PRESENCE_FLAG") = "b" Then
                        Dim cmd As New SqlClient.SqlCommand("UPDATE tblINVENTORY SET PRESENCE_FLAG = 'b' WHERE ISN_FUND = @ISN_FUND AND Deleted = 0")
                        cmd.Parameters.AddWithValue("@ISN_FUND", DataSetHeaderDataRow("ISN_FUND"))
                        AppSettings.ExecCommand(cmd)
                    End If

                    If DOCS_CALC.Contains(DocTypeURL.ToLower) AndAlso Not BasePage.IsModalDialog Then
                        Response.Redirect(BasePage.GetDocURL(DocTypeURL, DocID, False), False)
                    End If

                    Return True
                End If
            End If
        Else
            Return MyBase.SaveDoc()
        End If
        Return False
    End Function

    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        MyBase.OnLoad(e)

        If Request.QueryString("DocTypeURL") IsNot Nothing Then
            If DOCS_EDIT.Contains(Request.QueryString("DocTypeURL").ToLower) Then
                If Request.QueryString("ListDoc") Is Nothing Then
#If FC Or CFC Or GR Then
                    If Not GetLock(LockID) Then
                        For Each c In Controls
                            DisableControl(c)
                        Next
                    Else
                        ' Для ролей кроме админов, технологов, архивистов запрещаем редактирование архивов
                        If Not (AppSettings.UserSecurities.HasRole(New Guid("CA8E8667-9661-4669-BF2E-15D511D41ACD")) Or AppSettings.UserSecurities.HasRole(New Guid("EC3FE4E9-2132-4123-8969-15D511FDDD43")) Or AppSettings.UserSecurities.HasRole(New Guid("0A72C6D3-DD28-4A72-9C34-15D5126C5606")) Or AppSettings.UserInfo.UserSecurities.IsAdministrator) Then
                            For Each c In Controls
                                DisableControl(c)
                            Next
                        End If
                    End If
#Else
                    If Not GetLock(LockID) Then
                        For Each c In Controls
                            DisableControl(c)
                        Next
                    End If
#End If
                End If
            End If
        End If
    End Sub

    Public Overrides Sub DisableControl(ByVal TargetControl As System.Web.UI.Control)
        If TypeOf TargetControl Is RefFileUpload Then
            TargetControl.Visible = False
        ElseIf TypeOf TargetControl Is AjaxControlToolkit.CalendarExtender Then
            DirectCast(TargetControl, AjaxControlToolkit.CalendarExtender).Enabled = False
        Else
            MyBase.DisableControl(TargetControl)
        End If
    End Sub
    Private Sub EnableChildControls(ByVal ParentControl As Web.UI.Control)
        If ParentControl.HasControls Then
            For Each TargetControl As Web.UI.Control In ParentControl.Controls
                EnableControl(TargetControl)
            Next
        End If
    End Sub

    Private Sub EnableControl(ByVal TargetControl As System.Web.UI.Control)
        If TargetControl IsNot Nothing Then
            Select Case TargetControl.GetType.FullName
                Case "Equipage.WebUI.eqTextBox"
                    DirectCast(TargetControl, eqTextBox).ReadOnly = False
                Case "System.Web.UI.WebControls.DropDownList"
                    DirectCast(TargetControl, DropDownList).Enabled = True
                Case Else
                    If TypeOf TargetControl Is eqDocument Then
                        Dim TargetDocument = DirectCast(TargetControl, eqDocument)
                        TargetDocument.ListDocMode = eqBaseContainer.ListDocModes.None
                        TargetDocument.LoadDocMode = eqBaseContainer.LoadDocModes.NewWindow
                    ElseIf TypeOf TargetControl Is eqSpecification Then
                        Dim TargetSpecification = DirectCast(TargetControl, eqSpecification)
                        TargetSpecification.AllowDelete = True
                        TargetSpecification.AllowInsert = True
                        TargetSpecification.AllowReorder = True
                        EnableChildControls(TargetControl)
                    End If
            End Select
        End If
    End Sub

    Public Overrides Function GetControlValueProperty(ByVal Control As System.Web.UI.Control) As String
        Select Case Control.GetType.FullName
            Case "WebApplication.WebUI.eqDocument"
                Return "KeyValue"
            Case "WebApplication.WebUI_G.eqDocument"
                Return "KeyValue"
            Case "WebApplication.RefFile"
                Return "Value"
            Case Else
                Return MyBase.GetControlValueProperty(Control)
        End Select
    End Function

    Public Overrides Function ReadValueFromSpecificationRowControlToDataSet(ByVal SpecificationRow As Equipage.WebUI.eqSpecificationRow, ByVal FieldName As String) As Boolean
        If FieldName = "ISN_DOC_TYPE" AndAlso TypeOf SpecificationRow.FindControl("ISN_DOC_TYPE") Is Label Then
            Return True
        Else
            Return MyBase.ReadValueFromSpecificationRowControlToDataSet(SpecificationRow, FieldName)
        End If
    End Function

    Public Overrides ReadOnly Property DataSetSpecificationDataTable(ByVal SpecificationTableName As String) As System.Data.DataTable
        Get
            If SpecificationTableName = "vFUND_RENAME" Then
                MyBase.DataSetSpecificationDataTable(SpecificationTableName).DefaultView.Sort = "CREATE_DATE, DELETE_DATE"
                Return MyBase.DataSetSpecificationDataTable(SpecificationTableName)
            End If
            Return MyBase.DataSetSpecificationDataTable(SpecificationTableName)
        End Get
    End Property

#End Region

#Region "Блокировка документов"

    ''' <summary>
    ''' Сохраняем нулевой код блокировки страницы (на уровне ViewState)
    ''' </summary>
    Public Sub SetLock()
        ViewState("LockID_" & Guid.Empty.ToString("N")) = True
    End Sub

    ''' <summary>
    ''' Сохраняем код блокировки страницы (на уровне Session)
    ''' </summary>
    Public Sub SetLock(ByVal ID As Guid)
        Session("LockID_" & ID.ToString("N")) = True
    End Sub

    ''' <summary>
    ''' Получаем код блокировки страницы
    ''' </summary>
    Public Function GetLock(ByVal ID As Guid) As Boolean
        If ViewState("LockID_" & ID.ToString("N")) IsNot Nothing Then
            Return True
        End If
        If Session("LockID_" & ID.ToString("N")) IsNot Nothing Then
            ViewState("LockID_" & ID.ToString("N")) = True
            Session.Remove("LockID_" & ID.ToString("N"))
            Return True
        End If
        Return False
    End Function

    ''' <summary>
    ''' ID блокировки передается через URL
    ''' </summary>
    Public ReadOnly Property LockID() As Guid
        Get
            If Request.QueryString("LockID") IsNot Nothing Then
                Return New Guid(Request.QueryString("LockID"))
            End If
            Return Guid.Empty
        End Get
    End Property

    ''' <summary>
    ''' Открывает карточку в режиме редактирования
    ''' </summary>
    Public Sub Unlock()
        Dim NewLockID = Guid.NewGuid
        SetLock(NewLockID)
        Dim NewLockURL = BasePage.GetDocURL(DocTypeURL, DocID, BasePage.IsModal) & "&LockID=" & NewLockID.ToString
        Response.Redirect(NewLockURL, False)
    End Sub

#End Region

#Region "Спецификации"

    Public Sub CheckSpecifications()
        If ViewState("CheckSpecifikations") Is Nothing Then
            If InStr("FUND.ascx;INVENTORY.ascx;ARCHIVE_PASSPORT.ascx;DEPOSIT.ascx", DocTypeURL, CompareMethod.Text) > 0 Then
                Dim DocTypeCommand As New SqlCommand("SELECT NULL ISN_DOC_TYPE, 'Всего' NAME UNION ALL SELECT TOP 10 [ISN_DOC_TYPE], [NAME] FROM [tblDOC_TYPE_CL] ORDER BY [ISN_DOC_TYPE]")
                Dim DocTypeTable = AppSettings.GetDataTable(DocTypeCommand)
                Dim DocTypeFullCommand As New SqlCommand("SELECT NULL ISN_DOC_TYPE, 'Всего' NAME UNION ALL SELECT [ISN_DOC_TYPE], [NAME] FROM [tblDOC_TYPE_CL]")
                Dim DocTypeFullTable = AppSettings.GetDataTable(DocTypeFullCommand)
                Dim SpecNames = New String() {}

                If DocTypeURL.ToUpper = "FUND.ASCX" Then
                    SpecNames = New String() {"vFUND_DOCUMENT_STATS", "vFUND_DOCUMENT_STATS_P", "vFUND_DOCUMENT_STATS_A", "vFUND_DOCUMENT_STATS_M", "vFUND_DOCUMENT_STATS_E"}
                ElseIf DocTypeURL.ToUpper = "INVENTORY.ASCX" Then
                    SpecNames = New String() {"vINVENTORY_DOCUMENT_STATS", "vINVENTORY_DOCUMENT_STATS_P", "vINVENTORY_DOCUMENT_STATS_A", "vINVENTORY_DOCUMENT_STATS_M", "vINVENTORY_DOCUMENT_STATS_E"}
                ElseIf DocTypeURL.ToUpper = "ARCHIVE_PASSPORT.ASCX" Then
                    SpecNames = New String() {"vARCHIVE_STATS1", "vARCHIVE_STATS_P1", "vARCHIVE_STATS_A1", "vARCHIVE_STATS_E1", "vARCHIVE_STATS_M1"}
                ElseIf DocTypeURL.ToUpper = "DEPOSIT.ASCX" Then
                    SpecNames = New String() {"vDEPOSIT_DOC_TYPE_P", "vDEPOSIT_DOC_TYPE_A", "vDEPOSIT_DOC_TYPE_E", "vDEPOSIT_DOC_TYPE_M"}
                End If

                For Each Name In SpecNames
                    Dim Spec = DataSetSpecificationDataTable(Name)

                    If Spec.Rows.Count <> DocTypeTable.Rows.Count Then
                        Dim DocTypeView As IEnumerable(Of DataRow)

                        If Name.TrimEnd("1", "2", "3").EndsWith("STATS") Then
                            DocTypeView = DocTypeTable.Rows.OfType(Of DataRow).Take(1)
                        ElseIf Name.TrimEnd("1", "2", "3").EndsWith("STATS_P") Then
                            DocTypeView = DocTypeTable.Rows.OfType(Of DataRow).Take(5)
                        ElseIf Name.TrimEnd("1", "2", "3").EndsWith("STATS_A") Then
                            DocTypeView = DocTypeTable.Rows.OfType(Of DataRow).Take(1).Union(DocTypeTable.Rows.OfType(Of DataRow).Skip(5).Take(4))
                        ElseIf Name.TrimEnd("1", "2", "3").EndsWith("STATS_M") Then
                            DocTypeView = DocTypeTable.Rows.OfType(Of DataRow).Skip(9).Take(1)
                        ElseIf Name.EndsWith("TYPE_P") Then
                            DocTypeView = DocTypeTable.Rows.OfType(Of DataRow).Take(5)
                        ElseIf Name.EndsWith("TYPE_A") Then
                            DocTypeView = DocTypeTable.Rows.OfType(Of DataRow).Take(1).Union(DocTypeTable.Rows.OfType(Of DataRow).Skip(5).Take(4))
                        ElseIf Name.EndsWith("TYPE_M") Then
                            DocTypeView = DocTypeTable.Rows.OfType(Of DataRow).Skip(9).Take(1)
                        ElseIf Name.EndsWith("TYPE_E") Then
                            DocTypeView = DocTypeTable.Rows.OfType(Of DataRow).Take(1).Union(DocTypeTable.Rows.OfType(Of DataRow).Skip(4).Take(4))
                        Else
                            DocTypeView = DocTypeTable.Rows.OfType(Of DataRow).Take(1).Union(DocTypeTable.Rows.OfType(Of DataRow).Skip(4).Take(5))
                        End If

                        For Each DocTypeRow As DataRow In DocTypeView
                            Dim FindedRow As DataRow = Nothing

                            For Each SpecRow As DataRow In Spec.Rows
                                Dim DocTypeFullView = DocTypeFullTable.AsEnumerable.Where(Function(F) F("NAME") = DocTypeRow("NAME"))

                                For Each DocTypeFullRow As DataRow In DocTypeFullView
                                    If SpecRow("ISN_DOC_TYPE").ToString = DocTypeFullRow("ISN_DOC_TYPE").ToString Then
                                        FindedRow = SpecRow
                                        Exit For
                                    End If
                                Next
                            Next

                            If FindedRow Is Nothing Then
                                Dim NewSpecRow As DataRow = Spec.NewRow
                                NewSpecRow("ID") = Guid.NewGuid
                                NewSpecRow("CreationDateTime") = Now
                                NewSpecRow("OwnerID") = AppSettings.UserInfo.UserID
                                NewSpecRow("ISN_DOC_TYPE") = DocTypeRow("ISN_DOC_TYPE")
                                Spec.Rows.Add(NewSpecRow)

                                If DocTypeURL = "ARCHIVE_PASSPORT.ascx" Then
                                    ' Copy to 2
                                    Dim Spec2 = DataSetSpecificationDataTable(Name.Replace("1", "2"))
                                    Dim NewSpecRow2 = Spec2.NewRow
                                    NewSpecRow2("ID") = NewSpecRow("ID")
                                    NewSpecRow2("CreationDateTime") = NewSpecRow("CreationDateTime")
                                    NewSpecRow2("OwnerID") = NewSpecRow("OwnerID")
                                    NewSpecRow2("ISN_DOC_TYPE") = NewSpecRow("ISN_DOC_TYPE")
                                    Spec2.Rows.Add(NewSpecRow2)
                                    NewSpecRow2.AcceptChanges()

                                    ' Copy to 3
                                    Dim Spec3 = DataSetSpecificationDataTable(Name.Replace("1", "3"))
                                    Dim NewSpecRow3 = Spec3.NewRow
                                    NewSpecRow3("ID") = NewSpecRow("ID")
                                    NewSpecRow3("CreationDateTime") = NewSpecRow("CreationDateTime")
                                    NewSpecRow3("OwnerID") = NewSpecRow("OwnerID")
                                    NewSpecRow3("ISN_DOC_TYPE") = NewSpecRow("ISN_DOC_TYPE")
                                    Spec3.Rows.Add(NewSpecRow3)
                                    NewSpecRow3.AcceptChanges()
                                End If
                            End If
                        Next

                        PutValuesFromDataSetToSpecificationControls(Name)
                        If DocTypeURL = "ARCHIVE_PASSPORT.ascx" Then
                            PutValuesFromDataSetToSpecificationControls(Name.Replace("1", "2"))
                            PutValuesFromDataSetToSpecificationControls(Name.Replace("1", "3"))
                        End If
                    End If
                Next
            End If
            ViewState("CheckSpecifikations") = True
        End If
    End Sub

    Private Sub StatsFieldDisable(ByVal Stats As eqSpecification, ByVal Field As String, ByVal Type As String, Optional ByVal Types As String() = Nothing)
        Dim r = Stats.Rows.FirstOrDefault(Function(F) DirectCast(F.FindControl("ISN_DOC_TYPE"), Label).Text.ToLower.Contains(Type.ToLower))
        If r IsNot Nothing Then
            Dim c = DirectCast(r.FindControl(Field), eqTextBox)
            If c IsNot Nothing Then
                c.ReadOnly = True
                If Types IsNot Nothing Then
                    For Each T In Types
                        If T.ToLower.Contains(Type.ToLower) Then
                            c.ReadOnly = False
                            Exit For
                        End If
                    Next
                End If
            End If
        End If

    End Sub

    ''' <summary>
    ''' Блокирует поля спецификаций DOCUMENT_STATS
    ''' </summary>
    ''' <param name="P">Документы на бумажной основе</param>
    ''' <param name="A">Аудиовизуальные документы на традиционных носителях</param>
    ''' <param name="E">Документы на электронных носителях</param>
    ''' <param name="M">Микроформы на пр. подл.</param>
    ''' <param name="T">Всего</param>
    ''' <param name="D">Массив типов документов</param>
    Public Sub StatsDisable(ByVal P As eqSpecification, ByVal A As eqSpecification, ByVal E As eqSpecification, ByVal M As eqSpecification, ByVal T As eqSpecification, Optional ByVal D As String() = Nothing)
        Const TYPE_PHOTO = "фотодокументы"
        Const TYPE_PHONO = "фонодокументы"
        Const TYPE_MOVIE = "кинодокументы"
        Const TYPE_VIDEO = "видеодокументы"
        Const TYPE_UD = "управленческая документация"
        Const TYPE_LS = "личному составу"
        Const TYPE_LP = "личного происхождения"
        Const TYPE_NTD = "нтд"
        Const TYPE_MICRO = "микроформы"
        Const TYPE_TOTAL = "всего"

        Select Case DocTypeURL.ToLower
            Case "fund.ascx", "inventory.ascx"
                ' Блочим поля в зависимости от типа

                StatsFieldDisable(P, "UNIT_COUNT", TYPE_UD, D)
                StatsFieldDisable(P, "UNIT_UNDESCRIBED", TYPE_UD, D)
                StatsFieldDisable(P, "UNIT_OC_COUNT", TYPE_UD, D)
                StatsFieldDisable(P, "UNITS_UNIQUE", TYPE_UD, D)
                StatsFieldDisable(P, "UNIT_HAS_SF", TYPE_UD, D)
                StatsFieldDisable(P, "UNIT_HAS_FP", TYPE_UD, D)
                StatsFieldDisable(P, "UNITS_NOT_FOUND", TYPE_UD, D)
                StatsFieldDisable(P, "SECRET_UNITS", TYPE_UD, D)
                StatsFieldDisable(P, "UNITS_CATALOGUED", TYPE_UD, D)

                StatsFieldDisable(P, "UNIT_COUNT", TYPE_LS, D)
                StatsFieldDisable(P, "UNIT_UNDESCRIBED", TYPE_LS, D)
                'StatsFieldDisable(P, "UNIT_OC_COUNT", TYPE_LS, D )
                'StatsFieldDisable(P, "UNITS_UNIQUE", TYPE_LS, D )
                'StatsFieldDisable(P, "UNIT_HAS_SF", TYPE_LS, D )
                StatsFieldDisable(P, "UNIT_HAS_FP", TYPE_LS, D)
                StatsFieldDisable(P, "UNITS_NOT_FOUND", TYPE_LS, D)
                StatsFieldDisable(P, "SECRET_UNITS", TYPE_LS, D)
                StatsFieldDisable(P, "UNITS_CATALOGUED", TYPE_LS, D)

                StatsFieldDisable(P, "UNIT_COUNT", TYPE_LP, D)
                StatsFieldDisable(P, "UNIT_UNDESCRIBED", TYPE_LP, D)
                StatsFieldDisable(P, "UNIT_OC_COUNT", TYPE_LP, D)
                StatsFieldDisable(P, "UNITS_UNIQUE", TYPE_LP, D)
                StatsFieldDisable(P, "UNIT_HAS_SF", TYPE_LP, D)
                StatsFieldDisable(P, "UNIT_HAS_FP", TYPE_LP, D)
                StatsFieldDisable(P, "UNITS_NOT_FOUND", TYPE_LP, D)
                'StatsFieldDisable(P, "SECRET_UNITS", TYPE_LP, D )
                StatsFieldDisable(P, "UNITS_CATALOGUED", TYPE_LP, D)

                StatsFieldDisable(P, "UNIT_COUNT", TYPE_NTD, D)
                StatsFieldDisable(P, "UNIT_UNDESCRIBED", TYPE_NTD, D)
                StatsFieldDisable(P, "UNIT_OC_COUNT", TYPE_NTD, D)
                StatsFieldDisable(P, "UNITS_UNIQUE", TYPE_NTD, D)
                StatsFieldDisable(P, "UNIT_HAS_SF", TYPE_NTD, D)
                StatsFieldDisable(P, "UNIT_HAS_FP", TYPE_NTD, D)
                StatsFieldDisable(P, "UNITS_NOT_FOUND", TYPE_NTD, D)
                StatsFieldDisable(P, "SECRET_UNITS", TYPE_NTD, D)
                StatsFieldDisable(P, "UNITS_CATALOGUED", TYPE_NTD, D)

                StatsFieldDisable(A, "UNIT_COUNT", TYPE_PHOTO, D)
                StatsFieldDisable(A, "UNIT_UNDESCRIBED", TYPE_PHOTO, D)
                StatsFieldDisable(A, "UNIT_OC_COUNT", TYPE_PHOTO, D)
                StatsFieldDisable(A, "UNITS_UNIQUE", TYPE_PHOTO, D)
                StatsFieldDisable(A, "UNIT_HAS_SF", TYPE_PHOTO, D)
                StatsFieldDisable(A, "UNIT_HAS_FP", TYPE_PHOTO, D)
                StatsFieldDisable(A, "UNITS_NOT_FOUND", TYPE_PHOTO, D)
                StatsFieldDisable(A, "SECRET_UNITS", TYPE_PHOTO, D)
                StatsFieldDisable(A, "UNITS_CATALOGUED", TYPE_PHOTO, D)
                'StatsFieldDisable(A, "REG_UNIT", TYPE_PHOTO, D )
                'StatsFieldDisable(A, "REG_UNIT_UNDESCRIBED", TYPE_PHOTO, D )
                'StatsFieldDisable(A, "REG_UNIT_OC", TYPE_PHOTO, D )
                'StatsFieldDisable(A, "REG_UNITS_UNIQUE", TYPE_PHOTO, D )
                'StatsFieldDisable(A, "REG_UNIT_HAS_SF", TYPE_PHOTO, D )
                'StatsFieldDisable(A, "REG_UNIT_HAS_FP", TYPE_PHOTO, D )
                'StatsFieldDisable(A, "REG_UNITS_NOT_FOUND", TYPE_PHOTO, D )
                'StatsFieldDisable(A, "SECRET_REG_UNITS", TYPE_PHOTO, D )
                'StatsFieldDisable(A, "REG_UNITS_CTALOGUE", TYPE_PHOTO, D )

                StatsFieldDisable(A, "UNIT_COUNT", TYPE_PHONO, D)
                StatsFieldDisable(A, "UNIT_UNDESCRIBED", TYPE_PHONO, D)
                StatsFieldDisable(A, "UNIT_OC_COUNT", TYPE_PHONO, D)
                StatsFieldDisable(A, "UNITS_UNIQUE", TYPE_PHONO, D)
                StatsFieldDisable(A, "UNIT_HAS_SF", TYPE_PHONO, D)
                StatsFieldDisable(A, "UNIT_HAS_FP", TYPE_PHONO, D)
                StatsFieldDisable(A, "UNITS_NOT_FOUND", TYPE_PHONO, D)
                StatsFieldDisable(A, "SECRET_UNITS", TYPE_PHONO, D)
                'StatsFieldDisable(A, "UNITS_CATALOGUED", TYPE_PHONO, D )
                StatsFieldDisable(A, "REG_UNIT", TYPE_PHONO, D)
                StatsFieldDisable(A, "REG_UNIT_UNDESCRIBED", TYPE_PHONO, D)
                StatsFieldDisable(A, "REG_UNIT_OC", TYPE_PHONO, D)
                StatsFieldDisable(A, "REG_UNITS_UNIQUE", TYPE_PHONO, D)
                StatsFieldDisable(A, "REG_UNIT_HAS_SF", TYPE_PHONO, D)
                StatsFieldDisable(A, "REG_UNIT_HAS_FP", TYPE_PHONO, D)
                StatsFieldDisable(A, "REG_UNITS_NOT_FOUND", TYPE_PHONO, D)
                StatsFieldDisable(A, "SECRET_REG_UNITS", TYPE_PHONO, D)
                StatsFieldDisable(A, "REG_UNITS_CTALOGUE", TYPE_PHONO, D)

                StatsFieldDisable(A, "UNIT_COUNT", TYPE_MOVIE, D)
                StatsFieldDisable(A, "UNIT_UNDESCRIBED", TYPE_MOVIE, D)
                StatsFieldDisable(A, "UNIT_OC_COUNT", TYPE_MOVIE, D)
                StatsFieldDisable(A, "UNITS_UNIQUE", TYPE_MOVIE, D)
                StatsFieldDisable(A, "UNIT_HAS_SF", TYPE_MOVIE, D)
                StatsFieldDisable(A, "UNIT_HAS_FP", TYPE_MOVIE, D)
                StatsFieldDisable(A, "UNITS_NOT_FOUND", TYPE_MOVIE, D)
                StatsFieldDisable(A, "SECRET_UNITS", TYPE_MOVIE, D)
                'StatsFieldDisable(A, "UNITS_CATALOGUED", TYPE_MOVIE, D )
                StatsFieldDisable(A, "REG_UNIT", TYPE_MOVIE, D)
                StatsFieldDisable(A, "REG_UNIT_UNDESCRIBED", TYPE_MOVIE, D)
                StatsFieldDisable(A, "REG_UNIT_OC", TYPE_MOVIE, D)
                StatsFieldDisable(A, "REG_UNITS_UNIQUE", TYPE_MOVIE, D)
                StatsFieldDisable(A, "REG_UNIT_HAS_SF", TYPE_MOVIE, D)
                StatsFieldDisable(A, "REG_UNIT_HAS_FP", TYPE_MOVIE, D)
                StatsFieldDisable(A, "REG_UNITS_NOT_FOUND", TYPE_MOVIE, D)
                StatsFieldDisable(A, "SECRET_REG_UNITS", TYPE_MOVIE, D)
                StatsFieldDisable(A, "REG_UNITS_CTALOGUE", TYPE_MOVIE, D)

                StatsFieldDisable(A, "UNIT_COUNT", TYPE_VIDEO, D)
                StatsFieldDisable(A, "UNIT_UNDESCRIBED", TYPE_VIDEO, D)
                StatsFieldDisable(A, "UNIT_OC_COUNT", TYPE_VIDEO, D)
                StatsFieldDisable(A, "UNITS_UNIQUE", TYPE_VIDEO, D)
                StatsFieldDisable(A, "UNIT_HAS_SF", TYPE_VIDEO, D)
                StatsFieldDisable(A, "UNIT_HAS_FP", TYPE_VIDEO, D)
                StatsFieldDisable(A, "UNITS_NOT_FOUND", TYPE_VIDEO, D)
                StatsFieldDisable(A, "SECRET_UNITS", TYPE_VIDEO, D)
                'StatsFieldDisable(A, "UNITS_CATALOGUED", TYPE_VIDEO, D )
                StatsFieldDisable(A, "REG_UNIT", TYPE_VIDEO, D)
                StatsFieldDisable(A, "REG_UNIT_UNDESCRIBED", TYPE_VIDEO, D)
                StatsFieldDisable(A, "REG_UNIT_OC", TYPE_VIDEO, D)
                StatsFieldDisable(A, "REG_UNITS_UNIQUE", TYPE_VIDEO, D)
                StatsFieldDisable(A, "REG_UNIT_HAS_SF", TYPE_VIDEO, D)
                StatsFieldDisable(A, "REG_UNIT_HAS_FP", TYPE_VIDEO, D)
                StatsFieldDisable(A, "REG_UNITS_NOT_FOUND", TYPE_VIDEO, D)
                StatsFieldDisable(A, "SECRET_REG_UNITS", TYPE_VIDEO, D)
                StatsFieldDisable(A, "REG_UNITS_CTALOGUE", TYPE_VIDEO, D)

                StatsFieldDisable(M, "UNIT_COUNT", TYPE_MICRO, D)
                StatsFieldDisable(M, "UNIT_UNDESCRIBED", TYPE_MICRO, D)
                StatsFieldDisable(M, "UNIT_OC_COUNT", TYPE_MICRO, D)
                StatsFieldDisable(M, "UNITS_UNIQUE", TYPE_MICRO, D)
                StatsFieldDisable(M, "UNIT_HAS_SF", TYPE_MICRO, D)
                StatsFieldDisable(M, "UNIT_HAS_FP", TYPE_MICRO, D)
                StatsFieldDisable(M, "UNITS_NOT_FOUND", TYPE_MICRO, D)
                'StatsFieldDisable(M, "SECRET_UNITS", TYPE_MICRO, D )
                StatsFieldDisable(M, "UNITS_CATALOGUED", TYPE_MICRO, D)

                StatsFieldDisable(E, "UNIT_COUNT", TYPE_NTD, D)
                StatsFieldDisable(E, "UNIT_UNDESCRIBED", TYPE_NTD, D)
                StatsFieldDisable(E, "UNIT_OC_COUNT", TYPE_NTD, D)
                StatsFieldDisable(E, "UNITS_UNIQUE", TYPE_NTD, D)
                StatsFieldDisable(E, "UNIT_HAS_SF", TYPE_NTD, D)
                StatsFieldDisable(E, "UNIT_HAS_FP", TYPE_NTD, D)
                StatsFieldDisable(E, "UNITS_NOT_FOUND", TYPE_NTD, D)
                StatsFieldDisable(E, "SECRET_UNITS", TYPE_NTD, D)
                StatsFieldDisable(E, "UNITS_CATALOGUED", TYPE_NTD, D)
                StatsFieldDisable(E, "REG_UNIT", TYPE_NTD, D)
                StatsFieldDisable(E, "REG_UNIT_UNDESCRIBED", TYPE_NTD, D)
                StatsFieldDisable(E, "REG_UNIT_OC", TYPE_NTD, D)
                StatsFieldDisable(E, "REG_UNITS_UNIQUE", TYPE_NTD, D)
                StatsFieldDisable(E, "REG_UNIT_HAS_SF", TYPE_NTD, D)
                StatsFieldDisable(E, "REG_UNIT_HAS_FP", TYPE_NTD, D)
                StatsFieldDisable(E, "REG_UNITS_NOT_FOUND", TYPE_NTD, D)
                StatsFieldDisable(E, "SECRET_REG_UNITS", TYPE_NTD, D)
                StatsFieldDisable(E, "REG_UNITS_CTALOGUE", TYPE_NTD, D)

                StatsFieldDisable(E, "UNIT_COUNT", TYPE_PHOTO, D)
                StatsFieldDisable(E, "UNIT_UNDESCRIBED", TYPE_PHOTO, D)
                StatsFieldDisable(E, "UNIT_OC_COUNT", TYPE_PHOTO, D)
                StatsFieldDisable(E, "UNITS_UNIQUE", TYPE_PHOTO, D)
                StatsFieldDisable(E, "UNIT_HAS_SF", TYPE_PHOTO, D)
                StatsFieldDisable(E, "UNIT_HAS_FP", TYPE_PHOTO, D)
                StatsFieldDisable(E, "UNITS_NOT_FOUND", TYPE_PHOTO, D)
                StatsFieldDisable(E, "SECRET_UNITS", TYPE_PHOTO, D)
                'StatsFieldDisable(E, "UNITS_CATALOGUED", TYPE_PHOTO, D )
                StatsFieldDisable(E, "REG_UNIT", TYPE_PHOTO, D)
                StatsFieldDisable(E, "REG_UNIT_UNDESCRIBED", TYPE_PHOTO, D)
                StatsFieldDisable(E, "REG_UNIT_OC", TYPE_PHOTO, D)
                StatsFieldDisable(E, "REG_UNITS_UNIQUE", TYPE_PHOTO, D)
                StatsFieldDisable(E, "REG_UNIT_HAS_SF", TYPE_PHOTO, D)
                StatsFieldDisable(E, "REG_UNIT_HAS_FP", TYPE_PHOTO, D)
                StatsFieldDisable(E, "REG_UNITS_NOT_FOUND", TYPE_PHOTO, D)
                StatsFieldDisable(E, "SECRET_REG_UNITS", TYPE_PHOTO, D)
                StatsFieldDisable(E, "REG_UNITS_CTALOGUE", TYPE_PHOTO, D)

                StatsFieldDisable(E, "UNIT_COUNT", TYPE_PHONO, D)
                StatsFieldDisable(E, "UNIT_UNDESCRIBED", TYPE_PHONO, D)
                StatsFieldDisable(E, "UNIT_OC_COUNT", TYPE_PHONO, D)
                StatsFieldDisable(E, "UNITS_UNIQUE", TYPE_PHONO, D)
                StatsFieldDisable(E, "UNIT_HAS_SF", TYPE_PHONO, D)
                StatsFieldDisable(E, "UNIT_HAS_FP", TYPE_PHONO, D)
                StatsFieldDisable(E, "UNITS_NOT_FOUND", TYPE_PHONO, D)
                StatsFieldDisable(E, "SECRET_UNITS", TYPE_PHONO, D)
                StatsFieldDisable(E, "UNITS_CATALOGUED", TYPE_PHONO, D)
                StatsFieldDisable(E, "REG_UNIT", TYPE_PHONO, D)
                StatsFieldDisable(E, "REG_UNIT_UNDESCRIBED", TYPE_PHONO, D)
                StatsFieldDisable(E, "REG_UNIT_OC", TYPE_PHONO, D)
                StatsFieldDisable(E, "REG_UNITS_UNIQUE", TYPE_PHONO, D)
                StatsFieldDisable(E, "REG_UNIT_HAS_SF", TYPE_PHONO, D)
                StatsFieldDisable(E, "REG_UNIT_HAS_FP", TYPE_PHONO, D)
                StatsFieldDisable(E, "REG_UNITS_NOT_FOUND", TYPE_PHONO, D)
                StatsFieldDisable(E, "SECRET_REG_UNITS", TYPE_PHONO, D)
                StatsFieldDisable(E, "REG_UNITS_CTALOGUE", TYPE_PHONO, D)

                StatsFieldDisable(E, "UNIT_COUNT", TYPE_MOVIE, D)
                StatsFieldDisable(E, "UNIT_UNDESCRIBED", TYPE_MOVIE, D)
                StatsFieldDisable(E, "UNIT_OC_COUNT", TYPE_MOVIE, D)
                StatsFieldDisable(E, "UNITS_UNIQUE", TYPE_MOVIE, D)
                StatsFieldDisable(E, "UNIT_HAS_SF", TYPE_MOVIE, D)
                StatsFieldDisable(E, "UNIT_HAS_FP", TYPE_MOVIE, D)
                StatsFieldDisable(E, "UNITS_NOT_FOUND", TYPE_MOVIE, D)
                StatsFieldDisable(E, "SECRET_UNITS", TYPE_MOVIE, D)
                StatsFieldDisable(E, "UNITS_CATALOGUED", TYPE_MOVIE, D)
                StatsFieldDisable(E, "REG_UNIT", TYPE_MOVIE, D)
                StatsFieldDisable(E, "REG_UNIT_UNDESCRIBED", TYPE_MOVIE, D)
                StatsFieldDisable(E, "REG_UNIT_OC", TYPE_MOVIE, D)
                StatsFieldDisable(E, "REG_UNITS_UNIQUE", TYPE_MOVIE, D)
                StatsFieldDisable(E, "REG_UNIT_HAS_SF", TYPE_MOVIE, D)
                StatsFieldDisable(E, "REG_UNIT_HAS_FP", TYPE_MOVIE, D)
                StatsFieldDisable(E, "REG_UNITS_NOT_FOUND", TYPE_MOVIE, D)
                StatsFieldDisable(E, "SECRET_REG_UNITS", TYPE_MOVIE, D)
                StatsFieldDisable(E, "REG_UNITS_CTALOGUE", TYPE_MOVIE, D)

                StatsFieldDisable(E, "UNIT_COUNT", TYPE_VIDEO, D)
                StatsFieldDisable(E, "UNIT_UNDESCRIBED", TYPE_VIDEO, D)
                StatsFieldDisable(E, "UNIT_OC_COUNT", TYPE_VIDEO, D)
                StatsFieldDisable(E, "UNITS_UNIQUE", TYPE_VIDEO, D)
                StatsFieldDisable(E, "UNIT_HAS_SF", TYPE_VIDEO, D)
                StatsFieldDisable(E, "UNIT_HAS_FP", TYPE_VIDEO, D)
                StatsFieldDisable(E, "UNITS_NOT_FOUND", TYPE_VIDEO, D)
                StatsFieldDisable(E, "SECRET_UNITS", TYPE_VIDEO, D)
                StatsFieldDisable(E, "UNITS_CATALOGUED", TYPE_VIDEO, D)
                StatsFieldDisable(E, "REG_UNIT", TYPE_VIDEO, D)
                StatsFieldDisable(E, "REG_UNIT_UNDESCRIBED", TYPE_VIDEO, D)
                StatsFieldDisable(E, "REG_UNIT_OC", TYPE_VIDEO, D)
                StatsFieldDisable(E, "REG_UNITS_UNIQUE", TYPE_VIDEO, D)
                StatsFieldDisable(E, "REG_UNIT_HAS_SF", TYPE_VIDEO, D)
                StatsFieldDisable(E, "REG_UNIT_HAS_FP", TYPE_VIDEO, D)
                StatsFieldDisable(E, "REG_UNITS_NOT_FOUND", TYPE_VIDEO, D)
                StatsFieldDisable(E, "SECRET_REG_UNITS", TYPE_VIDEO, D)
                StatsFieldDisable(E, "REG_UNITS_CTALOGUE", TYPE_VIDEO, D)

                ' Блочим поля первой и второй колонок

                StatsFieldDisable(P, "UNIT_INVENTORY", TYPE_UD)
                StatsFieldDisable(P, "UNIT_REGISTERED", TYPE_UD)
                StatsFieldDisable(P, "UNIT_INVENTORY", TYPE_LS)
                StatsFieldDisable(P, "UNIT_REGISTERED", TYPE_LS)
                StatsFieldDisable(P, "UNIT_INVENTORY", TYPE_LP)
                StatsFieldDisable(P, "UNIT_REGISTERED", TYPE_LP)
                StatsFieldDisable(P, "UNIT_INVENTORY", TYPE_NTD)
                StatsFieldDisable(P, "UNIT_REGISTERED", TYPE_NTD)
                StatsFieldDisable(P, "UNIT_INVENTORY", TYPE_TOTAL)
                StatsFieldDisable(P, "UNIT_REGISTERED", TYPE_TOTAL)

                StatsFieldDisable(A, "UNIT_INVENTORY", TYPE_PHOTO)
                StatsFieldDisable(A, "UNIT_REGISTERED", TYPE_PHOTO)
                StatsFieldDisable(A, "REG_UNIT_INVENTORY", TYPE_PHOTO)
                StatsFieldDisable(A, "REG_UNIT_REGISTERED", TYPE_PHOTO)
                StatsFieldDisable(A, "UNIT_INVENTORY", TYPE_PHONO)
                StatsFieldDisable(A, "UNIT_REGISTERED", TYPE_PHONO)
                StatsFieldDisable(A, "REG_UNIT_INVENTORY", TYPE_PHONO)
                StatsFieldDisable(A, "REG_UNIT_REGISTERED", TYPE_PHONO)
                StatsFieldDisable(A, "UNIT_INVENTORY", TYPE_MOVIE)
                StatsFieldDisable(A, "UNIT_REGISTERED", TYPE_MOVIE)
                StatsFieldDisable(A, "REG_UNIT_INVENTORY", TYPE_MOVIE)
                StatsFieldDisable(A, "REG_UNIT_REGISTERED", TYPE_MOVIE)
                StatsFieldDisable(A, "UNIT_INVENTORY", TYPE_VIDEO)
                StatsFieldDisable(A, "UNIT_REGISTERED", TYPE_VIDEO)
                StatsFieldDisable(A, "REG_UNIT_INVENTORY", TYPE_VIDEO)
                StatsFieldDisable(A, "REG_UNIT_REGISTERED", TYPE_VIDEO)
                StatsFieldDisable(A, "UNIT_INVENTORY", TYPE_TOTAL)
                StatsFieldDisable(A, "UNIT_REGISTERED", TYPE_TOTAL)
                StatsFieldDisable(A, "REG_UNIT_INVENTORY", TYPE_TOTAL)
                StatsFieldDisable(A, "REG_UNIT_REGISTERED", TYPE_TOTAL)

                StatsFieldDisable(M, "UNIT_INVENTORY", TYPE_MICRO)
                StatsFieldDisable(M, "UNIT_REGISTERED", TYPE_MICRO)

                StatsFieldDisable(E, "UNIT_INVENTORY", TYPE_NTD)
                StatsFieldDisable(E, "UNIT_REGISTERED", TYPE_NTD)
                StatsFieldDisable(E, "REG_UNIT_INVENTORY", TYPE_NTD)
                StatsFieldDisable(E, "REG_UNIT_REGISTERED", TYPE_NTD)
                StatsFieldDisable(E, "UNIT_INVENTORY", TYPE_PHOTO)
                StatsFieldDisable(E, "UNIT_REGISTERED", TYPE_PHOTO)
                StatsFieldDisable(E, "REG_UNIT_INVENTORY", TYPE_PHOTO)
                StatsFieldDisable(E, "REG_UNIT_REGISTERED", TYPE_PHOTO)
                StatsFieldDisable(E, "UNIT_INVENTORY", TYPE_PHONO)
                StatsFieldDisable(E, "UNIT_REGISTERED", TYPE_PHONO)
                StatsFieldDisable(E, "REG_UNIT_INVENTORY", TYPE_PHONO)
                StatsFieldDisable(E, "REG_UNIT_REGISTERED", TYPE_PHONO)
                StatsFieldDisable(E, "UNIT_INVENTORY", TYPE_MOVIE)
                StatsFieldDisable(E, "UNIT_REGISTERED", TYPE_MOVIE)
                StatsFieldDisable(E, "REG_UNIT_INVENTORY", TYPE_MOVIE)
                StatsFieldDisable(E, "REG_UNIT_REGISTERED", TYPE_MOVIE)
                StatsFieldDisable(E, "UNIT_INVENTORY", TYPE_VIDEO)
                StatsFieldDisable(E, "UNIT_REGISTERED", TYPE_VIDEO)
                StatsFieldDisable(E, "REG_UNIT_INVENTORY", TYPE_VIDEO)
                StatsFieldDisable(E, "REG_UNIT_REGISTERED", TYPE_VIDEO)
                StatsFieldDisable(E, "UNIT_INVENTORY", TYPE_TOTAL)
                StatsFieldDisable(E, "UNIT_REGISTERED", TYPE_TOTAL)
                StatsFieldDisable(E, "REG_UNIT_INVENTORY", TYPE_TOTAL)
                StatsFieldDisable(E, "REG_UNIT_REGISTERED", TYPE_TOTAL)

                StatsFieldDisable(T, "UNIT_INVENTORY", TYPE_TOTAL)
                StatsFieldDisable(T, "UNIT_REGISTERED", TYPE_TOTAL)
                StatsFieldDisable(T, "REG_UNIT_INVENTORY", TYPE_TOTAL)
                StatsFieldDisable(T, "REG_UNIT_REGISTERED", TYPE_TOTAL)

                ' Блочим поля хз по какому принципу

                StatsFieldDisable(P, "UNIT_OC_COUNT", TYPE_LS)
                StatsFieldDisable(P, "UNITS_UNIQUE", TYPE_LS)
                StatsFieldDisable(P, "UNIT_HAS_SF", TYPE_LS)
                StatsFieldDisable(P, "SECRET_UNITS", TYPE_LP)
                StatsFieldDisable(A, "REG_UNIT_UNDESCRIBED", TYPE_TOTAL)
                StatsFieldDisable(A, "REG_UNIT_UNDESCRIBED", TYPE_PHONO)
                StatsFieldDisable(A, "REG_UNIT_UNDESCRIBED", TYPE_MOVIE)
                StatsFieldDisable(A, "REG_UNIT_UNDESCRIBED", TYPE_VIDEO)
                StatsFieldDisable(A, "UNITS_CATALOGUED", TYPE_PHONO)
                StatsFieldDisable(A, "UNITS_CATALOGUED", TYPE_MOVIE)
                StatsFieldDisable(A, "UNITS_CATALOGUED", TYPE_VIDEO)
                StatsFieldDisable(A, "REG_UNIT", TYPE_PHOTO)
                StatsFieldDisable(A, "REG_UNIT_UNDESCRIBED", TYPE_PHOTO)
                StatsFieldDisable(A, "REG_UNIT_OC", TYPE_PHOTO)
                StatsFieldDisable(A, "REG_UNITS_UNIQUE", TYPE_PHOTO)
                StatsFieldDisable(A, "REG_UNIT_HAS_SF", TYPE_PHOTO)
                StatsFieldDisable(A, "REG_UNIT_HAS_FP", TYPE_PHOTO)
                StatsFieldDisable(A, "REG_UNITS_NOT_FOUND", TYPE_PHOTO)
                StatsFieldDisable(A, "SECRET_REG_UNITS", TYPE_PHOTO)
                StatsFieldDisable(A, "REG_UNITS_CTALOGUE", TYPE_PHOTO)
                StatsFieldDisable(M, "SECRET_UNITS", TYPE_MICRO)
                'StatsFieldDisable(E, "REG_UNIT", TYPE_TOTAL)
                StatsFieldDisable(E, "REG_UNIT_UNDESCRIBED", TYPE_PHOTO)
                StatsFieldDisable(E, "REG_UNIT_UNDESCRIBED", TYPE_PHONO)
                StatsFieldDisable(E, "REG_UNIT_UNDESCRIBED", TYPE_MOVIE)
                StatsFieldDisable(T, "REG_UNIT_UNDESCRIBED", TYPE_TOTAL)
            Case "archive_passport.ascx"
                ' /состав арх. док.

                StatsFieldDisable(P, "UNIT_UNIQUE", TYPE_LS)
                StatsFieldDisable(P, "UNIT_OC", TYPE_LS)
                StatsFieldDisable(P, "UNIT_SECRET", TYPE_LP)
                StatsFieldDisable(M, "UNIT_SECRET", TYPE_MICRO)

                ' /состав НСА

                StatsFieldDisable(P, "DB_COUNT", TYPE_UD)
                StatsFieldDisable(P, "DB_RECORD_COUNT", TYPE_UD)
                StatsFieldDisable(P, "DB_VOLUME", TYPE_UD)
                StatsFieldDisable(P, "DB_COUNT", TYPE_LS)
                StatsFieldDisable(P, "DB_RECORD_COUNT", TYPE_LS)
                StatsFieldDisable(P, "DB_VOLUME", TYPE_LS)
                StatsFieldDisable(P, "DB_COUNT", TYPE_LP)
                StatsFieldDisable(P, "DB_RECORD_COUNT", TYPE_LP)
                StatsFieldDisable(P, "DB_VOLUME", TYPE_LP)
                StatsFieldDisable(P, "DB_COUNT", TYPE_NTD)
                StatsFieldDisable(P, "DB_RECORD_COUNT", TYPE_NTD)
                StatsFieldDisable(P, "DB_VOLUME", TYPE_NTD)
                StatsFieldDisable(A, "DB_COUNT", TYPE_PHOTO)
                StatsFieldDisable(A, "DB_RECORD_COUNT", TYPE_PHOTO)
                StatsFieldDisable(A, "DB_VOLUME", TYPE_PHOTO)
                StatsFieldDisable(A, "DB_COUNT", TYPE_PHONO)
                StatsFieldDisable(A, "DB_RECORD_COUNT", TYPE_PHONO)
                StatsFieldDisable(A, "DB_VOLUME", TYPE_PHONO)
                StatsFieldDisable(A, "DB_COUNT", TYPE_MOVIE)
                StatsFieldDisable(A, "DB_RECORD_COUNT", TYPE_MOVIE)
                StatsFieldDisable(A, "DB_VOLUME", TYPE_MOVIE)
                StatsFieldDisable(A, "DB_COUNT", TYPE_VIDEO)
                StatsFieldDisable(A, "DB_RECORD_COUNT", TYPE_VIDEO)
                StatsFieldDisable(A, "DB_VOLUME", TYPE_VIDEO)
        End Select
    End Sub

#End Region

End Class
