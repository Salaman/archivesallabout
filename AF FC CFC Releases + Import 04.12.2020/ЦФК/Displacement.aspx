﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Displacement.aspx.vb" Inherits="WebApplication.Displacement" %>
<%@ Register Src="ListDoc.ascx" TagName="ListDoc" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Перемещение</title>
</head>
<body style="background-color: White; padding: 5px;">
    <form id="form1" runat="server">
    <aj:ToolkitScriptManager ID="ToolkitScriptManagerDefault" runat="server" EnableScriptGlobalization="true"
        ScriptMode="Release" EnablePartialRendering="True" EnableHistory="true" />
    <table style="width: 100%;">
        <tr>
            <td>
                <div class="LogicBlockCaption">
                    <asp:Label id="LableTitle" Text="Выберите контейнер, в который вы хотели бы осуществить перемещение." runat="server"/>
                </div>
            </td>


            <td style="text-align: right;">
                
            </td>

        </tr>
        <!--tr>
            <td>
                <asp:Button ID="ButtonReplacementFinish" runat="server" Text="Завершить" />
            </td>
            <td>
                <asp:Button ID="ButtonAttachInventoryStructure" runat="server" Text="Прикрепить к разделу описи" />
            </td>
        </tr-->
    </table>
    <asp:UpdatePanel ID="UpdatePanelDisplacement" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <uc1:ListDoc ID="ListDocDisplacement" runat="server" DocTypeURL= "INVENTORY.ascx" SelectDocMode="Single" />
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>

