﻿Public Partial Class ListDocFilter
    Inherits BaseUserControl

    Public Event FilterChanged(ByVal sender As Object, ByVal e As EventArgs)

    Public Property FieldID() As Guid
        Get
            Return ViewState("FieldID")
        End Get
        Set(ByVal value As Guid)
            ViewState("FieldID") = value
        End Set
    End Property

    Public Property TypeName() As String
        Get
            Return ViewState("TypeName")
        End Get
        Set(ByVal value As String)
            ViewState("TypeName") = value
            If FiltersVeriflyed Then
                VeriflyFilters()
            End If
        End Set
    End Property

    Public Property Operator1() As String
        Get
            Return Operator1HiddenField.Value
        End Get
        Set(ByVal value As String)
            Operator1HiddenField.Value = value
            Operator1HiddenFieldNew.Value = value
            If FiltersVeriflyed Then
                VeriflyFilters()
            End If
        End Set
    End Property

    Public Property Filter1() As String
        Get
            Return Filter1HiddenField.Value
        End Get
        Set(ByVal value As String)
            Filter1HiddenField.Value = value
            Filter1HiddenFieldNew.Value = value
            If FiltersVeriflyed Then
                VeriflyFilters()
            End If
        End Set
    End Property

    Public Property Operator2() As String
        Get
            Return Operator2HiddenField.Value
        End Get
        Set(ByVal value As String)
            Operator2HiddenField.Value = value
            Operator2HiddenFieldNew.Value = value
            If FiltersVeriflyed Then
                VeriflyFilters()
            End If
        End Set
    End Property

    Public Property Filter2() As String
        Get
            Return Filter2HiddenField.Value
        End Get
        Set(ByVal value As String)
            Filter2HiddenField.Value = value
            Filter2HiddenFieldNew.Value = value
            If FiltersVeriflyed Then
                VeriflyFilters()
            End If
        End Set
    End Property

    Public Property Logic12() As String
        Get
            Return Logic12HiddenField.Value
        End Get
        Set(ByVal value As String)
            Logic12HiddenField.Value = value
            Logic12HiddenFieldNew.Value = value
            If FiltersVeriflyed Then
                VeriflyFilters()
            End If
        End Set
    End Property

    Private _ValuesScript As String
    Public Property ValuesScript() As String
        Get
            Return _ValuesScript
        End Get
        Set(ByVal value As String)
            _ValuesScript = value
        End Set
    End Property

    Public ReadOnly Property FilterText() As String
        Get
            FilterText = ""
            If Operator1 <> "" Then
                FilterText += String.Format("{0} {1}", Operator1, Filter1)
                If Logic12 <> "" And Operator2 <> "" Then
                    FilterText += String.Format(" {0} ", Logic12)
                End If
            End If
            If Operator2 <> "" Then
                FilterText += String.Format("{0} {1}", Operator2, Filter2)
            End If
        End Get
    End Property

    Public Overrides Sub Focus()
        qtb.Focus()
    End Sub

    Dim FiltersVeriflyed As Boolean

    Public Sub VeriflyFilters()
        Filter1TextBox.Visible = False
        Filter1DropDownList.Visible = False
        Filter1CheckBox.Visible = False
        Filter1CalendarExtender.Enabled = False
        Filter1FilteredTextBoxExtender.Enabled = False
        Filter2TextBox.Visible = False
        Filter2DropDownList.Visible = False
        Filter2CheckBox.Visible = False
        Filter2CalendarExtender.Enabled = False
        Filter2FilteredTextBoxExtender.Enabled = False
        Operator1DropDownList.Items.Clear()
        Operator2DropDownList.Items.Clear()
        Dim OperatorValues As String() = Nothing
        Select Case TypeName
            Case "decimal", "money", "numeric", "smallmoney", "float", "real"
                OperatorValues = New String() {"", "=", "≠", ">", "≥", "<", "≤"}
                Filter1TextBox.Visible = True
                Filter1TextBox.Text = Filter1
                Filter1FilteredTextBoxExtender.Enabled = True
                Filter1FilteredTextBoxExtender.ValidChars = "."
                Filter1FilteredTextBoxExtender.FilterType = AjaxControlToolkit.FilterTypes.Custom Or AjaxControlToolkit.FilterTypes.Numbers
                Filter2TextBox.Visible = True
                Filter2TextBox.Text = Filter2
                Filter2FilteredTextBoxExtender.Enabled = True
                Filter2FilteredTextBoxExtender.ValidChars = "."
                Filter2FilteredTextBoxExtender.FilterType = AjaxControlToolkit.FilterTypes.Custom Or AjaxControlToolkit.FilterTypes.Numbers
            Case "bigint", "int", "smallint", "tinyint"
                OperatorValues = New String() {"", "=", "≠", ">", "≥", "<", "≤"}
                Filter1TextBox.Visible = True
                Filter1TextBox.Text = Filter1
                Filter1FilteredTextBoxExtender.Enabled = True
                Filter1FilteredTextBoxExtender.FilterType = AjaxControlToolkit.FilterTypes.Numbers
                Filter2TextBox.Visible = True
                Filter2TextBox.Text = Filter2
                Filter2FilteredTextBoxExtender.Enabled = True
                Filter2FilteredTextBoxExtender.FilterType = AjaxControlToolkit.FilterTypes.Numbers
            Case "char", "varchar", "nchar", "nvarchar", "text", "ntext"
                OperatorValues = New String() {"", "≈", "=", "≠"}
                Filter1TextBox.Visible = True
                Filter1TextBox.Text = Filter1
                Filter2TextBox.Visible = True
                Filter2TextBox.Text = Filter2
            Case "uniqueidentifier"
                OperatorValues = New String() {"", "=", "≠"}
                Filter1TextBox.Visible = True
                Filter1TextBox.Text = Filter1
                Filter2TextBox.Visible = True
                Filter2TextBox.Text = Filter2
            Case "date", "datetime", "smalldatetime"
                OperatorValues = New String() {"", "=", "≠", ">", "≥", "<", "≤"}
                Filter1TextBox.Visible = True
                Filter1TextBox.Text = Filter1
                Filter1CalendarExtender.Enabled = True
                Filter2TextBox.Visible = True
                Filter2TextBox.Text = Filter2
                Filter2CalendarExtender.Enabled = True
            Case "bit"
                OperatorValues = New String() {"", "=", "≠"}
                Filter1CheckBox.Visible = True
                Boolean.TryParse(Filter1, Filter1CheckBox.Checked)
                Filter1CheckBox.Text = "Is True"
                Filter2CheckBox.Visible = True
                Boolean.TryParse(Filter2, Filter2CheckBox.Checked)
                Filter2CheckBox.Text = "Is True"
            Case "binary", "varbinary", "image", "timestamp", "sql_variant"
                OperatorValues = New String() {"", "=", "≠"}
                Filter1CheckBox.Visible = True
                Boolean.TryParse(Filter1, Filter1CheckBox.Checked)
                Filter1CheckBox.Text = "Has Value"
                Filter2CheckBox.Visible = True
                Boolean.TryParse(Filter2, Filter2CheckBox.Checked)
                Filter2CheckBox.Text = "Has Value"
        End Select
        If OperatorValues IsNot Nothing Then
            For Each OperatorValue As String In OperatorValues
                Operator1DropDownList.Items.Add(New ListItem(OperatorValue, OperatorValue))
                Operator2DropDownList.Items.Add(New ListItem(OperatorValue, OperatorValue))
            Next
            If Operator1DropDownList.Items.FindByValue(Operator1) IsNot Nothing Then
                Operator1DropDownList.SelectedValue = Operator1
            Else
                Operator1 = ""
            End If
            If Operator2DropDownList.Items.FindByValue(Operator2) IsNot Nothing Then
                Operator2DropDownList.SelectedValue = Operator2
            Else
                Operator2 = ""
            End If
        End If
        If Logic12RadioButtonList.Items.FindByValue(Logic12) IsNot Nothing Then
            Logic12RadioButtonList.SelectedValue = Logic12
        Else
            Logic12 = "AND"
        End If
        If Operator1 <> "" OrElse Operator2 <> "" Then
            Show.ImageUrl = BasePage.GetThemeRelativeURL("Images/Filter.gif")
            Show.ToolTip = FilterText
        Else
            Show.ImageUrl = BasePage.GetThemeRelativeURL("Images/FilterOff.gif")
            Show.ToolTip = Nothing
        End If
        ' QuickFilter
        qtb.Text = String.Empty
        If Operator1 <> "" Then
            qtb.Text += String.Format("{0} {1}", Operator1, Filter1)
            If Logic12 <> "" And Operator2 <> "" Then
                qtb.Text += String.Format(" {0} ", IIf(Logic12 = "AND", "&", "|"))
            End If
        End If
        If Operator2 <> "" Then
            qtb.Text += String.Format("{0} {1}", Operator2, Filter2)
        End If
        ' qtbAutoCompleteExtender
        qtbe.ContextKey = FieldID.ToString
        If ValuesScript IsNot Nothing Then
            qdd.Visible = True
            qtb.Visible = False
            qtbe.Enabled = False
            If qdd.Items.Count = 0 Then
                Dim ValuesCommand As New SqlClient.SqlCommand(ValuesScript)
                ValuesCommand.Parameters.AddWithValue("@UserID", AppSettings.UserInfo.UserID)
                Dim ValuesTable = AppSettings.GetDataTable(ValuesCommand)
                qdd.Items.Add(New ListItem(String.Empty, String.Empty))
                For Each R As DataRow In ValuesTable.Rows
                    Dim Value As String = R(0).ToString.Trim
                    Dim TextSB As New StringBuilder
                    If ValuesTable.Columns.Count > 1 Then
                        For i As Integer = 1 To ValuesTable.Columns.Count - 1
                            If TextSB.Length > 0 Then TextSB.Append(" | ")
                            TextSB.Append(R(i).ToString)
                        Next
                    Else
                        TextSB.Append(R(0).ToString)
                    End If
                    Dim Text = TextSB.ToString.Trim
                    If Value <> String.Empty AndAlso Text <> String.Empty Then
                        Dim ValueItem As New ListItem(Text, Value)
                        qdd.Items.Add(ValueItem)
                    End If
                Next
            End If
            Dim SelectedItem = qdd.Items.FindByValue(Filter1)
            If SelectedItem IsNot Nothing And Operator1 = eqView.OperatorEqual And Operator2 = eqView.OperatorEmpty Then
                qdd.SelectedValue = Filter1
            Else
                SelectedItem = qdd.Items.FindByValue(qtb.Text)
                If SelectedItem Is Nothing Then
                    qdd.Items.Add(New ListItem(qtb.Text, qtb.Text))
                End If
                qdd.SelectedValue = qtb.Text
            End If
        End If
        ' FiltersVeriflyed
        FiltersVeriflyed = True
    End Sub

#Region "Page Events"

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.ClientScript.IsClientScriptBlockRegistered("SetOperator") Then
            ' SetOperator
            Dim SetOperatorSB As New StringBuilder
            SetOperatorSB.AppendLine("function SetOperator(OperatorControlId) {")
            SetOperatorSB.AppendLine("  var OperatorControl = document.getElementById(OperatorControlId);")
            SetOperatorSB.AppendLine("  if (OperatorControl && OperatorControl.selectedIndex == 0) {")
            SetOperatorSB.AppendLine("      OperatorControl.selectedIndex = 1;")
            SetOperatorSB.AppendLine("  }")
            SetOperatorSB.AppendLine("}")

            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "SetOperator", SetOperatorSB.ToString, True)
        End If

        If Not Page.ClientScript.IsClientScriptBlockRegistered("CheckRecovery") Then
            'CheckRecovery Добавление функции Checket() для галочки выбрать все в Восстановление - 30.11.2020 Саламан Андрей
            Dim CheckRecovery As New StringBuilder
            CheckRecovery.AppendLine("function Checket(obj) {")
            CheckRecovery.AppendLine("  var inputCheck = obj.id;")
            CheckRecovery.AppendLine("  if ($(""#"" + inputCheck).hasClass(""activeCheckBox"")) {")
            CheckRecovery.AppendLine("      $(""#"" + inputCheck).removeClass(""activeCheckBox"");")
            CheckRecovery.AppendLine("      $('span.CheckBox input').prop('checked', false);")
            CheckRecovery.AppendLine("  }")
            CheckRecovery.AppendLine("else {")
            CheckRecovery.AppendLine("      $(""#"" + inputCheck).addClass(""activeCheckBox"");")
            CheckRecovery.AppendLine("      $('span.CheckBox input').prop('checked', true);")
            CheckRecovery.AppendLine("  }")
            CheckRecovery.AppendLine("}")

            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "CheckRecovery", CheckRecovery.ToString, True)
        End If
    End Sub

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        VeriflyFilters()
        qtb.Columns = Math.Max(qtb.Text.Length, 3)
    End Sub

#End Region

#Region "Events Change"

    Public Sub Operator1DropDownList_OnTextChanged(ByVal sender As Object, ByVal e As EventArgs)
        Operator1HiddenFieldNew.Value = Operator1DropDownList.SelectedValue.ToString
    End Sub

    Public Sub Filter1TextBox_OnTextChanged(ByVal sender As Object, ByVal e As EventArgs)
        Filter1HiddenFieldNew.Value = Filter1TextBox.Text
    End Sub

    Public Sub Filter1DropDownList_OnSelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Filter1HiddenFieldNew.Value = Filter1DropDownList.SelectedValue.ToString
    End Sub

    Public Sub Filter1CheckBox_OnCheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        Filter1HiddenFieldNew.Value = Filter1CheckBox.Checked.ToString
    End Sub

    Public Sub Operator2DropDownList_OnTextChanged(ByVal sender As Object, ByVal e As EventArgs)
        Operator2HiddenFieldNew.Value = Operator2DropDownList.SelectedValue.ToString
    End Sub

    Public Sub Filter2TextBox_OnTextChanged(ByVal sender As Object, ByVal e As EventArgs)
        Filter2HiddenFieldNew.Value = Filter2TextBox.Text
    End Sub

    Public Sub Filter2DropDownList_OnSelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Filter2HiddenFieldNew.Value = Filter2DropDownList.SelectedValue.ToString
    End Sub

    Public Sub Filter2CheckBox_OnCheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        Filter2HiddenFieldNew.Value = Filter2CheckBox.Checked.ToString
    End Sub

    Private Sub Logic12RadioButtonList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Logic12RadioButtonList.SelectedIndexChanged
        Logic12HiddenFieldNew.Value = Logic12RadioButtonList.SelectedValue
    End Sub

#End Region

#Region "Events"

    Private Sub Show_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Show.Click

        PlaceHolderShowFilters.Visible = True
        ModalPopupExtenderFilters.Show()

        Filter1TextBox.Focus()

        Filter1TextBox.Attributes("onkeyup") = "SetOperator('" & Operator1DropDownList.ClientID & "');"
        Filter1DropDownList.Attributes("onchange") = "SetOperator('" & Operator1DropDownList.ClientID & "');"
        Filter1CheckBox.Attributes("onchange") = "SetOperator('" & Operator1DropDownList.ClientID & "');"

        Filter2TextBox.Attributes("onkeyup") = "SetOperator('" & Operator2DropDownList.ClientID & "');"
        Filter2DropDownList.Attributes("onchange") = "SetOperator('" & Operator2DropDownList.ClientID & "');"
        Filter2CheckBox.Attributes("onchange") = "SetOperator('" & Operator2DropDownList.ClientID & "');"

    End Sub

    Private Sub ButtonConfirmOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonConfirmOk.Click

        qtb.Focus()

        Operator1HiddenField.Value = Operator1HiddenFieldNew.Value
        Filter1HiddenField.Value = Filter1HiddenFieldNew.Value
        Operator2HiddenField.Value = Operator2HiddenFieldNew.Value
        Filter2HiddenField.Value = Filter2HiddenFieldNew.Value
        Logic12HiddenField.Value = Logic12HiddenFieldNew.Value
        RaiseEvent FilterChanged(Me, EventArgs.Empty)
    End Sub

    Private Sub qtb_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles qtb.TextChanged
        Operator1HiddenField.Value = String.Empty
        Filter1HiddenField.Value = String.Empty
        Logic12HiddenField.Value = String.Empty
        Operator2HiddenField.Value = String.Empty
        Filter2HiddenField.Value = String.Empty
        Dim QuickFilterValues = qtb.Text.Split(New String() {" &", " |"}, StringSplitOptions.RemoveEmptyEntries)
        For ValueNumber As Integer = 0 To Math.Min(QuickFilterValues.Count - 1, 1)
            Dim QuickFilterValue = QuickFilterValues(ValueNumber).Trim
            Dim OperatorValue As String = String.Empty
            Dim FilterValue As String = String.Empty
            If QuickFilterValue <> String.Empty Then
                Select Case TypeName
                    Case "decimal", "money", "numeric", "smallmoney", "float", "real", "bigint", "int", "smallint", "tinyint", "date", "datetime", "smalldatetime"
                        If QuickFilterValue.StartsWith("≥") Or QuickFilterValue.StartsWith(">=") Then
                            OperatorValue = "≥"
                        ElseIf QuickFilterValue.StartsWith(">") Then
                            OperatorValue = ">"
                        ElseIf QuickFilterValue.StartsWith("≤") Or QuickFilterValue.StartsWith("<=") Then
                            OperatorValue = "≤"
                        ElseIf QuickFilterValue.StartsWith("<") Then
                            OperatorValue = "<"
                        ElseIf QuickFilterValue.StartsWith("≠") Or QuickFilterValue.StartsWith("!=") Then
                            OperatorValue = "≠"
                        Else
                            OperatorValue = "="
                        End If
                    Case "char", "varchar", "nchar", "nvarchar", "text", "ntext"
                        If QuickFilterValue.StartsWith("=") Then
                            OperatorValue = "="
                        ElseIf QuickFilterValue.StartsWith("≠") Or QuickFilterValue.StartsWith("!=") Then
                            OperatorValue = "≠"
                        Else
                            OperatorValue = "≈"
                        End If
                    Case "uniqueidentifier", "bit", "binary", "varbinary", "image", "timestamp", "sql_variant"
                        If QuickFilterValue.StartsWith("≠") Or QuickFilterValue.StartsWith("!=") Then
                            OperatorValue = "≠"
                        Else
                            OperatorValue = "="
                        End If
                End Select
                FilterValue = QuickFilterValue.TrimStart(New Char() {" ", "=", "≠", "≈", "<", "≤", ">", "≥", "!"})
            End If
            If ValueNumber = 0 Then
                Operator1HiddenField.Value = OperatorValue
                Filter1HiddenField.Value = FilterValue
                Logic12HiddenField.Value = String.Empty
            Else
                Logic12HiddenField.Value = IIf(qtb.Text.Contains(" &"), "AND", "OR")
                Operator2HiddenField.Value = OperatorValue
                Filter2HiddenField.Value = FilterValue
            End If
        Next
        RaiseEvent FilterChanged(Me, EventArgs.Empty)
    End Sub

    Private Sub qdd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles qdd.SelectedIndexChanged
        qtb.Text = String.Empty
        If qdd.SelectedValue <> String.Empty Then
            qtb.Text = eqView.OperatorEqual & " " & qdd.SelectedValue
        End If
        qtb_TextChanged(qtb, EventArgs.Empty)
    End Sub

#End Region

End Class