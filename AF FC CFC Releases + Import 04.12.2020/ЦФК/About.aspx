﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="About.aspx.vb" Inherits="WebApplication.About"
    MasterPageFile="~/Site.Master" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ContentPlaceHolderID="Header" runat="server">
</asp:Content>
<asp:Content ContentPlaceHolderID="Body" runat="server">
    <div style="padding: 20px;">
        <h2>
            <%#GetTitle()%>
        </h2>
        <p>
            <b>
                Версия :
                <%#GetVersion()%>
            </b>
        </p>
        <p>
            <%#GetCopyright()%>
        </p>
    </div>
</asp:Content>
