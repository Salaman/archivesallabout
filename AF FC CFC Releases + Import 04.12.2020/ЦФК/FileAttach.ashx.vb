﻿Imports System.Web
Imports System.Web.Services

Public Class FileAttach
    Implements System.Web.IHttpHandler

    Public Class FileAttachItem
        Public FileName As String
        Public FileBytes As Byte()
        Public ContentType As String = "application/x-unknown"
    End Class

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim FileItem As FileAttachItem = context.Cache(context.Request.QueryString("ID"))
        If FileItem IsNot Nothing Then
            context.Response.ContentType = FileItem.ContentType
            If context.Request.Browser.Browser = "Firefox" Then
                context.Response.AddHeader("Content-Disposition", "attachment;filename=""" + FileItem.FileName + """")
            Else
                context.Response.AddHeader("Content-Disposition", "attachment;filename=""" + context.Server.UrlEncode(FileItem.FileName) + """")
            End If
            context.Response.AddHeader("Content-Length", FileItem.FileBytes.Count)
            context.Response.OutputStream.Write(FileItem.FileBytes, 0, FileItem.FileBytes.Count)
        Else
            context.Response.ContentType = "text/html"
            context.Response.Write("File not found!")
        End If
    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class