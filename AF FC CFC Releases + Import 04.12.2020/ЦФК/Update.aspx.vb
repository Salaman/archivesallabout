﻿Partial Public Class Update
    Inherits eqBasePage

    Private Sub GenerateUpdateFromScript_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles GenerateUpdateFromScript.Click
        Try
            'Dim CreationDateTime As DateTime = IIf(DocID <> Guid.Empty, DataSetHeaderDataRow("CreationDateTime"), Now)
            Dim UpdateFromDataset As New DataSet("Update")
            '' eqUpdates
            'Dim eqUpdatesSqlDataAdapter As New SqlClient.SqlDataAdapter("SELECT * FROM eqUpdates WHERE CreationDateTime>=@CreationDateTime AND Deleted=0 ORDER BY CreationDateTime ASC", AppSettings.Cnn)
            'eqUpdatesSqlDataAdapter.SelectCommand.Parameters.AddWithValue("@CreationDateTime", CreationDateTime)
            'eqUpdatesSqlDataAdapter.Fill(UpdateFromDataset, "eqUpdates")
            '' eqTables
            'FillUpdateFromDataset(UpdateFromDataset, "eqCategories")
            'FillUpdateFromDataset(UpdateFromDataset, "eqDocTypes")
            'FillUpdateFromDataset(UpdateFromDataset, "eqDocStates")
            'FillUpdateFromDataset(UpdateFromDataset, "eqDocToolBar")
            'FillUpdateFromDataset(UpdateFromDataset, "eqDocRoots")
            'FillUpdateFromDataset(UpdateFromDataset, "eqDocStructure")
            'FillUpdateFromDataset(UpdateFromDataset, "eqListDoc")
            For Each eqTableName In CheckedeqTableNames
                FillUpdateFromDataset(UpdateFromDataset, eqTableName)
            Next
            For Each TableName In CheckedTableNames
                FillUpdateFromDataset(UpdateFromDataset, TableName)
            Next
            If Compressed.Checked Then
                Dim FileName As String = "eqUpdate_From_" & Now.ToString("yyyy.MM.dd_HH.mm.ss") & ".gzip"
                Dim UpdateMemoryStream As New IO.MemoryStream
                Dim UpdateGZipStream As New IO.Compression.GZipStream(UpdateMemoryStream, IO.Compression.CompressionMode.Compress)
                UpdateFromDataset.WriteXml(UpdateGZipStream, XmlWriteMode.WriteSchema)
                UpdateGZipStream.Close()
                AttachFile(FileName, UpdateMemoryStream.ToArray)
            Else
                Dim FileName As String = "eqUpdate_From_" & Now.ToString("yyyy.MM.dd_HH.mm.ss") & ".xml"
                Dim UpdateMemoryStream As New IO.MemoryStream
                UpdateFromDataset.WriteXml(UpdateMemoryStream, XmlWriteMode.WriteSchema)
                AttachFile(FileName, UpdateMemoryStream.ToArray)
            End If
        Catch ex As Exception
            AddMessage("Ошибка при выгрузке в UpdateScript в файл -> " & ex.Message)
        End Try
    End Sub

    Public Sub FillUpdateFromDataset(ByVal UpdateFromDataset As DataSet, ByVal TableName As String)
        Dim UpdateSqlDataAdapter As New SqlClient.SqlDataAdapter("SELECT * FROM " + TableName, AppSettings.Cnn)
        UpdateSqlDataAdapter.Fill(UpdateFromDataset, TableName)
    End Sub

    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        'Dim CurrentScriptManager = ScriptManager.GetCurrent(Page)
        'If CurrentScriptManager IsNot Nothing Then
        '    CurrentScriptManager.RegisterPostBackControl(Me)
        'End If
    End Sub

    Private Sub ApplyUpdateFromScriptFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ApplyUpdateFromScriptFile.Click
        If UpdateFromScriptFile.HasFile Then
            Try
                AppSettings.BeginTransaction()
                Dim UpdateFromDataset As New DataSet
                Try
                    If UpdateFromScriptFile.FileName.ToLower.EndsWith(".gzip") Then
                        Dim UpdateGZipStream As New IO.Compression.GZipStream(UpdateFromScriptFile.FileContent, IO.Compression.CompressionMode.Decompress)
                        UpdateFromDataset.ReadXml(UpdateGZipStream, XmlReadMode.ReadSchema)
                    ElseIf UpdateFromScriptFile.FileName.ToLower.EndsWith(".xml") Then
                        UpdateFromDataset.ReadXml(UpdateFromScriptFile.FileContent, XmlReadMode.ReadSchema)
                    Else
                        Throw New Exception("Данное расширение не поддерживается")
                    End If
                Catch ex As Exception
                    Throw New Exception("Непрафильный формат файла -> " & ex.Message)
                End Try
                If UpdateFromDataset.DataSetName = "Update" Then
                    For Each eqTable In eqTables
                        If eqTable = "eqUpdates" Then
                            ' eqUpdates
                            Dim GoSeparatorsList As New List(Of String)
                            For Each inString As String In New String() {" ", vbCr, vbLf, vbCrLf, vbTab}
                                For Each outString As String In New String() {" ", vbCr, vbLf, vbCrLf, vbTab}
                                    GoSeparatorsList.Add(inString & "GO" & outString)
                                    GoSeparatorsList.Add(inString & "go" & outString)
                                    GoSeparatorsList.Add(inString & "Go" & outString)
                                    GoSeparatorsList.Add(inString & "gO" & outString)
                                Next
                            Next
                            Dim GoSeparatorsArray = GoSeparatorsList.ToArray
                            If NoSqlUpdates.Checked = False Then
                                Dim eqUpdatesOriginalSqlCommand As New SqlClient.SqlCommand("SELECT ID FROM eqUpdates")
                                Dim eqUpdatesOriginalDataTable = AppSettings.GetDataTable(eqUpdatesOriginalSqlCommand)
                                Dim eqUpdatesOriginalID = From R As DataRow In eqUpdatesOriginalDataTable.Rows Select DirectCast(R("ID"), Guid)
                                For Each UpdateRow In From R In UpdateFromDataset.Tables("eqUpdates") Where Not eqUpdatesOriginalID.Contains(DirectCast(R("ID"), Guid)) AndAlso R("Deleted") = False Order By R("CreationDateTime") Ascending
                                    Dim UpdateScript = UpdateRow("UpdateScript").ToString & " "
                                    Dim UpdateScriptArray = UpdateScript.Split(GoSeparatorsArray, StringSplitOptions.RemoveEmptyEntries)
                                    For Each UpdateScriptBatch In UpdateScriptArray
                                        If UpdateScriptBatch.Trim <> "" Then
                                            Try
                                                Dim UpdateSqlCommand As New SqlClient.SqlCommand(UpdateScriptBatch)
                                                AppSettings.ExecCommand(UpdateSqlCommand)
                                            Catch ex As Exception
                                                Throw New Exception("Ошибка при исполнении скрипта: " & vbCrLf & "" & ex.Message & vbCrLf & UpdateScriptBatch)
                                            End Try
                                        End If
                                    Next
                                    AppSettings.WriteUpdate(UpdateRow)
                                Next
                            End If
                        Else
                            ' eqTables
                            If UpdateFromDataset.Tables.Contains(eqTable) Then
                                UpdateTableFromDataset(UpdateFromDataset, eqTable)
                            End If
                        End If
                    Next
                    ' Tables
                    For Each Table As DataTable In UpdateFromDataset.Tables
                        If Not eqTables.Contains(Table.TableName) Then
                            UpdateTableFromDataset(UpdateFromDataset, Table.TableName)
                        End If
                    Next
                    ' SampleAppSettings
                    Try
                        Dim SampleAppSettings As New eqAppSettings
                        SampleAppSettings.OpenSession(AppSettings.Cnn, AppSettings.UserInfo.UserLogin, AppSettings.Trn)
                        SampleAppSettings.Manager = My.Resources.ResourceManager
                        Session("AppSettings") = SampleAppSettings
                    Catch ex As Exception
                        Throw New Exception("Не удалось загрузить систему с новой конфигурацией -> " & ex.Message)
                    End Try
                Else
                    Throw New Exception("Непрафильный формат файла -> Не содержит корневого узла Update")
                End If
                AppSettings.CommitTransaction()
                AddMessage("Обновление успешно применено")
            Catch ex As Exception
                AppSettings.RollbackTransaction()
                AddMessage("Ошибка при загрузке UpdateScript файла -> " & ex.Message)
            End Try
        Else
            AddMessage("Приложите UpdateScript файл")
        End If
    End Sub


    ''' <summary>
    ''' SQL Shema Информация
    ''' </summary>
    Private ShemaDataTable As DataTable = Nothing

    ''' <summary>
    ''' SQL Table Row Shema Информация Взяа из eqDocType с небольшими переделками
    ''' </summary>
    ''' <param name="TableName">Имя таблицы</param>
    ''' <param name="ColumnName">Имя колонки</param>
    ''' <returns>DataRow с колонками TableName, ColumnName, TypeName, MaxLength, IsNullable</returns>
    Public Function GetShemaDataRow(ByVal TableName, ByVal ColumnName) As DataRow
        If ShemaDataTable Is Nothing Then
            Dim SqlCommandSB As New Text.StringBuilder
            SqlCommandSB.AppendLine("SELECT")
            SqlCommandSB.AppendLine("   tablesviews.name TableName")
            SqlCommandSB.AppendLine("   ,sys.columns.name ColumnName")
            SqlCommandSB.AppendLine("   ,sys.types.name TypeName")
            SqlCommandSB.AppendLine("   ,sys.columns.max_length MaxLength")
            SqlCommandSB.AppendLine("   ,sys.columns.is_nullable IsNullable")
            SqlCommandSB.AppendLine("   ,sys.columns.is_identity IsIdentity")
            SqlCommandSB.AppendLine("   ,sys.columns.is_computed IsComputed")
            SqlCommandSB.AppendLine("   ,sys.fulltext_index_columns.object_id FullText")
            SqlCommandSB.AppendLine("FROM")
            SqlCommandSB.AppendLine("   (SELECT object_id, name FROM sys.tables UNION SELECT object_id, name FROM sys.views) AS tablesviews")
            SqlCommandSB.AppendLine("JOIN")
            SqlCommandSB.AppendLine("   sys.columns ON sys.columns.object_id = tablesviews.object_id")
            SqlCommandSB.AppendLine("JOIN")
            SqlCommandSB.AppendLine("   sys.types ON sys.types.system_type_id = sys.columns.system_type_id")
            SqlCommandSB.AppendLine("LEFT JOIN")
            SqlCommandSB.AppendLine("   sys.fulltext_index_columns ON sys.fulltext_index_columns.object_id=sys.columns.object_id AND sys.fulltext_index_columns.column_id=sys.columns.column_id")
            SqlCommandSB.AppendLine("WHERE")
            SqlCommandSB.AppendLine("   NOT sys.types.name = 'sysname'")
            Dim GetShemaSqlCommand As New System.Data.SqlClient.SqlCommand(SqlCommandSB.ToString)
            ShemaDataTable = AppSettings.GetDataTable(GetShemaSqlCommand)
            ShemaDataTable.PrimaryKey = New DataColumn() {ShemaDataTable.Columns("TableName"), ShemaDataTable.Columns("ColumnName")}
        End If
        'Dim GetShemaSqlCommand As New System.Data.SqlClient.SqlCommand(System.Data.SqlClient.SqlCommandSB.ToString)
        'ShemaDataTable = AppSettings.GetDataTable(GetShemaSqlCommand)
        'ShemaDataTable.PrimaryKey = New DataColumn() {ShemaDataTable.Columns("TableName"), ShemaDataTable.Columns("ColumnName")}
        Dim ShemaDataRow As DataRow = ShemaDataTable.Rows.Find(New Object() {TableName, ColumnName})
        'If ShemaDataRow Is Nothing Then
        '  Throw New Exception("В таблице " & TableName & " не найдена колонка " & ColumnName)
        'Else
        Return ShemaDataRow
        'End If
    End Function

    Public Sub UpdateTableFromDataset(ByVal UpdateFromDataset As DataSet, ByVal TableName As String)
        Try
            ShemaDataTable = Nothing
            If UpdateFromDataset.Tables.Contains(TableName) Then
                Dim CurrentDataTable As New DataTable
                Dim SelectSB As New StringBuilder
                For Each Column As DataColumn In UpdateFromDataset.Tables(TableName).Columns
                    If Not GetShemaDataRow(TableName, Column.ColumnName)("IsComputed") Then
                        If SelectSB.Length = 0 Then
                            SelectSB.AppendLine("SELECT")
                            SelectSB.Append("   ")
                        Else
                            SelectSB.Append("   ,")
                        End If
                        SelectSB.AppendLine("[" & Column.ColumnName & "]")
                    End If
                Next
                SelectSB.AppendLine("FROM " + TableName)
                Dim CurrentDatasetSqlDataAdapter As New SqlClient.SqlDataAdapter(SelectSB.ToString, AppSettings.Cnn)
                Dim CurrentDatasetSqlCommandBuilder As New SqlClient.SqlCommandBuilder(CurrentDatasetSqlDataAdapter)
                CurrentDatasetSqlDataAdapter.SelectCommand.Transaction = AppSettings.Trn
                'CurrentDatasetSqlDataAdapter.UpdateCommand.Transaction = AppSettings.Trn
                'CurrentDatasetSqlDataAdapter.DeleteCommand.Transaction = AppSettings.Trn
                CurrentDatasetSqlDataAdapter.Fill(CurrentDataTable)
                For Each eqUpdateRow As DataRow In UpdateFromDataset.Tables(TableName).Rows
                    Dim eqUpdateRowID = DirectCast(eqUpdateRow("ID"), Guid)
                    Dim CurrentDataTableRow As DataRow
                    Select Case TableName
                        Case "eqDocRoots"
                            Dim SourceStateID = DirectCast(eqUpdateRow("SourceStateID"), Guid)
                            Dim DestStateID = DirectCast(eqUpdateRow("DestStateID"), Guid)
                            CurrentDataTableRow = CurrentDataTable.AsEnumerable.SingleOrDefault(Function(R) R("SourceStateID") = SourceStateID And R("DestStateID") = DestStateID)
                        Case Else
                            CurrentDataTableRow = CurrentDataTable.AsEnumerable.SingleOrDefault(Function(R) R("ID") = eqUpdateRowID)
                    End Select
                    If CurrentDataTableRow Is Nothing Then
                        ' Insert
                        CurrentDataTableRow = CurrentDataTable.NewRow
                        CurrentDataTable.Rows.Add(CurrentDataTableRow)
                    End If
                    ' Update
                    For Each Column As DataColumn In UpdateFromDataset.Tables(TableName).Columns
                        If Not GetShemaDataRow(TableName, Column.ColumnName)("IsComputed") Then
                            If CurrentDataTable.Columns.Contains(Column.ColumnName) Then
                                CurrentDataTableRow(Column.ColumnName) = eqUpdateRow(Column.ColumnName)
                            End If
                        End If
                    Next
                Next
                Dim eqCategoryUpdateIDs = From R In UpdateFromDataset.Tables(TableName) Select DirectCast(R("ID"), Guid)
                If DontDelete Is Nothing OrElse DontDelete.Checked = False Then
                    For Each CurrentDataTableRow As DataRow In CurrentDataTable.Rows
                        Dim CurrentDataTableRowID = DirectCast(CurrentDataTableRow("ID"), Guid)
                        If Not eqCategoryUpdateIDs.Contains(CurrentDataTableRowID) Then
                            CurrentDataTableRow.Delete()
                        End If
                    Next
                End If
                CurrentDatasetSqlDataAdapter.Update(CurrentDataTable)
            End If
        Catch ex As Exception
            Throw New Exception("Ошибка при обновлении данных в таблице " & TableName & ": " & ex.Message)
        End Try
    End Sub

    Public ReadOnly Property CheckedeqTableNames() As String()
        Get
            Dim eqNodes = From N As TreeNode In IncludedUpdateTables.Nodes(0).ChildNodes Where N.Checked Select N.Text
            Return eqNodes.ToArray
        End Get
    End Property

    Public ReadOnly Property CheckedTableNames() As String()
        Get
            Dim Nodes = From N As TreeNode In IncludedUpdateTables.Nodes(1).ChildNodes Where N.Checked Select N.Text
            Return Nodes.ToArray
        End Get
    End Property

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        phMessages.Controls.Clear()
        If Not AppSettings.UserSecurities.IsAdministrator Then
            phRestrictedContent.Visible = False
            AddMessage("Только административный пользователь имеет доступ к обновлению системы")
        Else
            phRestrictedContent.Visible = True
        End If
        If IncludedUpdateTables.Nodes.Count = 0 Then
            ' eqTables
            Dim eqTablesNode = New TreeNode("eqTables") With {.Expanded = True}
            IncludedUpdateTables.Nodes.Add(eqTablesNode)
            ' Tables
            Dim TablesNode = New TreeNode("Tables") With {.Expanded = True}
            IncludedUpdateTables.Nodes.Add(TablesNode)
            ' FillTreeView
            For Each TableName In CalcTablesNames
                If TableName.StartsWith("eq") Then
                    AddChildNode(eqTablesNode, TableName)
                Else
                    AddChildNode(TablesNode, TableName)
                End If
            Next
        End If
    End Sub

    Protected Overrides Sub ShowMessages(ByVal Messages As System.Collections.Generic.List(Of String))
        phMessages.Controls.Clear()
        Dim i As Integer = 1
        For Each Message In Messages
            Dim MessagePanel As New Panel
            MessagePanel.Style("margin") = "3px"
            'MessagePanel.Style("padding") = "2px"
            MessagePanel.Style("border") = "solid 1px gray"
            MessagePanel.Style("background-color") = "whitesmoke"

            Dim MessageNumberLabel As New Label
            MessageNumberLabel.Style("padding") = "2px"
            MessageNumberLabel.Style("border-right") = "solid 1px gray"
            MessageNumberLabel.Style("background-color") = "white"
            MessageNumberLabel.Height = System.Web.UI.WebControls.Unit.Percentage(100)
            MessageNumberLabel.Text = "&nbsp;" & i.ToString & "&nbsp;"
            MessagePanel.Controls.Add(MessageNumberLabel)

            Dim tbxMessage As New TextBox
            tbxMessage.TextMode = TextBoxMode.MultiLine
            tbxMessage.Width = New System.Web.UI.WebControls.Unit(95, UnitType.Percentage)
            tbxMessage.Wrap = True
            tbxMessage.Style("padding") = "2px"
            tbxMessage.Text = Message
            MessagePanel.Controls.Add(tbxMessage)

            phMessages.Controls.Add(MessagePanel)
            i += 1
        Next
    End Sub

    Public eqTables As String() = New String() {"eqUpdates", _
                                                "eqCategories", _
                                                "eqDocTypes", _
                                                "eqDocStates", _
                                                "eqDocToolBar", _
                                                "eqDocRoots", _
                                                "eqDocTypeFunctions", _
                                                "eqDocStructure", _
                                                "eqListDoc"}

    Public defaultNodes As String() = New String() {"eqUpdates", _
                                                    "eqCategories", _
                                                    "eqDocTypes", _
                                                    "eqDocStates", _
                                                    "eqDocToolBar", _
                                                    "eqDocRoots", _
                                                    "eqDocTypeFunctions", _
                                                    "eqDocStructure", _
                                                    "eqListDoc"}

    Sub AddChildNode(ByVal parentNode As TreeNode, ByVal childNodeText As String)
        Dim childNode = New TreeNode(childNodeText) With {.Checked = defaultNodes.Contains(childNodeText)}
        parentNode.ChildNodes.Add(childNode)
        If childNode.Text = "eqUpdates" Then
            parentNode.ChildNodes.Item(parentNode.ChildNodes.Count - 1).ShowCheckBox = False
        End If
    End Sub

    Private _CalcTablesNames As String()
    Public ReadOnly Property CalcTablesNames() As String()
        Get
            If _CalcTablesNames Is Nothing Then
                Dim SqlCommand As New SqlClient.SqlCommand("SELECT T.name TableName FROM sys.tables T" & vbCrLf & _
                                                           "JOIN (SELECT * FROM sys.columns WHERE name='ID') C ON T.object_id=C.object_id" & vbCrLf & _
                                                           "") '"JOIN (SELECT DISTINCT TableName FROM dbo.eqDocStructure WHERE Deleted=0) DS ON T.name=DS.TableName")
                Dim Tables = From R As DataRow In AppSettings.GetDataTable(SqlCommand).Rows Select TableName = DirectCast(R("TableName"), String) Order By TableName
                _CalcTablesNames = Tables.ToArray()
            End If
            Return _CalcTablesNames
        End Get
    End Property

End Class