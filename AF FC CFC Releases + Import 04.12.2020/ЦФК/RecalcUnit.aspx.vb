﻿Partial Public Class RecalcUnit
    Inherits BasePage

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        MultiViewTabs.ActiveViewIndex = CInt(MenuTabs.SelectedValue)
    End Sub

    Private Sub bGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles bGo.Click
        Dim r As New Recalc(AppSettings)

        r.Command.Parameters.AddWithValue("@A_NACH_DATA", c_A_NACH_DATA.Checked)
        r.Command.Parameters.AddWithValue("@A_KON_DATA", c_A_KON_DATA.Checked)
        r.Command.Parameters.AddWithValue("@A_LISTS", c_A_LISTS.Checked)
        r.Command.Parameters.AddWithValue("@A_EU", c_A_EU.Checked)

        r.Command.Parameters.AddWithValue("@K_NEG", c_K_NEG.Checked)
        r.Command.Parameters.AddWithValue("@M_NEG", c_M_NEG.Checked)
        r.Command.Parameters.AddWithValue("@K_DNEG", c_K_DNEG.Checked)
        r.Command.Parameters.AddWithValue("@M_DNEG", c_M_DNEG.Checked)
        r.Command.Parameters.AddWithValue("@K_FON", c_K_FON.Checked)
        r.Command.Parameters.AddWithValue("@M_FON", c_M_FON.Checked)
        r.Command.Parameters.AddWithValue("@K_MFON", c_K_MFON.Checked)
        r.Command.Parameters.AddWithValue("@M_MFON", c_M_MFON.Checked)
        r.Command.Parameters.AddWithValue("@K_MFONS", c_K_MFONS.Checked)
        r.Command.Parameters.AddWithValue("@M_MFONS", c_M_MFONS.Checked)
        r.Command.Parameters.AddWithValue("@K_PPOS", c_K_PPOS.Checked)
        r.Command.Parameters.AddWithValue("@M_PPOS", c_M_PPOS.Checked)
        r.Command.Parameters.AddWithValue("@K_POS", c_K_POS.Checked)
        r.Command.Parameters.AddWithValue("@M_POS", c_M_POS.Checked)

        r.Command.Parameters.AddWithValue("@K_ORIG", c_K_ORIG.Checked)
        r.Command.Parameters.AddWithValue("@K_KOP", c_K_KOP.Checked)

        r.Command.Parameters.AddWithValue("@K_ORIGN", c_K_ORIGN.Checked)
        r.Command.Parameters.AddWithValue("@M_ORIGN", c_M_ORIGN.Checked)
        r.Command.Parameters.AddWithValue("@K_KOPP", c_K_KOPP.Checked)
        r.Command.Parameters.AddWithValue("@M_KOPP", c_M_KOPP.Checked)
        r.Command.Parameters.AddWithValue("@K_GORIG", c_K_GORIG.Checked)
        r.Command.Parameters.AddWithValue("@K_GRAM", c_K_GRAM.Checked)

        Try
            Select Case ModalDialogArgument.Argument
                Case "One"
                    r.Calc(Recalc.Type.Unit, DirectCast(ModalDialogArgument.ArgumentObject, Guid).ToString())
                Case "All"
                    r.Calc(Recalc.Type.Unit)
            End Select
        Catch ex As Exception
            AddMessage("Не удалось выполнить расчет: >> " & ex.Message)
        End Try
    End Sub

    Private Sub bClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles bClose.Click
        CloseWindow()
    End Sub

End Class