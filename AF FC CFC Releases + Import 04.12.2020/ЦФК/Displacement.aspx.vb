﻿Imports System.Data.SqlClient

Partial Public Class Displacement
    Inherits BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Select Case Request.QueryString("DocTypeURL")
            Case "INVENTORY.ascx"
                ListDocDisplacement.DocTypeURL = "FUND.ascx"
            Case "UNIT.ascx"
                ListDocDisplacement.DocTypeURL = "INVENTORY.ascx"
            Case "DOCUMENT.ascx"
                ListDocDisplacement.DocTypeURL = "UNIT.ascx"
            Case Else
                ' ListDocDisplacement.DocTypeURL = ""
        End Select
        ButtonReplacementFinish.Visible = False
        ButtonAttachInventoryStructure.Visible = False
    End Sub

    Private oName As String '  objectName
    Private cName As String

    Private Sub ListDocDisplacement_SelectDoc(ByVal DocID As System.Guid) Handles ListDocDisplacement.SelectDoc
        Dim docTypeURL As String = Request.QueryString("DocTypeURL")
        Dim selectedToDisplace = DirectCast(ModalDialogArgument.ArgumentObject, List(Of Guid))

        Dim invClsConflictTable As New DataTable() 'таблица конфликтных разделов описи
        Dim newContainerID As Guid = DocID
        Select Case docTypeURL
            Case "UNIT.ascx", "INVENTORY.ascx", "DOCUMENT.ascx"

                InitObjConNames(docTypeURL)
                Dim secureLevelCheck As Boolean = True
                Dim docTypeCheck As Boolean = True
                Dim nameConflictCheck As String = Nothing
                DisplacementPreparation(selectedToDisplace, newContainerID, secureLevelCheck, docTypeCheck, invClsConflictTable, nameConflictCheck)

                If selectedToDisplace.Count = 0 Then
                    AddMessage("Выбранные объекты учета уже находятся в этом контейнере. Выберите другой контейнер для перемещения.")
                    Return
                End If
                If Not docTypeCheck Then
                    AddMessage("Один или несколько выбранных объектов учета не соответствуют типу хранимых объектов контейнера. Перемещение невозможно.")
                    Return
                End If
                If Not secureLevelCheck Then
                    AddMessage("Один или несколько выбранных объектов учета не отвечают характеристике секретности нового контейнера. Произойдет изменение хорактеристик безопасности контейнера.")
                    ChangeConSecureLevel(newContainerID, 3)
                End If
                If nameConflictCheck <> Nothing Then
                    AddMessage(nameConflictCheck)
                    Return
                End If

                Dim extendedObjectsList As New List(Of Guid)
                If oName = "UNIT" Then
                    For Each selectedDoc In selectedToDisplace 'создаем расширенный список юнитов
                        For Each exDoc In GetRelatedUnitsList(selectedDoc.ToString)
                            If Not extendedObjectsList.Contains(exDoc) Then
                                extendedObjectsList.Add(exDoc)
                            End If
                        Next
                    Next
                    InvClsDisplace(invClsConflictTable, newContainerID)
                Else
                    extendedObjectsList = selectedToDisplace
                End If


                DoDisplacement(extendedObjectsList, newContainerID, invClsConflictTable)
                AddMessage("Выбранные объекты учета перемещены")

                If docTypeURL <> "UNIT.ascx" Then
                    CloseWindow()
                Else
                    'LableTitle.Text = "Перемещенные объекты учета можно прикрепить к разделу описи."
                    'UpdatePanelDisplacement.Visible = False
                    'UpdatePanelDisplacement.Update()


                    OpenModalDialogWindow(Me, "InventoryStructureChoose.aspx?INVENTORY=" + DocID.ToString(), ModalDialogArgument.ArgumentObject)
                    CloseWindow()
                End If

                
            Case Else
                AddMessage("Перемещение этого вида документов не предусмотренно. Странно, что вы смогли попасть на эту страницу.")
        End Select

        ' CloseWindow()
    End Sub

#Region " 2-й уровень"

    ''------------------------------------------------------------

    Private Sub DisplacementPreparation(ByRef selectedToDisplace As List(Of Guid), ByVal newContainerID As Guid, ByRef secLevelCheck As Boolean, ByRef docTypeCheck As Boolean, ByRef invClsConflictTable As DataTable, ByRef nameConflictCheck As String)
        Dim checkedObjectsList As New List(Of Guid)
        For Each selectedObj In selectedToDisplace
            If Not IsObjInContainer(selectedObj, newContainerID) Then
                checkedObjectsList.Add(selectedObj)
                secLevelCheck = secLevelCheck And SecureLevelCheck(selectedObj, newContainerID)
                docTypeCheck = docTypeCheck And DocumentTypeCheck(selectedObj, newContainerID)
                SearchNameConflict(selectedObj, newContainerID, nameConflictCheck)
                invClsConflictTable = GetInvClsConflictTable(selectedObj, newContainerID).Clone 'копирование структуры таблицы нужно для использования .Rows.Contains(...
                For Each row In GetInvClsConflictTable(selectedObj, newContainerID).Rows
                    If Not invClsConflictTable.Rows.Contains(row(0)) Then
                        invClsConflictTable.ImportRow(row) 'добавить конфликтующие разделы описи в специальную таблицу
                    End If
                Next

            End If

        Next
        selectedToDisplace = New List(Of Guid)(checkedObjectsList)
    End Sub

    Private Function GetInvClsConflictTable(ByVal objID As Guid, ByVal conID As Guid) As DataTable
        Dim cmdText As String
        If oName = "UNIT" Then
            cmdText = <sql>
                                    WITH INSU AS(
                                        SELECT INS.* 
                                        FROM tblUNIT U
                                        JOIN tblINVENTORY_STRUCTURE INS
                                            ON U.ISN_INVENTORY_CLS=INS.ISN_INVENTORY_CLS
                                        WHERE U.ID=@OID

                                        UNION ALL
                                        SELECT INSS.* 
                                        FROM tblINVENTORY_STRUCTURE INSS
                                        JOIN INSU 
                                            ON INSU.ISN_HIGH_INVENTORY_CLS=INSS.ISN_INVENTORY_CLS
                                    )
                                    SELECT * 
                                    FROM INSU
                                    WHERE Deleted = 0
                                    AND ISN_INVENTORY=(
                                        SELECT TOP 1 ISN_INVENTORY 
                                        FROM tblUNIT 
                                        WHERE ID=@OID
                                    )
                                    AND NAME NOT IN (
	                                    SELECT NAME
	                                    FROM tblINVENTORY_STRUCTURE
	                                    WHERE Deleted = 0
	                                    AND ISN_INVENTORY = (
		                                    SELECT ISN_INVENTORY
		                                    FROM tblINVENTORY
		                                    WHERE ID = @CID
	                                    )
                                    )
                                </sql>
        ElseIf oName = "INVENTORY" Then
            cmdText = <sql>
                            WITH INSF AS(
                                SELECT DISTINCT INS.* 
                                FROM tblUNIT U
                                JOIN tblINVENTORY I ON I.ISN_INVENTORY=U.ISN_INVENTORY
                                JOIN tblINVENTORY_STRUCTURE INS ON U.ISN_INVENTORY_CLS=INS.ISN_INVENTORY_CLS
                                WHERE I.ID=@OID
                                    AND U.Deleted=0
                                    AND INS.ISN_FUND is not null
                                UNION ALL
                                SELECT INSS.* 
                                FROM tblINVENTORY_STRUCTURE INSS
                                JOIN INSF ON INSF.ISN_HIGH_INVENTORY_CLS=INSS.ISN_INVENTORY_CLS
                            )
                            SELECT *
                            FROM INSF
                            WHERE Deleted = 0
                            AND ISN_FUND is not null
--                            AND NAME NOT IN (
--                                SELECT NAME
--                                FROM tblINVENTORY_STRUCTURE
--                                WHERE Deleted = 0
--                                AND ISN_FUND = (
--	                                SELECT ISN_FUND
--	                                FROM tblFUND
--	                                WHERE ID = @CID
--                                )
--                            )
                            
                      </sql>
        ElseIf oName = "DOCUMENT" Then
            Return New DataTable() ''не может быть никаких конфликтов разделов описи для документа
        Else
            cmdText = "Error in GetInvClsConflictTable"
        End If
        Dim cmd As New SqlCommand(cmdText)
        cmd.Parameters.AddWithValue("@OID", objID)
        cmd.Parameters.AddWithValue("@CID", conID)
        'invClsConflictTable.PrimaryKey = New DataColumn() {invClsConflictTable.Columns(0)}
        Dim ret As DataTable = AppSettings.GetDataTable(cmd)
        ret.PrimaryKey = New DataColumn() {ret.Columns(0)}
        ret.Columns.Add(New Data.DataColumn("old_ISN_INVENTORY_CLS")) 'создаем поле чтобы помнить старое сзначение
        Return ret 'AppSettings.GetDataTable(cmd)
    End Function

    Private Function IsObjInContainer(ByVal objID As Guid, ByVal containerID As Guid) As Boolean
        Dim cmdText As String = <sql>SELECT * 
                                    FROM tbl%CON%
                                    JOIN tbl%OBJ% 
                                        ON tbl%CON%.ISN_%CON%=tbl%OBJ%.ISN_%CON% 
                                    WHERE tbl%CON%.ID   =@CID 
                                        AND tbl%OBJ%.ID =@OID
                                </sql>
        cmdText = cmdText.Replace("%CON%", cName).Replace("%OBJ%", oName)
        Dim cmd As New SqlCommand(cmdText)
        cmd.Parameters.AddWithValue("@OID", objID)
        cmd.Parameters.AddWithValue("@CID", containerID)
        If AppSettings.ExecCommandScalar(cmd) IsNot Nothing Then
            Return True
        End If
        Return False
    End Function

    Private Function SecureLevelCheck(ByVal objID As Guid, ByVal conID As Guid) As Boolean
        Dim cmdText As String = <sql>   SELECT ISN_SECURLEVEL
                                        FROM tbl%CON%
                                        WHERE tbl%CON%.ID=@CID
                                </sql>
        cmdText = cmdText.Replace("%CON%", cName)
        Dim cmd As New SqlCommand(cmdText)
        cmd.Parameters.AddWithValue("@CID", conID)
        Dim conSL As Int32 = AppSettings.ExecCommandScalar(cmd)
        If conSL = 3 Then 'чс позволяет любые настройки секретности
            Return True
        End If
        cmdText = <sql>   SELECT ISN_SECURLEVEL
                                        FROM tbl%OBJ%
                                        WHERE tbl%OBJ%.ID=@OID
                                </sql>
        cmdText = cmdText.Replace("%OBJ%", oName)
        cmd = New SqlCommand(cmdText)
        cmd.Parameters.AddWithValue("@OID", objID)
        If IsDBNull(AppSettings.ExecCommandScalar(cmd)) Then
            Return True
        End If
        Dim objSL As Int32 = AppSettings.ExecCommandScalar(cmd)
        If objSL = conSL Then
            Return True
        End If

        Return False
    End Function

    Private Sub SearchNameConflict(ByVal objID As Guid, ByVal conID As Guid, ByRef answer As String)

        If answer = Nothing Then

            Dim getObjFromCon As String
            Dim cmdGetObjFromCon As SqlCommand
            Dim getObjToDisplace As String
            Dim cmdGetObjToDisplace As SqlCommand

            Select Case oName
                Case "INVENTORY"
                    getObjFromCon = <sql> select INVENTORY_NUM_1,INVENTORY_NUM_2,INVENTORY_NUM_3
                                                        from tblINVENTORY
                                                        WHERE ISN_FUND=(SELECT ISN_FUND FROM tblFUND WHERE ID=@CID)
                                                        AND Deleted=0
                                                  </sql>
                    cmdGetObjFromCon = New SqlCommand(getObjFromCon)
                    cmdGetObjFromCon.Parameters.AddWithValue("@CID", conID)

                    getObjToDisplace = <sql> select INVENTORY_NUM_1,INVENTORY_NUM_2,INVENTORY_NUM_3
                                                        from tblINVENTORY
                                                        WHERE ID=@OID
                                                        AND Deleted=0
                                                  </sql>
                    cmdGetObjToDisplace = New SqlCommand(getObjToDisplace)
                    cmdGetObjToDisplace.Parameters.AddWithValue("@OID", objID)

                    answer = "В фонде назначения уже существует Опись № %0%%1% ,том %2%. Перемещение невозможно."


                Case "UNIT"
                    getObjFromCon = <sql> select UNIT_KIND,UNIT_NUM_1,UNIT_NUM_2,VOL_NUM,UNIT_NUM_TXT--, *
                                                        from tblUNIT
                                                        WHERE ISN_INVENTORY=(SELECT ISN_INVENTORY FROM tblINVENTORY WHERE ID=@CID)
                                                        AND Deleted=0
                                                  </sql>
                    cmdGetObjFromCon = New SqlCommand(getObjFromCon)
                    cmdGetObjFromCon.Parameters.AddWithValue("@CID", conID)

                    getObjToDisplace = <sql> select UNIT_KIND,UNIT_NUM_1,UNIT_NUM_2,VOL_NUM,UNIT_NUM_TXT
                                                        from tblUNIT
                                                        WHERE ID=@OID
                                                        AND Deleted=0
                                                  </sql>
                    cmdGetObjToDisplace = New SqlCommand(getObjToDisplace)
                    cmdGetObjToDisplace.Parameters.AddWithValue("@OID", objID)


                Case "DOCUMENT"
                    getObjFromCon = <sql> select DOC_NUM
                                                        from tblDOCUMENT
                                                        WHERE ISN_UNIT=(SELECT ISN_UNIT FROM tblUNIT  WHERE ID=@CID)
                                                        AND Deleted=0
                                                  </sql>
                    cmdGetObjFromCon = New SqlCommand(getObjFromCon)
                    cmdGetObjFromCon.Parameters.AddWithValue("@CID", conID)

                    getObjToDisplace = <sql> select DOC_NUM
                                                        from tblDOCUMENT
                                                        WHERE ID=@OID
                                                        AND Deleted=0
                                                  </sql>
                    cmdGetObjToDisplace = New SqlCommand(getObjToDisplace)
                    cmdGetObjToDisplace.Parameters.AddWithValue("@OID", objID)

                    answer = "В единице хранения назначения уже существует Документ № %0%. Перемещение невозможно."

                Case Else
                    Throw New System.Exception("Ошибка в SearchNameConflict(). Неопознаный тип документа.")
                    Return
            End Select
            Dim tblObjFromCon As DataTable = AppSettings.GetDataTable(cmdGetObjFromCon) 'получаем имена хранящихся в контейнере объектов
            Dim tblObjToDisplace As DataTable = AppSettings.GetDataTable(cmdGetObjToDisplace) 'получаем имя перемещаемого объекта

            '''''''''''' Создание сообщения об ошибке конфликта имен
            Dim par As String
            If oName = "INVENTORY" Or oName = "DOCUMENT" Then
                For i As Integer = 0 To tblObjToDisplace.Rows(0).ItemArray.Length - 1
                    If IsDBNull(tblObjToDisplace.Rows(0)(i)) Then
                        par = "-"
                    Else
                        par = tblObjToDisplace.Rows(0)(i).ToString
                    End If
                    answer = answer.Replace("%" + i.ToString + "%", par)
                Next
            ElseIf oName = "UNIT" Then
                If tblObjToDisplace.Rows(0)(0) = 703 Then 'Ед.Хр.
                    answer = "В описи назначения уже существует Единица хранения № %TEXT% , том %3%. Перемещение невозможно."
                Else 'Ед.Уч.
                    answer = "В описи назначения уже существует Единица учета № %TEXT%. Перемещение невозможно."
                End If

                If IsDBNull(tblObjToDisplace.Rows(0)(4)) OrElse tblObjToDisplace.Rows(0)(4).ToString.Trim = "" Then
                    answer = answer.Replace("%TEXT%", "%1%%2%")
                Else
                    answer = answer.Replace("%TEXT%", "%4%")
                End If
                For i As Integer = 0 To tblObjToDisplace.Rows(0).ItemArray.Length - 1
                    If IsDBNull(tblObjToDisplace.Rows(0)(i)) Then
                        par = "-"
                    Else
                        par = tblObjToDisplace.Rows(0)(i).ToString
                    End If
                    answer = answer.Replace("%" + i.ToString + "%", par)
                Next
            End If
            ''''''''''''

            Dim flag As Integer

            For Each objFromCon As DataRow In tblObjFromCon.Rows
                flag = objFromCon.ItemArray.Length
                For i As Integer = 0 To objFromCon.ItemArray.Length - 1
                    If IsDBNull(objFromCon(i)) OrElse IsDBNull(tblObjToDisplace.Rows(0)(i)) OrElse objFromCon(i) = tblObjToDisplace.Rows(0)(i) Then
                        flag = flag - 1
                    End If
                Next
                If flag <= 0 Then
                    Return
                End If
            Next
            answer = Nothing
        End If
    End Sub

    Private Function DocumentTypeCheck(ByVal objID As Guid, ByVal conID As Guid) As Boolean
        Dim cmdText As String
        If oName = "INVENTORY" Then
            cmdText = <sql>   SELECT tbl%OBJ%_DOC_TYPE.ISN_DOC_TYPE 
                                        FROM tbl%OBJ%
                                        JOIN tbl%OBJ%_DOC_TYPE ON tbl%OBJ%.ISN_%OBJ%=tbl%OBJ%_DOC_TYPE.ISN_%OBJ%
                                        WHERE tbl%OBJ%.ID=@OID
                                         AND tbl%OBJ%_DOC_TYPE.ISN_DOC_TYPE IN (
                                             SELECT DISTINCT ISN_DOC_TYPE
                                             FROM tbl%CON%_DOC_TYPE
                                             WHERE ISN_%CON%=(
                                              SELECT ISN_%CON%
                                              FROM tbl%CON%
                                              WHERE ID=@CID
                                         )
                                        )
                                </sql>
        ElseIf oName = "UNIT" Then
            cmdText = <sql>   SELECT ISN_DOC_TYPE 
                                        FROM tbl%OBJ%
                                        WHERE ID=@OID
                                         AND ISN_DOC_TYPE IN (
                                             SELECT DISTINCT ISN_DOC_TYPE
                                             FROM tbl%CON%_DOC_TYPE
                                             WHERE ISN_%CON%=(
                                              SELECT ISN_%CON%
                                              FROM tbl%CON%
                                              WHERE ID=@CID
                                         )
                                        )
                                </sql>
        ElseIf oName = "DOCUMENT" Then
            Return True
        Else
            cmdText = "Error in DocumentTypeCheck"
        End If

        cmdText = cmdText.Replace("%OBJ%", oName)
        cmdText = cmdText.Replace("%CON%", cName)
        Dim cmd As New SqlCommand(cmdText)

        cmd.Parameters.AddWithValue("@CID", conID)
        cmd.Parameters.AddWithValue("@OID", objID)
        If AppSettings.ExecCommandScalar(cmd) Is Nothing Then
            If oName = "INVENTORY" Then
                ' тип документов отсутствует в контейнере
                'добавляем в фонд новые типы документов из перемещаемой описи
                cmdText = <sql>   SELECT tblINVENTORY_DOC_TYPE.ISN_DOC_TYPE, tblINVENTORY_DOC_TYPE.OwnerID, tblINVENTORY.ISN_FUND
                                        FROM tblINVENTORY
                                        JOIN tblINVENTORY_DOC_TYPE ON tblINVENTORY.ISN_INVENTORY=tblINVENTORY_DOC_TYPE.ISN_INVENTORY
                                        WHERE tblINVENTORY.ID=@OID
                                         AND tblINVENTORY_DOC_TYPE.ISN_DOC_TYPE NOT IN (
                                             SELECT DISTINCT ISN_DOC_TYPE
                                             FROM tblFUND_DOC_TYPE
                                             WHERE ISN_FUND=(
                                              SELECT ISN_FUND
                                              FROM tblFUND
                                              WHERE ID=@CID
                                         )
                                        )
                                </sql>
                cmd = New SqlCommand(cmdText)
                cmd.Parameters.AddWithValue("@CID", conID)
                cmd.Parameters.AddWithValue("@OID", objID)
                Dim docTypes As Data.DataTable = AppSettings.GetDataTable(cmd)
                Dim insertComandText As String = <sql>
                                        INSERT INTO tblFUND_DOC_TYPE VALUES (
	                                        newid(), @OwnerID, getdate(), @DocID, 
	                                        (SELECT MAX(RowID)+1 FROM tblFUND_DOC_TYPE), 
	                                        @ISN_FUND, @ISN_DOC_TYPE
                                        )
                                </sql>
                For Each row As Data.DataRow In docTypes.Rows
                    Dim insertCmd As New SqlCommand(insertComandText)
                    insertCmd.Parameters.AddWithValue("@OwnerID", row("OwnerID"))
                    insertCmd.Parameters.AddWithValue("@DocID", conID)
                    insertCmd.Parameters.AddWithValue("@ISN_FUND", row("ISN_FUND"))
                    insertCmd.Parameters.AddWithValue("@ISN_DOC_TYPE", row("ISN_DOC_TYPE"))
                    AppSettings.ExecCommand(insertCmd)
                Next
                Return True
            Else
                ' тип документов отсутствует в контейнере
                ' автоматическое добавление типа не предусмотрено
                Return False
            End If
        Else
            ' тип документов присутствует в контейнере
            Return True
        End If
    End Function

    Private Sub InvClsDisplace(ByRef invClsConflictTable As DataTable, ByVal newContaynerID As Guid)
        If invClsConflictTable.Rows.Count > 0 Then
            AppSettings.ExecCommand(New SqlCommand("ALTER TABLE tblINVENTORY_STRUCTURE NOCHECK CONSTRAINT FK_tblINVENTORY_STRUCTURE_tblINVENTORY_STRUCTURE"))
            Dim insertComandText As String = <sql>
                                        insert into tblINVENTORY_STRUCTURE 
                                        ( id, OwnerID, CreationDateTime, DocID, RowID, StatusID, Deleted, 
                                         ISN_INVENTORY_CLS, ISN_HIGH_INVENTORY_CLS, ISN_ARCHIVE, ISN_FUND, ISN_INVENTORY, 
                                         CODE, NAME, NOTE, FOREST_ELEM, PROTECTED, WEIGHT)
                                        values
                                        (newid(), @OwnerID_01, getdate(), @DocID_03, 0, @StatusID_05, 0,
                                        @ISN_INVENTORY_CLS_07, @ISN_HIGH_INVENTORY_CLS_08, @ISN_ARCHIVE_09, 
                                        @ISN_FUND_10, @ISN_INVENTORY_11, @CODE_12, @NAME_13, @NOTE, @FOREST_ELEM_15, @PROTECTED_16, @WEIGHT_17)
                                </sql>
            Dim insertCmd As SqlCommand
            Dim old_ISN_INVENTORY_CLS As String
            Dim ISN_INVENTORY_CLS_07 As String

            ISN_INVENTORY_CLS_07 = AppSettings.ExecCommandScalar(New SqlCommand("SELECT MAX(ISN_INVENTORY_CLS)+1 FROM tblINVENTORY_STRUCTURE"))
            For Each row As Data.DataRow In invClsConflictTable.Rows
                old_ISN_INVENTORY_CLS = row(7) 'ISN_INVENTORY_CLS
                For Each rrow As Data.DataRow In invClsConflictTable.Rows
                    If Not IsDBNull(rrow(8)) AndAlso rrow(8) = old_ISN_INVENTORY_CLS Then 'if ISN_HIGH_INVENTORY_CLS=old_ISN_INVENTORY_CLS
                        rrow(8) = ISN_INVENTORY_CLS_07 'заменяем ссылку на вышестоящий раздел
                    End If
                Next
                row(7) = ISN_INVENTORY_CLS_07
                ISN_INVENTORY_CLS_07 += 1
                row("old_ISN_INVENTORY_CLS") = old_ISN_INVENTORY_CLS
            Next
            For Each row As Data.DataRow In invClsConflictTable.Rows
                insertCmd = New SqlCommand(insertComandText)
                insertCmd.Parameters.AddWithValue("@OwnerID_01", row("OwnerID"))
                insertCmd.Parameters.AddWithValue("@DocID_03", row("DocID"))
                insertCmd.Parameters.AddWithValue("@StatusID_05", row("StatusID"))
                insertCmd.Parameters.AddWithValue("@ISN_INVENTORY_CLS_07", row("ISN_INVENTORY_CLS"))
                insertCmd.Parameters.AddWithValue("@ISN_HIGH_INVENTORY_CLS_08", row("ISN_HIGH_INVENTORY_CLS"))
                insertCmd.Parameters.AddWithValue("@ISN_ARCHIVE_09", row("ISN_ARCHIVE"))
                If oName = "UNIT" Then
                    insertCmd.Parameters.AddWithValue("@ISN_FUND_10", row("ISN_FUND"))
                    row("ISN_INVENTORY") = GetConIsnById(newContaynerID)
                    insertCmd.Parameters.AddWithValue("@ISN_INVENTORY_11", row("ISN_INVENTORY"))
                ElseIf oName = "INVENTORY" Then
                    row("ISN_FUND") = GetFundIsnById(newContaynerID)
                    insertCmd.Parameters.AddWithValue("@ISN_FUND_10", row("ISN_FUND"))
                    insertCmd.Parameters.AddWithValue("@ISN_INVENTORY_11", row("ISN_INVENTORY"))
                Else
                    Throw New System.Exception("Error in function 'InvClsDisplace(...)'")
                End If

                insertCmd.Parameters.AddWithValue("@CODE_12", row("CODE"))
                insertCmd.Parameters.AddWithValue("@NAME_13", row("NAME"))
                insertCmd.Parameters.AddWithValue("@NOTE", row("NOTE"))
                insertCmd.Parameters.AddWithValue("@FOREST_ELEM_15", row("FOREST_ELEM"))
                insertCmd.Parameters.AddWithValue("@PROTECTED_16", row("PROTECTED"))
                insertCmd.Parameters.AddWithValue("@WEIGHT_17", row("WEIGHT"))

                AppSettings.ExecCommand(insertCmd)
            Next
            AppSettings.ExecCommand(New SqlCommand("ALTER TABLE tblINVENTORY_STRUCTURE CHECK CONSTRAINT FK_tblINVENTORY_STRUCTURE_tblINVENTORY_STRUCTURE"))
        End If
    End Sub

    ''' <summary>
    ''' Делает перацию перемещения над всеми строками
    ''' </summary>
    ''' <param name="selectedToDisplace"></param>
    ''' <param name="newContaynerID"></param>
    ''' <param name="invClsConflictTable"></param>
    ''' <remarks></remarks>
    Private Sub DoDisplacement(ByRef selectedToDisplace As List(Of Guid), ByVal newContaynerID As Guid, ByRef invClsConflictTable As DataTable)
        Dim updateCmd As SqlCommand
        Dim old_ISN_INVENTORY_CLS As String

        For Each objId In selectedToDisplace
            'old_ISN_INVENTORY_CLS = GetNewObjIsnInventoryCls(objId, invClsConflictTable) 'invClsConflictTable.Rows.Find(objId)(0).ToString ' Get_Obj_ISN_INVENTORY_CLS(objId)
            old_ISN_INVENTORY_CLS = Get_Obj_ISN_INVENTORY_CLS(objId)


            If oName = "INVENTORY" Then ''у всех юнитов описи должны быть модефицированны разделы описи из invClsConflictTable
                RefwrightUnitsInInventory(objId, invClsConflictTable)
            End If
            updateCmd = MakeUpdateCmd(objId, GetNewIsnInventoryCls(old_ISN_INVENTORY_CLS, invClsConflictTable, objId, newContaynerID), GetConIsnById(newContaynerID))

            AppSettings.ExecCommand(updateCmd)
        Next
    End Sub

    ''' <summary>
    ''' Проверяет все юниты описи на предмет существования раздела описи в новом контейнере
    ''' </summary>
    ''' <param name="objID"></param>
    ''' <param name="invClsConflictTable"></param>
    ''' <remarks></remarks>
    Private Sub RefwrightUnitsInInventory(ByVal objID As Guid, ByRef invClsConflictTable As DataTable)
        Dim selectCmdText As String = <sql> 
                            SELECT ID,ISN_INVENTORY_CLS FROM tblUNIT
                            WHERE ISN_INVENTORY= (
                                SELECT ISN_INVENTORY 
                                FROM tblINVENTORY 
                                WHERE ID=@OID
                            )
                                    </sql> ''Этот запрос выведет все юниты данной описи
        Dim selectCmd As New SqlCommand(selectCmdText)
        selectCmd.Parameters.AddWithValue("@OID", objID)
        Dim units As Data.DataTable = AppSettings.GetDataTable(selectCmd)
        Dim updateCmdText As String = <sql>
                                        UPDATE tblUNIT
                                        SET ISN_INVENTORY_CLS=NULL --@ISN_INVENTORY_CLS
                                        WHERE ID=@UnitID
                                      </sql>
        Dim updateCmd As SqlCommand
        For Each unit In units.Rows
            For Each row In invClsConflictTable.Rows
                updateCmd = New SqlCommand(updateCmdText)
                If Not IsDBNull(unit("ISN_INVENTORY_CLS")) AndAlso Not IsDBNull(row("ISN_INVENTORY_CLS")) AndAlso unit("ISN_INVENTORY_CLS") = row("ISN_INVENTORY_CLS") Then
                    'updateCmd.Parameters.AddWithValue("@ISN_INVENTORY_CLS", row("ISN_INVENTORY_CLS"))
                    updateCmd.Parameters.AddWithValue("@UnitID", unit("ID"))
                End If
                AppSettings.ExecCommand(updateCmd)
            Next
        Next



    End Sub

    ''------------------------------------------------------------

    ''' <summary>
    ''' Смена хор-ки секретности у описи. По умолчанию ставит уровень 'чс'. 1 - откр, 2 - с, 3 - чс.
    ''' </summary>
    ''' <param name="SecureLevel">Параметр добавлен из соображений читаемости и с перспективой на расширение. Фактически сейчас он ни на что не влияет.</param>
    ''' <remarks></remarks>
    Private Sub ChangeConSecureLevel(ByVal containerID As Guid, Optional ByVal SecureLevel As Int32 = 3)
        If SecureLevel = 3 Then
            Dim cmdText As String = <sql>UPDATE tbl%CON%
                                        SET ISN_SECURLEVEL=@SecLevel
                                        WHERE ID=@CID</sql>
            cmdText = cmdText.Replace("%CON%", cName)
            Dim cmd As New SqlCommand(cmdText)
            cmd.Parameters.AddWithValue("@SecLevel", SecureLevel)
            cmd.Parameters.AddWithValue("@CID", containerID)
            AppSettings.ExecCommand(cmd)
            Return
        Else
            ''замена хор-к секретности на 'с' или 'о' в 99% случаев приведет к конфликту с хор-кой секретности хранимых юнитов
            ''в рамках задачи перемещения такие изменения не предполагаются
            Return
        End If
    End Sub


    ''' <summary>
    ''' Создает sqlComand, которая меняет контейнер для одного юнита
    ''' </summary>
    ''' <param name="unitID"></param>
    ''' <param name="old_ISN_INVENTORY_CLS"></param>
    ''' <param name="new_ISN_INVENTORY"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function MakeUpdateCmd(ByVal objID As Guid, ByVal new_ISN_INVENTORY_CLS As String, ByVal new_ISN_CON As String) As SqlCommand
        Dim ret As SqlCommand
        Dim updateCmdText As String

        If new_ISN_INVENTORY_CLS Is Nothing Then
            If oName = "UNIT" Then
                updateCmdText = <sql>
                                        UPDATE tbl%OBJ%
                                        SET ISN_%CON%=@ISN_NEW_CON, ISN_INVENTORY_CLS=null
                                        WHERE ID=@OID
                                    </sql>
            Else
                updateCmdText = <sql>
                                        UPDATE tbl%OBJ%
                                        SET ISN_%CON%=@ISN_NEW_CON
                                        WHERE ID=@OID
                                    </sql>
            End If
            updateCmdText = updateCmdText.Replace("%OBJ%", oName).Replace("%CON%", cName)
            ret = New SqlCommand(updateCmdText)
        Else
            updateCmdText = <sql>
                                        UPDATE tbl%OBJ%
                                        SET ISN_%CON%=@ISN_NEW_CON, ISN_INVENTORY_CLS=@ISN_INVENTORY_CLS
                                        WHERE ID=@OID
                                    </sql>
            updateCmdText = updateCmdText.Replace("%OBJ%", oName).Replace("%CON%", cName)
            ret = New SqlCommand(updateCmdText)
            ret.Parameters.AddWithValue("@ISN_INVENTORY_CLS", new_ISN_INVENTORY_CLS)
        End If
        ret.Parameters.AddWithValue("@ISN_NEW_CON", new_ISN_CON)
        ret.Parameters.AddWithValue("@OID", objID.ToString)
        Return ret
    End Function

#End Region

#Region " 1-й уровень"

    ''' <summary>
    ''' Составляет список привязаных к данному юниту элементов из таблицы UNIT (например, все ед.уч, которые относятся к данной ед.хр)
    ''' </summary>
    ''' <param name="unitID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetRelatedUnitsList(ByVal unitID) As List(Of Guid)
        Dim ret As New List(Of Guid)
        Dim cmdText As String = <sql>   SELECT U.ID 
                                        FROM tblUNIT U
                                        WHERE U.ID=@UID

                                        UNION

                                        SELECT U.ID 
                                        FROM tblUNIT U
                                        WHERE ISN_HIGH_UNIT=(
                                            SELECT TOP 1 ISN_UNIT 
                                            FROM tblUNIT
                                            WHERE ID=@UID
                                        )

                                        UNION

                                        SELECT U.ID 
                                        FROM tblUNIT U
                                        WHERE ISN_UNIT=(
                                            SELECT TOP 1 ISN_HIGH_UNIT 
                                            FROM tblUNIT 
                                            WHERE ID=@UID
                                        )
                                </sql>
        Dim cmd As New SqlCommand(cmdText)
        cmd.Parameters.AddWithValue("@UID", unitID)
        For Each row In AppSettings.GetDataTable(cmd).Rows
            ret.Add(row(0))
        Next
        Return ret
    End Function

    ''' <summary>
    ''' Возвращает строки таблицы разделов описи, к которым относится данный юнит и которые уникальны для его описи.
    ''' Т.е. при переносе в другую опись нужно будет создать такие же разделы в ней.
    ''' </summary>
    ''' <param name="UnitID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetUnitsClsConflictTable(ByVal UnitID) As DataTable
        Dim cmdText As String = <sql>
                                    WITH INSU AS(
                                        SELECT INS.* 
                                        FROM tblUNIT U
                                        JOIN tblINVENTORY_STRUCTURE INS
                                            ON U.ISN_INVENTORY_CLS=INS.ISN_INVENTORY_CLS
                                        WHERE U.ID=@UID

                                        UNION ALL
                                        SELECT INSS.* 
                                        FROM tblINVENTORY_STRUCTURE INSS
                                        JOIN INSU 
                                            ON INSU.ISN_HIGH_INVENTORY_CLS=INSS.ISN_INVENTORY_CLS
                                    )
                                    SELECT * FROM INSU
                                    WHERE ISN_INVENTORY=(
                                        SELECT TOP 1 ISN_INVENTORY 
                                        FROM tblUNIT 
                                        WHERE ID=@UID
                                    )
                                </sql>
        Dim cmd As New SqlCommand(cmdText)
        cmd.Parameters.AddWithValue("@UID", UnitID)
        Return AppSettings.GetDataTable(cmd)

        'dtbl.Rows.Contains()
    End Function



    Private Function GetConIsnById(ByVal containerId As Guid) As String
        Dim cmdText As String = <sql>
                                        SELECT ISN_%CON%
                                        FROM tbl%CON%
                                        WHERE ID=@CID
                                </sql>
        cmdText = cmdText.Replace("%CON%", cName)
        Dim cmd As New SqlCommand(cmdText)
        cmd.Parameters.AddWithValue("@CID", containerId)
        Return AppSettings.ExecCommandScalar(cmd).ToString  'проверь, точно ли он корректно конвертирует стринг из инта
        Return "Error in GetConIsnById"
    End Function


    Private Function GetConIsnByObjId(ByVal objectId As Guid) As Int64
        Dim cmdText As String = <sql>
                                        SELECT tbl%CON%.ISN_%CON%
                                        FROM tbl%CON%
                                        JOIN tbl%OBJ% ON tbl%CON%.ISN_%CON%=tbl%OBJ%.ISN_%CON%
                                        WHERE tbl%OBJ%.ID=@OID
                                </sql>
        cmdText = cmdText.Replace("%CON%", cName).Replace("%OBJ%", oName)
        Dim cmd As New SqlCommand(cmdText)
        cmd.Parameters.AddWithValue("@OID", objectId)
        Return AppSettings.ExecCommandScalar(cmd)
        Return 0
    End Function


    Private Function GetFundIsnById(ByVal fundId As Guid) As Int64
        Dim cmdText As String = <sql>
                                        SELECT ISN_FUND
                                        FROM tblFUND
                                        WHERE ID=@FID
                                </sql>
        Dim cmd As New SqlCommand(cmdText)
        cmd.Parameters.AddWithValue("@FID", fundId)
        Return AppSettings.ExecCommandScalar(cmd)
        Return 0
    End Function



    Private Function Get_Obj_ISN_INVENTORY_CLS(ByVal unitID As Guid) As String
        Dim ret As String
        'Dim selectCmdText As String = <sql>
        '                                SELECT ISN_INVENTORY_CLS
        '                                FROM tblUNIT
        '                                WHERE ID=@UID
        '                            </sql>
        Dim selectCmd As New SqlCommand(<sql>
                                        SELECT ISN_INVENTORY_CLS
                                        FROM tblUNIT
                                        WHERE ID=@UID
                                        </sql>)
        selectCmd.Parameters.AddWithValue("@UID", unitID)
        Try
            ret = AppSettings.ExecCommandScalar(selectCmd)
        Catch ex As InvalidCastException
            ret = Nothing
        End Try
        Return ret
    End Function

    ''' <summary>
    ''' По старому ISN позвращает новое 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetNewIsnInventoryCls(ByVal oldISN As String, ByRef invClsConflictTable As DataTable, ByVal objectId As Guid, ByVal containerId As Guid) As String
        Select Case oName
            Case "UNIT"
                For Each row In invClsConflictTable.Rows
                    If row("old_ISN_INVENTORY_CLS") = oldISN Then
                        Return row("ISN_INVENTORY_CLS")
                    End If
                Next
                Dim selectCmd As New SqlCommand(<sql>
SELECT TOP 1 ISN_INVENTORY_CLS
FROM tblINVENTORY_STRUCTURE
WHERE Deleted = 0
AND ISN_INVENTORY = (
	SELECT TOP 1 ISN_INVENTORY
	FROM tblINVENTORY
	WHERE ID = @CID
)
AND NAME = (
	SELECT TOP 1 NAME
	FROM tblINVENTORY_STRUCTURE
	WHERE Deleted = 0
	AND ISN_INVENTORY_CLS = (
		SELECT ISN_INVENTORY_CLS
		FROM tblUNIT
		WHERE ID = @OID
	)
)
                                        </sql>)
                selectCmd.Parameters.AddWithValue("@OID", objectId)
                selectCmd.Parameters.AddWithValue("@CID", containerId)
                Dim ret As String = AppSettings.ExecCommandScalar(selectCmd)
                If ret <> Nothing Then
                    Return ret
                End If
                Return Nothing
            Case "INVENTORY"


                Return Nothing
            Case "DOCUMENT"
                Return Nothing
            Case Else
                Return "Error in GetNewObjIsnInventoryCls(...)"
        End Select


    End Function

    Private Sub InitObjConNames(ByVal docTypeUrl As String)
        Select Case docTypeUrl
            Case "UNIT.ascx"
                cName = "INVENTORY"
                oName = "UNIT"
            Case "INVENTORY.ascx"
                cName = "FUND"
                oName = "INVENTORY"
            Case "DOCUMENT.ascx"
                cName = "UNIT"
                oName = "DOCUMENT"
            Case Else
                cName = "ERROR IN InitObjConNames"
                oName = "ERROR IN InitObjConNames"
        End Select
    End Sub

#End Region


End Class