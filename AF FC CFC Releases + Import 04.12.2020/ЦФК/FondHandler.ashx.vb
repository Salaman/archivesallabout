﻿Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.Services

Public Class FondHandler
    Implements System.Web.IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.AddHeader("content-disposition", "attachment; filename= Estimate1.xlsx")
        context.Response.ContentType = "application/excel"
        Dim ExcelWorkbook As New OfficeOpenXml.ExcelPackage
        Dim ws = ExcelWorkbook.Workbook.Worksheets.Add("MySheet")

        Dim conn = New SqlConnection(My.MySettings.Default.DatabaseConnectionString)
        Dim cmd = New SqlCommand("SELECT * FROM vDOC_COUNT", conn)
        conn.Open()
        Dim reader = cmd.ExecuteReader()
        Dim rowNumber = 1
        Dim nodes = New List(Of Node)
        While reader.Read()
            Dim node = New Node()
            node.ISN_CLS = CLng(reader("ISN_CLS"))
            If IsDBNull(reader("ISN_HIGH_CLS")) Then
                node.ISN_HIGH_CLS = 0
            Else
                node.ISN_HIGH_CLS = CLng(reader("ISN_HIGH_CLS"))
            End If
            node.LEVEL = 0
            node.NAME = reader("NAME").ToString
            node.CODE = reader("CODE").ToString
            node.DOC_COUNT = CInt(reader("DOC_COUNT"))
            nodes.Add(node)
        End While
        conn.Close()
        Dim roots = nodes.Where(Function(x) x.ISN_HIGH_CLS = 0)
        Dim tree = New List(Of Node)
        Dim root = roots.ToList()(1)
        AddNodeToTree(root, nodes, 0)
        tree.Add(root)
        WriteNodeToExcel(root, rowNumber, ws)
        context.Response.BinaryWrite(ExcelWorkbook.GetAsByteArray())
    End Sub

    Private Sub AddNodeToTree(node As Node, source As List(Of Node), level As Integer)
        node.CHILD = New List(Of Node)
        Dim childrens = source.Where(Function(x) x.ISN_HIGH_CLS = node.ISN_CLS)
        For Each child In childrens
            child.LEVEL = level
            AddNodeToTree(child, source, level + 1)
            node.CHILD.Add(child)
            node.DOC_COUNT = node.DOC_COUNT + child.DOC_COUNT
        Next
    End Sub

    Private Sub WriteNodeToExcel(node As Node, ByRef rowNumber As Integer, ws As OfficeOpenXml.ExcelWorksheet)
        ws.Cells(rowNumber, 1).Value = node.CODE
        ws.Cells(rowNumber, 2 + node.LEVEL).Value = node.NAME
        ws.Cells(rowNumber, 9).Value = node.DOC_COUNT
        For Each child In node.CHILD
            rowNumber = rowNumber + 1
            WriteNodeToExcel(child, rowNumber, ws)
        Next
    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Class Node
        Public ISN_CLS As Long
        Public ISN_HIGH_CLS As Long
        Public LEVEL As Integer
        Public CODE As String
        Public NAME As String
        Public DOC_COUNT As Integer
        Public CHILD As List(Of Node)
    End Class

End Class