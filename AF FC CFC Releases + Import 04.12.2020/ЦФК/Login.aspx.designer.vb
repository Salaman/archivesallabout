﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:2.0.50727.4952
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On



Partial Public Class Login

    '''<summary>
    '''PanelLoginPadding control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PanelLoginPadding As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''LocalizeUser control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LocalizeUser As Global.System.Web.UI.WebControls.Localize

    '''<summary>
    '''TextBoxLogin control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TextBoxLogin As Global.Equipage.WebUI.eqTextBox

    '''<summary>
    '''LocalizePassword control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LocalizePassword As Global.System.Web.UI.WebControls.Localize

    '''<summary>
    '''TextBoxPassword control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TextBoxPassword As Global.Equipage.WebUI.eqTextBox

    '''<summary>
    '''ButtonLogin control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ButtonLogin As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''PanelMessagesPadding control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PanelMessagesPadding As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''PanelMessagesContent control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PanelMessagesContent As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''PlaceHolderMessages control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PlaceHolderMessages As Global.System.Web.UI.WebControls.PlaceHolder

    '''<summary>
    '''Master property.
    '''</summary>
    '''<remarks>
    '''Auto-generated property.
    '''</remarks>
    Public Shadows ReadOnly Property Master() As WebApplication.Site
        Get
            Return CType(MyBase.Master, WebApplication.Site)
        End Get
    End Property
End Class
