﻿Public Partial Class Site
    Inherits System.Web.UI.MasterPage

    Public ReadOnly Property HeaderPanel() As Panel
        Get
            Return PanelSystem
        End Get
    End Property

#Region "Page Events"

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        RefreshError.Visible = False
        ' Переводим то, что зашито в тему
        ImageEquipageLogo.ToolTip = GetGlobalResourceObject("eqResources", "TIP_EquipageLogo")
        HyperLinkLogo.ToolTip = GetGlobalResourceObject("eqResources", "TIP_Logo")
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Not ToolkitScriptManagerDefault.IsInAsyncPostBack Then
            ' Высота для XHTML 1.0 Transitional
            Dim GetWindowParamsSB As New StringBuilder
            GetWindowParamsSB.AppendLine("function GetWindowHeight()")
            GetWindowParamsSB.AppendLine("{")
            GetWindowParamsSB.AppendLine(" if (self.innerHeight)")
            GetWindowParamsSB.AppendLine(" {")
            GetWindowParamsSB.AppendLine("     return self.innerHeight;")
            GetWindowParamsSB.AppendLine(" }")
            GetWindowParamsSB.AppendLine(" else if (document.documentElement && document.documentElement.clientHeight)")
            GetWindowParamsSB.AppendLine(" {")
            GetWindowParamsSB.AppendLine("     return document.documentElement.clientHeight;")
            GetWindowParamsSB.AppendLine(" }")
            GetWindowParamsSB.AppendLine(" else if (document.body)")
            GetWindowParamsSB.AppendLine(" {")
            GetWindowParamsSB.AppendLine("     return document.body.clientHeight;")
            GetWindowParamsSB.AppendLine(" }")
            GetWindowParamsSB.AppendLine("};")
            GetWindowParamsSB.AppendLine("function GetWindowWidth()")
            GetWindowParamsSB.AppendLine("{")
            GetWindowParamsSB.AppendLine(" if (self.innerWidth)")
            GetWindowParamsSB.AppendLine(" {")
            GetWindowParamsSB.AppendLine("     return self.innerWidth;")
            GetWindowParamsSB.AppendLine(" }")
            GetWindowParamsSB.AppendLine(" else if (document.documentElement && document.documentElement.clientWidth)")
            GetWindowParamsSB.AppendLine(" {")
            GetWindowParamsSB.AppendLine("     return document.documentElement.clientWidth;")
            GetWindowParamsSB.AppendLine(" }")
            GetWindowParamsSB.AppendLine(" else if (document.body)")
            GetWindowParamsSB.AppendLine(" {")
            GetWindowParamsSB.AppendLine("     return document.body.clientWidth;")
            GetWindowParamsSB.AppendLine(" }")
            GetWindowParamsSB.AppendLine("};")
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType, "GetWindowParams", GetWindowParamsSB.ToString, True)
            Dim SetPanelContentMinHeightSB As New StringBuilder
            SetPanelContentMinHeightSB.AppendLine("function SetPanelContentMinHeightWidth()")
            SetPanelContentMinHeightSB.AppendLine("{")
            SetPanelContentMinHeightSB.AppendLine(" document.getElementById('tdCompensator').style.height = Math.max(0,GetWindowHeight()-document.getElementById('tdContent').clientHeight-16)+'px';")
            'SetPanelContentMinHeightSB.AppendLine(" document.getElementById('tdCompensator').innerHTML = GetWindowHeight()+' - '+document.getElementById('tdContent').clientHeight+' - '+'16'+'px';")
            SetPanelContentMinHeightSB.AppendLine("};")
            SetPanelContentMinHeightSB.AppendLine("$addHandler(window,'load',SetPanelContentMinHeightWidth);")
            SetPanelContentMinHeightSB.AppendLine("$addHandler(window,'resize',SetPanelContentMinHeightWidth);")
            ScriptManager.RegisterStartupScript(Page, Page.GetType, "SetPanelContentMinHeightWidth", SetPanelContentMinHeightSB.ToString, True)
            '' UpdateProgress when navigate
            'Dim ShowUpdateProgressSB As New StringBuilder
            'ShowUpdateProgressSB.AppendLine("function ShowUpdateProgress()")
            'ShowUpdateProgressSB.AppendLine("{")
            'ShowUpdateProgressSB.AppendLine("   var panel = document.getElementById('" & UpdateProgressUpdatePanelPlaceHolderContainer.ClientID & "');")
            'ShowUpdateProgressSB.AppendLine("   if(!Sys.Browser.WebKit){panel.innerHTML = panel.innerHTML;}")
            'ShowUpdateProgressSB.AppendLine("   panel.style.display = '';")
            'ShowUpdateProgressSB.AppendLine("};")
            'ShowUpdateProgressSB.AppendLine("function HideUpdateProgress()")
            'ShowUpdateProgressSB.AppendLine("{")
            'ShowUpdateProgressSB.AppendLine("   var panel = document.getElementById('" & UpdateProgressUpdatePanelPlaceHolderContainer.ClientID & "');")
            'ShowUpdateProgressSB.AppendLine("   panel.style.display = 'none';")
            'ShowUpdateProgressSB.AppendLine("};")
            'ShowUpdateProgressSB.AppendLine("$addHandler(window,'beforeunload',ShowUpdateProgress);")
            'ShowUpdateProgressSB.AppendLine("$addHandler(document,'readystatechange',HideUpdateProgress);")
            'ScriptManager.RegisterStartupScript(Page, Page.GetType, "ShowUpdateProgress", ShowUpdateProgressSB.ToString, True)
            ' Сохранение фокуса после пост бэка
            Dim MaintainFocusSB As New StringBuilder
            MaintainFocusSB.AppendLine("function MaintainFocus()")
            MaintainFocusSB.AppendLine("{")
            MaintainFocusSB.AppendLine("    var lfel=document.getElementById('" & FocusedHiddenField.ClientID & "');")
            MaintainFocusSB.AppendLine("    var ael=document.activeElement;")
            MaintainFocusSB.AppendLine("    if (lfel!=null)")
            MaintainFocusSB.AppendLine("    {")
            MaintainFocusSB.AppendLine("        if (ael!=null)")
            MaintainFocusSB.AppendLine("        {")
            MaintainFocusSB.AppendLine("            lfel.value = ael.id;")
            MaintainFocusSB.AppendLine("        }")
            MaintainFocusSB.AppendLine("        else")
            MaintainFocusSB.AppendLine("        {")
            MaintainFocusSB.AppendLine("            lfel.value = '';")
            MaintainFocusSB.AppendLine("        }")
            MaintainFocusSB.AppendLine("    }")
            MaintainFocusSB.AppendLine("};")
            MaintainFocusSB.AppendLine("function RestoreFocus()")
            MaintainFocusSB.AppendLine("{")
            MaintainFocusSB.AppendLine("    var lfel=document.getElementById('" & FocusedHiddenField.ClientID & "');")
            MaintainFocusSB.AppendLine("    if (lfel!=null)")
            MaintainFocusSB.AppendLine("    {")
            MaintainFocusSB.AppendLine("        var ael=document.getElementById(lfel.value+'');")
            MaintainFocusSB.AppendLine("        if (ael!=null)")
            MaintainFocusSB.AppendLine("        {")
            MaintainFocusSB.AppendLine("            ael.focus();")
            MaintainFocusSB.AppendLine("        }")
            MaintainFocusSB.AppendLine("    }")
            MaintainFocusSB.AppendLine("};")
            ScriptManager.RegisterStartupScript(Page, Page.GetType, "MaintainFocus", MaintainFocusSB.ToString, True)
            ScriptManager.RegisterOnSubmitStatement(Page, Page.GetType, "MaintainFocus", "MaintainFocus();")
        End If
        If FocusedHiddenField.Value <> String.Empty Then
            If Context.Request.Browser.Browser = "Firefox" Then
                ScriptManager.RegisterStartupScript(Page, Page.GetType, "RestoreFocus", "RestoreFocus();", True)
            Else
                Page.Form.DefaultFocus = FocusedHiddenField.Value
            End If
        End If
    End Sub

#End Region

    Private Sub ImageEquipageLogo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageEquipageLogo.Click
        Try
            Dim CurrentAppSettings = DirectCast(Session("AppSettings"), eqAppSettings)
            If CurrentAppSettings IsNot Nothing Then
                Dim UserAppSettings As New eqAppSettings
                UserAppSettings.AdministratorUserLogin = My.Settings.AdministratorUserLogin
                UserAppSettings.RolesAllowPriority = My.Settings.RolesAllowPriority
                UserAppSettings.Manager = My.Resources.ResourceManager
                UserAppSettings.OpenSession(CurrentAppSettings.Cnn, CurrentAppSettings.UserInfo.UserLogin)
                Session("AppSettings") = UserAppSettings
                CurrentAppSettings = Nothing
                RefreshError.Text = "Refresh successfully!"
                RefreshError.Visible = True
                If TypeOf Page Is _Default Then
                    DirectCast(Page, _Default).UpdateSystemMenu()
                End If
            End If
        Catch ex As Exception
            RefreshError.Text = ex.Message
            RefreshError.Visible = True
        End Try
    End Sub

    Public Function GetSiteTite() As String
        Return My.Resources.SiteTitle()
    End Function

End Class