﻿Public Partial Class About
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        DataBind()
    End Sub

    Public Function GetTitle() As String
#If FC Then
        Return "Фондовый Каталог"
#ElseIf CFC Then
        Return "Центральный Фондовый Каталог"
#ElseIf GR Then
        Return "Госреестр уникальных документов Архивного фонда Российской Федерации"
#Else
        Return "Архивный Фонд"
#End If
    End Function

    Public Function GetVersion() As String
#If GR Then
        Return "2.0"
#Else

     Return String.Format("{0}.{1}.{2}", My.Application.Info.Version.Major, My.Application.Info.Version.Minor, My.Application.Info.Version.Build)
#End If

    End Function

    Public Function GetCopyright() As String
#If GR Then
        Return "Copyright ©  2015 АДАПТ"
#Else
        Return My.Application.Info.Copyright
#End If

    End Function

End Class