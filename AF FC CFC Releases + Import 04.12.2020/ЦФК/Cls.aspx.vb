﻿Imports System.Data.SqlClient

Partial Public Class Cls1
    Inherits BasePage

    Private Sub ButtonCls_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCls.Click
        Dim cmd As New SqlClient.SqlCommand
        cmd.Parameters.AddWithValue("@OwnerID", AppSettings.UserInfo.UserID)
        cmd.Parameters.Add(New SqlParameter("@ISN_REF_CLS", SqlDbType.BigInt) With {.Value = 0})
        cmd.Parameters.Add(New SqlParameter("@ISN_CLS", SqlDbType.BigInt) With {.Value = 0})
        cmd.Parameters.Add(New SqlParameter("@ISN_OBJ", SqlDbType.BigInt) With {.Value = 0})
        cmd.Parameters.Add(New SqlParameter("@KIND", SqlDbType.BigInt) With {.Value = 0})

        Dim selectedDocs = DirectCast(ModalDialogArgument.ArgumentObject, List(Of Guid))
        For Each selectedCls In ListDocCLS701.SelectedDocuments
            For Each selectedDoc In selectedDocs
                cmd.CommandText += GetInsertRefClsCmd(Request.QueryString("DocTypeURL"), selectedDoc, selectedCls)
            Next
        Next
        For Each selectedCls In ListDocCLS702.SelectedDocuments
            For Each selectedDoc In selectedDocs
                cmd.CommandText += GetInsertRefClsCmd(Request.QueryString("DocTypeURL"), selectedDoc, selectedCls)
            Next
        Next
        For Each selectedCls In ListDocCLS703.SelectedDocuments
            For Each selectedDoc In selectedDocs
                cmd.CommandText += GetInsertRefClsCmd(Request.QueryString("DocTypeURL"), selectedDoc, selectedCls)
            Next
        Next

        If String.IsNullOrEmpty(cmd.CommandText) Then
            AddMessage("Выберите не менее одного АНСА")
        Else
            AppSettings.ExecCommand(cmd)
            CloseWindow()
        End If
    End Sub

    Private Function GetInsertRefClsCmd(ByVal docTypeURL As String, ByVal docId As Guid, ByVal clsId As Guid) As String
        Dim isnRefCls = "SELECT @ISN_REF_CLS=ISNULL(MAX(ISN_REF_CLS)+1,1) FROM tblREF_CLS" & vbCrLf
        Dim isnCls = "SELECT @ISN_CLS=ISN_CLS FROM tblCLS WHERE ID='" & clsId.ToString & "'" & vbCrLf
        Dim isnObj As String = Nothing
        Dim kind As String = Nothing

        Select Case docTypeURL.ToUpper
            Case "FUND.ASCX"
                isnObj = "SELECT @ISN_OBJ=ISN_FUND FROM tblFUND WHERE ID='" & docId.ToString & "'" & vbCrLf
                kind = "SELECT @KIND=701" & vbCrLf
            Case "INVENTORY.ASCX"
                isnObj = "SELECT @ISN_OBJ=ISN_INVENTORY FROM tblINVENTORY WHERE ID='" & docId.ToString & "'" & vbCrLf
                kind = "SELECT @KIND=702" & vbCrLf
            Case "UNIT.ASCX"
                isnObj = "SELECT @ISN_OBJ=ISN_UNIT FROM tblUNIT WHERE ID='" & docId.ToString & "'" & vbCrLf
                kind = "SELECT @KIND=UNIT_KIND FROM tblUNIT WHERE ID='" & docId.ToString & "'" & vbCrLf
            Case "DOCUMENT.ASCX"
                isnObj = "SELECT @ISN_OBJ=ISN_DOCUM FROM tblDOCUMENT WHERE ID='" & docId.ToString & "'" & vbCrLf
                kind = "SELECT @KIND=705" & vbCrLf
        End Select

        Return isnRefCls & isnCls & isnObj & kind & "DELETE FROM [tblREF_CLS] WHERE DocID='" & docId.ToString & "' AND ISN_CLS=@ISN_CLS" & vbCrLf & _
        "INSERT INTO [tblREF_CLS] ([ID],[OwnerID],[CreationDateTime],[DocID],[RowID],[ISN_REF_CLS],[ISN_CLS],[ISN_CLSID],[ISN_TREE],[ISN_OBJ],[ORDER_NUM],[KIND],[NOTE])" & vbCrLf & _
                            "VALUES (NEWID(),@OwnerID,GETDATE(),'" & docId.ToString & "',0,@ISN_REF_CLS,@ISN_CLS,NULL,NULL,@ISN_OBJ,NULL,@KIND,NULL)" & vbCrLf
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Select Case Request.QueryString("DocTypeURL").ToUpper
                Case "FUND.ASCX"
                    MenuTabs.Items.RemoveAt(2)
            End Select
        End If
    End Sub

    Private Sub Cls1_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        MultiViewTabs.ActiveViewIndex = CInt(MenuTabs.SelectedValue)
    End Sub

End Class