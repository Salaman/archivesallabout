﻿Imports System.Data.SqlClient

Public Class Recalc

#Region "SQL"
    Private Shared SQL As XElement() = {<sql>


-- Пересчет фонда

-- замена параметров для тестирования запроса
/*
DECLARE @SROK_HRAN bit = 1				-- срок хранения
DECLARE @NACH_DATA bit = 1				-- начальная дата документов
DECLARE @KON_DATA bit = 1				-- конечная дата документов
DECLARE @VVEDENO_O bit = 1				-- введено описей
DECLARE @HAS_MUSEUM bit = 1				-- наличие музейных предметов
DECLARE @TREASURE_COUNT bit = 1			-- кол-во ед хр с драг камнями и металлами в оформлении

DECLARE @ERH_ALL bit = 1				-- всего ед хранения
DECLARE @EU_ALL bit = 1					-- всего ед учета
DECLARE @ERHO_ALL bit = 1				-- кол-во ед хр по описям
DECLARE @EUO_ALL bit = 1				-- всего ед уч по описям
DECLARE @NEOP_EHR bit = 1				-- кол-во неоп ед хр, документов, листов
DECLARE @EHR_REG bit = 1				-- кол-во зарег ед хр
DECLARE @EU_REG bit = 1					-- кол-во зарег ед уч
DECLARE @ERH_OC bit = 1					-- кол-во особо ценных ед хр
DECLARE @EU_OC bit = 1					-- кол-во особо ценных ед уч
DECLARE @ERH_U bit = 1					-- кол-во уникальных ед хр
DECLARE @EU_U bit = 1					-- кол-во уникальных ед уч
DECLARE @EHR_SF bit = 1					-- кол-во ед хр имеющих СФ
DECLARE @EU_SF bit = 1					-- кол-во ед уч имеющих СФ
DECLARE @EHR_FP bit = 1					-- кол-во ед хр имеющих ФП
DECLARE @EU_FP bit = 1					-- кол-во ед уч имеющих ФП
DECLARE @EHR_NEOB bit = 1				-- кол-во необнаруж ед хр
DECLARE @EU_NEOB bit = 1				-- кол-во необнаруж ед уч
DECLARE @EHR_SEC bit = 1				-- кол-во секретных ед хр
DECLARE @EU_SEC bit = 1					-- кол-во секретных ед уч
DECLARE @EHR_CAT bit = 1				-- кол-во закат ед хр
DECLARE @EU_CAT bit = 1					-- кол-во закат ед уч

DECLARE @E_KARTED bit = 1				-- кол-во закартониров ед хр
DECLARE @E_SHIFR bit = 1				-- кол-во ед хр требующих шифровки
DECLARE @E_KART bit = 1					-- кол-во ед хр требующих картониз
DECLARE @E_ZAM_OBL bit = 1				-- кол-во ед хр требующих замены обложки
DECLARE @E_POVR bit = 1					-- кол-во неисправимо поврежденных ед хр
DECLARE @E_ZAT bit = 1					-- кол-во листов с затухающими текстами
DECLARE @E_RESTAVR bit = 1				-- кол-во ед хр требующих реставрации
DECLARE @E_GOR bit = 1					-- кол-во ед хр на горючей основе
DECLARE @E_PODSH bit = 1				-- кол-во ед хр требующих подшифки или переплета
DECLARE @E_KPO bit = 1					-- кол-во ед хр требующих КПО
DECLARE @E_DEZINF bit = 1				-- кол-во ед хр требующих дезинфекции
DECLARE @E_DEZINS bit = 1				-- кол-во ед хр требующих дезинсекции

DECLARE @HRAN_FOND bit = 1				-- топография

DECLARE @SUM_BO bit = 1					-- сум кол чар-ки документов на бумажной основе
DECLARE @SUM_TN bit = 1					-- сум кол хар-ки аудиовизуальных документов на трад носителях
DECLARE @SUM_EN bit = 1					-- сум кол хар-ки докуметов на электронных носителях
DECLARE @SUM_AA bit = 1					-- сум кол хар-ки всех типов документов на всех носителях
*/
-- замена параметров для тестирования запроса

if (@VVEDENO_O = 1)			-- +Введено описей
	update R set R.AUTO_INVENTORY_COUNT = (
		select count(c.NUM)
		from (
			select distinct ti.INVENTORY_NUM_1 + isnull(ti.INVENTORY_NUM_2,'') NUM
			from tblINVENTORY ti where ti.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		) c
	)
	from tblFUND R
	left join tblINVENTORY I on R.ISN_FUND = I.ISN_FUND
    where /*where*/ and I.ID is not null and R.Deleted = 0

if (@HAS_MUSEUM = 1)		-- +Наличие музейных предметов
	update R set R.HAS_MUSEUM_ITEMS = (
		case when (
			select count(ti.ID) from tblINVENTORY ti
			where ti.ISN_FUND = R.ISN_FUND and ti.MUSEUM_UNITS_COUNT > '0' and ti.Deleted = 0
		) > 0 then 'Y' else 'N' end
	)
	from tblFUND R
	left join tblINVENTORY I on R.ISN_FUND = I.ISN_FUND
    where /*where*/ and I.ID is not null and I.Deleted = 0 and R.Deleted = 0

if (@SROK_HRAN = 1)			-- +Срок хранения
begin
    declare @a int
    declare @b int
    declare @c int
    declare @d int
    set @a = (select COUNT(T.ID) from (
		select * from tblINVENTORY R where /*where*/ and R.Deleted = 0) T where T.INVENTORY_KEEP_PERIOD = 'a')
    set @b = (select COUNT(T.ID) from (
		select * from tblINVENTORY R where /*where*/ and R.Deleted = 0) T where T.INVENTORY_KEEP_PERIOD = 'b')
    set @c = (select COUNT(T.ID) from (
		select * from tblINVENTORY R where /*where*/ and R.Deleted = 0) T where T.INVENTORY_KEEP_PERIOD = 'c')
    set @d = (select COUNT(T.ID) from (
		select * from tblINVENTORY R where /*where*/ and R.Deleted = 0) T where T.INVENTORY_KEEP_PERIOD = 'd')
    if (@a > 0 and @b = 0 and @c = 0)
		update R set R.KEEP_PERIOD = 'a'
		from tblFUND R
		left join tblINVENTORY I on R.ISN_FUND = I.ISN_FUND
		where /*where*/ and I.INVENTORY_KEEP_PERIOD is not null and I.Deleted = 0 and R.Deleted = 0
    if (@b > 0 and @a = 0 and @c = 0)
		update R set R.KEEP_PERIOD = 'b'
		from tblFUND R
		left join tblINVENTORY I on R.ISN_FUND = I.ISN_FUND
		where /*where*/ and I.INVENTORY_KEEP_PERIOD is not null and I.Deleted = 0 and R.Deleted = 0
    if ((@a > 0 and @b > 0) or @c > 0)
		update R set R.KEEP_PERIOD = 'c'
		from tblFUND R
		left join tblINVENTORY I on R.ISN_FUND = I.ISN_FUND
		where /*where*/ and I.INVENTORY_KEEP_PERIOD is not null and I.Deleted = 0 and R.Deleted = 0
    if (@a = 0 and @b = 0 and @c = 0 and @d > 0)
		update R set R.KEEP_PERIOD = 'd'
		from tblFUND R
		left join tblINVENTORY I on R.ISN_FUND = I.ISN_FUND
		where /*where*/ and I.INVENTORY_KEEP_PERIOD is not null and I.Deleted = 0 and R.Deleted = 0
end

if (@NACH_DATA = 1)			-- +Начальная дата документов
    update R set R.DOC_START_YEAR = (
		select min(ti.DOC_START_YEAR) from tblINVENTORY ti
		where ti.ISN_FUND = R.ISN_FUND and ti.DOC_START_YEAR is not null and ti.Deleted = 0
	)
	from tblFUND R
	left join tblINVENTORY I on R.ISN_FUND = I.ISN_FUND
    where /*where*/ and I.DOC_START_YEAR is not null and I.Deleted = 0 and R.Deleted = 0

if (@KON_DATA = 1)			-- +Конечная дата документов
    update R set R.DOC_END_YEAR = (
		select max(ti.DOC_END_YEAR) from tblINVENTORY ti
		where ti.ISN_FUND = R.ISN_FUND and ti.DOC_END_YEAR is not null and ti.Deleted = 0
	)
	from tblFUND R
	left join tblINVENTORY I on R.ISN_FUND = I.ISN_FUND
    where /*where*/ and I.DOC_END_YEAR is not null and I.Deleted = 0 and R.Deleted = 0

if (@TREASURE_COUNT = 1)	-- +Кол-во ед.хр. с драг. камнями и металлами в оформлении
	update R set R.TREASURE_UNITS_COUNT = (
		select coalesce(sum(c.UNITS_WITH_TREASURES_COUNT), 0)
		from (
			select distinct ti.INVENTORY_NUM_1 + isnull(ti.INVENTORY_NUM_2,'') NUM, ti.UNITS_WITH_TREASURES_COUNT
			from tblINVENTORY ti where ti.ISN_FUND = R.ISN_FUND and ti.Deleted = 0
		) c
	)
	from tblFUND R
	left join tblINVENTORY I on R.ISN_FUND = I.ISN_FUND
    where /*where*/ and I.ID is not null and I.Deleted = 0 and R.Deleted = 0

if (@ERH_ALL = 1)			-- +Всего единиц хранения
begin
    update R set R.UNIT_COUNT = (
		select coalesce(sum(td.UNIT_COUNT), 0) + coalesce(sum(td.UNIT_UNDESCRIBED), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 1 and td.CARRIER_TYPE = 'P' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 1 and R.CARRIER_TYPE = 'P' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_COUNT = (
		select coalesce(sum(td.UNIT_COUNT), 0) + coalesce(sum(td.UNIT_UNDESCRIBED), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 2 and td.CARRIER_TYPE = 'P' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 2 and R.CARRIER_TYPE = 'P' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_COUNT = (
		select coalesce(sum(td.UNIT_COUNT), 0) + coalesce(sum(td.UNIT_UNDESCRIBED), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 3 and td.CARRIER_TYPE = 'P' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 3 and R.CARRIER_TYPE = 'P' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_COUNT = (
		select coalesce(sum(td.UNIT_COUNT), 0) + coalesce(sum(td.UNIT_UNDESCRIBED), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 4 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_COUNT = (
		select coalesce(sum(td.UNIT_COUNT), 0) + coalesce(sum(td.UNIT_UNDESCRIBED), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 4 and td.CARRIER_TYPE = 'P' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'P' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_COUNT = (
		select coalesce(sum(td.UNIT_COUNT), 0) + coalesce(sum(td.UNIT_UNDESCRIBED), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 5 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_COUNT = (
		select coalesce(sum(td.UNIT_COUNT), 0) + coalesce(sum(td.UNIT_UNDESCRIBED), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 5 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_COUNT = (
		select coalesce(sum(td.UNIT_COUNT), 0) + coalesce(sum(td.UNIT_UNDESCRIBED), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 6 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_COUNT = (
		select coalesce(sum(td.UNIT_COUNT), 0) + coalesce(sum(td.UNIT_UNDESCRIBED), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 6 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_COUNT = (
		select coalesce(sum(td.UNIT_COUNT), 0) + coalesce(sum(td.UNIT_UNDESCRIBED), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 7 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_COUNT = (
		select coalesce(sum(td.UNIT_COUNT), 0) + coalesce(sum(td.UNIT_UNDESCRIBED), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 7 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_COUNT = (
		select coalesce(sum(td.UNIT_COUNT), 0) + coalesce(sum(td.UNIT_UNDESCRIBED), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 8 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_COUNT = (
		select coalesce(sum(td.UNIT_COUNT), 0) + coalesce(sum(td.UNIT_UNDESCRIBED), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 8 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_COUNT = (
		select coalesce(sum(td.UNIT_COUNT), 0) + coalesce(sum(td.UNIT_UNDESCRIBED), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 9 and td.CARRIER_TYPE = 'M' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and  R.ISN_DOC_TYPE = 9 and R.CARRIER_TYPE = 'M' and R.ISN_INVENTORY is null and F.Deleted = 0
end

if (@EU_ALL = 1)			-- +Всего единиц учета
begin
    update R set R.REG_UNIT = (
		select coalesce(sum(td.REG_UNIT), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 4 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.REG_UNIT = (
		select coalesce(sum(td.REG_UNIT), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 5 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.REG_UNIT = (
		select coalesce(sum(td.REG_UNIT), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 6 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.REG_UNIT = (
		select coalesce(sum(td.REG_UNIT), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 6 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.REG_UNIT = (
		select coalesce(sum(td.REG_UNIT), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 7 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.REG_UNIT = (
		select coalesce(sum(td.REG_UNIT), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 7 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.REG_UNIT = (
		select coalesce(sum(td.REG_UNIT), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 8 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and  F.Deleted = 0
    
    update R set R.REG_UNIT = (
		select coalesce(sum(td.REG_UNIT), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 8 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and  F.Deleted = 0
end

if (@ERHO_ALL = 1)				-- +Всего единиц хранения по описям	
begin
    update R set R.UNIT_INVENTORY = (
		select coalesce(sum(td.UNIT_COUNT), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 1 and td.CARRIER_TYPE = 'P' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 1 and R.CARRIER_TYPE = 'P' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.UNIT_INVENTORY = (
		select coalesce(sum(td.UNIT_COUNT), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 2 and td.CARRIER_TYPE = 'P' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 2 and R.CARRIER_TYPE = 'P' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.UNIT_INVENTORY = (
		select coalesce(sum(td.UNIT_COUNT), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 3 and td.CARRIER_TYPE = 'P' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 3 and R.CARRIER_TYPE = 'P' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.UNIT_INVENTORY = (
		select coalesce(sum(td.UNIT_COUNT), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 4 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.UNIT_INVENTORY = (
		select coalesce(sum(td.UNIT_COUNT), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 4 and td.CARRIER_TYPE = 'P' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'P' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.UNIT_INVENTORY = (
		select coalesce(sum(td.UNIT_COUNT), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 5 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.UNIT_INVENTORY = (
		select coalesce(sum(td.UNIT_COUNT), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 5 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.UNIT_INVENTORY = (
		select coalesce(sum(td.UNIT_COUNT), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 6 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.UNIT_INVENTORY = (
		select coalesce(sum(td.UNIT_COUNT), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 6 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.UNIT_INVENTORY = (
		select coalesce(sum(td.UNIT_COUNT), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 7 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.UNIT_INVENTORY = (
		select coalesce(sum(td.UNIT_COUNT), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 7 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.UNIT_INVENTORY = (
		select coalesce(sum(td.UNIT_COUNT), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 8 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and  F.Deleted = 0
    
    update R set R.UNIT_INVENTORY = (
		select coalesce(sum(td.UNIT_COUNT), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 8 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.UNIT_INVENTORY = (
		select coalesce(sum(td.UNIT_COUNT), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 9 and td.CARRIER_TYPE = 'M' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 9 and R.CARRIER_TYPE = 'M' and R.ISN_INVENTORY is null and  F.Deleted = 0
end

if (@EUO_ALL = 1)			-- +Всего единиц учета по описям
begin
    update R set R.REG_UNIT_INVENTORY = (
		select coalesce(sum(td.REG_UNIT), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 4 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.REG_UNIT_INVENTORY = (
		select coalesce(sum(td.REG_UNIT), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 5 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.REG_UNIT_INVENTORY = (
		select coalesce(sum(td.REG_UNIT), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 6 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.REG_UNIT_INVENTORY = (
		select coalesce(sum(td.REG_UNIT), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 6 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.REG_UNIT_INVENTORY = (
		select coalesce(sum(td.REG_UNIT), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 7 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.REG_UNIT_INVENTORY = (
		select coalesce(sum(td.REG_UNIT), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 7 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.REG_UNIT_INVENTORY = (
		select coalesce(sum(td.REG_UNIT), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 8 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and  F.Deleted = 0
    
    update R set R.REG_UNIT_INVENTORY = (
		select coalesce(sum(td.REG_UNIT), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 8 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and  F.Deleted = 0
end

if (@NEOP_EHR = 1)			-- +Количество неописанных единиц хранения, документов, листов
begin
    update R set R.UNIT_UNDESCRIBED = (
		select coalesce(sum(ty.UNIT_COUNT - ty.PROCESSED_COUNT), 0)
		from tblDEPOSIT_DOC_TYPE ty join tblDEPOSIT tp on ty.ISN_DEPOSIT = tp.ISN_DEPOSIT
		where tp.ISN_FUND = R.ISN_FUND and tp.Deleted = 0
		and ty.ISN_DOC_TYPE = 1 and ty.CARRIER_TYPE = 'P'
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 1 and R.CARRIER_TYPE = 'P' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_UNDESCRIBED = (
		select coalesce(sum(ty.UNIT_COUNT - ty.PROCESSED_COUNT), 0)
		from tblDEPOSIT_DOC_TYPE ty join tblDEPOSIT tp on ty.ISN_DEPOSIT = tp.ISN_DEPOSIT
		where tp.ISN_FUND = R.ISN_FUND and tp.Deleted = 0
		and ty.ISN_DOC_TYPE = 2 and ty.CARRIER_TYPE = 'P'
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 2 and R.CARRIER_TYPE = 'P' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_UNDESCRIBED = (
		select coalesce(sum(ty.UNIT_COUNT - ty.PROCESSED_COUNT), 0)
		from tblDEPOSIT_DOC_TYPE ty join tblDEPOSIT tp on ty.ISN_DEPOSIT = tp.ISN_DEPOSIT
		where tp.ISN_FUND = R.ISN_FUND and tp.Deleted = 0
		and ty.ISN_DOC_TYPE = 3 and ty.CARRIER_TYPE = 'P'
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 3 and R.CARRIER_TYPE = 'P' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_UNDESCRIBED = (
		select coalesce(sum(ty.UNIT_COUNT - ty.PROCESSED_COUNT), 0)
		from tblDEPOSIT_DOC_TYPE ty join tblDEPOSIT tp on ty.ISN_DEPOSIT = tp.ISN_DEPOSIT
		where tp.ISN_FUND = R.ISN_FUND and tp.Deleted = 0
		and ty.ISN_DOC_TYPE = 4 and ty.CARRIER_TYPE = 'E'
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_UNDESCRIBED = (
		select coalesce(sum(ty.UNIT_COUNT - ty.PROCESSED_COUNT), 0)
		from tblDEPOSIT_DOC_TYPE ty join tblDEPOSIT tp on ty.ISN_DEPOSIT = tp.ISN_DEPOSIT
		where tp.ISN_FUND = R.ISN_FUND and tp.Deleted = 0
		and ty.ISN_DOC_TYPE = 4 and ty.CARRIER_TYPE = 'P'
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'P' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_UNDESCRIBED = (
		select coalesce(sum(ty.UNIT_COUNT - ty.PROCESSED_COUNT), 0)
		from tblDEPOSIT_DOC_TYPE ty join tblDEPOSIT tp on ty.ISN_DEPOSIT = tp.ISN_DEPOSIT
		where tp.ISN_FUND = R.ISN_FUND and tp.Deleted = 0
		and ty.ISN_DOC_TYPE = 5 and ty.CARRIER_TYPE = 'E'
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_UNDESCRIBED = (
		select coalesce(sum(ty.UNIT_COUNT - ty.PROCESSED_COUNT), 0)
		from tblDEPOSIT_DOC_TYPE ty join tblDEPOSIT tp on ty.ISN_DEPOSIT = tp.ISN_DEPOSIT
		where tp.ISN_FUND = R.ISN_FUND and tp.Deleted = 0
		and ty.ISN_DOC_TYPE = 5 and ty.CARRIER_TYPE = 'A'
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_UNDESCRIBED = (
		select coalesce(sum(ty.UNIT_COUNT - ty.PROCESSED_COUNT), 0)
		from tblDEPOSIT_DOC_TYPE ty join tblDEPOSIT tp on ty.ISN_DEPOSIT = tp.ISN_DEPOSIT
		where tp.ISN_FUND = R.ISN_FUND and tp.Deleted = 0
		and ty.ISN_DOC_TYPE = 6 and ty.CARRIER_TYPE = 'E'
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_UNDESCRIBED = (
		select coalesce(sum(ty.UNIT_COUNT - ty.PROCESSED_COUNT), 0)
		from tblDEPOSIT_DOC_TYPE ty join tblDEPOSIT tp on ty.ISN_DEPOSIT = tp.ISN_DEPOSIT
		where tp.ISN_FUND = R.ISN_FUND and tp.Deleted = 0
		and ty.ISN_DOC_TYPE = 6 and ty.CARRIER_TYPE = 'A'
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_UNDESCRIBED = (
		select coalesce(sum(ty.UNIT_COUNT - ty.PROCESSED_COUNT), 0)
		from tblDEPOSIT_DOC_TYPE ty join tblDEPOSIT tp on ty.ISN_DEPOSIT = tp.ISN_DEPOSIT
		where tp.ISN_FUND = R.ISN_FUND and tp.Deleted = 0
		and ty.ISN_DOC_TYPE = 7 and ty.CARRIER_TYPE = 'E'
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_UNDESCRIBED = (
		select coalesce(sum(ty.UNIT_COUNT - ty.PROCESSED_COUNT), 0)
		from tblDEPOSIT_DOC_TYPE ty join tblDEPOSIT tp on ty.ISN_DEPOSIT = tp.ISN_DEPOSIT
		where tp.ISN_FUND = R.ISN_FUND and tp.Deleted = 0
		and ty.ISN_DOC_TYPE = 7 and ty.CARRIER_TYPE = 'A'
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_UNDESCRIBED = (
		select coalesce(sum(ty.UNIT_COUNT - ty.PROCESSED_COUNT), 0)
		from tblDEPOSIT_DOC_TYPE ty join tblDEPOSIT tp on ty.ISN_DEPOSIT = tp.ISN_DEPOSIT
		where tp.ISN_FUND = R.ISN_FUND and tp.Deleted = 0
		and ty.ISN_DOC_TYPE = 8 and ty.CARRIER_TYPE = 'E'
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_UNDESCRIBED = (
		select coalesce(sum(ty.UNIT_COUNT - ty.PROCESSED_COUNT), 0)
		from tblDEPOSIT_DOC_TYPE ty join tblDEPOSIT tp on ty.ISN_DEPOSIT = tp.ISN_DEPOSIT
		where tp.ISN_FUND = R.ISN_FUND and tp.Deleted = 0
		and ty.ISN_DOC_TYPE = 8 and ty.CARRIER_TYPE = 'A'
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_UNDESCRIBED = (
		select coalesce(sum(ty.UNIT_COUNT - ty.PROCESSED_COUNT), 0)
		from tblDEPOSIT_DOC_TYPE ty join tblDEPOSIT tp on ty.ISN_DEPOSIT = tp.ISN_DEPOSIT
		where tp.ISN_FUND = R.ISN_FUND and tp.Deleted = 0
		and ty.ISN_DOC_TYPE = 9 and ty.CARRIER_TYPE = 'M'
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 9 and R.CARRIER_TYPE = 'M' and R.ISN_INVENTORY is null and F.Deleted = 0

	update R set
	R.UNDESCRIBED_DOC_COUNT = (
		select coalesce(sum(tp.DOC_COUNT),0) - coalesce(sum(tp.PROCESSED_DOC),0)
		from tblDEPOSIT tp
		where tp.ISN_FUND = R.ISN_FUND and tp.Deleted = 0),
	R.PERSONAL_UNDESCRIBED_DOC_COUNT = (
		select coalesce(sum(tp.PERSONAL_DOC_COUNT),0) - coalesce(sum(tp.PROCESSED_PERSONAL_DOC),0)
		from tblDEPOSIT tp
		where tp.ISN_FUND = R.ISN_FUND and tp.Deleted = 0),
	R.UNDECSRIBED_PAGE_COUNT = (
		select coalesce(sum(tp.PAGE_COUNT),0) - coalesce(sum(tp.PROCESSED_PAGE),0)
		from tblDEPOSIT tp
		where tp.ISN_FUND = R.ISN_FUND and tp.Deleted = 0)
	from tblFUND R
	where /*where*/ and R.Deleted = 0
end

if (@EHR_REG = 1)			-- +Количество зарегистрированных единиц хранения
begin
    update R set R.UNIT_REGISTERED = (
		select coalesce(sum(td.UNIT_REGISTERED), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 1 and td.CARRIER_TYPE = 'P' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 1 and R.CARRIER_TYPE = 'P' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_REGISTERED = (
		select coalesce(sum(td.UNIT_REGISTERED), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 2 and td.CARRIER_TYPE = 'P' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 2 and R.CARRIER_TYPE = 'P' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_REGISTERED = (
		select coalesce(sum(td.UNIT_REGISTERED), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 3 and td.CARRIER_TYPE = 'P' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 3 and R.CARRIER_TYPE = 'P' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_REGISTERED = (
		select coalesce(sum(td.UNIT_REGISTERED), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 4 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_REGISTERED = (
		select coalesce(sum(td.UNIT_REGISTERED), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 4 and td.CARRIER_TYPE = 'P' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'P' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_REGISTERED = (
		select coalesce(sum(td.UNIT_REGISTERED), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 5 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_REGISTERED = (
		select coalesce(sum(td.UNIT_REGISTERED), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 5 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_REGISTERED = (
		select coalesce(sum(td.UNIT_REGISTERED), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 6 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_REGISTERED = (
		select coalesce(sum(td.UNIT_REGISTERED), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 6 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_REGISTERED = (
		select coalesce(sum(td.UNIT_REGISTERED), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 7 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_REGISTERED = (
		select coalesce(sum(td.UNIT_REGISTERED), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 7 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_REGISTERED = (
		select coalesce(sum(td.UNIT_REGISTERED), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 8 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_REGISTERED = (
		select coalesce(sum(td.UNIT_REGISTERED), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 8 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_REGISTERED = (
		select coalesce(sum(td.UNIT_REGISTERED), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 9 and td.CARRIER_TYPE = 'M' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 9 and R.CARRIER_TYPE = 'M' and R.ISN_INVENTORY is null and F.Deleted = 0
end

if (@EU_REG = 1)			-- +Количество зарегистрированных единиц учета
begin
    update R set R.REG_UNIT_REGISTERED = (
		select coalesce(sum(td.REG_UNIT_REGISTERED), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 4 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.REG_UNIT_REGISTERED = (
		select coalesce(sum(td.REG_UNIT_REGISTERED), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 5 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.REG_UNIT_REGISTERED = (
		select coalesce(sum(td.REG_UNIT_REGISTERED), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 6 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.REG_UNIT_REGISTERED = (
		select coalesce(sum(td.REG_UNIT_REGISTERED), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 6 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.REG_UNIT_REGISTERED = (
		select coalesce(sum(td.REG_UNIT_REGISTERED), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 7 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.REG_UNIT_REGISTERED = (
		select coalesce(sum(td.REG_UNIT_REGISTERED), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 7 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.REG_UNIT_REGISTERED = (
		select coalesce(sum(td.REG_UNIT_REGISTERED), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 8 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.REG_UNIT_REGISTERED = (
		select coalesce(sum(td.REG_UNIT_REGISTERED), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 8 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and  F.Deleted = 0
end

if (@ERH_OC = 1)			-- +Количество особо ценных единиц хранения
begin
    update R set R.UNIT_OC_COUNT = (
		select coalesce(sum(td.UNIT_OC_COUNT), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_DOC_TYPE = 1 and td.CARRIER_TYPE = 'P' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 1 and R.CARRIER_TYPE = 'P' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_OC_COUNT = (
		select coalesce(sum(td.UNIT_OC_COUNT), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_DOC_TYPE = 3 and td.CARRIER_TYPE = 'P' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 3 and R.CARRIER_TYPE = 'P' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_OC_COUNT = (
		select coalesce(sum(td.UNIT_OC_COUNT), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_DOC_TYPE = 4 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_OC_COUNT = (
		select coalesce(sum(td.UNIT_OC_COUNT), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_DOC_TYPE = 4 and td.CARRIER_TYPE = 'P' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'P' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_OC_COUNT = (
		select coalesce(sum(td.UNIT_OC_COUNT), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_DOC_TYPE = 5 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_OC_COUNT = (
		select coalesce(sum(td.UNIT_OC_COUNT), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_DOC_TYPE = 5 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_OC_COUNT = (
		select coalesce(sum(td.UNIT_OC_COUNT), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_DOC_TYPE = 6 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_OC_COUNT = (
		select coalesce(sum(td.UNIT_OC_COUNT), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_DOC_TYPE = 6 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_OC_COUNT = (
		select coalesce(sum(td.UNIT_OC_COUNT), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_DOC_TYPE = 7 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_OC_COUNT = (
		select coalesce(sum(td.UNIT_OC_COUNT), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_DOC_TYPE = 7 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_OC_COUNT = (
		select coalesce(sum(td.UNIT_OC_COUNT), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_DOC_TYPE = 8 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_OC_COUNT = (
		select coalesce(sum(td.UNIT_OC_COUNT), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_DOC_TYPE = 8 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_OC_COUNT = (
		select coalesce(sum(td.UNIT_OC_COUNT), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_DOC_TYPE = 9 and td.CARRIER_TYPE = 'M' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 9 and R.CARRIER_TYPE = 'M' and R.ISN_INVENTORY is null and F.Deleted = 0
end

if (@EU_OC = 1)				-- +Количество особо ценных единиц учета
begin
    update R set R.REG_UNIT_OC = (
		select coalesce(sum(td.REG_UNIT_OC), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND
		and td.ISN_DOC_TYPE = 4 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.REG_UNIT_OC = (
		select coalesce(sum(td.REG_UNIT_OC), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND
		and td.ISN_DOC_TYPE = 5 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.REG_UNIT_OC = (
		select coalesce(sum(td.REG_UNIT_OC), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND
		and td.ISN_DOC_TYPE = 6 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.REG_UNIT_OC = (
		select coalesce(sum(td.REG_UNIT_OC), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND
		and td.ISN_DOC_TYPE = 6 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.REG_UNIT_OC = (
		select coalesce(sum(td.REG_UNIT_OC), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND
		and td.ISN_DOC_TYPE = 7 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.REG_UNIT_OC = (
		select coalesce(sum(td.REG_UNIT_OC), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND
		and td.ISN_DOC_TYPE = 7 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.REG_UNIT_OC = (
		select coalesce(sum(td.REG_UNIT_OC), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND
		and td.ISN_DOC_TYPE = 8 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.REG_UNIT_OC = (
		select coalesce(sum(td.REG_UNIT_OC), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND
		and td.ISN_DOC_TYPE = 8 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and F.Deleted = 0
end

if (@ERH_U = 1)				-- +Количество уникальных единиц хранения
begin
    update R set R.UNITS_UNIQUE = (
		select coalesce(sum(td.UNITS_UNIQUE), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_DOC_TYPE = 1 and td.CARRIER_TYPE = 'P' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 1 and R.CARRIER_TYPE = 'P' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNITS_UNIQUE = (
		select coalesce(sum(td.UNITS_UNIQUE), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_DOC_TYPE = 3 and td.CARRIER_TYPE = 'P' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 3 and R.CARRIER_TYPE = 'P' and R.ISN_INVENTORY is null and F.Deleted = 0

     update R set R.UNITS_UNIQUE = (
		select coalesce(sum(td.UNITS_UNIQUE), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_DOC_TYPE = 4 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNITS_UNIQUE = (
		select coalesce(sum(td.UNITS_UNIQUE), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_DOC_TYPE = 4 and td.CARRIER_TYPE = 'P' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'P' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNITS_UNIQUE = (
		select coalesce(sum(td.UNITS_UNIQUE), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_DOC_TYPE = 5 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNITS_UNIQUE = (
		select coalesce(sum(td.UNITS_UNIQUE), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_DOC_TYPE = 5 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNITS_UNIQUE = (
		select coalesce(sum(td.UNITS_UNIQUE), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND
		and td.ISN_DOC_TYPE = 6 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNITS_UNIQUE = (
		select coalesce(sum(td.UNITS_UNIQUE), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_DOC_TYPE = 6 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNITS_UNIQUE = (
		select coalesce(sum(td.UNITS_UNIQUE), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_DOC_TYPE = 7 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNITS_UNIQUE = (
		select coalesce(sum(td.UNITS_UNIQUE), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_DOC_TYPE = 7 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNITS_UNIQUE = (
		select coalesce(sum(td.UNITS_UNIQUE), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_DOC_TYPE = 8 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNITS_UNIQUE = (
		select coalesce(sum(td.UNITS_UNIQUE), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_DOC_TYPE = 8 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNITS_UNIQUE = (
		select coalesce(sum(td.UNITS_UNIQUE), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_DOC_TYPE = 9 and td.CARRIER_TYPE = 'M' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 9 and R.CARRIER_TYPE = 'M' and R.ISN_INVENTORY is null and F.Deleted = 0
end

if (@EU_U = 1)				-- +Количество уникальных единиц учета
begin
    update R set R.REG_UNITS_UNIQUE = (
		select coalesce(sum(td.REG_UNITS_UNIQUE), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_DOC_TYPE = 4 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.REG_UNITS_UNIQUE = (
		select coalesce(sum(td.REG_UNITS_UNIQUE), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_DOC_TYPE = 5 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.REG_UNITS_UNIQUE = (
		select coalesce(sum(td.REG_UNITS_UNIQUE), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_DOC_TYPE = 6 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.REG_UNITS_UNIQUE = (
		select coalesce(sum(td.REG_UNITS_UNIQUE), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_DOC_TYPE = 6 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.REG_UNITS_UNIQUE = (
		select coalesce(sum(td.REG_UNITS_UNIQUE), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_DOC_TYPE = 7 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.REG_UNITS_UNIQUE = (
		select coalesce(sum(td.REG_UNITS_UNIQUE), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_DOC_TYPE = 7 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.REG_UNITS_UNIQUE = (
		select coalesce(sum(td.REG_UNITS_UNIQUE), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_DOC_TYPE = 8 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.REG_UNITS_UNIQUE = (
		select coalesce(sum(td.REG_UNITS_UNIQUE), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
      and td.ISN_DOC_TYPE = 8 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and F.Deleted = 0
end

if (@EHR_SF = 1)			-- +Количество единиц хранения, имеющих СФ
begin
    update R set R.UNIT_HAS_SF = (
		select coalesce(sum(td.UNIT_HAS_SF), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 1 and td.CARRIER_TYPE = 'P' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 1 and R.CARRIER_TYPE = 'P' and R.ISN_INVENTORY is null and F.Deleted = 0

	/*
    update R set R.UNIT_HAS_SF = (
		select coalesce(sum(td.UNIT_HAS_SF), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 2 and td.CARRIER_TYPE = 'P' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 2 and R.CARRIER_TYPE = 'P' and R.ISN_INVENTORY is null and F.Deleted = 0
	*/

    update R set R.UNIT_HAS_SF = (
		select coalesce(sum(td.UNIT_HAS_SF), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 3 and td.CARRIER_TYPE = 'P' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 3 and R.CARRIER_TYPE = 'P' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_HAS_SF = (
		select coalesce(sum(td.UNIT_HAS_SF), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 4 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_HAS_SF = (
		select coalesce(sum(td.UNIT_HAS_SF), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 4 and td.CARRIER_TYPE = 'P' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'P' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_HAS_SF = (
		select coalesce(sum(td.UNIT_HAS_SF), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 5 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_HAS_SF = (
		select coalesce(sum(td.UNIT_HAS_SF), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 5 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_HAS_SF = (
		select coalesce(sum(td.UNIT_HAS_SF), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 6 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_HAS_SF = (
		select coalesce(sum(td.UNIT_HAS_SF), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 6 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_HAS_SF = (
		select coalesce(sum(td.UNIT_HAS_SF), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 7 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_HAS_SF = (
		select coalesce(sum(td.UNIT_HAS_SF), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 7 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and F.Deleted = 0

     update R set R.UNIT_HAS_SF = (
		select coalesce(sum(td.UNIT_HAS_SF), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 8 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_HAS_SF = (
		select coalesce(sum(td.UNIT_HAS_SF), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 8 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.UNIT_HAS_SF = (
		select coalesce(sum(td.UNIT_HAS_SF), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 9 and td.CARRIER_TYPE = 'M' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 9 and R.CARRIER_TYPE = 'M' and R.ISN_INVENTORY is null and F.Deleted = 0
end

if (@EU_SF = 1)				-- +Количество единиц учета, имеющих СФ
begin
    update R set R.REG_UNIT_HAS_SF = (
		select coalesce(sum(td.REG_UNIT_HAS_SF), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 4 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.REG_UNIT_HAS_SF = (
		select coalesce(sum(td.REG_UNIT_HAS_SF), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 5 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.REG_UNIT_HAS_SF = (
		select coalesce(sum(td.REG_UNIT_HAS_SF), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 6 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.REG_UNIT_HAS_SF = (
		select coalesce(sum(td.REG_UNIT_HAS_SF), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 6 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.REG_UNIT_HAS_SF = (
		select coalesce(sum(td.REG_UNIT_HAS_SF), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 7 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.REG_UNIT_HAS_SF = (
		select coalesce(sum(td.REG_UNIT_HAS_SF), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 7 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.REG_UNIT_HAS_SF = (
		select coalesce(sum(td.REG_UNIT_HAS_SF), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 8 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.REG_UNIT_HAS_SF = (
		select coalesce(sum(td.REG_UNIT_HAS_SF), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 8 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and  F.Deleted = 0
end

if (@EHR_FP = 1)			-- +Количество единиц хранения, имеющих ФП
begin
    update R set R.UNIT_HAS_FP = (
		select coalesce(sum(td.UNIT_HAS_FP), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 1 and td.CARRIER_TYPE = 'P' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 1 and R.CARRIER_TYPE = 'P' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.UNIT_HAS_FP = (
		select coalesce(sum(td.UNIT_HAS_FP), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 2 and td.CARRIER_TYPE = 'P' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 2 and R.CARRIER_TYPE = 'P' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.UNIT_HAS_FP = (
		select coalesce(sum(td.UNIT_HAS_FP), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 3 and td.CARRIER_TYPE = 'P' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 3 and R.CARRIER_TYPE = 'P' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.UNIT_HAS_FP = (
		select coalesce(sum(td.UNIT_HAS_FP), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 4 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.UNIT_HAS_FP = (
		select coalesce(sum(td.UNIT_HAS_FP), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 4 and td.CARRIER_TYPE = 'P' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'P' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.UNIT_HAS_FP = (
		select coalesce(sum(td.UNIT_HAS_FP), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 5 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.UNIT_HAS_FP = (
		select coalesce(sum(td.UNIT_HAS_FP), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 5 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.UNIT_HAS_FP = (
		select coalesce(sum(td.UNIT_HAS_FP), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 6 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.UNIT_HAS_FP = (
		select coalesce(sum(td.UNIT_HAS_FP), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 6 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.UNIT_HAS_FP = (
		select coalesce(sum(td.UNIT_HAS_FP), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 7 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.UNIT_HAS_FP = (
		select coalesce(sum(td.UNIT_HAS_FP), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 7 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.UNIT_HAS_FP = (
		select coalesce(sum(td.UNIT_HAS_FP), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 8 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.UNIT_HAS_FP = (
		select coalesce(sum(td.UNIT_HAS_FP), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 8 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.UNIT_HAS_FP = (
		select coalesce(sum(td.UNIT_HAS_FP), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 9 and td.CARRIER_TYPE = 'M' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 9 and R.CARRIER_TYPE = 'M' and R.ISN_INVENTORY is null and  F.Deleted = 0
end

if (@EU_FP = 1)				-- +Количество единиц учета, имеющих ФП
begin
    update R set R.REG_UNIT_HAS_FP = (
		select coalesce(sum(td.REG_UNIT_HAS_FP), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 4 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.REG_UNIT_HAS_FP = (
		select coalesce(sum(td.REG_UNIT_HAS_FP), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 5 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.REG_UNIT_HAS_FP = (
		select coalesce(sum(td.REG_UNIT_HAS_FP), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 6 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.REG_UNIT_HAS_FP = (
		select coalesce(sum(td.REG_UNIT_HAS_FP), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 6 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.REG_UNIT_HAS_FP = (
		select coalesce(sum(td.REG_UNIT_HAS_FP), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 7 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.REG_UNIT_HAS_FP = (
		select coalesce(sum(td.REG_UNIT_HAS_FP), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 7 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.REG_UNIT_HAS_FP = (
		select coalesce(sum(td.REG_UNIT_HAS_FP), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 8 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.REG_UNIT_HAS_FP = (
		select coalesce(sum(td.REG_UNIT_HAS_FP), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 8 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and  F.Deleted = 0
end

if (@EHR_NEOB = 1)			-- +Количество необнаруженных единиц хранения
begin
    update R set R.UNITS_NOT_FOUND = (
		select coalesce(sum(td.UNITS_NOT_FOUND), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 1 and td.CARRIER_TYPE = 'P' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 1 and R.CARRIER_TYPE = 'P' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.UNITS_NOT_FOUND = (
		select coalesce(sum(td.UNITS_NOT_FOUND), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 2 and td.CARRIER_TYPE = 'P' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 2 and R.CARRIER_TYPE = 'P' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.UNITS_NOT_FOUND = (
		select coalesce(sum(td.UNITS_NOT_FOUND), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 3 and td.CARRIER_TYPE = 'P' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 3 and R.CARRIER_TYPE = 'P' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.UNITS_NOT_FOUND = (
		select coalesce(sum(td.UNITS_NOT_FOUND), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 4 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.UNITS_NOT_FOUND = (
		select coalesce(sum(td.UNITS_NOT_FOUND), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 4 and td.CARRIER_TYPE = 'P' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'P' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.UNITS_NOT_FOUND = (
		select coalesce(sum(td.UNITS_NOT_FOUND), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 5 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.UNITS_NOT_FOUND = (
		select coalesce(sum(td.UNITS_NOT_FOUND), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 5 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.UNITS_NOT_FOUND = (
		select coalesce(sum(td.UNITS_NOT_FOUND), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 6 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.UNITS_NOT_FOUND = (
		select coalesce(sum(td.UNITS_NOT_FOUND), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 6 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.UNITS_NOT_FOUND = (
		select coalesce(sum(td.UNITS_NOT_FOUND), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 7 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.UNITS_NOT_FOUND = (
		select coalesce(sum(td.UNITS_NOT_FOUND), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 7 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and  F.Deleted = 0

        update R set R.UNITS_NOT_FOUND = (
		select coalesce(sum(td.UNITS_NOT_FOUND), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 8 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.UNITS_NOT_FOUND = (
		select coalesce(sum(td.UNITS_NOT_FOUND), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 8 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.UNITS_NOT_FOUND = (
		select coalesce(sum(td.UNITS_NOT_FOUND), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 9 and td.CARRIER_TYPE = 'M' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 9 and R.CARRIER_TYPE = 'M' and R.ISN_INVENTORY is null and  F.Deleted = 0
end

if (@EU_NEOB = 1)			-- +Количество необнаруженных единиц учета
begin
    update R set R.REG_UNITS_NOT_FOUND = (
		select coalesce(sum(td.REG_UNITS_NOT_FOUND), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 4 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.REG_UNITS_NOT_FOUND = (
		select coalesce(sum(td.REG_UNITS_NOT_FOUND), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 5 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.REG_UNITS_NOT_FOUND = (
		select coalesce(sum(td.REG_UNITS_NOT_FOUND), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 6 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.REG_UNITS_NOT_FOUND = (
		select coalesce(sum(td.REG_UNITS_NOT_FOUND), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 6 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.REG_UNITS_NOT_FOUND = (
		select coalesce(sum(td.REG_UNITS_NOT_FOUND), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 7 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.REG_UNITS_NOT_FOUND = (
		select coalesce(sum(td.REG_UNITS_NOT_FOUND), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 7 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.REG_UNITS_NOT_FOUND = (
		select coalesce(sum(td.REG_UNITS_NOT_FOUND), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 8 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.REG_UNITS_NOT_FOUND = (
		select coalesce(sum(td.REG_UNITS_NOT_FOUND), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 8 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and F.Deleted = 0
end

if (@EHR_SEC = 1)			-- +Количество секретных единиц хранения
begin
    update R set R.SECRET_UNITS = (
		select coalesce(sum(td.SECRET_UNITS), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_DOC_TYPE = 1 and td.CARRIER_TYPE = 'P' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 1 and R.CARRIER_TYPE = 'P' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.SECRET_UNITS = (
		select coalesce(sum(td.SECRET_UNITS), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
      and td.ISN_DOC_TYPE = 2 and td.CARRIER_TYPE = 'P' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 2 and R.CARRIER_TYPE = 'P' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.SECRET_UNITS = (
		select coalesce(sum(td.SECRET_UNITS), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_DOC_TYPE = 4 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.SECRET_UNITS = (
		select coalesce(sum(td.SECRET_UNITS), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_DOC_TYPE = 4 and td.CARRIER_TYPE = 'P' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'P' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.SECRET_UNITS = (
		select coalesce(sum(td.SECRET_UNITS), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_DOC_TYPE = 5 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.SECRET_UNITS = (
		select coalesce(sum(td.SECRET_UNITS), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_DOC_TYPE = 5 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.SECRET_UNITS = (
		select coalesce(sum(td.SECRET_UNITS), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_DOC_TYPE = 6 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.SECRET_UNITS = (
		select coalesce(sum(td.SECRET_UNITS), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_DOC_TYPE = 6 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.SECRET_UNITS = (
		select coalesce(sum(td.SECRET_UNITS), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_DOC_TYPE = 7 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.SECRET_UNITS = (
		select coalesce(sum(td.SECRET_UNITS), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_DOC_TYPE = 7 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.SECRET_UNITS = (
		select coalesce(sum(td.SECRET_UNITS), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_DOC_TYPE = 8 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.SECRET_UNITS = (
		select coalesce(sum(td.SECRET_UNITS), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_DOC_TYPE = 8 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and F.Deleted = 0
end

if (@EU_SEC = 1)			-- +Количество секретных единиц учета
begin
    update R set R.SECRET_REG_UNITS = (
		select coalesce(sum(td.SECRET_REG_UNITS), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_DOC_TYPE = 4 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.SECRET_REG_UNITS = (
		select coalesce(sum(td.SECRET_REG_UNITS), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_DOC_TYPE = 5 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.SECRET_REG_UNITS = (
		select coalesce(sum(td.SECRET_REG_UNITS), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_DOC_TYPE = 6 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.SECRET_REG_UNITS = (
		select coalesce(sum(td.SECRET_REG_UNITS), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_DOC_TYPE = 6 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.SECRET_REG_UNITS = (
		select coalesce(sum(td.SECRET_REG_UNITS), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_DOC_TYPE = 7 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.SECRET_REG_UNITS = (
		select coalesce(sum(td.SECRET_REG_UNITS), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_DOC_TYPE = 7 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.SECRET_REG_UNITS = (
		select coalesce(sum(td.SECRET_REG_UNITS), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_DOC_TYPE = 8 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.SECRET_REG_UNITS = (
		select coalesce(sum(td.SECRET_REG_UNITS), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_DOC_TYPE = 8 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and F.Deleted = 0
end

if (@EHR_CAT = 1)			-- +Количество закаталогизированных единиц хранения
begin
    update R set R.UNITS_CATALOGUED = (
		select coalesce(sum(td.UNITS_CATALOGUED), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 1 and td.CARRIER_TYPE = 'P' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 1 and R.CARRIER_TYPE = 'P' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.UNITS_CATALOGUED = (
		select coalesce(sum(td.UNITS_CATALOGUED), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 2 and td.CARRIER_TYPE = 'P' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 2 and R.CARRIER_TYPE = 'P' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.UNITS_CATALOGUED = (
		select coalesce(sum(td.UNITS_CATALOGUED), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 3 and td.CARRIER_TYPE = 'P' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 3 and R.CARRIER_TYPE = 'P' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.UNITS_CATALOGUED = (
		select coalesce(sum(td.UNITS_CATALOGUED), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 4 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.UNITS_CATALOGUED = (
		select coalesce(sum(td.UNITS_CATALOGUED), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 4 and td.CARRIER_TYPE = 'P' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'P' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.UNITS_CATALOGUED = (
		select coalesce(sum(td.UNITS_CATALOGUED), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 5 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.UNITS_CATALOGUED = (
		select coalesce(sum(td.UNITS_CATALOGUED), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 5 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.UNITS_CATALOGUED = (
		select coalesce(sum(td.UNITS_CATALOGUED), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 6 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.UNITS_CATALOGUED = (
		select coalesce(sum(td.UNITS_CATALOGUED), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 7 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.UNITS_CATALOGUED = (
		select coalesce(sum(td.UNITS_CATALOGUED), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 8 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.UNITS_CATALOGUED = (
		select coalesce(sum(td.UNITS_CATALOGUED), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 8 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and  F.Deleted = 0

    update R set R.UNITS_CATALOGUED = (
		select coalesce(sum(td.UNITS_CATALOGUED), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 9 and td.CARRIER_TYPE = 'M' and td.ISN_INVENTORY is not null
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 9 and R.CARRIER_TYPE = 'M' and R.ISN_INVENTORY is null and  F.Deleted = 0
end

if (@EU_CAT = 1)			-- +Количество закаталогизированных единиц учета
begin
    update R set R.REG_UNITS_CTALOGUE = (
		select coalesce(sum(td.REG_UNITS_CTALOGUE), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 4 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.REG_UNITS_CTALOGUE = (
		select coalesce(sum(td.REG_UNITS_CTALOGUE), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 5 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.REG_UNITS_CTALOGUE = (
		select coalesce(sum(td.REG_UNITS_CTALOGUE), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 6 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.REG_UNITS_CTALOGUE = (
		select coalesce(sum(td.REG_UNITS_CTALOGUE), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 6 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.REG_UNITS_CTALOGUE = (
		select coalesce(sum(td.REG_UNITS_CTALOGUE), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 7 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.REG_UNITS_CTALOGUE = (
		select coalesce(sum(td.REG_UNITS_CTALOGUE), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 7 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.REG_UNITS_CTALOGUE = (
		select coalesce(sum(td.REG_UNITS_CTALOGUE), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 8 and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null and F.Deleted = 0

    update R set R.REG_UNITS_CTALOGUE = (
		select coalesce(sum(td.REG_UNITS_CTALOGUE), 0)
		from tblDOCUMENT_STATS td join tblINVENTORY ti on td.ISN_INVENTORY = ti.ISN_INVENTORY
		where td.ISN_FUND = R.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
		and td.ISN_DOC_TYPE = 8 and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY is not null
	)
    from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null and F.Deleted = 0
end

if (@E_KARTED = 1)			-- +Количество закартонированных единиц хранения
    update R set R.CARDBOARDED = (
	    select coalesce(sum(tic.CARDBOARDED), 0)
	    from tblINVENTORY ti join tblINVENTORY_CHECK tic on ti.ISN_INVENTORY = tic.ISN_INVENTORY
	    where R.ISN_FUND = ti.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
	)
    from tblFUND_CHECK R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and F.Deleted = 0

if (@E_SHIFR = 1)			-- +Количество единиц хранения, требующих шифровки
	update R set R.UNITS_NEED_ENCIPHERING = (
	    select coalesce(sum(tic.UNITS_NEED_ENCIPHERING), 0)
	    from tblINVENTORY ti join tblINVENTORY_CHECK tic on ti.ISN_INVENTORY = tic.ISN_INVENTORY
	    where R.ISN_FUND = ti.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
	)
    from tblFUND_CHECK R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and F.Deleted = 0

if (@E_KART = 1)			-- +Количество единиц хранения, требующих картонирования
	update R set R.UNITS_NEED_CARDBOARDED = (
	    select coalesce(sum(tic.UNITS_NEED_CARDBOARDED), 0)
	    from tblINVENTORY ti join tblINVENTORY_CHECK tic on ti.ISN_INVENTORY = tic.ISN_INVENTORY
	    where R.ISN_FUND = ti.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
	)
    from tblFUND_CHECK R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and F.Deleted = 0

if (@E_ZAM_OBL = 1)			-- +Количество единиц хранения, требующих замены обложки
	update R set R.UNITS_NEED_COVER_CHANGE = (
	    select coalesce(sum(tic.UNITS_NEED_COVER_CHANGE), 0)
	    from tblINVENTORY ti join tblINVENTORY_CHECK tic on ti.ISN_INVENTORY = tic.ISN_INVENTORY
	    where R.ISN_FUND = ti.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
	)
    from tblFUND_CHECK R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and F.Deleted = 0

if (@E_POVR = 1)			-- +Количество неисправимо поврежденных единиц хранения
	update R set R.UNITS_DBR = (
	    select coalesce(sum(tic.UNITS_DBR), 0)
	    from tblINVENTORY ti join tblINVENTORY_CHECK tic on ti.ISN_INVENTORY = tic.ISN_INVENTORY
	    where R.ISN_FUND = ti.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
	)
    from tblFUND_CHECK R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and F.Deleted = 0

if (@E_ZAT = 1)				-- +Количество листов с затухающими текстами
	update R set R.FADING_PAGES = (
	    select coalesce(sum(tic.FADING_PAGES), 0)
	    from tblINVENTORY ti join tblINVENTORY_CHECK tic on ti.ISN_INVENTORY = tic.ISN_INVENTORY
	    where R.ISN_FUND = ti.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
	)
    from tblFUND_CHECK R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and F.Deleted = 0

if (@E_RESTAVR = 1)			-- +Количество единиц хранения, требующих реставрации
	update R set R.UNITS_NEED_RESTORATION = (
	    select coalesce(sum(tic.UNITS_NEED_RESTORATION), 0)
	    from tblINVENTORY ti join tblINVENTORY_CHECK tic on ti.ISN_INVENTORY = tic.ISN_INVENTORY
	    where R.ISN_FUND = ti.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
	)
    from tblFUND_CHECK R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and F.Deleted = 0

if (@E_GOR = 1)				-- +Количество единиц хранения на горючей основе
	update R set R.UNITS_INFLAMMABLE = (
	    select coalesce(sum(tic.UNITS_INFLAMMABLE), 0)
	    from tblINVENTORY ti join tblINVENTORY_CHECK tic on ti.ISN_INVENTORY = tic.ISN_INVENTORY
	    where R.ISN_FUND = ti.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
	)
    from tblFUND_CHECK R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and F.Deleted = 0

if (@E_PODSH = 1)			-- +Количество единиц хранения, требующих подшивки или переплета
	update R set R.UNITS_NEED_BINDING = (
	    select coalesce(sum(tic.UNITS_NEED_BINDING), 0)
	    from tblINVENTORY ti join tblINVENTORY_CHECK tic on ti.ISN_INVENTORY = tic.ISN_INVENTORY
	    where R.ISN_FUND = ti.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
	)
    from tblFUND_CHECK R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and F.Deleted = 0

if (@E_KPO = 1)				-- +Количество единиц хранения, требующих КПО
	update R set R.UNITS_NEED_KPO = (
	    select coalesce(sum(tic.UNITS_NEED_KPO), 0)
	    from tblINVENTORY ti join tblINVENTORY_CHECK tic on ti.ISN_INVENTORY = tic.ISN_INVENTORY
	    where R.ISN_FUND = ti.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
	)
    from tblFUND_CHECK R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and F.Deleted = 0

if (@E_DEZINF = 1)			-- +Количество единиц хранения, требующих дезинфекции
	update R set R.UNITS_NEED_DISINFECTION = (
	    select coalesce(sum(tic.UNITS_NEED_DISINFECTION), 0)
	    from tblINVENTORY ti join tblINVENTORY_CHECK tic on ti.ISN_INVENTORY = tic.ISN_INVENTORY
	    where R.ISN_FUND = ti.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
	)
    from tblFUND_CHECK R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and F.Deleted = 0

if (@E_DEZINS = 1)			-- +Количество единиц хранения, требующих дезинсекции
	update R set R.UNITS_NEED_DISINSECTION = (
	    select coalesce(sum(tic.UNITS_NEED_DISINSECTION), 0)
	    from tblINVENTORY ti join tblINVENTORY_CHECK tic on ti.ISN_INVENTORY = tic.ISN_INVENTORY
	    where R.ISN_FUND = ti.ISN_FUND and ti.PRESENCE_FLAG = 'a' and ti.Deleted = 0
	)
    from tblFUND_CHECK R join tblFUND F on R.ISN_FUND = F.ISN_FUND
    where /*where*/ and F.Deleted = 0

if (@HRAN_FOND = 1)			-- Топография Хранилища фонда	
begin
	declare @max_num bigint
	set @max_num = (select coalesce(MAX(ISN_REF_LOCATION),0) + 1 from tblREF_LOCATION)

	delete tblREF_LOCATION
	where tblREF_LOCATION.ISN_OBJ in (
		select R.ISN_FUND from tblFUND R where 1=1 and R.Deleted = 0) and tblREF_LOCATION.KIND = 701

	insert into tblREF_LOCATION (ID, OwnerID, CreationDateTime, DocID, RowID,
								 ISN_REF_LOCATION, ISN_LOCATION, ISN_OBJ, UNIT_NUM_FROM, UNIT_NUM_TO, KIND)
	select
		newid(), '12345678-9012-3456-7890-123456789012', getdate(), i.ID, 0,
		row_number() over (order by i.ID) + @max_num,
		i.ISN,
		i.ISN_FUND,
		null,
		null,
		701
	from (
		select distinct	
			FUND.ID,
			FUND.ISN_FUND,
			Untree.ISN
		from tblINVENTORY INVENTORY
		join (select * from tblFUND R where 1=1 and R.Deleted = 0) FUND on INVENTORY.ISN_FUND = FUND.ISN_FUND
		join tblREF_LOCATION REF on REF.ISN_OBJ = INVENTORY.ISN_INVENTORY
		join tblLOCATION LOCATION on LOCATION.ISN_LOCATION = REF.ISN_LOCATION
		join vLocationUntree Untree on Untree.ISN_LOCATION = LOCATION.ISN_LOCATION
		where REF.KIND = 702 and REF.ISN_LOCATION is not null and INVENTORY.Deleted = 0 and LOCATION.Deleted = 0
	) i
end

if (@SUM_BO = 1)			-- +Суммарные количественные характеристики документов на бумажной основе
begin
	update R set
	R.UNIT_COUNT = (
		select coalesce(sum(td.UNIT_COUNT), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.ISN_INVENTORY is null and td.CARRIER_TYPE = 'P' and td.ISN_DOC_TYPE in (1, 2, 3, 4)
	),
	R.UNIT_INVENTORY = (
		select coalesce(sum(td.UNIT_INVENTORY), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND  
		and td.ISN_INVENTORY is null and td.CARRIER_TYPE = 'P' and td.ISN_DOC_TYPE in (1, 2, 3, 4)
	),
	R.UNIT_REGISTERED = (
		select coalesce(sum(td.UNIT_REGISTERED), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND  
		and td.ISN_INVENTORY is null and td.CARRIER_TYPE = 'P' and td.ISN_DOC_TYPE in (1, 2, 3, 4)
	),
	R.UNIT_UNDESCRIBED = (
		select coalesce(sum(td.UNIT_UNDESCRIBED), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND  
		and td.ISN_INVENTORY is null and td.CARRIER_TYPE = 'P' and td.ISN_DOC_TYPE in (1, 2, 3, 4)
	),
	R.UNIT_OC_COUNT = (
		select coalesce(sum(td.UNIT_OC_COUNT), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND  
		and td.ISN_INVENTORY is null and td.CARRIER_TYPE = 'P' and td.ISN_DOC_TYPE in (1, 2, 3, 4)
	),
	R.UNITS_UNIQUE = (
		select coalesce(sum(td.UNITS_UNIQUE), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND  
		and td.ISN_INVENTORY is null and td.CARRIER_TYPE = 'P' and td.ISN_DOC_TYPE in (1, 2, 3, 4)
	),
	R.UNIT_HAS_SF = (
		select coalesce(sum(td.UNIT_HAS_SF), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND  
		and td.ISN_INVENTORY is null and td.CARRIER_TYPE = 'P' and td.ISN_DOC_TYPE in (1, 2, 3, 4)
	),
	R.UNIT_HAS_FP = (
		select coalesce(sum(td.UNIT_HAS_FP), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND  
		and td.ISN_INVENTORY is null and td.CARRIER_TYPE = 'P' and td.ISN_DOC_TYPE in (1, 2, 3, 4)
	),
	R.SECRET_UNITS = (
		select coalesce(sum(td.SECRET_UNITS), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND  
		and td.ISN_INVENTORY is null and td.CARRIER_TYPE = 'P' and td.ISN_DOC_TYPE in (1, 2, 3, 4)
	),
	R.UNITS_NOT_FOUND = (
		select coalesce(sum(td.UNITS_NOT_FOUND), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND  
		and td.ISN_INVENTORY is null and td.CARRIER_TYPE = 'P' and td.ISN_DOC_TYPE in (1, 2, 3, 4)
	),
	R.UNITS_CATALOGUED = (
		select coalesce(sum(td.UNITS_CATALOGUED), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND  
		and td.ISN_INVENTORY is null and td.CARRIER_TYPE = 'P' and td.ISN_DOC_TYPE in (1, 2, 3, 4)
	)
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
	where R.ISN_DOC_TYPE is null and R.CARRIER_TYPE = 'P' and R.ISN_INVENTORY is null
	and /*where*/ and F.Deleted = 0
end

if (@SUM_TN = 1)			-- +Суммарные количественные характеристики аудиовизуальных документов на традиционных носителях
begin
	update R set
	R.UNIT_COUNT = (
		select coalesce(sum(td.UNIT_COUNT), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND  
		and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE in (5, 6, 7, 8) and td.ISN_INVENTORY is null
	),
	R.UNIT_INVENTORY = (
		select coalesce(sum(td.UNIT_INVENTORY), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE in (5, 6, 7, 8) and td.ISN_INVENTORY is null
	),
	R.UNIT_REGISTERED = (
		select coalesce(sum(td.UNIT_REGISTERED), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE in (5, 6, 7, 8) and td.ISN_INVENTORY is null
	),
	R.UNIT_UNDESCRIBED = (
		select coalesce(sum(td.UNIT_UNDESCRIBED), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE in (5, 6, 7, 8) and td.ISN_INVENTORY is null
	),
	R.UNIT_OC_COUNT = (
		select coalesce(sum(td.UNIT_OC_COUNT), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND  
		and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE in (5, 6, 7, 8) and td.ISN_INVENTORY is null
	),
	R.UNITS_UNIQUE = (
		select coalesce(sum(td.UNITS_UNIQUE), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE in (5, 6, 7, 8) and td.ISN_INVENTORY is null
	),
	R.UNIT_HAS_SF = (
		select coalesce(sum(td.UNIT_HAS_SF), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND  
		and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE in (5, 6, 7, 8) and td.ISN_INVENTORY is null
	),
	R.UNIT_HAS_FP = (
		select coalesce(sum(td.UNIT_HAS_FP), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE in (5, 6, 7, 8) and td.ISN_INVENTORY is null
	),
	R.SECRET_UNITS = (
		select coalesce(sum(td.SECRET_UNITS), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE in (5, 6, 7, 8) and td.ISN_INVENTORY is null
	),
	R.UNITS_NOT_FOUND = (
		select coalesce(sum(td.UNITS_NOT_FOUND), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE in (5, 6, 7, 8) and td.ISN_INVENTORY is null
	),
	R.UNITS_CATALOGUED = (
		select coalesce(sum(td.UNITS_CATALOGUED), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE in (5, 6, 7, 8) and td.ISN_INVENTORY is null
	),
	R.REG_UNIT = (
		select coalesce(sum(td.REG_UNIT), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE in (5, 6, 7, 8) and td.ISN_INVENTORY is null
	),            
	R.REG_UNIT_INVENTORY = (
		select coalesce(sum(td.REG_UNIT_INVENTORY), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE in (5, 6, 7, 8) and td.ISN_INVENTORY is null
	),            
	R.REG_UNIT_REGISTERED = (
		select coalesce(sum(td.REG_UNIT_REGISTERED), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE in (5, 6, 7, 8) and td.ISN_INVENTORY is null
	),            
	R.REG_UNIT_OC = (
		select coalesce(sum(td.REG_UNIT_OC), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE in (5, 6, 7, 8) and td.ISN_INVENTORY is null
	),            
	R.REG_UNITS_UNIQUE = (
		select coalesce(sum(td.REG_UNITS_UNIQUE), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE in (5, 6, 7, 8) and td.ISN_INVENTORY is null
	),            
	R.REG_UNIT_HAS_SF = (
		select coalesce(sum(td.REG_UNIT_HAS_SF), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND  
		and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE in (5, 6, 7, 8) and td.ISN_INVENTORY is null
	),            
	R.REG_UNIT_HAS_FP = (
		select coalesce(sum(td.REG_UNIT_HAS_FP), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND  
		and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE in (5, 6, 7, 8) and td.ISN_INVENTORY is null
	),            
	R.SECRET_REG_UNITS = (
		select coalesce(sum(td.SECRET_REG_UNITS), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND  
		and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE in (5, 6, 7, 8) and td.ISN_INVENTORY is null
	),            
	R.REG_UNITS_NOT_FOUND = (
		select coalesce(sum(td.REG_UNITS_NOT_FOUND), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND  
		and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE in (5, 6, 7, 8) and td.ISN_INVENTORY is null
	),
	R.REG_UNITS_CTALOGUE = (
		select coalesce(sum(td.REG_UNITS_CTALOGUE), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND  
		and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE in (5, 6, 7, 8) and td.ISN_INVENTORY is null
	)            
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND
	where R.ISN_DOC_TYPE is null and R.CARRIER_TYPE = 'A' and R.ISN_INVENTORY is null
	and /*where*/ and F.Deleted = 0
end

if (@SUM_EN = 1)			-- +Суммарные количественные характеристики документов на электронных носителях
begin
	update R set
	R.UNIT_COUNT = (
		select coalesce(sum(td.UNIT_COUNT), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND  
		and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE in (4, 5, 6, 7, 8) and td.ISN_INVENTORY is null
	),
	R.UNIT_INVENTORY = (
		select coalesce(sum(td.UNIT_INVENTORY), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND  
		and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE in (4, 5, 6, 7, 8) and td.ISN_INVENTORY is null
	),
	R.UNIT_REGISTERED = (
		select coalesce(sum(td.UNIT_REGISTERED), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND  
		and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE in (4, 5, 6, 7, 8) and td.ISN_INVENTORY is null
	),
	R.UNIT_UNDESCRIBED = (
		select coalesce(sum(td.UNIT_UNDESCRIBED), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND  
		and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE in (4, 5, 6, 7, 8) and td.ISN_INVENTORY is null
	),
	R.UNIT_OC_COUNT = (
		select coalesce(sum(td.UNIT_OC_COUNT), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND  
		and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE in (4, 5, 6, 7, 8) and td.ISN_INVENTORY is null
	),
	R.UNITS_UNIQUE = (
		select coalesce(sum(td.UNITS_UNIQUE), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND  
		and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE in (4, 5, 6, 7, 8) and td.ISN_INVENTORY is null
	),
	R.UNIT_HAS_SF = (
		select coalesce(sum(td.UNIT_HAS_SF), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND  
		and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE in (4, 5, 6, 7, 8) and td.ISN_INVENTORY is null
	),
	R.UNIT_HAS_FP = (
		select coalesce(sum(td.UNIT_HAS_FP), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND  
		and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE in (4, 5, 6, 7, 8) and td.ISN_INVENTORY is null
	),
	R.SECRET_UNITS = (
		select coalesce(sum(td.SECRET_UNITS), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND  
		and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE in (4, 5, 6, 7, 8) and td.ISN_INVENTORY is null
	),
	R.UNITS_NOT_FOUND = (
		select coalesce(sum(td.UNITS_NOT_FOUND), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND  
		and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE in (4, 5, 6, 7, 8) and td.ISN_INVENTORY is null
	),
	R.UNITS_CATALOGUED = (
		select coalesce(sum(td.UNITS_CATALOGUED), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND  
		and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE in (4, 5, 6, 7, 8) and td.ISN_INVENTORY is null
	),
	R.REG_UNIT = (
		select coalesce(sum(REG_UNIT), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND  
		and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE in (4, 5, 6, 7, 8) and td.ISN_INVENTORY is null
	),            
	R.REG_UNIT_INVENTORY = (
		select coalesce(sum(td.REG_UNIT_INVENTORY), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND  
		and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE in (4, 5, 6, 7, 8) and td.ISN_INVENTORY is null
	),            
	R.REG_UNIT_REGISTERED = (
		select coalesce(sum(td.REG_UNIT_REGISTERED), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND  
		and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE in (4, 5, 6, 7, 8) and td.ISN_INVENTORY is null
	),            
	R.REG_UNIT_OC = (
		select coalesce(sum(td.REG_UNIT_OC), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND  
		and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE in (4, 5, 6, 7, 8) and td.ISN_INVENTORY is null
	),            
	R.REG_UNITS_UNIQUE = (
		select coalesce(sum(td.REG_UNITS_UNIQUE), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND  
		and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE in (4, 5, 6, 7, 8) and td.ISN_INVENTORY is null
	),            
	R.REG_UNIT_HAS_SF = (
		select coalesce(sum(td.REG_UNIT_HAS_SF), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND  
		and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE in (4, 5, 6, 7, 8) and td.ISN_INVENTORY is null
	),            
	R.REG_UNIT_HAS_FP = (
		select coalesce(sum(td.REG_UNIT_HAS_FP), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND  
		and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE in (4, 5, 6, 7, 8) and td.ISN_INVENTORY is null
	),            
	R.SECRET_REG_UNITS = (
		select coalesce(sum(td.SECRET_REG_UNITS), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND  
		and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE in (4, 5, 6, 7, 8) and td.ISN_INVENTORY is null
	),            
	R.REG_UNITS_NOT_FOUND = (
		select coalesce(sum(td.REG_UNITS_NOT_FOUND), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND  
		and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE in (4, 5, 6, 7, 8) and td.ISN_INVENTORY is null
	),
	R.REG_UNITS_CTALOGUE = (
		select coalesce(sum(td.REG_UNITS_CTALOGUE), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND  
		and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE in (4, 5, 6, 7, 8) and td.ISN_INVENTORY is null
	)      
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND      
	where R.ISN_DOC_TYPE is null and R.CARRIER_TYPE = 'E' and R.ISN_INVENTORY is null
	and /*where*/ and F.Deleted = 0
end                            

if (@SUM_AA = 1)			-- +Суммарные количественные характеристики всех типов документов на всех носителях
begin
	update R set
	R.UNIT_COUNT = (
		select coalesce(sum(td.UNIT_COUNT), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and (td.ISN_DOC_TYPE is null or td.ISN_DOC_TYPE = 9) and td.ISN_INVENTORY is null
		and td.CARRIER_TYPE in ('P', 'A', 'M', 'E')
	),
	R.UNIT_INVENTORY = (
		select coalesce(sum(td.UNIT_INVENTORY), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and (td.ISN_DOC_TYPE is null or td.ISN_DOC_TYPE = 9) and td.ISN_INVENTORY is null
		and td.CARRIER_TYPE in ('P', 'A', 'M', 'E')
	),
	R.UNIT_REGISTERED = (
		select coalesce(sum(td.UNIT_REGISTERED), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and (td.ISN_DOC_TYPE is null or td.ISN_DOC_TYPE = 9) and td.ISN_INVENTORY is null
		and td.CARRIER_TYPE in ('P', 'A', 'M', 'E')
	),
	R.UNIT_UNDESCRIBED = (
		select coalesce(sum(td.UNIT_UNDESCRIBED), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and (td.ISN_DOC_TYPE is null or td.ISN_DOC_TYPE = 9) and td.ISN_INVENTORY is null
		and td.CARRIER_TYPE in ('P', 'A', 'M', 'E')
	),
	R.UNIT_OC_COUNT = (
		select coalesce(sum(td.UNIT_OC_COUNT), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and (td.ISN_DOC_TYPE is null or td.ISN_DOC_TYPE = 9) and td.ISN_INVENTORY is null
		and td.CARRIER_TYPE in ('P', 'A', 'M', 'E')
	),
	R.UNITS_UNIQUE = (
		select coalesce(sum(td.UNITS_UNIQUE), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and (td.ISN_DOC_TYPE is null or td.ISN_DOC_TYPE = 9) and td.ISN_INVENTORY is null
		and td.CARRIER_TYPE in ('P', 'A', 'M', 'E')
	),
	R.UNIT_HAS_SF = (
		select coalesce(sum(td.UNIT_HAS_SF), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and (td.ISN_DOC_TYPE is null or td.ISN_DOC_TYPE = 9) and td.ISN_INVENTORY is null
		and td.CARRIER_TYPE in ('P', 'A', 'M', 'E')
	),
	R.UNIT_HAS_FP = (
		select coalesce(sum(td.UNIT_HAS_FP), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and (td.ISN_DOC_TYPE is null or td.ISN_DOC_TYPE = 9) and td.ISN_INVENTORY is null
		and td.CARRIER_TYPE in ('P', 'A', 'M', 'E')
	),
	R.SECRET_UNITS = (
		select coalesce(sum(td.SECRET_UNITS), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and (td.ISN_DOC_TYPE is null or td.ISN_DOC_TYPE = 9) and td.ISN_INVENTORY is null
		and td.CARRIER_TYPE in ('P', 'A', 'M', 'E')
	),
	R.UNITS_NOT_FOUND = (
		select coalesce(sum(td.UNITS_NOT_FOUND), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and (td.ISN_DOC_TYPE is null or td.ISN_DOC_TYPE = 9) and td.ISN_INVENTORY is null
		and td.CARRIER_TYPE in ('P', 'A', 'M', 'E')
	),
	R.UNITS_CATALOGUED = (
		select coalesce(sum(td.UNITS_CATALOGUED), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and (td.ISN_DOC_TYPE is null or td.ISN_DOC_TYPE = 9) and td.ISN_INVENTORY is null
		and td.CARRIER_TYPE in ('P', 'A', 'M', 'E')
	),            
	R.REG_UNIT = (
		select coalesce(sum(td.REG_UNIT), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and (td.ISN_DOC_TYPE is null or td.ISN_DOC_TYPE = 9) and td.ISN_INVENTORY is null
		and td.CARRIER_TYPE in ('A', 'E')
	),            
	R.REG_UNIT_INVENTORY = (
		select coalesce(sum(td.REG_UNIT_INVENTORY), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and (td.ISN_DOC_TYPE is null or td.ISN_DOC_TYPE = 9) and td.ISN_INVENTORY is null
		and td.CARRIER_TYPE in ('A', 'E')
	),            
	R.REG_UNIT_REGISTERED = (
		select coalesce(sum(td.REG_UNIT_REGISTERED), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and (td.ISN_DOC_TYPE is null or td.ISN_DOC_TYPE = 9) and td.ISN_INVENTORY is null
		and td.CARRIER_TYPE in ('A', 'E')
	),            
	R.REG_UNIT_OC = (
		select coalesce(sum(td.REG_UNIT_OC), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and (td.ISN_DOC_TYPE is null or td.ISN_DOC_TYPE = 9) and td.ISN_INVENTORY is null
		and td.CARRIER_TYPE in ('A', 'E')
	),            
	R.REG_UNITS_UNIQUE = (
		select coalesce(sum(td.REG_UNITS_UNIQUE), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and (td.ISN_DOC_TYPE is null or td.ISN_DOC_TYPE = 9) and td.ISN_INVENTORY is null
		and td.CARRIER_TYPE in ('A', 'E')
	),            
	R.REG_UNIT_HAS_SF = (
		select coalesce(sum(td.REG_UNIT_HAS_SF), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and (td.ISN_DOC_TYPE is null or td.ISN_DOC_TYPE = 9) and td.ISN_INVENTORY is null
		and td.CARRIER_TYPE in ('A', 'E')
	),            
	R.REG_UNIT_HAS_FP = (
		select coalesce(sum(td.REG_UNIT_HAS_FP), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and (td.ISN_DOC_TYPE is null or td.ISN_DOC_TYPE = 9) and td.ISN_INVENTORY is null
		and td.CARRIER_TYPE in ('A', 'E')
	),            
	R.SECRET_REG_UNITS = (
		select coalesce(sum(td.SECRET_REG_UNITS), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and (td.ISN_DOC_TYPE is null or td.ISN_DOC_TYPE = 9) and td.ISN_INVENTORY is null
		and td.CARRIER_TYPE in ('A', 'E')
	),            
	R.REG_UNITS_NOT_FOUND = (
		select coalesce(sum(td.REG_UNITS_NOT_FOUND), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and (td.ISN_DOC_TYPE is null or td.ISN_DOC_TYPE = 9) and td.ISN_INVENTORY is null
		and td.CARRIER_TYPE in ('A', 'E')
	),
	R.REG_UNITS_CTALOGUE = (
		select coalesce(sum(td.REG_UNITS_CTALOGUE), 0)
		from tblDOCUMENT_STATS td
		where td.ISN_FUND = R.ISN_FUND 
		and (td.ISN_DOC_TYPE is null or td.ISN_DOC_TYPE = 9) and td.ISN_INVENTORY is null
		and td.CARRIER_TYPE in ('A', 'E')
	)      
	from tblDOCUMENT_STATS R join tblFUND F on R.ISN_FUND = F.ISN_FUND      
	where R.ISN_DOC_TYPE is null and R.CARRIER_TYPE is null and R.ISN_INVENTORY is null
	and /*where*/ and F.Deleted = 0
end


                                        </sql>, _
                                         <sql>


-- Пересчет описи

-- замена параметров для тестирования запроса
/*
DECLARE @A_NACH_DATA bit = 1	-- начальная дата документов
DECLARE @A_KON_DATA bit = 1		-- конечная дата документов
DECLARE @A_MUSEUM bit = 1		-- наличие музейных предметов
DECLARE @A_TREASURE bit = 1		-- кол-во ед.хр. с драг. камнями и металлами в оформлении

DECLARE @E_ALL_DV bit = 1		-- всего ед.хр. в соответствии с разделом 'Движение документов'
DECLARE @E_ALL bit = 1			-- кол-во ед.хр.
DECLARE @U_ALL bit = 1			-- кол-во ед.уч.
DECLARE @E_REG bit = 1			-- кол-во зарег. ед.хр.
DECLARE @U_REG bit = 1			-- кол-во зарег. ед.уч.
DECLARE @E_OC bit = 1			-- кол-во особо ценных ед.хр.
DECLARE @U_OC bit = 1			-- кол-во особо ценных ед.уч.
DECLARE @E_UN bit = 1			-- кол-во уникальных ед.хр.
DECLARE @U_UN bit = 1			-- кол-во уникальных ед.уч.
DECLARE @E_SF bit = 1			-- кол-во ед.хр. имеющих СФ
DECLARE @U_SF bit = 1			-- кол-во ед.уч. имеющих СФ
DECLARE @E_FP bit = 1			-- кол-во ед.хр. имеющих ФП
DECLARE @U_FP bit = 1			-- кол-во ед.уч. имеющих ФП
DECLARE @E_NEO bit = 1			-- кол-во необнаруж. ед.хр.
DECLARE @U_NEO bit = 1			-- кол-во необнаруж. ед.уч.
DECLARE @E_SEC bit = 1			-- кол-во секретных ед.хр.
DECLARE @U_SEC bit = 1			-- кол-во секретных ед.уч.
DECLARE @E_CAT bit = 1			-- кол-во закаталог. ед.хр.
DECLARE @U_CAT bit = 1			-- кол-во закаталог. ед.уч.

DECLARE @E_KARTED bit = 1		-- кол-во закартониров. ед.хр.
DECLARE @E_SHIFR bit = 1		-- кол-во ед.хр. требующих шифрофки
DECLARE @E_KART bit = 1			-- кол-во ед.хр. требующих еартонизации
DECLARE @E_ZAM_OBL bit = 1		-- кол-во ед.хр. требующих замены обложки
DECLARE @E_POVR bit = 1			-- кол-во неисправимо поврежденных ед.хр. 
DECLARE @E_ZAT bit = 1			-- кол-во листов с затухающими текстами
DECLARE @E_RESTAVR bit = 1		-- кол-во ед.хр. требующих реставрации
DECLARE @E_GOR bit = 1			-- кол-во ед.хр. требующих на горючей основе
DECLARE @E_PODSH bit = 1		-- кол-во ед.хр. требующих подшивки и переплета
DECLARE @E_KPO bit = 1			-- кол-во ед.хр. требующих требующих КПО
DECLARE @E_DEZINF bit = 1		-- кол-во ед.хр. требующих дезинфекции
DECLARE @E_DEZINS bit = 1		-- кол-во ед.хр. требующих дезинсекции
DECLARE @E_RAB bit = 1			-- работы проводимые с документами описи

DECLARE @E_TOPO bit = 1			-- топография и номера единиц хранения

DECLARE @SUM_AA bit = 1			-- сум. кол. хар-ки всех типов документов на всех носителях
DECLARE @SUM_BO bit = 1			-- сум. кол. хар-ки документов на бумажной основе
DECLARE @SUM_TN bit = 1			-- сум. кол. хар-ки аудиовизуальных документов на трад. носителях
DECLARE @SUM_EN bit = 1			-- сум. кол. хар-ки документов на электронных носителях
*/
-- замена параметров для тестирования запроса

if (@A_NACH_DATA = 1)	 -- Начальная дата документов
	update R set R.DOC_START_YEAR = (
		select MIN(tu.START_YEAR) from tblUNIT tu
		where tu.ISN_INVENTORY = R.ISN_INVENTORY and
		tu.START_YEAR is not null and tu.Deleted = 0 and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST='')))
	from tblINVENTORY R
	left join tblUNIT U on R.ISN_INVENTORY = U.ISN_INVENTORY
	where /*where*/ and U.START_YEAR is not null and U.Deleted = 0 and R.Deleted = 0

if (@A_KON_DATA = 1)	 -- Конечная дата документов
	update R set R.DOC_END_YEAR = (
		select MAX(tu.END_YEAR) from tblUNIT tu
		where tu.ISN_INVENTORY = R.ISN_INVENTORY
		and tu.END_YEAR is not null and tu.Deleted = 0 and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST='')))
	from tblINVENTORY R
	left join  tblUNIT U on R.ISN_INVENTORY = U.ISN_INVENTORY
	where /*where*/ and U.END_YEAR is not null and U.Deleted = 0 and R.Deleted = 0

if (@A_MUSEUM = 1)		 -- Наличие музейных предметов
	update R set R.MUSEUM_UNITS_COUNT = (
		case when exists(
			select tu.ID from tblUNIT tu
			where tu.ISN_INVENTORY = R.ISN_INVENTORY and
			tu.IS_MUSEUM_ITEM = 'Y' and tu.Deleted = 0 and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST='')))
		then 'Y' else 'N' end)
	from tblINVENTORY R
	left join  tblUNIT U on R.ISN_INVENTORY = U.ISN_INVENTORY
	where /*where*/ and U.IS_MUSEUM_ITEM is not null and U.Deleted = 0 and R.Deleted = 0

if (@A_TREASURE = 1)	-- Кол-во ед.хр. с драг. металлами и камнями в оформлении
	update R set R.UNITS_WITH_TREASURES_COUNT = (
		select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_INVENTORY = R.ISN_INVENTORY and
		tu.HAS_TREASURES = 'Y' and tu.Deleted = 0 and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST='')))
	from tblINVENTORY R
	left join  tblUNIT U on R.ISN_INVENTORY = U.ISN_INVENTORY
	where /*where*/ and U.HAS_TREASURES is not null and U.Deleted = 0 and R.Deleted = 0

----

if (@E_ALL_DV = 1)		-- Всего единиц хранения в соответствии с разделом «Движение документов»
	update R set R.UNIT_COUNT =
		((select coalesce(SUM(tr.UNIT_COUNT),0) from tblACT ta, tblINVENTORY ti, tblREF_ACT tr
		where ti.ISN_INVENTORY = tr.ISN_OBJ and ti.ISN_INVENTORY = R.ISN_INVENTORY
		and tr.KIND = 702 and tr.ISN_ACT = ta.ISN_ACT and ta.MOVEMENT_FLAG = 0
		and ta.Deleted = 0 and ti.Deleted = 0)
		-
		(select coalesce(SUM(tr.UNIT_COUNT),0) from tblACT ta, tblINVENTORY ti, tblREF_ACT tr
		where ti.ISN_INVENTORY = tr.ISN_OBJ and ti.ISN_INVENTORY = R.ISN_INVENTORY
		and tr.KIND = 702 and tr.ISN_ACT = ta.ISN_ACT and ta.MOVEMENT_FLAG = 2
		and ta.Deleted = 0 and ti.Deleted = 0))
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE is NULL and R.CARRIER_TYPE is NULL
	and /*where*/ and I.Deleted = 0

if (@E_ALL = 1)			-- Всего единиц хранения
begin
	update R set R.UNIT_COUNT =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 1 and tu.UNIT_KIND = 703 and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 1 and R.CARRIER_TYPE = 'P'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_COUNT =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 2 and tu.UNIT_KIND = 703 and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 2 and R.CARRIER_TYPE = 'P'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_COUNT =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 3 and tu.UNIT_KIND = 703 and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 3 and R.CARRIER_TYPE = 'P'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_COUNT =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 4 and tu.UNIT_KIND = 703 and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'P'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_COUNT =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 5 and tu.UNIT_KIND = 703 and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_COUNT =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 6 and tu.UNIT_KIND = 703 and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_COUNT =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 7 and tu.UNIT_KIND = 703 and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_COUNT =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 8 and tu.UNIT_KIND = 703 and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_COUNT =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 9 and tu.UNIT_KIND = 703 and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 9 and R.CARRIER_TYPE = 'M'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_COUNT =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 5 and tu.UNIT_KIND = 703 and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_COUNT =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 6 and tu.UNIT_KIND = 703 and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_COUNT =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 7 and tu.UNIT_KIND = 703 and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_COUNT =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 8 and tu.UNIT_KIND = 703 and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_COUNT =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 4 and tu.UNIT_KIND = 703 and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0
end

if (@U_ALL = 1)			-- Всего единиц учета
begin
	update R set R.REG_UNIT =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 6 and tu.UNIT_KIND = 704 and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNIT =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 7 and tu.UNIT_KIND = 704 and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNIT =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 8 and tu.UNIT_KIND = 704 and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNIT =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 6 and tu.UNIT_KIND = 704 and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNIT =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 7 and tu.UNIT_KIND = 704 and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNIT =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 8 and tu.UNIT_KIND = 704 and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNIT =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 4 and tu.UNIT_KIND = 704 and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNIT =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 5 and tu.UNIT_KIND = 704 and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0
end

if (@E_REG = 1)			-- Количество зарегистрированных единиц хранения
begin
	update R set R.UNIT_REGISTERED =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 1 and tu.UNIT_KIND = 703 and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 1 and R.CARRIER_TYPE = 'P'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_REGISTERED =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 2 and tu.UNIT_KIND = 703 and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 2 and R.CARRIER_TYPE = 'P'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_REGISTERED =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 3 and tu.UNIT_KIND = 703 and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 3 and R.CARRIER_TYPE = 'P'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_REGISTERED =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 4 and tu.UNIT_KIND = 703 and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'P'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_REGISTERED =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 5 and tu.UNIT_KIND = 703 and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_REGISTERED =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 6 and tu.UNIT_KIND = 703 and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_REGISTERED =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 7 and tu.UNIT_KIND = 703 and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_REGISTERED =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 8 and tu.UNIT_KIND = 703 and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_REGISTERED =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 9 and tu.UNIT_KIND = 703 and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 9 and R.CARRIER_TYPE = 'M'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_REGISTERED =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 5 and tu.UNIT_KIND = 703 and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_REGISTERED =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 6 and tu.UNIT_KIND = 703 and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_REGISTERED =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 7 and tu.UNIT_KIND = 703 and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_REGISTERED =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 8 and tu.UNIT_KIND = 703 and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_REGISTERED =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 4 and tu.UNIT_KIND = 703 and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0
end

if (@U_REG = 1)			-- Количество зарегистрированных единиц учета
begin
	update R set R.REG_UNIT_REGISTERED =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 6 and tu.UNIT_KIND = 704 and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNIT_REGISTERED =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 7 and tu.UNIT_KIND = 704 and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNIT_REGISTERED =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 8 and tu.UNIT_KIND = 704 and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNIT_REGISTERED =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 6 and tu.UNIT_KIND = 704 and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNIT_REGISTERED =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 7 and tu.UNIT_KIND = 704 and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNIT_REGISTERED =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 8 and tu.UNIT_KIND = 704 and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNIT_REGISTERED =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 4 and tu.UNIT_KIND = 704 and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNIT_REGISTERED =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 5 and tu.UNIT_KIND = 704 and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0
end

if (@E_OC = 1)			-- Количество особо ценных единиц хранения
begin
	update R set R.UNIT_OC_COUNT =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 1 and tu.UNIT_KIND = 703 and tu.UNIT_CATEGORY = 'c' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 1 and R.CARRIER_TYPE = 'P'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_OC_COUNT =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 3 and tu.UNIT_KIND = 703 and tu.UNIT_CATEGORY = 'c' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 3 and R.CARRIER_TYPE = 'P'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_OC_COUNT =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 4 and tu.UNIT_KIND = 703 and tu.UNIT_CATEGORY = 'c' and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'P'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_OC_COUNT =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 5 and tu.UNIT_KIND = 703 and tu.UNIT_CATEGORY = 'c' and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_OC_COUNT =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 6 and tu.UNIT_KIND = 703 and tu.UNIT_CATEGORY = 'c' and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_OC_COUNT =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 7 and tu.UNIT_KIND = 703 and tu.UNIT_CATEGORY = 'c' and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_OC_COUNT =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 8 and tu.UNIT_KIND = 703 and tu.UNIT_CATEGORY = 'c' and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_OC_COUNT =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 9 and tu.UNIT_KIND = 703 and tu.UNIT_CATEGORY = 'c' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 9 and R.CARRIER_TYPE = 'M'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_OC_COUNT =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 5 and tu.UNIT_KIND = 703 and tu.UNIT_CATEGORY = 'c' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_OC_COUNT =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 6 and tu.UNIT_KIND = 703 and tu.UNIT_CATEGORY = 'c' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_OC_COUNT =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 7 and tu.UNIT_KIND = 703 and tu.UNIT_CATEGORY = 'c' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_OC_COUNT =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 8 and tu.UNIT_KIND = 703 and tu.UNIT_CATEGORY = 'c' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_OC_COUNT =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 4 and tu.UNIT_KIND = 703 and tu.UNIT_CATEGORY = 'c' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0
end

if (@U_OC = 1)			-- Количество особо ценных единиц учета
begin
	update R set R.REG_UNIT_OC =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 6 and tu.UNIT_KIND = 704 and tu.UNIT_CATEGORY = 'c' and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNIT_OC =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 7 and tu.UNIT_KIND = 704 and tu.UNIT_CATEGORY = 'c' and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNIT_OC =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 8 and tu.UNIT_KIND = 704 and tu.UNIT_CATEGORY = 'c' and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNIT_OC =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 6 and tu.UNIT_KIND = 704 and tu.UNIT_CATEGORY = 'c' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNIT_OC =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 7 and tu.UNIT_KIND = 704 and tu.UNIT_CATEGORY = 'c' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNIT_OC =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 8 and tu.UNIT_KIND = 704 and tu.UNIT_CATEGORY = 'c' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNIT_OC =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 4 and tu.UNIT_KIND = 704 and tu.UNIT_CATEGORY = 'c' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNIT_OC =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 5 and tu.UNIT_KIND = 704 and tu.UNIT_CATEGORY = 'c' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0
end

if (@E_UN = 1)			-- Количество уникальных единиц хранения
begin
	update R set R.UNITS_UNIQUE =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 1 and tu.UNIT_KIND = 703 and tu.UNIT_CATEGORY = 'a' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 1 and R.CARRIER_TYPE = 'P'
	and /*where*/ and I.Deleted = 0

	update R set R.UNITS_UNIQUE =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 3 and tu.UNIT_KIND = 703 and tu.UNIT_CATEGORY = 'a' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 3 and R.CARRIER_TYPE = 'P'
	and /*where*/ and I.Deleted = 0

	update R set R.UNITS_UNIQUE =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 4 and tu.UNIT_KIND = 703 and tu.UNIT_CATEGORY = 'a' and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'P'
	and /*where*/ and I.Deleted = 0

	update R set R.UNITS_UNIQUE =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 5 and tu.UNIT_KIND = 703 and tu.UNIT_CATEGORY = 'a' and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.UNITS_UNIQUE =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 6 and tu.UNIT_KIND = 703 and tu.UNIT_CATEGORY = 'a' and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.UNITS_UNIQUE =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 7 and tu.UNIT_KIND = 703 and tu.UNIT_CATEGORY = 'a' and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.UNITS_UNIQUE =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 8 and tu.UNIT_KIND = 703 and tu.UNIT_CATEGORY = 'a' and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.UNITS_UNIQUE =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 9 and tu.UNIT_KIND = 703 and tu.UNIT_CATEGORY = 'a' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 9 and R.CARRIER_TYPE = 'M'
	and /*where*/ and I.Deleted = 0

	update R set R.UNITS_UNIQUE =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 5 and tu.UNIT_KIND = 703 and tu.UNIT_CATEGORY = 'a' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.UNITS_UNIQUE =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 6 and tu.UNIT_KIND = 703 and tu.UNIT_CATEGORY = 'a' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.UNITS_UNIQUE =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 7 and tu.UNIT_KIND = 703 and tu.UNIT_CATEGORY = 'a' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.UNITS_UNIQUE =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 8 and tu.UNIT_KIND = 703 and tu.UNIT_CATEGORY = 'a' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.UNITS_UNIQUE =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 4 and tu.UNIT_KIND = 703 and tu.UNIT_CATEGORY = 'a' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0
end

if (@U_UN = 1)			-- Количество уникальных единиц учета
begin
	update R set R.REG_UNITS_UNIQUE =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 6 and tu.UNIT_KIND = 704 and tu.UNIT_CATEGORY = 'a' and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNITS_UNIQUE =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 7 and tu.UNIT_KIND = 704 and tu.UNIT_CATEGORY = 'a' and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNITS_UNIQUE =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 8 and tu.UNIT_KIND = 704 and tu.UNIT_CATEGORY = 'a' and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNITS_UNIQUE =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 6 and tu.UNIT_KIND = 704 and tu.UNIT_CATEGORY = 'a' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNITS_UNIQUE =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 7 and tu.UNIT_KIND = 704 and tu.UNIT_CATEGORY = 'a' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNITS_UNIQUE =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 8 and tu.UNIT_KIND = 704 and tu.UNIT_CATEGORY = 'a' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNITS_UNIQUE =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 4 and tu.UNIT_KIND = 704 and tu.UNIT_CATEGORY = 'a' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNITS_UNIQUE =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 5 and tu.UNIT_KIND = 704 and tu.UNIT_CATEGORY = 'a' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0
end

if (@E_SF = 1)			-- Количество единиц хранения, имеющих СФ
begin
	update R set R.UNIT_HAS_SF =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 1 and tu.UNIT_KIND = 703 and tu.HAS_SF = 'Y' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 1 and R.CARRIER_TYPE = 'P'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_HAS_SF =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 3 and tu.UNIT_KIND = 703 and tu.HAS_SF = 'Y' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 3 and R.CARRIER_TYPE = 'P'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_HAS_SF =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 4 and tu.UNIT_KIND = 703 and tu.HAS_SF = 'Y' and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'P'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_HAS_SF =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 5 and tu.UNIT_KIND = 703 and tu.HAS_SF = 'Y' and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_HAS_SF =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 6 and tu.UNIT_KIND = 703 and tu.HAS_SF = 'Y' and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_HAS_SF =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 7 and tu.UNIT_KIND = 703 and tu.HAS_SF = 'Y' and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_HAS_SF =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 8 and tu.UNIT_KIND = 703 and tu.HAS_SF = 'Y' and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_HAS_SF =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 9 and tu.UNIT_KIND = 703 and tu.HAS_SF = 'Y' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 9 and R.CARRIER_TYPE = 'M'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_HAS_SF =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 5 and tu.UNIT_KIND = 703 and tu.HAS_SF = 'Y' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_HAS_SF =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 6 and tu.UNIT_KIND = 703 and tu.HAS_SF = 'Y' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_HAS_SF =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 7 and tu.UNIT_KIND = 703 and tu.HAS_SF = 'Y' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_HAS_SF =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 8 and tu.UNIT_KIND = 703 and tu.HAS_SF = 'Y' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_HAS_SF =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 4 and tu.UNIT_KIND = 703 and tu.HAS_SF = 'Y' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0
end

if (@U_SF = 1)			-- Количество единиц учета, имеющих СФ
begin
	update R set R.REG_UNIT_HAS_SF =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 6 and tu.UNIT_KIND = 704 and tu.HAS_SF = 'Y' and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNIT_HAS_SF =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 7 and tu.UNIT_KIND = 704 and tu.HAS_SF = 'Y' and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNIT_HAS_SF =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 8 and tu.UNIT_KIND = 704 and tu.HAS_SF = 'Y' and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNIT_HAS_SF =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 6 and tu.UNIT_KIND = 704 and tu.HAS_SF = 'Y' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNIT_HAS_SF =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 7 and tu.UNIT_KIND = 704 and tu.HAS_SF = 'Y' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNIT_HAS_SF =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 8 and tu.UNIT_KIND = 704 and tu.HAS_SF = 'Y' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNIT_HAS_SF =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 4 and tu.UNIT_KIND = 704 and tu.HAS_SF = 'Y' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNIT_HAS_SF =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 5 and tu.UNIT_KIND = 704 and tu.HAS_SF = 'Y' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0
end

if (@E_FP = 1)			-- Количество единиц хранения, имеющих ФП
begin
	update R set R.UNIT_HAS_FP =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 1 and tu.UNIT_KIND = 703 and tu.HAS_FP = 'Y' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 1 and R.CARRIER_TYPE = 'P'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_HAS_FP =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 2 and tu.UNIT_KIND = 703 and tu.HAS_FP = 'Y' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 2 and R.CARRIER_TYPE = 'P'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_HAS_FP =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 3 and tu.UNIT_KIND = 703 and tu.HAS_FP = 'Y' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 3 and R.CARRIER_TYPE = 'P'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_HAS_FP =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 4 and tu.UNIT_KIND = 703 and tu.HAS_FP = 'Y' and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'P'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_HAS_FP =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 5 and tu.UNIT_KIND = 703 and tu.HAS_FP = 'Y' and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_HAS_FP =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 6 and tu.UNIT_KIND = 703 and tu.HAS_FP = 'Y' and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_HAS_FP =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 7 and tu.UNIT_KIND = 703 and tu.HAS_FP = 'Y' and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_HAS_FP =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 8 and tu.UNIT_KIND = 703 and tu.HAS_FP = 'Y' and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_HAS_FP =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 9 and tu.UNIT_KIND = 703 and tu.HAS_FP = 'Y' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 9 and R.CARRIER_TYPE = 'M'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_HAS_FP =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 5 and tu.UNIT_KIND = 703 and tu.HAS_FP = 'Y' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_HAS_FP =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 6 and tu.UNIT_KIND = 703 and tu.HAS_FP = 'Y' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_HAS_FP =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 7 and tu.UNIT_KIND = 703 and tu.HAS_FP = 'Y' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_HAS_FP =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 8 and tu.UNIT_KIND = 703 and tu.HAS_FP = 'Y' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.UNIT_HAS_FP =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 4 and tu.UNIT_KIND = 703 and tu.HAS_FP = 'Y' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0
end

if (@U_FP = 1)			-- Количество единиц учета, имеющих ФП
begin
	update R set R.REG_UNIT_HAS_FP =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 6 and tu.UNIT_KIND = 704 and tu.HAS_FP = 'Y' and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNIT_HAS_FP =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 7 and tu.UNIT_KIND = 704 and tu.HAS_FP = 'Y' and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNIT_HAS_FP =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 8 and tu.UNIT_KIND = 704 and tu.HAS_FP = 'Y' and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNIT_HAS_FP =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 6 and tu.UNIT_KIND = 704 and tu.HAS_FP = 'Y' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNIT_HAS_FP =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 7 and tu.UNIT_KIND = 704 and tu.HAS_FP = 'Y' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNIT_HAS_FP =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 8 and tu.UNIT_KIND = 704 and tu.HAS_FP = 'Y' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNIT_HAS_FP =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 4 and tu.UNIT_KIND = 704 and tu.HAS_FP = 'Y' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNIT_HAS_FP =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 5 and tu.UNIT_KIND = 704 and tu.HAS_FP = 'Y' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0
end

if (@E_NEO = 1)			-- Количество необнаруженных единиц хранения
begin
	update R set R.UNITS_NOT_FOUND =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 1 and tu.UNIT_KIND = 703 and tu.IS_IN_SEARCH = 'Y' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 1 and R.CARRIER_TYPE = 'P'
	and /*where*/ and I.Deleted = 0

	update R set R.UNITS_NOT_FOUND =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 2 and tu.UNIT_KIND = 703 and tu.IS_IN_SEARCH = 'Y' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 2 and R.CARRIER_TYPE = 'P'
	and /*where*/ and I.Deleted = 0

	update R set R.UNITS_NOT_FOUND =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 3 and tu.UNIT_KIND = 703 and tu.IS_IN_SEARCH = 'Y' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 3 and R.CARRIER_TYPE = 'P'
	and /*where*/ and I.Deleted = 0

	update R set R.UNITS_NOT_FOUND =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 4 and tu.UNIT_KIND = 703 and tu.IS_IN_SEARCH = 'Y' and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'P'
	and /*where*/ and I.Deleted = 0

	update R set R.UNITS_NOT_FOUND =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 5 and tu.UNIT_KIND = 703 and tu.IS_IN_SEARCH = 'Y' and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.UNITS_NOT_FOUND =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 6 and tu.UNIT_KIND = 703 and tu.IS_IN_SEARCH = 'Y' and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.UNITS_NOT_FOUND =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 7 and tu.UNIT_KIND = 703 and tu.IS_IN_SEARCH = 'Y' and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.UNITS_NOT_FOUND =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 8 and tu.UNIT_KIND = 703 and tu.IS_IN_SEARCH = 'Y' and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.UNITS_NOT_FOUND =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 9 and tu.UNIT_KIND = 703 and tu.IS_IN_SEARCH = 'Y' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 9 and R.CARRIER_TYPE = 'M'
	and /*where*/ and I.Deleted = 0

	update R set R.UNITS_NOT_FOUND =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 5 and tu.UNIT_KIND = 703 and tu.IS_IN_SEARCH = 'Y' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.UNITS_NOT_FOUND =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 6 and tu.UNIT_KIND = 703 and tu.IS_IN_SEARCH = 'Y' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.UNITS_NOT_FOUND =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 7 and tu.UNIT_KIND = 703 and tu.IS_IN_SEARCH = 'Y' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.UNITS_NOT_FOUND =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 8 and tu.UNIT_KIND = 703 and tu.IS_IN_SEARCH = 'Y' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.UNITS_NOT_FOUND =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 4 and tu.UNIT_KIND = 703 and tu.IS_IN_SEARCH = 'Y' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0
end

if (@U_NEO = 1)			-- Количество необнаруженных единиц учета
begin
	update R set R.REG_UNITS_NOT_FOUND =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 6 and tu.UNIT_KIND = 704 and tu.IS_IN_SEARCH = 'Y' and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNITS_NOT_FOUND =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 7 and tu.UNIT_KIND = 704 and tu.IS_IN_SEARCH = 'Y' and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNITS_NOT_FOUND =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 8 and tu.UNIT_KIND = 704 and tu.IS_IN_SEARCH = 'Y' and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNITS_NOT_FOUND =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 6 and tu.UNIT_KIND = 704 and tu.IS_IN_SEARCH = 'Y' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNITS_NOT_FOUND =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 7 and tu.UNIT_KIND = 704 and tu.IS_IN_SEARCH = 'Y' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNITS_NOT_FOUND =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 8 and tu.UNIT_KIND = 704 and tu.IS_IN_SEARCH = 'Y' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNITS_NOT_FOUND =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 4 and tu.UNIT_KIND = 704 and tu.IS_IN_SEARCH = 'Y' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNITS_NOT_FOUND =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 5 and tu.UNIT_KIND = 704 and tu.IS_IN_SEARCH = 'Y' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0
end

if (@E_SEC = 1)			-- Количество секретных единиц хранения
begin
	update R set R.SECRET_UNITS =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 1 and tu.UNIT_KIND = 703 and tu.ISN_SECURLEVEL=2 and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 1 and R.CARRIER_TYPE = 'P'
	and /*where*/ and I.Deleted = 0

	update R set R.SECRET_UNITS =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 2 and tu.UNIT_KIND = 703 and tu.ISN_SECURLEVEL=2 and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 2 and R.CARRIER_TYPE = 'P'
	and /*where*/ and I.Deleted = 0

	update R set R.SECRET_UNITS =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 4 and tu.UNIT_KIND = 703 and tu.ISN_SECURLEVEL=2 and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'P'
	and /*where*/ and I.Deleted = 0

	update R set R.SECRET_UNITS =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 5 and tu.UNIT_KIND = 703 and tu.ISN_SECURLEVEL=2 and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.SECRET_UNITS =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 6 and tu.UNIT_KIND = 703 and tu.ISN_SECURLEVEL=2 and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.SECRET_UNITS =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 7 and tu.UNIT_KIND = 703 and tu.ISN_SECURLEVEL=2 and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.SECRET_UNITS =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 8 and tu.UNIT_KIND = 703 and tu.ISN_SECURLEVEL=2 and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.SECRET_UNITS =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 5 and tu.UNIT_KIND = 703 and tu.ISN_SECURLEVEL=2 and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.SECRET_UNITS =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 6 and tu.UNIT_KIND = 703 and tu.ISN_SECURLEVEL=2 and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.SECRET_UNITS =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 7 and tu.UNIT_KIND = 703 and tu.ISN_SECURLEVEL=2 and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.SECRET_UNITS =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 8 and tu.UNIT_KIND = 703 and tu.ISN_SECURLEVEL=2 and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.SECRET_UNITS =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 4 and tu.UNIT_KIND = 703 and tu.ISN_SECURLEVEL=2 and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0
end

if (@U_SEC = 1)			-- Количество секретных единиц учета
begin
	update R set R.SECRET_REG_UNITS =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 6 and tu.UNIT_KIND = 704 
        and tu.ISN_SECURLEVEL IN (SELECT ISN_SECURLEVEL FROM  tblSECURLEVEL  WHERE NAME='с') 
        and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.SECRET_REG_UNITS =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 7 and tu.UNIT_KIND = 704  
        and tu.ISN_SECURLEVEL IN (SELECT ISN_SECURLEVEL FROM  tblSECURLEVEL  WHERE NAME='с')  
        and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.SECRET_REG_UNITS =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 8 and tu.UNIT_KIND = 704 
        and tu.ISN_SECURLEVEL IN (SELECT ISN_SECURLEVEL FROM  tblSECURLEVEL  WHERE NAME='с')  
        and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.SECRET_REG_UNITS =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 6 and tu.UNIT_KIND = 704 
        and tu.ISN_SECURLEVEL IN (SELECT ISN_SECURLEVEL FROM  tblSECURLEVEL  WHERE NAME='с') 
        and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.SECRET_REG_UNITS =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 7 and tu.UNIT_KIND = 704 
        and tu.ISN_SECURLEVEL IN (SELECT ISN_SECURLEVEL FROM  tblSECURLEVEL  WHERE NAME='с')  
        and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.SECRET_REG_UNITS =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 8 and tu.UNIT_KIND = 704 
        and tu.ISN_SECURLEVEL IN (SELECT ISN_SECURLEVEL FROM  tblSECURLEVEL  WHERE NAME='с')  
        and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.SECRET_REG_UNITS =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 4 and tu.UNIT_KIND = 704
        and tu.ISN_SECURLEVEL IN (SELECT ISN_SECURLEVEL FROM  tblSECURLEVEL  WHERE NAME='с') 
        and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.SECRET_REG_UNITS =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 5 and tu.UNIT_KIND = 704
        and tu.ISN_SECURLEVEL IN (SELECT ISN_SECURLEVEL FROM  tblSECURLEVEL  WHERE NAME='с') 
        and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0
end

if (@E_CAT = 1)			-- Количество закаталогизированных единиц хранения
begin
	update R set R.UNITS_CATALOGUED =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 1 and tu.UNIT_KIND = 703 and tu.CATALOGUED = 'Y' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 1 and R.CARRIER_TYPE = 'P'
	and /*where*/ and I.Deleted = 0

	update R set R.UNITS_CATALOGUED =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 2 and tu.UNIT_KIND = 703 and tu.CATALOGUED = 'Y' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 2 and R.CARRIER_TYPE = 'P'
	and /*where*/ and I.Deleted = 0

	update R set R.UNITS_CATALOGUED =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 3 and tu.UNIT_KIND = 703 and tu.CATALOGUED = 'Y' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 3 and R.CARRIER_TYPE = 'P'
	and /*where*/ and I.Deleted = 0

	update R set R.UNITS_CATALOGUED =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 4 and tu.UNIT_KIND = 703 and tu.CATALOGUED = 'Y' and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'P'
	and /*where*/ and I.Deleted = 0

	update R set R.UNITS_CATALOGUED =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 5 and tu.UNIT_KIND = 703 and tu.CATALOGUED = 'Y' and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.UNITS_CATALOGUED =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 9 and tu.UNIT_KIND = 703 and tu.CATALOGUED = 'Y' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 9 and R.CARRIER_TYPE = 'M'
	and /*where*/ and I.Deleted = 0

	update R set R.UNITS_CATALOGUED =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 5 and tu.UNIT_KIND = 703 and tu.CATALOGUED = 'Y' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.UNITS_CATALOGUED =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 6 and tu.UNIT_KIND = 703 and tu.CATALOGUED = 'Y' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.UNITS_CATALOGUED =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 7 and tu.UNIT_KIND = 703 and tu.CATALOGUED = 'Y' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.UNITS_CATALOGUED =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 8 and tu.UNIT_KIND = 703 and tu.CATALOGUED = 'Y' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.UNITS_CATALOGUED =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 4 and tu.UNIT_KIND = 703 and tu.CATALOGUED = 'Y' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0
end

if (@U_CAT = 1)			-- Количество закаталогизированных единиц учета
begin
	update R set R.REG_UNITS_CTALOGUE =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 6 and tu.UNIT_KIND = 704 and tu.CATALOGUED = 'Y' and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNITS_CTALOGUE =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 7 and tu.UNIT_KIND = 704 and tu.CATALOGUED = 'Y' and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNITS_CTALOGUE =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 8 and tu.UNIT_KIND = 704 and tu.CATALOGUED = 'Y' and tu.MEDIUM_TYPE = 'T' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNITS_CTALOGUE =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 6 and tu.UNIT_KIND = 704 and tu.CATALOGUED = 'Y' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNITS_CTALOGUE =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 7 and tu.UNIT_KIND = 704 and tu.CATALOGUED = 'Y' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNITS_CTALOGUE =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 8 and tu.UNIT_KIND = 704 and tu.CATALOGUED = 'Y' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNITS_CTALOGUE =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 4 and tu.UNIT_KIND = 704 and tu.CATALOGUED = 'Y' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

	update R set R.REG_UNITS_CTALOGUE =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_DOC_TYPE = 5 and tu.UNIT_KIND = 704 and tu.CATALOGUED = 'Y' and tu.MEDIUM_TYPE = 'E' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0
end

----

if (@E_KARTED = 1)		-- Количество закартонированных единиц хранения
	update R set R.CARDBOARDED =
		(select COUNT(tu.ID) from tblUNIT tu
		where tu.UNIT_KIND = 703 and tu.CARDBOARDED = 'Y' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblINVENTORY_CHECK R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where /*where*/ and I.Deleted = 0

if (@E_SHIFR = 1)		-- Количество единиц хранения, требующих шифровки
	update R set R.UNITS_NEED_ENCIPHERING =
		(select COUNT(tu.ID) from tblUNIT tu, tblUNIT_REQUIRED_WORK tr
		where tr.ISN_WORK = 7 and tr.IS_ACTUAL = 'Y' and tu.UNIT_KIND = 703 and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tr.ISN_UNIT = tu.ISN_UNIT and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblINVENTORY_CHECK R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where /*where*/ and I.Deleted = 0

if (@E_KART = 1)		-- Количество единиц хранения, требующих картонирования
	update R set R.UNITS_NEED_CARDBOARDED =
		(select COUNT(tu.ID) from tblUNIT tu, tblUNIT_REQUIRED_WORK tr
		where tr.ISN_WORK = 9 and tr.IS_ACTUAL = 'Y' and tu.UNIT_KIND = 703 and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tr.ISN_UNIT = tu.ISN_UNIT and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblINVENTORY_CHECK R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where /*where*/ and I.Deleted = 0

if (@E_ZAM_OBL = 1)		-- Количество единиц хранения, требующих замены обложки
	update R set R.UNITS_NEED_COVER_CHANGE =
		(select COUNT(tu.ID) from tblUNIT tu, tblUNIT_REQUIRED_WORK tr
		where tr.ISN_WORK = 8 and tr.IS_ACTUAL = 'Y' and tu.UNIT_KIND = 703 and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tr.ISN_UNIT = tu.ISN_UNIT and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblINVENTORY_CHECK R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where /*where*/ and I.Deleted = 0

if (@E_POVR = 1)		-- Количество неисправимо повреждённых единиц хранения
begin
	update R set R.UNITS_DBR =
		(select COUNT(tu.ID) from tblUNIT tu, tblREF_FEATURE tr, tblFEATURE tf
		where tu.UNIT_KIND = 703 and tr.KIND = 703 and tf.NAME = 'неисправимо поврежденные' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tr.ISN_OBJ = tu.ISN_UNIT and tr.ISN_FEATURE = tf.ISN_FEATURE
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0 and tf.Deleted = 0)
	from tblINVENTORY_CHECK R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where /*where*/ and I.Deleted = 0
end

if (@E_ZAT = 1)			-- Количество листов с затухающими текстами
begin
	update R set R.FADING_PAGES =
		(select coalesce(SUM(ts.PAGE_COUNT),0) from tblUNIT tu, tblUNIT_STATE ts, tblSTATE_CL tc
		where tu.UNIT_KIND = 703 and (tc.NAME = 'с повреждениями текста' or tc.ISN_HIGH_STATE =(select ISN_STATE from tblSTATE_CL where NAME = 'с повреждениями текста')) 
        and ts.IS_ACTUAL = 'Y' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tu.ISN_UNIT = ts.ISN_UNIT and ts.ISN_STATE = tc.ISN_STATE
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0 and tc.Deleted = 0)
	from tblINVENTORY_CHECK R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where /*where*/ and I.Deleted = 0
end

if (@E_RESTAVR = 1)		--кол-во ед.хр. требующих реставрации
	update R set R.UNITS_NEED_RESTORATION =
		(select COUNT(tu.ID) from tblUNIT tu, tblUNIT_REQUIRED_WORK tr
		where tr.ISN_WORK = 1 and tr.IS_ACTUAL = 'Y' and tu.UNIT_KIND = 703 and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tr.ISN_UNIT = tu.ISN_UNIT and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblINVENTORY_CHECK R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where /*where*/ and I.Deleted = 0

if (@E_GOR = 1)			-- Количество единиц хранения на горючей основе
	update R set R.UNITS_INFLAMMABLE =
		(select COUNT(tu.ID) from tblUNIT tu, tblREF_FEATURE tr, tblFEATURE tf
		where tu.UNIT_KIND = 703 and tr.KIND = 703 and tf.NAME = 'на горючей основе' and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tr.ISN_OBJ = tu.ISN_UNIT and tr.ISN_FEATURE = tf.ISN_FEATURE
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0 and tf.Deleted = 0)
	from tblINVENTORY_CHECK R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where /*where*/ and I.Deleted = 0

if (@E_PODSH = 1)		-- Количество единиц хранения, требующих подшивки или переплета
	update R set R.UNITS_NEED_BINDING =
		(select COUNT(tu.ID) from tblUNIT tu, tblUNIT_REQUIRED_WORK tr
		where tr.ISN_WORK = 2 and tr.IS_ACTUAL = 'Y' and tu.UNIT_KIND = 703 and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tr.ISN_UNIT = tu.ISN_UNIT and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblINVENTORY_CHECK R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where /*where*/ and I.Deleted = 0

if (@E_KPO = 1)			-- Количество единиц хранения, требующих КПО
	update R set R.UNITS_NEED_KPO =
		(select COUNT(tu.ID) from tblUNIT tu, tblUNIT_REQUIRED_WORK tr
		where tr.ISN_WORK = 5 and tr.IS_ACTUAL = 'Y' and tu.UNIT_KIND = 703 and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tr.ISN_UNIT = tu.ISN_UNIT and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblINVENTORY_CHECK R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where /*where*/ and I.Deleted = 0

if (@E_DEZINF = 1)		-- Количество единиц хранения, требующих дезинфекции
	update R set R.UNITS_NEED_DISINFECTION =
		(select COUNT(tu.ID) from tblUNIT tu, tblUNIT_REQUIRED_WORK tr
		where tr.ISN_WORK = 3 and tr.IS_ACTUAL = 'Y' and tu.UNIT_KIND = 703 and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tr.ISN_UNIT = tu.ISN_UNIT and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblINVENTORY_CHECK R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where /*where*/ and I.Deleted = 0

if (@E_DEZINS = 1)		-- Количество единиц хранения, требующих дезинсекции
	update R set R.UNITS_NEED_DISINSECTION =
		(select COUNT(tu.ID) from tblUNIT tu, tblUNIT_REQUIRED_WORK tr
		where tr.ISN_WORK = 4 and tr.IS_ACTUAL = 'Y' and tu.UNIT_KIND = 703 and ((tu.IS_LOST IS NULL) OR (tu.IS_LOST='N') OR (tu.IS_LOST=''))
		and tr.ISN_UNIT = tu.ISN_UNIT and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0)
	from tblINVENTORY_CHECK R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where /*where*/ and I.Deleted = 0

if (@E_RAB = 1)			-- Работы, проводимые с документами описи
	update R set R.INVENTORY_DOC_WORK =
		(select convert(nvarchar(20), tw.WORK_DATE, 104) + ' ' + tc.NAME + CHAR(13) + CHAR(10) as [text()]
		from tblUNIT tu, tblUNIT_WORK tw, tblWORK_CL tc
		where tu.UNIT_KIND = 703
		and tu.ISN_UNIT = tw.ISN_UNIT and tw.ISN_WORK = tc.ISN_WORK
		and tu.ISN_INVENTORY = R.ISN_INVENTORY and tu.Deleted = 0 and tc.Deleted = 0
		for xml path (''), type).value('.[1]','nvarchar(max)')
	from tblINVENTORY R
	where /*where*/ and R.Deleted = 0

----

if (@E_TOPO = 1)		-- Топография и номера единиц хранения
begin
declare @max_num bigint
    set @max_num = (select coalesce(MAX(ISN_REF_LOCATION),0) + 1 from tblREF_LOCATION)

delete tblREF_LOCATION
    where tblREF_LOCATION.ISN_OBJ in (
	    select R.ISN_INVENTORY from tblINVENTORY R where 1=1 and R.Deleted = 0) and tblREF_LOCATION.KIND = 702  

insert into tblREF_LOCATION (ID, OwnerID, CreationDateTime, DocID, RowID,
							     ISN_REF_LOCATION, ISN_LOCATION, ISN_OBJ, UNIT_NUM_FROM, UNIT_NUM_TO, KIND, UNIT_NUM_OLD_FROM,UNIT_NUM_OLD_TO)
select 
	    newid(), '12345678-9012-3456-7890-123456789012', getdate(), T.ID 
	    ,row_number() over (order by CAST(T.MIN_ as INTEGER)) 
	    ,row_number() over (order by CAST(T.MIN_ as INTEGER)) + @max_num,
	     T.ISN_LOCATION, T.ISN_INVENTORY, T.MIN_, T.MAX_, T.KIND_,T.OLD_MIN , T.OLD_MAX 
from (
	select distinct
		ISN_LOCATION,
		D.ISN_INVENTORY ISN_INVENTORY,
		INVENTORY.ID ID,
		MIN_,
		MAX_,
		702 KIND_,
		null OLD_MIN,
		null OLD_MAX
	from (
	--таблица для номеров в новом формате
		SELECT ISN_LOCATION ISN_LOCATION, ISN_INVENTORY, MIN(CAST(C.u as int)) MIN_,MAX(CAST(C.u as int)) MAX_, C.group_index
			FROM (
				SELECT ISN_LOCATION, ISN_INVENTORY, B.UNIT_NUM_1 as u,NULL as old_num_1,NULL as old_num_2,(B.UNIT_NUM_1 - row_number() OVER(ORDER BY ISN_INVENTORY, ISN_LOCATION, CAST( B.UNIT_NUM_1 AS INTEGER)))  group_index
				FROM
				(
                    SELECT ISN_LOCATION, ISN_INVENTORY,UNIT_NUM_1, UNIT_NUM_TXT 
					FROM tblUNIT UNIT
					WHERE UNIT.UNIT_KIND = 703 and UNIT.ISN_LOCATION is not null and UNIT.Deleted = 0
				) B --первоначальная информация по номерам в новом формате
				WHERE (NOT (UNIT_NUM_1='') AND UNIT_NUM_1 IS NOT NULL) AND NOT (NOT(UNIT_NUM_TXT='') AND UNIT_NUM_TXT IS NOT NULL)
			) C --вычисляется индекс группировки по непрерывным последовательностям
			GROUP BY C.group_index,ISN_LOCATION,ISN_INVENTORY
		) D --ваыводим начало и конец последовательности
	JOIN (select * from tblINVENTORY R where 1=1 and R.Deleted = 0) INVENTORY
		on D.ISN_INVENTORY = INVENTORY.ISN_INVENTORY

	union
	--таблица для номеров в старом формате (подозреваю, что будут возникать баги при сортировек строк MIN(old_num_1+old_num_2_min) OLD_MIN)
	select distinct
			ISN_LOCATION,
			D.ISN_INVENTORY ISN_INVENTORY,
			INVENTORY.ID ID,
			null MIN_,
			null MAX_,
			702 KIND_,
			OLD_MIN,
			OLD_MAX
	from (
		SELECT ISN_LOCATION, ISN_INVENTORY
			--,MIN(CAST(old_num_1 as int)) MIN_
			,MIN(old_num_1+old_num_2_min) OLD_MIN
			--,MAX(CAST(old_num_1 as int)) MAX_
			,MAX(old_num_1+old_num_2_max) OLD_MAX
			--,group_index
		FROM (
		-----
			SELECT ISN_LOCATION, ISN_INVENTORY, null as u, old_num_1, MIN(old_num_2) old_num_2_min, MAX(old_num_2) old_num_2_max
				,(old_num_1-row_number() OVER(ORDER BY ISN_INVENTORY, ISN_LOCATION, old_num_1))  group_index

			FROM (
				SELECT ISN_LOCATION, ISN_INVENTORY--, null as u
					, SUBSTRING(UNIT_NUM_TXT,1,PATINDEX('%[^0-9]%',UNIT_NUM_TXT)-1) as old_num_1
					, SUBSTRING(UNIT_NUM_TXT,PATINDEX('%[^0-9]%',UNIT_NUM_TXT),LEN(UNIT_NUM_TXT)) as old_num_2
				FROM
				(
					SELECT ISN_LOCATION, ISN_INVENTORY,UNIT_NUM_1, UNIT_NUM_TXT
					FROM tblUNIT UNIT
					WHERE UNIT.UNIT_KIND = 703 and UNIT.ISN_LOCATION is not null and UNIT.Deleted = 0
				) B --первоначальная информация по номерам в новом формате
				WHERE (NOT (UNIT_NUM_TXT='') AND UNIT_NUM_TXT IS NOT NULL) AND NOT (NOT(UNIT_NUM_1='') AND UNIT_NUM_1 IS NOT NULL)
			) BB
			GROUP BY ISN_LOCATION, ISN_INVENTORY,old_num_1
		-----
		) C
		GROUP BY C.group_index,ISN_LOCATION,ISN_INVENTORY
	) D --ваыводим начало и конец последовательности
	JOIN (select * from tblINVENTORY R where 1=1 and R.Deleted = 0) INVENTORY
			on D.ISN_INVENTORY = INVENTORY.ISN_INVENTORY
) T
end

----

if (@SUM_BO = 1)		-- Суммарные количественные характеристики документов на бумажной основе
	update R set
	R.UNIT_COUNT = (
		select coalesce(SUM(td.UNIT_COUNT),0) from tblDOCUMENT_STATS td
		where td.ISN_DOC_TYPE in (1, 2, 3, 4) and td.CARRIER_TYPE = 'P' and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.UNIT_REGISTERED = (
		select coalesce(SUM(td.UNIT_REGISTERED),0) from tblDOCUMENT_STATS td
		where td.ISN_DOC_TYPE in (1, 2, 3, 4) and td.CARRIER_TYPE = 'P' and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.UNIT_OC_COUNT = (
		select coalesce(SUM(td.UNIT_OC_COUNT),0) from tblDOCUMENT_STATS td
		where td.ISN_DOC_TYPE in (1, 2, 3, 4) and td.CARRIER_TYPE = 'P' and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.UNITS_UNIQUE = (
		select coalesce(SUM(td.UNITS_UNIQUE),0) from tblDOCUMENT_STATS td
		where td.ISN_DOC_TYPE in (1, 2, 3, 4) and td.CARRIER_TYPE = 'P' and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.UNIT_HAS_SF = (
		select coalesce(SUM(td.UNIT_HAS_SF),0) from tblDOCUMENT_STATS td
		where td.ISN_DOC_TYPE in (1, 2, 3, 4) and td.CARRIER_TYPE = 'P' and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.UNIT_HAS_FP = (
		select coalesce(SUM(td.UNIT_HAS_FP),0) from tblDOCUMENT_STATS td
		where td.ISN_DOC_TYPE in (1, 2, 3, 4) and td.CARRIER_TYPE = 'P' and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.SECRET_UNITS = (
		select coalesce(SUM(td.SECRET_UNITS),0) from tblDOCUMENT_STATS td
		where td.ISN_DOC_TYPE in (1, 2, 3, 4) and td.CARRIER_TYPE = 'P' and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.UNITS_NOT_FOUND = (
		select coalesce(SUM(td.UNITS_NOT_FOUND),0) from tblDOCUMENT_STATS td
		where td.ISN_DOC_TYPE in (1, 2, 3, 4) and td.CARRIER_TYPE = 'P' and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.UNITS_CATALOGUED = (
		select coalesce(SUM(td.UNITS_CATALOGUED),0) from tblDOCUMENT_STATS td
		where td.ISN_DOC_TYPE in (1, 2, 3, 4) and td.CARRIER_TYPE = 'P' and td.ISN_INVENTORY = R.ISN_INVENTORY)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE is NULL and R.CARRIER_TYPE = 'P'
	and /*where*/ and I.Deleted = 0

if (@SUM_TN = 1)		-- Суммарные количественные характеристики аудиовизуальных документов на традиционных носителях
	update R set
	R.UNIT_COUNT = (
		select coalesce(SUM(td.UNIT_COUNT),0) from tblDOCUMENT_STATS td
		where td.ISN_DOC_TYPE in (5, 6, 7, 8) and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.UNIT_REGISTERED = (
		select coalesce(SUM(td.UNIT_REGISTERED),0) from tblDOCUMENT_STATS td
		where td.ISN_DOC_TYPE in (5, 6, 7, 8) and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.UNIT_OC_COUNT = (
		select coalesce(SUM(td.UNIT_OC_COUNT),0) from tblDOCUMENT_STATS td
		where td.ISN_DOC_TYPE in (5, 6, 7, 8) and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.UNITS_UNIQUE = (
		select coalesce(SUM(td.UNITS_UNIQUE),0) from tblDOCUMENT_STATS td
		where td.ISN_DOC_TYPE in (5, 6, 7, 8) and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.UNIT_HAS_SF = (
		select coalesce(SUM(td.UNIT_HAS_SF),0) from tblDOCUMENT_STATS td
		where td.ISN_DOC_TYPE in (5, 6, 7, 8) and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.UNIT_HAS_FP = (
		select coalesce(SUM(td.UNIT_HAS_FP),0) from tblDOCUMENT_STATS td
		where td.ISN_DOC_TYPE in (5, 6, 7, 8) and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.SECRET_UNITS = (
		select coalesce(SUM(td.SECRET_UNITS),0) from tblDOCUMENT_STATS td
		where td.ISN_DOC_TYPE in (5, 6, 7, 8) and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.UNITS_NOT_FOUND = (
		select coalesce(SUM(td.UNITS_NOT_FOUND),0) from tblDOCUMENT_STATS td
		where td.ISN_DOC_TYPE in (5, 6, 7, 8) and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.UNITS_CATALOGUED = (
		select coalesce(SUM(td.UNITS_CATALOGUED),0) from tblDOCUMENT_STATS td
		where td.ISN_DOC_TYPE in (5, 6, 7, 8) and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.REG_UNIT = (
		select coalesce(SUM(td.REG_UNIT),0) from tblDOCUMENT_STATS td
		where td.ISN_DOC_TYPE in (5, 6, 7, 8) and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.REG_UNIT_REGISTERED = (
		select coalesce(SUM(td.REG_UNIT_REGISTERED),0) from tblDOCUMENT_STATS td
		where td.ISN_DOC_TYPE in (5, 6, 7, 8) and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.REG_UNIT_OC = (
		select coalesce(SUM(td.REG_UNIT_OC),0) from tblDOCUMENT_STATS td
		where td.ISN_DOC_TYPE in (5, 6, 7, 8) and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.REG_UNITS_UNIQUE = (
		select coalesce(SUM(td.REG_UNITS_UNIQUE),0) from tblDOCUMENT_STATS td
		where td.ISN_DOC_TYPE in (5, 6, 7, 8) and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.REG_UNIT_HAS_SF = (
		select coalesce(SUM(td.REG_UNIT_HAS_SF),0) from tblDOCUMENT_STATS td
		where td.ISN_DOC_TYPE in (5, 6, 7, 8) and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.REG_UNIT_HAS_FP = (
		select coalesce(SUM(td.REG_UNIT_HAS_FP),0) from tblDOCUMENT_STATS td
		where td.ISN_DOC_TYPE in (5, 6, 7, 8) and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.SECRET_REG_UNITS = (
		select coalesce(SUM(td.SECRET_REG_UNITS),0) from tblDOCUMENT_STATS td
		where td.ISN_DOC_TYPE in (5, 6, 7, 8) and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.REG_UNITS_NOT_FOUND = (
		select coalesce(SUM(td.REG_UNITS_NOT_FOUND),0) from tblDOCUMENT_STATS td
		where td.ISN_DOC_TYPE in (5, 6, 7, 8) and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.REG_UNITS_CTALOGUE = (
		select coalesce(SUM(td.REG_UNITS_CTALOGUE),0) from tblDOCUMENT_STATS td
		where td.ISN_DOC_TYPE in (5, 6, 7, 8) and td.CARRIER_TYPE = 'A' and td.ISN_INVENTORY = R.ISN_INVENTORY)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE is NULL and R.CARRIER_TYPE = 'A'
	and /*where*/ and I.Deleted = 0

if (@SUM_EN = 1)		-- Суммарные количественные характеристики документов на электронных носителях
	update R set
	R.UNIT_COUNT = (
		select coalesce(SUM(td.UNIT_COUNT),0) from tblDOCUMENT_STATS td
		where td.ISN_DOC_TYPE in (4, 5, 6, 7, 8) and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.UNIT_REGISTERED = (
		select coalesce(SUM(td.UNIT_REGISTERED),0) from tblDOCUMENT_STATS td
		where td.ISN_DOC_TYPE in (4, 5, 6, 7, 8) and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.UNIT_OC_COUNT = (
		select coalesce(SUM(td.UNIT_OC_COUNT),0) from tblDOCUMENT_STATS td
		where td.ISN_DOC_TYPE in (4, 5, 6, 7, 8) and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.UNITS_UNIQUE = (
		select coalesce(SUM(td.UNITS_UNIQUE),0) from tblDOCUMENT_STATS td
		where td.ISN_DOC_TYPE in (4, 5, 6, 7, 8) and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.UNIT_HAS_SF = (
		select coalesce(SUM(td.UNIT_HAS_SF),0) from tblDOCUMENT_STATS td
		where td.ISN_DOC_TYPE in (4, 5, 6, 7, 8) and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.UNIT_HAS_FP = (
		select coalesce(SUM(td.UNIT_HAS_FP),0) from tblDOCUMENT_STATS td
		where td.ISN_DOC_TYPE in (4, 5, 6, 7, 8) and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.SECRET_UNITS = (
		select coalesce(SUM(td.SECRET_UNITS),0) from tblDOCUMENT_STATS td
		where td.ISN_DOC_TYPE in (4, 5, 6, 7, 8) and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.UNITS_NOT_FOUND = (
		select coalesce(SUM(td.UNITS_NOT_FOUND),0) from tblDOCUMENT_STATS td
		where td.ISN_DOC_TYPE in (4, 5, 6, 7, 8) and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.UNITS_CATALOGUED = (
		select coalesce(SUM(td.UNITS_CATALOGUED),0) from tblDOCUMENT_STATS td
		where td.ISN_DOC_TYPE in (4, 5, 6, 7, 8) and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.REG_UNIT = (
		select coalesce(SUM(td.REG_UNIT),0) from tblDOCUMENT_STATS td
		where td.ISN_DOC_TYPE in (4, 5, 6, 7, 8) and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.REG_UNIT_REGISTERED = (
		select coalesce(SUM(td.REG_UNIT_REGISTERED),0) from tblDOCUMENT_STATS td
		where td.ISN_DOC_TYPE in (4, 5, 6, 7, 8) and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.REG_UNIT_OC = (
		select coalesce(SUM(td.REG_UNIT_OC),0) from tblDOCUMENT_STATS td
		where td.ISN_DOC_TYPE in (4, 5, 6, 7, 8) and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.REG_UNITS_UNIQUE = (
		select coalesce(SUM(td.REG_UNITS_UNIQUE),0) from tblDOCUMENT_STATS td
		where td.ISN_DOC_TYPE in (4, 5, 6, 7, 8) and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.REG_UNIT_HAS_SF = (
		select coalesce(SUM(td.REG_UNIT_HAS_SF),0) from tblDOCUMENT_STATS td
		where td.ISN_DOC_TYPE in (4, 5, 6, 7, 8) and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.REG_UNIT_HAS_FP = (
		select coalesce(SUM(td.REG_UNIT_HAS_FP),0) from tblDOCUMENT_STATS td
		where td.ISN_DOC_TYPE in (4, 5, 6, 7, 8) and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.SECRET_REG_UNITS = (
		select coalesce(SUM(td.SECRET_REG_UNITS),0) from tblDOCUMENT_STATS td
		where td.ISN_DOC_TYPE in (4, 5, 6, 7, 8) and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.REG_UNITS_NOT_FOUND = (
		select coalesce(SUM(td.REG_UNITS_NOT_FOUND),0) from tblDOCUMENT_STATS td
		where td.ISN_DOC_TYPE in (4, 5, 6, 7, 8) and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.REG_UNITS_CTALOGUE = (
		select coalesce(SUM(td.REG_UNITS_CTALOGUE),0) from tblDOCUMENT_STATS td
		where td.ISN_DOC_TYPE in (4, 5, 6, 7, 8) and td.CARRIER_TYPE = 'E' and td.ISN_INVENTORY = R.ISN_INVENTORY)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE is NULL and R.CARRIER_TYPE = 'E'
	and /*where*/ and I.Deleted = 0

if (@SUM_AA = 1)		-- Суммарные количественные характеристики всех типов документации на всех носителях
	update R set
	R.UNIT_COUNT = (
		select coalesce(SUM(UNIT_COUNT),0) from tblDOCUMENT_STATS td
		where (td.ISN_DOC_TYPE is NULL or td.ISN_DOC_TYPE = 9)
		and td.CARRIER_TYPE in ('P', 'A', 'M', 'E') and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.UNIT_REGISTERED = (
		select coalesce(SUM(UNIT_REGISTERED),0) from tblDOCUMENT_STATS td
		where (td.ISN_DOC_TYPE is NULL or td.ISN_DOC_TYPE = 9) 
		and td.CARRIER_TYPE in ('P', 'A', 'M', 'E') and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.UNIT_OC_COUNT = (
		select coalesce(SUM(UNIT_OC_COUNT),0) from tblDOCUMENT_STATS td
		where (td.ISN_DOC_TYPE is NULL or td.ISN_DOC_TYPE = 9) 
		and td.CARRIER_TYPE in ('P', 'A', 'M', 'E') and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.UNITS_UNIQUE = (
		select coalesce(SUM(UNITS_UNIQUE),0) from tblDOCUMENT_STATS td
		where (td.ISN_DOC_TYPE is NULL or td.ISN_DOC_TYPE = 9) 
		and td.CARRIER_TYPE in ('P', 'A', 'M', 'E') and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.UNIT_HAS_SF = (
		select coalesce(SUM(UNIT_HAS_SF),0) from tblDOCUMENT_STATS td
		where (td.ISN_DOC_TYPE is NULL or td.ISN_DOC_TYPE = 9) 
		and td.CARRIER_TYPE in ('P', 'A', 'M', 'E') and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.UNIT_HAS_FP = (
		select coalesce(SUM(UNIT_HAS_FP),0) from tblDOCUMENT_STATS td
		where (td.ISN_DOC_TYPE is NULL or td.ISN_DOC_TYPE = 9) 
		and td.CARRIER_TYPE in ('P', 'A', 'M', 'E') and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.SECRET_UNITS = (
		select coalesce(SUM(SECRET_UNITS),0) from tblDOCUMENT_STATS td
		where (td.ISN_DOC_TYPE is NULL or td.ISN_DOC_TYPE = 9)
		and td.CARRIER_TYPE in ('P', 'A', 'M', 'E') and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.UNITS_NOT_FOUND = (
		select coalesce(SUM(UNITS_NOT_FOUND),0) from tblDOCUMENT_STATS td
		where (td.ISN_DOC_TYPE is NULL or td.ISN_DOC_TYPE = 9) 
		and td.CARRIER_TYPE in ('P', 'A', 'M', 'E') and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.UNITS_CATALOGUED = (
		select coalesce(SUM(UNITS_CATALOGUED),0) from tblDOCUMENT_STATS td
		where (td.ISN_DOC_TYPE is NULL or td.ISN_DOC_TYPE = 9) 
		and td.CARRIER_TYPE in ('P', 'A', 'M', 'E') and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.REG_UNIT = (
		select coalesce(SUM(REG_UNIT),0) from tblDOCUMENT_STATS td
		where (td.ISN_DOC_TYPE is NULL or td.ISN_DOC_TYPE = 9) 
		and td.CARRIER_TYPE in ('A', 'E') and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.REG_UNIT_REGISTERED = (
		select coalesce(SUM(REG_UNIT_REGISTERED),0) from tblDOCUMENT_STATS td
		where (td.ISN_DOC_TYPE is NULL or td.ISN_DOC_TYPE = 9) 
		and td.CARRIER_TYPE in ('A', 'E') and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.REG_UNIT_OC = (
		select coalesce(SUM(REG_UNIT_OC),0) from tblDOCUMENT_STATS td
		where (td.ISN_DOC_TYPE is NULL or td.ISN_DOC_TYPE = 9) 
		and td.CARRIER_TYPE in ('A', 'E') and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.REG_UNITS_UNIQUE = (
		select coalesce(SUM(REG_UNITS_UNIQUE),0) from tblDOCUMENT_STATS td
		where (td.ISN_DOC_TYPE is NULL or td.ISN_DOC_TYPE = 9) 
		and td.CARRIER_TYPE in ('A', 'E') and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.REG_UNIT_HAS_SF = (
		select coalesce(SUM(REG_UNIT_HAS_SF),0) from tblDOCUMENT_STATS td
		where (td.ISN_DOC_TYPE is NULL or td.ISN_DOC_TYPE = 9) 
		and td.CARRIER_TYPE in ('A', 'E') and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.REG_UNIT_HAS_FP = (
		select coalesce(SUM(REG_UNIT_HAS_FP),0) from tblDOCUMENT_STATS td
		where (td.ISN_DOC_TYPE is NULL or td.ISN_DOC_TYPE = 9) 
		and td.CARRIER_TYPE in ('A', 'E') and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.SECRET_REG_UNITS = (
		select coalesce(SUM(SECRET_REG_UNITS),0) from tblDOCUMENT_STATS td
		where (td.ISN_DOC_TYPE is NULL or td.ISN_DOC_TYPE = 9) 
		and td.CARRIER_TYPE in ('A', 'E') and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.REG_UNITS_NOT_FOUND = (
		select coalesce(SUM(REG_UNITS_NOT_FOUND),0) from tblDOCUMENT_STATS td
		where (td.ISN_DOC_TYPE is NULL or td.ISN_DOC_TYPE = 9) 
		and td.CARRIER_TYPE in ('A', 'E') and td.ISN_INVENTORY = R.ISN_INVENTORY),
	R.REG_UNITS_CTALOGUE = (
		select coalesce(SUM(REG_UNITS_CTALOGUE),0) from tblDOCUMENT_STATS td
		where (td.ISN_DOC_TYPE is NULL or td.ISN_DOC_TYPE = 9) 
		and td.CARRIER_TYPE in ('A', 'E') and td.ISN_INVENTORY = R.ISN_INVENTORY)
	from tblDOCUMENT_STATS R join tblINVENTORY I on R.ISN_INVENTORY = I.ISN_INVENTORY
	where R.ISN_DOC_TYPE is NULL and R.CARRIER_TYPE is NULL
	and /*where*/ and I.Deleted = 0

                                         </sql>, _
                                        <sql>


-- Пересчет ед. хр/уч

-- замена параметров для тестирования запроса
/*
DECLARE @A_NACH_DATA bit = 1	-- начальная дата
DECLARE @A_KON_DATA bit = 1		-- конечная дата
DECLARE @A_LISTS bit = 1		-- листов 
DECLARE @A_EU bit = 1			-- кол-во ед хр/уч

DECLARE @K_NEG bit = 1			-- кол-во негативов
DECLARE @M_NEG bit = 1			-- метраж негативов
DECLARE @K_DNEG bit = 1			-- кол-во дубль-негативов (контратипов)
DECLARE @M_DNEG bit = 1			-- метраж дубль-негативов (контратипов)
DECLARE @K_FON bit = 1			-- кол-во фонограмм (негативов)
DECLARE @M_FON bit = 1			-- метраж фонограмм (негативов)
DECLARE @K_MFON bit = 1			-- кол-во магнитных фонограмм
DECLARE @M_MFON bit = 1			-- метраж магнитных фонограмм
DECLARE @K_MFONS bit = 1		-- кол-во магнитных фонограмм совмещен.
DECLARE @M_MFONS bit = 1		-- метраж магнитных фонограмм совмещен.
DECLARE @K_PPOS bit = 1			-- кол-во промежуточных позитивов
DECLARE @M_PPOS bit = 1			-- метраж промежуточных позитивов
DECLARE @K_POS bit = 1			-- кол-во позитивов
DECLARE @M_POS bit = 1			-- метраж позитивов

DECLARE @K_ORIG bit = 1			-- кол-во оригиналов
DECLARE @K_KOP bit = 1			-- кол-во копий

DECLARE @K_ORIGN bit = 1		-- кол-во оригиналов (негативов)
DECLARE @M_ORIGN bit = 1		-- метраж оригиналов (негативов)
DECLARE @K_KOPP bit = 1			-- кол-во копий (позитивов)
DECLARE @M_KOPP bit = 1			-- метраж копий (позитивов)
DECLARE @K_GORIG bit = 1		-- кол-во граммофонных оригиналов
DECLARE @K_GRAM bit = 1			-- кол-во грампластинок
*/
-- замена параметров для тестирования запроса

if (@A_NACH_DATA = 1)		-- Начальная дата документов
    update R set R.START_YEAR = (
		select MIN(
			case when ISNUMERIC(SUBSTRING(td.DOCUM_DATE, 1, 4)) = 1
			then CAST(SUBSTRING(td.DOCUM_DATE, 1, 4) as INT) else 0 end
		) from tblDOCUMENT td
		where td.ISN_UNIT = R.ISN_UNIT and td.DOCUM_DATE is not null and td.Deleted = 0)
	from tblUNIT R
	left join tblDOCUMENT D on R.ISN_UNIT = D.ISN_UNIT
	where /*where*/ and D.DOCUM_DATE is not null and D.Deleted = 0 and R.Deleted = 0

if (@A_KON_DATA = 1)		-- Конечная дата документов
    update R set R.END_YEAR = (
		select MAX(
			case when ISNUMERIC(SUBSTRING(td.DOCUM_DATE,1,4)) = 1
			then CAST(SUBSTRING(td.DOCUM_DATE,1,4) as INT) else 0 end
		) from tblDOCUMENT td
		where td.ISN_UNIT = R.ISN_UNIT and td.DOCUM_DATE is not null and td.Deleted = 0)
	from tblUNIT R
	left join tblDOCUMENT D on R.ISN_UNIT = D.ISN_UNIT
	where /*where*/ and D.DOCUM_DATE is not null and D.Deleted = 0 and R.Deleted = 0

if (@A_LISTS = 1)			-- Листов
	update R set R.PAGE_COUNT = (
		select coalesce(SUM(td.PAGE_COUNT),0) from tblDOCUMENT td
		where td.ISN_UNIT = R.ISN_UNIT and td.Deleted = 0)
	from tblUNIT R
	left join tblDOCUMENT D on R.ISN_UNIT = D.ISN_UNIT
	where /*where*/ and D.PAGE_COUNT is not null and D.Deleted = 0 and R.Deleted = 0

if (@A_EU = 1)				-- Количество единиц хранения/учета
	update R set R.UNIT_CNT = (
		select COUNT(tu.ID) from tblUNIT tu
		where tu.ISN_HIGH_UNIT = R.ISN_UNIT)
	from tblUNIT R
	where /*where*/ and R.Deleted = 0

----

if (@K_NEG = 1)				-- Количество негативов
	update R set R.NEGATIVE_COUNT = (
		select COUNT(tu.ID) from tblUNIT tu, tblUNIT_MOVIE tm
		where tu.ISN_UNIT = tm.ISN_UNIT and tu.ISN_HIGH_UNIT = R.ISN_UNIT
		and tm.UNIT_TYPE = 'a' and tu.UNIT_KIND = 703)
	from tblUNIT_MOVIE R join tblUNIT U on R.ISN_UNIT = U.ISN_UNIT
	where /*where*/ and U.Deleted = 0

if (@M_NEG = 1)				-- Метраж негативов
	update R set R.NEGATIVE_LENGTH = (
		select coalesce(SUM(tm.NEGATIVE_LENGTH),0) from tblUNIT tu, tblUNIT_MOVIE tm
		where tu.ISN_UNIT = tm.ISN_UNIT and tu.ISN_HIGH_UNIT = R.ISN_UNIT
		and tm.UNIT_TYPE = 'a' and tu.UNIT_KIND = 703)
	from tblUNIT_MOVIE R join tblUNIT U on R.ISN_UNIT = U.ISN_UNIT
	where /*where*/ and U.Deleted = 0

if (@K_DNEG = 1)			-- Количество дубль-негативов (контратипов)
	update R set R.DUP_NEGATIVE_COUNT = (
		select COUNT(tu.ID) from tblUNIT tu, tblUNIT_MOVIE tm
		where tu.ISN_UNIT = tm.ISN_UNIT and tu.ISN_HIGH_UNIT = R.ISN_UNIT
		and tm.UNIT_TYPE = 'b' and tu.UNIT_KIND = 703)
	from tblUNIT_MOVIE R join tblUNIT U on R.ISN_UNIT = U.ISN_UNIT
	where /*where*/ and U.Deleted = 0

if (@M_DNEG = 1)			-- Метраж дубль-негативов (контратипов)
	update R set R.DUP_NEGATIVE_LENGTH = (
		select coalesce(SUM(tm.DUP_NEGATIVE_LENGTH),0) from tblUNIT tu, tblUNIT_MOVIE tm
		where tu.ISN_UNIT = tm.ISN_UNIT and tu.ISN_HIGH_UNIT = R.ISN_UNIT
		and tm.UNIT_TYPE = 'b' and tu.UNIT_KIND = 703)
	from tblUNIT_MOVIE R join tblUNIT U on R.ISN_UNIT = U.ISN_UNIT
	where /*where*/ and U.Deleted = 0

if (@K_FON = 1)				-- Количество негативов фонограмм
	update R set R.PHONO_NEGATIVE_COUNT = (
		select COUNT(tu.ID) from tblUNIT tu, tblUNIT_MOVIE tm
		where tu.ISN_UNIT = tm.ISN_UNIT and tu.ISN_HIGH_UNIT = R.ISN_UNIT
		and tm.UNIT_TYPE = 'c' and tu.UNIT_KIND = 703)
	from tblUNIT_MOVIE R join tblUNIT U on R.ISN_UNIT = U.ISN_UNIT
	where /*where*/ and U.Deleted = 0

if (@M_FON = 1)				-- Метраж фонограмм (негатив)
	update R set R.PHONO_NEGATIVE_LENGTH = (
		select coalesce(SUM(tm.PHONO_NEGATIVE_LENGTH),0) from tblUNIT tu, tblUNIT_MOVIE tm
		where tu.ISN_UNIT = tm.ISN_UNIT and tu.ISN_HIGH_UNIT = R.ISN_UNIT
		and tm.UNIT_TYPE = 'c' and tu.UNIT_KIND = 703)
	from tblUNIT_MOVIE R join tblUNIT U on R.ISN_UNIT = U.ISN_UNIT
	where /*where*/ and U.Deleted = 0

if (@K_MFON = 1)			-- Количество магнитных фонограмм
	update R set R.PHONO_MAG_COUNT = (
		select COUNT(tu.ID) from tblUNIT tu, tblUNIT_MOVIE tm
		where tu.ISN_UNIT = tm.ISN_UNIT and tu.ISN_HIGH_UNIT = R.ISN_UNIT
		and tm.UNIT_TYPE = 'd' and tu.UNIT_KIND = 703)
	from tblUNIT_MOVIE R join tblUNIT U on R.ISN_UNIT = U.ISN_UNIT
	where /*where*/ and U.Deleted = 0

if (@M_MFON = 1)			-- Метраж магнитных фонограмм
	update R set R.PHONO_MAG_LENGTH = (
		select coalesce(SUM(tm.PHONO_MAG_LENGTH),0) from tblUNIT tu, tblUNIT_MOVIE tm
		where tu.ISN_UNIT = tm.ISN_UNIT and tu.ISN_HIGH_UNIT = R.ISN_UNIT
		and tm.UNIT_TYPE = 'd' and tu.UNIT_KIND = 703)
	from tblUNIT_MOVIE R join tblUNIT U on R.ISN_UNIT = U.ISN_UNIT
	where /*where*/ and U.Deleted = 0

if (@K_MFONS = 1)			-- Количество магнитных фонограмм совмещен.
	update R set R.PHONO_COMBINED_COUNT = (
		select COUNT(tu.ID) from tblUNIT tu, tblUNIT_MOVIE tm
		where tu.ISN_UNIT = tm.ISN_UNIT and tu.ISN_HIGH_UNIT = R.ISN_UNIT
		and tm.UNIT_TYPE = 'e' and tu.UNIT_KIND = 703)
	from tblUNIT_MOVIE R join tblUNIT U on R.ISN_UNIT = U.ISN_UNIT
	where /*where*/ and U.Deleted = 0

if (@M_MFONS = 1)			-- Метраж магнитных фонограмм совмещен.
	update R set R.PHONO_COMBINED_LENGTH = (
		select coalesce(SUM(tm.PHONO_COMBINED_LENGTH),0) from tblUNIT tu, tblUNIT_MOVIE tm
		where tu.ISN_UNIT = tm.ISN_UNIT and tu.ISN_HIGH_UNIT = R.ISN_UNIT
		and tm.UNIT_TYPE = 'e' and tu.UNIT_KIND = 703)
	from tblUNIT_MOVIE R join tblUNIT U on R.ISN_UNIT = U.ISN_UNIT
	where /*where*/ and U.Deleted = 0

if (@K_PPOS = 1)			-- Количество промежуточных позитивов
	update R set R.INTERPOSITIVE_COUNT = (
		select COUNT(tu.ID) from tblUNIT tu, tblUNIT_MOVIE tm
		where tu.ISN_UNIT = tm.ISN_UNIT and tu.ISN_HIGH_UNIT = R.ISN_UNIT
		and tm.UNIT_TYPE = 'f' and tu.UNIT_KIND = 703)
	from tblUNIT_MOVIE R join tblUNIT U on R.ISN_UNIT = U.ISN_UNIT
	where /*where*/ and U.Deleted = 0

if (@M_PPOS = 1)			-- Метраж промежуточных позитивов
	update R set R.INTERPOSITIVE_LENGTH = (
		select coalesce(SUM(tm.INTERPOSITIVE_LENGTH),0) from tblUNIT tu, tblUNIT_MOVIE tm
		where tu.ISN_UNIT = tm.ISN_UNIT and tu.ISN_HIGH_UNIT = R.ISN_UNIT
		and tm.UNIT_TYPE = 'f' and tu.UNIT_KIND = 703)
	from tblUNIT_MOVIE R join tblUNIT U on R.ISN_UNIT = U.ISN_UNIT
	where /*where*/ and U.Deleted = 0

if (@K_POS = 1)				-- Количество позитивов
	update R set R.POSITIVE_COUNT = (
		select COUNT(tu.ID) from tblUNIT tu, tblUNIT_MOVIE tm
		where tu.ISN_UNIT = tm.ISN_UNIT and tu.ISN_HIGH_UNIT = R.ISN_UNIT
		and tm.UNIT_TYPE = 'j' and tu.UNIT_KIND = 703)
	from tblUNIT_MOVIE R join tblUNIT U on R.ISN_UNIT = U.ISN_UNIT
	where /*where*/ and U.Deleted = 0

if (@M_POS = 1)				-- Метраж позитивов
	update R set R.POSITIVE_LENGTH = (
		select coalesce(SUM(tm.POSITIVE_LENGTH),0) from tblUNIT tu, tblUNIT_MOVIE tm
		where tu.ISN_UNIT = tm.ISN_UNIT and tu.ISN_HIGH_UNIT = R.ISN_UNIT
		and tm.UNIT_TYPE = 'j' and tu.UNIT_KIND = 703)
	from tblUNIT_MOVIE R join tblUNIT U on R.ISN_UNIT = U.ISN_UNIT
	where /*where*/ and U.Deleted = 0

----

if (@K_ORIG = 1)			-- Количество оригиналов
	update R set R.ORIGINAL_COUNT = (
		select COUNT(tv.ORIGINAL) from tblUNIT tu, tblUNIT_VIDEO tv
		where tv.ISN_UNIT = tu.ISN_HIGH_UNIT and tu.ISN_HIGH_UNIT = R.ISN_UNIT
		and tu.UNIT_KIND = 703 and tv.ORIGINAL = 'Y')
	from tblUNIT_VIDEO R join tblUNIT U on R.ISN_UNIT = U.ISN_UNIT
	where U.UNIT_KIND = 704 and /*where*/ and U.Deleted = 0 

if (@K_KOP = 1)				-- Количество копий
	update R set R.COPY_COUNT = (
		select COUNT(tv.ORIGINAL) from tblUNIT tu, tblUNIT_VIDEO tv
		where tv.ISN_UNIT = tu.ISN_HIGH_UNIT and tu.ISN_HIGH_UNIT = R.ISN_UNIT
		and tu.UNIT_KIND = 703 and tv.ORIGINAL = 'N')
	from tblUNIT_VIDEO R join tblUNIT U on R.ISN_UNIT = U.ISN_UNIT
	where U.UNIT_KIND = 704 and /*where*/ and U.Deleted = 0

----

if (@K_ORIGN = 1)			-- Количество оригиналов (негативов)
	update R set R.NEGATIVE_COUNT = (
		select COUNT(tu.ID) from tblUNIT tu, tblUNIT_PHONO tp
		where tu.ISN_UNIT = tp.ISN_UNIT and tu.ISN_HIGH_UNIT = R.ISN_UNIT
		and tp.PHONO_TYPE = 'c' and tu.UNIT_KIND = 703)
	from tblUNIT_PHONO R join tblUNIT U on R.ISN_UNIT = U.ISN_UNIT
	where U.UNIT_KIND = 704 and /*where*/ and U.Deleted = 0

if (@M_ORIGN = 1)			-- Метраж оригиналов (негативов)
	update R set R.NEGATIVE_LENGTH = (
		select coalesce(SUM(tp.NEGATIVE_LENGTH),0) from tblUNIT tu, tblUNIT_PHONO tp
		where tu.ISN_UNIT = tp.ISN_UNIT and tu.ISN_HIGH_UNIT = R.ISN_UNIT
		and tp.PHONO_TYPE = 'c' and tu.UNIT_KIND = 703)
	from tblUNIT_PHONO R join tblUNIT U on R.ISN_UNIT = U.ISN_UNIT
	where U.UNIT_KIND = 704 and /*where*/ and U.Deleted = 0

if (@K_KOPP = 1)			-- Количество копий (позитивов)
	update R set R.COPY_COUNT = (
		select COUNT(tu.ID) from tblUNIT tu, tblUNIT_PHONO tp
		where tu.ISN_UNIT = tp.ISN_UNIT and tu.ISN_HIGH_UNIT = R.ISN_UNIT
		and tp.PHONO_TYPE = 'd' and tu.UNIT_KIND = 703)
	from tblUNIT_PHONO R join tblUNIT U on R.ISN_UNIT = U.ISN_UNIT
	where U.UNIT_KIND = 704 and /*where*/ and U.Deleted = 0

if (@M_KOPP = 1)			-- Метраж копий (позитивов)
	update R set R.COPY_LENGTH = (
		select coalesce(SUM(tp.COPY_LENGTH),0) from tblUNIT tu, tblUNIT_PHONO tp
		where tu.ISN_UNIT = tp.ISN_UNIT and tu.ISN_HIGH_UNIT = R.ISN_UNIT
		and tp.PHONO_TYPE = 'd' and tu.UNIT_KIND = 703)
	from tblUNIT_PHONO R join tblUNIT U on R.ISN_UNIT = U.ISN_UNIT
	where U.UNIT_KIND = 704 and /*where*/ and U.Deleted = 0

if (@K_GORIG = 1)			-- Количество граммофонных оригиналов
	update R set R.ORIGINAL_COUNT = (
		select COUNT(tu.ID) from tblUNIT tu, tblUNIT_PHONO tp
		where tu.ISN_UNIT = tp.ISN_UNIT and tu.ISN_HIGH_UNIT = R.ISN_UNIT
		and tp.PHONO_TYPE = 'a' and tu.UNIT_KIND = 703)
	from tblUNIT_PHONO R join tblUNIT U on R.ISN_UNIT = U.ISN_UNIT
	where U.UNIT_KIND = 704 and /*where*/ and U.Deleted = 0

if (@K_GRAM = 1)			-- Количество грампластинок
	update R set R.DISK_COUNT = (
		select COUNT(tu.ID) from tblUNIT tu, tblUNIT_PHONO tp
		where tu.ISN_UNIT = tp.ISN_UNIT and tu.ISN_HIGH_UNIT = R.ISN_UNIT
		and tp.PHONO_TYPE = 'b' and tu.UNIT_KIND = 703)
	from tblUNIT_PHONO R join tblUNIT U on R.ISN_UNIT = U.ISN_UNIT
	where U.UNIT_KIND = 704 and /*where*/ and U.Deleted = 0


                                        </sql>, _
                                        <sql>



-- Пересчет паспорта
-- ВНИМАНИЕ! действительно только для АФ (один архив)

-- declare @ISN_PASSPORT bigint = 1

create table #f (id bigint)
--insert into #f
--	select f.ISN_FUND from tblFUND f
--	left join tblACT a on a.ISN_FUND = f.ISN_FUND
--	where f.PRESENCE_FLAG = 'a'
--		and not exists(select a.ID from tblACT a where a.ACT_OBJ = 701 and a.MOVEMENT_FLAG = 2 
--       and a.ISN_FUND = f.ISN_FUND)
--		and f.Deleted = 0 and isnull(a.Deleted, 0) = 0

insert into #f
	select f.ISN_FUND from tblFUND f
	where f.PRESENCE_FLAG = 'a'
		and f.Deleted = 0


create table #fnew (id bigint)
insert into #fnew
	select f.ISN_FUND from tblFUND f
	left join tblACT a on a.ISN_FUND = f.ISN_FUND
	where f.PRESENCE_FLAG = 'a' and f.Deleted = 0

create table #l (id bigint)
insert into #l select ISN_LOCATION from tblLOCATION where Deleted = 0

-- [1]

-- (02) Документы на бумажных носителях - Управленческие

update R set
R.FUND_COUNT = (
	select count(tf.ISN_FUND) from tblFUND tf
	where tf.ISN_FUND in (select ID from #fnew)
	and tf.ISN_DOC_TYPE = 1
),
R.UNIT_COUNT = (
	select coalesce(sum(td.UNIT_COUNT),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'P' and td.ISN_DOC_TYPE = 1 and td.ISN_INVENTORY is null
),
R.UNIT_INVENTORY = (
	select coalesce(sum(td.UNIT_INVENTORY),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'P' and td.ISN_DOC_TYPE = 1 and td.ISN_INVENTORY is null
),
R.UNIT_SECRET = (
	select coalesce(sum(td.SECRET_UNITS),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'P' and td.ISN_DOC_TYPE = 1 and td.ISN_INVENTORY is null
),
R.UNIT_UNIQUE = (
	select coalesce(sum(td.UNITS_UNIQUE),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'P' and td.ISN_DOC_TYPE = 1 and td.ISN_INVENTORY is null
),
R.UNIT_OC = (
	select coalesce(sum(td.UNIT_OC_COUNT),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'P' and td.ISN_DOC_TYPE = 1 and td.ISN_INVENTORY is null
)/*,
R.UNIT_DEP = (
	select count(tu.NUM) from (
		select distinct isnull(dtu.UNIT_NUM_1,'') + isnull(dtu.UNIT_NUM_2,'') NUM
		from tblUNIT dtu
		left join tblINVENTORY dti on dti.ISN_INVENTORY = dtu.ISN_INVENTORY
		where dtu.Deleted = 0 and dti.Deleted = 0 and
		dti.INVENTORY_KEEP_PERIOD = 'b' and dti.PRESENCE_FLAG = 'a' and
		dtu.MEDIUM_TYPE = 'T' and dtu.IS_LOST = 'N' and
		dtu.ISN_DOC_TYPE = 1
	) tu
)*/
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE = 1 and R.CARRIER_TYPE = 'P'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0


-- (03) Документы на бумажных носителях - Личного происхождения

update R set
R.FUND_COUNT = (
	select count(tf.ISN_FUND) from tblFUND tf
	where tf.ISN_FUND in (select ID from #fnew)
	and tf.ISN_DOC_TYPE = 3
),
R.UNIT_COUNT = (
	select coalesce(sum(td.UNIT_COUNT),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'P' and td.ISN_DOC_TYPE = 3 and td.ISN_INVENTORY is null
),
R.UNIT_INVENTORY = (
	select coalesce(sum(td.UNIT_INVENTORY),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'P' and td.ISN_DOC_TYPE = 3 and td.ISN_INVENTORY is null
),
R.UNIT_SECRET = (
	select coalesce(sum(td.SECRET_UNITS),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #f)
	and td.CARRIER_TYPE = 'P' and td.ISN_DOC_TYPE = 3 and td.ISN_INVENTORY is null
),
R.UNIT_UNIQUE = (
	select coalesce(sum(td.UNITS_UNIQUE),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'P' and td.ISN_DOC_TYPE = 3 and td.ISN_INVENTORY is null
),
R.UNIT_OC = (
	select coalesce(sum(td.UNIT_OC_COUNT),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'P' and td.ISN_DOC_TYPE = 3 and td.ISN_INVENTORY is null
)/*,
R.UNIT_DEP = (
	select count(tu.NUM) from (
		select distinct isnull(dtu.UNIT_NUM_1,'') + isnull(dtu.UNIT_NUM_2,'') NUM
		from tblUNIT dtu
		left join tblINVENTORY dti on dti.ISN_INVENTORY = dtu.ISN_INVENTORY
		where dtu.Deleted = 0 and dti.Deleted = 0 and
		dti.INVENTORY_KEEP_PERIOD = 'b' and dti.PRESENCE_FLAG = 'a' and
		dtu.MEDIUM_TYPE = 'T' and dtu.IS_LOST = 'N' and
		dtu.ISN_DOC_TYPE = 3
	) tu
)*/
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE = 3 and R.CARRIER_TYPE = 'P'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (04) Документы на бумажных носителях - НТД

update R set
R.FUND_COUNT = (
	select count(tf.ISN_FUND) from tblFUND tf
	where tf.ISN_FUND in (select ID from #fnew)
	and tf.ISN_DOC_TYPE = 4 and tf.CARRIER_TYPE = 'T'
),
R.UNIT_COUNT = (
	select coalesce(sum(td.UNIT_COUNT),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'P' and td.ISN_DOC_TYPE = 4 and td.ISN_INVENTORY is null
),
R.UNIT_INVENTORY = (
	select coalesce(sum(td.UNIT_INVENTORY),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'P' and td.ISN_DOC_TYPE = 4 and td.ISN_INVENTORY is null
),
R.UNIT_SECRET = (
	select coalesce(sum(td.SECRET_UNITS),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'P' and td.ISN_DOC_TYPE = 4 and td.ISN_INVENTORY is null
),
R.UNIT_UNIQUE = (
	select coalesce(sum(td.UNITS_UNIQUE),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'P' and td.ISN_DOC_TYPE = 4 and td.ISN_INVENTORY is null
),
R.UNIT_OC = (
	select coalesce(sum(td.UNIT_OC_COUNT),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'P' and td.ISN_DOC_TYPE = 4 and td.ISN_INVENTORY is null
)/*,
R.UNIT_DEP = (
	select count(tu.NUM) from (
		select distinct isnull(dtu.UNIT_NUM_1,'') + isnull(dtu.UNIT_NUM_2,'') NUM
		from tblUNIT dtu
		left join tblINVENTORY dti on dti.ISN_INVENTORY = dtu.ISN_INVENTORY
		where dtu.Deleted = 0 and dti.Deleted = 0 and
		dti.INVENTORY_KEEP_PERIOD = 'b' and dti.PRESENCE_FLAG = 'a' and
		dtu.MEDIUM_TYPE = 'T' and dtu.IS_LOST = 'N' and
		dtu.ISN_DOC_TYPE = 4
	) tu
)*/
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'P'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (05) Документы на бумажных носителях - По личному составу

update R set
R.FUND_COUNT = (
	select count(tf.ISN_FUND) from tblFUND tf
	where tf.ISN_FUND in (select ID from #fnew)
	and tf.ISN_DOC_TYPE = 2
),
R.UNIT_COUNT = (
	select coalesce(sum(td.UNIT_COUNT),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'P' and td.ISN_DOC_TYPE = 2 and td.ISN_INVENTORY is null
),
R.UNIT_INVENTORY = (
	select coalesce(sum(td.UNIT_INVENTORY),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'P' and td.ISN_DOC_TYPE = 2 and td.ISN_INVENTORY is null
),
R.UNIT_SECRET = (
	select coalesce(sum(td.SECRET_UNITS),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'P' and td.ISN_DOC_TYPE = 2 and td.ISN_INVENTORY is null
),
R.UNIT_UNIQUE = (
	select coalesce(sum(td.UNITS_UNIQUE),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'P' and td.ISN_DOC_TYPE = 2 and td.ISN_INVENTORY is null
),
R.UNIT_OC = (
	select coalesce(sum(td.UNIT_OC_COUNT),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #f)
	and td.CARRIER_TYPE = 'P' and td.ISN_DOC_TYPE = 2 and td.ISN_INVENTORY is null
)/*,
R.UNIT_DEP = (
	select count(tu.NUM) from (
		select distinct isnull(dtu.UNIT_NUM_1,'') + isnull(dtu.UNIT_NUM_2,'') NUM
		from tblUNIT dtu
		left join tblINVENTORY dti on dti.ISN_INVENTORY = dtu.ISN_INVENTORY
		where dtu.Deleted = 0 and dti.Deleted = 0 and
		dti.INVENTORY_KEEP_PERIOD = 'b' and dti.PRESENCE_FLAG = 'a' and
		dtu.MEDIUM_TYPE = 'T' and dtu.IS_LOST = 'N' and
		dtu.ISN_DOC_TYPE = 2
	) tu
)*/
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE = 2 and R.CARRIER_TYPE = 'P'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (07) Аудиовизуальные документы на традиционных носителях - Кинодокументы

update R set
R.FUND_COUNT = (
	select count(tf.ISN_FUND) from tblFUND tf
	where tf.ISN_FUND in (select ID from #fnew)
	and tf.ISN_DOC_TYPE = 7 and tf.CARRIER_TYPE = 'T'
),
R.UNIT_COUNT = (
	select coalesce(sum(td.UNIT_COUNT),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE = 7 and td.ISN_INVENTORY is null
),
R.UNIT_INVENTORY = (
	select coalesce(sum(td.UNIT_INVENTORY),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE = 7 and td.ISN_INVENTORY is null
),
R.UNIT_SECRET = (
	select coalesce(sum(td.SECRET_UNITS),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE = 7 and td.ISN_INVENTORY is null
),
R.UNIT_UNIQUE = (
	select coalesce(sum(td.UNITS_UNIQUE),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE = 7 and td.ISN_INVENTORY is null
),
R.UNIT_OC = (
	select coalesce(sum(td.UNIT_OC_COUNT),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE = 7 and td.ISN_INVENTORY is null
),
/*R.UNIT_DEP = (
	select count(tu.NUM) from (
		select distinct isnull(dtu.UNIT_NUM_1,'') + isnull(dtu.UNIT_NUM_2,'') NUM
		from tblUNIT dtu
		left join tblINVENTORY dti on dti.ISN_INVENTORY = dtu.ISN_INVENTORY
		where dtu.Deleted = 0 and dti.Deleted = 0 and
		dti.INVENTORY_KEEP_PERIOD = 'b' and dti.PRESENCE_FLAG = 'a' and
		dtu.MEDIUM_TYPE = 'T' and dtu.IS_LOST = 'N' and
		dtu.ISN_DOC_TYPE = 7
	) tu
),*/
R.REG_UNIT_COUNT = (
	select coalesce(sum(td.REG_UNIT),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #f)
	and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE = 7 and td.ISN_INVENTORY is null
),
R.REG_UNIT_INVENTORY  = (
	select coalesce(sum(td.REG_UNIT_INVENTORY),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #f)
	and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE = 7 and td.ISN_INVENTORY is null
)
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'A'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (08) Аудиовизуальные документы на традиционных носителях - Фотодокументы

update R set
R.FUND_COUNT = (
	select count(tf.ISN_FUND) from tblFUND tf
	where tf.ISN_FUND in (select ID from #fnew)
	and tf.ISN_DOC_TYPE = 5 and tf.CARRIER_TYPE = 'T'
),
R.UNIT_COUNT = (
	select coalesce(sum(td.UNIT_COUNT),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE = 5 and td.ISN_INVENTORY is null
),
R.UNIT_INVENTORY = (
	select coalesce(sum(td.UNIT_INVENTORY),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE = 5 and td.ISN_INVENTORY is null
),
R.UNIT_SECRET = (
	select coalesce(sum(td.SECRET_UNITS),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE = 5 and td.ISN_INVENTORY is null
),
R.UNIT_UNIQUE = (
	select coalesce(sum(td.UNITS_UNIQUE),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE = 5 and td.ISN_INVENTORY is null
),
R.UNIT_OC = (
	select coalesce(sum(td.UNIT_OC_COUNT),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE = 5 and td.ISN_INVENTORY is null
)/*,
R.UNIT_DEP = (
	select count(tu.NUM) from (
		select distinct isnull(dtu.UNIT_NUM_1,'') + isnull(dtu.UNIT_NUM_2,'') NUM
		from tblUNIT dtu
		left join tblINVENTORY dti on dti.ISN_INVENTORY = dtu.ISN_INVENTORY
		where dtu.Deleted = 0 and dti.Deleted = 0 and
		dti.INVENTORY_KEEP_PERIOD = 'b' and dti.PRESENCE_FLAG = 'a' and
		dtu.MEDIUM_TYPE = 'T' and dtu.IS_LOST = 'N' and
		dtu.ISN_DOC_TYPE = 5
	) tu
)*/
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'A'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (09) Аудиовизуальные документы на традиционных носителях - Фонодокументы

update R set
R.FUND_COUNT = (
	select count(tf.ISN_FUND) from tblFUND tf
	where tf.ISN_FUND in (select ID from #fnew)
	and tf.ISN_DOC_TYPE = 6 and tf.CARRIER_TYPE = 'T'
),
R.UNIT_COUNT = (
	select coalesce(sum(td.UNIT_COUNT),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE = 6 and td.ISN_INVENTORY is null
),
R.UNIT_INVENTORY = (
	select coalesce(sum(td.UNIT_INVENTORY),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE = 6 and td.ISN_INVENTORY is null
),
R.UNIT_SECRET = (
	select coalesce(sum(td.SECRET_UNITS),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE = 6 and td.ISN_INVENTORY is null
),
R.UNIT_UNIQUE = (
	select coalesce(sum(td.UNITS_UNIQUE),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE = 6 and td.ISN_INVENTORY is null
),
R.UNIT_OC = (
	select coalesce(sum(td.UNIT_OC_COUNT),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE = 6 and td.ISN_INVENTORY is null
),
/*R.UNIT_DEP = (
	select count(tu.NUM) from (
		select distinct isnull(dtu.UNIT_NUM_1,'') + isnull(dtu.UNIT_NUM_2,'') NUM
		from tblUNIT dtu
		left join tblINVENTORY dti on dti.ISN_INVENTORY = dtu.ISN_INVENTORY
		where dtu.Deleted = 0 and dti.Deleted = 0 and
		dti.INVENTORY_KEEP_PERIOD = 'b' and dti.PRESENCE_FLAG = 'a' and
		dtu.MEDIUM_TYPE = 'T' and dtu.IS_LOST = 'N' and
		dtu.ISN_DOC_TYPE = 6
	) tu
),*/
R.REG_UNIT_COUNT = (
	select coalesce(sum(td.REG_UNIT),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #f)
	and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE = 6 and td.ISN_INVENTORY is null
),
R.REG_UNIT_INVENTORY = (
	select coalesce(sum(td.REG_UNIT_INVENTORY),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #f)
	and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE = 6 and td.ISN_INVENTORY is null
)
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'A'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (10) Аудиовизуальные документы на традиционных носителях - Видеодокументы

update R set
R.FUND_COUNT = (
	select count(tf.ISN_FUND) from tblFUND tf
	where tf.ISN_FUND in (select ID from #fnew)
	and tf.CARRIER_TYPE = 'T' and tf.ISN_DOC_TYPE = 8
),
R.UNIT_COUNT = (
	select coalesce(sum(td.UNIT_COUNT),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE = 8 and td.ISN_INVENTORY is null
),
R.UNIT_INVENTORY = (
	select coalesce(sum(td.UNIT_INVENTORY),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE = 8 and td.ISN_INVENTORY is null
),
R.UNIT_SECRET = (
	select coalesce(sum(td.SECRET_UNITS),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE = 8 and td.ISN_INVENTORY is null
),
R.UNIT_UNIQUE = (
	select coalesce(sum(td.UNITS_UNIQUE),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE = 8 and td.ISN_INVENTORY is null
),
R.UNIT_OC = (
	select coalesce(sum(td.UNIT_OC_COUNT),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE = 8 and td.ISN_INVENTORY is null
),
/*R.UNIT_DEP = (
	select count(tu.NUM) from (
		select distinct isnull(dtu.UNIT_NUM_1,'') + isnull(dtu.UNIT_NUM_2,'') NUM
		from tblUNIT dtu
		left join tblINVENTORY dti on dti.ISN_INVENTORY = dtu.ISN_INVENTORY
		where dtu.Deleted = 0 and dti.Deleted = 0 and
		dti.INVENTORY_KEEP_PERIOD = 'b' and dti.PRESENCE_FLAG = 'a' and
		dtu.MEDIUM_TYPE = 'T' and dtu.IS_LOST = 'N' and
		dtu.ISN_DOC_TYPE = 8
	) tu
),*/
R.REG_UNIT_COUNT = (
	select coalesce(sum(td.REG_UNIT),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE = 8 and td.ISN_INVENTORY is null
),
R.REG_UNIT_INVENTORY = (
	select coalesce(sum(td.REG_UNIT_INVENTORY),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE = 8 and td.ISN_INVENTORY is null
)
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'A'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (12) документы на электронных носителях - НТД

update R set
R.FUND_COUNT = (
	select count(tf.ISN_FUND) from tblFUND tf
	where tf.ISN_FUND in (select ID from #fnew)
	and tf.CARRIER_TYPE = 'E' and tf.ISN_DOC_TYPE = 4
),
R.UNIT_COUNT = (
	select coalesce(sum(td.UNIT_COUNT),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE = 4 and td.ISN_INVENTORY is null
),
R.UNIT_INVENTORY = (
	select coalesce(sum(td.UNIT_INVENTORY),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE = 4 and td.ISN_INVENTORY is null
),
R.UNIT_SECRET = (
	select coalesce(sum(td.SECRET_UNITS),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE = 4 and td.ISN_INVENTORY is null
),
R.UNIT_UNIQUE = (
	select coalesce(sum(td.UNITS_UNIQUE),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE = 4 and td.ISN_INVENTORY is null
),
R.UNIT_OC = (
	select coalesce(sum(td.UNIT_OC_COUNT),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE = 4 and td.ISN_INVENTORY is null
)/*,
R.UNIT_DEP = (
	select count(tu.NUM) from (
		select distinct isnull(dtu.UNIT_NUM_1,'') + isnull(dtu.UNIT_NUM_2,'') NUM
		from tblUNIT dtu
		left join tblINVENTORY dti on dti.ISN_INVENTORY = dtu.ISN_INVENTORY
		where dtu.Deleted = 0 and dti.Deleted = 0 and
		dti.INVENTORY_KEEP_PERIOD = 'b' and dti.PRESENCE_FLAG = 'a' and
		dtu.MEDIUM_TYPE = 'E' and dtu.IS_LOST = 'N' and
		dtu.ISN_DOC_TYPE = 4
	) tu
)*/
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'E'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (13) документы на электронных носителях - Кинодокументы

update R set
R.FUND_COUNT = (
	select count(tf.ISN_FUND) from tblFUND tf
	where tf.ISN_FUND in (select ID from #fnew)
	and tf.CARRIER_TYPE = 'E' and tf.ISN_DOC_TYPE = 7
),
R.UNIT_COUNT = (
	select coalesce(sum(td.UNIT_COUNT),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE = 7 and td.ISN_INVENTORY is null
),
R.UNIT_INVENTORY = (
	select coalesce(sum(td.UNIT_INVENTORY),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE = 7 and td.ISN_INVENTORY is null
),
R.UNIT_SECRET = (
	select coalesce(sum(td.SECRET_UNITS),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE = 7 and td.ISN_INVENTORY is null
),
R.UNIT_UNIQUE = (
	select coalesce(sum(td.UNITS_UNIQUE),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE = 7 and td.ISN_INVENTORY is null
),
R.UNIT_OC = (
	select coalesce(sum(td.UNIT_OC_COUNT),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE = 7 and td.ISN_INVENTORY is null
)/*,
R.UNIT_DEP = (
	select count(tu.NUM) from (
		select distinct isnull(dtu.UNIT_NUM_1,'') + isnull(dtu.UNIT_NUM_2,'') NUM
		from tblUNIT dtu
		left join tblINVENTORY dti on dti.ISN_INVENTORY = dtu.ISN_INVENTORY
		where dtu.Deleted = 0 and dti.Deleted = 0 and
		dti.INVENTORY_KEEP_PERIOD = 'b' and dti.PRESENCE_FLAG = 'a' and
		dtu.MEDIUM_TYPE = 'E' and dtu.IS_LOST = 'N' and
		dtu.ISN_DOC_TYPE = 7
	) tu
)*/
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'E'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (13 и 3/4) документы на электронных носителях - Видеодокументы

update R set
R.FUND_COUNT = (
	select count(tf.ISN_FUND) from tblFUND tf
	where tf.ISN_FUND in (select ID from #fnew)
	and tf.CARRIER_TYPE = 'E' and tf.ISN_DOC_TYPE = 8
),
R.UNIT_COUNT = (
	select coalesce(sum(td.UNIT_COUNT),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE = 8 and td.ISN_INVENTORY is null
),
R.UNIT_INVENTORY = (
	select coalesce(sum(td.UNIT_INVENTORY),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE = 8 and td.ISN_INVENTORY is null
),
R.UNIT_SECRET = (
	select coalesce(sum(td.SECRET_UNITS),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE = 8 and td.ISN_INVENTORY is null
),
R.UNIT_UNIQUE = (
	select coalesce(sum(td.UNITS_UNIQUE),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE = 8 and td.ISN_INVENTORY is null
),
R.UNIT_OC = (
	select coalesce(sum(td.UNIT_OC_COUNT),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE = 8 and td.ISN_INVENTORY is null
)/*,
R.UNIT_DEP = (
	select count(tu.NUM) from (
		select distinct isnull(dtu.UNIT_NUM_1,'') + isnull(dtu.UNIT_NUM_2,'') NUM
		from tblUNIT dtu
		left join tblINVENTORY dti on dti.ISN_INVENTORY = dtu.ISN_INVENTORY
		where dtu.Deleted = 0 and dti.Deleted = 0 and
		dti.INVENTORY_KEEP_PERIOD = 'b' and dti.PRESENCE_FLAG = 'a' and
		dtu.MEDIUM_TYPE = 'E' and dtu.IS_LOST = 'N' and
		dtu.ISN_DOC_TYPE = 8
	) tu
)*/
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'E'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (14) документы на электронных носителях - Фотодокументы

update R set
R.FUND_COUNT = (
	select count(tf.ISN_FUND) from tblFUND tf
	where tf.ISN_FUND in (select ID from #fnew)
	and tf.CARRIER_TYPE = 'E' and tf.ISN_DOC_TYPE = 5
),
R.UNIT_COUNT = (
	select coalesce(sum(td.UNIT_COUNT),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE = 5 and td.ISN_INVENTORY is null
),
R.UNIT_INVENTORY = (
	select coalesce(sum(td.UNIT_INVENTORY),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE = 5 and td.ISN_INVENTORY is null
),
R.UNIT_SECRET = (
	select coalesce(sum(td.SECRET_UNITS),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE = 5 and td.ISN_INVENTORY is null
),
R.UNIT_UNIQUE = (
	select coalesce(sum(td.UNITS_UNIQUE),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
    and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE = 5 and td.ISN_INVENTORY is null
),
R.UNIT_OC = (
	select coalesce(sum(td.UNIT_OC_COUNT),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
    and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE = 5 and td.ISN_INVENTORY is null
)/*,
R.UNIT_DEP = (
	select count(tu.NUM) from (
		select distinct isnull(dtu.UNIT_NUM_1,'') + isnull(dtu.UNIT_NUM_2,'') NUM
		from tblUNIT dtu
		left join tblINVENTORY dti on dti.ISN_INVENTORY = dtu.ISN_INVENTORY
		where dtu.Deleted = 0 and dti.Deleted = 0 and
		dti.INVENTORY_KEEP_PERIOD = 'b' and dti.PRESENCE_FLAG = 'a' and
		dtu.MEDIUM_TYPE = 'E' and dtu.IS_LOST = 'N' and
		dtu.ISN_DOC_TYPE = 5
	) tu
)*/
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'E'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (15) документы на электронных носителях - Фонодокументы

update R set
R.FUND_COUNT = (
	select count(tf.ISN_FUND) from tblFUND tf
	where tf.ISN_FUND in (select ID from #fnew)
	and tf.CARRIER_TYPE = 'E' and tf.ISN_DOC_TYPE = 6
),
R.UNIT_COUNT = (
	select coalesce(sum(td.UNIT_COUNT),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE = 6 and td.ISN_INVENTORY is null
),
R.UNIT_INVENTORY = (
	select coalesce(sum(td.UNIT_INVENTORY),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE = 6 and td.ISN_INVENTORY is null
),
R.UNIT_SECRET = (
	select coalesce(sum(td.SECRET_UNITS),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE = 6 and td.ISN_INVENTORY is null
),
R.UNIT_UNIQUE = (
	select coalesce(sum(td.UNITS_UNIQUE),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE = 6 and td.ISN_INVENTORY is null
),
R.UNIT_OC = (
	select coalesce(sum(td.UNIT_OC_COUNT),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE = 6 and td.ISN_INVENTORY is null
)/*,
R.UNIT_DEP = (
	select count(tu.NUM) from (
		select distinct isnull(dtu.UNIT_NUM_1,'') + isnull(dtu.UNIT_NUM_2,'') NUM
		from tblUNIT dtu
		left join tblINVENTORY dti on dti.ISN_INVENTORY = dtu.ISN_INVENTORY
		where dtu.Deleted = 0 and dti.Deleted = 0 and
		dti.INVENTORY_KEEP_PERIOD = 'b' and dti.PRESENCE_FLAG = 'a' and
		dtu.MEDIUM_TYPE = 'E' and dtu.IS_LOST = 'N' and
		dtu.ISN_DOC_TYPE = 6
	) tu
)*/
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'E'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (16) Микроформы на правах подлинников

update R set
R.FUND_COUNT = (
	select count(tf.ISN_FUND) from tblFUND tf
	where tf.ISN_FUND in (select ID from #fnew)
	and tf.ISN_DOC_TYPE = 9
),
R.UNIT_COUNT = (
	select coalesce(sum(td.UNIT_COUNT),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'M' and td.ISN_DOC_TYPE = 9 and td.ISN_INVENTORY is null
),
R.UNIT_INVENTORY = (
	select coalesce(sum(td.UNIT_INVENTORY),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'M' and td.ISN_DOC_TYPE = 9 and td.ISN_INVENTORY is null
),
R.UNIT_SECRET = (
	select coalesce(sum(td.SECRET_UNITS),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'M' and td.ISN_DOC_TYPE = 9 and td.ISN_INVENTORY is null
),
R.UNIT_UNIQUE = (
	select coalesce(sum(td.UNITS_UNIQUE),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'M' and td.ISN_DOC_TYPE = 9 and td.ISN_INVENTORY is null
),
R.UNIT_OC = (
	select coalesce(sum(td.UNIT_OC_COUNT),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'M' and td.ISN_DOC_TYPE = 9 and td.ISN_INVENTORY is null
)/*,
R.UNIT_DEP = (
	select count(tu.NUM) from (
		select distinct isnull(dtu.UNIT_NUM_1,'') + isnull(dtu.UNIT_NUM_2,'') NUM
		from tblUNIT dtu
		left join tblINVENTORY dti on dti.ISN_INVENTORY = dtu.ISN_INVENTORY
		where dtu.Deleted = 0 and dti.Deleted = 0 and
		dti.INVENTORY_KEEP_PERIOD = 'b' and dti.PRESENCE_FLAG = 'a' and
		dtu.IS_LOST = 'N' and
		dtu.ISN_DOC_TYPE = 9
	) tu
)*/
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE = 9 and R.CARRIER_TYPE = 'M'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (21) Документы на бумажных носителях - Управленческие

update R set
R.UNIT_HAS_SF = (
	select coalesce(sum(td.UNIT_HAS_SF),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'P' and td.ISN_DOC_TYPE = 1 and td.ISN_INVENTORY is null
),
R.UNIT_HAS_FP = (
	select coalesce(sum(td.UNIT_HAS_FP),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'P' and td.ISN_DOC_TYPE = 1 and td.ISN_INVENTORY is null
)
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE = 1 and R.CARRIER_TYPE = 'P'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (22) Документы на бумажных носителях - Личного происхождения

update R set
R.UNIT_HAS_SF = (
	select coalesce(sum(td.UNIT_HAS_SF),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'P' and td.ISN_DOC_TYPE = 3 and td.ISN_INVENTORY is null
),
R.UNIT_HAS_FP = (
	select coalesce(sum(td.UNIT_HAS_FP),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'P' and td.ISN_DOC_TYPE = 3 and td.ISN_INVENTORY is null
)
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE = 3 and R.CARRIER_TYPE = 'P'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (23) Документы на бумажных носителях - НТД

update R set
R.UNIT_HAS_SF = (
	select coalesce(sum(td.UNIT_HAS_SF),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'P' and td.ISN_DOC_TYPE = 4 and td.ISN_INVENTORY is null
),
R.UNIT_HAS_FP = (
	select coalesce(sum(td.UNIT_HAS_FP),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'P' and td.ISN_DOC_TYPE = 4 and td.ISN_INVENTORY is null
)
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'P'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (24) Документы на бумажных носителях - По личному составу

update R set
R.UNIT_HAS_FP = (
	select coalesce(sum(td.UNIT_HAS_FP),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'P' and td.ISN_DOC_TYPE = 2 and td.ISN_INVENTORY is null
)
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE = 2 and R.CARRIER_TYPE = 'P'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (26) Аудиовизуальные документы на традиционных носителях - Кинодокументы

update R set
R.UNIT_HAS_SF = (
	select coalesce(sum(td.UNIT_HAS_SF),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE = 7 and td.ISN_INVENTORY is null
),
R.UNIT_HAS_FP = (
	select coalesce(sum(td.UNIT_HAS_FP),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE = 7 and td.ISN_INVENTORY is null
)
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'A'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (27) Аудиовизуальные документы на традиционных носителях - Фотодокументы

update R set
R.UNIT_HAS_SF = (
	select coalesce(sum(td.UNIT_HAS_SF),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE = 5 and td.ISN_INVENTORY is null
),
R.UNIT_HAS_FP = (
	select coalesce(sum(td.UNIT_HAS_FP),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE = 5 and td.ISN_INVENTORY is null
)
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'A'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (28) Аудиовизуальные документы на традиционных носителях - Фонодокументы

update R set
R.UNIT_HAS_SF = (
	select coalesce(sum(td.UNIT_HAS_SF),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE = 6 and td.ISN_INVENTORY is null
),
R.UNIT_HAS_FP = (
	select coalesce(sum(td.UNIT_HAS_FP),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE = 6 and td.ISN_INVENTORY is null
)
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'A'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (29) Аудиовизуальные документы на традиционных носителях - Видеодокументы

update R set
R.UNIT_HAS_SF = (
	select coalesce(sum(td.UNIT_HAS_SF),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE = 8 and td.ISN_INVENTORY is null
),
R.UNIT_HAS_FP = (
	select coalesce(sum(td.UNIT_HAS_FP),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE = 8 and td.ISN_INVENTORY is null
)
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'A'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (31) документы на электронных носителях - НТД

update R set
R.UNIT_HAS_SF = (
	select coalesce(sum(td.UNIT_HAS_SF),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE = 4 and td.ISN_INVENTORY is null
),
R.UNIT_HAS_FP = (
	select coalesce(sum(td.UNIT_HAS_FP),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE = 4 and td.ISN_INVENTORY is null
)
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'E'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (32) документы на электронных носителях - Кинодокументы

update R set
R.UNIT_HAS_SF = (
	select coalesce(sum(td.UNIT_HAS_SF),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE = 7 and td.ISN_INVENTORY is null
),
R.UNIT_HAS_FP = (
	select coalesce(sum(td.UNIT_HAS_FP),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE = 7 and td.ISN_INVENTORY is null
)
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'E'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (33) документы на электронных носителях - Фотодокументы

update R set
R.UNIT_HAS_SF = (
	select coalesce(sum(td.UNIT_HAS_SF),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE = 5 and td.ISN_INVENTORY is null
),
R.UNIT_HAS_FP = (
	select coalesce(sum(td.UNIT_HAS_FP),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE = 5 and td.ISN_INVENTORY is null
)
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'E'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (34) документы на электронных носителях - Фонодокументы

update R set
R.UNIT_HAS_SF = (
	select coalesce(sum(td.UNIT_HAS_SF),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE = 6 and td.ISN_INVENTORY is null
),
R.UNIT_HAS_FP = (
	select coalesce(sum(td.UNIT_HAS_FP),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE = 6 and td.ISN_INVENTORY is null
)
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'E'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (34 и 3/4) документы на электронных носителях - Видеодокументы

update R set
R.UNIT_HAS_SF = (
	select coalesce(sum(td.UNIT_HAS_SF),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE = 8 and td.ISN_INVENTORY is null
),
R.UNIT_HAS_FP = (
	select coalesce(sum(td.UNIT_HAS_FP),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE = 8 and td.ISN_INVENTORY is null
)
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'E'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (35) Микроформы на правах подлинников

update R set
R.UNIT_HAS_SF = (
	select coalesce(sum(td.UNIT_HAS_SF),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'M' and td.ISN_DOC_TYPE = 9 and td.ISN_INVENTORY is null
),
R.UNIT_HAS_FP = (
	select coalesce(sum(td.UNIT_HAS_FP),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'M' and td.ISN_DOC_TYPE = 9 and td.ISN_INVENTORY is null
)
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE = 9 and R.CARRIER_TYPE = 'M'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (38) Документы на бумажных носителях - Управленческие

update R set
R.INVENTORY_COUNT = (
	select count(ti.NUM) from (
		select distinct dti.INVENTORY_NUM_1 + isnull(dti.INVENTORY_NUM_2,'') NUM, dti.ISN_FUND from tblINVENTORY dti
		where dti.Deleted = 0 and dti.PRESENCE_FLAG = 'a'
		and dti.CARRIER_TYPE = 'T' and dti.ISN_INVENTORY_TYPE = 1
	) ti ),
--R.INVENTORY_COUNT_FULL = (
--	select count(ti.NUM) from (
--		select distinct dti.INVENTORY_NUM_1 + isnull(dti.INVENTORY_NUM_2,'') NUM, dti.ISN_FUND from tblINVENTORY dti
--		where dti.Deleted = 0 and dti.PRESENCE_FLAG = 'a' and dti.COPY_COUNT = 3
--		and dti.CARRIER_TYPE = 'T' and dti.ISN_INVENTORY_TYPE = 1
--	) ti ),
R.INVENTORY_COUNT_FULL = (
	select count(ti.NUM) from (
		select distinct dti.INVENTORY_NUM_1 + isnull(dti.INVENTORY_NUM_2,'') NUM, dti.ISN_FUND from tblINVENTORY dti
		where dti.Deleted = 0 and dti.PRESENCE_FLAG = 'a' and 
		((dti.COPY_COUNT > 2 and dti.ISN_INVENTORY_TYPE IN (1,2,3,4)) or 
		(dti.COPY_COUNT > 1 and dti.ISN_INVENTORY_TYPE IN (5,6,7,8,9,10)))and dti.CARRIER_TYPE = 'T' and dti.ISN_INVENTORY_TYPE = 1
	) ti ),
R.UNIT_CATALOGUED = (
	select coalesce(sum(td.UNITS_CATALOGUED),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'P' and td.ISN_DOC_TYPE = 1 and td.ISN_INVENTORY is null
)
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE = 1 and R.CARRIER_TYPE = 'P'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (39) Документы на бумажных носителях - Личного происхождения

update R set
R.INVENTORY_COUNT = (
	select count(ti.NUM) from (
		select distinct dti.INVENTORY_NUM_1 + isnull(dti.INVENTORY_NUM_2,'') NUM, dti.ISN_FUND from tblINVENTORY dti
		where dti.Deleted = 0 and dti.PRESENCE_FLAG = 'a'
		and dti.CARRIER_TYPE = 'T' and dti.ISN_INVENTORY_TYPE = 3
	) ti ),
--R.INVENTORY_COUNT_FULL = (
--	select count(ti.NUM) from (
--		select distinct dti.INVENTORY_NUM_1 + isnull(dti.INVENTORY_NUM_2,'') NUM, dti.ISN_FUND from tblINVENTORY dti
--		where dti.Deleted = 0 and dti.PRESENCE_FLAG = 'a' and dti.COPY_COUNT = 3
--		and dti.CARRIER_TYPE = 'T' and dti.ISN_INVENTORY_TYPE = 3
--	) ti ),	
R.INVENTORY_COUNT_FULL = (
	select count(ti.NUM) from (
		select distinct dti.INVENTORY_NUM_1 + isnull(dti.INVENTORY_NUM_2,'') NUM, dti.ISN_FUND from tblINVENTORY dti
		where dti.Deleted = 0 and dti.PRESENCE_FLAG = 'a' and 
		((dti.COPY_COUNT > 2 and dti.ISN_INVENTORY_TYPE IN (1,2,3,4)) or 
		(dti.COPY_COUNT > 1 and dti.ISN_INVENTORY_TYPE IN (5,6,7,8,9,10)))and dti.CARRIER_TYPE = 'T' and dti.ISN_INVENTORY_TYPE = 3
	) ti ),	
R.UNIT_CATALOGUED = (
	select coalesce(sum(td.UNITS_CATALOGUED),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'P' and td.ISN_DOC_TYPE = 3 and td.ISN_INVENTORY is null
)
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE = 3 and R.CARRIER_TYPE = 'P'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (40) Документы на бумажных носителях - НТД

update R set
R.INVENTORY_COUNT = (
	select count(ti.NUM) from (
		select distinct dti.INVENTORY_NUM_1 + isnull(dti.INVENTORY_NUM_2,'') NUM, dti.ISN_FUND from tblINVENTORY dti
		where dti.Deleted = 0 and dti.PRESENCE_FLAG = 'a'
		and dti.CARRIER_TYPE = 'T' and dti.ISN_INVENTORY_TYPE = 4
	) ti ),
--R.INVENTORY_COUNT_FULL = (
--	select count(ti.NUM) from (
--		select distinct dti.INVENTORY_NUM_1 + isnull(dti.INVENTORY_NUM_2,'') NUM, dti.ISN_FUND from tblINVENTORY dti
--		where dti.Deleted = 0 and dti.PRESENCE_FLAG = 'a' and dti.COPY_COUNT = 3
--		and dti.CARRIER_TYPE = 'T' and dti.ISN_INVENTORY_TYPE = 4
--	) ti ),	
R.INVENTORY_COUNT_FULL = (
	select count(ti.NUM) from (
		select distinct dti.INVENTORY_NUM_1 + isnull(dti.INVENTORY_NUM_2,'') NUM, dti.ISN_FUND from tblINVENTORY dti
		where dti.Deleted = 0 and dti.PRESENCE_FLAG = 'a' and 
		((dti.COPY_COUNT > 2 and dti.ISN_INVENTORY_TYPE IN (1,2,3,4)) or 
		(dti.COPY_COUNT > 1 and dti.ISN_INVENTORY_TYPE IN (5,6,7,8,9,10)))and dti.CARRIER_TYPE = 'T' and dti.ISN_INVENTORY_TYPE = 4
	) ti ),	
R.UNIT_CATALOGUED = (
	select coalesce(sum(td.UNITS_CATALOGUED),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'P' and td.ISN_DOC_TYPE = 4 and td.ISN_INVENTORY is null
)
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'P'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (41) Документы на бумажных носителях - По личному составу

update R set
R.INVENTORY_COUNT = (
	select count(ti.NUM) from (
		select distinct dti.INVENTORY_NUM_1 + isnull(dti.INVENTORY_NUM_2,'') NUM, dti.ISN_FUND from tblINVENTORY dti
		where dti.Deleted = 0 and dti.PRESENCE_FLAG = 'a'
		and dti.CARRIER_TYPE = 'T' and dti.ISN_INVENTORY_TYPE = 2
	) ti ),
--R.INVENTORY_COUNT_FULL = (
--	select count(ti.NUM) from (
--		select distinct dti.INVENTORY_NUM_1 + isnull(dti.INVENTORY_NUM_2,'') NUM, dti.ISN_FUND from tblINVENTORY dti
--		where dti.Deleted = 0 and dti.PRESENCE_FLAG = 'a' and dti.COPY_COUNT = 3
--		and dti.CARRIER_TYPE = 'T' and dti.ISN_INVENTORY_TYPE = 2
--	) ti ),	
R.INVENTORY_COUNT_FULL = (
	select count(ti.NUM) from (
		select distinct dti.INVENTORY_NUM_1 + isnull(dti.INVENTORY_NUM_2,'') NUM, dti.ISN_FUND from tblINVENTORY dti
		where dti.Deleted = 0 and dti.PRESENCE_FLAG = 'a' and 
		((dti.COPY_COUNT > 2 and dti.ISN_INVENTORY_TYPE IN (1,2,3,4)) or 
		(dti.COPY_COUNT > 1 and dti.ISN_INVENTORY_TYPE IN (5,6,7,8,9,10)))and dti.CARRIER_TYPE = 'T' and dti.ISN_INVENTORY_TYPE = 2
	) ti ),		
R.UNIT_CATALOGUED = (
	select coalesce(sum(td.UNITS_CATALOGUED),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'P' and td.ISN_DOC_TYPE = 2 and td.ISN_INVENTORY is null
)
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE = 2 and R.CARRIER_TYPE = 'P'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (44) Аудиовизуальные документы на традиционных носителях - Кинодокументы

update R set
R.INVENTORY_COUNT = (
	select count(ti.NUM) from (
		select distinct dti.INVENTORY_NUM_1 + isnull(dti.INVENTORY_NUM_2,'') NUM, dti.ISN_FUND from tblINVENTORY dti
		where dti.Deleted = 0 and dti.PRESENCE_FLAG = 'a'
		and dti.CARRIER_TYPE = 'T' and dti.ISN_INVENTORY_TYPE = 7
	) ti ),
--R.INVENTORY_COUNT_FULL = (
--	select count(ti.NUM) from (
--		select distinct dti.INVENTORY_NUM_1 + isnull(dti.INVENTORY_NUM_2,'') NUM, dti.ISN_FUND from tblINVENTORY dti
--		where dti.Deleted = 0 and dti.PRESENCE_FLAG = 'a' and dti.COPY_COUNT = 3
--		and dti.CARRIER_TYPE = 'T' and dti.ISN_INVENTORY_TYPE = 7
--	) ti ),
R.INVENTORY_COUNT_FULL = (
	select count(ti.NUM) from (
		select distinct dti.INVENTORY_NUM_1 + isnull(dti.INVENTORY_NUM_2,'') NUM, dti.ISN_FUND from tblINVENTORY dti
		where dti.Deleted = 0 and dti.PRESENCE_FLAG = 'a' and 
		((dti.COPY_COUNT > 2 and dti.ISN_INVENTORY_TYPE IN (1,2,3,4)) or 
		(dti.COPY_COUNT > 1 and dti.ISN_INVENTORY_TYPE IN (5,6,7,8,9,10)))and dti.CARRIER_TYPE = 'T' and dti.ISN_INVENTORY_TYPE = 7
	) ti ),
R.REG_UNIT_CATALOGUED = (
	select coalesce(sum(td.REG_UNITS_CTALOGUE),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE = 7 and td.ISN_INVENTORY is null
)
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'A'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (45) Аудиовизуальные документы на традиционных носителях - Фотодокументы

update R set
R.INVENTORY_COUNT = (
	select count(ti.NUM) from (
		select distinct dti.INVENTORY_NUM_1 + isnull(dti.INVENTORY_NUM_2,'') NUM, dti.ISN_FUND from tblINVENTORY dti
		where dti.Deleted = 0 and dti.PRESENCE_FLAG = 'a'
		and dti.CARRIER_TYPE = 'T' and dti.ISN_INVENTORY_TYPE = 5
	) ti ),
--R.INVENTORY_COUNT_FULL = (
--	select count(ti.NUM) from (
--		select distinct dti.INVENTORY_NUM_1 + isnull(dti.INVENTORY_NUM_2,'') NUM, dti.ISN_FUND from tblINVENTORY dti
--		where dti.Deleted = 0 and dti.PRESENCE_FLAG = 'a' and dti.COPY_COUNT = 3
--		and dti.CARRIER_TYPE = 'T' and dti.ISN_INVENTORY_TYPE = 5
--	) ti ),	
R.INVENTORY_COUNT_FULL = (
	select count(ti.NUM) from (
		select distinct dti.INVENTORY_NUM_1 + isnull(dti.INVENTORY_NUM_2,'') NUM, dti.ISN_FUND from tblINVENTORY dti
		where dti.Deleted = 0 and dti.PRESENCE_FLAG = 'a' and 
		((dti.COPY_COUNT > 2 and dti.ISN_INVENTORY_TYPE IN (1,2,3,4)) or 
		(dti.COPY_COUNT > 1 and dti.ISN_INVENTORY_TYPE IN (5,6,7,8,9,10)))and dti.CARRIER_TYPE = 'T' and dti.ISN_INVENTORY_TYPE = 5
	) ti ),
R.UNIT_CATALOGUED = (
	select coalesce(sum(td.UNITS_CATALOGUED),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE = 5 and td.ISN_INVENTORY is null
)
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'A'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (46) Аудиовизуальные документы на традиционных носителях - Фонодокументы

update R set
R.INVENTORY_COUNT = (
	select count(ti.NUM) from (
		select distinct dti.INVENTORY_NUM_1 + isnull(dti.INVENTORY_NUM_2,'') NUM, dti.ISN_FUND from tblINVENTORY dti
		where dti.Deleted = 0 and dti.PRESENCE_FLAG = 'a'
		and dti.CARRIER_TYPE = 'T' and dti.ISN_INVENTORY_TYPE = 6
	) ti ),
--R.INVENTORY_COUNT_FULL = (
--	select count(ti.NUM) from (
--		select distinct dti.INVENTORY_NUM_1 + isnull(dti.INVENTORY_NUM_2,'') NUM, dti.ISN_FUND from tblINVENTORY dti
--		where dti.Deleted = 0 and dti.PRESENCE_FLAG = 'a' and dti.COPY_COUNT = 3
--		and dti.CARRIER_TYPE = 'T' and dti.ISN_INVENTORY_TYPE = 6
--	) ti ),
R.INVENTORY_COUNT_FULL = (
	select count(ti.NUM) from (
		select distinct dti.INVENTORY_NUM_1 + isnull(dti.INVENTORY_NUM_2,'') NUM, dti.ISN_FUND from tblINVENTORY dti
		where dti.Deleted = 0 and dti.PRESENCE_FLAG = 'a' and 
		((dti.COPY_COUNT > 2 and dti.ISN_INVENTORY_TYPE IN (1,2,3,4)) or 
		(dti.COPY_COUNT > 1 and dti.ISN_INVENTORY_TYPE IN (5,6,7,8,9,10)))and dti.CARRIER_TYPE = 'T' and dti.ISN_INVENTORY_TYPE = 6
	) ti ),
R.REG_UNIT_CATALOGUED = (
	select coalesce(sum(td.REG_UNITS_CTALOGUE),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE = 6 and td.ISN_INVENTORY is null
)
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'A'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (47) Аудиовизуальные документы на традиционных носителях - Видеодокументы

update R set
R.INVENTORY_COUNT = (
	select count(ti.NUM) from (
		select distinct dti.INVENTORY_NUM_1 + isnull(dti.INVENTORY_NUM_2,'') NUM, dti.ISN_FUND from tblINVENTORY dti
		where dti.Deleted = 0 and dti.PRESENCE_FLAG = 'a'
		and dti.CARRIER_TYPE = 'T' and dti.ISN_INVENTORY_TYPE = 8
	) ti ),
--R.INVENTORY_COUNT_FULL = (
--	select count(ti.NUM) from (
--		select distinct dti.INVENTORY_NUM_1 + isnull(dti.INVENTORY_NUM_2,'') NUM, dti.ISN_FUND from tblINVENTORY dti
--		where dti.Deleted = 0 and dti.PRESENCE_FLAG = 'a' and dti.COPY_COUNT = 3
--		and dti.CARRIER_TYPE = 'T' and dti.ISN_INVENTORY_TYPE = 8
--	) ti ),	
R.INVENTORY_COUNT_FULL = (
	select count(ti.NUM) from (
		select distinct dti.INVENTORY_NUM_1 + isnull(dti.INVENTORY_NUM_2,'') NUM, dti.ISN_FUND from tblINVENTORY dti
		where dti.Deleted = 0 and dti.PRESENCE_FLAG = 'a' and 
		((dti.COPY_COUNT > 2 and dti.ISN_INVENTORY_TYPE IN (1,2,3,4)) or 
		(dti.COPY_COUNT > 1 and dti.ISN_INVENTORY_TYPE IN (5,6,7,8,9,10)))and dti.CARRIER_TYPE = 'T' and dti.ISN_INVENTORY_TYPE = 8
	) ti ),	
R.REG_UNIT_CATALOGUED = (
	select coalesce(sum(td.REG_UNITS_CTALOGUE),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'A' and td.ISN_DOC_TYPE = 8 and td.ISN_INVENTORY is null
)
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'A'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (49) документы на электронных носителях - НТД

update R set
R.INVENTORY_COUNT = (
	select count(ti.NUM) from (
		select distinct dti.INVENTORY_NUM_1 + isnull(dti.INVENTORY_NUM_2,'') NUM, dti.ISN_FUND from tblINVENTORY dti
		where dti.Deleted = 0 and dti.PRESENCE_FLAG = 'a'
		and dti.CARRIER_TYPE = 'E' and dti.ISN_INVENTORY_TYPE = 4
	) ti ),
--R.INVENTORY_COUNT_FULL = (
--	select count(ti.NUM) from (
--		select distinct dti.INVENTORY_NUM_1 + isnull(dti.INVENTORY_NUM_2,'') NUM, dti.ISN_FUND from tblINVENTORY dti
--		where dti.Deleted = 0 and dti.PRESENCE_FLAG = 'a' and dti.COPY_COUNT = 3
--		and dti.CARRIER_TYPE = 'E' and dti.ISN_INVENTORY_TYPE = 4

R.INVENTORY_COUNT_FULL = (
	select count(ti.NUM) from (
		select distinct dti.INVENTORY_NUM_1 + isnull(dti.INVENTORY_NUM_2,'') NUM, dti.ISN_FUND from tblINVENTORY dti
		where dti.Deleted = 0 and dti.PRESENCE_FLAG = 'a' and 
		((dti.COPY_COUNT > 2 and dti.ISN_INVENTORY_TYPE IN (1,2,3,4)) or 
		(dti.COPY_COUNT > 1 and dti.ISN_INVENTORY_TYPE IN (5,6,7,8,9,10)))
		and dti.CARRIER_TYPE = 'E' and dti.ISN_INVENTORY_TYPE = 4
	) ti ),	
R.UNIT_CATALOGUED = (
	select coalesce(sum(td.UNITS_CATALOGUED),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE = 4 and td.ISN_INVENTORY is null
)
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE = 4 and R.CARRIER_TYPE = 'E'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (50) документы на электронных носителях - Кинодокументы

update R set
R.INVENTORY_COUNT = (
	select count(ti.NUM) from (
		select distinct dti.INVENTORY_NUM_1 + isnull(dti.INVENTORY_NUM_2,'') NUM, dti.ISN_FUND from tblINVENTORY dti
		where dti.Deleted = 0 and dti.PRESENCE_FLAG = 'a'
		and dti.CARRIER_TYPE = 'E' and dti.ISN_INVENTORY_TYPE = 7
	) ti ),
R.INVENTORY_COUNT_FULL = (
	select count(ti.NUM) from (
		select distinct dti.INVENTORY_NUM_1 + isnull(dti.INVENTORY_NUM_2,'') NUM, dti.ISN_FUND from tblINVENTORY dti
		where dti.Deleted = 0 and dti.PRESENCE_FLAG = 'a' and dti.COPY_COUNT = 3
		and dti.CARRIER_TYPE = 'E' and dti.ISN_INVENTORY_TYPE = 7
	) ti ),	
R.UNIT_CATALOGUED = (
	select coalesce(sum(td.UNITS_CATALOGUED),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE = 7 and td.ISN_INVENTORY is null
)
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE = 7 and R.CARRIER_TYPE = 'E'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (50 и 3/4) документы на электронных носителях - Видеодокументы

update R set
R.INVENTORY_COUNT = (
	select count(ti.NUM) from (
		select distinct dti.INVENTORY_NUM_1 + isnull(dti.INVENTORY_NUM_2,'') NUM, dti.ISN_FUND from tblINVENTORY dti
		where dti.Deleted = 0 and dti.PRESENCE_FLAG = 'a'
		and dti.CARRIER_TYPE = 'E' and dti.ISN_INVENTORY_TYPE = 8
	) ti ),
R.INVENTORY_COUNT_FULL = (
	select count(ti.NUM) from (
		select distinct dti.INVENTORY_NUM_1 + isnull(dti.INVENTORY_NUM_2,'') NUM, dti.ISN_FUND from tblINVENTORY dti
		where dti.Deleted = 0 and dti.PRESENCE_FLAG = 'a' and dti.COPY_COUNT = 3
		and dti.CARRIER_TYPE = 'E' and dti.ISN_INVENTORY_TYPE = 8
	) ti ),	
R.UNIT_CATALOGUED = (
	select coalesce(sum(td.UNITS_CATALOGUED),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE = 8 and td.ISN_INVENTORY is null
)
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE = 8 and R.CARRIER_TYPE = 'E'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (51) документы на электронных носителях - Фотодокументы

update R set
R.INVENTORY_COUNT = (
	select count(ti.NUM) from (
		select distinct dti.INVENTORY_NUM_1 + isnull(dti.INVENTORY_NUM_2,'') NUM, dti.ISN_FUND from tblINVENTORY dti
		where dti.Deleted = 0 and dti.PRESENCE_FLAG = 'a'
		and dti.CARRIER_TYPE = 'E' and dti.ISN_INVENTORY_TYPE = 5
	) ti ),
R.INVENTORY_COUNT_FULL = (
	select count(ti.NUM) from (
		select distinct dti.INVENTORY_NUM_1 + isnull(dti.INVENTORY_NUM_2,'') NUM, dti.ISN_FUND from tblINVENTORY dti
		where dti.Deleted = 0 and dti.PRESENCE_FLAG = 'a' and dti.COPY_COUNT = 3
		and dti.CARRIER_TYPE = 'E' and dti.ISN_INVENTORY_TYPE = 5
	) ti ),	
R.UNIT_CATALOGUED = (
	select coalesce(sum(td.UNITS_CATALOGUED),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE = 5 and td.ISN_INVENTORY is null
)
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE = 5 and R.CARRIER_TYPE = 'E'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (52) документы на электронных носителях - Фонодокументы

update R set
R.INVENTORY_COUNT = (
	select count(ti.NUM) from (
		select distinct dti.INVENTORY_NUM_1 + isnull(dti.INVENTORY_NUM_2,'') NUM, dti.ISN_FUND from tblINVENTORY dti
		where dti.Deleted = 0 and dti.PRESENCE_FLAG = 'a'
		and dti.CARRIER_TYPE = 'E' and dti.ISN_INVENTORY_TYPE = 6
	) ti ),
R.INVENTORY_COUNT_FULL = (
	select count(ti.NUM) from (
		select distinct dti.INVENTORY_NUM_1 + isnull(dti.INVENTORY_NUM_2,'') NUM, dti.ISN_FUND from tblINVENTORY dti
		where dti.Deleted = 0 and dti.PRESENCE_FLAG = 'a' and dti.COPY_COUNT = 3
		and dti.CARRIER_TYPE = 'E' and dti.ISN_INVENTORY_TYPE = 6
	) ti ),	
R.UNIT_CATALOGUED = (
	select coalesce(sum(td.UNITS_CATALOGUED),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'E' and td.ISN_DOC_TYPE = 6 and td.ISN_INVENTORY is null
)
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE = 6 and R.CARRIER_TYPE = 'E'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (53) Микроформы на правах подлинников

update R set
R.INVENTORY_COUNT = (
	select count(ti.NUM) from (
		select distinct dti.INVENTORY_NUM_1 + isnull(dti.INVENTORY_NUM_2,'') NUM, dti.ISN_FUND from tblINVENTORY dti
		where dti.Deleted = 0 and dti.PRESENCE_FLAG = 'a'
		and dti.CARRIER_TYPE = 'T' and dti.ISN_INVENTORY_TYPE = 9
	) ti ),
R.INVENTORY_COUNT_FULL = (
	select count(ti.NUM) from (
		select distinct dti.INVENTORY_NUM_1 + isnull(dti.INVENTORY_NUM_2,'') NUM, dti.ISN_FUND from tblINVENTORY dti
		where dti.Deleted = 0 and dti.PRESENCE_FLAG = 'a'  and 
		(
            --(
            --    dti.COPY_COUNT > 2 and dti.ISN_INVENTORY_TYPE IN (1,2,3,4)
            --) 
         --or 
            (
                dti.COPY_COUNT > 1 and dti.ISN_INVENTORY_TYPE = 9 --IN (5,6,7,8,9,10)
            )
        )
		and dti.CARRIER_TYPE = 'T' 
	) ti ),	
R.UNIT_CATALOGUED = (
	select coalesce(sum(td.UNITS_CATALOGUED),0) from tblDOCUMENT_STATS td
	where td.ISN_FUND in (select ID from #fnew)
	and td.CARRIER_TYPE = 'M' and td.ISN_DOC_TYPE = 9 and td.ISN_INVENTORY is null
)
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE = 9 and R.CARRIER_TYPE = 'M'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (56) Система справочно-информационных изданий:

update R set
R.GUIDE_COUNT = (
	select count(tp.ID) from tblPublication_CL tp
	where tp.ISN_PUBLICATION_TYPE in (1, 2) and tp.Deleted = 0),
R.INVENTORY_COUNT = (
	select count(tp.ID) from tblPublication_CL tp
	where tp.ISN_PUBLICATION_TYPE = 5 and tp.Deleted = 0),
R.CATALOUGUE_COUNT = (
	select count(tp.ID) from tblPublication_CL tp
	where tp.ISN_PUBLICATION_TYPE = 6 and tp.Deleted = 0),
R.INDEX_COUNT = (
	select count(tp.ID) from tblPublication_CL tp
	where tp.ISN_PUBLICATION_TYPE = 7 and tp.Deleted = 0),
R.REVIEW_COUNT = (
	select count(tp.ID) from tblPublication_CL tp
	where tp.ISN_PUBLICATION_TYPE = 8 and tp.Deleted = 0),
R.HISTORY_COUNT = (
	select count(tp.ID) from tblPublication_CL tp
	where tp.ISN_PUBLICATION_TYPE = 4 and tp.Deleted = 0),
R.ATD_COUNT = (
	select count(tp.ID) from tblPublication_CL tp
	where tp.ISN_PUBLICATION_TYPE = 16433 and tp.Deleted = 0)
from tblARCHIVE_PASSPORT R
where R.ISN_PASSPORT = @ISN_PASSPORT and R.Deleted = 0

update R set R.ALL_SII_COUNT = 
	R.GUIDE_COUNT + R.INVENTORY_COUNT + R.CATALOUGUE_COUNT + R.INDEX_COUNT + R.REVIEW_COUNT + R.HISTORY_COUNT + R.ATD_COUNT
from tblARCHIVE_PASSPORT R
where R.ISN_PASSPORT = @ISN_PASSPORT and R.Deleted = 0

-- (57) Состав и объём научно-справочной библиотеки:

-- (58) Условия хранения документов:

update R set
R.STORAGE_SPACE = (
	select coalesce(sum(ta.TOTAL_SPACE),0) from tblARCHIVE_STORAGE ta
	where ta.ISN_LOCATION in (select ID from #l)),
R.SPACE_WITHOU_SECURITY_PROC = 
	ROUND(R.SPACE_WITHOUT_SECUR / R.TOTAL_SPACE * 100, 0),
R.SPACE_WITHOUT_ALARM_PROC =
	ROUND(R.SPACE_WITHOUT_ALARM / R.TOTAL_SPACE * 100, 0),
R.SHELF_LENGTH = (
	select coalesce(sum(ta.SHELF_LENGTH),0) from tblARCHIVE_STORAGE ta
	where ta.ISN_LOCATION in (select ID from #l)),
R.METAL_SHELF_LENGTH = (
	select coalesce(sum(ta.METAL_SHELF_LENGTH),0) from tblARCHIVE_STORAGE ta
	where ta.ISN_LOCATION in (select ID from #l)),
R.FREE_SHELF_LENGTH = (
	select coalesce(sum(ta.FREE_SHELF_LENGTH),0) from tblARCHIVE_STORAGE ta
	where ta.ISN_LOCATION in (select ID from #l)),
R.UNITS_CARDBOARDED = (
	select coalesce(sum(tc.CARDBOARDED),0) from tblFUND_CHECK tc
	where tc.ISN_FUND in (select ID from #f) )
from tblARCHIVE_PASSPORT R
where R.ISN_PASSPORT = @ISN_PASSPORT and R.Deleted = 0

-- [2]

-- (01) Документы на бумажных носителях - Всего

update R set
R.FUND_COUNT = (
	select coalesce(sum(ta.FUND_COUNT),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = R.ISN_PASSPORT
	and ta.CARRIER_TYPE = 'P' and ta.ISN_DOC_TYPE in (1, 2, 3, 4)
),
R.UNIT_COUNT = (
	select coalesce(sum(ta.UNIT_COUNT),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = R.ISN_PASSPORT
	and ta.CARRIER_TYPE = 'P' and ta.ISN_DOC_TYPE in (1, 2, 3, 4)
),
R.UNIT_INVENTORY = (
	select coalesce(sum(ta.UNIT_INVENTORY),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = R.ISN_PASSPORT
	and ta.CARRIER_TYPE = 'P' and ta.ISN_DOC_TYPE in (1, 2, 3, 4)
),
R.UNIT_SECRET = (
	select coalesce(sum(ta.UNIT_SECRET),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = R.ISN_PASSPORT
	and ta.CARRIER_TYPE = 'P' and ta.ISN_DOC_TYPE in (1, 2, 3, 4)
),
R.UNIT_UNIQUE = (
	select coalesce(sum(ta.UNIT_UNIQUE),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = R.ISN_PASSPORT
	and ta.CARRIER_TYPE = 'P' and ta.ISN_DOC_TYPE in (1, 2, 3, 4)
),
R.UNIT_OC = (
	select coalesce(sum(ta.UNIT_OC),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = R.ISN_PASSPORT
	and ta.CARRIER_TYPE = 'P' and ta.ISN_DOC_TYPE in (1, 2, 3, 4)
),
R.UNIT_DEP = (
	select coalesce(sum(ta.UNIT_DEP),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = R.ISN_PASSPORT
	and ta.CARRIER_TYPE = 'P' and ta.ISN_DOC_TYPE in (1, 2, 3, 4)
)
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE is null and R.CARRIER_TYPE = 'P'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (06) Аудиовизуальные документы на традиционных носителях - Всего

update R set
R.FUND_COUNT = (
	select coalesce(sum(ta.FUND_COUNT),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = R.ISN_PASSPORT
	and ta.CARRIER_TYPE = 'A' and ta.ISN_DOC_TYPE in (5, 6, 7, 8)
),
R.UNIT_COUNT = (
	select coalesce(sum(ta.UNIT_COUNT),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = R.ISN_PASSPORT
	and ta.CARRIER_TYPE = 'A' and ta.ISN_DOC_TYPE in (5, 6, 7, 8)
),
R.UNIT_INVENTORY = (
	select coalesce(sum(ta.UNIT_INVENTORY),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = R.ISN_PASSPORT
	and ta.CARRIER_TYPE = 'A' and ta.ISN_DOC_TYPE in (5, 6, 7, 8)
),
R.UNIT_SECRET = (
	select coalesce(sum(ta.UNIT_SECRET),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = R.ISN_PASSPORT
	and ta.CARRIER_TYPE = 'A' and ta.ISN_DOC_TYPE in (5, 6, 7, 8)
),
R.UNIT_UNIQUE = (
	select coalesce(sum(ta.UNIT_UNIQUE),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = R.ISN_PASSPORT
	and ta.CARRIER_TYPE = 'A' and ta.ISN_DOC_TYPE in (5, 6, 7, 8)
),
R.UNIT_OC = (
	select coalesce(sum(ta.UNIT_OC),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = R.ISN_PASSPORT
	and ta.CARRIER_TYPE = 'A' and ta.ISN_DOC_TYPE in (5, 6, 7, 8)
),
R.UNIT_DEP = (
	select coalesce(sum(ta.UNIT_DEP),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = R.ISN_PASSPORT
	and ta.CARRIER_TYPE = 'A' and ta.ISN_DOC_TYPE in (5, 6, 7, 8)
)
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE is null and R.CARRIER_TYPE = 'A'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (43) Аудиовизуальные документы на традиционных носителях - Всего ед. уч.

update R set
R.REG_UNIT_CATALOGUED = (
	select coalesce(sum(ta.REG_UNIT_CATALOGUED),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = R.ISN_PASSPORT
	and ta.CARRIER_TYPE = 'A' and ta.ISN_DOC_TYPE in (5, 6, 7, 8)
)
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE is null and R.CARRIER_TYPE = 'A'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (11) документы на электронных носителях - Всего

update R set
R.FUND_COUNT = (
	select coalesce(sum(ta.FUND_COUNT),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = R.ISN_PASSPORT
	and ta.CARRIER_TYPE = 'E' and ta.ISN_DOC_TYPE in (4, 5, 6, 7)
),
R.UNIT_COUNT = (
	select coalesce(sum(ta.UNIT_COUNT),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = R.ISN_PASSPORT
	and ta.CARRIER_TYPE = 'E' and ta.ISN_DOC_TYPE in (4, 5, 6, 7)
),
R.UNIT_INVENTORY = (
	select coalesce(sum(ta.UNIT_INVENTORY),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = R.ISN_PASSPORT
	and ta.CARRIER_TYPE = 'E' and ta.ISN_DOC_TYPE in (4, 5, 6, 7)
),
R.UNIT_SECRET = (
	select coalesce(sum(ta.UNIT_SECRET),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = R.ISN_PASSPORT
	and ta.CARRIER_TYPE = 'E' and ta.ISN_DOC_TYPE in (4, 5, 6, 7)
),
R.UNIT_UNIQUE = (
	select coalesce(sum(ta.UNIT_UNIQUE),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = R.ISN_PASSPORT
	and ta.CARRIER_TYPE = 'E' and ta.ISN_DOC_TYPE in (4, 5, 6, 7)
),
R.UNIT_OC = (
	select coalesce(sum(ta.UNIT_OC),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = R.ISN_PASSPORT
	and ta.CARRIER_TYPE = 'E' and ta.ISN_DOC_TYPE in (4, 5, 6, 7)
),
R.UNIT_DEP = (
	select coalesce(sum(ta.UNIT_DEP),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = R.ISN_PASSPORT
	and ta.CARRIER_TYPE = 'E' and ta.ISN_DOC_TYPE in (4, 5, 6, 7)
),
R.REG_UNIT_COUNT = (
	select coalesce(sum(ta.REG_UNIT_COUNT),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = R.ISN_PASSPORT
	and ta.CARRIER_TYPE = 'E' and ta.ISN_DOC_TYPE in (4, 5, 6, 7)
),
R.REG_UNIT_INVENTORY = (
	select coalesce(sum(ta.REG_UNIT_INVENTORY),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = R.ISN_PASSPORT
	and ta.CARRIER_TYPE = 'E' and ta.ISN_DOC_TYPE in (4, 5, 6, 7)
)
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE is null and R.CARRIER_TYPE = 'E'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (17) Документы ЛП, не внесённые в описи

update P set
P.LP_DOC_COUNT = (
	select coalesce(sum(tf.PERSONAL_UNDESCRIBED_DOC_COUNT),0) from tblFUND tf
	where tf.ISN_FUND in (select ID from #fnew)
	and tf.CARRIER_TYPE = 'T' and tf.ISN_DOC_TYPE = 3)
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE = 3 and R.CARRIER_TYPE = 'P'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (18) Всего кадров

update P set
P.MICROFORM_FRAME_COUNT = (
	select sum(tm.FRAME_COUNT) from tblUNIT_MICROFORM tm
	join tblUNIT tu on tm.ISN_UNIT = tu.ISN_UNIT
	join tblINVENTORY ti on tu.ISN_INVENTORY = ti.ISN_INVENTORY
	where tu.ISN_DOC_TYPE = 9
	and ti.PRESENCE_FLAG = 'a' and tu.IS_LOST = 'N')
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (20) Документы на бумажных носителях - Всего

update R set
R.UNIT_HAS_SF = (
	select coalesce(sum(ta.UNIT_HAS_SF),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = R.ISN_PASSPORT
	and ta.CARRIER_TYPE = 'P' and ta.ISN_DOC_TYPE in (1, 2, 3, 4)
),
R.UNIT_HAS_FP = (
	select coalesce(sum(ta.UNIT_HAS_FP),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = R.ISN_PASSPORT
	and ta.CARRIER_TYPE = 'P' and ta.ISN_DOC_TYPE in (1, 2, 3, 4)
)
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE is null and R.CARRIER_TYPE = 'P'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (25) Аудиовизуальные документы на традиционных носителях - Всего

update R set
R.UNIT_HAS_SF = (
	select coalesce(sum(ta.UNIT_HAS_SF),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = R.ISN_PASSPORT
	and ta.CARRIER_TYPE = 'A' and ta.ISN_DOC_TYPE in (5, 6, 7, 8)
),
R.UNIT_HAS_FP = (
	select coalesce(sum(ta.UNIT_HAS_FP),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = R.ISN_PASSPORT
	and ta.CARRIER_TYPE = 'A' and ta.ISN_DOC_TYPE in (5, 6, 7, 8)
)
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT 
where R.ISN_DOC_TYPE is null and R.CARRIER_TYPE = 'A'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (30) документы на электронных носителях - Всего

update R set
R.UNIT_HAS_SF = (
	select coalesce(sum(ta.UNIT_HAS_SF),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = R.ISN_PASSPORT
	and ta.CARRIER_TYPE = 'E' and ta.ISN_DOC_TYPE in (4, 5, 6, 7)
),
R.UNIT_HAS_FP = (
	select coalesce(sum(ta.UNIT_HAS_FP),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = R.ISN_PASSPORT
	and ta.CARRIER_TYPE = 'E' and ta.ISN_DOC_TYPE in (4, 5, 6, 7)
)
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE is null and R.CARRIER_TYPE = 'E'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (36) ИТОГО

update R set
R.UNIT_HAS_SF = (
	select coalesce(sum(ta.UNIT_HAS_SF),0) from tblArchive_Stats ta
	where ta.ISN_PASSPORT = R.ISN_PASSPORT
	and ta.CARRIER_TYPE in ('P', 'A', 'E', 'M') and (ta.ISN_DOC_TYPE is null or ta.ISN_DOC_TYPE = 9)
),
R.UNIT_HAS_FP = (
	select coalesce(sum(ta.UNIT_HAS_FP),0) from tblArchive_Stats ta
	where ta.ISN_PASSPORT = R.ISN_PASSPORT
	and ta.CARRIER_TYPE in ('P', 'A', 'E', 'M') and (ta.ISN_DOC_TYPE is null or ta.ISN_DOC_TYPE = 9)
),
R.NEGATIVE_COUNT = (
	select coalesce(sum(ta.NEGATIVE_COUNT),0) from tblArchive_Stats ta
	where ta.ISN_PASSPORT = R.ISN_PASSPORT
	and ta.CARRIER_TYPE in ('P', 'A', 'E', 'M') and (ta.ISN_DOC_TYPE is null or ta.ISN_DOC_TYPE = 9)
),
R.SF_COUNT = (
	select coalesce(sum(ta.SF_COUNT),0) from tblArchive_Stats ta
	where ta.ISN_PASSPORT = R.ISN_PASSPORT
	and ta.CARRIER_TYPE in ('P', 'A', 'E', 'M') and (ta.ISN_DOC_TYPE is null or ta.ISN_DOC_TYPE = 9)
)
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE is null and R.CARRIER_TYPE is null
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (37) Документы на бумажных носителях - Всего

update R set
R.INVENTORY_COUNT = (
	select coalesce(sum(ta.INVENTORY_COUNT),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = R.ISN_PASSPORT
	and ta.CARRIER_TYPE = 'P' and ta.ISN_DOC_TYPE in (1, 2, 3, 4)
),
R.INVENTORY_COUNT_FULL = (
	select coalesce(sum(ta.INVENTORY_COUNT_FULL),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = R.ISN_PASSPORT
	and ta.CARRIER_TYPE = 'P' and ta.ISN_DOC_TYPE in (1, 2, 3, 4)
),
R.UNIT_CATALOGUED = (
	select coalesce(sum(ta.UNIT_CATALOGUED),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = R.ISN_PASSPORT
	and ta.CARRIER_TYPE = 'P' and ta.ISN_DOC_TYPE in (1, 2, 3, 4)
)
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE is null and R.CARRIER_TYPE = 'P'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (42) Аудиовизуальные документы на традиционных носителях - Всего ед. хр.

update R set
R.INVENTORY_COUNT = (
	select coalesce(sum(ta.INVENTORY_COUNT),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = R.ISN_PASSPORT
	and ta.CARRIER_TYPE = 'A' and ta.ISN_DOC_TYPE in (5, 6, 7, 8)
),
R.INVENTORY_COUNT_FULL = (
	select coalesce(sum(ta.INVENTORY_COUNT_FULL),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = R.ISN_PASSPORT
	and ta.CARRIER_TYPE = 'A' and ta.ISN_DOC_TYPE in (5, 6, 7, 8)
),
R.UNIT_CATALOGUED = (
	select coalesce(sum(ta.UNIT_CATALOGUED),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = R.ISN_PASSPORT
	and ta.CARRIER_TYPE = 'A' and ta.ISN_DOC_TYPE in (5, 6, 7, 8)
)
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE is null and R.CARRIER_TYPE = 'A'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (48) документы на электронных носителях - Всего

update R set
R.INVENTORY_COUNT = (
	select coalesce(sum(ta.INVENTORY_COUNT),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = P.ISN_PASSPORT
	and ta.CARRIER_TYPE = 'E' and ta.ISN_DOC_TYPE in (4, 5, 6, 7)
),
R.INVENTORY_COUNT_FULL = (
	select coalesce(sum(ta.INVENTORY_COUNT_FULL),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = P.ISN_PASSPORT
	and ta.CARRIER_TYPE = 'E' and ta.ISN_DOC_TYPE in (4, 5, 6, 7)
),
R.UNIT_CATALOGUED = (
	select coalesce(sum(ta.UNIT_CATALOGUED),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = P.ISN_PASSPORT
	and ta.CARRIER_TYPE = 'E' and ta.ISN_DOC_TYPE in (4, 5, 6, 7)
)
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE is null and R.CARRIER_TYPE = 'E'
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- [3]

-- (19) ИТОГО

update R set
R.FUND_COUNT = (
	select coalesce(sum(ta.FUND_COUNT),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = R.ISN_PASSPORT
	and ta.CARRIER_TYPE in ('P', 'A', 'E', 'M') and (ta.ISN_DOC_TYPE is null or ta.ISN_DOC_TYPE = 9)
),
R.UNIT_COUNT = (
	select coalesce(sum(ta.UNIT_COUNT),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = R.ISN_PASSPORT
	and ta.CARRIER_TYPE in ('P', 'A', 'E', 'M') and (ta.ISN_DOC_TYPE is null or ta.ISN_DOC_TYPE = 9)
),
R.UNIT_INVENTORY = (
	select coalesce(sum(ta.UNIT_INVENTORY),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = R.ISN_PASSPORT
	and ta.CARRIER_TYPE in ('P', 'A', 'E', 'M') and (ta.ISN_DOC_TYPE is null or ta.ISN_DOC_TYPE = 9)
),
R.UNIT_SECRET = (
	select coalesce(sum(ta.UNIT_SECRET),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = R.ISN_PASSPORT
	and ta.CARRIER_TYPE in ('P', 'A', 'E', 'M') and (ta.ISN_DOC_TYPE is null or ta.ISN_DOC_TYPE = 9)
),
R.UNIT_UNIQUE = (
	select coalesce(sum(ta.UNIT_UNIQUE),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = R.ISN_PASSPORT
	and ta.CARRIER_TYPE in ('P', 'A', 'E', 'M') and (ta.ISN_DOC_TYPE is null or ta.ISN_DOC_TYPE = 9)
),
R.UNIT_OC = (
	select coalesce(sum(ta.UNIT_OC),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = R.ISN_PASSPORT
	and ta.CARRIER_TYPE in ('P', 'A', 'E', 'M') and (ta.ISN_DOC_TYPE is null or ta.ISN_DOC_TYPE = 9)
),
R.UNIT_DEP = (
	select coalesce(sum(ta.UNIT_DEP),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = R.ISN_PASSPORT
	and ta.CARRIER_TYPE in ('P', 'A', 'E', 'M') and (ta.ISN_DOC_TYPE is null or ta.ISN_DOC_TYPE = 9)
)
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE is null and R.CARRIER_TYPE is null
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (54) ИТОГО ед. хран

update R set
R.INVENTORY_COUNT = (
	select coalesce(sum(ta.INVENTORY_COUNT),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = P.ISN_PASSPORT
	and ta.CARRIER_TYPE in ('P', 'A', 'E', 'M') and ta.ISN_DOC_TYPE is null or ta.ISN_DOC_TYPE = 9
),
R.INVENTORY_COUNT_FULL = (
	select coalesce(sum(ta.INVENTORY_COUNT_FULL),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = P.ISN_PASSPORT
	and ta.CARRIER_TYPE in ('P', 'A', 'E', 'M') and ta.ISN_DOC_TYPE is null or ta.ISN_DOC_TYPE = 9
),
R.EL_INVENTORY_COUNT = (
	select coalesce(sum(ta.EL_INVENTORY_COUNT),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = P.ISN_PASSPORT
	and ta.CARRIER_TYPE in ('P', 'A', 'E', 'M') and ta.ISN_DOC_TYPE is null or ta.ISN_DOC_TYPE = 9
),
R.UNIT_CATALOGUED = (
	select coalesce(sum(ta.UNIT_CATALOGUED),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = P.ISN_PASSPORT
	and ta.CARRIER_TYPE in ('P', 'A', 'E', 'M') and ta.ISN_DOC_TYPE is null or ta.ISN_DOC_TYPE = 9
),
R.PAPER_CARD_COUNT = (
	select coalesce(sum(ta.PAPER_CARD_COUNT),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = P.ISN_PASSPORT
	and ta.CARRIER_TYPE in ('P', 'A', 'E', 'M') and ta.ISN_DOC_TYPE is null or ta.ISN_DOC_TYPE = 9
),
R.EL_RECORD_COUNT = (
	select coalesce(sum(ta.EL_RECORD_COUNT),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = P.ISN_PASSPORT
	and ta.CARRIER_TYPE in ('P', 'A', 'E', 'M') and ta.ISN_DOC_TYPE is null or ta.ISN_DOC_TYPE = 9
),
R.EL_CATALOGUE_VOLUME = (
	select coalesce(sum(ta.EL_CATALOGUE_VOLUME),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = P.ISN_PASSPORT
	and ta.CARRIER_TYPE in ('P', 'A', 'E', 'M') and ta.ISN_DOC_TYPE is null or ta.ISN_DOC_TYPE = 9
),
R.DB_COUNT = (
	select coalesce(sum(ta.DB_COUNT),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = P.ISN_PASSPORT
	and ta.CARRIER_TYPE in ('P', 'A', 'E', 'M') and ta.ISN_DOC_TYPE is null or ta.ISN_DOC_TYPE = 9
),
R.DB_RECORD_COUNT = (
	select coalesce(sum(ta.DB_RECORD_COUNT),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = P.ISN_PASSPORT
	and ta.CARRIER_TYPE in ('P', 'A', 'E', 'M') and ta.ISN_DOC_TYPE is null or ta.ISN_DOC_TYPE = 9
),
R.DB_VOLUME = (
	select coalesce(sum(ta.DB_VOLUME),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = P.ISN_PASSPORT
	and ta.CARRIER_TYPE in ('P', 'A', 'E', 'M') and ta.ISN_DOC_TYPE is null or ta.ISN_DOC_TYPE = 9
)
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE is null and R.CARRIER_TYPE is null
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0

-- (55) ИТОГО ед. уч

update R set
R.REG_UNIT_CATALOGUED = (
	select coalesce(sum(ta.REG_UNIT_CATALOGUED),0) from tblARCHIVE_STATS ta
	where ta.ISN_PASSPORT = P.ISN_PASSPORT
	and ta.CARRIER_TYPE in ('P', 'A', 'E', 'M') and (ta.ISN_DOC_TYPE is null or ta.ISN_DOC_TYPE = 9)
)
from tblARCHIVE_STATS R join tblARCHIVE_PASSPORT P on R.ISN_PASSPORT = P.ISN_PASSPORT
where R.ISN_DOC_TYPE is null and R.CARRIER_TYPE is null
and R.ISN_PASSPORT = @ISN_PASSPORT and P.Deleted = 0



                                        </sql>}
#End Region

    Private AppSettings As eqAppSettings = Nothing
    Public Command As SqlCommand = Nothing

    Public Enum Type
        Fund = 0
        Inventory = 1
        Unit = 2
        Passport = 3
    End Enum

    Public Sub New(ByVal AppSettings As eqAppSettings)
        Me.AppSettings = AppSettings
        Command = New SqlCommand() With {.CommandTimeout = 3600}
    End Sub

    Public Sub Calc(ByVal Type As Type, Optional ByVal ID As String = "")
        Dim MyTransaction = Not AppSettings.HasTransaction

        Try
            AppSettings.BeginTransaction()

            Select Case Type
                Case Recalc.Type.Fund
                    Command.CommandText = SQL(Type).Value.Replace( _
                        "/*where*/", _
                        IIf(ID = "", _
                            "1=1", _
                            "R.ISN_FUND=(select j.ISN_FUND from tblFUND j where j.ID='" & ID & "')") _
                        & " AND R.ISN_FUND IN (select j.ISN_FUND from tblFUND j where NOT (ISNULL(j.ForbidRecalc, 'N') = 'Y')) ")
                Case Recalc.Type.Inventory
                    Command.CommandText = SQL(Type).Value.Replace( _
                        "/*where*/", _
                        IIf(ID = "", _
                            "1=1", _
                            "R.ISN_INVENTORY=(select j.ISN_INVENTORY from tblINVENTORY j where j.ID='" & ID & "')") _
                        & " AND R.ISN_INVENTORY IN (select j.ISN_INVENTORY from tblINVENTORY j where NOT (ISNULL(j.ForbidRecalc, 'N') = 'Y')) ")
                Case Recalc.Type.Unit
                    Command.CommandText = SQL(Type).Value.Replace("/*where*/", IIf(ID = "", "1=1", "R.ISN_UNIT=(select j.ISN_UNIT from tblUNIT j where j.ID='" & ID & "')"))
                Case Recalc.Type.Passport
                    Command.CommandText = SQL(Type).Value
                    Command.Parameters.Add(New SqlParameter("@ISN_PASSPORT", SqlDbType.BigInt) With {.Value = ID})
            End Select

            Command.Connection = AppSettings.Cnn
            Command.Transaction = AppSettings.Trn
            Command.ExecuteNonQuery()

            If MyTransaction Then
                AppSettings.CommitTransaction()
            End If
        Catch ex As Exception
            If MyTransaction Then
                AppSettings.RollbackTransaction()
            End If
            Throw ex
        End Try
    End Sub

    Public Sub CalcDocument()
        Dim MyTransaction = Not AppSettings.HasTransaction

        Dim docs As DataTable = Nothing
        Dim page As Triplet = Nothing

        Try
            Command.Connection = AppSettings.Cnn
            Command.Transaction = AppSettings.Trn
            Command.CommandText = "SELECT ID, NOTE FROM tblDOCUMENT WHERE Deleted = 0"

            docs = AppSettings.GetDataTable(Command)
            Command.CommandText = "UPDATE tblDOCUMENT SET PAGE_COUNT = @PageCount, PAGE_FROM = @PageFrom, PAGE_TO = @PageTo WHERE ID = @ID"

            For Each doc In docs.Rows
                CalcDocument(doc("NOTE").ToString, page)

                Command.Parameters.Clear()
                Command.Parameters.AddWithValue("@ID", doc("ID"))
                Command.Parameters.AddWithValue("@PageCount", page.First)
                Command.Parameters.AddWithValue("@PageFrom", page.Second)
                Command.Parameters.AddWithValue("@PageTo", page.Third)
                Command.ExecuteNonQuery()
            Next
            If MyTransaction Then
                AppSettings.CommitTransaction()
            End If
        Catch ex As Exception
            If MyTransaction Then
                AppSettings.RollbackTransaction()
            End If
            Throw ex
        End Try
    End Sub

    Public Sub CalcDocument(ByVal Note As String, ByRef Page As Triplet)
        Dim Result As New Triplet(DBNull.Value, DBNull.Value, DBNull.Value)

        If Not String.IsNullOrEmpty(Note) Then
            Dim lsts As New List(Of Integer)
            Dim lstsArr = Note.Replace(" ", "").Split(New Char() {","c}, StringSplitOptions.RemoveEmptyEntries)

            For Each lst In lstsArr
                ' Ищем диапозон листов
                Dim m = Regex.Match(lst, "((?<l1>\d+)(?<t1>.*)?)-((?<l2>\d+)(?<t2>.*))")
                If m.Success Then
                    ' Записываем номера листов
                    For i As Integer = CInt(m.Groups("l1").Value) To CInt(m.Groups("l2").Value)
                        lsts.Add(i)
                    Next
                    Continue For
                End If

                ' Всего один лист
                m = Regex.Match(lst, "(?<l>\d+)(?<t>.*)")
                If m.Success Then
                    ' Если после номера не более одной буквы то это разные листы
                    If lsts.Contains(CInt(m.Groups("l").Value)) Then
                        If m.Groups("t").Value.Length < 2 Then
                            lsts.Add(CInt(m.Groups("l").Value))
                        End If
                        Continue For
                    End If
                    lsts.Add(CInt(m.Groups("l").Value))
                    Continue For
                End If
            Next

            ' Выводим сумму и диапозон
            Result.First = lsts.Count
            If lsts.Count > 0 Then
                Result.Second = lsts.Min
                Result.Third = lsts.Max
            End If
        End If
        Page = Result
    End Sub

End Class
