﻿Imports System
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic

<HideModuleName()> _
Module ServiceModule

    ''' <summary>
    ''' Проверяет как давно создавался бэкап БД
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub CheckBackup(ByVal AppSettings As eqAppSettings)
        Dim cmd As New SqlCommand("SELECT * FROM tblService WHERE Deleted=0")
        Dim dt = AppSettings.GetDataTable(cmd)

        If dt.Rows.Count > 0 Then
            cmd = New SqlCommand("SELECT * FROM tblServiceLog WHERE DocID = '" & dt.Rows(0)("ID").ToString & "' ORDER BY CreationDateTime DESC")
            Dim dt2 = AppSettings.GetDataTable(cmd)

            If dt2.Rows.Count > 0 Then
                If DateAdd(DateInterval.Day, dt.Rows(0)("BackupInterval"), dt2.Rows(0)("CreationDateTime")) > Now Then
                    Exit Sub
                End If
            End If
            Throw New Exception("Резервная копия базы данных устарела. Необходимо создать резервную копию.")
        Else
            Throw New Exception("Отсутствует запись о настройках резервного копирования.")
        End If
    End Sub

End Module
