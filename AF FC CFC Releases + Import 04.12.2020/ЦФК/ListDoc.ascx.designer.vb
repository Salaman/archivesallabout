Option Strict On
Option Explicit On


Partial Public Class ListDoc

    '''<summary>
    '''UpdatePanelTableListDoc control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdatePanelTableListDoc As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''ViewFunctionsTable control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ViewFunctionsTable As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''RoleFunctionsTable control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RoleFunctionsTable As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''TableListDoc control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TableListDoc As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''PlaceHolderShowListDocPreFilter control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PlaceHolderShowListDocPreFilter As Global.System.Web.UI.WebControls.PlaceHolder

    '''<summary>
    '''PanelListDocPreFilter control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PanelListDocPreFilter As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''PanelListDocPreFilterTitle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PanelListDocPreFilterTitle As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''LabelListDocPreFilterTitle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LabelListDocPreFilterTitle As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''PanelListDocPreFilterContent control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PanelListDocPreFilterContent As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''PlaceHolderListDocPreFilter control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PlaceHolderListDocPreFilter As Global.System.Web.UI.WebControls.PlaceHolder

    '''<summary>
    '''ButtonListDocPreFilterOk control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ButtonListDocPreFilterOk As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''ButtonShowListDocPreFilter control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ButtonShowListDocPreFilter As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''ModalPopupExtenderListDocPreFilter control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ModalPopupExtenderListDocPreFilter As Global.AjaxControlToolkit.ModalPopupExtender

    '''<summary>
    '''UploadFilePopup control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UploadFilePopup As Global.AjaxControlToolkit.ModalPopupExtender

    '''<summary>
    '''hiddenBtn control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hiddenBtn As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''UploadFilePanel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UploadFilePanel As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''PanelMessagesTitle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PanelMessagesTitle As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''UpdatePanelUploadFile control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdatePanelUploadFile As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''FileUpload control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents FileUpload As Global.System.Web.UI.WebControls.FileUpload

    '''<summary>
    '''ImageButtonMessagesCancel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ImageButtonMessagesCancel As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''btnFileUploadOk control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnFileUploadOk As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnFileUploadCancel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnFileUploadCancel As Global.System.Web.UI.WebControls.Button
End Class
