﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:2.0.50727.8745
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On



Partial Public Class _Default

    '''<summary>
    '''PanelSessionMonitor control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PanelSessionMonitor As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''PanelLoggedIn control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PanelLoggedIn As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''LocalizeUser control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LocalizeUser As Global.System.Web.UI.WebControls.Localize

    '''<summary>
    '''HyperLinkUser control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents HyperLinkUser As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''LogOut control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LogOut As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''PanelLoggedOut control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PanelLoggedOut As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''LocalizeLogin control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LocalizeLogin As Global.System.Web.UI.WebControls.Localize

    '''<summary>
    '''LocalizePassword control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LocalizePassword As Global.System.Web.UI.WebControls.Localize

    '''<summary>
    '''tbLogin control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tbLogin As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''tbPassword control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tbPassword As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''LogIn control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LogIn As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''LocalizeRegistration control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LocalizeRegistration As Global.System.Web.UI.WebControls.Localize

    '''<summary>
    '''LinkRemember control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LinkRemember As Global.System.Web.UI.HtmlControls.HtmlAnchor

    '''<summary>
    '''LocalizeRemember control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LocalizeRemember As Global.System.Web.UI.WebControls.Localize

    '''<summary>
    '''PopupControlExtenderRemember control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PopupControlExtenderRemember As Global.AjaxControlToolkit.PopupControlExtender

    '''<summary>
    '''PanelRemember control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PanelRemember As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''LocalizeEnterYourEmail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LocalizeEnterYourEmail As Global.System.Web.UI.WebControls.Localize

    '''<summary>
    '''EmailRemember control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents EmailRemember As Global.Equipage.WebUI.eqTextBox

    '''<summary>
    '''SendRemember control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents SendRemember As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''PanelFakeUser control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PanelFakeUser As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''FakeUserLabel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents FakeUserLabel As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''FakeUserID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents FakeUserID As Global.Equipage.WebUI.eqDocument

    '''<summary>
    '''FakeUserLogOut control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents FakeUserLogOut As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''PanelCheckBackup control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PanelCheckBackup As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''LabelCheckBackup control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LabelCheckBackup As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''LinkCheckButton control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LinkCheckButton As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''PanelContentPlaceHolder control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PanelContentPlaceHolder As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''ButtonContentPlaceHolder control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ButtonContentPlaceHolder As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''TableSystemMenuDocument control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TableSystemMenuDocument As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''TableCellSystemMenu control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TableCellSystemMenu As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''UpdatePanelMenuSystem control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdatePanelMenuSystem As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''ShowHideButton control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ShowHideButton As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''MenuSystem control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents MenuSystem As Global.Equipage.WebUI.eqMenu

    '''<summary>
    '''TableCellDocument control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TableCellDocument As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''UpdatePanelPlaceHolderContainer control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdatePanelPlaceHolderContainer As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''PlaceHolderContainer control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PlaceHolderContainer As Global.System.Web.UI.WebControls.PlaceHolder

    '''<summary>
    '''UpdatePanelModalMessages control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdatePanelModalMessages As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''PlaceHolderModalMessages control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PlaceHolderModalMessages As Global.System.Web.UI.WebControls.PlaceHolder

    '''<summary>
    '''PanelMessages control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PanelMessages As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''PanelMessagesTitle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PanelMessagesTitle As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''LabelMessagesTitle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LabelMessagesTitle As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ImageButtonMessagesOk control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ImageButtonMessagesOk As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''PanelMessagesContent control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PanelMessagesContent As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''PlaceHolderMessages control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PlaceHolderMessages As Global.System.Web.UI.WebControls.PlaceHolder

    '''<summary>
    '''PanelMessagesButton control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PanelMessagesButton As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''PlaceHolderButtons control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PlaceHolderButtons As Global.System.Web.UI.WebControls.PlaceHolder

    '''<summary>
    '''ButtonShowMessages control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ButtonShowMessages As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''ModalPopupExtenderMessages control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ModalPopupExtenderMessages As Global.AjaxControlToolkit.ModalPopupExtender

    '''<summary>
    '''UpdatePanelModalDialog control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdatePanelModalDialog As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''PlaceHolderModalDialog control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PlaceHolderModalDialog As Global.System.Web.UI.WebControls.PlaceHolder

    '''<summary>
    '''PanelDialog control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PanelDialog As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''PanelDialogTitle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PanelDialogTitle As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''LabelDialogTitle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LabelDialogTitle As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ImageButtonDialogOk control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ImageButtonDialogOk As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''PanelDialogContent control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PanelDialogContent As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''ButtonShowDialog control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ButtonShowDialog As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''ModalPopupExtenderDialog control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ModalPopupExtenderDialog As Global.AjaxControlToolkit.ModalPopupExtender

    '''<summary>
    '''Master property.
    '''</summary>
    '''<remarks>
    '''Auto-generated property.
    '''</remarks>
    Public Shadows ReadOnly Property Master() As WebApplication.Site
        Get
            Return CType(MyBase.Master, WebApplication.Site)
        End Get
    End Property
End Class
