﻿Public Partial Class DefaultMain
    Inherits BaseUserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        ' MainPageText
#If CFC Then
        MainText.Controls.Add(LoadControl("~\MainPageTextCFC.ascx"))
#ElseIf GR Then
        MainText.Controls.Add(LoadControl("~\MainPageTextGR.ascx"))
#ElseIf FC Then
        MainText.Controls.Add(LoadControl("~\MainPageTextFC.ascx"))
#Else
        MainText.Controls.Add(LoadControl("~\MainPageText.ascx"))
#End If

        ' Categories
        Dim Categories = From DT As eqDocType In AppSettings.DocTypes.Values _
                         Where AppSettings.UserSecurities.HasDocumentsAccessMenu(DT.DocTypeID) _
                         Order By DT.Category.SortOrder, DT.SortOrder _
                         Group DT By DT.Category.ID Into DocTypes = Group _
                         Select New With {DocTypes, .C = DocTypes(0).Category}
        For Each Category In Categories
            ' Создаем элемент
            Dim CategoryButton As New HyperLink With {.ID = "CategoryButton" & Category.C.ID.ToString}
            If _Default.NavigateMode = _Default.NavigateModes.Redirect Then
                If Category.C.NavigateUrl <> "" Then
                    CategoryButton.NavigateUrl = Category.C.NavigateUrl
                Else
                    CategoryButton.NavigateUrl = BasePage.GetCategoryURL(Category.C.ID, False)
                End If
            Else
                CategoryButton.NavigateUrl = Page.ClientScript.GetPostBackClientHyperlink(Me, "Category|" & Category.C.ID.ToString)
            End If
            CategoryButton.Text = Category.C.CategoryName
            CategoryButton.ToolTip = Category.C.ToolTip
            CategoryButton.Style.Add("margin", "0px 4px 4px 0px")
            ' Добавляем
            MainCategories.Controls.Add(New LiteralControl("<p>"))
            MainCategories.Controls.Add(CategoryButton)
            MainCategories.Controls.Add(New LiteralControl("</p>"))
        Next
        PHChilds.Visible = Categories.Count > 0

    End Sub

    Public Overrides Sub RaisePostBackEvent(ByVal eventArgument As String)
        Dim Values = eventArgument.Split(New Char() {"|"})
        Select Case Values(0)
            Case "DocType"
                BasePage.IsListDoc = Values(3) = "L"
                BasePage.DocTypeURL = Values(1)
            Case "Category"
                BasePage.CategoryID = New Guid(Values(1))
        End Select
    End Sub

    Public ReadOnly Property CalcUsersCount() As Integer
        Get
            If ViewState("CalcUsersCount") Is Nothing Then
                Dim cmd As New SqlClient.SqlCommand("SELECT COUNT(*) FROM eqUsers WHERE Deleted=0")
                ViewState("CalcUsersCount") = AppSettings.ExecCommandScalar(cmd)
            End If
            Return DirectCast(ViewState("CalcUsersCount"), Integer)
        End Get
    End Property

End Class