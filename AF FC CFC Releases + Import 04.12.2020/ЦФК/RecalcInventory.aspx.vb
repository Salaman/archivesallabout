﻿Imports System
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient


Partial Public Class RecalcInventory
    Inherits BasePage


    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        MultiViewTabs.ActiveViewIndex = CInt(MenuTabs.SelectedValue)
    End Sub

    Private Sub bGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles bGo.Click
        Dim r As New Recalc(AppSettings)

        r.Command.Parameters.AddWithValue("@A_NACH_DATA", c_A_NACH_DATA.Checked)
        r.Command.Parameters.AddWithValue("@A_KON_DATA", c_A_KON_DATA.Checked)
        r.Command.Parameters.AddWithValue("@A_MUSEUM", c_A_MUSEUM.Checked)
        r.Command.Parameters.AddWithValue("@A_TREASURE", c_A_TREASURE.Checked)

        r.Command.Parameters.AddWithValue("@SUM_AA", c_SUM_AA.Checked)
        r.Command.Parameters.AddWithValue("@SUM_BO", c_SUM_BO.Checked)
        r.Command.Parameters.AddWithValue("@SUM_TN", c_SUM_TN.Checked)
        r.Command.Parameters.AddWithValue("@SUM_EN", c_SUM_EN.Checked)

        r.Command.Parameters.AddWithValue("@E_ALL_DV", c_E_ALL_DV.Checked)
        r.Command.Parameters.AddWithValue("@E_ALL", c_E_ALL.Checked)
        r.Command.Parameters.AddWithValue("@U_ALL", c_U_ALL.Checked)
        r.Command.Parameters.AddWithValue("@E_REG", c_E_REG.Checked)
        r.Command.Parameters.AddWithValue("@U_REG", c_U_REG.Checked)
        r.Command.Parameters.AddWithValue("@E_OC", c_E_OC.Checked)
        r.Command.Parameters.AddWithValue("@U_OC", c_U_OC.Checked)
        r.Command.Parameters.AddWithValue("@E_UN", c_E_UN.Checked)
        r.Command.Parameters.AddWithValue("@U_UN", c_U_UN.Checked)
        r.Command.Parameters.AddWithValue("@E_SF", c_E_SF.Checked)
        r.Command.Parameters.AddWithValue("@U_SF", c_U_SF.Checked)
        r.Command.Parameters.AddWithValue("@E_FP", c_E_FP.Checked)
        r.Command.Parameters.AddWithValue("@U_FP", c_U_FP.Checked)
        r.Command.Parameters.AddWithValue("@E_NEO", c_E_NEO.Checked)
        r.Command.Parameters.AddWithValue("@U_NEO", c_U_NEO.Checked)
        r.Command.Parameters.AddWithValue("@E_SEC", c_E_SEC.Checked)
        r.Command.Parameters.AddWithValue("@U_SEC", c_U_SEC.Checked)
        r.Command.Parameters.AddWithValue("@E_CAT", c_E_CAT.Checked)
        r.Command.Parameters.AddWithValue("@U_CAT", c_U_CAT.Checked)

        r.Command.Parameters.AddWithValue("@E_KARTED", c_E_KARTED.Checked)
        r.Command.Parameters.AddWithValue("@E_SHIFR", c_E_SHIFR.Checked)
        r.Command.Parameters.AddWithValue("@E_KART", c_E_KART.Checked)
        r.Command.Parameters.AddWithValue("@E_ZAM_OBL", c_E_ZAM_OBL.Checked)
        r.Command.Parameters.AddWithValue("@E_POVR", c_E_POVR.Checked)
        r.Command.Parameters.AddWithValue("@E_ZAT", c_E_ZAT.Checked)
        r.Command.Parameters.AddWithValue("@E_RESTAVR", c_E_RESTAVR.Checked)
        r.Command.Parameters.AddWithValue("@E_GOR", c_E_GOR.Checked)
        r.Command.Parameters.AddWithValue("@E_PODSH", c_E_PODSH.Checked)
        r.Command.Parameters.AddWithValue("@E_KPO", c_E_KPO.Checked)
        r.Command.Parameters.AddWithValue("@E_DEZINF", c_E_DEZINF.Checked)
        r.Command.Parameters.AddWithValue("@E_DEZINS", c_E_DEZINS.Checked)
        r.Command.Parameters.AddWithValue("@E_RAB", c_E_RAB.Checked)

        r.Command.Parameters.AddWithValue("@E_TOPO", c_E_TOPO.Checked)

        Try
            Select Case ModalDialogArgument.Argument
                Case "One"
                    r.Calc(Recalc.Type.Inventory, DirectCast(ModalDialogArgument.ArgumentObject, Guid).ToString())
                Case "All"
                    r.Calc(Recalc.Type.Inventory)
            End Select
        Catch ex As Exception
            AddMessage("Не удалось выполнить расчет: >> " & ex.Message)
        End Try
    End Sub

    Private Sub bClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles bClose.Click
        CloseWindow()
    End Sub


    Private Sub wWindowEnabled(ByVal fl As Boolean)
        worningAnsYes.Enabled = fl
        worningAnsNo.Enabled = fl
        worningAnsCancel.Enabled = fl
        worningLabel.Visible = Not fl
        If fl Then
            worningLabel.Text = ""
        Else
            worningLabel.Text = "Идет процесс резервного копирования..."
        End If

    End Sub

    Private Sub worningAnsYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles worningAnsYes.Click
        wWindowEnabled(False)

        Try
            ''backupThread = New Threading.Thread(New Threading.ThreadStart(AddressOf backupMake))



            ''backupThread.Start()
            backupMake()
            worningPanel.Visible = False
        Catch ex As Exception
            wWindowEnabled(True)
            worningLabel.Text = ex.Message
            worningAnsCancel.Enabled = True
        End Try

    End Sub

    Private Sub worningAnsNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles worningAnsNo.Click
        worningPanel.Visible = False
    End Sub

    Private Sub worningAnsCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles worningAnsCancel.Click
        CloseWindow()
    End Sub

#Region "Бессмысленно-беспощадная копипаста из Service.ascx.vb"

    Private Sub backupMake()
        Dim SQL As New StringBuilder
        SQL.AppendFormat("BACKUP DATABASE [{0}]", BackupDatabaseName()).AppendLine()
        SQL.AppendFormat("TO DISK = N'{0}'", BackupDatabasePath()).AppendLine()
        SQL.AppendFormat("WITH NAME = N'{0} - Full Database Backup'", BackupDatabaseName()).AppendLine()
        'If Overwrite.Checked Then
        'SQL.AppendLine(", INIT")
        'End If

        Dim cmd = New SqlCommand With {.CommandTimeout = 3600}
        cmd.CommandText = SQL.ToString

        'Threading.Thread.Sleep(5000)

        Try
            AppSettings.ExecCommand(cmd)
            BackupDataWrite()
            'If BackupDataWrite() Then
            '    If SaveDoc() Then
            '        Response.Redirect(BasePage.GetDocURL(DocTypeURL, DocID, BasePage.IsModal))
            '    End If
            'End If

            ''RaiseEvent eWorningClosed()

        Catch ex As Exception
            'BasePage.AddMessage("Ошибка во время создания резервной копии: " & ex.Message)
            wWindowEnabled(True)
            worningLabel.Text = ex.Message
            'worningAnsCancel.Enabled = True
        End Try
    End Sub

    Private Function BackupDatabaseName() As String
        Dim cmd As New SqlCommand("SELECT db_name()")
        Return AppSettings.ExecCommandScalar(cmd)
    End Function

    Private Function BackupDatabasePath() As String
        Dim cmd As New SqlCommand("SELECT physical_name FROM sys.database_files WHERE file_id = 1")
        Dim result = AppSettings.ExecCommandScalar(cmd)
        result = Path.Combine(Path.GetFullPath(Path.Combine(result, "..\..")), String.Format("Backup\{0}.bak", BackupDatabaseName))
        Return result
    End Function

    Private Function BackupDataWrite() As Boolean
        Dim SQL As New StringBuilder
        SQL.AppendFormat("RESTORE HEADERONLY").AppendLine()
        SQL.AppendFormat("FROM DISK = N'{0}'", BackupDatabasePath()).AppendLine()
        SQL.AppendFormat("WITH NOUNLOAD").AppendLine()

        Dim cmd As New SqlCommand With {.CommandTimeout = 3600}
        cmd.CommandText = SQL.ToString

        Dim dt = AppSettings.GetDataTable(cmd)
        Dim v = dt.DefaultView
        v.Sort = "Position DESC" ' Последний бэкап

        'If Overwrite.Checked Then
        '    SQL.Remove(0, SQL.Length)
        '    SQL.AppendLine("DELETE tblServiceLog")

        '    cmd.CommandText = SQL.ToString
        '    AppSettings.ExecCommand(cmd)
        'End If

        SQL.Remove(0, SQL.Length)
        SQL.AppendLine("INSERT tblServiceLog (ID, DocID, OwnerID, CreationDateTime, RowID, BackupSetGUID, BackupPath)")
        SQL.AppendLine("VALUES (newid(), @DocID, @OwnerID, @CreationDateTime, 0, @BackupSetGUID, @BackupPath)")

        cmd.CommandText = SQL.ToString
        cmd.Parameters.AddWithValue("@DocID", "09f6b3eb-48f0-4e08-b151-15be4481c472") ' DocID из tblService
        cmd.Parameters.AddWithValue("@OwnerID", AppSettings.UserInfo.UserID)
        cmd.Parameters.AddWithValue("@CreationDateTime", v(0)("BackupFinishDate"))
        cmd.Parameters.AddWithValue("@BackupSetGUID", v(0)("BackupSetGUID"))
        cmd.Parameters.AddWithValue("@BackupPath", BackupDatabasePath())
        Dim result = AppSettings.ExecCommand(cmd)
        Return (result = 1)
    End Function

#End Region

End Class