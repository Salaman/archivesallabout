﻿Partial Public Class Container
    Inherits BaseContainer

#Region "Properties"

    Protected Overrides ReadOnly Property ContentPlaceHolder() As System.Web.UI.WebControls.PlaceHolder
        Get
            Return PlaceHolderContent
        End Get
    End Property

#End Region

#Region "Page Events"

    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        PanelInfo.Visible = False
        PanelMenu.Visible = False
        PanelPlaceHolder.Visible = False
        PlaceHolderShowConfirm.Visible = False
    End Sub

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Request.QueryString("ARCHIVE") IsNot Nothing Then
                If New Guid(Request.QueryString("ARCHIVE")) <> Guid.Empty Then
                    Crumbs.Level = WebUI.ArchiveCrumbs.ArchiveCrumbsLevel.ARCHIVE
                    Crumbs.ISN_ARCHIVE.DocID = New Guid(Request.QueryString("ARCHIVE"))
                Else
                    AddMessage("Необходимо сохранить карточку Архива")
                End If
            End If
            If Request.QueryString("FUND") IsNot Nothing Then
                If New Guid(Request.QueryString("FUND")) <> Guid.Empty Then
                    Crumbs.Level = WebUI.ArchiveCrumbs.ArchiveCrumbsLevel.FUND
                    Crumbs.ISN_FUND.DocID = New Guid(Request.QueryString("FUND"))
                    Crumbs.ISN_ARCHIVE.KeyValue = Crumbs.ISN_FUND.DataSetHeaderDataRow("ISN_ARCHIVE")
                Else
                    AddMessage("Необходимо сохранить карточку Фонда")
                End If
            End If
            If Request.QueryString("INVENTORY") IsNot Nothing Then
                If New Guid(Request.QueryString("INVENTORY")) <> Guid.Empty Then
                    Crumbs.Level = WebUI.ArchiveCrumbs.ArchiveCrumbsLevel.INVENTORY
                    Crumbs.ISN_INVENTORY.DocID = New Guid(Request.QueryString("INVENTORY"))
                    Crumbs.ISN_FUND.KeyValue = Crumbs.ISN_INVENTORY.DataSetHeaderDataRow("ISN_FUND")
                    Crumbs.ISN_ARCHIVE.KeyValue = Crumbs.ISN_FUND.DataSetHeaderDataRow("ISN_ARCHIVE")
                Else
                    AddMessage("Необходимо сохранить карточку Описи")
                End If
            End If
            If Request.QueryString("UNIT") IsNot Nothing Then
                If New Guid(Request.QueryString("UNIT")) <> Guid.Empty Then
                    Crumbs.Level = WebUI.ArchiveCrumbs.ArchiveCrumbsLevel.UNIT
                    Crumbs.ISN_UNIT.DocID = New Guid(Request.QueryString("UNIT"))
                    Crumbs.ISN_INVENTORY.KeyValue = Crumbs.ISN_UNIT.DataSetHeaderDataRow("ISN_INVENTORY")
                    Crumbs.ISN_FUND.KeyValue = Crumbs.ISN_INVENTORY.DataSetHeaderDataRow("ISN_FUND")
                    Crumbs.ISN_ARCHIVE.KeyValue = Crumbs.ISN_FUND.DataSetHeaderDataRow("ISN_ARCHIVE")
                Else
                    AddMessage("Необходимо сохранить карточку Ед. хранения / учета")
                End If
            End If
        End If

        ' Мониторинг истечения сессии
        Dim SessionStateSection = DirectCast(ConfigurationManager.GetSection("system.web/sessionState"), System.Web.Configuration.SessionStateSection)
        Dim SessionTimeout = SessionStateSection.Timeout.TotalSeconds
        Dim SessionScript As New StringBuilder
        SessionScript.AppendLine("var sessionIntervalId;")
        SessionScript.AppendLine("var sessionStart;")
        SessionScript.AppendFormat("var sessionTimeout = Math.floor({0});", SessionTimeout).AppendLine()
        SessionScript.AppendFormat("var saveDocHistoryFlag = document.getElementById('{0}');", SaveDocHistoryFlag.ClientID).AppendLine()
        SessionScript.AppendLine("function sessionMonitor() {")
        SessionScript.AppendLine(" var now = new Date();")
        SessionScript.AppendLine(" var delta = Math.floor(now.getTime() / 1000 - sessionStart.getTime() / 1000);")
        SessionScript.AppendLine(" if (delta > sessionTimeout) {")
        SessionScript.AppendLine("  var div = document.getElementById('panelSessionMonitorDiv');")
        SessionScript.AppendLine("  div.innerHTML = 'Сессия истекла!';")
        SessionScript.AppendLine("  clearInterval(sessionIntervalId);")
        SessionScript.AppendLine("  return;")
        SessionScript.AppendLine(" }")
        If DocControl IsNot Nothing Then
            SessionScript.AppendLine(" if (saveDocHistoryFlag.value != '1' && delta > 60 * 5) {")
            SessionScript.AppendLine("  saveDocHistoryFlag.value = '1';")
            SessionScript.AppendFormat("  var btn = document.getElementById('{0}');", SaveDocHistoryTrigger.ClientID).AppendLine()
            SessionScript.AppendLine("  btn.click();")
            'SessionScript.AppendLine("  $.ajax({")
            'SessionScript.AppendLine("   type: 'POST',")
            'SessionScript.AppendLine("   url: 'Default.aspx/SaveDocHistory',")
            'SessionScript.AppendLine("   contentType: 'application/json; charset=utf-8',")
            'SessionScript.AppendLine("   dataType: 'json',")
            'SessionScript.AppendLine("   success: function(response) { console.log('Черновик сохранен'); },")
            'SessionScript.AppendLine("   failure: function(response) { console.log(response.d); }")
            'SessionScript.AppendLine("  });")
            SessionScript.AppendLine(" }")
        End If
        SessionScript.AppendLine(" if (delta > sessionTimeout - 60 * 2) {")
        SessionScript.AppendLine("  clearInterval(sessionIntervalId);")
        SessionScript.AppendLine("  sessionIntervalId = setInterval(sessionMonitor, 500);")
        SessionScript.AppendLine("  var delta2 = sessionTimeout - delta;")
        SessionScript.AppendLine("  var div = document.getElementById('sessionMonitorDiv');")
        SessionScript.AppendLine("  var h = Math.floor(delta2 / 60);")
        SessionScript.AppendLine("  var m = Math.floor(delta2 - Math.floor(delta2 / 60) * 60);")
        SessionScript.AppendLine("  div.innerHTML = 'До окончания сессии осталось: ' + (h < 10 ? '0' + h : h) + ':' + (m < 10 ? '0' + m : m);")
        SessionScript.AppendLine(" }")
        SessionScript.AppendLine("}")
        SessionScript.AppendLine("sessionStart = new Date();")
        SessionScript.AppendLine("sessionIntervalId = setInterval(sessionMonitor, 5000);")
        If Not Page.ClientScript.IsStartupScriptRegistered("sessionMonitor") Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "sessionMonitor", SessionScript.ToString(), True)
        End If
    End Sub

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        PanelPlaceHolder.Visible = PlaceHolderContent.Controls.Count > 0
        divLevel.Visible = ContainerMode = ContainerModes.ListDoc AndAlso ListDocControl IsNot Nothing AndAlso ListDocControl.DocType.Category.CategoryName = "Работа с базой"
        ImageButtonFirst.Visible = ContainerMode = ContainerModes.Doc
        ImageButtonPrevious.Visible = ContainerMode = ContainerModes.Doc
        ImageButtonNext.Visible = ContainerMode = ContainerModes.Doc
        ImageButtonLast.Visible = ContainerMode = ContainerModes.Doc
        If Not IsPostBack Then
            LoadTab()
        End If
        SaveTab()
    End Sub

    Public Overrides Sub RaisePostBackEvent(ByVal eventArgument As String)
        Select Case eventArgument
            Case "Confirm"
                ModalPopupExtenderConfirm.Hide()
                ExecuteDocumentCommand(ButtonConfirmOk.CommandName)
            Case Else
                MyBase.RaisePostBackEvent(eventArgument)
        End Select
        UpdateParentUpdatePanel()
    End Sub

#End Region

#Region "Events"

    Protected Sub MenuActions_MenuItemClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MenuEventArgs) Handles MenuActions.MenuItemClick
        e.Item.Selected = False
        ' Find
        Dim ToolBarRow As DataRow = Nothing
        Select Case ContainerMode
            Case ContainerModes.Doc
                ToolBarRow = DocControl.DocType.ToolBarRows.SingleOrDefault(Function(R) _
                                            R(eqDocType.FLD_TOOLBAR_COMMANDNAME) = e.Item.Value _
                                            And R("ShowInListOrDocument") = "D")
            Case ContainerModes.ListDoc
                ToolBarRow = ListDocControl.DocType.ToolBarRows.SingleOrDefault(Function(R) _
                                            R(eqDocType.FLD_TOOLBAR_COMMANDNAME) = e.Item.Value _
                                            And R("ShowInListOrDocument") = "L")
        End Select
        ' Exec
        If ToolBarRow IsNot Nothing AndAlso Not IsDBNull(ToolBarRow("ConfirmMessage")) AndAlso ToolBarRow("ConfirmMessage").ToString.Trim <> String.Empty Then
            ButtonConfirmOk.CommandName = e.Item.Value
            Dim ConfirmMessage = ToolBarRow("ConfirmMessage").ToString
            ShowConfirm(ConfirmMessage)
        ElseIf e.Item.Depth = 1 AndAlso e.Item.Parent.Value = "SelectView" Then
            If ContainerMode = ContainerModes.ListDoc Then
                ListDocControl.LoadView(New Guid(e.Item.Value))
                ListDocControl.Refresh()
            End If
        Else
            ExecuteDocumentCommand(e.Item.Value)
        End If
    End Sub

    Private Sub SaveDocHistoryTrigger_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles SaveDocHistoryTrigger.Click
        Try
            If DocControl IsNot Nothing Then
                DocControl.SaveDocHistory()
            End If
        Catch ex As Exception
        End Try
    End Sub

#End Region

#Region "Metods"

    Public Overrides Sub AddMenuItem(ByVal Text As String, ByVal CommandName As String, ByVal ImageUrl As String, Optional ByVal ToolTip As String = "", Optional ByVal SeparatorBefore As Boolean = False, Optional ByVal AddAtIndex As Integer = -1)
        Dim MenuActionsItem As New MenuItem(Text, CommandName)
        If ImageUrl <> "" Then MenuActionsItem.ImageUrl = BasePage.GetThemeRelativeURL(ImageUrl)
        If ToolTip <> "" Then MenuActionsItem.ToolTip = ToolTip
        If AddAtIndex > -1 Then
            MenuActions.Items.AddAt(AddAtIndex, MenuActionsItem)
        Else
            MenuActions.Items.Add(MenuActionsItem)
        End If
        PanelMenu.Visible = True
        If CommandName = "SelectView" Then
            FillViewSelector(MenuActionsItem)
        End If
    End Sub

    Public Overrides Function IndexOfMenuItem(ByVal CommandName As String) As Integer
        For Each MenuActionsItem As MenuItem In MenuActions.Items
            If MenuActionsItem.Value = CommandName Then
                Return MenuActions.Items.IndexOf(MenuActionsItem)
            End If
        Next
        Return -1
    End Function

    Public Overrides Sub RemoveMenuItem(ByVal CommandName As String)
        For Each MenuActionsItem As MenuItem In MenuActions.Items
            If MenuActionsItem.Value = CommandName Then
                MenuActions.Items.Remove(MenuActionsItem)
                Exit Sub
            End If
        Next
        PanelMenu.Visible = MenuActions.Items.Count > 0
    End Sub

    Public Overrides Sub ClearMenuItems()
        MenuActions.Items.Clear()
        PanelMenu.Visible = False
    End Sub

    Public Overrides Sub SetTitle(ByVal Title As String)
        LabelInfo.Text = Title
        BasePage.SetTitle(Title)
        PanelInfo.Visible = LabelInfo.Text <> String.Empty
    End Sub

    Public Sub ShowConfirm(ByVal Message As String)
        PlaceHolderConfirm.Controls.Clear()
        Dim MessagePanel As New Panel With {.CssClass = "MenuSubItemStyle"}
        Dim MessageLabel As New Label With {.Text = Message}
        MessagePanel.Controls.Add(MessageLabel)
        PlaceHolderConfirm.Controls.Add(MessagePanel)
        PlaceHolderShowConfirm.Visible = True
        ModalPopupExtenderConfirm.OnOkScript = Page.ClientScript.GetPostBackEventReference(Me, "Confirm")
        ModalPopupExtenderConfirm.Show()
    End Sub

#End Region

#Region "SelectView"

    Private Sub FillViewSelector(ByVal SelectViewItem As MenuItem)
        If ContainerMode = ContainerModes.ListDoc AndAlso IndexOfMenuItem("SelectView") > -1 Then
            SelectViewItem.ChildItems.Clear()

            SelectViewItem.ChildItems.Add(New MenuItem(GetGlobalResourceObject("eqResources", "MNU_ViewAll"), Guid.Empty.ToString))

            Dim SqlCommand As New SqlClient.SqlCommand
            Dim Data As DataTable

            Dim CurrentViewDocID As Guid
            If ListDocControl.CurrentViewDataSet IsNot Nothing Then
                If Not IsDBNull(ListDocControl.CurrentViewDataSet.Tables(0).Rows(0).Item("ID")) Then
                    CurrentViewDocID = ListDocControl.CurrentViewDataSet.Tables(0).Rows(0).Item("ID")
                End If
            End If

            SqlCommand.CommandText = "SELECT * FROM eqView WHERE Deleted=0 AND Access='Личное' AND ParentID=@ParentID AND OwnerID=@OwnerID ORDER BY IsDefault DESC, ViewName ASC"
            SqlCommand.Parameters.AddWithValue("@ParentID", ListDocControl.DocType.DocTypeID)
            SqlCommand.Parameters.AddWithValue("@OwnerID", AppSettings.UserInfo.UserID)
            Data = AppSettings.GetDataTable(SqlCommand)

            If Data IsNot Nothing Then
                For Each R As DataRow In Data.Rows
                    Dim ViewItem As New MenuItem(R("ViewName"), R("ID").ToString)
                    SelectViewItem.ChildItems.Add(ViewItem)
                Next
            End If

            SqlCommand.CommandText = "SELECT * FROM eqView WHERE Deleted=0 AND Access='Общее' AND ParentID=@ParentID ORDER BY IsDefault DESC, ViewName ASC"
            Data = AppSettings.GetDataTable(SqlCommand)

            If Data IsNot Nothing Then
                For Each R As DataRow In Data.Rows
                    Dim ViewItem As New MenuItem(R("ViewName"), R("ID").ToString)
                    SelectViewItem.ChildItems.Add(ViewItem)
                Next
            End If

            Dim CurrentItem As MenuItem = Nothing
            For Each ViewItem As MenuItem In SelectViewItem.ChildItems
                If ViewItem.Value = CurrentViewDocID.ToString Then
                    ViewItem.Selected = True
                    Exit Sub
                End If
            Next

            ' Если наша настроечка
            Dim SelectViewMenuItem As New MenuItem With {.Selected = True}
            SelectViewMenuItem.Text = ListDocControl.CurrentViewDataSet.Tables(0).Rows(0).Item("ViewName")
            SelectViewMenuItem.Value = ListDocControl.CurrentViewDataSet.Tables(0).Rows(0).Item("ID").ToString
            SelectViewItem.ChildItems.Add(SelectViewMenuItem)

        End If
    End Sub

#End Region

#Region "Navigation"

    Private Sub ImageButtonFirst_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonFirst.Click
        Dim FirstID = ListNavigation.First
        SaveTab()
        Response.Redirect(BasePage.GetDocURL(DocTypeURL, FirstID, BasePage.IsModal))
    End Sub

    Private Sub ImageButtonLast_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonLast.Click
        Dim LastID = ListNavigation.Last
        SaveTab()
        Response.Redirect(BasePage.GetDocURL(DocTypeURL, LastID, BasePage.IsModal))
    End Sub

    Private Sub ImageButtonNext_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonNext.Click
        Dim NextID As Guid = ListNavigation(Math.Min(ListNavigation.Count - 1, ListNavigation.IndexOf(DocID) + 1))
        SaveTab()
        Response.Redirect(BasePage.GetDocURL(DocTypeURL, NextID, BasePage.IsModal))
    End Sub

    Private Sub ImageButtonPrevious_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonPrevious.Click
        Dim PrevID As Guid = ListNavigation(Math.Max(0, ListNavigation.IndexOf(DocID) - 1))
        SaveTab()
        Response.Redirect(BasePage.GetDocURL(DocTypeURL, PrevID, BasePage.IsModal))
    End Sub

    Private Sub SaveTab()
        If DocControl IsNot Nothing Then
            Dim MenuTabs = DirectCast(DocControl.FindControl("MenuTabs"), Menu)
            If MenuTabs IsNot Nothing Then
                Response.Cookies.Add(New HttpCookie("TabFor" & DocTypeURL, MenuTabs.SelectedValue))
            End If
        End If
    End Sub

    Private Sub LoadTab()
        If Request.Cookies("TabFor" & DocTypeURL) IsNot Nothing Then
            If DocControl IsNot Nothing Then
                Dim MenuTabs = DirectCast(DocControl.FindControl("MenuTabs"), Menu)
                If MenuTabs IsNot Nothing Then
                    For Each MenuTabsItem As MenuItem In MenuTabs.Items
                        MenuTabsItem.Selected = MenuTabsItem.Value = Request.Cookies("TabFor" & DocTypeURL).Value
                    Next
                End If
            End If
        End If
    End Sub

#End Region

End Class