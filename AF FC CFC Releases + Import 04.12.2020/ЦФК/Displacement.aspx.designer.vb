﻿'------------------------------------------------------------------------------
' <auto-generated>
'     Этот код создан программой.
'     Исполняемая версия:2.0.50727.5472
'
'     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
'     повторной генерации кода.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On



Partial Public Class Displacement

    '''<summary>
    '''Head1 элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents Head1 As Global.System.Web.UI.HtmlControls.HtmlHead

    '''<summary>
    '''form1 элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm

    '''<summary>
    '''ToolkitScriptManagerDefault элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents ToolkitScriptManagerDefault As Global.AjaxControlToolkit.ToolkitScriptManager

    '''<summary>
    '''LableTitle элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents LableTitle As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ButtonReplacementFinish элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents ButtonReplacementFinish As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''ButtonAttachInventoryStructure элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents ButtonAttachInventoryStructure As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''UpdatePanelDisplacement элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents UpdatePanelDisplacement As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''ListDocDisplacement элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents ListDocDisplacement As Global.WebApplication.ListDoc
End Class
