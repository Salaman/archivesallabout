﻿Imports System.Data.SqlClient

Partial Public Class Movement
    Inherits BasePage

    Private Sub ButtonMovement_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonMovement.Click
        Dim cmd As New SqlCommand
        cmd.Parameters.AddWithValue("@OwnerID", AppSettings.UserInfo.UserID)
        cmd.Parameters.Add(New SqlParameter("@ISN_REF_ACT", SqlDbType.BigInt) With {.Value = 0})
        cmd.Parameters.Add(New SqlParameter("@ISN_OBJ", SqlDbType.BigInt) With {.Value = 0})
        cmd.Parameters.Add(New SqlParameter("@ISN_ACT", SqlDbType.BigInt) With {.Value = 0})
        cmd.Parameters.Add(New SqlParameter("@KIND", SqlDbType.BigInt) With {.Value = 0})

        Dim selectedDocs = DirectCast(ModalDialogArgument.ArgumentObject, List(Of Guid))
        For Each selectedMovement In ListDocMovement.SelectedDocuments
            For Each selectedDoc In selectedDocs
                cmd.CommandText += GetInsertRefMovementCmd(Request.QueryString("DocTypeURL"), selectedDoc, selectedMovement)
            Next
        Next

        If String.IsNullOrEmpty(cmd.CommandText) Then
            AddMessage("Выберите не менее одного акта")
        Else
            AppSettings.ExecCommand(cmd)
            CloseWindow()
        End If
    End Sub

    Private Function GetInsertRefMovementCmd(ByVal docTypeURL As String, ByVal docId As Guid, ByVal MovementId As Guid) As String
        Dim isnRefLoc = "SELECT @ISN_REF_ACT=ISNULL(MAX(ISN_REF_ACT)+1,1) FROM tblREF_ACT" & vbCrLf
        Dim isnLoc = "SELECT @ISN_ACT=ISN_ACT FROM tblACT WHERE ID='" & MovementId.ToString & "'" & vbCrLf
        Dim isnObj As String = Nothing
        Dim kind As String = Nothing

        Select Case docTypeURL.ToUpper
            Case "FUND.ASCX"
                isnObj = "SELECT @ISN_OBJ=ISN_FUND FROM tblFUND WHERE ID='" & docId.ToString & "'" & vbCrLf
                kind = "SELECT @KIND=701" & vbCrLf
            Case "INVENTORY.ASCX"
                isnObj = "SELECT @ISN_OBJ=ISN_INVENTORY FROM tblINVENTORY WHERE ID='" & docId.ToString & "'" & vbCrLf
                kind = "SELECT @KIND=702" & vbCrLf
            Case "UNIT.ASCX"
                isnObj = "SELECT @ISN_OBJ=ISN_UNIT FROM tblUNIT WHERE ID='" & docId.ToString & "'" & vbCrLf
                kind = "SELECT @KIND=UNIT_KIND FROM tblUNIT WHERE ID='" & docId.ToString & "'" & vbCrLf
            Case "DEPOSIT.ASCX"
                isnObj = "SELECT @ISN_OBJ=ISN_DEPOSIT FROM tblDEPOSIT WHERE ID='" & docId.ToString & "'" & vbCrLf
                kind = "SELECT @KIND=707" & vbCrLf
        End Select

        Return isnRefLoc & isnLoc & isnObj & kind & "DELETE FROM [tblREF_ACT] WHERE DocID='" & docId.ToString & "' AND ISN_ACT=@ISN_ACT" & vbCrLf & _
        "INSERT INTO [tblREF_ACT] ([ID],[OwnerID],[CreationDateTime],[DocID],[RowID],[ISN_REF_ACT],[ISN_ACT],[ISN_OBJ],[KIND],[UNIT_COUNT])" & vbCrLf & _
                            "VALUES (NEWID(),@OwnerID,GETDATE(),'" & docId.ToString & "',0,@ISN_REF_ACT,@ISN_ACT,@ISN_OBJ,@KIND,NULL)" & vbCrLf
    End Function

End Class