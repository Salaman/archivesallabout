﻿Imports System.Data.SqlClient

Partial Public Class Recovery
    Inherits BasePage
    'Inherits BaseDoc

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        InvStructOptions.Visible = False
        Select Case Request.QueryString("DocTypeURL")
            Case "FUND.ascx"
                ListDocRecovery.DocTypeURL = "FUND.ascx"
            Case "INVENTORY.ascx"
                ListDocRecovery.DocTypeURL = "INVENTORY.ascx"
            Case "UNIT.ascx"
                ListDocRecovery.DocTypeURL = "UNIT.ascx"
            Case "DOCUMENT.ascx"
                ListDocRecovery.DocTypeURL = "DOCUMENT.ascx"
            Case "ACT.ascx"
                ListDocRecovery.DocTypeURL = "ACT.ascx"
            Case "INVENTORYSTRUCTURE.ascx"
                ListDocRecovery.DocTypeURL = "INVENTORYSTRUCTURE.ascx"
                SetInvStructDataTable(ListDocRecovery.ListDocDataTable)
                InvStructOptions.Visible = True
            Case Else
                ' ListDocDisplacement.DocTypeURL = ""
        End Select
        'Подписка на функцию Checket() галочка Выбрать все. 01.12.2020 Саламан Андрей
        ExecuteScript(String.Format("$('input#ListDocRecovery_SelectBoxAll').attr('onclick','Checket(this)');"), "Checket", True)
        ListDocRecovery.CurrentViewDataSet.Tables(0).Rows(0).Item("ShowDeleted") = True
    End Sub

    ''' <summary>
    ''' В случае восстановления структур описи, нужно формировать таблицу данных таким образом, чтобы в списке отображались
    '''  так же и удаленные элементы, имеющие не удаленных родителей (которые по умолчанию не попадают в таблицу)
    ''' </summary>
    ''' <param name="ListDocDataTable"></param>
    ''' <remarks></remarks>
    Private Sub SetInvStructDataTable(ByRef ListDocDataTable As DataTable)
        Dim selCmdString As String = <sql>
                                            WITH A as (
                                            SELECT ID, ISN_INVENTORY_CLS as TreeKey, ISN_HIGH_INVENTORY_CLS as TreeParentKey, '' as b21d2421822942219724e5fefd678592, %XML0%+NAME+%XML1% as e8c040db497040bd8740e9166a68a564
                                            FROM tblINVENTORY_STRUCTURE
                                            WHERE Deleted=1
                                            %WHERE_OPT%
                                            )
                                            SELECT ID,TreeKey ,
                                            CASE 
	                                            WHEN EXISTS(SELECT * FROM A as AA WHERE AA.TreeKey=A.TreeParentKey ) THEN A.TreeParentKey
	                                            ELSE NULL
                                            END
                                             TreeParentKey,
                                            b21d2421822942219724e5fefd678592,e8c040db497040bd8740e9166a68a564
                                            FROM A
                                     </sql>
        '(Roman 060813) в этом месте может возникнуть проблема совместимости, если вдруг айдишники полей внезапно не совпадут 
        ' в таком случае полезно будет взглянуть в  ListDoc.ascx.vb, на код около Dim Value = R(FieldID.ToString("n"))
        selCmdString = selCmdString.Replace("%XML0%", "'<div class=""inventory-structure"" title=""...""></div><div style=""float: left;"">&nbsp;'").Replace("%XML1%", "'</div>'")
        If Not Request.QueryString("FUND") = Nothing Then
            selCmdString = selCmdString.Replace("%WHERE_OPT%", " AND ISN_FUND=(SELECT ISN_FUND FROM tblFUND WHERE ID='" + Request.QueryString("FUND") + "')")

        ElseIf Not Request.QueryString("INVENTORY") = Nothing Then
            selCmdString = selCmdString.Replace("%WHERE_OPT%", " AND ISN_INVENTORY=(SELECT ISN_INVENTORY FROM tblINVENTORY WHERE ID='" + Request.QueryString("INVENTORY") + "')")
        Else
            selCmdString = selCmdString.Replace("%WHERE_OPT%", "")
        End If

        Dim selCmd As New SqlCommand(selCmdString)
        ListDocDataTable = AppSettings.GetDataTable(selCmd)
        'ListDocRecovery.DoSelectDoc(New Guid("84751916-F550-4E39-8EE0-15A7751F958F"))
        'ListDocRecovery.SelectedDocuments.Add(New Guid("84751916-F550-4E39-8EE0-15A7751F958F"))
    End Sub

    Private Sub Recovery_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        ListDocRecovery.CurrentViewDataSet.Tables(0).Rows(0).Item("ShowDeleted") = False
    End Sub


    Private oName As String '  objectName
    Private cName As String

    Private Const datePeriod As Integer = 1

    Private Sub ButtonRecovery_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonRecovery.Click
        InitObjConNames(Request.QueryString("DocTypeURL"))
        Dim selObjects As List(Of Guid) = ListDocRecovery.SelectedDocuments
        If selObjects.Count = 0 Then
            AddMessage("Не выбрано ни одного объекта для востановления")
            Return
        End If

        If AddParents(selObjects) Then
            AddMessage("К списку восстанавливаемых объектов учета былыи так же добавлены недостающие родительские объекты")
        End If

        MakeRecovery(selObjects, oName, 0)
        AddMessage("Выбранные объекты учета восстановлены")
        'BasePage.CloseModalDialogWindow("Recovery", New DataSet)
        'CloseModalDialogWindow("Recovery", New DataSet)

        'ListDocRecovery.Page_ModalDialogClosed1(New eqModalDialogArgument("SetupView"))
        'CloseModalDialogWindow(New eqModalDialogArgument("SetupView"))
        'CloseModalDialogWindow("Recovery.aspx")
        CloseWindow()
    End Sub

    ''' <summary>
    ''' Добавляет в список на всоотановление недостающие родительские элементы, отсутствие которых не позволило бы корректно построить дерево в иерархии восстановленных объектов учета.
    ''' </summary>
    ''' <param name="selObjectsList"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function AddParents(ByRef selObjectsList As List(Of Guid)) As Boolean
        If Not oName = "INVENTORY_STRUCTURE" Then
            Return False
        End If
        Dim ret As Boolean = False
        For Each el As DataRow In ListDocRecovery.ListDocDataTable.Rows 'по всем строкам выводимой на экран таблицы
            If selObjectsList.Contains(el.Item("ID")) Then 'если эта строка выбрана
                Dim pointedEl As DataRow = el 'ссылка на элемент, корень которого пытаемся отследить
                While Not IsDBNull(pointedEl.Item("TreeParentKey")) 'пока не добиремся до корневого элемента
                    For Each ell As DataRow In ListDocRecovery.ListDocDataTable.Rows 'ищем родителя
                        If ell.Item("TreeKey") = pointedEl.Item("TreeParentKey") Then 'нашли родителя
                            pointedEl = ell 'переводим ссылку на объект более близкий к корню
                            If Not selObjectsList.Contains(pointedEl.Item("ID")) Then 'если этот объект не содержится в листе восстановления
                                selObjectsList.Add(pointedEl.Item("ID")) 'то добавим его туда
                                ret = True 'и функция вернет True если добавили хотя бы один элемент
                            End If
                            Exit For
                        End If
                    Next
                End While
            End If
        Next


        Return ret
    End Function

    ''' <summary>
    ''' инициализирует глобальные поля класса oName(тип объекта, к которому будет применено востановление) и cName(тип контейнера этого объекта)
    ''' </summary>
    ''' <param name="docTypeUrl"></param>
    ''' <remarks></remarks>
    Private Sub InitObjConNames(ByVal docTypeUrl As String)
        Select Case docTypeUrl
            Case "UNIT.ascx"
                cName = "INVENTORY"
                oName = "UNIT"
            Case "INVENTORY.ascx"
                cName = "FUND"
                oName = "INVENTORY"
            Case "DOCUMENT.ascx"
                cName = "UNIT"
                oName = "DOCUMENT"
            Case "FUND.ascx"
                cName = "ARCHIVE"
                oName = "FUND"
            Case "ACT.ascx"
                cName = "ERROR IN InitObjConNames"
                oName = "ACT"
            Case "INVENTORYSTRUCTURE.ascx"
                cName = "ERROR IN InitObjConNames"
                oName = "INVENTORY_STRUCTURE"
            Case Else
                cName = "ERROR IN InitObjConNames"
                oName = "ERROR IN InitObjConNames"
        End Select
    End Sub

    ''' <summary>
    ''' Производит востановление элементов (изменение поля Deleted с 1 на 0) из списка selObjects.
    '''  Для каждого эл-та этого списка составляется список содержавшихся в нем элементов,
    '''  удаленных примерно в то же время, и к ним рекурсивно применяется эта же ф-я.
    ''' </summary>
    ''' <param name="selObjects">Список объектов для восстановления</param>
    ''' <param name="objectName">Тип объектов</param>
    ''' <param name="delDate">Момент удаления верхнего элемента иерархии</param>
    ''' <param name="depth">Глубина рекурсии</param>
    ''' <remarks></remarks>
    Private Sub MakeRecovery(ByRef selObjects As List(Of Guid), ByVal objectName As String, ByVal depth As Integer)
        If selObjects.Count = 0 Then
            Return
        End If
        Select Case objectName
            Case "DOCUMENT"
                Dim cmdString As String = <sql>
                                UPDATE tblDOCUMENT
                                SET Deleted=0
                                WHERE ID=@OID AND Deleted=1
                            </sql>
                Dim cmd As New SqlCommand(cmdString)
                For Each obj In selObjects
                    cmd.Parameters.AddWithValue("@OID", obj)
                    If AppSettings.ExecCommand(cmd) = 1 Then ''AndAlso depth = 0 Then
                        WriteDocStatesLOG(obj, oName)
                    End If
                    cmd.Parameters.Clear()
                Next
            Case "UNIT"
                Dim extObjList As New List(Of Guid)
                For Each selectedDoc In selObjects 'создаем расширенный список юнитов
                    extObjList.Add(selectedDoc)
                    For Each exDoc In GetRelatedDelUnitsList(selectedDoc)
                        If Not extObjList.Contains(exDoc) Then
                            extObjList.Add(exDoc)
                        End If
                    Next
                Next
                Dim cmdString As String = <sql>
                                UPDATE tblUNIT
                                SET Deleted=0
                                WHERE ID=@OID AND Deleted=1
                            </sql>
                Dim cmd As New SqlCommand(cmdString)
                For Each obj In extObjList
                    cmd.Parameters.AddWithValue("@OID", obj)
                    If AppSettings.ExecCommand(cmd) = 1 Then ''AndAlso depth = 0 Then
                        WriteDocStatesLOG(obj, oName)
                    End If
                    cmd.Parameters.Clear()
                    'MakeRecovery(GetRelatedDelObjsList(obj, objectName, "ACT"), "ACT", depth + 1)
                    MakeRecovery(GetRelatedDelObjsList(obj, objectName, "DOCUMENT"), "DOCUMENT", depth + 1)
                    'MakeRecovery(GetRelatedDelObjsList(obj, objectName, "INVENTORY_STRUCTURE", delDate), "INVENTORY_STRUCTURE", delDate)
                Next
            Case "INVENTORY"
                Dim cmdString As String = <sql>
                                UPDATE tblINVENTORY
                                SET Deleted=0
                                WHERE ID=@OID AND Deleted=1
                            </sql>
                Dim cmd As New SqlCommand(cmdString)
                For Each obj In selObjects
                    cmd.Parameters.AddWithValue("@OID", obj)
                    If AppSettings.ExecCommand(cmd) = 1 Then '' AndAlso depth = 0 Then
                        WriteDocStatesLOG(obj, oName)
                    End If
                    cmd.Parameters.Clear()
                    'MakeRecovery(GetRelatedDelObjsList(obj, objectName, "ACT"), "ACT", depth + 1)
                    MakeRecovery(GetRelatedDelObjsList(obj, objectName, "UNIT"), "UNIT", depth + 1)
                    MakeRecovery(GetRelatedDelObjsList(obj, objectName, "INVENTORY_STRUCTURE"), "INVENTORY_STRUCTURE", depth + 1)
                Next
            Case "FUND"
                Dim cmdString As String = <sql>
                                UPDATE tblFUND
                                SET Deleted=0
                                WHERE ID=@OID AND Deleted=1
                            </sql>
                Dim cmd As New SqlCommand(cmdString)
                For Each obj In selObjects
                    cmd.Parameters.AddWithValue("@OID", obj)
                    If AppSettings.ExecCommand(cmd) = 1 Then ''AndAlso depth = 0 Then
                        WriteDocStatesLOG(obj, oName)
                    End If
                    cmd.Parameters.Clear()
                    'MakeRecovery(GetRelatedDelObjsList(obj, objectName, "ACT"), "ACT", depth + 1)
                    MakeRecovery(GetRelatedDelObjsList(obj, objectName, "INVENTORY"), "INVENTORY", depth + 1)
                    'при удалении фонда, его разделы описей не удаляются. Делаю вывод, что восстанавливать каскадом их тоже ненадо
                    'MakeRecovery(GetRelatedDelObjsList(obj, objectName, "INVENTORY_STRUCTURE"), "INVENTORY_STRUCTURE", depth + 1)
                Next
            Case "ACT"
                Dim cmdString As String = <sql>
                                UPDATE tblACT
                                SET Deleted=0
                                WHERE ID=@OID AND Deleted=1
                            </sql>
                Dim cmd As New SqlCommand(cmdString)
                For Each obj In selObjects
                    cmd.Parameters.AddWithValue("@OID", obj)
                    If AppSettings.ExecCommand(cmd) = 1 Then ''AndAlso depth = 0 Then
                        WriteDocStatesLOG(obj, oName)
                    End If
                    cmd.Parameters.Clear()
                Next
            Case "INVENTORY_STRUCTURE"
                Dim cmdString As String = <sql>
                                UPDATE tblINVENTORY_STRUCTURE
                                SET Deleted=0
                                WHERE ID=@OID AND Deleted=1
                            </sql>
                Dim cmd As New SqlCommand(cmdString)
                For Each obj In selObjects
                    cmd.Parameters.AddWithValue("@OID", obj)
                    If AppSettings.ExecCommand(cmd) = 1 Then ''AndAlso depth = 0 Then
                        WriteDocStatesLOG(obj, oName)
                    End If
                    cmd.Parameters.Clear()
                Next
            Case Else
                Dim cmdString As String = <sql>
                                UPDATE tbl%OBJ%
                                SET Deleted=0
                                WHERE ID=@OID AND Deleted=1
                            </sql>
                cmdString = cmdString.Replace("%OBJ%", objectName)

                Dim cmd As New SqlCommand(cmdString)
                For Each obj In selObjects
                    cmd.Parameters.AddWithValue("@OID", obj)
                    If AppSettings.ExecCommand(cmd) = 1 AndAlso depth = 0 Then
                        WriteDocStatesLOG(obj, oName)
                    End If
                    cmd.Parameters.Clear()
                Next
        End Select


    End Sub

    ''' <summary>
    ''' Для одного юнита возвращает список из него и всех связанных с ним юнитов, удаленных не раньше, чем за datePeriod до delDate.
    ''' </summary>
    ''' <param name="unitID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetRelatedDelUnitsList(ByVal unitID As Guid) As List(Of Guid)
        Dim ret As New List(Of Guid)
        Dim cmdText As String = <sql>
                                SELECT A.ID, D.lastDeleted 
                                FROM (
	                                SELECT U.ID
	                                FROM tblUNIT U
	                                WHERE U.ID=@UID
	                                and Deleted=1

	                                UNION

	                                SELECT U.ID
	                                FROM tblUNIT U
	                                WHERE ISN_HIGH_UNIT=(
		                                SELECT TOP 1 ISN_UNIT 
		                                FROM tblUNIT
		                                WHERE ID=@UID
		                                and Deleted=1
	                                )

	                                UNION

	                                SELECT U.ID
	                                FROM tblUNIT U
	                                WHERE ISN_UNIT=(
		                                SELECT TOP 1 ISN_HIGH_UNIT 
		                                FROM tblUNIT 
		                                WHERE ID=@UID
		                                and Deleted=1
	                                )
                                ) as A
                                LEFT JOIN (
	                                SELECT A.* FROM (
		                                SELECT DocID,MAX(EventTime) as lastDeleted
		                                FROM eqDocStatesLOG
		                                WHERE StatusName LIKE '%-> Удален'
		                                GROUP BY DocID
	                                ) as A
	                                LEFT JOIN (
		                                SELECT  DocID,MAX(EventTime) as lastRecovered
		                                FROM eqDocStatesLOG
		                                WHERE StatusName LIKE '%-> Восcтановлен'
		                                GROUP BY DocID
	                                ) as B 
	                                ON A.DocID=B.DocID 
	                                WHERE A.lastDeleted>B.lastRecovered OR B.lastRecovered is NULL 
                                ) as D ON D.DocID=A.ID
                                </sql>
        Dim cmd As New SqlCommand(cmdText)
        cmd.Parameters.AddWithValue("@UID", unitID)
        For Each row In AppSettings.GetDataTable(cmd).Rows
            If IsDBNull(row(1)) Then ' OrElse row(1) > delDate.AddDays(-datePeriod) Then
                ret.Add(row(0))
            End If
        Next
        Return ret
    End Function


    ''' <summary>
    ''' Возвращает список ID объектов, связанных с данным контейнером, и удаленных не раньше, чем за dataPeriod=1 дней до delDate. Применяется для нахождения объектов, которые требуется востановить вместе с данным контейнером.
    ''' </summary>
    ''' <param name="conID">ID объекта-контейнера</param>
    ''' <param name="conType">Тип контейнера</param>
    ''' <param name="objType">Тип объектов, среди которых будет поиск на связь с контейнером</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetRelatedDelObjsList(ByVal conID As Guid, ByVal conType As String, ByVal objType As String) As List(Of Guid)
        Dim ret As New List(Of Guid)

        Select Case objType
            Case "DOCUMENT", "UNIT", "INVENTORY", "FUND", "INVENTORY_STRUCTURE" ' предполагается, что контейнеры для этих объектов будут введены корректно
                Dim cmdText As String = <sql>
WITH A0 as( -- все записи об удалении данного контейнера
	SELECT *
	FROM eqDocStatesLOG
	WHERE StatusName LIKE '%Удален'
		AND DocID IN ( 
			SELECT ID 
			FROM tbl%C_TYPE%
			WHERE ID = @CID
				-- AND Deleted = 1 -- а он точно удален?
		)
		AND DocTypeID IN (
			SELECT ID
			FROM eqDocTypes
			WHERE DocURL LIKE '%'+REPLACE('%C_TYPE%', '_', '')+'.ascx'
		)
)
, A1 as ( -- последняя запись об удалении контейнера
	SELECT *
	FROM A0
	WHERE EventTime IN (
		SELECT MAX(EventTime) 
		FROM A0
	)
)
, B0 as ( -- все записи об удалении подконтейнеров 1го уровня
	SELECT *
	FROM eqDocStatesLOG
	WHERE StatusName LIKE '%Удален'
		AND DocID IN (
			SELECT ID 
			FROM tbl%O_TYPE%
			WHERE ISN_%C_TYPE% IN (
				SELECT ISN_%C_TYPE%
				FROM tbl%C_TYPE%
				WHERE ID = @CID
			)
				AND Deleted=1
		)
		AND DocTypeID IN (
			SELECT ID
			FROM eqDocTypes
			WHERE DocURL LIKE '%'+REPLACE('%O_TYPE%', '_', '')+'.ascx'
		)
)	
, B1 as ( -- последние записи об удалении подконтейнеров
	SELECT B0.* 
	FROM B0
	JOIN (
		SELECT DocID, MAX(EventTime) EventTime
		FROM B0
		GROUP BY DocID
	) X ON B0.EventTime = X.EventTime AND B0.DocID = X.DocID
)
SELECT DocID
FROM B1
WHERE 
	CONVERT(date, EventTime)
	 = --если в один и тот же день удалили, значит нужно восстанавливать вместе
	CONVERT(date, (SELECT TOP 1 EventTime FROM A1))

                                        </sql>
                cmdText = cmdText.Replace("%O_TYPE%", objType).Replace("%C_TYPE%", conType)
                Dim cmd As New SqlCommand(cmdText)
                cmd.Parameters.AddWithValue("@CID", conID)
                For Each row In AppSettings.GetDataTable(cmd).Rows
                    'If Not IsDBNull(row("lastDeleted")) AndAlso Not IsDBNull(row("conDeleted")) AndAlso DirectCast(row("lastDeleted"), DateTime).Date = DirectCast(row("conDeleted"), DateTime).Date Then
                    ret.Add(row(0))
                    'End If
                Next
            Case "ACT"
                Select Case conType
                    Case "UNIT", "INVENTORY", "FUND"
                        Dim cmdText As String = <sql>
                                            SELECT *
                                            FROM tblREF_ACT as A
                                            LEFT JOIN (
                                            	SELECT A.* FROM (
	                                                SELECT DocID,MAX(EventTime) as lastDeleted
	                                                FROM eqDocStatesLOG
	                                                WHERE StatusName LIKE '%-> Удален'
	                                                GROUP BY DocID
	                                                ) as A
	                                                LEFT JOIN (
		                                                SELECT  DocID,MAX(EventTime) as lastRecovered
		                                                FROM eqDocStatesLOG
		                                                WHERE StatusName LIKE '%-> Восcтановлен'
		                                                GROUP BY DocID
	                                                ) as B 
	                                                ON A.DocID=B.DocID 
	                                                WHERE A.lastDeleted>B.lastRecovered OR B.lastRecovered is NULL 
                                            ) as D ON D.DocID=A.ID
                                            WHERE A.ISN_OBJ=(SELECT TOP 1 ISN_%C_TYPE% FROM tbl%C_TYPE% WHERE ID=@CID)
                                       </sql>
                        cmdText = cmdText.Replace("%C_TYPE%", conType)
                        Dim cmd As New SqlCommand(cmdText)
                        cmd.Parameters.AddWithValue("@CID", conID)
                        For Each row In AppSettings.GetDataTable(cmd).Rows
                            If IsDBNull(row(1)) Then ' OrElse row(1) > delDate.AddDays(-datePeriod) Then
                                ret.Add(row(0))
                            End If
                        Next
                    Case Else
                End Select
        End Select

        Return ret
    End Function

    ''' <summary>
    ''' Возвращает дату последнего удаления объекта
    ''' </summary>
    ''' <param name="oID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetObjLastDelDate(ByVal oID As Guid) As DateTime
        Dim cmd As New SqlCommand(<sql>
                                    SELECT TOP 1 EventTime
                                    FROM eqDocStatesLOG
                                    WHERE DocID=@OID
                                    AND StatusName LIKE '%Удален%'
                                    ORDER BY EventTime DESC
                               </sql>)
        cmd.Parameters.AddWithValue("oID", oID)
        Return AppSettings.ExecCommandScalar(cmd)
    End Function

    ''' <summary>
    ''' Пишет в лог запись о восстановлении одного объекта 
    ''' </summary>
    ''' <param name="objId"></param>
    ''' <param name="objType"></param>
    ''' <remarks></remarks>
    Private Sub WriteDocStatesLOG(ByVal objId As Guid, ByVal objType As String)
        'eqDocType: WriteDocState

        Dim selectCmdStr As String = <sql>
                                        SELECT TOP 1 ID 
                                        FROM eqDocTypes 
                                        WHERE DocURL=@DocURL                                        
                                     </sql>
        Dim cmdGetObjTypeId As New SqlCommand(selectCmdStr)
        cmdGetObjTypeId.Parameters.AddWithValue("@DocURL", objType + ".ascx")
        Dim objTypeId As Guid = AppSettings.ExecCommandScalar(cmdGetObjTypeId)

        selectCmdStr = <sql>
                        SELECT TOP 1 ID
                        FROM eqDocStates
                        WHERE DocTypeID=@objTypeId
                            AND 
                            StateName='Восстановлен'
                     </sql>
        Dim cmdGetStatId As New SqlCommand(selectCmdStr)
        cmdGetStatId.Parameters.AddWithValue("@objTypeId", objTypeId)
        Dim statId As Guid = AppSettings.ExecCommandScalar(cmdGetStatId)

        Dim insertCmdStr As String = <sql>
                                         INSERT INTO eqDocStatesLOG 
                                               (ID,    DocID, UserID, StatusID, [RollBack], DocTypeID, UserName, StatusName) 
                                        SELECT @RecID,@DocID,@UserID,@StatusID, @RollBack ,@DocTypeID,@UserName,@StatusName
                                     </sql>
        Dim cmdLOG As New SqlCommand(insertCmdStr)
        cmdLOG.Parameters.AddWithValue("@RecID", Guid.NewGuid())
        cmdLOG.Parameters.AddWithValue("@StatusID", statId)
        cmdLOG.Parameters.AddWithValue("@DocID", objId)
        cmdLOG.Parameters.AddWithValue("@USERID", AppSettings.GetRealUserID)
        cmdLOG.Parameters.AddWithValue("@RollBack", False)
        cmdLOG.Parameters.AddWithValue("@DocTypeID", objTypeId)
        cmdLOG.Parameters.AddWithValue("@UserName", AppSettings.UserInfo.UserDisplayName)
        cmdLOG.Parameters.AddWithValue("@StatusName", "Удален -> Восcтановлен")
        AppSettings.ExecCommand(cmdLOG)
    End Sub

    Private Sub ListDocRecovery_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListDocRecovery.Load

    End Sub

    ''' <summary>
    ''' Кнопка выбора всех элементов ниже по иерархии, чем уже отмеченные
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub ButtonSelectChildren_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSelectChildren.Click
        Dim oldSelDocList As New List(Of Guid)(ListDocRecovery.SelectedDocuments)
        For Each elementID In oldSelDocList 'ListDocRecovery.ListDocDataTable.Rows
            For Each el As DataRow In ListDocRecovery.ListDocDataTable.Rows
                If elementID = el.Item("ID") Then
                    SelectAllChildren(el)
                End If
            Next
        Next
        ListDocRecovery.Refresh()

    End Sub

    ''' <summary>
    ''' Рекурсивно делает все невыбранные дочерние элементы выбранных элементов так же выбранными
    ''' </summary>
    ''' <param name="selectedElement"></param>
    ''' <remarks></remarks>
    Private Sub SelectAllChildren(ByVal selectedElement As DataRow)
        For Each el As DataRow In ListDocRecovery.ListDocDataTable.Rows
            If Not IsDBNull(el.Item("TreeParentKey")) AndAlso el.Item("TreeParentKey") = selectedElement.Item("TreeKey") Then
                ListDocRecovery.SelectedDocuments.Add(el.Item("ID"))
                SelectAllChildren(el)
            End If
        Next
    End Sub
End Class