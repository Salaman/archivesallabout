﻿-- Spravochn
CREATE TABLE [dbo].[STORAGE_MEDIUM_CL](
	ID uniqueidentifier NOT NULL,
	OwnerID uniqueidentifier NOT NULL,
	CreationDateTime datetime NOT NULL,
	StatusID uniqueidentifier NOT NULL,
	Deleted bit NOT NULL,
	[ISN_STORAGE_MEDIUM] [int] NOT NULL,
	[ISN_HIGH_STORAGE_MEDIUM] [int] NULL,
	[CODE] [varchar](20) NULL,
	[NAME] [varchar](300) NULL,
	[FOREST_ELEM] [varchar](1) NOT NULL,
	[NOTE] [varchar](max) NULL,
	[PROTECTED] [varchar](1) NULL,
	[DELETED] [varchar](1) NULL,
	[WEIGHT] [int] NULL)
GO
