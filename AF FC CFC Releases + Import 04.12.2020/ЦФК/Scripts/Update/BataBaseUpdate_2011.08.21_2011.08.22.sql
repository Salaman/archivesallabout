﻿set identity_insert dbo.[tblSECURITY_REASON] on

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[tblESTIMATE_REASON_CL]') AND type in (N'U'))
CREATE TABLE [tblESTIMATE_REASON_CL](
	[ID] [uniqueidentifier] NOT NULL,
	[OwnerID] [uniqueidentifier] NOT NULL,
	[CreationDateTime] [datetime] NOT NULL,
	[StatusID] [uniqueidentifier] NOT NULL,
	[Deleted] [bit] NOT NULL,
	[ISN_ESTIMATE_REASON] [int] NOT NULL,
	[CODE] [varchar](20) NULL,
	[NAME] [varchar](300) NULL,
	[NOTE] [varchar](max) NULL,
	[PROTECTED] [varchar](1) NULL,
	[WEIGHT] [int] NULL,
)

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[tblORGANIZ_CL]') AND type in (N'U'))
CREATE TABLE [tblORGANIZ_CL](
	[ID] [uniqueidentifier] NOT NULL,
	[OwnerID] [uniqueidentifier] NOT NULL,
	[CreationDateTime] [smalldatetime] NOT NULL,
	[StatusID] [uniqueidentifier] NOT NULL,
	[Deleted] [int] NOT NULL,
	[ISN_ORGANIZ] [int] NOT NULL,
	[NAME] [varchar](max) NULL,
	[CODE] [varchar](20) NULL,
	[CREATE_YEAR] [smallint] NULL,
	[CREATE_YEAR_INEXACT] [varchar](1) NULL,
	[DELETE_YEAR] [smallint] NULL,
	[DELETE_YEAR_INEXACT] [varchar](1) NULL,
	[ADDRESS] [varchar](max) NULL,
	[CEO_NAME] [varchar](300) NULL,
	[ARCHIVIST_NAME] [varchar](300) NULL,
	[CEO_PHONE] [varchar](300) NULL,
	[ARCHIVIST_PHONE] [varchar](300) NULL,
	[ARCHIVE_REGULATIONS] [varchar](max) NULL,
	[HAS_EPK] [varchar](1) NULL,
	[DELO_INSTRUCTION_YEAR] [smallint] NULL,
	[WORKER_COUNT] [int] NULL,
	[APPROVED_NOM] [varchar](max) NULL,
	[KEEPING_PLACE] [varchar](max) NULL,
	[NOTE] [varchar](max) NULL,
	[WEIGHT] [int] NULL
)

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[tblDECL_COMMISSION_CL]') AND type in (N'U'))
CREATE TABLE [tblDECL_COMMISSION_CL](
	[ID] [uniqueidentifier] NOT NULL,
	[OwnerID] [uniqueidentifier] NOT NULL,
	[CreationDateTime] [smalldatetime] NOT NULL,
	[StatusID] [uniqueidentifier] NOT NULL,
	[Deleted] [int] NOT NULL,
	[ISN_COMMISSION] [int] NOT NULL,
	[CODE] [varchar](20) NULL,
	[NAME_SHORT] [varchar](300) NULL,
	[NAME] [varchar](max) NULL,
	[CREATE_DATE] [datetime] NULL,
	[DELETE_DATE] [datetime] NULL,
	[NOTE] [varchar](max) NULL,
	[PROTECTED] [varchar](1) NULL,
	[WEIGHT] [int] NULL
)

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[tblINVENTORY_STRUCTURE]') AND type in (N'U'))
CREATE TABLE [tblINVENTORY_STRUCTURE](
	[ID] [uniqueidentifier] NOT NULL,
	[OwnerID] [uniqueidentifier] NOT NULL,
	[CreationDateTime] [smalldatetime] NOT NULL,
	[StatusID] [uniqueidentifier] NOT NULL,
	[Deleted] [int] NOT NULL,
	[ISN_INVENTORY_CLS] [int] NOT NULL,
	[ISN_HIGH_INVENTORY_CLS] [int] NULL,
	[ISN_INVENTORY] [int] NULL,
	[CODE] [varchar](20) NULL,
	[NAME] [varchar](300) NULL,
	[NOTE] [varchar](max) NULL,
	[FOREST_ELEM] [varchar](1) NULL,
	[PROTECTED] [varchar](1) NULL,
	[WEIGHT] [int] NULL
)

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[tblTREE_SUPPORT]') AND type in (N'U'))
CREATE TABLE [tblTREE_SUPPORT](
	[ID] [uniqueidentifier] NOT NULL,
	[OwnerID] [uniqueidentifier] NOT NULL,
	[CreationDateTime] [datetime] NOT NULL,
	[StatusID] [uniqueidentifier] NOT NULL,
	[Deleted] [bit] NOT NULL,
	[ISN] [int] NOT NULL,
	[DUE] [varchar](250) NULL,
	[WDUE] [varchar](250) NULL,
)