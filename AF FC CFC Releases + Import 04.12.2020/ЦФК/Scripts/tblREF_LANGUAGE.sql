﻿-- Specification
CREATE TABLE [dbo].[tblREF_LANGUAGE](
	ID uniqueidentifier NOT NULL,
	OwnerID uniqueidentifier NOT NULL,
	CreationDateTime datetime NOT NULL,
	DocID uniqueidentifier NOT NULL,
	RowID int NOT NULL,
	[ISN_REF_LANGUAGE] [int] NOT NULL,
	[ISN_LANGUAGE] [int] NOT NULL,
	[ISN_OBJ] [int] NOT NULL,
	[KIND] [smallint] NULL)
GO
