﻿-- Spravochn
CREATE TABLE [dbo].[LOCATION](
	ID uniqueidentifier NOT NULL,
	OwnerID uniqueidentifier NOT NULL,
	CreationDateTime datetime NOT NULL,
	StatusID uniqueidentifier NOT NULL,
	Deleted bit NOT NULL,
	[ISN_LOCATION] [int] NOT NULL,
	[ISN_HIGH_LOCATION] [int] NULL,
	[ISN_ARCHIVE] [int] NULL,
	[CODE] [varchar](20) NULL,
	[NAME] [varchar](300) NULL,
	[NOTE] [varchar](max) NULL,
	[FOREST_ELEM] [varchar](1) NULL,
	[PROTECTED] [varchar](1) NULL,
	[WEIGHT] [int] NULL)
GO
