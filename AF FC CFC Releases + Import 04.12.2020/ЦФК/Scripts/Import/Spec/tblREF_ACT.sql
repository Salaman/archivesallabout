﻿-- Delete Specification
DELETE FROM [eqTest].[dbo].[tblREF_ACT]
-- Specification
SET IDENTITY_INSERT [eqTest].[dbo].[tblREF_ACT] ON
GO
DECLARE @OwnerID uniqueidentifier
SELECT TOP 1 @OwnerID=ID FROM eqUsers WHERE Login='sa'
INSERT INTO [eqTest].[dbo].[tblREF_ACT]
           ([ID]
           ,[OwnerID]
           ,[CreationDateTime]
           ,[DocID]
           ,[RowID]
           ,[ISN_REF_ACT]
           ,[ISN_ACT]
           ,[ISN_OBJ]
           ,[KIND]
           ,[UNIT_COUNT])
     SELECT
           NEWID()
           ,@OwnerID
           ,GETDATE()
           ,CASE KIND
              WHEN 703 THEN (SELECT TOP 1 ID FROM tblUNIT WHERE tblUNIT.ISN_UNIT = ISN_OBJ)
              ELSE NULL
            END
           ,0
           ,[ISN_REF_ACT]
           ,[ISN_ACT]
           ,[ISN_OBJ]
           ,[KIND]
           ,[UNIT_COUNT]
     FROM [ROS_DB].[dbo].[REF_ACT]
GO
SET IDENTITY_INSERT [eqTest].[dbo].[tblREF_ACT] OFF
GO
