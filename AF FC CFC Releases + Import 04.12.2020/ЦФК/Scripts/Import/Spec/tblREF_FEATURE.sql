﻿-- Delete Specification
DELETE FROM [eqTest].[dbo].[tblREF_FEATURE]
-- Specification
SET IDENTITY_INSERT [eqTest].[dbo].[tblREF_FEATURE] ON
GO
DECLARE @OwnerID uniqueidentifier
SELECT TOP 1 @OwnerID=ID FROM eqUsers WHERE Login='sa'
INSERT INTO [eqTest].[dbo].[tblREF_FEATURE]
           ([ID]
           ,[OwnerID]
           ,[CreationDateTime]
           ,[DocID]
           ,[RowID]
           ,[ISN_REF_FEATURE]
           ,[ISN_FEATURE]
           ,[ISN_OBJ]
           ,[KIND]
           ,[ORDER_NUM])
     SELECT
           NEWID()
           ,@OwnerID
           ,GETDATE()
           ,CASE KIND
              WHEN 703 THEN (SELECT TOP 1 ID FROM tblUNIT WHERE tblUNIT.ISN_UNIT = ISN_OBJ)
              ELSE NULL
            END
           ,0
           ,[ISN_REF_FEATURE]
           ,[ISN_FEATURE]
           ,[ISN_OBJ]
           ,[KIND]
           ,[ORDER_NUM]
     FROM [ROS_DB].[dbo].[REF_FEATURE]
GO
SET IDENTITY_INSERT [eqTest].[dbo].[tblREF_FEATURE] OFF
GO
