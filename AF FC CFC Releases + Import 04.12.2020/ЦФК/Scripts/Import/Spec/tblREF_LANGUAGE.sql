﻿-- Delete Specification
DELETE FROM [eqTest].[dbo].[tblREF_LANGUAGE]
-- Specification
SET IDENTITY_INSERT [eqTest].[dbo].[tblREF_LANGUAGE] ON
GO
DECLARE @OwnerID uniqueidentifier
SELECT TOP 1 @OwnerID=ID FROM eqUsers WHERE Login='sa'
INSERT INTO [eqTest].[dbo].[tblREF_LANGUAGE]
           ([ID]
           ,[OwnerID]
           ,[CreationDateTime]
           ,[DocID]
           ,[RowID]
           ,[ISN_REF_LANGUAGE]
           ,[ISN_LANGUAGE]
           ,[ISN_OBJ]
           ,[KIND])
     SELECT
           NEWID()
           ,@OwnerID
           ,GETDATE()
           ,CASE KIND
              WHEN 703 THEN (SELECT TOP 1 ID FROM tblUNIT WHERE tblUNIT.ISN_UNIT = ISN_OBJ)
              ELSE NULL
            END
           ,0
           ,[ISN_REF_LANGUAGE]
           ,[ISN_LANGUAGE]
           ,[ISN_OBJ]
           ,[KIND]
     FROM [ROS_DB].[dbo].[REF_LANGUAGE]
GO
SET IDENTITY_INSERT [eqTest].[dbo].[tblREF_LANGUAGE] OFF
GO
