﻿SET IDENTITY_INSERT [eqTest].[dbo].[tblSECURLEVEL] ON
GO
DECLARE @OwnerID uniqueidentifier
DECLARE @StatusID uniqueidentifier
SELECT TOP 1 @OwnerID=ID FROM eqUsers WHERE Login='sa'
SELECT @StatusID=ID FROM eqDocStates WHERE StateName='Сохранен' AND DocTypeID=(SELECT ID FROM eqDocTypes WHERE DocType='Уровень доступа')
INSERT INTO [eqTest].[dbo].[tblSECURLEVEL]
           ([ID]
           ,[OwnerID]
           ,[CreationDateTime]
           ,[StatusID]
           ,[Deleted]
           ,[ISN_SECURLEVEL]
           ,[CODE]
           ,[NAME]
           ,[NOTE]
           ,[PROTECTED]
           ,[WEIGHT])
     SELECT
           NEWID()
           ,@OwnerID
           ,GETDATE()
           ,@StatusID
           ,CASE [Deleted] WHEN 'Y' THEN 1 ELSE 0 END
           ,[ISN_SECURLEVEL]
           ,[CODE]
           ,[NAME]
           ,[NOTE]
           ,[PROTECTED]
           ,[WEIGHT]
     FROM [ROS_DB].[dbo].[SECURLEVEL]
GO
SET IDENTITY_INSERT [eqTest].[dbo].[tblSECURLEVEL] OFF
GO
