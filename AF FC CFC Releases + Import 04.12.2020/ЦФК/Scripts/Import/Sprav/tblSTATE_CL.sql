﻿SET IDENTITY_INSERT [eqTest].[dbo].[tblSTATE_CL] ON
GO
DECLARE @OwnerID uniqueidentifier
DECLARE @StatusID uniqueidentifier
SELECT TOP 1 @OwnerID=ID FROM eqUsers WHERE Login='sa'
SELECT @StatusID=ID FROM eqDocStates WHERE StateName='Сохранен' AND DocTypeID=(SELECT ID FROM eqDocTypes WHERE DocType='Дефекты документов')
INSERT INTO [eqTest].[dbo].[tblSTATE_CL]
           ([ID]
           ,[OwnerID]
           ,[CreationDateTime]
           ,[StatusID]
           ,[Deleted]
           ,[ISN_HIGH_STATE]
           ,[CODE]
           ,[NAME]
           ,[NOTE]
           ,[FOREST_ELEM]
           ,[PROTECTED]
           ,[WEIGHT])
     SELECT
           NEWID()
           ,@OwnerID
           ,GETDATE()
           ,@StatusID
           ,CASE [Deleted] WHEN 'Y' THEN 1 ELSE 0 END
           ,[ISN_HIGH_STATE]
           ,[CODE]
           ,[NAME]
           ,[NOTE]
           ,[FOREST_ELEM]
           ,[PROTECTED]
           ,[WEIGHT]
     FROM [ROS_DB].[dbo].[STATE_CL]
GO
SET IDENTITY_INSERT [eqTest].[dbo].[tblSTATE_CL] OFF
GO
