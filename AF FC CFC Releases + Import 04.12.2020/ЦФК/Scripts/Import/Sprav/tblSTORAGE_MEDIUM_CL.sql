﻿SET IDENTITY_INSERT [eqTest].[dbo].[tblSTORAGE_MEDIUM_CL] ON
GO
DECLARE @OwnerID uniqueidentifier
DECLARE @StatusID uniqueidentifier
SELECT TOP 1 @OwnerID=ID FROM eqUsers WHERE Login='sa'
SELECT @StatusID=ID FROM eqDocStates WHERE StateName='Сохранен' AND DocTypeID=(SELECT ID FROM eqDocTypes WHERE DocType='Носители документов')
INSERT INTO [eqTest].[dbo].[tblSTORAGE_MEDIUM_CL]
           ([ID]
           ,[OwnerID]
           ,[CreationDateTime]
           ,[StatusID]
           ,[Deleted]
           ,[ISN_HIGH_STORAGE_MEDIUM]
           ,[CODE]
           ,[NAME]
           ,[FOREST_ELEM]
           ,[NOTE]
           ,[PROTECTED]
           ,[WEIGHT])
     SELECT
           NEWID()
           ,@OwnerID
           ,GETDATE()
           ,@StatusID
           ,CASE [Deleted] WHEN 'Y' THEN 1 ELSE 0 END
           ,[ISN_HIGH_STORAGE_MEDIUM]
           ,[CODE]
           ,[NAME]
           ,[FOREST_ELEM]
           ,[NOTE]
           ,[PROTECTED]
           ,[WEIGHT]
     FROM [ROS_DB].[dbo].[STORAGE_MEDIUM_CL]
GO
SET IDENTITY_INSERT [eqTest].[dbo].[tblSTORAGE_MEDIUM_CL] OFF
GO
