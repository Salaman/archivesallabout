﻿-- Spravochn
CREATE TABLE [dbo].[tblDOC_KIND_CL](
	ID uniqueidentifier NOT NULL,
	OwnerID uniqueidentifier NOT NULL,
	CreationDateTime datetime NOT NULL,
	StatusID uniqueidentifier NOT NULL,
	Deleted bit NOT NULL,
	[ISN_DOC_KIND] [int] NOT NULL,
	[ISN_HIGH_DOC_KIND] [int] NULL,
	[CODE] [varchar](20) NULL,
	[NAME] [varchar](300) NULL,
	[FOREST_ELEM] [varchar](1) NOT NULL,
	[NOTE] [varchar](max) NULL,
	[PROTECTED] [varchar](1) NULL,
	[WEIGHT] [int] NULL)
GO
