﻿-- Specification
CREATE TABLE [dbo].[tblREF_FEATURE](
	ID uniqueidentifier NOT NULL,
	OwnerID uniqueidentifier NOT NULL,
	CreationDateTime datetime NOT NULL,
	DocID uniqueidentifier NOT NULL,
	RowID int NOT NULL,
	[ISN_REF_FEATURE] [int] NOT NULL,
	[ISN_FEATURE] [int] NULL,
	[ISN_OBJ] [int] NULL,
	[KIND] [smallint] NULL,
	[ORDER_NUM] [int] NULL)
GO