﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ChooseArchives.aspx.vb"
    Inherits="WebApplication.ChooseArchives" %>

<%@ Register Src="ListDoc.ascx" TagName="ListDoc" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Архивы</title>
</head>
<body style="background-color: White; padding: 5px;">
    <form id="form1" runat="server">
    <aj:ToolkitScriptManager ID="ToolkitScriptManagerDefault" runat="server" EnableScriptGlobalization="true"
        ScriptMode="Release" EnablePartialRendering="True" EnableHistory="true" />
    <table style="width: 100%;">
        <tr>
            <td>
                <div class="LogicBlockCaption">
                    Выберите архивы и нажмите "Выбрать"
                </div>
            </td>
            <td style="text-align: right;">
                <asp:Button ID="ButtonChooseArchives" runat="server" Text="Выбрать" />
            </td>
        </tr>
    </table>
    <asp:UpdatePanel ID="UpdatePanelMovement" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <uc1:ListDoc ID="ListArchives" runat="server" DocTypeURL="ARCHIVE.ascx" SelectDocMode="Multi" />
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
