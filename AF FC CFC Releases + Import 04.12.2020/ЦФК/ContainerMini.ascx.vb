﻿Public Partial Class ContainerMini
    Inherits BaseContainer

#Region "Properties"

    ''' <summary>Задание режима отображения</summary>
    ''' <value>Название отображения</value>
    Public Property ShowMode() As String
        Get
            If ViewState("ShowMode") Is Nothing Then
                Return String.Empty
            Else
                Return ViewState("ShowMode")
            End If
        End Get
        Set(ByVal value As String)
            ViewState("ShowMode") = value
        End Set
    End Property

    Public Property EnabledDoc() As Boolean
        Get
            Return PanelPlaceHolder.Enabled
        End Get
        Set(ByVal value As Boolean)
            PanelPlaceHolder.Enabled = value
        End Set
    End Property

    Public Property VisibleDoc() As Boolean
        Get
            Return PanelPlaceHolder.Visible
        End Get
        Set(ByVal value As Boolean)
            PanelPlaceHolder.Visible = value
        End Set
    End Property

    Public Property ShowingFieldsFormat() As String
        Get
            Return _ShowingFieldsFormat
        End Get
        Set(ByVal value As String)
            _ShowingFieldsFormat = value
        End Set
    End Property
    Private _ShowingFieldsFormat As String

    Public Property ShowingFields() As String
        Get
            Return _ShowingFields
        End Get
        Set(ByVal value As String)
            _ShowingFields = value
        End Set
    End Property
    Private _ShowingFields As String

    Protected Overrides ReadOnly Property ContentPlaceHolder() As System.Web.UI.WebControls.PlaceHolder
        Get
            Return PlaceHolderContent
        End Get
    End Property

#End Region

#Region "Events"

    Private Sub Action_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Action.Click
        If DocID <> Guid.Empty Then
            DoLoadDoc(DocID)
        Else
            DoListDoc()
        End If
    End Sub

#End Region

#Region "Metods"

    Public Overrides Sub LoadDocumentMenu()
        'MyBase.LoadDocumentMenu()
        'If DocID <> Guid.Empty Then
        '    AddMenuItem("Открыть", "LoadDoc", "Images/Open.gif", "Открыть документ")
        'End If
    End Sub

    Public Overrides Sub AddMenuItem(ByVal Text As String, ByVal CommandName As String, ByVal ImageUrl As String, Optional ByVal ToolTip As String = "", Optional ByVal SeparatorBefore As Boolean = False, Optional ByVal AddAtIndex As Integer = -1)
        'Dim MenuActionsItem As New MenuItem(Text, CommandName)
        'If ImageUrl <> "" Then MenuActionsItem.ImageUrl = BasePage.GetThemeRelativeURL(ImageUrl)
        'If ToolTip <> "" Then MenuActionsItem.ToolTip = ToolTip Else MenuActionsItem.ToolTip = Text
        'Select Case MenuItemMode
        '    Case MenuItemModes.Images
        '        MenuActions.StaticItemFormatString = ""
        '        MenuActions.DynamicItemFormatString = ""
        '        MenuActionsItem.Text = String.Empty
        '    Case MenuItemModes.Text
        '        MenuActionsItem.ImageUrl = String.Empty
        'End Select
        'If AddAtIndex > -1 Then
        '    MenuActions.Items.AddAt(AddAtIndex, MenuActionsItem)
        'Else
        '    MenuActions.Items.Add(MenuActionsItem)
        'End If
        'MenuActions.Visible = True
    End Sub

    Public Overrides Function IndexOfMenuItem(ByVal CommandName As String) As Integer
        'For Each MenuActionsItem As MenuItem In MenuActions.Items
        '    If MenuActionsItem.Value = CommandName Then
        '        Return MenuActions.Items.IndexOf(MenuActionsItem)
        '    End If
        'Next
        Return -1
    End Function

    Public Overrides Sub RemoveMenuItem(ByVal CommandName As String)
        'For Each MenuActionsItem As MenuItem In MenuActions.Items
        '    If MenuActionsItem.Value = CommandName Then
        '        MenuActions.Items.Remove(MenuActionsItem)
        '        Exit Sub
        '    End If
        'Next
        'MenuActions.Visible = MenuActions.Items.Count > 0
    End Sub

    Public Overrides Sub ClearMenuItems()
        'MenuActions.Items.Clear()
        'MenuActions.Visible = False
    End Sub

    Public Overrides Sub SetTitle(ByVal Title As String)
        Action.Text = Title
        Action.Enabled = True
        Action.Visible = True
        If DocID <> Guid.Empty Then
            Try
                If ShowingFields IsNot Nothing Then
                    Dim Fields As New List(Of Object)
                    For Each ShowingField In ShowingFields.Split(New Char() {";", ","}, StringSplitOptions.RemoveEmptyEntries)
                        Fields.Add(DocControl.DataSetHeaderDataRow(ShowingField.Trim))
                    Next
                    Action.Text = String.Format(ShowingFieldsFormat, Fields.ToArray)
                End If
            Catch ex As Exception
                AddMessage("ContainerMini (" & DocTypeURL & ") Error -> SetTitle -> " & ex.Message)
                Action.Text = Title
            End Try
            If LoadDocMode = eqBaseContainer.LoadDocModes.NewWindowAndSelect Then
                'Action.ToolTip = "Открыть документ в новом окне или выбрать другой документ из списка"
                Action.ToolTip = GetGlobalResourceObject("eqResources", "TIP_OpenDocNewWindowOrSelectFromList")
            ElseIf LoadDocMode = eqBaseContainer.LoadDocModes.NewWindow Then
                'Action.ToolTip = "Открыть документ в новом окне"
                Action.ToolTip = GetGlobalResourceObject("eqResources", "TIP_OpenDocNewWindow")
            Else
                Action.Enabled = False
            End If
        Else
            If ListDocMode = eqBaseContainer.ListDocModes.NewWindowAndSelect Then
                'Action.Text = "Выбрать"
                Action.Text = GetGlobalResourceObject("eqResources", "CAP_Select")
                'Action.ToolTip = "Выбрать документ из списка"
                Action.ToolTip = GetGlobalResourceObject("eqResources", "TIP_SelectDocFromList")
            Else
                Action.Visible = False
            End If
        End If
    End Sub

#End Region

End Class