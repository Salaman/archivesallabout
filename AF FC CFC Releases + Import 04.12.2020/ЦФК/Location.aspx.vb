﻿Imports System.Data.SqlClient

Partial Public Class Location1
    Inherits BasePage

    Private Sub ButtonLocation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonLocation.Click
        Dim cmd As New SqlCommand
        cmd.Parameters.AddWithValue("@OwnerID", AppSettings.UserInfo.UserID)
        cmd.Parameters.Add(New SqlParameter("@ISN_REF_LOCATION", SqlDbType.BigInt) With {.Value = 0})
        cmd.Parameters.Add(New SqlParameter("@ISN_LOCATION", SqlDbType.BigInt) With {.Value = 0})
        cmd.Parameters.Add(New SqlParameter("@ISN_OBJ", SqlDbType.BigInt) With {.Value = 0})
        cmd.Parameters.Add(New SqlParameter("@KIND", SqlDbType.BigInt) With {.Value = 0})

        Dim selectedDocs = DirectCast(ModalDialogArgument.ArgumentObject, List(Of Guid))
        For Each selectedLocation In ListDocLocation.SelectedDocuments
            For Each selectedDoc In selectedDocs
                cmd.CommandText += GetInsertRefLocationCmd(Request.QueryString("DocTypeURL"), selectedDoc, selectedLocation)
            Next
        Next

        If String.IsNullOrEmpty(cmd.CommandText) Then
            AddMessage("Выберите не менее одной рубрики")
        Else
            AppSettings.ExecCommand(cmd)
            CloseWindow()
        End If
    End Sub

    Private Function GetInsertRefLocationCmd(ByVal docTypeURL As String, ByVal docId As Guid, ByVal locationId As Guid) As String
        Dim isnRefLoc = "SELECT @ISN_REF_LOCATION=ISNULL(MAX(ISN_REF_LOCATION)+1,1) FROM tblREF_LOCATION" & vbCrLf
        Dim isnLoc = "SELECT @ISN_LOCATION=ISN_LOCATION FROM tblLOCATION WHERE ID='" & locationId.ToString & "'" & vbCrLf
        Dim isnObj As String = Nothing
        Dim kind As String = Nothing

        Select Case docTypeURL.ToUpper
            Case "FUND.ASCX"
                isnObj = "SELECT @ISN_OBJ=ISN_FUND FROM tblFUND WHERE ID='" & docId.ToString & "'" & vbCrLf
                kind = "SELECT @KIND=701" & vbCrLf
            Case "INVENTORY.ASCX"
                isnObj = "SELECT @ISN_OBJ=ISN_INVENTORY FROM tblINVENTORY WHERE ID='" & docId.ToString & "'" & vbCrLf
                kind = "SELECT @KIND=702" & vbCrLf
            Case "UNIT.ASCX"
                Return isnLoc & "UPDATE tblUNIT SET ISN_LOCATION=@ISN_LOCATION WHERE ID='" & docId.ToString & "'" & vbCrLf
        End Select

        Return isnRefLoc & isnLoc & isnObj & kind & "DELETE FROM [tblREF_LOCATION] WHERE DocID='" & docId.ToString & "' AND ISN_LOCATION=@ISN_LOCATION" & vbCrLf & _
        "INSERT INTO [tblREF_LOCATION] ([ID],[OwnerID],[CreationDateTime],[DocID],[RowID],[ISN_REF_LOCATION],[ISN_LOCATION],[ISN_OBJ],[UNIT_NUM_FROM],[UNIT_NUM_TO],[KIND],[NOTE])" & vbCrLf & _
                            "VALUES (NEWID(),@OwnerID,GETDATE(),'" & docId.ToString & "',0,@ISN_REF_LOCATION,@ISN_LOCATION,@ISN_OBJ,NULL,NULL,@KIND,NULL)" & vbCrLf
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Select Case Request.QueryString("DocTypeURL").ToUpper
            Case "UNIT.ASCX"
                ListDocLocation.SelectDocMode = eqBaseContainer.SelectDocModes.Single
                ButtonLocation.Visible = False
        End Select
    End Sub

    Private Sub ListDocLocation_SelectDoc(ByVal DocID As System.Guid) Handles ListDocLocation.SelectDoc
        ListDocLocation.SelectedDocuments.Clear()
        ListDocLocation.SelectedDocuments.Add(DocID)
        ButtonLocation_Click(Me, EventArgs.Empty)
    End Sub

End Class