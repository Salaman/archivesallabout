﻿Imports System.Diagnostics.Eventing.Reader
Imports System.IO
Imports System.Net
Imports System.Security.AccessControl
Imports Microsoft.VisualBasic.FileIO
Imports Aspose.Cells

Partial Public Class ListDoc

#Region "Excel"
    Public Overrides Sub ExportToExcel()
        Try
            Dim ExcelWorkbook As New Aspose.Cells.Workbook
            Dim ExcelWorksheet As Aspose.Cells.Worksheet = ExcelWorkbook.Worksheets(0)
            ExcelWorksheet.Name = DocType.DocTypeName
            BuildWorksheet(ExcelWorksheet)
            Dim ExcelWorkbookMemoryStream As New IO.MemoryStream
            ExcelWorkbook.Save(ExcelWorkbookMemoryStream, Aspose.Cells.FileFormatType.Default)
            'ExcelWorkbook.Save(ExcelWorkbookMemoryStream, Aspose.Cells.FileFormatType.AsposePdf)
            BasePage.AttachFile("ListDoc_" & eqMath.Transliterate(DocType.DocTypeName) & "_" & Now.ToString("yyyy.MM.dd_HH.mm.ss") & ".xls", ExcelWorkbookMemoryStream.ToArray)
            'BasePage.AttachFile("ListDoc_" & DocType.DocTypeName & "_" & Now.ToString("yyyy.MM.dd_HH.mm.ss") & ".pdf", ExcelWorkbookMemoryStream.ToArray)
        Catch ex As Exception
            AddMessage("Ошибка при выгрузке в Excel -> " & ex.Message)
        End Try
    End Sub

    Protected Sub FileUpload_Init(sender As Object, e As EventArgs) Handles FileUpload.Init
        FileUpload.Attributes.Add("accept",".xls, .xlsx")
    End Sub

    Protected Sub btnFileUploadOk_Click(sender As Object, e As EventArgs) Handles btnFileUploadOk.Click

        If FileUpload.HasFile
            Dim tempFile = Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString() & Path.GetExtension(FileUpload.FileName))
            FileUpload.SaveAs(tempFile)

            Select Case DocTypeURL.ToUpper
                Case "UNIT.ASCX"
                    _ImportUnitsFromExcel(tempFile)
                Case "DOCUMENT.ASCX"
                    _ImportUnitDocsFromExcel(tempFile)
            End Select

            File.Delete(tempFile)
        End If
    End Sub
    
    Public Overrides Sub ExportUnitDocsExcelTemplate()
        Dim fileName = "Shablon spiska documentov.xls"
        BasePage.AttachFile(fileName, FileSystem.ReadAllBytes(Path.Combine(Server.MapPath("~/App_Data"), fileName)))
    End Sub

    Public Overrides Sub ExportUnitsExcelTemplate()
        Dim fileName = "Shablon spiska edenic hraneniya.xls"
        BasePage.AttachFile(fileName, FileSystem.ReadAllBytes(Path.Combine(Server.MapPath("~/App_Data"), fileName)))
    End Sub

    Public Overrides Sub ImportUnitDocsFromExcel()
        UploadFilePopup.Show()
    End Sub

    Private Sub _ImportUnitDocsFromExcel(fileName As String)
        
        Dim wb As New Workbook()
        Try
            wb.LoadData(fileName)
        Catch ex As IOException
            AddMessage("Не удаётся получиться доступ к файлу '" & fileName & "'")
            Return
        End Try   
        
        Dim ws As Worksheet = wb.Worksheets(0)
        Dim rowsCount As Integer = ws.Cells.MaxRow
        'Кол-во документов в которых обнаружены ошибки
        Dim failedCount As Integer = 0
        Dim isFailed As Boolean = False

          For i = 2 To rowsCount 
            
            isFailed = False
            Dim doc As DOCUMENT = LoadControl(BasePage.DocumentsDirectoryUrl & "DOCUMENT.ascx")
            Dim oldErrorsCount = doc.BasePage.GetMessagesCollection().Count

            doc.DocID = Guid.Empty
            doc.NewDoc()
            'doc.Load(Nothing, Nothing)
            doc.BaseContainer = BaseContainer

            doc.DOC_NUM.Text = ws.Cells(i, 0).StringValue
            doc.NAME.Text =  ws.Cells(i, 1).StringValue
            doc.DOCUM_DATE_FMT.Text =  ws.Cells(i, 2).StringValue
            doc.PAGE_COUNT.Text = ws.Cells(i, 3).StringValue
            doc.PAGE_FROM.Text = ws.Cells(i, 4).StringValue
            doc.PAGE_TO.Text = ws.Cells(i, 5).StringValue
            doc.NOTE.Text = doc.PAGE_FROM.Text & " - " & doc.PAGE_TO.Text

            Dim text = ws.Cells(i, 6).StringValue
            If Not TrySetValueByText(doc.ISN_SECURLEVEL, text) AndAlso text IsNot Nothing
                AddMessage("Поле: Характеристика секретности -> Содержит недопустимое значение " & "'" & text & "'")
                isFailed = True
            End If

            text = ws.Cells(i, 7).StringValue
            If Not String.IsNullOrEmpty(text)
                Dim DocType = AppSettings.GetDocType("DOC_KIND_CL.ascx")
                Dim SqlCommand As New SqlClient.SqlCommand(String.Format("SELECT ISN_DOC_KIND FROM {1} WHERE [{0}]=@KeyValue", "NAME", DocType.GetHeaderTableName))
                SqlCommand.Parameters.AddWithValue("@KeyValue", text)
                Dim DocumentDataTable = AppSettings.GetDataTable(SqlCommand)
                If DocumentDataTable IsNot Nothing AndAlso DocumentDataTable.Rows.Count = 0 Then
                    AddMessage("Поле: Вид документа -> Содержит недопустимое значение " & "'" & text & "'")
                    isFailed = True
                Else 
                    doc.ISN_DOC_KIND.KeyValue =  DocumentDataTable.Rows(0)("ISN_DOC_KIND")
                End If
            End If
            
            If Not IsFailed Then
                doc.LoadDocAfterSave = False
                doc.SaveDoc()
            End If

            Dim errors = BasePage.GetMessagesCollection()
            If errors.Count - oldErrorsCount > 0 Then
                errors.Insert(oldErrorsCount, "Номер документа - " & doc.DOC_NUM.Text & ". Ошибки:")
                failedCount += 1
                AddMessage("======================================================")
            End If
            
        Next i

        Dim msgs = BasePage.GetMessagesCollection()
        msgs.Insert(0, "Успешно загружены " & rowsCount - 1 - failedCount  & " документов")
        msgs.Insert(1, "Обнаружены ошибки в " & failedCount & " документов")
        msgs.Insert(2, "======================================================")

        AddMessage(string.Empty)

    End Sub


    Public Overrides Sub ImportUnitsFromExcel()
        UploadFilePopup.Show()
    End Sub

    Private Sub _ImportUnitsFromExcel(fileName As String)
        
        Dim wb As New Workbook()
        Try
            wb.LoadData(fileName)
        Catch ex As IOException
            AddMessage("Не удаётся получиться доступ к файлу '" & fileName & "'")
            Return
        End Try   
        
        Dim ws As Worksheet = wb.Worksheets(0)
        Dim rowsCount As Integer = ws.Cells.MaxRow
        'Кол-во ед.хранения в которых обнаружены ошибки
        Dim failedCount As Integer = 0
        Dim isFailed As Boolean = False

        For i = 2 To rowsCount 
            
            isFailed = False
            Dim unit As _UNIT = LoadControl(BasePage.DocumentsDirectoryUrl & "UNIT.ascx")
            Dim oldErrorsCount = unit.BasePage.GetMessagesCollection().Count

            unit.DocID = Guid.Empty
            unit.NewDoc()
            unit.Page_Load(Nothing, Nothing)
            unit.BaseContainer = BaseContainer

            unit.UNIT_NUM_1.Text = ws.Cells(i, 0).StringValue
            unit.UNIT_NUM_2.Text =  ws.Cells(i, 1).StringValue
            unit.UNIT_NUM_TXT.Text =  ws.Cells(i, 2).StringValue
            unit.VOL_NUM.Text = ws.Cells(i, 3).StringValue
            unit.PAGE_COUNT.Text = ws.Cells(i, 4).StringValue
            unit.NAME.Text = ws.Cells(i, 5).StringValue

            Dim text = ws.Cells(i, 6).StringValue
            If Not TrySetValueByText(unit.ISN_DOC_TYPE, text) AndAlso text IsNot Nothing
                AddMessage("Поле: Тип документов -> Содержит недопустимое значение " & "'" & text & "'")
                isFailed = True
            End If

            text = ws.Cells(i, 7).StringValue
            If Not TrySetValueByText(unit.MEDIUM_TYPE, text) AndAlso text IsNot Nothing
                AddMessage("Поле: Тип носителя -> Содержит недопустимое значение " & "'" & text & "'")
                isFailed = True
            End If
            
            'Doc.ISN_STORAGE_MEDIUM =  ws.Cells(i, 8).StringValue
            'Doc.vREF_LANGUAGE_UNIT.
            'Doc.ISN_DOC_TYPE.SelectedIndex = 1

            text = ws.Cells(i, 8).StringValue
            If Not TrySetValueByText(unit.ISN_SECURLEVEL, text) AndAlso text IsNot Nothing
                AddMessage("Поле: Характеристика секретности -> Содержит недопустимое значение " & "'" & text & "'")
                isFailed = True
            End If

            text = ws.Cells(i, 9).StringValue
            If Not TrySetValueByText(unit.SECURITY_CHAR, text) AndAlso text IsNot Nothing
                AddMessage("Поле: Доступ -> Содержит недопустимое значение " & "'" & text & "'")
                isFailed = True
            End If
            
            Dim langList = DirectCast(unit.vREF_LANGUAGE_UNIT.Rows(0).FindControl("ISN_LANGUAGE"), eqDropDownList)
            text = ws.Cells(i, 10).StringValue
            If Not TrySetValueByText(langList, text.ToLower()) AndAlso text IsNot Nothing
                AddMessage("Поле: Язык документов -> Содержит недопустимое значение " & "'" & text & "'")
                isFailed = True
            End If
            
            unit.START_YEAR.Text = ws.Cells(i, 11).StringValue
            unit.END_YEAR.Text = ws.Cells(i, 12).StringValue
            unit.ALL_DATE.Text = ws.Cells(i, 13).StringValue
            unit.Note.Text = ws.Cells(i, 14).StringValue
            unit.UNIT_KIND.SelectedIndex = 1
            
            If Not IsFailed Then
                unit.LoadDocAfterSave = False
                unit.SaveDoc()
            End If

            Dim errors = BasePage.GetMessagesCollection()
            If errors.Count - oldErrorsCount > 0 Then
                errors.Insert(oldErrorsCount, "Номер единицы хранения - " & unit.UNIT_NUM_1.Text & ". Ошибки:")
                failedCount += 1
                AddMessage("======================================================")
            End If
            
        Next i

        Dim msgs = BasePage.GetMessagesCollection()
        msgs.Insert(0, "Успешно загружены " & rowsCount - 1 - failedCount  & " eд. хранения")
        msgs.Insert(1, "Обнаружены ошибки в " & failedCount & " eд. хранения")
        msgs.Insert(2, "======================================================")

        AddMessage(string.Empty)
       
    End Sub

    Private Function TrySetValueByText(ByRef list As DropDownList, ByVal text As String) As Boolean
        Dim item = list.Items.FindByText(text)
        If item IsNot Nothing Then
            list.SelectedValue = item.Value
            Return True
        End If
        Return False
    End Function

    Public Overrides Sub ImportExcel()
        'Dim BaseDocControl As eqBaseDoc = LoadControl(BasePage.DocumentsDirectoryUrl & "UNIT.ascx")
        'BaseDocControl.SaveDoc()
        'Dim message = BaseDocControl.BasePage.GetMessagesCollection
        'BasePage.AttachFile(fileName, FileSystem.ReadAllBytes(Path.Combine(Server.MapPath("~/App_Data"), fileName)))
    End Sub

    Dim CurrentRowIndex As Integer
    Dim CurrentColIndex As Integer

    Private Sub BuildWorksheet(ByVal Worksheet As Aspose.Cells.Worksheet)

        ' Header
        CurrentRowIndex = 0
        CurrentColIndex = 0

        ' Fields
        For Each F As DataRowView In ViewEnabledFields
            Dim FieldID = DirectCast(F("FieldID"), Guid)
            Dim FieldInfo = DocType.GetFieldInfo(FieldID)
            Dim ListDocFieldInfo = DocType.GetListDocFieldInfo(FieldID)
            Dim Cell As Aspose.Cells.Cell = Worksheet.Cells(CurrentRowIndex, CurrentColIndex)
            If Not IsDBNull(F("Width")) Then
                Dim Width As Unit
                Try
                    Width = Unit.Parse(F("Width"))
                Catch ex As Exception
                    Width = Unit.Empty
                End Try
                If Not Width.IsEmpty Then
                    Select Case Width.Type
                        Case UnitType.Pixel
                            Worksheet.Cells.SetColumnWidthPixel(CurrentColIndex, CInt(Width.Value))
                            'Case UnitType.Inch
                            'Worksheet.Cells.SetColumnWidthInch(CurrentColIndex, Width.Value)
                        Case Else
                            Worksheet.Cells.SetColumnWidth(CurrentColIndex, Width.Value)
                    End Select
                Else
                    Worksheet.Cells.SetColumnWidth(CurrentColIndex, 26)
                End If
            End If
            Worksheet.Comments.Add(CurrentRowIndex, CurrentColIndex)
            Worksheet.Comments(CurrentRowIndex, CurrentColIndex).Note = ListDocFieldInfo("FieldDesc").ToString
            PutValueToCell(Cell, F("Header"))
            Cell.Style.Font.IsBold = True
            Cell.Style.IsTextWrapped = True
            CurrentColIndex += 1
        Next

        If ListDocDataTable IsNot Nothing Then

            ' Groups
            Dim ListDocRows = From R As DataRow In ListDocDataTable.Rows
            Dim FirstListDocGroup = CreateListDocGroup(ListDocRows)

            ' Groups
            ProcessListDocGroupWorksheet(Worksheet, FirstListDocGroup)

            ' Aggregates
            If FirstListDocGroup.AggregatesValues.Count > 0 Then
                CurrentRowIndex += 1
                CurrentColIndex = 0
                For Each F As DataRowView In ViewEnabledFields
                    Dim FieldID = DirectCast(F("FieldID"), Guid)
                    Dim FieldInfo = DocType.GetFieldInfo(FieldID)
                    Dim ListDocFieldInfo = DocType.GetListDocFieldInfo(FieldID)
                    If FirstListDocGroup.AggregatesValues.ContainsKey(FieldID.ToString("n")) Then
                        Dim AggregateValue = FirstListDocGroup.AggregatesValues(FieldID.ToString("n"))
                        If AggregateValue IsNot Nothing AndAlso Not IsDBNull(AggregateValue) Then
                            Dim Cell As Aspose.Cells.Cell = Worksheet.Cells(CurrentRowIndex, CurrentColIndex)
                            Dim FieldFormat = ListDocFieldInfo("FieldFormat").ToString
                            If FieldFormat Like "{0:d}" Then
                                FieldFormat = "dd-mmm-yy"
                            End If

                            If FieldFormat Like "{0:*}" Then
                                FieldFormat = Replace(FieldFormat, "}", "")
                                FieldFormat = Replace(FieldFormat, "{0:", "")
                            End If
                            If FieldFormat = "" Then
                                PutValueToCell(Cell, eqView.GetAggregateTypeString(F("AggregateType")) & ": " & AggregateValue.ToString)
                            Else
                                'PutValueToCell(Cell, eqView.GetAggregateTypeString(F("AggregateType")) & ": " & Format(AggregateValue, FieldFormat))
                                If FieldFormat Like "{0:*}" Then
                                    FieldFormat = Replace(FieldFormat, "}", "")
                                    FieldFormat = Replace(FieldFormat, "{0:", "")
                                Else
                                    FieldFormat = ""
                                End If
                                PutValueToCell(Cell, eqView.GetAggregateTypeString(F("AggregateType")) & ": " & Format(AggregateValue, FieldFormat))
                            End If
                        End If
                    End If
                    CurrentColIndex += 1
                Next
                Worksheet.Cells.GroupRows(1, CurrentRowIndex - 1, False)
            End If

        End If

        'Worksheet.AutoFitColumns()
        Worksheet.AutoFitRows()

    End Sub

    Private Sub ProcessListDocGroupWorksheet(ByVal Worksheet As Aspose.Cells.Worksheet, ByVal CurrentListDocGroup As ListDocGroup)
        If CurrentListDocGroup.ListDocGroups IsNot Nothing Then
            For Each G In CurrentListDocGroup.ListDocGroups
                ' ProcessListDocGroup
                Dim StartRowIndex As Integer = CurrentRowIndex + 1
                ProcessListDocGroupWorksheet(Worksheet, G)
                Dim FinishRowIndex As Integer = CurrentRowIndex
                Worksheet.Cells.GroupRows(StartRowIndex, FinishRowIndex, Not G.GroupExpanded)
                ' Group
                CurrentRowIndex += 1
                CurrentColIndex = 0
                ' Fields
                For Each F As DataRowView In ViewEnabledFields
                    Dim FieldID = DirectCast(F("FieldID"), Guid)
                    Dim FieldInfo = DocType.GetFieldInfo(FieldID)
                    Dim ListDocFieldInfo = DocType.GetListDocFieldInfo(FieldID)
                    Dim Cell As Aspose.Cells.Cell = Worksheet.Cells(CurrentRowIndex, CurrentColIndex)
                    Dim FieldFormat = ListDocFieldInfo("FieldFormat").ToString
                    If FieldFormat Like "{0:d}" Then
                        FieldFormat = "dd-mmm-yy"
                    End If

                    If FieldFormat Like "{0:*}" Then
                        FieldFormat = Replace(FieldFormat, "}", "")
                        FieldFormat = Replace(FieldFormat, "{0:", "")
                    End If
                    If G.GroupColumnName = FieldID.ToString("n") Then
                        If ListDocFieldInfo("DocTypeURL").ToString <> "" AndAlso ListDocFieldInfo("ShowingFields").ToString <> "" Then
                            Dim FieldDocTypeURL = ListDocFieldInfo("DocTypeURL").ToString
                            Dim FieldShowingFields = ListDocFieldInfo("ShowingFields").ToString
                            Dim ShowingFieldsFormat = ListDocFieldInfo("ShowingFieldsFormat").ToString
                            Dim FieldDocType = AppSettings.GetDocType(FieldDocTypeURL)
                            Dim FieldShowingFieldsValues As New List(Of Object)
                            For Each FieldShowingField In FieldShowingFields.Split(New Char() {";", ","})
                                Dim FieldShowingFieldAlias = FieldID.ToString("n") & "_" & FieldDocType.GetHeaderTableName & "_" & FieldShowingField
                                FieldShowingFieldsValues.Add(G.FirstListDocRow(FieldShowingFieldAlias))
                            Next
                            If Not IsDBNull(G.GroupValue) AndAlso TypeOf (G.GroupValue) Is Guid Then
                                Dim ValueGuid = DirectCast(G.GroupValue, Guid)
                                If ValueGuid <> Guid.Empty Then
                                    PutValueToCell(Cell, String.Format(ShowingFieldsFormat, FieldShowingFieldsValues.ToArray) & " (" & G.GroupRowsCounter.ToString & ")")
                                    'Cell.Formula = String.Format("=HYPERLINK(""{0}"";""{1}"")", BasePage.GetDocURL(FieldDocTypeURL, ValueGuid, True), String.Format(ShowingFieldsFormat, FieldShowingFieldsValues.ToArray) & " (" & G.GroupRowsCounter.ToString & ")")
                                End If
                            End If
                        ElseIf ListDocFieldInfo("SourceTable").ToString <> "" AndAlso ListDocFieldInfo("KeyField").ToString <> "" AndAlso ListDocFieldInfo("DisplayField").ToString <> "" Then
                            Dim SourceTable = ListDocFieldInfo("SourceTable").ToString
                            Dim DisplayField = ListDocFieldInfo("DisplayField").ToString
                            Dim DisplayFieldAlias = FieldID.ToString("n") & "_" & SourceTable & "_" & DisplayField
                            If FieldFormat = "" Or IsDBNull(G.GroupValue) Then
                                PutValueToCell(Cell, G.FirstListDocRow(DisplayFieldAlias).ToString & " (" & G.GroupRowsCounter.ToString & ")")
                            Else
                                PutValueToCell(Cell, Format(G.FirstListDocRow(DisplayFieldAlias), FieldFormat) & " (" & G.GroupRowsCounter.ToString & ")")
                            End If
                        Else
                            If FieldFormat = "" Or IsDBNull(G.GroupValue) Then
                                PutValueToCell(Cell, G.GroupValue.ToString & " (" & G.GroupCounter.ToString & ")")
                            Else
                                PutValueToCell(Cell, Format(G.GroupValue, FieldFormat) & " (" & G.GroupCounter.ToString & ")")
                            End If
                        End If
                    Else
                        If G.AggregatesValues.ContainsKey(FieldID.ToString("n")) Then
                            Dim AggregateValue = G.AggregatesValues(FieldID.ToString("n"))
                            If AggregateValue IsNot Nothing AndAlso Not IsDBNull(AggregateValue) Then
                                If FieldFormat = "" Then
                                    PutValueToCell(Cell, eqView.GetAggregateTypeString(F("AggregateType")) & ": " & AggregateValue.ToString)
                                Else
                                    PutValueToCell(Cell, eqView.GetAggregateTypeString(F("AggregateType")) & ": " & Format(AggregateValue, FieldFormat))
                                End If
                            End If
                        End If
                    End If
                    CurrentColIndex += 1
                Next
            Next
        ElseIf CurrentListDocGroup.ListDocRows IsNot Nothing Then
            Dim W(ViewEnabledFields.Count - 1) As Long
            For Each R In CurrentListDocGroup.ListDocRows
                CurrentRowIndex += 1
                CurrentColIndex = 0
                '' OpenDoc
                'Dim OpenDocCell As Aspose.Cells.Cell = Worksheet.Cells(CurrentRowIndex, 0)
                'OpenDocCell.Formula = "=HYPERLINK(""" & BasePage.GetDocURL(DocTypeURL, DirectCast(R("ID"), Guid), True) & """;""Open"")"
                ' Fields
                For Each F As DataRowView In ViewEnabledFields
                    Dim FieldID = DirectCast(F("FieldID"), Guid)
                    Dim FieldInfo = DocType.GetFieldInfo(FieldID)
                    Dim ListDocFieldInfo = DocType.GetListDocFieldInfo(FieldID)
                    Dim Cell As Aspose.Cells.Cell = Worksheet.Cells(CurrentRowIndex, CurrentColIndex)
                    Dim Value = R(FieldID.ToString("n"))
                    Dim FieldFormat = ListDocFieldInfo("FieldFormat").ToString
                    If FieldFormat Like "{0:d}" Then
                        FieldFormat = "dd-mmm-yy"
                    End If

                    If FieldFormat Like "{0:*}" Then
                        FieldFormat = Replace(FieldFormat, "}", "")
                        FieldFormat = Replace(FieldFormat, "{0:", "")
                    End If
                    If ListDocFieldInfo("DocTypeURL").ToString <> "" AndAlso ListDocFieldInfo("ShowingFields").ToString <> "" Then
                        Dim FieldDocTypeURL = ListDocFieldInfo("DocTypeURL").ToString
                        Dim FieldShowingFields = ListDocFieldInfo("ShowingFields").ToString
                        Dim ShowingFieldsFormat = ListDocFieldInfo("ShowingFieldsFormat").ToString
                        Dim FieldDocType = AppSettings.GetDocType(FieldDocTypeURL)
                        Dim FieldShowingFieldsValues As New List(Of Object)
                        For Each FieldShowingField In FieldShowingFields.Split(New Char() {";", ","})
                            Dim FieldShowingFieldAlias = FieldID.ToString("n") & "_" & FieldDocType.GetHeaderTableName & "_" & FieldShowingField
                            FieldShowingFieldsValues.Add(R(FieldShowingFieldAlias))
                        Next
                        If Not IsDBNull(Value) AndAlso TypeOf (Value) Is Guid Then
                            Dim ValueGuid = DirectCast(Value, Guid)
                            If ValueGuid <> Guid.Empty Then
                                PutValueToCell(Cell, String.Format(ShowingFieldsFormat, FieldShowingFieldsValues.ToArray))
                                'Cell.Formula = String.Format("=HYPERLINK(""{0}"";""{1}"")", BasePage.GetDocURL(FieldDocTypeURL, ValueGuid, True), String.Format(ShowingFieldsFormat, FieldShowingFieldsValues.ToArray))
                            End If
                        End If
                    ElseIf ListDocFieldInfo("SourceTable").ToString <> "" AndAlso ListDocFieldInfo("KeyField").ToString <> "" AndAlso ListDocFieldInfo("DisplayField").ToString <> "" Then
                        Dim SourceTable = ListDocFieldInfo("SourceTable").ToString
                        Dim DisplayField = ListDocFieldInfo("DisplayField").ToString
                        Dim DisplayFieldAlias = FieldID.ToString("n") & "_" & SourceTable & "_" & DisplayField
                        If FieldFormat = "" Or IsDBNull(R(DisplayFieldAlias)) Then
                            PutValueToCell(Cell, R(DisplayFieldAlias), FieldFormat)
                        Else
                            'PutValueToCell(Cell, Format(R(DisplayFieldAlias), FieldFormat))
                            PutValueToCell(Cell, R(DisplayFieldAlias), FieldFormat)
                        End If
                    Else
                        If FieldFormat = "" Or IsDBNull(Value) Then
                            PutValueToCell(Cell, Value, FieldFormat)
                        Else
                            'PutValueToCell(Cell, Format(Value, FieldFormat))
                            PutValueToCell(Cell, Value, FieldFormat)
                        End If
                    End If
                    W(CurrentColIndex) += Len(Value.ToString)
                    CurrentColIndex += 1
                Next
            Next

            CurrentColIndex = 0
            If CurrentRowIndex > 0 Then
                For Each F As DataRowView In ViewEnabledFields
                    If Worksheet.Cells.GetColumnWidth(CurrentColIndex) = 26 Then
                        Dim AverWith As Double = W(CurrentColIndex) * 1.1 / CurrentRowIndex
                        Dim WWidth As Double = AverWith
                        If AverWith > 50 Then
                            WWidth = 50
                        End If
                        If AverWith < 12 Then
                            WWidth = 13
                        End If
                        Worksheet.Cells.SetColumnWidth(CurrentColIndex, WWidth)
                    End If
                    CurrentColIndex += 1
                Next
            End If
        End If
    End Sub

    Private Sub PutValueToCell(ByVal Cell As Aspose.Cells.Cell, ByVal Value As Object)
        If TypeOf Value Is Boolean Then
            Cell.PutValue(DirectCast(Value, Boolean))
        ElseIf TypeOf Value Is Integer Then
            Cell.PutValue(DirectCast(Value, Integer))
        ElseIf TypeOf Value Is Double Then
            Cell.PutValue(DirectCast(Value, Double))
        ElseIf TypeOf Value Is String Then
            Cell.PutValue(DirectCast(Value, String))
        ElseIf TypeOf Value Is DateTime Then
            Cell.PutValue(DirectCast(Value, DateTime).ToString)
        ElseIf Not IsDBNull(Value) AndAlso Value IsNot Nothing Then
            Cell.PutValue(Value)
        End If
    End Sub

    Private Sub PutValueToCell(ByVal Cell As Aspose.Cells.Cell, ByVal Value As Object, ByVal NumericFormat As String)

        If NumericFormat Like "{0:d}" Then
            NumericFormat = "dd-mmm-yy"
        End If

        If NumericFormat Like "{0:*}" Then
            NumericFormat = Replace(NumericFormat, "}", "")
            NumericFormat = Replace(NumericFormat, "{0:", "")
        End If

        If TypeOf Value Is Boolean Then
            If NumericFormat = "" And Not IsDBNull(Value) Then
                If DirectCast(Value, Boolean) = True Then
                    Cell.PutValue("Да")
                Else
                    Cell.PutValue("Нет")
                End If
            Else
                Cell.PutValue(DirectCast(Value, Boolean))
            End If
        ElseIf TypeOf Value Is Integer Then
            Cell.PutValue(DirectCast(Value, Integer))
        ElseIf TypeOf Value Is Double Then
            Cell.PutValue(DirectCast(Value, Double))
        ElseIf TypeOf Value Is String Then
            Cell.PutValue(DirectCast(Value, String))
        ElseIf TypeOf Value Is DateTime Then
            Cell.PutValue(DirectCast(Value, DateTime).ToString)
        ElseIf Not IsDBNull(Value) AndAlso Value IsNot Nothing Then
            Cell.PutValue(Value)
        End If

        If NumericFormat <> "" Then
            Cell.Style.Custom = NumericFormat
        End If
        Cell.Style.IsTextWrapped = True
    End Sub

#End Region

End Class
