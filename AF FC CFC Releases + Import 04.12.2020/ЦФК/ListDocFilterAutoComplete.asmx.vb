﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
<System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class ListDocFilterAutoComplete
    Inherits System.Web.Services.WebService

    Private ReadOnly Property AppSettings() As eqAppSettings
        Get
            Return Session("AppSettings")
        End Get
    End Property

    <WebMethod(True)> _
    Public Function GetAutoComplete(ByVal prefixText As String, ByVal count As Integer, ByVal contextKey As String) As String()
        Try
            Dim FieldID As New Guid(contextKey)
            ' cmdGetFieldInfo
            Dim cmdGetFieldInfo As New SqlClient.SqlCommand("SELECT * FROM eqDocStructure WHERE ID=@ID")
            cmdGetFieldInfo.Parameters.AddWithValue("@ID", FieldID)
            Dim FieldInfo = AppSettings.GetDataTable(cmdGetFieldInfo).Rows(0)
            Dim DocType = AppSettings.GetDocType(DirectCast(FieldInfo("DocTypeID"), Guid))
            Dim TableName As String = FieldInfo("TableName")
            Dim FieldName As String = FieldInfo("FieldName")
            Dim ShemaDataRow = DocType.GetShemaDataRow(TableName, FieldName)
            ' cmdAutoComplete
            Dim cmdAutoCompleteSB As New Text.StringBuilder
            If IsDBNull(ShemaDataRow("FullText")) Then
                cmdAutoCompleteSB.AppendLine("SELECT DISTINCT TOP 20 " & FieldName & " FROM " & TableName & " WHERE CAST(" & FieldName & " AS nvarchar(max)) LIKE @" & FieldName)
                Dim cmdAutoComplete As New SqlClient.SqlCommand(cmdAutoCompleteSB.ToString)
                cmdAutoComplete.CommandTimeout = 2
                cmdAutoComplete.Parameters.AddWithValue("@" & FieldName, prefixText & "%")
                Try
                    Dim dtAutoComplete = AppSettings.GetDataTable(cmdAutoComplete)
                    Dim retList As New SortedList(Of String, String)
                    For Each R As DataRow In dtAutoComplete.Rows
                        Dim vMatch = Regex.Match(R(0).ToString, prefixText & "\w*", RegexOptions.IgnoreCase)
                        If vMatch.Success Then
                            If Not retList.ContainsKey(vMatch.Groups(0).Value.ToUpper) Then
                                retList.Add(vMatch.Groups(0).Value.ToUpper, vMatch.Groups(0).Value)
                            End If
                        End If
                    Next
                    Return retList.Values.ToArray
                Catch ex As Exception
                    Return New String() {prefixText}
                End Try
            Else
                cmdAutoCompleteSB.AppendLine("SELECT DISTINCT TOP 20 " & FieldName & " FROM " & TableName & " WHERE CONTAINS(" & FieldName & ",@" & FieldName & ")")
                Dim cmdAutoComplete As New SqlClient.SqlCommand(cmdAutoCompleteSB.ToString)
                cmdAutoComplete.CommandTimeout = 2
                cmdAutoComplete.Parameters.AddWithValue("@" & FieldName, """" & prefixText & "*""")
                Try
                    Dim dtAutoComplete = AppSettings.GetDataTable(cmdAutoComplete)
                    Dim retList As New SortedList(Of String, String)
                    For Each R As DataRow In dtAutoComplete.Rows
                        Dim vMatch = Regex.Match(R(0).ToString, prefixText & "\w*", RegexOptions.IgnoreCase)
                        If vMatch.Success Then
                            If Not retList.ContainsKey(vMatch.Groups(0).Value.ToUpper) Then
                                retList.Add(vMatch.Groups(0).Value.ToUpper, vMatch.Groups(0).Value)
                            End If
                        End If
                    Next
                    Return retList.Values.ToArray
                Catch ex As Exception
                    Return New String() {prefixText}
                End Try
            End If
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

End Class