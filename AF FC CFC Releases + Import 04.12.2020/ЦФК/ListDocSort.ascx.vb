﻿Public Partial Class ListDocSort
    Inherits eqBaseUserControl

    Public Event OnSortAsc(ByVal sender As Object, ByVal e As EventArgs)
    Public Event OnSortDesc(ByVal sender As Object, ByVal e As EventArgs)

    Public Property FieldID() As Guid
        Get
            Return ViewState("FieldID")
        End Get
        Set(ByVal value As Guid)
            ViewState("FieldID") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        SortAsc.ImageUrl = BasePage.GetThemeRelativeURL("\Images\SortAsc.gif")
        SortDesc.ImageUrl = BasePage.GetThemeRelativeURL("\Images\SortDesc.gif")
    End Sub

    Private Sub SortAsc_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles SortAsc.Click
        RaiseEvent OnSortAsc(Me, EventArgs.Empty)
    End Sub

    Private Sub SortDesc_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles SortDesc.Click
        RaiseEvent OnSortDesc(Me, EventArgs.Empty)
    End Sub

End Class