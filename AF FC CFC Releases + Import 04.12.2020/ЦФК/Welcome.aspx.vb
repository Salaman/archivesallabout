﻿Imports System
Imports System.Data
Imports System.Data.SqlClient

Partial Public Class Welcome
    Inherits eqBasePage

#Region "Override"

    Protected Overrides Sub ShowMessages(ByVal Messages As System.Collections.Generic.List(Of String))
        PlaceHolderMessages.Controls.Clear()
        For Each Message As String In Messages
            Dim MessagePanel As New Panel With {.CssClass = "MenuSubItemStyle"}
            Dim MessageLabel As New Label With {.Text = Message}
            MessagePanel.Controls.Add(MessageLabel)
            PlaceHolderMessages.Controls.Add(MessagePanel)
        Next
    End Sub

    Protected Overrides Sub OnPreLoad(ByVal e As System.EventArgs)
        ' Ignore basepage
    End Sub

#End Region

#Region "Methods"

    Private Sub FillDatabases()
        Dim cnnsb As New SqlConnectionStringBuilder(My.Settings.DatabaseConnectionString)

        Using cnn As New SqlConnection(cnnsb.ConnectionString)
            Try
                cnn.Open()
            Catch ex As SqlException
                Throw New Exception(String.Format(GetGlobalResourceObject("eqResources", "MSG_DBConnectionFailed"), ex.Message))
            End Try

            Dim cmd As New SqlCommand("SELECT * FROM vDatabases WHERE Deleted = 0 ORDER BY [Name]", cnn)
            Dim ad As New SqlDataAdapter(cmd)
            Dim dt As New DataTable

            ad.Fill(dt)
            Databases.DataSource = dt
            Databases.DataTextField = "Name"
            Databases.DataValueField = "Connection"
            Databases.DataBind()

            cnn.Close()
        End Using
    End Sub

#End Region

#Region "Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Session.Remove("ActiveDatabaseConnectionString")
            FillDatabases()
        End If

        Dim HomeLink = DirectCast(Master.FindControl("HyperLinkLogo"), HyperLink)
        HomeLink.NavigateUrl = "#"
    End Sub

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Not Web.UI.ScriptManager.GetCurrent(Page).IsInAsyncPostBack Then
            ' Высота для XHTML 1.0 Transitional
            Dim SetPanelLoginPaddingTopSB As New StringBuilder
            SetPanelLoginPaddingTopSB.AppendLine("function SetPanelWelcomePaddingTop()")
            SetPanelLoginPaddingTopSB.AppendLine("{")
            SetPanelLoginPaddingTopSB.AppendLine(" document.getElementById('" & PanelWelcomePadding.ClientID & "').style.height = Math.max(0,GetWindowHeight()/2-120)+'px';")
            SetPanelLoginPaddingTopSB.AppendLine("}")
            SetPanelLoginPaddingTopSB.AppendLine("SetPanelWelcomePaddingTop();")
            SetPanelLoginPaddingTopSB.AppendLine("$addHandler(window,'resize',SetPanelWelcomePaddingTop);")
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "SetPanelWelcomePaddingTop", SetPanelLoginPaddingTopSB.ToString, True)
        End If
    End Sub

    Private Sub ButtonEnter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonEnter.Click
        Session("ActiveDatabaseConnectionString") = Databases.SelectedValue
        MyBase.RedirectToDefaultPage()
    End Sub

#End Region

End Class