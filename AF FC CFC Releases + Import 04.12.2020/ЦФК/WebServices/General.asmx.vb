﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
<System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class General
    Inherits System.Web.Services.WebService

    Private ReadOnly Property AppSettings() As eqAppSettings
        Get
            Try
                Return Session("AppSettings")
            Catch ex As Exception
                Throw New Exception("Not Authenticated!")
            End Try
        End Get
    End Property

#Region "Authentication"

    <WebMethod(True)> _
    Public Function Authenticated() As Boolean
        Return Session("AppSettings") IsNot Nothing
    End Function

    <WebMethod(True)> _
    Public Sub AuthenticateOut()
        Session("AppSettings") = Nothing
    End Sub

    <WebMethod(True)> _
    Public Function AuthenticateForms(ByVal Login As String, ByVal Pass As String) As Result
        Try

            Dim SqlConnectionStringBuilder As New SqlClient.SqlConnectionStringBuilder(My.Settings.DatabaseConnectionString)
            Dim UserSqlConnection As New SqlClient.SqlConnection(SqlConnectionStringBuilder.ConnectionString)
            Try
                UserSqlConnection.Open()
            Catch ex As SqlClient.SqlException
                Throw New Exception("Невозможно установить соединение с базой данных -> " & ex.Message)
            End Try

            Dim ValidateUserSqlCommand As New SqlClient.SqlCommand("SELECT TOP 1 ID FROM eqUsers WHERE Login=@Login AND Pass=@Pass AND Deleted=0", UserSqlConnection)
            ValidateUserSqlCommand.Parameters.AddWithValue("@Login", Login)
            ValidateUserSqlCommand.Parameters.AddWithValue("@Pass", Pass)
            Dim UserID As Object = ValidateUserSqlCommand.ExecuteScalar
            Dim AuthSuccess As Boolean = True
            If IsDBNull(UserID) Or UserID Is Nothing Then
                Throw New Exception("Неправильное имя пользователя или пароль")
                AuthSuccess = False
            End If

            eqAppSettings.WriteActitityLog(UserSqlConnection, Login, Pass, AuthSuccess, "SilverLight", "SilverLight", "SilverLight")

            Dim UserAppSettings As New eqAppSettings
            UserAppSettings.OpenSession(UserSqlConnection, Login)
            UserAppSettings.Manager = My.Resources.ResourceManager
            UserAppSettings.AdministratorUserLogin = My.Settings.AdministratorUserLogin
            Session("AppSettings") = UserAppSettings

            Return New Result With {.Succefull = True}

        Catch ex As Exception
            Return New Result With {.Message = ex.Message}
        End Try
    End Function

#End Region

#Region "Categories"

    Public Class Category
        Public ID As Guid
        Public Name As String
        Public ToolTip As String
        Public SortOrder As Integer
        Sub New()

        End Sub
        Friend Sub New(ByVal cat As eqCategory)
            ID = cat.ID
            Name = cat.CategoryName
            ToolTip = cat.ToolTip
            SortOrder = cat.SortOrder
        End Sub
    End Class

    Public Class CategoriesResult
        Inherits Result
        Public Categories As Category()
    End Class

    <WebMethod(True)> _
    Public Function Categories() As CategoriesResult
        Try
            Dim res As New CategoriesResult With {.Succefull = True}
            res.Categories = AppSettings.Categories.Select(Function(C) New Category(C)).ToArray
            Return res
        Catch ex As Exception
            Return New CategoriesResult With {.Message = ex.Message}
        End Try
    End Function

#End Region

#Region "DocTypes"

    Public Class DocType
        Public ID As Guid
        Public Name As String
        Public Url As String
        Public UrlTarget As String
        Public IsManaged As Boolean
        Public OpenMode As String
        Public CategoryID As Guid
        Sub New()

        End Sub
        Friend Sub New(ByVal dt As eqDocType)
            ID = dt.DocTypeID
            Name = dt.DocTypeName
            Url = dt.URL
            UrlTarget = dt.URLTarget
            IsManaged = dt.IsManaged
            OpenMode = dt.OpenMode
            CategoryID = dt.Category.ID
        End Sub
    End Class

    Public Class DocTypesResult
        Inherits Result
        Public DocTypes As DocType()
    End Class

    Public Function DocTypes() As DocTypesResult
        Try
            Dim res As New DocTypesResult With {.Succefull = True}
            res.DocTypes = AppSettings.DocTypes.Select(Function(DT) New DocType(DT.Value)).ToArray
            Return res
        Catch ex As Exception
            Return New DocTypesResult With {.Message = ex.Message}
        End Try
    End Function

#End Region

#Region "WebService"

    Public Class Result
        Public Succefull As Boolean
        Public Message As String
    End Class

#End Region

End Class