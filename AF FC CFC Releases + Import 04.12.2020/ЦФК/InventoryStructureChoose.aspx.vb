﻿Imports System.Data.SqlClient

Partial Public Class InventoryStructureChoose
    Inherits BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Private Sub ButtonRefuse_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonRefuse.Click
        CloseWindow()
    End Sub

    Private Sub ButtonClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonClear.Click
        Dim selectedToDisplace = DirectCast(ModalDialogArgument.ArgumentObject, List(Of Guid))
        Try
            AppSettings.BeginTransaction()
            Dim cmdStr As String = <sql>
UPDATE tblUNIT
SET ISN_INVENTORY_CLS = NULL
WHERE ID = @ID
                                   </sql>
            For Each id As Guid In selectedToDisplace
                Dim cmd As New SqlCommand(cmdStr)
                cmd.Parameters.AddWithValue("@ID", id)
                cmd.Connection = AppSettings.Cnn
                cmd.Transaction = AppSettings.Trn
                cmd.ExecuteNonQuery()
            Next
            AppSettings.CommitTransaction()
            CloseWindow()
        Catch ex As Exception
            AppSettings.RollbackTransaction()
        End Try

    End Sub

    Private Sub ListDocInventoryStructure_SelectDoc(ByVal DocID As System.Guid) Handles ListDocInventoryStructure.SelectDoc
        Dim selectedToDisplace = DirectCast(ModalDialogArgument.ArgumentObject, List(Of Guid))

        Try
            AppSettings.BeginTransaction()
            For Each unitId In selectedToDisplace
                setInventroyStructureQuery(unitId, DocID)
            Next
            AppSettings.CommitTransaction()
            AddMessage("Объекты учета прикреплены к разделу описи")
            CloseWindow()
        Catch ex As Exception
            AppSettings.RollbackTransaction()
            AddMessage(ex.Message)
        End Try

    End Sub


    Private ReadOnly setInventroyStructureQueryString As String = <sql>
            UPDATE tblUNIT
            SET ISN_INVENTORY_CLS = (
                SELECT ISN_INVENTORY_CLS
                FROM tblINVENTORY_STRUCTURE
                WHERE ID = @ISN_INVENTORY_ID
            )
            WHERE ID = @ID
        </sql>.Value 'команда для смены раздела описи у юнита по его id

    Private Function setInventroyStructureQuery(ByVal UnitID As System.Guid, ByVal InventoryStructureID As System.Guid) As Int32
        Dim cmd As New SqlCommand(setInventroyStructureQueryString)
        cmd.Parameters.AddWithValue("@ID", UnitID)
        cmd.Parameters.AddWithValue("@ISN_INVENTORY_ID", InventoryStructureID)
        cmd.Connection = AppSettings.Cnn
        cmd.Transaction = AppSettings.Trn
        Return cmd.ExecuteNonQuery()
    End Function




End Class