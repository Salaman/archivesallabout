﻿Imports System.Web
Imports System.Web.Services

Public Class ImageHandler
    Implements System.Web.IHttpHandler


    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Try
            Dim FilePath = context.Server.UrlDecode(HttpContext.Current.Request.QueryString("FilePath"))

            context.Response.ContentType = "image/jpeg"
            context.Response.WriteFile(FilePath)
        Catch ex As Exception

        End Try
    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class