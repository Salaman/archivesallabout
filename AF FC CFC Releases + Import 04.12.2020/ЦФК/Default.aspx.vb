﻿Partial Public Class _Default
    Inherits BasePage

    Public Enum NavigateModes As Byte
        Redirect = 0
        PostBack = 1
    End Enum

    Public Const NavigateMode As NavigateModes = NavigateModes.Redirect
    Public Const ExtendedMenuMode As Boolean = False

    Public Overrides Property CategoryID() As System.Guid
        Get
            Return MyBase.CategoryID
        End Get
        Set(ByVal value As System.Guid)
            MyBase.DocTypeURL = Nothing
            MyBase.CategoryID = value
            If _ContainerLoaded Then
                LoadContainer()
            End If
            UpdatePanelMenuSystem.Update()
            UpdatePanelPlaceHolderContainer.Update()
        End Set
    End Property

    Public Overrides Property DocTypeURL() As String
        Get
            Return MyBase.DocTypeURL
        End Get
        Set(ByVal value As String)
            MyBase.CategoryID = Nothing
            MyBase.DocTypeURL = value
            If _ContainerLoaded Then
                LoadContainer()
            End If
            UpdatePanelMenuSystem.Update()
            UpdatePanelPlaceHolderContainer.Update()
        End Set
    End Property

#Region "Page Events"

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'EnsureChildControls()
        LoadContainer()
        _ContainerLoaded = True
        ' Переводим то, что зашито в тему
        HyperLinkUser.ToolTip = GetGlobalResourceObject("eqResources", "TIP_User")
        ' Проверяем бэкап
        If Not IsPostBack Then
            If Cache("CheckBackup") Is Nothing Then
                Try
                    CheckBackup(AppSettings)
                    Cache.Insert("CheckBackup", Now, Nothing, Cache.NoAbsoluteExpiration, TimeSpan.FromHours(1))
                Catch ex As Exception
                    LabelCheckBackup.Text = ex.Message
                    LinkCheckButton.NavigateUrl = GetNewDocURL("Service.ascx", False)
                    PanelCheckBackup.Visible = True
                End Try
            End If
        End If
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If IsModal Then
            Master.HeaderPanel.Visible = False
            TableCellSystemMenu.Visible = False
        End If
        ShowFakeUserBlock()
        ' ModalDialogs
        If Not ScriptManager.GetCurrent(Page).IsInAsyncPostBack Then
            Dim modalDialogsScript = "function openModalDialog(Url,Width,Height){" & _
                    " if($find('" & ModalPopupExtenderDialog.ClientID & "')){" & _
                    "  Width=Width!=0?Width:GetWindowWidth()-50;" & _
                    "  Height=Height!=0?Height:GetWindowHeight()-50;" & _
                    "  document.getElementById('" & PanelDialog.ClientID & "').style.width=Math.max(Width,600)+'px';" & _
                    "  document.getElementById('" & PanelDialogContent.ClientID & "').style.height=Math.max(Height,400)+'px';" & _
                    "  document.getElementById('" & PanelDialogContent.ClientID & "').innerHTML='<iframe width=""100%"" height=""100%"" marginwidth=""0"" marginheight=""0"" frameborder=""0"" scrolling=""auto"" src=""'+Url+'""/>';" & _
                    "  $find('" & ModalPopupExtenderDialog.ClientID & "').show();" & _
                    " }else{" & vbCrLf & _
                    "  Sys.Application.add_load(function(){openModalDialog(Url,Width,Height);});" & vbCrLf & _
                    " }" & vbCrLf & _
                    "}" & vbCrLf & _
                    "function closeModalDialog(Result){" & _
                    "$find('" & ModalPopupExtenderDialog.ClientID & "').hide();setTimeout(Result,0);" & _
                    "};"
            ' "Sys.Application.add_load(function(){" & Script & "});"
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "modalDialogs", modalDialogsScript, True)
        End If
    End Sub

#End Region

#Region "LoadContainer"

    Private _ContainerLoaded As Boolean = False
    Private Sub LoadContainer()
        PlaceHolderContainer.Controls.Clear()
        If DocTypeURL IsNot Nothing Then
            Try
                Dim Container As eqBaseContainer = LoadControl(ContainerControlUrl)
                AddHandler Container.NewDoc, AddressOf Container_OnNewDoc
                AddHandler Container.ListDoc, AddressOf Container_OnListDoc
                AddHandler Container.LoadDoc, AddressOf Container_OnLoadDoc
                AddHandler Container.SelectDoc, AddressOf Container_OnSelectDoc
                Container.ID = "Container"
                Container.DocTypeURL = DocTypeURL
                Container.ContainerMode = IIf(IsListDoc, eqBaseContainer.ContainerModes.ListDoc, eqBaseContainer.ContainerModes.Doc)
                If IsModalDialog Then
                    Container.ListDocMode = eqBaseContainer.ListDocModes.Self
                    Container.NewDocMode = eqBaseContainer.NewDocModes.Self
                    Container.LoadDocMode = eqBaseContainer.LoadDocModes.Self
                    Container.DeleteDocMode = eqBaseContainer.DeleteDocModes.None
                    Container.SelectDocMode = eqBaseContainer.SelectDocModes.Single
                    Container.DocCommandMode = eqBaseContainer.DocCommandModes.Single
                Else
                    Container.ListDocMode = eqBaseContainer.ListDocModes.Self
                    Container.NewDocMode = eqBaseContainer.NewDocModes.Self
                    Container.LoadDocMode = eqBaseContainer.LoadDocModes.Self
                    Container.DeleteDocMode = eqBaseContainer.DeleteDocModes.Single
                    Container.SelectDocMode = eqBaseContainer.SelectDocModes.None
                    Container.DocCommandMode = eqBaseContainer.DocCommandModes.SingleAndMulti
                    Container.ShowSelectBox = False
                End If
                PlaceHolderContainer.Controls.Add(Container)
                If Not IsPostBack Then
                    Container.DocID = DocID
                End If
            Catch ex As Exception
                AddMessage("Ошибка загрузки контрола контейнера -> " & ex.Message)
            End Try
        ElseIf CategoryID <> Guid.Empty Then
            Try
                Dim Category = LoadControl("~/DefaultCategory.ascx")
                Category.ID = "DefaultCategory"
                PlaceHolderContainer.Controls.Add(Category)
            Catch ex As Exception
                AddMessage("Ошибка загрузки страницы разделы -> " & ex.Message)
            End Try
        Else
            Try
                Dim Main = LoadControl("~/DefaultMain.ascx")
                Main.ID = "DefaultMain"
                PlaceHolderContainer.Controls.Add(Main)
            Catch ex As Exception
                AddMessage("Ошибка загрузки главной страницы -> " & ex.Message)
            End Try
        End If
        LoadSystemMenu()
        LoadUserInfo()
    End Sub

    'Protected Overrides Sub CreateChildControls()
    '    PlaceHolderContainer.Controls.Clear()
    '    If DocTypeURL IsNot Nothing Then
    '        Try
    '            Dim Container As eqBaseContainer = LoadControl(ContainerControlUrl)
    '            AddHandler Container.NewDoc, AddressOf Container_OnNewDoc
    '            AddHandler Container.ListDoc, AddressOf Container_OnListDoc
    '            AddHandler Container.LoadDoc, AddressOf Container_OnLoadDoc
    '            AddHandler Container.SelectDoc, AddressOf Container_OnSelectDoc
    '            Container.ID = "Container"
    '            Container.DocTypeURL = DocTypeURL
    '            Container.ContainerMode = IIf(IsListDoc, eqBaseContainer.ContainerModes.ListDoc, eqBaseContainer.ContainerModes.Doc)
    '            If IsModalDialog Then
    '                Container.ListDocMode = eqBaseContainer.ListDocModes.Self
    '                Container.NewDocMode = eqBaseContainer.NewDocModes.Self
    '                Container.LoadDocMode = eqBaseContainer.LoadDocModes.Self
    '                Container.DeleteDocMode = eqBaseContainer.DeleteDocModes.None
    '                Container.SelectDocMode = eqBaseContainer.SelectDocModes.Single
    '                Container.DocCommandMode = eqBaseContainer.DocCommandModes.Single
    '            Else
    '                Container.ListDocMode = eqBaseContainer.ListDocModes.Self
    '                Container.NewDocMode = eqBaseContainer.NewDocModes.Self
    '                Container.LoadDocMode = eqBaseContainer.LoadDocModes.Self
    '                Container.DeleteDocMode = eqBaseContainer.DeleteDocModes.Single
    '                Container.SelectDocMode = eqBaseContainer.SelectDocModes.None
    '                Container.DocCommandMode = eqBaseContainer.DocCommandModes.SingleAndMulti
    '            End If
    '            PlaceHolderContainer.Controls.Add(Container)
    '            If Not IsPostBack Then
    '                Container.DocID = DocID
    '            End If
    '        Catch ex As Exception
    '            AddMessage("Ошибка загрузки контрола контейнера -> " & ex.Message)
    '        End Try
    '    ElseIf CategoryID <> Guid.Empty Then
    '        Try
    '            Dim Category = LoadControl("~/DefaultCategory.ascx")
    '            Category.ID = "DefaultCategory"
    '            PlaceHolderContainer.Controls.Add(Category)
    '        Catch ex As Exception
    '            AddMessage("Ошибка загрузки страницы категории -> " & ex.Message)
    '        End Try
    '    Else
    '        Try
    '            Dim Main = LoadControl("~/DefaultMain.ascx")
    '            Main.ID = "DefaultMain"
    '            PlaceHolderContainer.Controls.Add(Main)
    '        Catch ex As Exception
    '            AddMessage("Ошибка загрузки главной страницы -> " & ex.Message)
    '        End Try
    '    End If
    '    LoadSystemMenu()
    '    LoadUserInfo()
    'End Sub

#End Region

#Region "Container Events"

    Private Sub Container_OnNewDoc(ByVal sender As Object, ByVal e As eqBaseContainer.NewDocEventArgs)
        If NavigateMode = NavigateModes.Redirect Then
            Dim OptionalQueryString As String = String.Empty
            If IsModalDialog Then
                OptionalQueryString = ConstModalDialog & "=" & ModalDialogID & "&" & ConstModalDialogSource & "=" & ModalDialogSource
            Else
                OptionalQueryString = String.Empty
            End If
            Response.Redirect(GetNewDocURL(DocTypeURL, IsModal, OptionalQueryString))
            e.Suppress = True
        End If
    End Sub

    Private Sub Container_OnListDoc(ByVal sender As Object, ByVal e As eqBaseContainer.ListDocEventArgs)
        If NavigateMode = NavigateModes.Redirect Then
            Dim OptionalQueryString As String = String.Empty
            If IsModalDialog Then
                OptionalQueryString = ConstModalDialog & "=" & ModalDialogID & "&" & ConstModalDialogSource & "=" & ModalDialogSource
            Else
                OptionalQueryString = String.Empty
            End If
            Response.Redirect(GetListDocURL(DocTypeURL, IsModal, OptionalQueryString))
            e.Suppress = True
        End If
    End Sub

    Private Sub Container_OnLoadDoc(ByVal sender As Object, ByVal e As eqBaseContainer.LoadDocEventArgs)
        If NavigateMode = NavigateModes.Redirect Then
            Dim OptionalQueryString As String = String.Empty
            If IsModalDialog Then
                OptionalQueryString = ConstModalDialog & "=" & ModalDialogID & "&" & ConstModalDialogSource & "=" & ModalDialogSource
            Else
                OptionalQueryString = String.Empty
            End If
            Response.Redirect(GetDocURL(DocTypeURL, e.DocID, IsModal, OptionalQueryString))
            e.Suppress = True
        End If
    End Sub

    Private Sub Container_OnSelectDoc(ByVal sender As Object, ByVal e As eqBaseContainer.SelectDocEventArgs)
        If IsModalDialog AndAlso ModalDialogArgument.Argument = "SelectDoc" Then
            CloseModalDialogWindow("SelectDoc", e.DocID)
            e.Suppress = True
        End If
    End Sub

#End Region

#Region "Events"

    Protected Sub MenuSystem_MenuItemClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MenuEventArgs) Handles MenuSystem.MenuItemClick
        Dim Values = e.Item.Value.Split(New Char() {"|"})
        Select Case Values(0)
            Case "Main"
                ' Поменьше действий и в начальное состояние
                MyBase.DocTypeURL = Nothing
                MyBase.CategoryID = Nothing
                If _ContainerLoaded Then
                    LoadContainer()
                End If
                UpdatePanelMenuSystem.Update()
                UpdatePanelPlaceHolderContainer.Update()
            Case "Exit"
                ' Чистим куки
                'Response.Cookies("eqLogin").Expires = Now
                Response.Cookies(My.Settings.CookieName).Expires = Now
                ' Убиваем все, что есть в сессии
                Session.Abandon()
                ' Уходим на страницу 
                'Response.Redirect(GetAppRelativeURL(LoginPageUrl) & "?Reason=Exit")
                RedirectToLoginPage()
            Case "Category"
                CategoryID = New Guid(Values(1))
            Case "DocType"
                IsListDoc = Values(3) = "L"
                DocTypeURL = Values(1)
        End Select
    End Sub

    Private Sub Button_Command(ByVal sender As Object, ByVal e As CommandEventArgs)
        Dim Values = e.CommandName.Split(New Char() {"|"})
        Select Case Values(0)
            Case "DocType"
                IsListDoc = Values(3) = "L"
                DocTypeURL = Values(1)
            Case "Category"
                CategoryID = New Guid(Values(1))
        End Select
    End Sub

#End Region

#Region "Metods"

    Public Sub UpdateSystemMenu()
        LoadSystemMenu()
        UpdatePanelMenuSystem.Update()
    End Sub

    Public Sub LoadSystemMenu()
        ' Удаляем всё кроме
        MenuSystem.Items.Clear()
        ' Добавляем Home
        Dim MainMenuItem As New MenuItem(GetGlobalResourceObject("eqResources", "MNU_MenuMain"))
        MainMenuItem.Value = "Main"
        MainMenuItem.ImageUrl = GetThemeRelativeURL("Images/Home.gif")
        MainMenuItem.ToolTip = GetGlobalResourceObject("eqResources", "TIP_MenuMain")
        If NavigateMode = NavigateModes.Redirect Then
            MainMenuItem.NavigateUrl = GetAppRelativeURL(DefaultPageUrl)
        End If
        MainMenuItem.Selected = True
        MenuSystem.Items.Add(MainMenuItem)
        ' Добавляем всё остальное
        Dim DocTypes = From DT As eqDocType In AppSettings.DocTypes.Values _
                         Where AppSettings.UserSecurities.HasDocumentsAccessMenu(DT.DocTypeID) _
                         Order By DT.Category.SortOrder, DT.SortOrder
        For Each DocType In DocTypes
            Dim Categories = DocType.Category.CategoryName
            If Not String.IsNullOrEmpty(DocType.Category2) Then
                Categories += "/" & DocType.Category2.Trim.Trim("/"c)
            End If
            Dim CategoriesArray = Categories.Split(New Char() {"/"c}, StringSplitOptions.RemoveEmptyEntries)
            Dim CategoryItem As MenuItem = Nothing
            For Each Category In CategoriesArray
                Dim FindedCategoryItem As MenuItem = Nothing
                If CategoryItem Is Nothing Then
                    FindedCategoryItem = MenuSystem.Items.OfType(Of MenuItem).FirstOrDefault(Function(F) F.Text = Category)
                Else
                    FindedCategoryItem = CategoryItem.ChildItems.OfType(Of MenuItem).FirstOrDefault(Function(F) F.Text = Category)
                End If
                If FindedCategoryItem IsNot Nothing Then
                    ' Категория уже есть
                    CategoryItem = FindedCategoryItem
                Else
                    ' Создаем элемент
                    Dim NewCategoryItem As New MenuItem(Category)
                    NewCategoryItem.ImageUrl = GetThemeRelativeURL("Images/Folders.gif")
                    NewCategoryItem.ToolTip = Category
                    If CategoryItem Is Nothing Then
                        MenuSystem.Items.Add(NewCategoryItem)
                    Else
                        CategoryItem.ChildItems.Add(NewCategoryItem)
                    End If
                    CategoryItem = NewCategoryItem
                End If
            Next
            ' Создаем элемент
            Dim DocTypeItem As New MenuItem(DocType.DocTypeName, "DocType|" & DocType.URL & "|OpenMode|" & DocType.OpenMode)
            If DocType.ImageURL <> "" Then
                DocTypeItem.ImageUrl = GetThemeRelativeURL(DocType.ImageURL)
            Else
                If DocType.OpenMode = "L" Then
                    DocTypeItem.ImageUrl = GetThemeRelativeURL("Images/Folder.gif")
                Else
                    DocTypeItem.ImageUrl = GetThemeRelativeURL("Images/New.gif")
                End If
            End If
            If DocType.IsManaged Then
                If NavigateMode = NavigateModes.Redirect Then
                    If DocType.OpenMode = "L" Then
                        ' List mode
                        DocTypeItem.NavigateUrl = GetListDocURL(DocType.URL, False)
                    Else
                        ' Document mode
                        DocTypeItem.NavigateUrl = GetNewDocURL(DocType.URL, False)
                    End If
                End If
            Else
                DocTypeItem.NavigateUrl = DocType.URL
            End If
            DocTypeItem.Target = DocType.URLTarget
            DocTypeItem.ToolTip = DocType.ToolTip
            If DocTypeURL = DocType.URL Then DocTypeItem.Selected = True
            ' Добавляем
            CategoryItem.ChildItems.Add(DocTypeItem)
            ' ExtendedMenuMode
            If DocType.IsManaged AndAlso ExtendedMenuMode Then
                Select Case DocType.OpenMode
                    Case "D"
                        If AppSettings.UserSecurities.HasDocumentsAccessList(DocType.DocTypeID) Then
                            Dim cmdListDoc = DocType.ToolBarRows.FirstOrDefault(Function(TBR) TBR(eqDocType.FLD_TOOLBAR_COMMANDNAME) = "ListDoc")
                            If cmdListDoc IsNot Nothing Then
                                Dim DocTypeItemListDoc As New MenuItem(GetGlobalResourceObject("eqResources", "MNU_MenuList"), "DocType|" & DocType.URL & "|OpenMode|L")
                                DocTypeItemListDoc.ImageUrl = GetThemeRelativeURL("Images/Folder.gif")
                                If NavigateMode = NavigateModes.Redirect Then
                                    DocTypeItemListDoc.NavigateUrl = GetListDocURL(DocType.URL, False)
                                End If
                                DocTypeItem.ChildItems.Add(DocTypeItemListDoc)
                            End If
                        End If
                    Case "L"
                        If AppSettings.UserSecurities.HasDocumentsAccessNew(DocType.DocTypeID) Then
                            Dim cmdNewDoc = DocType.ToolBarRows.FirstOrDefault(Function(TBR) TBR(eqDocType.FLD_TOOLBAR_COMMANDNAME) = "NewDoc")
                            If cmdNewDoc IsNot Nothing Then
                                Dim DocTypeItemNewDoc As New MenuItem(GetGlobalResourceObject("eqResources", "MNU_MenuNew"), "DocType|" & DocType.URL & "|OpenMode|D")
                                DocTypeItemNewDoc.ImageUrl = GetThemeRelativeURL("Images/New.gif")
                                If NavigateMode = NavigateModes.Redirect Then
                                    DocTypeItemNewDoc.NavigateUrl = GetNewDocURL(DocType.URL, False)
                                End If
                                DocTypeItem.ChildItems.Add(DocTypeItemNewDoc)
                            End If
                        End If
                End Select
            End If
        Next
        ''Dim Securities As New eqSecurities(AppSettings)
        'Dim Categories2 = From DT As eqDocType In AppSettings.DocTypes.Values _
        '                 Where AppSettings.UserSecurities.HasDocumentsAccessMenu(DT.DocTypeID) _
        '                 Order By DT.Category.SortOrder, DT.SortOrder _
        '                 Group DT By DT.Category2 Into DocTypes = Group _
        '                 Select New With {DocTypes, .C2 = DocTypes(0).Category2}
        'For Each Category2 In Categories2
        '    Dim Category2Item As MenuItem = Nothing
        '    If Not String.IsNullOrEmpty(Category2.C2) Then
        '        ' Создаем элемент
        '        Category2Item = New MenuItem(Category2.C2)
        '        Category2Item.ImageUrl = GetThemeRelativeURL("Images/Folders.gif")
        '        Category2Item.ToolTip = Category2.C2
        '        'If CategoryID = Category.C.ID Then CategoryItem.Selected = True
        '        ' Добавляем
        '        MenuSystem.Items.Add(Category2Item)
        '    End If
        '    ' Групируем по категории
        '    Dim Categories = From DT As eqDocType In Category2.DocTypes _
        '                     Order By DT.Category.SortOrder, DT.SortOrder _
        '                     Group DT By DT.Category.ID Into DocTypes = Group _
        '                     Select New With {DocTypes, .C = DocTypes(0).Category}
        '    ' Заполняем
        '    For Each Category In Categories
        '        ' Создаем элемент
        '        Dim CategoryItem As New MenuItem(Category.C.CategoryName, "Category|" & Category.C.ID.ToString)
        '        If Category.C.ImageURL <> "" Then
        '            CategoryItem.ImageUrl = GetThemeRelativeURL(Category.C.ImageURL)
        '        Else
        '            CategoryItem.ImageUrl = GetThemeRelativeURL("Images/Folders.gif")
        '        End If
        '        If NavigateMode = NavigateModes.Redirect Then
        '            If Category.C.NavigateUrl <> "" Then
        '                CategoryItem.NavigateUrl = Category.C.NavigateUrl
        '            Else
        '                CategoryItem.NavigateUrl = GetCategoryURL(Category.C.ID, False)
        '            End If
        '        End If
        '        CategoryItem.ToolTip = Category.C.ToolTip
        '        If CategoryID = Category.C.ID Then CategoryItem.Selected = True
        '        ' Добавляем
        '        If Category2Item IsNot Nothing Then
        '            Category2Item.ChildItems.Add(CategoryItem)
        '        Else
        '            MenuSystem.Items.Add(CategoryItem)
        '        End If
        '        For Each DocType In Category.DocTypes
        '            ' Создаем элемент
        '            Dim DocTypeItem As New MenuItem(DocType.DocTypeName, "DocType|" & DocType.URL & "|OpenMode|" & DocType.OpenMode)
        '            If DocType.ImageURL <> "" Then
        '                DocTypeItem.ImageUrl = GetThemeRelativeURL(DocType.ImageURL)
        '            Else
        '                If DocType.OpenMode = "L" Then
        '                    DocTypeItem.ImageUrl = GetThemeRelativeURL("Images/Folder.gif")
        '                Else
        '                    DocTypeItem.ImageUrl = GetThemeRelativeURL("Images/New.gif")
        '                End If
        '            End If
        '            If DocType.IsManaged Then
        '                If NavigateMode = NavigateModes.Redirect Then
        '                    If DocType.OpenMode = "L" Then
        '                        ' List mode
        '                        DocTypeItem.NavigateUrl = GetListDocURL(DocType.URL, False)
        '                    Else
        '                        ' Document mode
        '                        DocTypeItem.NavigateUrl = GetNewDocURL(DocType.URL, False)
        '                    End If
        '                End If
        '            Else
        '                DocTypeItem.NavigateUrl = DocType.URL
        '            End If
        '            DocTypeItem.Target = DocType.URLTarget
        '            DocTypeItem.ToolTip = DocType.ToolTip
        '            If DocTypeURL = DocType.URL Then DocTypeItem.Selected = True
        '            ' Добавляем
        '            CategoryItem.ChildItems.Add(DocTypeItem)
        '            ' ExtendedMenuMode
        '            If DocType.IsManaged AndAlso ExtendedMenuMode Then
        '                Select Case DocType.OpenMode
        '                    Case "D"
        '                        If AppSettings.UserSecurities.HasDocumentsAccessList(DocType.DocTypeID) Then
        '                            Dim cmdListDoc = DocType.ToolBarRows.FirstOrDefault(Function(TBR) TBR(eqDocType.FLD_TOOLBAR_COMMANDNAME) = "ListDoc")
        '                            If cmdListDoc IsNot Nothing Then
        '                                Dim DocTypeItemListDoc As New MenuItem(GetGlobalResourceObject("eqResources", "MNU_MenuList"), "DocType|" & DocType.URL & "|OpenMode|L")
        '                                DocTypeItemListDoc.ImageUrl = GetThemeRelativeURL("Images/Folder.gif")
        '                                If NavigateMode = NavigateModes.Redirect Then
        '                                    DocTypeItemListDoc.NavigateUrl = GetListDocURL(DocType.URL, False)
        '                                End If
        '                                DocTypeItem.ChildItems.Add(DocTypeItemListDoc)
        '                            End If
        '                        End If
        '                    Case "L"
        '                        If AppSettings.UserSecurities.HasDocumentsAccessNew(DocType.DocTypeID) Then
        '                            Dim cmdNewDoc = DocType.ToolBarRows.FirstOrDefault(Function(TBR) TBR(eqDocType.FLD_TOOLBAR_COMMANDNAME) = "NewDoc")
        '                            If cmdNewDoc IsNot Nothing Then
        '                                Dim DocTypeItemNewDoc As New MenuItem(GetGlobalResourceObject("eqResources", "MNU_MenuNew"), "DocType|" & DocType.URL & "|OpenMode|D")
        '                                DocTypeItemNewDoc.ImageUrl = GetThemeRelativeURL("Images/New.gif")
        '                                If NavigateMode = NavigateModes.Redirect Then
        '                                    DocTypeItemNewDoc.NavigateUrl = GetNewDocURL(DocType.URL, False)
        '                                End If
        '                                DocTypeItem.ChildItems.Add(DocTypeItemNewDoc)
        '                            End If
        '                        End If
        '                End Select
        '            End If
        '        Next
        '    Next
        'Next
        ' Добавляем "О программе"
        Dim AboutMenuItem As New MenuItem("О программе", "About", GetThemeRelativeURL("Images/Help.gif"), "About.aspx") With {.ToolTip = "О программе"}
        MenuSystem.Items.Add(AboutMenuItem)
        ' Добавляем Exit
        If My.Settings.AnonymousUserLogin Is Nothing OrElse AppSettings.UserInfo.UserLogin <> My.Settings.AnonymousUserLogin Then
            Dim ExitMenuItem As New MenuItem(GetGlobalResourceObject("eqResources", "MNU_MenuExit"))
            ExitMenuItem.Value = "Exit"
            ExitMenuItem.ImageUrl = GetThemeRelativeURL("Images/Exit.gif")
            ExitMenuItem.ToolTip = GetGlobalResourceObject("eqResources", "TIP_MenuExit")
            MenuSystem.Items.Add(ExitMenuItem)
        End If
    End Sub

    Public Sub LoadUserInfo()
        PanelLoggedIn.Visible = False
        PanelLoggedOut.Visible = False
        If My.Settings.AnonymousUserLogin IsNot Nothing AndAlso AppSettings.UserInfo.UserLogin = My.Settings.AnonymousUserLogin Then
            ' Анонимный
            PanelLoggedOut.Visible = True
        Else
            ' Нормальный
            Dim Position As String = AppSettings.UserInfo.GetUserSetting("Position").ToString.Trim
            Dim DisplayName As String = AppSettings.UserInfo.GetUserSetting("DisplayName").ToString.Trim
            HyperLinkUser.Text = Position & IIf(Position <> String.Empty, " : ", "") & DisplayName
            HyperLinkUser.NavigateUrl = GetDocURL("Users.ascx", AppSettings.UserInfo.UserID, False)
            PanelLoggedIn.Visible = True
        End If
    End Sub

    Protected Overrides Sub ShowMessages(ByVal Messages As System.Collections.Generic.List(Of String))
        PlaceHolderMessages.Controls.Clear()
        For Each Message As String In Messages
            Dim MessagePanel As New Panel With {.CssClass = "MenuSubItemStyle"}
            Dim MessageLabel As New Label With {.Text = Message}
            MessagePanel.Controls.Add(MessageLabel)
            PlaceHolderMessages.Controls.Add(MessagePanel)
        Next
        PlaceHolderModalMessages.Visible = True
        ModalPopupExtenderMessages.Show()
        PanelMessagesContent.Attributes("onclick") = "$find('" & ModalPopupExtenderMessages.ClientID & "').hide();"
        UpdatePanelModalMessages.Update()
    End Sub

    Protected Overrides Sub ShowMessages(ByVal Messages As System.Collections.Generic.List(Of String), ByVal Caption As String, ByVal Buttons As Equipage.WebUI.eqBasePage.AddMessageButtons, ByVal ShowCloseIcon As Boolean)
        PlaceHolderMessages.Controls.Clear()
        For Each Message As String In Messages
            Dim MessagePanel As New Panel With {.CssClass = "MenuSubItemStyle"}
            Dim MessageLabel As New Label With {.Text = Message}
            MessagePanel.Controls.Add(MessageLabel)
            PlaceHolderMessages.Controls.Add(MessagePanel)
        Next

        If Caption <> "" Then
            LabelMessagesTitle.Text = Caption
        End If
        ImageButtonMessagesOk.Attributes("onclick") = "$find('" & ModalPopupExtenderMessages.ClientID & "').hide();"
        ImageButtonMessagesOk.Visible = ShowCloseIcon

        Select Case Buttons
            Case AddMessageButtons.AbortRetryIgnore
                Dim ButtonAbort As New Button
                ButtonAbort.Style("position") = "relative"
                ButtonAbort.Style("margin") = "5px"
                ButtonAbort.Attributes("onclick") = Page.ClientScript.GetPostBackEventReference(Page, "MSG:" & [Enum].GetName(GetType(eqBasePage.AddMessageResult), eqBasePage.AddMessageResult.Abort)) & "; $find('" & ModalPopupExtenderMessages.ClientID & "').hide(); return false;"
                ButtonAbort.Text = "Прервать"
                PlaceHolderButtons.Controls.Add(ButtonAbort)
                Dim ButtonRetry As New Button
                ButtonRetry.Style("position") = "relative"
                ButtonRetry.Style("margin") = "5px"
                ButtonRetry.Attributes("onclick") = Page.ClientScript.GetPostBackEventReference(Page, "MSG:" & [Enum].GetName(GetType(eqBasePage.AddMessageResult), eqBasePage.AddMessageResult.Retry)) & "; $find('" & ModalPopupExtenderMessages.ClientID & "').hide(); return false;"
                ButtonRetry.Text = "Повтор"
                PlaceHolderButtons.Controls.Add(ButtonRetry)
                Dim ButtonIgnore As New Button
                ButtonIgnore.Style("position") = "relative"
                ButtonIgnore.Style("margin") = "5px"
                ButtonIgnore.Attributes("onclick") = Page.ClientScript.GetPostBackEventReference(Page, "MSG:" & [Enum].GetName(GetType(eqBasePage.AddMessageResult), eqBasePage.AddMessageResult.Ignore)) & "; $find('" & ModalPopupExtenderMessages.ClientID & "').hide(); return false;"
                ButtonIgnore.Text = "Пропустить"
                PlaceHolderButtons.Controls.Add(ButtonIgnore)
            Case AddMessageButtons.OK
                Dim ButtonOK As New Button
                ButtonOK.Style("position") = "relative"
                ButtonOK.Style("margin") = "5px"
                ButtonOK.Attributes("onclick") = Page.ClientScript.GetPostBackEventReference(Page, "MSG:" & [Enum].GetName(GetType(eqBasePage.AddMessageResult), eqBasePage.AddMessageResult.OK)) & "; $find('" & ModalPopupExtenderMessages.ClientID & "').hide(); return false;"
                ButtonOK.Text = "OK"
                PlaceHolderButtons.Controls.Add(ButtonOK)
            Case AddMessageButtons.OKCancel
                Dim ButtonOK As New Button
                ButtonOK.Style("position") = "relative"
                ButtonOK.Style("margin") = "5px"
                ButtonOK.Attributes("onclick") = Page.ClientScript.GetPostBackEventReference(Page, "MSG:" & [Enum].GetName(GetType(eqBasePage.AddMessageResult), eqBasePage.AddMessageResult.OK)) & "; $find('" & ModalPopupExtenderMessages.ClientID & "').hide(); return false;"
                ButtonOK.Text = "OK"
                PlaceHolderButtons.Controls.Add(ButtonOK)
                Dim ButtonCancel As New Button
                ButtonCancel.Style("position") = "relative"
                ButtonCancel.Style("margin") = "5px"
                ButtonCancel.Attributes("onclick") = Page.ClientScript.GetPostBackEventReference(Page, "MSG:" & [Enum].GetName(GetType(eqBasePage.AddMessageResult), eqBasePage.AddMessageResult.Cancel)) & "; $find('" & ModalPopupExtenderMessages.ClientID & "').hide(); return false;"
                ButtonCancel.Text = "Отмена"
                PlaceHolderButtons.Controls.Add(ButtonCancel)
            Case AddMessageButtons.RetryCancel
                Dim ButtonRetry As New Button
                ButtonRetry.Style("position") = "relative"
                ButtonRetry.Style("margin") = "5px"
                ButtonRetry.Attributes("onclick") = Page.ClientScript.GetPostBackEventReference(Page, "MSG:" & [Enum].GetName(GetType(eqBasePage.AddMessageResult), eqBasePage.AddMessageResult.Retry)) & "; $find('" & ModalPopupExtenderMessages.ClientID & "').hide(); return false;"
                ButtonRetry.Text = "Повтор"
                PlaceHolderButtons.Controls.Add(ButtonRetry)
                Dim ButtonCancel As New Button
                ButtonCancel.Style("position") = "relative"
                ButtonCancel.Style("margin") = "5px"
                ButtonCancel.Attributes("onclick") = Page.ClientScript.GetPostBackEventReference(Page, "MSG:" & [Enum].GetName(GetType(eqBasePage.AddMessageResult), eqBasePage.AddMessageResult.Cancel)) & "; $find('" & ModalPopupExtenderMessages.ClientID & "').hide(); return false;"
                ButtonCancel.Text = "Отмена"
                PlaceHolderButtons.Controls.Add(ButtonCancel)
            Case AddMessageButtons.YesNo
                Dim ButtonYes As New Button
                ButtonYes.Style("position") = "relative"
                ButtonYes.Style("margin") = "5px"
                ButtonYes.Attributes("onclick") = Page.ClientScript.GetPostBackEventReference(Page, "MSG:" & [Enum].GetName(GetType(eqBasePage.AddMessageResult), eqBasePage.AddMessageResult.Yes)) & "; $find('" & ModalPopupExtenderMessages.ClientID & "').hide(); return false;"
                ButtonYes.Text = "Да"
                PlaceHolderButtons.Controls.Add(ButtonYes)
                Dim ButtonNo As New Button
                ButtonNo.Style("position") = "relative"
                ButtonNo.Style("margin") = "5px"
                ButtonNo.Attributes("onclick") = Page.ClientScript.GetPostBackEventReference(Page, "MSG:" & [Enum].GetName(GetType(eqBasePage.AddMessageResult), eqBasePage.AddMessageResult.No)) & "; $find('" & ModalPopupExtenderMessages.ClientID & "').hide(); return false;"
                ButtonNo.Text = "Нет"
                PlaceHolderButtons.Controls.Add(ButtonNo)
            Case AddMessageButtons.YesNoCancel
                Dim ButtonYes As New Button
                ButtonYes.Style("position") = "relative"
                ButtonYes.Style("margin") = "5px"
                ButtonYes.Attributes("onclick") = Page.ClientScript.GetPostBackEventReference(Page, "MSG:" & [Enum].GetName(GetType(eqBasePage.AddMessageResult), eqBasePage.AddMessageResult.Yes)) & "; $find('" & ModalPopupExtenderMessages.ClientID & "').hide(); return false;"
                ButtonYes.Text = "Да"
                PlaceHolderButtons.Controls.Add(ButtonYes)
                Dim ButtonNo As New Button
                ButtonNo.Style("position") = "relative"
                ButtonNo.Style("margin") = "5px"
                ButtonNo.Attributes("onclick") = Page.ClientScript.GetPostBackEventReference(Page, "MSG:" & [Enum].GetName(GetType(eqBasePage.AddMessageResult), eqBasePage.AddMessageResult.No)) & "; $find('" & ModalPopupExtenderMessages.ClientID & "').hide(); return false;"
                ButtonNo.Text = "Нет"
                PlaceHolderButtons.Controls.Add(ButtonNo)
                Dim ButtonCancel As New Button
                ButtonCancel.Style("position") = "relative"
                ButtonCancel.Style("margin") = "5px"
                ButtonCancel.Attributes("onclick") = Page.ClientScript.GetPostBackEventReference(Page, "MSG:" & [Enum].GetName(GetType(eqBasePage.AddMessageResult), eqBasePage.AddMessageResult.Cancel)) & "; $find('" & ModalPopupExtenderMessages.ClientID & "').hide(); return false;"
                ButtonCancel.Text = "Отмена"
                PlaceHolderButtons.Controls.Add(ButtonCancel)
        End Select

        PanelMessagesButton.Visible = True
        PlaceHolderModalMessages.Visible = True
        ModalPopupExtenderMessages.Show()
        UpdatePanelModalMessages.Update()
    End Sub

    Public Overrides Sub AttachFile(ByVal FileName As String, ByVal FileBytes() As Byte, Optional ByVal ContentType As String = "application/x-unknown")
        Dim FileAttachItem As New FileAttach.FileAttachItem With {.FileName = FileName, .FileBytes = FileBytes, .ContentType = ContentType}
        Dim FileAttachItemID = Guid.NewGuid
        Cache(FileAttachItemID.ToString) = FileAttachItem
        ExecuteScript(String.Format("window.location='FileAttach.ashx?ID={0}';", FileAttachItemID.ToString))
        'OpenWindow(GetAppRelativeURL("FileAttach.ashx?ID=" + FileAttachItemID.ToString))
    End Sub

#End Region

#Region "Authentification"

    Private Sub LogIn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LogIn.Click
        Try
            AuthenticateForms(tbLogin.Text, tbPassword.Text)
            Response.Redirect(Request.Url.ToString)
        Catch ex As Exception
            AddMessage(ex.Message)
        End Try
    End Sub

    Private Sub LogOut_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LogOut.Click
        Response.Cookies(My.Settings.CookieName).Expires = Now
        Session.Abandon()
        RedirectToLoginPage()
    End Sub

    Sub AuthenticateForms(ByVal Login As String, ByVal Pass As String)

        Dim SqlConnectionStringBuilder As New SqlClient.SqlConnectionStringBuilder(My.Settings.DatabaseConnectionString)
        Dim UserSqlConnection As New SqlClient.SqlConnection(SqlConnectionStringBuilder.ConnectionString)
        Try
            UserSqlConnection.Open()
        Catch ex As SqlClient.SqlException
            Throw New Exception(String.Format(GetGlobalResourceObject("eqResources", "MSG_DBConnectionFailed"), ex.Message))
        End Try

        Dim ValidateUserSqlCommand As New SqlClient.SqlCommand("SELECT TOP 1 ID FROM eqUsers WHERE Login=@Login AND Pass=@Pass AND Deleted=0", UserSqlConnection)
        ValidateUserSqlCommand.Parameters.AddWithValue("@Login", Login)
        ValidateUserSqlCommand.Parameters.AddWithValue("@Pass", Pass)
        Dim UserID As Object = ValidateUserSqlCommand.ExecuteScalar
        Dim AuthSuccess As Boolean = False
        If IsDBNull(UserID) Or UserID Is Nothing Then
            Throw New Exception(GetGlobalResourceObject("eqResources", "MSG_UserOrPasswordFailed"))
            AuthSuccess = True
        End If
        eqAppSettings.WriteActitityLog(UserSqlConnection, Login, "", AuthSuccess, Request.UserAgent.ToString(), Request.UserHostAddress, Request.ToString())


        Dim UserAppSettings As New eqAppSettings
        UserAppSettings.AdministratorUserLogin = My.Settings.AdministratorUserLogin
        UserAppSettings.RolesAllowPriority = My.Settings.RolesAllowPriority
        UserAppSettings.Manager = My.Resources.ResourceManager
        UserAppSettings.OpenSession(UserSqlConnection, Login)
        Session("AppSettings") = UserAppSettings

        If My.Settings.CookieName <> String.Empty Then

            'Dim Cookie As String = Math.Abs(Guid.NewGuid.GetHashCode).ToString & Math.Abs(Login.GetHashCode).ToString & Math.Abs(Now.Day).ToString & Math.Abs(Guid.NewGuid.GetHashCode).ToString & Math.Abs(My.Computer.Name.GetHashCode).ToString & Math.Abs(Now.Month).ToString & Math.Abs(Guid.NewGuid.GetHashCode).ToString & Math.Abs(Now.Month).ToString & Math.Abs(Now.Day).ToString & Math.Abs(Guid.NewGuid.GetHashCode).ToString & Math.Abs(Guid.NewGuid.GetHashCode).ToString
            Dim Cookie As String = Now.Year.GetHashCode.ToString & Login.GetHashCode.ToString & Now.Month.GetHashCode.ToString & Pass.GetHashCode.ToString & Now.Day.GetHashCode.ToString
            Dim UpdateUserCookieSqlCommand As New SqlClient.SqlCommand("UPDATE eqUsers SET Cookie=@Cookie WHERE Login=@Login AND Pass=@Pass AND Deleted=0", UserSqlConnection)
            UpdateUserCookieSqlCommand.Parameters.AddWithValue("@Cookie", Cookie)
            UpdateUserCookieSqlCommand.Parameters.AddWithValue("@Login", Login)
            UpdateUserCookieSqlCommand.Parameters.AddWithValue("@Pass", Pass)
            UpdateUserCookieSqlCommand.ExecuteNonQuery()

            Response.Cookies(My.Settings.CookieName).Expires = Now.AddDays(1)
            Response.Cookies(My.Settings.CookieName).Item("Login") = Login
            Response.Cookies(My.Settings.CookieName).Item("Cookie") = Cookie

        End If

    End Sub

#End Region

    Private Sub SendRemember_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles SendRemember.Click
        If EmailRemember.Text.Contains("@") And EmailRemember.Text.Contains(".") Then
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM eqUsers WHERE ';'+Email+';' LIKE @Email")
            cmd.Parameters.AddWithValue("@Email", "%;" & EmailRemember.Text & ";%")
            Dim usersDataTable = AppSettings.GetDataTable(cmd)
            If usersDataTable.Rows.Count = 0 Then
                AddMessage(GetGlobalResourceObject("eqResources", "MSG_UserEmailNoMatch"))
            Else
                For Each rUserInfo In usersDataTable.Rows

                    Dim client As New System.Net.Mail.SmtpClient
                    Dim message As New System.Net.Mail.MailMessage
                    message.From = New System.Net.Mail.MailAddress("zakusit-natroikh@yandex.ru", "Администратор ИПДО")
                    For Each addr In rUserInfo("Email").Split(New Char() {";"})
                        message.To.Add(New System.Net.Mail.MailAddress(addr, rUserInfo("DisplayName")))
                    Next

                    message.BodyEncoding = Text.Encoding.UTF8
                    message.SubjectEncoding = Text.Encoding.UTF8
                    message.Subject = "Восстановление пароля ИПДО"
                    message.Body = "Ваш логин: " & rUserInfo("Login") & " Ваш пароль: " & rUserInfo("Pass")

                    client.Send(message)
                Next
                AddMessage(GetGlobalResourceObject("eqResources", "MSG_UserEmailTakePassword"))
            End If
        Else
            AddMessage(GetGlobalResourceObject("eqResources", "MSG_UserEmailFailed"))
        End If
    End Sub

#Region "Modal"

    ' Выключил модальные окна

    'Public Overrides Function GetOpenModalDialogJavaScript(ByVal SourceControl As System.Web.UI.Control, ByVal Url As String, ByVal Argument As Equipage.WebUI.eqModalDialogArgument, Optional ByVal Width As Integer = 0, Optional ByVal Height As Integer = 0) As String
    '    Dim ModalDialogID As String = Guid.NewGuid.ToString
    '    Session(ModalDialogID) = Argument
    '    Return String.Format("openModalDialog('{0}',{1},{2});", Url & IIf(Url.Contains("?"), "&", "?") & ConstModalDialog & "=" & ModalDialogID & "&" & ConstModalDialogSource & "=" & Server.UrlEncode(SourceControl.UniqueID), Width, Height)
    'End Function

    'Public Overrides Function GetCloseModalDialogJavaScript(ByVal Argument As Equipage.WebUI.eqModalDialogArgument) As String
    '    Dim SourceControlUniqueID As String = ModalDialogSource
    '    Argument.DialogIdentifier = ModalDialogArgument.DialogIdentifier
    '    Argument.DelegateName = ModalDialogArgument.DelegateName
    '    Session(ModalDialogID) = Argument
    '    Return String.Format("window.parent.closeModalDialog('{0}');", "__doPostBack(\'" & SourceControlUniqueID & "\',\'" & ModalDialogID & "\')")
    'End Function

#End Region

#Region "FakeUser"

    ''' <summary>
    ''' Если выбран пользователь 
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub FakeUserID_SelectDocCompleted() Handles FakeUserID.SelectDocCompleted
        If FakeUserID.DocID <> Guid.Empty Then
            AppSettings.FakeUserLogIn(FakeUserID.DocID)
            Response.Redirect(Request.Url.AbsoluteUri)
        End If
    End Sub

    ''' <summary>
    ''' Вывод блока работы под другой учетной записью
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ShowFakeUserBlock()
        ' Если пользователь не авторизован
        If My.Settings.AnonymousUserLogin IsNot Nothing AndAlso AppSettings.UserInfo.UserLogin = My.Settings.AnonymousUserLogin Then
            PanelFakeUser.Visible = False
        Else ' Если пользователь авторизован
            ' Если пользователь сейчас работает под собой и у него есть право входить под другими учетками
            If AppSettings.IsRealUser AndAlso AppSettings.UserSecurities.IsAdministrator Then
                PanelFakeUser.Visible = True
                FakeUserLabel.Text = "Войти как:"
                FakeUserLogOut.Visible = False
            ElseIf Not AppSettings.IsRealUser Then ' Если Пользователь работает под чужой учеткой
                PanelFakeUser.Visible = True
                FakeUserLabel.Text = "Вы сейчас:"
                FakeUserID.DocID = AppSettings.UserInfo.UserID
                FakeUserLogOut.Visible = True
            Else ' Если пользователь работает под собой и у него нет права входить под чужими учетками
                PanelFakeUser.Visible = False
            End If
        End If
    End Sub

    ''' <summary>
    ''' Выход из под чужой учетки
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub FakeUserLogOut_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles FakeUserLogOut.Click
        AppSettings.FakeUserLogOut()
        Response.Redirect(Request.Url.AbsoluteUri)
    End Sub

#End Region

#Region "Menu"

    Private Sub ShowHideButton_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles ShowHideButton.PreRender
        If String.IsNullOrEmpty(DocTypeURL) Then
            ' Отображать меню
            ShowHideButton.Visible = False
        Else
            ' Скрыть меню
            Dim Script = <script>
                            var rootMenuVisible = false;
                            var rootMenuInterval = 0;
                            
                            function rootMenuShow(menuId) {
                                rootMenuVisible = true;
                                clearTimeout(rootMenuInterval);

                                var menu = document.getElementById(menuId);
                                menu.style.display='block';
                            }
                            function rootMenuHide(menuId, delay) {
                                rootMenuVisible = false;
                                clearTimeout(rootMenuInterval);
                                rootMenuInterval = setTimeout(function() {
                                    if (!rootMenuVisible) {
                                        var menu = document.getElementById(menuId);
                                        menu.style.display='none';
                                    }
                                }, delay);
                            }
                         </script>


            Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "rootMenu", Script.Value, True)
            MenuSystem.Style("display") = "none"
            MenuSystem.Style("position") = "absolute"

            Dim ShowScript = String.Format("rootMenuShow('{0}');", MenuSystem.ClientID)
            ShowHideButton.Attributes("onmouseover") = ShowScript
            MenuSystem.Attributes("onmouseover") = ShowScript

            Dim HideScript = String.Format("rootMenuHide('{0}', {1});", MenuSystem.ClientID, MenuSystem.DisappearAfter)
            ShowHideButton.Attributes("onmouseout") = HideScript
            MenuSystem.Attributes("onmouseout") = HideScript

            ShowHideButton.ImageUrl = GetThemeRelativeURL("/Images/Tree.gif")
            ShowHideButton.ToolTip = GetGlobalResourceObject("eqResources", "Default_ShowHideButton_Show")
        End If
    End Sub

#End Region

End Class
