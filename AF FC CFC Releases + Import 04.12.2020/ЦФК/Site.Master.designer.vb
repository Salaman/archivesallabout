﻿'------------------------------------------------------------------------------
' <автоматически создаваемое>
'     Этот код создан программой.
'
'     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
'     повторной генерации кода. 
' </автоматически создаваемое>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Site

    '''<summary>
    '''Head элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents Head As Global.System.Web.UI.WebControls.ContentPlaceHolder

    '''<summary>
    '''form1 элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm

    '''<summary>
    '''ToolkitScriptManagerDefault элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents ToolkitScriptManagerDefault As Global.AjaxControlToolkit.ToolkitScriptManager

    '''<summary>
    '''FocusedHiddenField элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents FocusedHiddenField As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''DefaultButton элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents DefaultButton As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''RefreshError элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents RefreshError As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''PanelContent элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents PanelContent As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''PanelSystem элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents PanelSystem As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''HyperLinkLogo элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents HyperLinkLogo As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''Header элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents Header As Global.System.Web.UI.WebControls.ContentPlaceHolder

    '''<summary>
    '''ImageEquipageLogo элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents ImageEquipageLogo As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Body элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents Body As Global.System.Web.UI.WebControls.ContentPlaceHolder

    '''<summary>
    '''PanelUpdateProgress элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents PanelUpdateProgress As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''ModalPopupExtenderProgress элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents ModalPopupExtenderProgress As Global.AjaxControlToolkit.ModalPopupExtender
End Class
