﻿Public Partial Class PrintForm
    Inherits BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If DocTypeURL IsNot Nothing AndAlso DocID <> Guid.Empty Then
            Try
                Dim BaseDocControl As eqBaseDoc = LoadControl(DocumentsDirectoryUrl & DocTypeURL)
                BaseDocControl.ID = "Doc"
                BaseDocControl.DocID = DocID
                ContentPlaceHolder.Controls.Add(BaseDocControl)
                'ShowPrintPreview()
            Catch ex As Exception
                AddMessage("Ошибка загрузки контрола документа -> " & ex.Message & " -> " & ex.StackTrace)
            End Try
        Else
            AddMessage("Необходимо указать DocTypeURL и DocID")
        End If
    End Sub

End Class