﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Welcome.aspx.vb"
    Inherits="WebApplication.Welcome" %>
<%@ MasterType VirtualPath="~/Site.master" %>

<asp:Content ContentPlaceHolderID="Body" runat="server">
    <asp:Panel runat="server" DefaultButton="ButtonEnter">
        <table width="100%">
            <tr>
                <td align="center">
                    <asp:Panel ID="PanelWelcomePadding" runat="server" />
                    <table style="width: 300px;" class="LogicBlockTable">
                        <tr>
                            <td>
                                База данных
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:DropDownList ID="Databases" runat="server" Width="100%" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="ButtonEnter" runat="server" Width="100%" Text="Выбрать" />
                            </td>
                        </tr>
                    </table>
                    <asp:Panel ID="PanelMessagesPadding" runat="server" Height="10px" />
                    <asp:Panel ID="PanelMessagesContent" runat="server" Style="width: 90%;">
                        <asp:PlaceHolder ID="PlaceHolderMessages" runat="server" />
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
