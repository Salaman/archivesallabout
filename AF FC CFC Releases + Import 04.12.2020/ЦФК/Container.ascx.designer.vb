﻿'------------------------------------------------------------------------------
' <автоматически создаваемое>
'     Этот код создан программой.
'
'     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
'     повторной генерации кода. 
' </автоматически создаваемое>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Container

    '''<summary>
    '''PanelInfo элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents PanelInfo As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''LabelInfo элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents LabelInfo As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''PanelMenu элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents PanelMenu As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''MenuActions элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents MenuActions As Global.Equipage.WebUI.eqMenu

    '''<summary>
    '''ImageButtonFirst элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents ImageButtonFirst As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''ImageButtonPrevious элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents ImageButtonPrevious As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''ImageButtonNext элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents ImageButtonNext As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''ImageButtonLast элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents ImageButtonLast As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''PanelPlaceHolder элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents PanelPlaceHolder As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''divLevel элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents divLevel As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Crumbs элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents Crumbs As Global.WebApplication.WebUI.ArchiveCrumbs

    '''<summary>
    '''PlaceHolderContent элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents PlaceHolderContent As Global.System.Web.UI.WebControls.PlaceHolder

    '''<summary>
    '''PlaceHolderShowConfirm элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents PlaceHolderShowConfirm As Global.System.Web.UI.WebControls.PlaceHolder

    '''<summary>
    '''PanelConfirm элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents PanelConfirm As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''PanelConfirmTitle элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents PanelConfirmTitle As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''LabelConfirmTitle элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents LabelConfirmTitle As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''PanelConfirmContent элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents PanelConfirmContent As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''PlaceHolderConfirm элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents PlaceHolderConfirm As Global.System.Web.UI.WebControls.PlaceHolder

    '''<summary>
    '''ButtonConfirmOk элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents ButtonConfirmOk As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''ButtonConfirmCancel элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents ButtonConfirmCancel As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''ButtonShowConfirm элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents ButtonShowConfirm As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''ModalPopupExtenderConfirm элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents ModalPopupExtenderConfirm As Global.AjaxControlToolkit.ModalPopupExtender

    '''<summary>
    '''SaveDocHistoryTrigger элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents SaveDocHistoryTrigger As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''SaveDocHistoryFlag элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents SaveDocHistoryFlag As Global.System.Web.UI.WebControls.HiddenField
End Class
