﻿Partial Public Class ListDoc
    Inherits BaseListDoc

    Public Property PageIndex() As Integer
        Get
            If ViewState("PageIndex") Is Nothing Then
                Return 1
            Else
                Return ViewState("PageIndex")
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("PageIndex") = value
            If TableBuilded Then
                BuildTable()
            End If
        End Set
    End Property

    Public ReadOnly Property ExpandedGroups() As Dictionary(Of String, Boolean)
        Get
            If ViewState("ExpandedGroups") Is Nothing Then
                ViewState("ExpandedGroups") = New Dictionary(Of String, Boolean)
            End If
            Return ViewState("ExpandedGroups")
        End Get
    End Property

    Private ExpandedGroupsLevel As New Dictionary(Of Integer, Boolean)
    Private ExpandedGroupsLevelExpand As Integer = -1
    Private ExpandedGroupsLevelCollapse As Integer = -1

    Public Property SetFocusOnFirstListDocFilterControl() As Boolean
        Get
            If ViewState("SetFocusOnFirstListDocFilterControl") Is Nothing Then
                Return True
            Else
                Return ViewState("SetFocusOnFirstListDocFilterControl")
            End If
        End Get
        Set(ByVal value As Boolean)
            ViewState("SetFocusOnFirstListDocFilterControl") = value
        End Set
    End Property

    Public Overrides ReadOnly Property LoadDocInNewWindow() As Boolean
        Get
            Return LoadDocMode = eqBaseContainer.LoadDocModes.NewWindow Or LoadDocMode = eqBaseContainer.LoadDocModes.NewWindowAndSelect
        End Get
    End Property

    Public Overrides Sub SetListDocData()

        MyBase.SetListDocData()

        PageIndex = 1
        ExpandedGroups.Clear()
        'Cache(ClientID & "FirstListDocGroup") = Nothing

        If TableBuilded Then
            BuildTable()
        End If

        UpdatePanelTableListDoc.Update()

    End Sub

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        BuildTable()
    End Sub

    Private Class ListDocGroup
        Public GroupLevel As Integer
        Public GroupPath As String

        Public GroupColumnName As String
        Public GroupValue As Object

        Public GroupExpanded As Boolean

        Public GroupCounter As Integer
        Public GroupRowsCounter As Integer
        Public GroupExpandedCounter As Integer

        Public ListDocGroups As List(Of ListDocGroup)
        Public ListDocRows As IEnumerable(Of DataRow)

        Public AggregatesValues As New Dictionary(Of String, Object)

        Public FirstListDocRow As DataRow
    End Class

    Private Function CreateListDocGroup(ByVal ListDocRows As IEnumerable(Of DataRow), Optional ByVal GroupLevel As Integer = 0, Optional ByVal GroupPath As String = "ROOT") As ListDocGroup
        CreateListDocGroup = New ListDocGroup With {.GroupLevel = GroupLevel, .GroupPath = GroupPath}
        CreateListDocGroup.FirstListDocRow = ListDocRows.FirstOrDefault
        ' Aggregates
        For Each R In ViewFields
            Dim FieldID = DirectCast(R("FieldID"), Guid)
            Dim AggregateType = R("AggregateType").ToString
            Dim AggregateValue As Object = Nothing
            Select Case AggregateType
                Case "COUNT"
                    Dim AggregateValues = From LDR In ListDocRows Where Not IsDBNull(LDR(FieldID.ToString("n"))) Select LDR(FieldID.ToString("n"))
                    AggregateValue = 12'AggregateValues.Count
                Case "SUM"
                    Dim AggregateValues = From LDR In ListDocRows Where IsNumeric(LDR(FieldID.ToString("n"))) Select CDbl(LDR(FieldID.ToString("n")))
                    AggregateValue = AggregateValues.Sum
                Case "MIN"
                    Dim AggregateValues = From LDR In ListDocRows Where IsNumeric(LDR(FieldID.ToString("n"))) Select CDbl(LDR(FieldID.ToString("n")))
                    If AggregateValues.Count > 0 Then
                        AggregateValue = AggregateValues.Min
                    End If
                Case "MAX"
                    Dim AggregateValues = From LDR In ListDocRows Where IsNumeric(LDR(FieldID.ToString("n"))) Select CDbl(LDR(FieldID.ToString("n")))
                    If AggregateValues.Count > 0 Then
                        AggregateValue = AggregateValues.Max
                    End If
                Case "AVG"
                    Dim AggregateValues = From LDR In ListDocRows Where IsNumeric(LDR(FieldID.ToString("n"))) Select CDbl(LDR(FieldID.ToString("n")))
                    If AggregateValues.Count > 0 Then
                        AggregateValue = AggregateValues.Average
                    End If
            End Select
            CreateListDocGroup.AggregatesValues.Add(FieldID.ToString("n"), AggregateValue)
        Next
        ' SubExpanded
        If ExpandedGroups.ContainsKey(GroupPath) Then
            If ExpandedGroupsLevelExpand > -1 AndAlso GroupLevel <= ExpandedGroupsLevelExpand Then
                CreateListDocGroup.GroupExpanded = True
                ExpandedGroups(GroupPath) = True
            ElseIf ExpandedGroupsLevelCollapse > -1 AndAlso GroupLevel >= ExpandedGroupsLevelCollapse Then
                CreateListDocGroup.GroupExpanded = False
                ExpandedGroups(GroupPath) = False
            Else
                CreateListDocGroup.GroupExpanded = ExpandedGroups(GroupPath)
            End If
        Else
            If GroupLevel > 0 Then
                Dim GroupExpanded = DirectCast(ViewGrouppingFields(GroupLevel - 1).Item("GroupExpanded"), Boolean)
                ExpandedGroups.Add(GroupPath, GroupExpanded)
                CreateListDocGroup.GroupExpanded = GroupExpanded
            End If
        End If
        If CreateListDocGroup.GroupExpanded Then
            ExpandedGroupsLevel(GroupLevel) = True
        End If
        If ViewGrouppingFields.Count > GroupLevel Then
            CreateListDocGroup.ListDocGroups = New List(Of ListDocGroup)
            Dim GroupColumnName = DirectCast(ViewGrouppingFields(GroupLevel).Item("FieldID"), Guid).ToString("n")
            Dim GrouppedRows = From R In ListDocRows Group R By GroupValue = R(GroupColumnName) Into Group
            For Index As Integer = 0 To GrouppedRows.Count - 1
                Dim G = GrouppedRows(Index)
                ' SubListDocGroup
                Dim SubListDocGroup = CreateListDocGroup(G.Group, GroupLevel + 1, GroupPath & "_" & Index.ToString)
                SubListDocGroup.GroupColumnName = GroupColumnName
                SubListDocGroup.GroupValue = G.GroupValue
                ' CreateListDocGroup
                CreateListDocGroup.ListDocGroups.Add(SubListDocGroup)
                ' GroupCounter
                CreateListDocGroup.GroupCounter += SubListDocGroup.GroupCounter + 1
                ' GroupRowsCounter
                CreateListDocGroup.GroupRowsCounter += SubListDocGroup.GroupRowsCounter
                ' GroupExpandedCounter
                If CreateListDocGroup.GroupExpanded Then
                    'CreateListDocGroup.GroupExpandedCounter = SubListDocGroup.GroupCounter + 1
                    CreateListDocGroup.GroupExpandedCounter += SubListDocGroup.GroupExpandedCounter + 1
                End If
            Next
        Else
            CreateListDocGroup.ListDocRows = ListDocRows
            CreateListDocGroup.GroupCounter = ListDocRows.Count
            CreateListDocGroup.GroupRowsCounter = ListDocRows.Count
            If CreateListDocGroup.GroupExpanded Then
                CreateListDocGroup.GroupExpandedCounter = ListDocRows.Count
            End If
        End If
    End Function

    Private Sub ProcessListDocGroup(ByVal CurrentListDocGroup As ListDocGroup)
        If CurrentListDocGroup.ListDocGroups IsNot Nothing Then
            For Each G In CurrentListDocGroup.ListDocGroups
                If G.GroupExpandedCounter + ProcessedRows >= ViewRowsPerPage * PageIndex - ViewRowsPerPage AndAlso ProcessedRows < ViewRowsPerPage * PageIndex Then
                    ' Group
                    Dim TableRowGroup As New TableRow With {.CssClass = "GridGroupItem"}
                    ' ExpandButton
                    Dim ExpandButton As New ImageButton With {.ID = "ExpandButton" & G.GroupPath, .CommandName = "Expand", .CommandArgument = G.GroupPath}
                    AddHandler ExpandButton.Command, AddressOf Button_Command
                    If G.GroupExpanded Then
                        ExpandButton.ImageUrl = BasePage.GetThemeRelativeURL("Images/Minus.gif")
                    Else
                        ExpandButton.ImageUrl = BasePage.GetThemeRelativeURL("Images/Plus.gif")
                    End If
                    ' Spacing
                    For Level As Integer = 1 To ViewGrouppingFields.Count
                        Dim TableCellSpacing As New TableCell
                        If Level = G.GroupLevel Then
                            TableCellSpacing.Controls.Add(ExpandButton)
                        End If
                        TableRowGroup.Cells.Add(TableCellSpacing)
                    Next
                    ' ShowNumeration
                    If ShowNumeration Then
                        Dim TableCellShowNumeration As New TableCell
                        TableRowGroup.Cells.Add(TableCellShowNumeration)
                    End If
                    ' ShowSelectBox
                    If ShowSelectBox Then
                        Dim TableCellSelectDoc As New TableCell
                        TableRowGroup.Cells.Add(TableCellSelectDoc)
                    End If
                    ' SelectDoc
                    If SelectDocMode = eqBaseContainer.SelectDocModes.Single Or SelectDocMode = eqBaseContainer.SelectDocModes.SingleAndMulti Then
                        Dim TableCellSelectDoc As New TableCell
                        TableRowGroup.Cells.Add(TableCellSelectDoc)
                    End If
                    ' LoadDoc
                    If LoadDocMode <> eqBaseContainer.LoadDocModes.None And OpenDocToolBarDataRow IsNot Nothing Then
                        Dim TableCellLoadDoc As New TableCell
                        TableRowGroup.Cells.Add(TableCellLoadDoc)
                    End If
                    ' Fields
                    For Each F As DataRowView In ViewEnabledFields
                        Dim FieldID = DirectCast(F("FieldID"), Guid)
                        Dim FieldInfo = DocType.GetFieldInfo(FieldID)
                        Dim ListDocFieldInfo = DocType.GetListDocFieldInfo(FieldID)
                        Dim TableCellField As New TableCell
                        If G.GroupColumnName = FieldID.ToString("n") Then
                            If ListDocFieldInfo("DocTypeURL").ToString <> "" AndAlso ListDocFieldInfo("ShowingFields").ToString <> "" Then
                                Dim FieldDocTypeURL = ListDocFieldInfo("DocTypeURL").ToString
                                Dim FieldShowingFields = ListDocFieldInfo("ShowingFields").ToString
                                Dim ShowingFieldsFormat = ListDocFieldInfo("ShowingFieldsFormat").ToString
                                Dim FieldDocType = AppSettings.GetDocType(FieldDocTypeURL)
                                Dim FieldShowingFieldsValues As New List(Of Object)
                                For Each FieldShowingField In FieldShowingFields.Split(New Char() {";", ","})
                                    Dim FieldShowingFieldAlias = FieldID.ToString("n") & "_" & FieldDocType.GetHeaderTableName & "_" & FieldShowingField
                                    FieldShowingFieldsValues.Add(G.FirstListDocRow(FieldShowingFieldAlias))
                                Next
                                If Not IsDBNull(G.GroupValue) AndAlso TypeOf (G.GroupValue) Is Guid Then
                                    Dim ValueGuid = DirectCast(G.GroupValue, Guid)
                                    If ValueGuid <> Guid.Empty Then
                                        If AppSettings.UserSecurities.HasDocumentsAccessOpen(FieldDocType.DocTypeID) Then
                                            Dim ActionHyperLink As New HyperLink
                                            ActionHyperLink.Text = String.Format(ShowingFieldsFormat, FieldShowingFieldsValues.ToArray) & " (" & G.GroupRowsCounter.ToString & ")"
                                            ActionHyperLink.Target = "_blank"
                                            ActionHyperLink.NavigateUrl = BasePage.GetDocURL(FieldDocTypeURL, ValueGuid, True)
                                            TableCellField.Controls.Add(ActionHyperLink)
                                        Else
                                            TableCellField.Text = String.Format(ShowingFieldsFormat, FieldShowingFieldsValues.ToArray) & " (" & G.GroupRowsCounter.ToString & ")"
                                        End If
                                    End If
                                End If
                            Else
                                Dim Value As Object = G.GroupValue
                                Dim FieldFormat = ListDocFieldInfo("FieldFormat").ToString
                                ' SourceTable
                                If ListDocFieldInfo("SourceTable").ToString <> "" AndAlso ListDocFieldInfo("KeyField").ToString <> "" AndAlso ListDocFieldInfo("DisplayField").ToString <> "" Then
                                    Dim SourceTable = ListDocFieldInfo("SourceTable").ToString
                                    Dim DisplayField = ListDocFieldInfo("DisplayField").ToString
                                    Dim DisplayFieldAlias = FieldID.ToString("n") & "_" & SourceTable & "_" & DisplayField
                                    Value = G.FirstListDocRow(DisplayFieldAlias)
                                End If
                                ' Format output
                                If Not Value Is Nothing AndAlso Not IsDBNull(Value) Then
                                    If FieldFormat = "" Then
                                        ' No Format
                                        If TypeOf Value Is Boolean Then
                                            Dim ValueCheckBox As New CheckBox With {.Checked = Value, .Enabled = False}
                                            TableCellField.Controls.Add(ValueCheckBox)
                                        ElseIf TypeOf Value Is String Then
                                            Dim ValueStr = DirectCast(Value, String)
                                            If ValueStr.Length > 20 Then
                                                TableCellField.Controls.Add(New LiteralControl(ValueStr.Substring(0, 20) & "..."))
                                            Else
                                                TableCellField.Controls.Add(New LiteralControl(ValueStr))
                                            End If
                                        Else
                                            TableCellField.Controls.Add(New LiteralControl(Value.ToString))
                                        End If
                                    ElseIf FieldFormat = "URL" Then 'Для совместимости с предыдущим ядром (added by Anatoly Melkov on 2009-08-27)
                                        Dim mURL As New HyperLink()
                                        mURL.Target = "_blank"
                                        mURL.Text = "Открыть"
                                        mURL.NavigateUrl = Value.ToString
                                        TableCellField.Controls.Add(mURL)
                                    Else
                                        ' Format
                                        If Regex.IsMatch(FieldFormat, "{\d.*}", RegexOptions.Compiled) Then
                                            TableCellField.Controls.Add(New LiteralControl(String.Format(FieldFormat, Value)))
                                        Else
                                            TableCellField.Controls.Add(New LiteralControl(Format(Value, FieldFormat)))
                                        End If
                                    End If
                                End If
                                TableCellField.Controls.Add(New LiteralControl(" (" & G.GroupRowsCounter.ToString & ")"))
                            End If
                        Else
                            If G.AggregatesValues.ContainsKey(FieldID.ToString("n")) Then
                                Dim AggregateValue = G.AggregatesValues(FieldID.ToString("n"))
                                If AggregateValue IsNot Nothing AndAlso Not IsDBNull(AggregateValue) Then
                                    Dim FieldFormat = ListDocFieldInfo("FieldFormat").ToString
                                    If FieldFormat = "" Then
                                        TableCellField.Controls.Add(New LiteralControl(eqView.GetAggregateTypeString(F("AggregateType")) & ": " & AggregateValue.ToString))
                                    Else
                                        TableCellField.Controls.Add(New LiteralControl(eqView.GetAggregateTypeString(F("AggregateType")) & ": " & Format(AggregateValue, FieldFormat)))
                                    End If
                                End If
                            End If
                        End If
                        TableRowGroup.Cells.Add(TableCellField)
                    Next
                    TableListDoc.Rows.Add(TableRowGroup)
                End If
                ' ProcessedRows
                ProcessedRows += 1
                ' ProcessListDocGroup
                If G.GroupExpanded Then
                    ProcessListDocGroup(G)
                End If
            Next
        ElseIf CurrentListDocGroup.ListDocRows IsNot Nothing Then
            ' Build Rows
            Dim rowIndexFrom = ViewRowsPerPage * PageIndex - ViewRowsPerPage
            'Dim rowIndexTo = Math.Min(ViewRowsPerPage * PageIndex - 1, CurrentListDocGroup.ListDocRows.Count - 1)
            Dim rowIndexTo = rowIndexFrom + ViewRowsPerPage - 1

            For i As Integer = 0 To CurrentListDocGroup.ListDocRows.Count - 1
                If ProcessedRows >= rowIndexFrom And ProcessedRows <= rowIndexTo Then
                    Dim R = CurrentListDocGroup.ListDocRows(i)
                    Dim RowIndex = ListDocDataTable.Rows.IndexOf(R)
                    Dim DocID = DirectCast(R("ID"), Guid)
                    Dim TableRowDoc As New TableRow
                    If ProcessedRows Mod 2 = 0 Then
                        TableRowDoc.CssClass = "GridItem"
                    Else
                        TableRowDoc.CssClass = "GridAlternatingItem"
                    End If
                    '' Numbering
                    'If ShowNumeration Then
                    '    Dim TableCellNumeration As New TableCell
                    '    TableCellNumeration.Text = ListDocDataTable.Rows.IndexOf(R) '  (ProcessedRows + 1).ToString
                    '    TableRowDoc.Cells.Add(TableCellNumeration)
                    'End If
                    ' Spacing
                    If ViewGrouppingFields.Count > 0 Then
                        Dim TableCellSpacing As New TableCell
                        TableCellSpacing.ColumnSpan = ViewGrouppingFields.Count
                        TableRowDoc.Cells.Add(TableCellSpacing)
                    End If
                    ' ShowNumeration
                    If ShowNumeration Then
                        Dim TableCellShowNumeration As New TableCell
                        Dim RowOrderedNum = RowIndex + 1
                        TableCellShowNumeration.Text = RowOrderedNum.ToString
                        TableRowDoc.Cells.Add(TableCellShowNumeration)
                    End If
                    ' ShowSelectBox
                    If ShowSelectBox Then
                        Dim TableCellSelectBox As New TableCell
                        Dim CheckBoxSelectBox As New CheckBox With {.ID = "SelectBox" & DocID.ToString("n") & RowIndex.ToString}
                        AddHandler CheckBoxSelectBox.CheckedChanged, AddressOf CheckBoxSelectBox_CheckedChanged
                        TableCellSelectBox.Controls.Add(CheckBoxSelectBox)
                        TableRowDoc.Cells.Add(TableCellSelectBox)
                        'TABLEGLOB.Controls.Add(TableCellSelectBox)
                        CheckBoxSelectBox.Checked = SelectedDocuments.Contains(DocID)
                    End If
                    ' SelectDoc
                    If SelectDocMode = eqBaseContainer.SelectDocModes.Single Or SelectDocMode = eqBaseContainer.SelectDocModes.SingleAndMulti Then
                        Dim TableCellSelectDoc As New TableCell
                        Dim ImageButtonSelectDoc As New ImageButton With {.ID = "SelectDoc" & DocID.ToString("n") & RowIndex.ToString, .CommandName = "SelectDoc", .CommandArgument = DocID.ToString("n")}
                        AddHandler ImageButtonSelectDoc.Command, AddressOf Button_Command
                        ImageButtonSelectDoc.ImageUrl = BasePage.GetThemeRelativeURL("Images/Close.gif")

                        If ProcessedRows < 9 Then
                            'Dim L As New LiteralControl With {.Text = "<sup>" & (ProcessedRows + 1).ToString & "</sup>"}
                            'TableCellSelectDoc.Controls.Add(L)
                            ImageButtonSelectDoc.AccessKey = (ProcessedRows + 1).ToString
                        End If

                        If Not DocType.HasCanSelectScript OrElse DirectCast(R("CanSelect"), Boolean) Then
                            TableCellSelectDoc.Controls.Add(ImageButtonSelectDoc)
                        End If

                        TableRowDoc.Cells.Add(TableCellSelectDoc)
                    End If
                    ' LoadDoc
                    If LoadDocMode <> eqBaseContainer.LoadDocModes.None And OpenDocToolBarDataRow IsNot Nothing Then
                        Dim TableCellLoadDoc As New TableCell
                        Dim ImageURL = DirectCast(OpenDocToolBarDataRow("ImageURL"), String)
                        Dim ToolTip = DirectCast(OpenDocToolBarDataRow("ToolTip"), String)
                        If _Default.NavigateMode = _Default.NavigateModes.Redirect Then
                            Dim HyperLinkLoadDoc As New HyperLink
                            HyperLinkLoadDoc.ImageUrl = BasePage.GetThemeRelativeURL(ImageURL)
                            HyperLinkLoadDoc.ToolTip = ToolTip

                            'If SelectDocMode = eqBaseContainer.SelectDocModes.None Then
                            '    If ProcessedRows < 9 Then
                            '        'Dim L As New LiteralControl With {.Text = "<sup>" & (ProcessedRows + 1).ToString & "</sup>"}
                            '        'TableCellLoadDoc.Controls.Add(L)
                            '        HyperLinkLoadDoc.AccessKey = (ProcessedRows + 1).ToString
                            '    End If
                            'End If

                            Dim OptionalQueryString = String.Empty
                            If BasePage.IsModalDialog Then
                                OptionalQueryString = "&" & eqBasePage.ConstModalDialog & "=" & BasePage.ModalDialogID & "&" & eqBasePage.ConstModalDialogSource & "=" & BasePage.ModalDialogSource
                            End If
                            HyperLinkLoadDoc.NavigateUrl = BasePage.GetDocURL(DocTypeURL, DocID, BasePage.IsModal, OptionalQueryString)
                            If MyBaseContainer IsNot Nothing Then
                                HyperLinkLoadDoc.NavigateUrl &= "&ListID=" & MyBaseContainer.ListID
                            End If

                            If LoadDocInNewWindow Then
                                HyperLinkLoadDoc.Target = "_blank"
                            End If
                            If LoadDocOnRowClick Then
                                HyperLinkLoadDoc.Enabled = False
                                If LoadDocInNewWindow Then
                                    TableRowDoc.Attributes("onclick") = String.Format("this.style.color='#aaaaaa';window.open('{0}');", HyperLinkLoadDoc.NavigateUrl)
                                Else
                                    TableRowDoc.Attributes("onclick") = String.Format("window.location='{0}';", HyperLinkLoadDoc.NavigateUrl)
                                End If
                                TableRowDoc.Style("cursor") = "pointer"
                            End If

                            TableCellLoadDoc.Controls.Add(HyperLinkLoadDoc)
                        Else
                            Dim ImageButtonLoadDoc As New ImageButton With {.ID = "LoadDoc" & DocID.ToString("n"), .CommandName = "LoadDoc", .CommandArgument = DocID.ToString("n")}
                            AddHandler ImageButtonLoadDoc.Command, AddressOf Button_Command
                            ImageButtonLoadDoc.ImageUrl = BasePage.GetThemeRelativeURL(ImageURL)
                            ImageButtonLoadDoc.ToolTip = ToolTip
                            TableCellLoadDoc.Controls.Add(ImageButtonLoadDoc)
                        End If
                        TableRowDoc.Cells.Add(TableCellLoadDoc)
                    End If
                    ' Fields
                    For Each F As DataRowView In ViewEnabledFields
                        Dim FieldID = DirectCast(F("FieldID"), Guid)
                        Dim FieldInfo = DocType.GetFieldInfo(FieldID)
                        Dim ListDocFieldInfo = DocType.GetListDocFieldInfo(FieldID)
                        Dim TableCellField As New TableCell
                        ' Fix for IE
                        If Not IsDBNull(F("Width")) Then
                            Dim Width As Unit
                            Try
                                Width = Unit.Parse(F("Width"))
                            Catch ex As Exception
                                Width = Unit.Empty
                            End Try
                            If Not Width.IsEmpty Then
                                TableCellField.Width = Width
                                TableCellField.Style("min-width") = Width.ToString
                                TableCellField.Style("max-width") = Width.ToString
                            End If
                        End If
                        ' End Fix for IE
                        Dim Value = R(FieldID.ToString("n"))
                        Dim FieldFormat = ListDocFieldInfo("FieldFormat").ToString
                        If Not ListDocFieldInfo.Table.Columns.Contains("Edit") OrElse IsDBNull(ListDocFieldInfo("Edit")) OrElse ListDocFieldInfo("Edit") = False Then
                            ' Readonly
                            If ListDocFieldInfo("DocTypeURL").ToString <> "" AndAlso ListDocFieldInfo("ShowingFields").ToString <> "" Then
                                Dim FieldDocTypeURL = ListDocFieldInfo("DocTypeURL").ToString
                                Dim FieldShowingFields = ListDocFieldInfo("ShowingFields").ToString
                                Dim ShowingFieldsFormat = ListDocFieldInfo("ShowingFieldsFormat").ToString
                                Dim FieldDocType = AppSettings.GetDocType(FieldDocTypeURL)
                                Dim FieldShowingFieldsValues As New List(Of Object)
                                For Each FieldShowingField In FieldShowingFields.Split(New Char() {";", ","})
                                    Dim FieldShowingFieldAlias = FieldID.ToString("n") & "_" & FieldDocType.GetHeaderTableName & "_" & FieldShowingField
                                    FieldShowingFieldsValues.Add(R(FieldShowingFieldAlias))
                                Next
                                If Not IsDBNull(Value) AndAlso TypeOf (Value) Is Guid Then
                                    Dim ValueGuid = DirectCast(Value, Guid)
                                    If ValueGuid <> Guid.Empty Then
                                        If AppSettings.UserSecurities.HasDocumentsAccessOpen(FieldDocType.DocTypeID) Then
                                            Dim ActionHyperLink As New HyperLink
                                            ActionHyperLink.Text = String.Format(ShowingFieldsFormat, FieldShowingFieldsValues.ToArray)
                                            ActionHyperLink.Target = "_blank"
                                            ActionHyperLink.NavigateUrl = BasePage.GetDocURL(FieldDocTypeURL, ValueGuid, True)
                                            TableCellField.Controls.Add(ActionHyperLink)
                                        Else
                                            TableCellField.Text = String.Format(ShowingFieldsFormat, FieldShowingFieldsValues.ToArray)
                                        End If
                                    End If
                                End If
                            Else
                                ' SourceTable
                                If ListDocFieldInfo("SourceTable").ToString <> "" AndAlso ListDocFieldInfo("KeyField").ToString <> "" AndAlso ListDocFieldInfo("DisplayField").ToString <> "" Then
                                    Dim SourceTable = ListDocFieldInfo("SourceTable").ToString
                                    Dim DisplayField = ListDocFieldInfo("DisplayField").ToString
                                    Dim DisplayFieldAlias = FieldID.ToString("n") & "_" & SourceTable & "_" & DisplayField
                                    Value = R(DisplayFieldAlias)
                                End If
                                ' Format output
                                If Not Value Is Nothing AndAlso Not IsDBNull(Value) Then
                                    If FieldFormat = "" Then
                                        ' No Format
                                        If TypeOf Value Is Boolean Then
                                            Dim ValueCheckBox As New CheckBox With {.Checked = Value, .Enabled = False}
                                            TableCellField.Controls.Add(ValueCheckBox)
                                        ElseIf TypeOf Value Is String Then
                                            Dim ValueStr = DirectCast(Value, String)
                                            If ValueStr.Length > 200 Then
                                                TableCellField.Controls.Add(New LiteralControl(ValueStr.Substring(0, 200) & "..."))
                                            Else
                                                TableCellField.Controls.Add(New LiteralControl(ValueStr))
                                            End If
                                        Else
                                            TableCellField.Controls.Add(New LiteralControl(Value.ToString))
                                        End If
                                    ElseIf FieldFormat = "URL" Then 'Для совместимости с предыдущим ядром (added by Anatoly Melkov on 2009-08-27)
                                        Dim mURL As New HyperLink()
                                        mURL.Target = "_blank"
                                        mURL.Text = "Открыть"
                                        mURL.NavigateUrl = Value.ToString
                                        TableCellField.Controls.Add(mURL)
                                    Else
                                        ' Format
                                        If Regex.IsMatch(FieldFormat, "{\d.*}", RegexOptions.Compiled) Then
                                            TableCellField.Controls.Add(New LiteralControl(String.Format(FieldFormat, Value)))
                                        Else
                                            TableCellField.Controls.Add(New LiteralControl(Format(Value, FieldFormat)))
                                        End If
                                    End If
                                End If
                            End If
                        Else
                            ' ValueControlId
                            Dim valueControlID = DocID.ToString("n") & "_" & FieldID.ToString("n")
                            ' Edit
                            Dim ValueStr = String.Empty
                            If Not Value Is Nothing AndAlso Not IsDBNull(Value) Then
                                If TypeOf Value Is DateTime Then
                                    Dim ValueDateTime = DirectCast(Value, DateTime)
                                    If ValueDateTime.Hour = 0 And ValueDateTime.Minute = 0 And ValueDateTime.Second = 0 Then
                                        ValueStr = ValueDateTime.ToShortDateString
                                    Else
                                        ValueStr = ValueDateTime.ToString
                                    End If
                                Else
                                    ValueStr = Value.ToString
                                End If
                            End If
                            'Dim ShemaInfo = DocType.GetShemaDataRow(DocType.GetHeaderTableName, FieldInfo("FieldName"))
                            Dim ShemaInfo = DocType.GetShemaDataRow(FieldInfo("TableName"), FieldInfo("FieldName"))
                            Dim valueControl As WebControl = Nothing
                            Dim valueCalendarExtender As AjaxControlToolkit.CalendarExtender = Nothing
                            'Dim regExpValidatorControl As RegularExpressionValidator = Nothing
                            'Dim rangeValidatorControl As RangeValidator = Nothing
                            Dim requiredValidatorControl As RequiredFieldValidator = Nothing
                            Dim required As Boolean = (FieldInfo("Required") = True)
                            If Not DocType.IsSystemField(FieldInfo("FieldName").ToString) Then
                                If FieldInfo("ValuesScript").ToString.Trim <> "" Then
                                    'valueControl = New DropDownList
                                Else
                                    Select Case ShemaInfo("TypeName")
                                        Case "decimal", "money", "numeric", "smallmoney", "float", "real"
                                            valueControl = New eqTextBox With {.Text = ValueStr}
                                            'regExpValidatorControl = New RegularExpressionValidator
                                            'regExpValidatorControl.ControlToValidate = valueControlID
                                            'regExpValidatorControl.ValidationGroup = "Edit"
                                            'regExpValidatorControl.ValidationExpression = "\d*(\.\d*)?"
                                            'regExpValidatorControl.ErrorMessage = "(Неверный формат)"
                                            'regExpValidatorControl.ForeColor = Drawing.Color.Red
                                            'regExpValidatorControl.EnableClientScript = True
                                        Case "bigint", "int", "smallint", "tinyint"
                                            valueControl = New eqTextBox With {.Text = ValueStr}
                                            'regExpValidatorControl = New RegularExpressionValidator
                                            'regExpValidatorControl.ControlToValidate = valueControlID
                                            'regExpValidatorControl.ValidationGroup = "Edit"
                                            'regExpValidatorControl.ValidationExpression = "\d*"
                                            'regExpValidatorControl.ErrorMessage = "(Неверный формат)"
                                            'regExpValidatorControl.ForeColor = Drawing.Color.Red
                                            'regExpValidatorControl.EnableClientScript = True
                                        Case "char", "varchar"
                                            Dim MaxLength As Integer = ShemaInfo("MaxLength")
                                            Dim tb As New eqTextBox With {.Text = ValueStr}
                                            If MaxLength > 0 Then
                                                tb.MaxLength = MaxLength
                                                If MaxLength > 200 Then
                                                    tb.TextMode = TextBoxMode.MultiLine
                                                    tb.Rows = MaxLength / 200
                                                End If
                                            Else
                                                tb.TextMode = TextBoxMode.MultiLine
                                                tb.Rows = 5
                                            End If
                                            valueControl = tb
                                        Case "nchar", "nvarchar"
                                            Dim MaxLength As Integer = ShemaInfo("MaxLength") / 2
                                            Dim tb As New eqTextBox With {.Text = ValueStr}
                                            If MaxLength > 0 Then
                                                tb.MaxLength = MaxLength
                                                If MaxLength > 200 Then
                                                    tb.TextMode = TextBoxMode.MultiLine
                                                    tb.Rows = MaxLength / 200
                                                End If
                                            Else
                                                tb.TextMode = TextBoxMode.MultiLine
                                                tb.Rows = 5
                                            End If
                                            valueControl = tb
                                        Case "text", "ntext"
                                            valueControl = New eqTextBox With {.Text = ValueStr, .TextMode = TextBoxMode.MultiLine, .Rows = 5}
                                        Case "uniqueidentifier"
                                            valueControl = New eqTextBox With {.Text = ValueStr}
                                            'regExpValidatorControl = New RegularExpressionValidator
                                            'regExpValidatorControl.ControlToValidate = valueControlID
                                            'regExpValidatorControl.ValidationGroup = "Edit"
                                            'regExpValidatorControl.ValidationExpression = "\d{8}-\d{4}-\d{4}-\d{4}-\d{12}"
                                            'regExpValidatorControl.ErrorMessage = "(Неверный формат)"
                                            'regExpValidatorControl.ForeColor = Drawing.Color.Red
                                            'regExpValidatorControl.EnableClientScript = True
                                        Case "date", "datetime", "smalldatetime"
                                            valueControl = New eqTextBox With {.Text = ValueStr}
                                            valueCalendarExtender = New AjaxControlToolkit.CalendarExtender
                                            valueCalendarExtender.TargetControlID = valueControlID
                                            'rangeValidatorControl = New RangeValidator
                                            'rangeValidatorControl.ControlToValidate = valueControlID
                                            'rangeValidatorControl.ValidationGroup = "Edit"
                                            'rangeValidatorControl.Type = ValidationDataType.Date
                                            'rangeValidatorControl.MaximumValue = SqlTypes.SqlDateTime.MaxValue.Value.ToString("yyyy\/MM\/dd")
                                            'rangeValidatorControl.MinimumValue = SqlTypes.SqlDateTime.MinValue.Value.ToString("yyyy\/MM\/dd")
                                            'rangeValidatorControl.ErrorMessage = "(Неверный формат)"
                                            'rangeValidatorControl.ForeColor = Drawing.Color.Red
                                            'rangeValidatorControl.EnableClientScript = True
                                        Case "bit"
                                            valueControl = New CheckBox With {.Checked = (Value IsNot Nothing AndAlso Not IsDBNull(Value) AndAlso Value = True)}
                                            required = False
                                        Case "binary", "varbinary", "image", "timestamp", "sql_variant"
                                            'vc = New eqTextBox
                                    End Select
                                End If
                            End If
                            If required Then
                                requiredValidatorControl = New RequiredFieldValidator
                                requiredValidatorControl.ControlToValidate = valueControlID
                                requiredValidatorControl.ValidationGroup = "Edit"
                                requiredValidatorControl.ErrorMessage = "(Обязательное)"
                                requiredValidatorControl.ForeColor = Drawing.Color.Red
                                requiredValidatorControl.EnableClientScript = True
                            End If
                            If valueControl IsNot Nothing Then
                                valueControl.ID = valueControlID
                                TableCellField.Controls.Add(valueControl)
                                If valueCalendarExtender IsNot Nothing Then
                                    valueCalendarExtender.ID = valueControlID & "_CalendarExtender"
                                    TableCellField.Controls.Add(valueCalendarExtender)
                                End If
                                If requiredValidatorControl IsNot Nothing Then
                                    requiredValidatorControl.ID = valueControlID & "_RequiredValidator"
                                    TableCellField.Controls.Add(requiredValidatorControl)
                                End If
                                'If regExpValidatorControl IsNot Nothing Then
                                '    regExpValidatorControl.ID = valueControlID & "_RegExpValidator"
                                '    TableCellField.Controls.Add(regExpValidatorControl)
                                'End If
                                'If rangeValidatorControl IsNot Nothing Then
                                '    rangeValidatorControl.ID = valueControlID & "_CompareValidator"
                                '    TableCellField.Controls.Add(rangeValidatorControl)
                                'End If
                                If TypeOf valueControl Is DropDownList Then
                                    Dim valueControlCasted = DirectCast(valueControl, DropDownList)
                                    valueControlCasted.AutoPostBack = True
                                    AddHandler valueControlCasted.SelectedIndexChanged, AddressOf valueControl_SelectedIndexChanged
                                ElseIf TypeOf valueControl Is eqTextBox Then
                                    Dim valueControlCasted = DirectCast(valueControl, eqTextBox)
                                    valueControlCasted.AutoPostBack = True
                                    AddHandler valueControlCasted.TextChanged, AddressOf valueControl_TextChanged
                                ElseIf TypeOf valueControl Is CheckBox Then
                                    Dim valueControlCasted = DirectCast(valueControl, CheckBox)
                                    valueControlCasted.AutoPostBack = True
                                    AddHandler valueControlCasted.CheckedChanged, AddressOf valueControl_CheckedChanged
                                End If
                            End If
                            'If ListDocFieldInfo("DocTypeURL").ToString <> "" AndAlso ListDocFieldInfo("ShowingFields").ToString <> "" Then
                            '    Dim FieldDocTypeURL = ListDocFieldInfo("DocTypeURL").ToString
                            '    Dim FieldShowingFields = ListDocFieldInfo("ShowingFields").ToString
                            '    Dim ShowingFieldsFormat = ListDocFieldInfo("ShowingFieldsFormat").ToString
                            '    Dim FieldDocType = AppSettings.GetDocType(FieldDocTypeURL)
                            '    Dim FieldShowingFieldsValues As New List(Of Object)
                            '    For Each FieldShowingField In FieldShowingFields.Split(New Char() {";", ","})
                            '        Dim FieldShowingFieldAlias = FieldID.ToString("n") & "_" & FieldDocType.GetHeaderTableName & "_" & FieldShowingField
                            '        FieldShowingFieldsValues.Add(R(FieldShowingFieldAlias))
                            '    Next
                            '    If Not IsDBNull(Value) AndAlso TypeOf (Value) Is Guid Then
                            '        Dim ValueGuid = DirectCast(Value, Guid)
                            '        If ValueGuid <> Guid.Empty Then
                            '            If AppSettings.UserSecurities.HasDocumentsAccessOpen(FieldDocType.DocTypeID) Then
                            '                Dim ActionHyperLink As New HyperLink
                            '                ActionHyperLink.Text = String.Format(ShowingFieldsFormat, FieldShowingFieldsValues.ToArray)
                            '                ActionHyperLink.Target = "_blank"
                            '                ActionHyperLink.NavigateUrl = BasePage.GetDocURL(FieldDocTypeURL, ValueGuid, True)
                            '                TableCellField.Controls.Add(ActionHyperLink)
                            '            Else
                            '                TableCellField.Text = String.Format(ShowingFieldsFormat, FieldShowingFieldsValues.ToArray)
                            '            End If
                            '        End If
                            '    End If
                            'Else
                            '    ' SourceTable
                            '    If ListDocFieldInfo("SourceTable").ToString <> "" AndAlso ListDocFieldInfo("KeyField").ToString <> "" AndAlso ListDocFieldInfo("DisplayField").ToString <> "" Then
                            '        Dim SourceTable = ListDocFieldInfo("SourceTable").ToString
                            '        Dim DisplayField = ListDocFieldInfo("DisplayField").ToString
                            '        Dim DisplayFieldAlias = FieldID.ToString("n") & "_" & SourceTable & "_" & DisplayField
                            '        Value = R(DisplayFieldAlias)
                            '    End If
                            '    ' Format output
                            '    If Not Value Is Nothing AndAlso Not IsDBNull(Value) Then
                            '        If FieldFormat = "" Then
                            '            ' No Format
                            '            If TypeOf Value Is Boolean Then
                            '                Dim ValueCheckBox As New CheckBox With {.Checked = Value, .Enabled = False}
                            '                TableCellField.Controls.Add(ValueCheckBox)
                            '            ElseIf TypeOf Value Is String Then
                            '                Dim ValueStr = DirectCast(Value, String)
                            '                If ValueStr.Length > 200 Then
                            '                    TableCellField.Controls.Add(New LiteralControl(ValueStr.Substring(0, 200) & "..."))
                            '                Else
                            '                    TableCellField.Controls.Add(New LiteralControl(ValueStr))
                            '                End If
                            '            Else
                            '                TableCellField.Controls.Add(New LiteralControl(Value.ToString))
                            '            End If
                            '        Else
                            '            ' Format
                            '            If Regex.IsMatch(FieldFormat, "{\d.*}", RegexOptions.Compiled) Then
                            '                TableCellField.Controls.Add(New LiteralControl(String.Format(FieldFormat, Value)))
                            '            Else
                            '                TableCellField.Controls.Add(New LiteralControl(Format(Value, FieldFormat)))
                            '            End If
                            '        End If
                            '    End If
                            'End If
                        End If
                        ' Painting
                        For Each RVP In ViewPainting.Where(Function(VP) VP("FieldID") = FieldID And VP("Color") <> String.Empty)
                            If RVP.Row.Table.Columns.Contains("FullRow") AndAlso RVP("FullRow") = False Then
                                Dim ID = DirectCast(RVP("ID"), Guid)
                                Dim ColorHtml = DirectCast(RVP("Color"), String)
                                Dim FieldAlias = ID.ToString("n")
                                If R.Table.Columns.Contains(FieldAlias) AndAlso R(FieldAlias) = 1 Then
                                    TableCellField.BackColor = Drawing.ColorTranslator.FromHtml(ColorHtml)
                                End If
                            End If
                        Next
                        ' Add Row
                        TableRowDoc.Cells.Add(TableCellField)
                    Next
                    ' Painting
                    For Each RVP As DataRowView In ViewPainting.Where(Function(VP) VP("Color") <> String.Empty)
                        If Not RVP.Row.Table.Columns.Contains("FullRow") OrElse RVP("FullRow") = True Then
                            Dim ID = DirectCast(RVP("ID"), Guid)
                            Dim ColorHtml = DirectCast(RVP("Color"), String)
                            Dim FieldAlias = ID.ToString("n")
                            If R.Table.Columns.Contains(FieldAlias) AndAlso R(FieldAlias) = 1 Then
                                TableRowDoc.BackColor = Drawing.ColorTranslator.FromHtml(ColorHtml)
                            End If
                        End If
                    Next
                    ' Add Row
                    TableListDoc.Rows.Add(TableRowDoc)
                End If
                ProcessedRows += 1
            Next

            'ProcessedRows += rowIndexFrom
            'Dim RowsToProcess = CurrentListDocGroup.ListDocRows.Skip(rowIndexFrom).Take(rowIndexTo - rowIndexFrom + 1)
            'For Each R In RowsToProcess

            '    ' ProcessedRows
            '    ProcessedRows += 1
            'Next
            'ProcessedRows += CurrentListDocGroup.ListDocRows.Count - 1 - rowIndexTo
        End If
    End Sub

    Private Class ListDocNode
        Public NodeLevel As Integer
        Public NodePath As String

        Public NodeExpanded As Boolean

        Public NodeCounter As Integer
        Public NodeRowsCounter As Integer
        Public NodeExpandedCounter As Integer

        Public ListDocNodes As List(Of ListDocNode)

        Public NodeListDocRow As DataRow
    End Class

    Private Function CreateListDocNode(ByVal ListDocRows As Dictionary(Of Object, IEnumerable(Of DataRow)), Optional ByVal NodeListDocRow As DataRow = Nothing, Optional ByVal NodeLevel As Integer = 0, Optional ByVal NodePath As String = "ROOT") As ListDocNode
        CreateListDocNode = New ListDocNode With {.NodeListDocRow = NodeListDocRow, .NodeLevel = NodeLevel, .NodePath = NodePath}
        ' SubExpanded
        If ExpandedGroups.ContainsKey(NodePath) Then
            If ExpandedGroupsLevelExpand > -1 AndAlso NodeLevel <= ExpandedGroupsLevelExpand Then
                CreateListDocNode.NodeExpanded = True
                ExpandedGroups(NodePath) = True
            ElseIf ExpandedGroupsLevelCollapse > -1 AndAlso NodeLevel >= ExpandedGroupsLevelCollapse Then
                CreateListDocNode.NodeExpanded = False
                ExpandedGroups(NodePath) = False
            Else
                CreateListDocNode.NodeExpanded = ExpandedGroups(NodePath)
            End If
        Else
            If NodeLevel = 0 Then
                Dim NodeExpanded = True
                ExpandedGroups.Add(NodePath, NodeExpanded)
                CreateListDocNode.NodeExpanded = NodeExpanded
            Else
                Dim NodeExpanded = False
                ExpandedGroups.Add(NodePath, NodeExpanded)
                CreateListDocNode.NodeExpanded = NodeExpanded
            End If
        End If
        If CreateListDocNode.NodeExpanded Then
            ExpandedGroupsLevel(NodeLevel) = True
        End If
        ' ListDocNodes
        Dim KeyValue As Object = Nothing
        If NodeListDocRow Is Nothing Then
            If ListDocRows.ContainsKey(DBNull.Value) Then
                KeyValue = DBNull.Value
            ElseIf ListDocRows.ContainsKey(Guid.Empty) Then
                KeyValue = Guid.Empty
            ElseIf ListDocRows.ContainsKey(0) Then
                KeyValue = 0
            End If
        Else
            KeyValue = NodeListDocRow("TreeKey")
        End If
        If KeyValue IsNot Nothing Then
            If ListDocRows.ContainsKey(KeyValue) Then
                Dim NodeListDocRows = ListDocRows(KeyValue)
                If CreateListDocNode.NodeExpanded Then
                    CreateListDocNode.ListDocNodes = New List(Of ListDocNode)
                    For Index As Integer = 0 To NodeListDocRows.Count - 1
                        Dim N = NodeListDocRows(Index)
                        ' SubListDocGroup
                        Dim SubListDocNode = CreateListDocNode(ListDocRows, N, NodeLevel + 1, NodePath & "_" & Index.ToString)
                        ' CreateListDocGroup
                        CreateListDocNode.ListDocNodes.Add(SubListDocNode)
                        ' GroupCounter
                        CreateListDocNode.NodeCounter += SubListDocNode.NodeCounter + 1
                        ' GroupExpandedCounter
                        If CreateListDocNode.NodeExpanded Then
                            'CreateListDocGroup.GroupExpandedCounter = SubListDocGroup.GroupCounter + 1
                            CreateListDocNode.NodeExpandedCounter += SubListDocNode.NodeExpandedCounter + 1
                        End If
                    Next
                End If
                ' GroupRowsCounter
                CreateListDocNode.NodeRowsCounter += NodeListDocRows.Count
            End If
        End If
    End Function

    Private Sub ProcessListDocNode(ByVal CurrentListDocNode As ListDocNode)
        If CurrentListDocNode.ListDocNodes IsNot Nothing Then
            For Each N In CurrentListDocNode.ListDocNodes
                If N.NodeExpandedCounter + ProcessedRows >= ViewRowsPerPage * PageIndex - ViewRowsPerPage AndAlso ProcessedRows < ViewRowsPerPage * PageIndex Then
                    Dim R = N.NodeListDocRow
                    Dim DocID = DirectCast(R("ID"), Guid)
                    Dim TableRowDoc As New TableRow
                    If ProcessedRows Mod 2 = 0 Then
                        TableRowDoc.CssClass = "GridItem"
                    Else
                        TableRowDoc.CssClass = "GridAlternatingItem"
                    End If
                    ' ExpandButton
                    Dim ExpandButton As New ImageButton With {.ID = "ExpandButton" & N.NodePath, .CommandName = "Expand", .CommandArgument = N.NodePath}
                    AddHandler ExpandButton.Command, AddressOf Button_Command
                    If N.NodeExpanded Then
                        ExpandButton.ImageUrl = BasePage.GetThemeRelativeURL("Images/Minus.gif")
                    Else
                        ExpandButton.ImageUrl = BasePage.GetThemeRelativeURL("Images/Plus.gif")
                    End If
                    ' Spacing
                    Dim TableCellSpacing As New TableCell
                    TableCellSpacing.Font.Size = FontUnit.Point(5)
                    TableCellSpacing.Controls.Add(New LiteralControl("<nobr>"))
                    For i = 2 To N.NodeLevel
                        Dim TreeImage As New Image
                        TreeImage.ImageAlign = ImageAlign.Top
                        TreeImage.ImageUrl = BasePage.GetThemeRelativeURL("Images/TreeSpace.gif")
                        TableCellSpacing.Controls.Add(TreeImage)
                    Next
                    If N.NodeRowsCounter > 0 Then
                        TableCellSpacing.Controls.Add(ExpandButton)
                        ExpandButton.ImageAlign = ImageAlign.Top
                        TableCellSpacing.Controls.Add(New LiteralControl(N.NodeRowsCounter))
                    End If
                    TableCellSpacing.Controls.Add(New LiteralControl("</nobr>"))
                    TableRowDoc.Cells.Add(TableCellSpacing)
                    ' ShowNumeration
                    If ShowNumeration Then
                        Dim TableCellShowNumeration As New TableCell
                        Dim RowOrderedNum = ProcessedRows + 1
                        TableCellShowNumeration.Text = RowOrderedNum.ToString
                        TableRowDoc.Cells.Add(TableCellShowNumeration)
                    End If
                    ' ShowSelectBox
                    If ShowSelectBox Then
                        Dim TableCellSelectBox As New TableCell
                        Dim CheckBoxSelectBox As New CheckBox With {.ID = "SelectBox" & DocID.ToString("n")}
                        AddHandler CheckBoxSelectBox.CheckedChanged, AddressOf CheckBoxSelectBox_CheckedChanged
                        TableCellSelectBox.Controls.Add(CheckBoxSelectBox)
                        TableRowDoc.Cells.Add(TableCellSelectBox)
                        'TABLEGLOB.Controls.Add(TableCellSelectBox)
                        CheckBoxSelectBox.Checked = SelectedDocuments.Contains(DocID)
                    End If
                    ' SelectDoc
                    If SelectDocMode = eqBaseContainer.SelectDocModes.Single Or SelectDocMode = eqBaseContainer.SelectDocModes.SingleAndMulti Then
                        Dim TableCellSelectDoc As New TableCell
                        Dim ImageButtonSelectDoc As New ImageButton With {.ID = "SelectDoc" & N.NodePath & DocID.ToString("n"), .CommandName = "SelectDoc", .CommandArgument = DocID.ToString("n")}
                        AddHandler ImageButtonSelectDoc.Command, AddressOf Button_Command
                        ImageButtonSelectDoc.ImageUrl = BasePage.GetThemeRelativeURL("Images/Close.gif")

                        If ProcessedRows < 9 Then
                            'Dim L As New LiteralControl With {.Text = "<sup>" & (ProcessedRows + 1).ToString & "</sup>"}
                            'TableCellSelectDoc.Controls.Add(L)
                            ImageButtonSelectDoc.AccessKey = (ProcessedRows + 1).ToString
                        End If

                        If Not DocType.HasCanSelectScript OrElse DirectCast(R("CanSelect"), Boolean) Then
                            TableCellSelectDoc.Controls.Add(ImageButtonSelectDoc)
                        End If
                        TableRowDoc.Cells.Add(TableCellSelectDoc)
                    End If
                    ' LoadDoc
                    If LoadDocMode <> eqBaseContainer.LoadDocModes.None And OpenDocToolBarDataRow IsNot Nothing Then
                        Dim TableCellLoadDoc As New TableCell
                        Dim ImageURL = DirectCast(OpenDocToolBarDataRow("ImageURL"), String)
                        Dim ToolTip = DirectCast(OpenDocToolBarDataRow("ToolTip"), String)
                        If _Default.NavigateMode = _Default.NavigateModes.Redirect Then
                            Dim HyperLinkLoadDoc As New HyperLink
                            HyperLinkLoadDoc.ImageUrl = BasePage.GetThemeRelativeURL(ImageURL)
                            HyperLinkLoadDoc.ToolTip = ToolTip

                            'If SelectDocMode = eqBaseContainer.SelectDocModes.None Then
                            '    If ProcessedRows < 9 Then
                            '        'Dim L As New LiteralControl With {.Text = "<sup>" & (ProcessedRows + 1).ToString & "</sup>"}
                            '        'TableCellLoadDoc.Controls.Add(L)
                            '        HyperLinkLoadDoc.AccessKey = (ProcessedRows + 1).ToString
                            '    End If
                            'End If

                            Dim OptionalQueryString = String.Empty
                            If BasePage.IsModalDialog Then
                                OptionalQueryString = "&" & eqBasePage.ConstModalDialog & "=" & BasePage.ModalDialogID & "&" & eqBasePage.ConstModalDialogSource & "=" & BasePage.ModalDialogSource
                            End If
                            HyperLinkLoadDoc.NavigateUrl = BasePage.GetDocURL(DocTypeURL, DocID, BasePage.IsModal, OptionalQueryString)
                            If MyBaseContainer IsNot Nothing Then
                                HyperLinkLoadDoc.NavigateUrl &= "&ListID=" & MyBaseContainer.ListID
                            End If

                            If LoadDocInNewWindow Then
                                HyperLinkLoadDoc.Target = "_blank"
                            End If
                            If LoadDocOnRowClick Then
                                HyperLinkLoadDoc.Enabled = False
                                If LoadDocInNewWindow Then
                                    TableRowDoc.Attributes("onclick") = String.Format("this.style.color='#aaaaaa';window.open('{0}');", HyperLinkLoadDoc.NavigateUrl)
                                Else
                                    TableRowDoc.Attributes("onclick") = String.Format("window.location='{0}';", HyperLinkLoadDoc.NavigateUrl)
                                End If
                                TableRowDoc.Style("cursor") = "pointer"
                            End If

                            TableCellLoadDoc.Controls.Add(HyperLinkLoadDoc)
                        Else
                            Dim ImageButtonLoadDoc As New ImageButton With {.ID = "LoadDoc" & DocID.ToString("n"), .CommandName = "LoadDoc", .CommandArgument = DocID.ToString("n")}
                            AddHandler ImageButtonLoadDoc.Command, AddressOf Button_Command
                            ImageButtonLoadDoc.ImageUrl = BasePage.GetThemeRelativeURL(ImageURL)
                            ImageButtonLoadDoc.ToolTip = ToolTip
                            TableCellLoadDoc.Controls.Add(ImageButtonLoadDoc)
                        End If
                        TableRowDoc.Cells.Add(TableCellLoadDoc)
                    End If
                    ' ChildDoc
                    If ChildDocToolBarDataRow IsNot Nothing Then
                        Dim TableCellChildDoc As New TableCell
                        Dim ImageURL = DirectCast(ChildDocToolBarDataRow("ImageURL"), String)
                        Dim ToolTip = DirectCast(ChildDocToolBarDataRow("ToolTip"), String)
                        If _Default.NavigateMode = _Default.NavigateModes.Redirect Then
                            Dim HyperLinkChildDoc As New HyperLink
                            HyperLinkChildDoc.ImageUrl = BasePage.GetThemeRelativeURL(ImageURL)
                            HyperLinkChildDoc.ToolTip = ToolTip
                            HyperLinkChildDoc.NavigateUrl = BasePage.GetNewDocURL(DocTypeURL, False) & "&ParentID=" & DocID.ToString
                            TableCellChildDoc.Controls.Add(HyperLinkChildDoc)
                        Else
                            Dim ImageButtonChildDoc As New ImageButton With {.ID = "ChildDoc" & DocID.ToString("n"), .CommandName = "ChildDoc", .CommandArgument = DocID.ToString("n")}
                            AddHandler ImageButtonChildDoc.Command, AddressOf Button_Command
                            ImageButtonChildDoc.ImageUrl = BasePage.GetThemeRelativeURL(ImageURL)
                            ImageButtonChildDoc.ToolTip = ToolTip
                            TableCellChildDoc.Controls.Add(ImageButtonChildDoc)
                        End If
                        TableRowDoc.Cells.Add(TableCellChildDoc)
                    End If
                    ' Fields
                    For Each F As DataRowView In ViewEnabledFields
                        Dim FieldID = DirectCast(F("FieldID"), Guid)
                        Dim FieldInfo = DocType.GetFieldInfo(FieldID)
                        Dim ListDocFieldInfo = DocType.GetListDocFieldInfo(FieldID)
                        Dim TableCellField As New TableCell
                        Dim Value = R(FieldID.ToString("n"))
                        Dim FieldFormat = ListDocFieldInfo("FieldFormat").ToString
                        If Not ListDocFieldInfo.Table.Columns.Contains("Edit") OrElse IsDBNull(ListDocFieldInfo("Edit")) OrElse ListDocFieldInfo("Edit") = False Then
                            ' Readonly
                            If ListDocFieldInfo("DocTypeURL").ToString <> "" AndAlso ListDocFieldInfo("ShowingFields").ToString <> "" Then
                                Dim FieldDocTypeURL = ListDocFieldInfo("DocTypeURL").ToString
                                Dim FieldShowingFields = ListDocFieldInfo("ShowingFields").ToString
                                Dim ShowingFieldsFormat = ListDocFieldInfo("ShowingFieldsFormat").ToString
                                Dim FieldDocType = AppSettings.GetDocType(FieldDocTypeURL)
                                Dim FieldShowingFieldsValues As New List(Of Object)
                                For Each FieldShowingField In FieldShowingFields.Split(New Char() {";", ","})
                                    Dim FieldShowingFieldAlias = FieldID.ToString("n") & "_" & FieldDocType.GetHeaderTableName & "_" & FieldShowingField
                                    FieldShowingFieldsValues.Add(R(FieldShowingFieldAlias))
                                Next
                                If Not IsDBNull(Value) AndAlso TypeOf (Value) Is Guid Then
                                    Dim ValueGuid = DirectCast(Value, Guid)
                                    If ValueGuid <> Guid.Empty Then
                                        If AppSettings.UserSecurities.HasDocumentsAccessOpen(FieldDocType.DocTypeID) Then
                                            Dim ActionHyperLink As New HyperLink
                                            ActionHyperLink.Text = String.Format(ShowingFieldsFormat, FieldShowingFieldsValues.ToArray)
                                            ActionHyperLink.Target = "_blank"
                                            ActionHyperLink.NavigateUrl = BasePage.GetDocURL(FieldDocTypeURL, ValueGuid, True)
                                            TableCellField.Controls.Add(ActionHyperLink)
                                        Else
                                            TableCellField.Text = String.Format(ShowingFieldsFormat, FieldShowingFieldsValues.ToArray)
                                        End If
                                    End If
                                End If
                            Else
                                ' SourceTable
                                If ListDocFieldInfo("SourceTable").ToString <> "" AndAlso ListDocFieldInfo("KeyField").ToString <> "" AndAlso ListDocFieldInfo("DisplayField").ToString <> "" Then
                                    Dim SourceTable = ListDocFieldInfo("SourceTable").ToString
                                    Dim DisplayField = ListDocFieldInfo("DisplayField").ToString
                                    Dim DisplayFieldAlias = FieldID.ToString("n") & "_" & SourceTable & "_" & DisplayField
                                    Value = R(DisplayFieldAlias)
                                End If
                                ' Format output
                                If Not Value Is Nothing AndAlso Not IsDBNull(Value) Then
                                    If FieldFormat = "" Then
                                        ' No Format
                                        If TypeOf Value Is Boolean Then
                                            Dim ValueCheckBox As New CheckBox With {.Checked = Value, .Enabled = False}
                                            TableCellField.Controls.Add(ValueCheckBox)
                                        ElseIf TypeOf Value Is String Then
                                            Dim ValueStr = DirectCast(Value, String)
                                            If ValueStr.Length > 200 Then
                                                TableCellField.Controls.Add(New LiteralControl(ValueStr.Substring(0, 200) & "..."))
                                            Else
                                                TableCellField.Controls.Add(New LiteralControl(ValueStr))
                                            End If
                                        Else
                                            TableCellField.Controls.Add(New LiteralControl(Value.ToString))
                                        End If
                                    ElseIf FieldFormat = "URL" Then 'Для совместимости с предыдущим ядром (added by Anatoly Melkov on 2009-08-27)
                                        Dim mURL As New HyperLink()
                                        mURL.Target = "_blank"
                                        mURL.Text = "Открыть"
                                        mURL.NavigateUrl = Value.ToString
                                        TableCellField.Controls.Add(mURL)
                                    Else
                                        ' Format
                                        If Regex.IsMatch(FieldFormat, "{\d.*}", RegexOptions.Compiled) Then
                                            TableCellField.Controls.Add(New LiteralControl(String.Format(FieldFormat, Value)))
                                        Else
                                            TableCellField.Controls.Add(New LiteralControl(Format(Value, FieldFormat)))
                                        End If
                                    End If
                                End If
                            End If
                        Else
                            ' ValueControlId
                            Dim valueControlID = DocID.ToString("n") & "_" & FieldID.ToString("n")
                            ' Edit
                            Dim ValueStr = String.Empty
                            If Not Value Is Nothing AndAlso Not IsDBNull(Value) Then
                                If TypeOf Value Is DateTime Then
                                    Dim ValueDateTime = DirectCast(Value, DateTime)
                                    If ValueDateTime.Hour = 0 And ValueDateTime.Minute = 0 And ValueDateTime.Second = 0 Then
                                        ValueStr = ValueDateTime.ToShortDateString
                                    Else
                                        ValueStr = ValueDateTime.ToString
                                    End If
                                Else
                                    ValueStr = Value.ToString
                                End If
                            End If
                            'Dim ShemaInfo = DocType.GetShemaDataRow(DocType.GetHeaderTableName, FieldInfo("FieldName"))
                            Dim ShemaInfo = DocType.GetShemaDataRow(FieldInfo("TableName"), FieldInfo("FieldName"))
                            Dim valueControl As WebControl = Nothing
                            Dim valueCalendarExtender As AjaxControlToolkit.CalendarExtender = Nothing
                            'Dim regExpValidatorControl As RegularExpressionValidator = Nothing
                            'Dim rangeValidatorControl As RangeValidator = Nothing
                            Dim requiredValidatorControl As RequiredFieldValidator = Nothing
                            Dim required As Boolean = (FieldInfo("Required") = True)
                            If Not DocType.IsSystemField(FieldInfo("FieldName").ToString) Then
                                If FieldInfo("ValuesScript").ToString.Trim <> "" Then
                                    'valueControl = New DropDownList
                                Else
                                    Select Case ShemaInfo("TypeName")
                                        Case "decimal", "money", "numeric", "smallmoney", "float", "real"
                                            valueControl = New eqTextBox With {.Text = ValueStr}
                                            'regExpValidatorControl = New RegularExpressionValidator
                                            'regExpValidatorControl.ControlToValidate = valueControlID
                                            'regExpValidatorControl.ValidationGroup = "Edit"
                                            'regExpValidatorControl.ValidationExpression = "\d*(\.\d*)?"
                                            'regExpValidatorControl.ErrorMessage = "(Неверный формат)"
                                            'regExpValidatorControl.ForeColor = Drawing.Color.Red
                                            'regExpValidatorControl.EnableClientScript = True
                                        Case "bigint", "int", "smallint", "tinyint"
                                            valueControl = New eqTextBox With {.Text = ValueStr}
                                            'regExpValidatorControl = New RegularExpressionValidator
                                            'regExpValidatorControl.ControlToValidate = valueControlID
                                            'regExpValidatorControl.ValidationGroup = "Edit"
                                            'regExpValidatorControl.ValidationExpression = "\d*"
                                            'regExpValidatorControl.ErrorMessage = "(Неверный формат)"
                                            'regExpValidatorControl.ForeColor = Drawing.Color.Red
                                            'regExpValidatorControl.EnableClientScript = True
                                        Case "char", "varchar"
                                            Dim MaxLength As Integer = ShemaInfo("MaxLength")
                                            Dim tb As New eqTextBox With {.Text = ValueStr}
                                            If MaxLength > 0 Then
                                                tb.MaxLength = MaxLength
                                                If MaxLength > 200 Then
                                                    tb.TextMode = TextBoxMode.MultiLine
                                                    tb.Rows = MaxLength / 200
                                                End If
                                            Else
                                                tb.TextMode = TextBoxMode.MultiLine
                                                tb.Rows = 5
                                            End If
                                            valueControl = tb
                                        Case "nchar", "nvarchar"
                                            Dim MaxLength As Integer = ShemaInfo("MaxLength") / 2
                                            Dim tb As New eqTextBox With {.Text = ValueStr}
                                            If MaxLength > 0 Then
                                                tb.MaxLength = MaxLength
                                                If MaxLength > 200 Then
                                                    tb.TextMode = TextBoxMode.MultiLine
                                                    tb.Rows = MaxLength / 200
                                                End If
                                            Else
                                                tb.TextMode = TextBoxMode.MultiLine
                                                tb.Rows = 5
                                            End If
                                            valueControl = tb
                                        Case "text", "ntext"
                                            valueControl = New eqTextBox With {.Text = ValueStr, .TextMode = TextBoxMode.MultiLine, .Rows = 5}
                                        Case "uniqueidentifier"
                                            valueControl = New eqTextBox With {.Text = ValueStr}
                                            'regExpValidatorControl = New RegularExpressionValidator
                                            'regExpValidatorControl.ControlToValidate = valueControlID
                                            'regExpValidatorControl.ValidationGroup = "Edit"
                                            'regExpValidatorControl.ValidationExpression = "\d{8}-\d{4}-\d{4}-\d{4}-\d{12}"
                                            'regExpValidatorControl.ErrorMessage = "(Неверный формат)"
                                            'regExpValidatorControl.ForeColor = Drawing.Color.Red
                                            'regExpValidatorControl.EnableClientScript = True
                                        Case "date", "datetime", "smalldatetime"
                                            valueControl = New eqTextBox With {.Text = ValueStr}
                                            valueCalendarExtender = New AjaxControlToolkit.CalendarExtender
                                            valueCalendarExtender.TargetControlID = valueControlID
                                            'rangeValidatorControl = New RangeValidator
                                            'rangeValidatorControl.ControlToValidate = valueControlID
                                            'rangeValidatorControl.ValidationGroup = "Edit"
                                            'rangeValidatorControl.Type = ValidationDataType.Date
                                            'rangeValidatorControl.MaximumValue = SqlTypes.SqlDateTime.MaxValue.Value.ToString("yyyy\/MM\/dd")
                                            'rangeValidatorControl.MinimumValue = SqlTypes.SqlDateTime.MinValue.Value.ToString("yyyy\/MM\/dd")
                                            'rangeValidatorControl.ErrorMessage = "(Неверный формат)"
                                            'rangeValidatorControl.ForeColor = Drawing.Color.Red
                                            'rangeValidatorControl.EnableClientScript = True
                                        Case "bit"
                                            valueControl = New CheckBox With {.Checked = (Value IsNot Nothing AndAlso Not IsDBNull(Value) AndAlso Value = True)}
                                            required = False
                                        Case "binary", "varbinary", "image", "timestamp", "sql_variant"
                                            'vc = New eqTextBox
                                    End Select
                                End If
                            End If
                            If required Then
                                requiredValidatorControl = New RequiredFieldValidator
                                requiredValidatorControl.ControlToValidate = valueControlID
                                requiredValidatorControl.ValidationGroup = "Edit"
                                requiredValidatorControl.ErrorMessage = "(Обязательное)"
                                requiredValidatorControl.ForeColor = Drawing.Color.Red
                                requiredValidatorControl.EnableClientScript = True
                            End If
                            If valueControl IsNot Nothing Then
                                valueControl.ID = valueControlID
                                TableCellField.Controls.Add(valueControl)
                                If valueCalendarExtender IsNot Nothing Then
                                    valueCalendarExtender.ID = valueControlID & "_CalendarExtender"
                                    TableCellField.Controls.Add(valueCalendarExtender)
                                End If
                                If requiredValidatorControl IsNot Nothing Then
                                    requiredValidatorControl.ID = valueControlID & "_RequiredValidator"
                                    TableCellField.Controls.Add(requiredValidatorControl)
                                End If
                                'If regExpValidatorControl IsNot Nothing Then
                                '    regExpValidatorControl.ID = valueControlID & "_RegExpValidator"
                                '    TableCellField.Controls.Add(regExpValidatorControl)
                                'End If
                                'If rangeValidatorControl IsNot Nothing Then
                                '    rangeValidatorControl.ID = valueControlID & "_CompareValidator"
                                '    TableCellField.Controls.Add(rangeValidatorControl)
                                'End If
                                If TypeOf valueControl Is DropDownList Then
                                    Dim valueControlCasted = DirectCast(valueControl, DropDownList)
                                    valueControlCasted.AutoPostBack = True
                                    AddHandler valueControlCasted.SelectedIndexChanged, AddressOf valueControl_SelectedIndexChanged
                                ElseIf TypeOf valueControl Is eqTextBox Then
                                    Dim valueControlCasted = DirectCast(valueControl, eqTextBox)
                                    valueControlCasted.AutoPostBack = True
                                    AddHandler valueControlCasted.TextChanged, AddressOf valueControl_TextChanged
                                ElseIf TypeOf valueControl Is CheckBox Then
                                    Dim valueControlCasted = DirectCast(valueControl, CheckBox)
                                    valueControlCasted.AutoPostBack = True
                                    AddHandler valueControlCasted.CheckedChanged, AddressOf valueControl_CheckedChanged
                                End If
                            End If
                            'If ListDocFieldInfo("DocTypeURL").ToString <> "" AndAlso ListDocFieldInfo("ShowingFields").ToString <> "" Then
                            '    Dim FieldDocTypeURL = ListDocFieldInfo("DocTypeURL").ToString
                            '    Dim FieldShowingFields = ListDocFieldInfo("ShowingFields").ToString
                            '    Dim ShowingFieldsFormat = ListDocFieldInfo("ShowingFieldsFormat").ToString
                            '    Dim FieldDocType = AppSettings.GetDocType(FieldDocTypeURL)
                            '    Dim FieldShowingFieldsValues As New List(Of Object)
                            '    For Each FieldShowingField In FieldShowingFields.Split(New Char() {";", ","})
                            '        Dim FieldShowingFieldAlias = FieldID.ToString("n") & "_" & FieldDocType.GetHeaderTableName & "_" & FieldShowingField
                            '        FieldShowingFieldsValues.Add(R(FieldShowingFieldAlias))
                            '    Next
                            '    If Not IsDBNull(Value) AndAlso TypeOf (Value) Is Guid Then
                            '        Dim ValueGuid = DirectCast(Value, Guid)
                            '        If ValueGuid <> Guid.Empty Then
                            '            If AppSettings.UserSecurities.HasDocumentsAccessOpen(FieldDocType.DocTypeID) Then
                            '                Dim ActionHyperLink As New HyperLink
                            '                ActionHyperLink.Text = String.Format(ShowingFieldsFormat, FieldShowingFieldsValues.ToArray)
                            '                ActionHyperLink.Target = "_blank"
                            '                ActionHyperLink.NavigateUrl = BasePage.GetDocURL(FieldDocTypeURL, ValueGuid, True)
                            '                TableCellField.Controls.Add(ActionHyperLink)
                            '            Else
                            '                TableCellField.Text = String.Format(ShowingFieldsFormat, FieldShowingFieldsValues.ToArray)
                            '            End If
                            '        End If
                            '    End If
                            'Else
                            '    ' SourceTable
                            '    If ListDocFieldInfo("SourceTable").ToString <> "" AndAlso ListDocFieldInfo("KeyField").ToString <> "" AndAlso ListDocFieldInfo("DisplayField").ToString <> "" Then
                            '        Dim SourceTable = ListDocFieldInfo("SourceTable").ToString
                            '        Dim DisplayField = ListDocFieldInfo("DisplayField").ToString
                            '        Dim DisplayFieldAlias = FieldID.ToString("n") & "_" & SourceTable & "_" & DisplayField
                            '        Value = R(DisplayFieldAlias)
                            '    End If
                            '    ' Format output
                            '    If Not Value Is Nothing AndAlso Not IsDBNull(Value) Then
                            '        If FieldFormat = "" Then
                            '            ' No Format
                            '            If TypeOf Value Is Boolean Then
                            '                Dim ValueCheckBox As New CheckBox With {.Checked = Value, .Enabled = False}
                            '                TableCellField.Controls.Add(ValueCheckBox)
                            '            ElseIf TypeOf Value Is String Then
                            '                Dim ValueStr = DirectCast(Value, String)
                            '                If ValueStr.Length > 200 Then
                            '                    TableCellField.Controls.Add(New LiteralControl(ValueStr.Substring(0, 200) & "..."))
                            '                Else
                            '                    TableCellField.Controls.Add(New LiteralControl(ValueStr))
                            '                End If
                            '            Else
                            '                TableCellField.Controls.Add(New LiteralControl(Value.ToString))
                            '            End If
                            '        Else
                            '            ' Format
                            '            If Regex.IsMatch(FieldFormat, "{\d.*}", RegexOptions.Compiled) Then
                            '                TableCellField.Controls.Add(New LiteralControl(String.Format(FieldFormat, Value)))
                            '            Else
                            '                TableCellField.Controls.Add(New LiteralControl(Format(Value, FieldFormat)))
                            '            End If
                            '        End If
                            '    End If
                            'End If
                        End If
                        ' Painting
                        For Each RVP In ViewPainting.Where(Function(VP) VP("FieldID") = FieldID And VP("Color") <> String.Empty)
                            If RVP.Row.Table.Columns.Contains("FullRow") AndAlso RVP("FullRow") = False Then
                                Dim ID = DirectCast(RVP("ID"), Guid)
                                Dim ColorHtml = DirectCast(RVP("Color"), String)
                                Dim FieldAlias = ID.ToString("n")
                                If R.Table.Columns.Contains(FieldAlias) AndAlso R(FieldAlias) = 1 Then
                                    TableCellField.BackColor = Drawing.ColorTranslator.FromHtml(ColorHtml)
                                End If
                            End If
                        Next
                        ' Add Row
                        TableRowDoc.Cells.Add(TableCellField)
                    Next
                    ' Painting
                    For Each RVP As DataRowView In ViewPainting.Where(Function(VP) VP("Color") <> String.Empty)
                        If Not RVP.Row.Table.Columns.Contains("FullRow") OrElse RVP("FullRow") = True Then
                            Dim ID = DirectCast(RVP("ID"), Guid)
                            Dim ColorHtml = DirectCast(RVP("Color"), String)
                            Dim FieldAlias = ID.ToString("n")
                            If R.Table.Columns.Contains(FieldAlias) AndAlso R(FieldAlias) = 1 Then
                                TableRowDoc.BackColor = Drawing.ColorTranslator.FromHtml(ColorHtml)
                            End If
                        End If
                    Next
                    ' Add Row
                    TableListDoc.Rows.Add(TableRowDoc)
                End If
                ' ProcessedRows
                ProcessedRows += 1
                ' ProcessListDocGroup
                If N.NodeExpanded Then
                    ProcessListDocNode(N)
                End If
            Next
        End If
    End Sub


#Region "Edit"

    Private Sub valueControl_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim valueControl = DirectCast(sender, DropDownList)
        Dim valueControlID = valueControl.ID.Split("_")
        Dim DocID = New Guid(valueControlID(0))
        Dim FieldID = New Guid(valueControlID(1))
        ' SetValue
        Dim TempDataSet As New DataSet
        Dim FieldInfo = DocType.GetFieldInfo(FieldID)
        If DocType.LoadDoc(DocID, TempDataSet) Then
            Dim DataSetHeaderDataRow = TempDataSet.Tables(0).Rows(0)
            Dim FieldName = FieldInfo("FieldName").ToString
            DataSetHeaderDataRow(FieldName) = TranslateValueToDataSetType(GetValueFromControl(valueControl), FieldInfo)
            DocType.SaveDoc(TempDataSet)
        Else
            ' Невозможно загрузить документ
        End If
    End Sub

    Private Sub valueControl_TextChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim valueControl = DirectCast(sender, eqTextBox)
        Dim valueControlID = valueControl.ID.Split("_")
        Dim DocID = New Guid(valueControlID(0))
        Dim FieldID = New Guid(valueControlID(1))
        ' SetValue
        Dim TempDataSet As New DataSet
        Dim FieldInfo = DocType.GetFieldInfo(FieldID)
        If DocType.LoadDoc(DocID, TempDataSet) Then
            Dim DataSetHeaderDataRow = TempDataSet.Tables(0).Rows(0)
            Dim FieldName = FieldInfo("FieldName").ToString
            DataSetHeaderDataRow(FieldName) = TranslateValueToDataSetType(GetValueFromControl(valueControl), FieldInfo)
            DocType.SaveDoc(TempDataSet)
        Else
            ' Невозможно загрузить документ
        End If
    End Sub

    Private Sub valueControl_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim valueControl = DirectCast(sender, CheckBox)
        Dim valueControlID = valueControl.ID.Split("_")
        Dim DocID = New Guid(valueControlID(0))
        Dim FieldID = New Guid(valueControlID(1))
        ' SetValue
        Dim TempDataSet As New DataSet
        Dim FieldInfo = DocType.GetFieldInfo(FieldID)
        If DocType.LoadDoc(DocID, TempDataSet) Then
            Dim DataSetHeaderDataRow = TempDataSet.Tables(0).Rows(0)
            Dim FieldName = FieldInfo("FieldName").ToString
            DataSetHeaderDataRow(FieldName) = TranslateValueToDataSetType(GetValueFromControl(valueControl), FieldInfo)
            DocType.SaveDoc(TempDataSet)
        Else
            ' Невозможно загрузить документ
        End If
    End Sub

#Region "Скопированно с eqBaseDoc"

    Public Overridable Function TranslateValueToDataSetType(ByVal Value As Object, ByVal FieldInfo As DataRow) As Object
        Dim ShemaDataRow As DataRow = DocType.GetShemaDataRow(FieldInfo("TableName"), FieldInfo("FieldName"))
        If ShemaDataRow Is Nothing Then
            Throw New Exception("Колонки: " & FieldInfo("FieldName") & " принадлежащей таблице: " & FieldInfo("TableName") & " в базе данных не найдено. Сматри в ЭсКуЭль или убей себя ап стену! ;)")
        Else
            If IsDBNull(Value) _
                OrElse Value Is Nothing _
                OrElse Value.ToString = "" _
                OrElse Value.ToString = Double.PositiveInfinity.ToString _
                OrElse Value.ToString = Double.NegativeInfinity.ToString _
                OrElse Value.ToString = Double.NaN.ToString _
                OrElse Value.ToString = DateTime.MinValue.ToString _
            Then
                ' Case "SQL Server type" ' .NET Framework type
                Select Case ShemaDataRow("TypeName")
                    Case "char", "varchar", "nchar", "nvarchar", "text", "ntext" ' String
                        If Not ShemaDataRow("IsNullable") Then
                            Return String.Empty
                        Else
                            Return DBNull.Value
                        End If
                    Case Else
                        Return DBNull.Value
                End Select
            Else
                Select Case ShemaDataRow("TypeName")
                    ' Case "SQL Server type" ' .NET Framework type
                    Case "bigint" ' Int64
                        Return CType(Value, Int64)
                    Case "binary", "varbinary", "image", "timestamp"  ' Byte()
                        Return CType(Value, Byte())
                    Case "bit" ' Boolean
                        Return CType(Value, Boolean)
                    Case "char", "varchar" ' String
                        Return Value.ToString
                    Case "nchar", "nvarchar" ' String
                        Return Value.ToString
                    Case "text", "ntext" ' String
                        Return Value.ToString
                    Case "date" ' Date
                        Return CType(Value, DateTime)
                    Case "datetime" ' DateTime
                        Return CType(Value, DateTime)
                    Case "smalldatetime" ' DateTime
                        Return CType(Value, DateTime)
                    Case "decimal", "money", "numeric", "smallmoney" ' Decimal
                        Return CType(Value, Decimal)
                    Case "float" ' Double
                        Return CType(Value, Double)
                    Case "int" ' Int32
                        Return CType(Value, Int32)
                    Case "real" ' Single
                        Return CType(Value, Single)
                    Case "smallint" ' Int16
                        Return CType(Value, Int16)
                    Case "sql_variant" ' Object
                        Return CType(Value, Object)
                    Case "tinyint" ' Byte
                        Return CType(Value, Byte)
                    Case "uniqueidentifier" ' Guid
                        If TypeOf Value Is String Then
                            Return New Guid(Value.ToString)
                        Else
                            Return CType(Value, Guid)
                        End If
                    Case Else
                        Throw New Exception("Нет поддержки типа данных " & ShemaDataRow("TypeName"))
                End Select
            End If
        End If
    End Function

    Public Overridable Function GetValueFromControl(ByVal SourceControl As Web.UI.Control) As Object
        If TypeOf SourceControl Is eqBaseContainer Then
            Dim SourceBaseContainer = DirectCast(SourceControl, eqBaseContainer)
            If SourceBaseContainer.DocID = Guid.Empty Then
                Return Nothing
            Else
                Return SourceBaseContainer.DocID
            End If
        Else
            Dim PropertyName As String = GetControlValueProperty(SourceControl)
            If PropertyName Is Nothing Then
                Throw New Exception("Неизвестный тип " & SourceControl.GetType.FullName & " контрола " & SourceControl.ID)
            Else
                Dim ControlProperty As System.Reflection.PropertyInfo = SourceControl.GetType.GetProperty(PropertyName)
                If ControlProperty Is Nothing Then
                    Throw New Exception("Не найдено Property " & PropertyName & " у контрола " & SourceControl.GetType.FullName)
                Else
                    Return ControlProperty.GetValue(SourceControl, Nothing)
                End If
            End If
        End If
    End Function

    ''' <summary>
    ''' Получить проперти значения контрола
    ''' </summary>
    ''' <param name="Control">Контрол</param>
    ''' <returns>Имя проперти</returns>
    Public Overridable Function GetControlValueProperty(ByVal Control As Web.UI.Control) As String
        Select Case Control.GetType.FullName
            Case "System.Web.UI.WebControls.Label"
                Return "Text"
            Case "System.Web.UI.WebControls.CheckBox"
                Return "Checked"
            Case "System.Web.UI.WebControls.RadioButton"
                Return "Checked"
            Case "System.Web.UI.WebControls.TextBox"
                Return "Text"
            Case "System.Web.UI.WebControls.DropDownList"
                Return "SelectedValue"
            Case "System.Web.UI.WebControls.RadioButtonList"
                Return "SelectedValue"
            Case "System.Web.UI.WebControls.HyperLink"
                Return "NavigateUrl"
            Case "System.Web.UI.WebControls.HiddenField"
                Return "Value"
            Case "System.Web.UI.WebControls.Calendar" '2009-09-21
                Return "SelectedDate"
            Case "Infragistics.WebUI.WebDataInput.WebDateTimeEdit"
                Return "Value"
            Case "Infragistics.WebUI.WebSchedule.WebDateChooser"
                Return "Value"
            Case "Infragistics.WebUI.WebCombo.WebCombo"
                Return "DataValue"
            Case "Infragistics.WebUI.WebDataInput.WebNumericEdit"
                Return "Value"
            Case "Infragistics.WebUI.WebDataInput.WebMaskEdit"
                Return "Value"
            Case "C1.Web.C1Input.C1WebNumericEdit"
                Return "Value"
            Case "C1.Web.C1Input.C1WebCurrencyEdit"
                Return "Value"
            Case "C1.Web.C1Input.C1WebDateEdit"
                Return "Date"
            Case "C1.Web.C1Input.C1WebMaskEdit"
                Return "Text"
            Case "C1.Web.C1Input.C1WebPercentEdit"
                Return "Value"
            Case "Equipage.WebUI.eqAttachedFile"
                Return "Value"
            Case "Equipage.WebUI.eqHyperLink"
                Return "Value"
            Case "Equipage.WebUI.eqDocument"
                Return "DocID"
            Case "Equipage.WebUI.eqDropDownList"
                Return "SelectedValue"
            Case "Equipage.WebUI.eqDropDownEconom"
                Return "Value"
            Case "Equipage.WebUI.eqTextBox"
                Return "Text"
            Case "Equipage.WebUI.eqColorPicker"
                Return "SelectedColorHtml"
            Case "Equipage.WebUI.eqTimeEdit"
                Return "Value"
            Case "AjaxControlToolkit.CalendarExtender"
                Return "SelectedDate"
            Case Else
                Return Nothing
        End Select
    End Function

#End Region

#End Region

    Private TableBuilded As Boolean
    Private ProcessedRows As Integer

    Public Property DocCommandMode() As eqBaseContainer.DocCommandModes
        Get
            If ViewState("DocCommandMode") Is Nothing Then
                If BaseContainer Is Nothing Then
                    Return eqBaseContainer.DocCommandModes.None
                Else
                    Return BaseContainer.DocCommandMode
                End If
            Else
                Return ViewState("DocCommandMode")
            End If
        End Get
        Set(ByVal value As eqBaseContainer.DocCommandModes)
            ViewState("DocCommandMode") = value
        End Set
    End Property

    Public Property DeleteDocMode() As eqBaseContainer.DeleteDocModes
        Get
            If ViewState("DeleteDocMode") Is Nothing Then
                If BaseContainer Is Nothing Then
                    Return eqBaseContainer.DeleteDocModes.None
                Else
                    Return BaseContainer.DeleteDocMode
                End If
            Else
                Return ViewState("DeleteDocMode")
            End If
        End Get
        Set(ByVal value As eqBaseContainer.DeleteDocModes)
            ViewState("DeleteDocMode") = value
        End Set
    End Property

    Public Property SelectDocMode() As eqBaseContainer.SelectDocModes
        Get
            If ViewState("SelectDocMode") Is Nothing Then
                If BaseContainer Is Nothing Then
                    Return eqBaseContainer.SelectDocModes.None
                Else
                    Return BaseContainer.SelectDocMode
                End If
            Else
                Return ViewState("SelectDocMode")
            End If
        End Get
        Set(ByVal value As eqBaseContainer.SelectDocModes)
            ViewState("SelectDocMode") = value
        End Set
    End Property

    Public Property LoadDocMode() As eqBaseContainer.LoadDocModes
        Get
            If ViewState("LoadDocMode") Is Nothing Then
                If BaseContainer Is Nothing Then
                    Return eqBaseContainer.LoadDocModes.None
                Else
                    Return BaseContainer.LoadDocMode
                End If
            Else
                Return ViewState("LoadDocMode")
            End If
        End Get
        Set(ByVal value As eqBaseContainer.LoadDocModes)
            ViewState("LoadDocMode") = value
        End Set
    End Property

    Private _OpenDocToolBarDataRow As DataRow = Nothing
    Private _OpenDocToolBarDataRowRetrived As Boolean = False
    Public ReadOnly Property OpenDocToolBarDataRow() As DataRow
        Get
            If Not _OpenDocToolBarDataRowRetrived Then
                If AppSettings.UserSecurities.HasDocumentsAccessOpen(DocType.DocTypeID) Then
                    _OpenDocToolBarDataRow = DocType.ToolBarRows.FirstOrDefault(Function(R) R("CommandName") = "LoadDoc")
                    If _OpenDocToolBarDataRow IsNot Nothing Then
                        Dim IDButton = DirectCast(_OpenDocToolBarDataRow("ID"), Guid)
                        If AppSettings.UserSecurities.HasToolBarDeniedList(DocType.DocTypeID, IDButton) Then
                            _OpenDocToolBarDataRow = Nothing
                        End If
                    End If
                    _OpenDocToolBarDataRowRetrived = True
                End If
            End If
            Return _OpenDocToolBarDataRow
        End Get
    End Property

    Private _ChildDocToolBarDataRow As DataRow = Nothing
    Private _ChildDocToolBarDataRowRetrived As Boolean = False
    Public ReadOnly Property ChildDocToolBarDataRow() As DataRow
        Get
            If Not _ChildDocToolBarDataRowRetrived Then
                If AppSettings.UserSecurities.HasDocumentsAccessOpen(DocType.DocTypeID) Then
                    _ChildDocToolBarDataRow = DocType.ToolBarRows.FirstOrDefault(Function(R) R("CommandName") = "ChildDoc")
                    If _ChildDocToolBarDataRow IsNot Nothing Then
                        Dim IDButton = DirectCast(_ChildDocToolBarDataRow("ID"), Guid)
                        If AppSettings.UserSecurities.HasToolBarDeniedList(DocType.DocTypeID, IDButton) Then
                            _ChildDocToolBarDataRow = Nothing
                        End If
                    End If
                    _ChildDocToolBarDataRowRetrived = True
                End If
            End If
            Return _ChildDocToolBarDataRow
        End Get
    End Property

    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        MyBase.OnLoad(e)
    End Sub

    Private ReadOnly Property ShowSelectBox() As Boolean
        Get
            ShowSelectBox = False
            ShowSelectBox = ShowSelectBox Or SelectDocMode = eqBaseContainer.SelectDocModes.Event
            ShowSelectBox = ShowSelectBox Or SelectDocMode = eqBaseContainer.SelectDocModes.Multi
            ShowSelectBox = ShowSelectBox Or SelectDocMode = eqBaseContainer.SelectDocModes.SingleAndMulti
            ' 2009.09.07 EP - 4-е строки ниже включил обратно, х.з. под какой травой отключал...
            ShowSelectBox = ShowSelectBox Or DeleteDocMode = eqBaseContainer.DeleteDocModes.Multi
            ShowSelectBox = ShowSelectBox Or DeleteDocMode = eqBaseContainer.DeleteDocModes.SingleAndMulti
            ShowSelectBox = ShowSelectBox Or DocCommandMode = eqBaseContainer.DocCommandModes.Multi
            ShowSelectBox = ShowSelectBox Or DocCommandMode = eqBaseContainer.DocCommandModes.SingleAndMulti
            If BaseContainer IsNot Nothing Then
                ' Показывать чекбоксы, только если они разрешены в контейнере
                Return ShowSelectBox And BaseContainer.ShowSelectBox
            End If
        End Get
    End Property

    Private Sub BuildTable()
        ExpandedGroupsLevel.Clear()

        Dim TreeMode As Boolean = DocType.HasTreeSupport

        Dim FirstListDocGroup As ListDocGroup = Nothing
        If Not TreeMode AndAlso ListDocDataTable IsNot Nothing Then
            Dim ListDocRows = ListDocDataTable.Rows.OfType(Of DataRow)()
            FirstListDocGroup = CreateListDocGroup(ListDocRows)
        End If
        Dim FirstListDocNode As ListDocNode = Nothing
        If TreeMode AndAlso ListDocDataTable IsNot Nothing Then
            Dim ListDocRows = ListDocDataTable.Rows.OfType(Of DataRow)()
            Dim GrouppedRows = From R In ListDocRows Group R By ParentKey = R("TreeParentKey") Into Group
            Dim GrouppedRowsDict = GrouppedRows.ToDictionary(Function(G) G.ParentKey, Function(G) G.Group)
            FirstListDocNode = CreateListDocNode(GrouppedRowsDict)
        End If

        BuildFunctionsTable()

        TableListDoc.Rows.Clear()
        ProcessedRows = 0

        ' Paging
        Dim TableRowPaging As New TableRow With {.CssClass = "GridPaging"}
        Dim TableCellPaging As New TableCell
        Dim TableRowPaging2 As New TableRow With {.CssClass = "GridPaging"}
        Dim TableCellPaging2 As New TableCell
        ' ColumnSpan
        If TreeMode Then
            TableCellPaging.ColumnSpan += 1
            If ShowNumeration Then
                TableCellPaging.ColumnSpan += 1
            End If
        Else
            If ViewGrouppingFields.Count > 0 Then
                TableCellPaging.ColumnSpan += ViewGrouppingFields.Count
            End If
            If ShowNumeration Then
                TableCellPaging.ColumnSpan += 1
            End If
        End If
        If ShowSelectBox Then
            TableCellPaging.ColumnSpan += 1
        End If
        If SelectDocMode <> eqBaseContainer.SelectDocModes.None Then
            TableCellPaging.ColumnSpan += 1
        End If
        If LoadDocMode <> eqBaseContainer.LoadDocModes.None And OpenDocToolBarDataRow IsNot Nothing Then
            TableCellPaging.ColumnSpan += 1
        End If
        If ChildDocToolBarDataRow IsNot Nothing Then
            TableCellPaging.ColumnSpan += 1
        End If
        TableCellPaging.ColumnSpan += ViewEnabledFields.Count()
        TableRowPaging.Cells.Add(TableCellPaging)
        TableListDoc.Rows.Add(TableRowPaging)
        TableCellPaging2.ColumnSpan = TableCellPaging.ColumnSpan
        TableRowPaging2.Cells.Add(TableCellPaging2)

        ' Header
        Dim TableRowHeader As New TableRow With {.CssClass = "GridHeader"}

        Dim TableCellSpacing As New TableCell

        'Добавление и подписка на функцию Checket() для галочки Выбрать все. 01.12.2020 Саламан Андрей
        Dim countt As Integer = 0
        If ShowSelectBox Then 'gg1
            Dim TB As New TableCell
            Dim CB As New CheckBox With {.ID = "SelectBox" & "All"}
            CB.Text = "Выбрать всё"
            TableCellSpacing.Controls.Add(CB)
            TableCellSpacing.Controls.Add(TB)
            CB.Checked = False

            If (countt = 0) Then
                ExecuteScript(String.Format("$('input#ctl00_Body_Container_ListDoc_SelectBoxAll').attr('onclick','Checket(this)');"), "Checket", True)
            End If
            countt = countt + 1
        End If


        ' Spacing
        If TreeMode Then
            TableCellSpacing.ColumnSpan += 1
            TableCellSpacing.Controls.Add(New LiteralControl("<nobr>"))
            Dim ImageButtonCollapse As New ImageButton With {.ID = "ibgCollapse"}
            ImageButtonCollapse.ImageUrl = BasePage.GetThemeRelativeURL("Images/TreeDelete.gif")
            ImageButtonCollapse.CommandName = "CollapseGroup"
            ImageButtonCollapse.CommandArgument = 1.ToString
            AddHandler ImageButtonCollapse.Command, AddressOf Button_Command
            TableCellSpacing.Controls.Add(ImageButtonCollapse)
            Dim ImageButtonExpand As New ImageButton With {.ID = "ibgExpand"}
            ImageButtonExpand.ImageUrl = BasePage.GetThemeRelativeURL("Images/TreeAdd.gif")
            ImageButtonExpand.CommandName = "ExpandGroup"
            ImageButtonExpand.CommandArgument = 10.ToString
            AddHandler ImageButtonExpand.Command, AddressOf Button_Command
            TableCellSpacing.Controls.Add(ImageButtonExpand)
            TableCellSpacing.Controls.Add(New LiteralControl("</nobr>"))
            ' Numeration
            If ShowNumeration Then
                TableCellSpacing.ColumnSpan += 1
            End If
        Else
            ' Groupping
            For i As Integer = 1 To ViewGrouppingFields.Count
                Dim TableCellGroupping As New TableCell
                TableCellGroupping.VerticalAlign = VerticalAlign.Bottom
                Dim ImageButtonGroupping As New ImageButton With {.ID = "ibg" & i.ToString}

                If FirstListDocGroup IsNot Nothing Then
                    Dim HasExpanded As Boolean = True
                    For ig As Integer = 1 To i
                        HasExpanded = HasExpanded AndAlso ExpandedGroupsLevel.ContainsKey(ig)
                    Next
                    If HasExpanded Then
                        ImageButtonGroupping.ImageUrl = BasePage.GetThemeRelativeURL("Images/TreeDelete.gif")
                        ImageButtonGroupping.CommandName = "CollapseGroup"
                    Else
                        ImageButtonGroupping.ImageUrl = BasePage.GetThemeRelativeURL("Images/TreeAdd.gif")
                        ImageButtonGroupping.CommandName = "ExpandGroup"
                    End If
                    ImageButtonGroupping.CommandArgument = i.ToString
                    AddHandler ImageButtonGroupping.Command, AddressOf Button_Command
                End If
            Next
            ' Numeration
            If ShowNumeration Then
                TableCellSpacing.ColumnSpan += 1
            End If
        End If

        If SelectDocMode = eqBaseContainer.SelectDocModes.Single OrElse SelectDocMode = eqBaseContainer.SelectDocModes.SingleAndMulti Then
            TableCellSpacing.ColumnSpan += 1
        End If
        If LoadDocMode <> eqBaseContainer.LoadDocModes.None And OpenDocToolBarDataRow IsNot Nothing Then
            TableCellSpacing.ColumnSpan += 1
        End If
        If ChildDocToolBarDataRow IsNot Nothing Then
            TableCellSpacing.ColumnSpan += 1
        End If
        If TableCellSpacing.ColumnSpan > 0 Then
            TableCellSpacing.Width = Unit.Percentage(1)
            TableRowHeader.Cells.Add(TableCellSpacing)
        End If

        ' Fields
        For Each F As DataRowView In ViewEnabledFields
            Dim FieldID = DirectCast(F("FieldID"), Guid)
            Dim FieldInfo = DocType.GetFieldInfo(FieldID)
            Dim ListDocFieldInfo = DocType.GetListDocFieldInfo(FieldID)
            Dim ShemaDataRow = DocType.GetShemaDataRow(FieldInfo("TableName"), FieldInfo("FieldName"))
            Dim TableCellField As New TableCell
            If Not IsDBNull(F("Width")) Then
                Dim Width As Unit
                Try
                    Width = Unit.Parse(F("Width"))
                Catch ex As Exception
                    Width = Unit.Empty
                End Try
                If Not Width.IsEmpty Then
                    TableCellField.Width = Width
                    TableCellField.Style("min-width") = Width.ToString
                    TableCellField.Style("max-width") = Width.ToString
                End If
            End If
            TableCellField.ToolTip = ListDocFieldInfo("FieldDesc").ToString & " [" & ListDocFieldInfo("FieldName") & "]"
            Dim LabelHeader As New Label With {.Text = F("Header").ToString}
            TableCellField.Controls.Add(LabelHeader)

            Dim TableBarWrapper As New Table With {.Width = Unit.Percentage(100), .CellPadding = 0, .CellSpacing = 0}
            Dim TableBarWrapperRow As New TableRow
            Dim TableBarWrapperSortCell As New TableCell With {.Width = Unit.Percentage(1)}
            Dim TableBarWrapperFilterCell As New TableCell With {.Width = Unit.Percentage(100)}

            ' Sort
            Dim ListDocSortControl As ListDocSort = LoadControl("ListDocSort.ascx")
            ListDocSortControl.ID = "ListDocSort" & FieldID.ToString("n")
            ListDocSortControl.FieldID = FieldID

            AddHandler ListDocSortControl.OnSortAsc, AddressOf ListDocSortControl_OnSortAsc
            AddHandler ListDocSortControl.OnSortDesc, AddressOf ListDocSortControl_OnSortDesc
            TableBarWrapperSortCell.Controls.Add(ListDocSortControl)
            TableBarWrapperRow.Cells.Add(TableBarWrapperSortCell)

            ' Filter
            Dim ListDocFilterControl As ListDocFilter = LoadControl("ListDocFilter.ascx")
            ListDocFilterControl.ID = "ListDocFilter" & FieldID.ToString("n")
            ListDocFilterControl.FieldID = FieldID

            ' Default Filter
            If Not IsPostBack Then
                If Not IsDBNull(DocType.GetListDocFieldInfo(FieldID).Item("DefaultValueInFilters")) AndAlso DocType.GetListDocFieldInfo(FieldID).Item("DefaultValueInFilters") <> "" Then
                    'ListDocFilterControl.Operator1 = "="
                    If Not IsDBNull(DocType.GetListDocFieldInfo(FieldID).Item("DefaultOperatorInFilters")) AndAlso DocType.GetListDocFieldInfo(FieldID).Item("DefaultOperatorInFilters") <> "" Then
                        ListDocFilterControl.Operator1 = DocType.GetListDocFieldInfo(FieldID).Item("DefaultOperatorInFilters")
                    Else
                        ListDocFilterControl.Operator1 = "="
                    End If
                    ListDocFilterControl.Filter1 = DocType.GetListDocFieldInfo(FieldID).Item("DefaultValueInFilters")
                    ListDocFilterControl.Filter1TextBox_OnTextChanged(ListDocFilterControl, System.EventArgs.Empty)
                    ListDocFilterControl_FilterChanged(ListDocFilterControl, System.EventArgs.Empty)
                End If
            End If

            ' TypeName
            If ListDocFieldInfo("DocTypeURL").ToString <> "" AndAlso ListDocFieldInfo("ShowingFields").ToString <> "" Then
                ListDocFilterControl.TypeName = "nvarchar"
            ElseIf ListDocFieldInfo("SourceTable").ToString <> "" AndAlso ListDocFieldInfo("KeyField").ToString <> "" AndAlso ListDocFieldInfo("DisplayField").ToString <> "" Then
                ListDocFilterControl.TypeName = "nvarchar"
            Else
                ListDocFilterControl.TypeName = ShemaDataRow("TypeName")
            End If
            ListDocFilterControl.Operator1 = F("Operator1").ToString
            ListDocFilterControl.Filter1 = F("Filter1").ToString
            ListDocFilterControl.Logic12 = F("Logic12").ToString
            ListDocFilterControl.Operator2 = F("Operator2").ToString
            ListDocFilterControl.Filter2 = F("Filter2").ToString
            If ListDocFieldInfo("ValuesScript").ToString.Trim <> String.Empty Then
                ListDocFilterControl.ValuesScript = ListDocFieldInfo("ValuesScript").ToString
            End If
            AddHandler ListDocFilterControl.FilterChanged, AddressOf ListDocFilterControl_FilterChanged
            TableBarWrapperFilterCell.Controls.Add(ListDocFilterControl)
            TableBarWrapperRow.Cells.Add(TableBarWrapperFilterCell)
            If SetFocusOnFirstListDocFilterControl Then
                ListDocFilterControl.Focus()
                SetFocusOnFirstListDocFilterControl = False
            End If

            TableBarWrapper.Rows.Add(TableBarWrapperRow)
            TableCellField.Controls.Add(TableBarWrapper)
            TableRowHeader.Cells.Add(TableCellField)
        Next
        TableListDoc.Rows.Add(TableRowHeader)

        If FirstListDocGroup IsNot Nothing Then

            ' Aggregates
            If FirstListDocGroup.AggregatesValues.Count > 0 Then
                Dim TableRowAggregates As New TableRow With {.CssClass = "GridGroupItem"}
                ' Spacing
                Dim TableCellAggregatesSpacing As New TableCell

                ''
                'Подсчет документов в разделе, и ниже по иерархии
                ''
                Dim stateListDoc = Session("StateListDoc")

                Dim some = Context.Request.QueryString.ToString

                If (stateListDoc = 1 And some = "DocTypeURL=SEARCH_CLS.ascx") Then

                    Dim SelectedCLS = Session("SelectedCLSforDocs")

                    Dim SqlConnectionStringBuilder As New SqlClient.SqlConnectionStringBuilder(My.Settings.DatabaseConnectionString)
                    Dim UserSqlConnection As New SqlClient.SqlConnection(SqlConnectionStringBuilder.ConnectionString)

                    Try
                        UserSqlConnection.Open()
                    Catch ex As SqlClient.SqlException
                        Throw New Exception(String.Format(GetGlobalResourceObject("eqResources", "MSG_DBConnectionFailed"), ex.Message))
                    End Try

                    Dim SqlCommand As New SqlClient.SqlCommand("COUNT_DOCUMENTS_FUND_CLASSIFIC", UserSqlConnection)

                    SqlCommand.CommandType = CommandType.StoredProcedure
                    SqlCommand.Parameters.AddWithValue("@fund_classific", SelectedCLS)

                    Dim outfromsql

                    'Проверить/Исправить ?
                    If Not IsDBNull(SelectedCLS) Then

                        outfromsql = SqlCommand.ExecuteScalar

                    Else

                        outfromsql = 0

                    End If

                    'Debug.WriteLine(outfromsql)

                    TableCellAggregatesSpacing.Text = "<nobr>" & String.Format(GetGlobalResourceObject("eqResources", "CAP_ListDocCatalogRowsCount"), outfromsql) & "</nobr>"

                Else

                    TableCellAggregatesSpacing.Text = "<nobr>" & String.Format(GetGlobalResourceObject("eqResources", "CAP_ListDocRowsCount"), ListDocDataTable.Rows.Count.ToString) & "</nobr>"

                End If

                If ViewGrouppingFields.Count > 0 Then
                    TableCellAggregatesSpacing.ColumnSpan += ViewGrouppingFields.Count
                End If
                If ShowNumeration Then
                    TableCellAggregatesSpacing.ColumnSpan += 1
                End If
                If ShowSelectBox Then
                    TableCellAggregatesSpacing.ColumnSpan += 1
                End If
                If SelectDocMode = eqBaseContainer.SelectDocModes.Single OrElse SelectDocMode = eqBaseContainer.SelectDocModes.SingleAndMulti Then
                    TableCellAggregatesSpacing.ColumnSpan += 1
                End If
                If LoadDocMode <> eqBaseContainer.LoadDocModes.None And OpenDocToolBarDataRow IsNot Nothing Then
                    TableCellAggregatesSpacing.ColumnSpan += 1
                End If
                If TableCellSpacing.ColumnSpan > 0 Then
                    TableRowAggregates.Cells.Add(TableCellAggregatesSpacing)
                End If
                ' Fields
                For Each F As DataRowView In ViewEnabledFields
                    Dim FieldID = DirectCast(F("FieldID"), Guid)
                    Dim FieldInfo = DocType.GetFieldInfo(FieldID)
                    Dim ListDocFieldInfo = DocType.GetListDocFieldInfo(FieldID)
                    Dim TableCellField As New TableCell
                    If FirstListDocGroup.AggregatesValues.ContainsKey(FieldID.ToString("n")) Then
                        Dim AggregateValue = FirstListDocGroup.AggregatesValues(FieldID.ToString("n"))
                        If AggregateValue IsNot Nothing AndAlso Not IsDBNull(AggregateValue) Then
                            Dim FieldFormat = ListDocFieldInfo("FieldFormat").ToString
                            If FieldFormat = "" Then
                                TableCellField.Text += eqView.GetAggregateTypeString(F("AggregateType")) & ": " & AggregateValue.ToString
                            Else
                                TableCellField.Text += eqView.GetAggregateTypeString(F("AggregateType")) & ": " & Format(AggregateValue, FieldFormat)
                            End If
                        End If
                    End If
                    TableRowAggregates.Cells.Add(TableCellField)
                Next
                TableListDoc.Rows.Add(TableRowAggregates)

                Dim TableRowAggregatesForDocument As New TableRow With {.CssClass = "GridGroupItem"}
                ' Spacing
                Dim TableCellAggregatesSpacingForDocument As New TableCell


                If (stateListDoc = 1 And some = "DocTypeURL=SEARCH_CLS.ascx") Then

                    TableCellAggregatesSpacingForDocument.Text = "<nobr>" & String.Format(GetGlobalResourceObject("eqResources", "CAP_ListDocDocumentsRowsCount"), ListDocDataTable.Rows.Count.ToString) & "</nobr>"

                    'ElseIf (stateListDoc = 2) Then

                    '    TableCellAggregatesSpacingForDocument.Text = "<nobr>" & String.Format(GetGlobalResourceObject("eqResources", "CAP_ListDocFundsRowsCount"), ListDocDataTable.Rows.Count.ToString) & "</nobr>"

                    'ElseIf (stateListDoc = 3) Then

                    '    TableCellAggregatesSpacingForDocument.Text = "<nobr>" & String.Format(GetGlobalResourceObject("eqResources", "CAP_ListDocInventoryRowsCount"), ListDocDataTable.Rows.Count.ToString) & "</nobr>"

                    'ElseIf (stateListDoc = 4) Then

                    '    TableCellAggregatesSpacingForDocument.Text = "<nobr>" & String.Format(GetGlobalResourceObject("eqResources", "CAP_ListDocUnitsRowsCount"), ListDocDataTable.Rows.Count.ToString) & "</nobr>"

                Else

                    TableCellAggregatesSpacing.Text = "<nobr>" & String.Format(GetGlobalResourceObject("eqResources", "CAP_ListDocRowsCount"), ListDocDataTable.Rows.Count.ToString) & "</nobr>"

                End If

                If ViewGrouppingFields.Count > 0 Then
                    TableCellAggregatesSpacingForDocument.ColumnSpan += ViewGrouppingFields.Count
                End If
                If ShowNumeration Then
                    TableCellAggregatesSpacingForDocument.ColumnSpan += 1
                End If
                If ShowSelectBox Then
                    TableCellAggregatesSpacingForDocument.ColumnSpan += 1
                End If
                If SelectDocMode = eqBaseContainer.SelectDocModes.Single OrElse SelectDocMode = eqBaseContainer.SelectDocModes.SingleAndMulti Then
                    TableCellAggregatesSpacingForDocument.ColumnSpan += 1
                End If
                If LoadDocMode <> eqBaseContainer.LoadDocModes.None And OpenDocToolBarDataRow IsNot Nothing Then
                    TableCellAggregatesSpacingForDocument.ColumnSpan += 1
                End If
                If TableCellSpacing.ColumnSpan > 0 Then
                    TableRowAggregatesForDocument.Cells.Add(TableCellAggregatesSpacingForDocument)
                End If
                ' Fields
                For Each F As DataRowView In ViewEnabledFields
                    Dim FieldID = DirectCast(F("FieldID"), Guid)
                    Dim FieldInfo = DocType.GetFieldInfo(FieldID)
                    Dim ListDocFieldInfo = DocType.GetListDocFieldInfo(FieldID)
                    Dim TableCellField As New TableCell
                    If FirstListDocGroup.AggregatesValues.ContainsKey(FieldID.ToString("n")) Then
                        Dim AggregateValue = FirstListDocGroup.AggregatesValues(FieldID.ToString("n"))
                        If AggregateValue IsNot Nothing AndAlso Not IsDBNull(AggregateValue) Then
                            Dim FieldFormat = ListDocFieldInfo("FieldFormat").ToString
                            If FieldFormat = "" Then
                                TableCellField.Text += eqView.GetAggregateTypeString(F("AggregateType")) & ": " & AggregateValue.ToString
                            Else
                                TableCellField.Text += eqView.GetAggregateTypeString(F("AggregateType")) & ": " & Format(AggregateValue, FieldFormat)
                            End If
                        End If
                    End If
                    TableRowAggregatesForDocument.Cells.Add(TableCellField)
                Next
                TableListDoc.Rows.Add(TableRowAggregatesForDocument)

            End If

            ' Groups
            ProcessListDocGroup(FirstListDocGroup)

            ' Pages
            TableRowPaging.Visible = False
            TableRowPaging2.Visible = False
            Select Case PagingMode
                Case PagingModes.NumericPages
                    Dim MaxPageNumber = Math.Round(ProcessedRows / ViewRowsPerPage + 0.49999)
                    If MaxPageNumber > 1 Then
                        Dim PagesPagingStep As Integer = 100
                        For PageNumberFrom As Integer = 1 To MaxPageNumber Step PagesPagingStep
                            TableRowPaging.Visible = True
                            TableRowPaging2.Visible = True
                            Dim PageNumberTo = Math.Min(PageNumberFrom + PagesPagingStep - 1, MaxPageNumber)
                            If PageIndex >= PageNumberFrom And PageIndex <= PageNumberTo Then
                                ' Попало
                                If TableCellPaging.Controls.Count > 0 Then
                                    TableCellPaging.Controls.Add(New LiteralControl("<br/>"))
                                    TableCellPaging2.Controls.Add(New LiteralControl("<br/>"))
                                End If
                                For PageNumber As Integer = PageNumberFrom To PageNumberTo
                                    Dim LinkButtonPage As New LinkButton With {.ID = "HeaderPage" & PageNumber.ToString, .CommandName = "Page", .CommandArgument = PageNumber.ToString}
                                    AddHandler LinkButtonPage.Command, AddressOf Button_Command
                                    LinkButtonPage.Text = PageNumber
                                    Dim LinkButtonPage2 As New LinkButton With {.ID = "HeaderPage2" & PageNumber.ToString, .CommandName = "Page", .CommandArgument = PageNumber.ToString}
                                    AddHandler LinkButtonPage2.Command, AddressOf Button_Command
                                    LinkButtonPage2.Text = PageNumber
                                    If PageNumber = PageIndex Then
                                        LinkButtonPage.Font.Bold = True
                                        LinkButtonPage.Font.Size = FontUnit.Larger
                                        LinkButtonPage2.Font.Bold = True
                                        LinkButtonPage2.Font.Size = FontUnit.Larger
                                    End If
                                    TableCellPaging.Controls.Add(LinkButtonPage)
                                    TableCellPaging.Controls.Add(New LiteralControl(" "))
                                    TableCellPaging2.Controls.Add(LinkButtonPage2)
                                    TableCellPaging2.Controls.Add(New LiteralControl(" "))
                                Next
                                If PageNumberTo < MaxPageNumber Then
                                    TableCellPaging.Controls.Add(New LiteralControl("<br/>"))
                                    TableCellPaging2.Controls.Add(New LiteralControl("<br/>"))
                                End If
                            Else
                                ' Не попало
                                Dim LinkButtonPage As New LinkButton With {.ID = "HeaderPage" & PageNumberFrom.ToString, .CommandName = "Page", .CommandArgument = PageNumberFrom.ToString}
                                AddHandler LinkButtonPage.Command, AddressOf Button_Command
                                LinkButtonPage.Text = String.Format("{0}..{1}", PageNumberFrom, PageNumberTo)
                                TableCellPaging.Controls.Add(LinkButtonPage)
                                TableCellPaging.Controls.Add(New LiteralControl(" "))
                                Dim LinkButtonPage2 As New LinkButton With {.ID = "HeaderPage2" & PageNumberFrom.ToString, .CommandName = "Page", .CommandArgument = PageNumberFrom.ToString}
                                AddHandler LinkButtonPage2.Command, AddressOf Button_Command
                                LinkButtonPage2.Text = String.Format("{0}..{1}", PageNumberFrom, PageNumberTo)
                                TableCellPaging2.Controls.Add(LinkButtonPage2)
                                TableCellPaging2.Controls.Add(New LiteralControl(" "))
                            End If
                        Next
                    End If
                Case PagingModes.PrevNextPages
                    Dim MaxPageNumber = Math.Ceiling(ProcessedRows / ViewRowsPerPage + 0.49)
                    ' Label
                    TableCellPaging.Controls.Add(New LiteralControl(GetGlobalResourceObject("eqResources", "CAP_ListDocPagingPage") & " "))
                    TableCellPaging2.Controls.Add(New LiteralControl(GetGlobalResourceObject("eqResources", "CAP_ListDocPagingPage") & " "))
                    ' Prev
                    If PageIndex > 1 Then
                        TableRowPaging.Visible = True
                        TableRowPaging2.Visible = True
                        Dim PageNumber = PageIndex - 1
                        Dim LinkButtonPage As New LinkButton With {.ID = "HeaderPage" & PageNumber.ToString, .CommandName = "Page", .CommandArgument = PageNumber.ToString}
                        AddHandler LinkButtonPage.Command, AddressOf Button_Command
                        LinkButtonPage.Style("border") = "solid 1px gray"
                        LinkButtonPage.Style("padding") = "2px 1px 1px 1px"
                        LinkButtonPage.Style("text-decoration") = "none"
                        LinkButtonPage.Style("background-color") = "white"
                        LinkButtonPage.Text = "&nbsp;<b>&lt;</b>&nbsp;"
                        LinkButtonPage.ToolTip = GetGlobalResourceObject("eqResources", "TIP_ListDocPagingPrevious")
                        TableCellPaging.Controls.Add(LinkButtonPage)
                        TableCellPaging.Controls.Add(New LiteralControl(" "))
                        Dim LinkButtonPage2 As New LinkButton With {.ID = "HeaderPage2" & PageNumber.ToString, .CommandName = "Page", .CommandArgument = PageNumber.ToString}
                        AddHandler LinkButtonPage2.Command, AddressOf Button_Command
                        LinkButtonPage2.Style("border") = "solid 1px gray"
                        LinkButtonPage2.Style("padding") = "2px 1px 1px 1px"
                        LinkButtonPage2.Style("text-decoration") = "none"
                        LinkButtonPage2.Style("background-color") = "white"
                        LinkButtonPage2.Text = "&nbsp;<b>&lt;</b>&nbsp;"
                        LinkButtonPage2.ToolTip = GetGlobalResourceObject("eqResources", "TIP_ListDocPagingPrevious")
                        TableCellPaging2.Controls.Add(LinkButtonPage2)
                        TableCellPaging2.Controls.Add(New LiteralControl(" "))
                    End If
                    ' Current
                    Dim cpTextBox As New TextBox With {.ID = "cpTextBox", .AutoPostBack = True}
                    cpTextBox.Text = PageIndex.ToString
                    cpTextBox.Columns = PageIndex.ToString.Length
                    AddHandler cpTextBox.TextChanged, AddressOf cpTextBox_TextChanged
                    TableCellPaging.Controls.Add(cpTextBox)
                    Dim cpTextBox2 As New TextBox With {.ID = "cpTextBox2", .AutoPostBack = True}
                    cpTextBox2.Text = PageIndex.ToString
                    cpTextBox2.Columns = PageIndex.ToString.Length
                    AddHandler cpTextBox2.TextChanged, AddressOf cpTextBox_TextChanged
                    TableCellPaging2.Controls.Add(cpTextBox2)
                    ' Next
                    If PageIndex < MaxPageNumber Then
                        TableRowPaging.Visible = True
                        TableRowPaging2.Visible = True
                        Dim PageNumber = PageIndex + 1
                        Dim LinkButtonPage As New LinkButton With {.ID = "HeaderPage" & PageNumber.ToString, .CommandName = "Page", .CommandArgument = PageNumber.ToString}
                        AddHandler LinkButtonPage.Command, AddressOf Button_Command
                        LinkButtonPage.Style("border") = "solid 1px gray"
                        LinkButtonPage.Style("padding") = "2px 1px 1px 1px"
                        LinkButtonPage.Style("text-decoration") = "none"
                        LinkButtonPage.Style("background-color") = "white"
                        LinkButtonPage.Text = "&nbsp;<b>&gt;</b>&nbsp;"
                        LinkButtonPage.ToolTip = GetGlobalResourceObject("eqResources", "TIP_ListDocPagingNext")
                        TableCellPaging.Controls.Add(New LiteralControl(" "))
                        TableCellPaging.Controls.Add(LinkButtonPage)
                        Dim LinkButtonPage2 As New LinkButton With {.ID = "HeaderPage2" & PageNumber.ToString, .CommandName = "Page", .CommandArgument = PageNumber.ToString}
                        AddHandler LinkButtonPage2.Command, AddressOf Button_Command
                        LinkButtonPage2.Style("border") = "solid 1px gray"
                        LinkButtonPage2.Style("padding") = "2px 1px 1px 1px"
                        LinkButtonPage2.Style("text-decoration") = "none"
                        LinkButtonPage2.Style("background-color") = "white"
                        LinkButtonPage2.Text = "&nbsp;<b>&gt;</b>&nbsp;"
                        LinkButtonPage2.ToolTip = GetGlobalResourceObject("eqResources", "TIP_ListDocPagingNext")
                        TableCellPaging2.Controls.Add(New LiteralControl(" "))
                        TableCellPaging2.Controls.Add(LinkButtonPage2)
                    End If
                    ' Max
                    TableCellPaging.Controls.Add(New LiteralControl(" " & GetGlobalResourceObject("eqResources", "CAP_ListDocPagingFrom") & " "))
                    TableCellPaging.Controls.Add(New LiteralControl(MaxPageNumber.ToString))
                    TableCellPaging2.Controls.Add(New LiteralControl(" " & GetGlobalResourceObject("eqResources", "CAP_ListDocPagingFrom") & " "))
                    TableCellPaging2.Controls.Add(New LiteralControl(MaxPageNumber.ToString))
            End Select

        End If

        If FirstListDocNode IsNot Nothing Then

            ' Aggregates
            Dim TableRowAggregates As New TableRow With {.CssClass = "GridGroupItem"}
            ' Spacing
            Dim TableCellAggregatesSpacing As New TableCell
            TableCellAggregatesSpacing.Text = "<nobr>" & String.Format(GetGlobalResourceObject("eqResources", "CAP_ListDocRowsCount"), ListDocDataTable.Rows.Count.ToString) & "</nobr>"
            TableCellAggregatesSpacing.ColumnSpan += 1
            If ViewEnabledFields.Count > 0 Then
                TableCellAggregatesSpacing.ColumnSpan += ViewEnabledFields.Count
            End If
            If ShowNumeration Then
                TableCellAggregatesSpacing.ColumnSpan += 1
            End If
            If ShowSelectBox Then
                TableCellAggregatesSpacing.ColumnSpan += 1
            End If
            If SelectDocMode = eqBaseContainer.SelectDocModes.Single OrElse SelectDocMode = eqBaseContainer.SelectDocModes.SingleAndMulti Then
                TableCellAggregatesSpacing.ColumnSpan += 1
            End If
            If LoadDocMode <> eqBaseContainer.LoadDocModes.None And OpenDocToolBarDataRow IsNot Nothing Then
                TableCellAggregatesSpacing.ColumnSpan += 1
            End If
            If ChildDocToolBarDataRow IsNot Nothing Then
                TableCellAggregatesSpacing.ColumnSpan += 1
            End If
            If TableCellSpacing.ColumnSpan > 0 Then
                TableRowAggregates.Cells.Add(TableCellAggregatesSpacing)
            End If
            TableListDoc.Rows.Add(TableRowAggregates)

            ' Groups
            ProcessListDocNode(FirstListDocNode)

            ' Pages
            TableRowPaging.Visible = False
            TableRowPaging2.Visible = False
            Select Case PagingMode
                Case PagingModes.NumericPages
                    Dim MaxPageNumber = Math.Round(ProcessedRows / ViewRowsPerPage + 0.49999)
                    If MaxPageNumber > 1 Then
                        Dim PagesPagingStep As Integer = 100
                        For PageNumberFrom As Integer = 1 To MaxPageNumber Step PagesPagingStep
                            TableRowPaging.Visible = True
                            TableRowPaging2.Visible = True
                            Dim PageNumberTo = Math.Min(PageNumberFrom + PagesPagingStep - 1, MaxPageNumber)
                            If PageIndex >= PageNumberFrom And PageIndex <= PageNumberTo Then
                                ' Попало
                                If TableCellPaging.Controls.Count > 0 Then
                                    TableCellPaging.Controls.Add(New LiteralControl("<br/>"))
                                    TableCellPaging2.Controls.Add(New LiteralControl("<br/>"))
                                End If
                                For PageNumber As Integer = PageNumberFrom To PageNumberTo
                                    Dim LinkButtonPage As New LinkButton With {.ID = "HeaderPage" & PageNumber.ToString, .CommandName = "Page", .CommandArgument = PageNumber.ToString}
                                    AddHandler LinkButtonPage.Command, AddressOf Button_Command
                                    LinkButtonPage.Text = PageNumber
                                    Dim LinkButtonPage2 As New LinkButton With {.ID = "HeaderPage2" & PageNumber.ToString, .CommandName = "Page", .CommandArgument = PageNumber.ToString}
                                    AddHandler LinkButtonPage2.Command, AddressOf Button_Command
                                    LinkButtonPage2.Text = PageNumber
                                    If PageNumber = PageIndex Then
                                        LinkButtonPage.Font.Bold = True
                                        LinkButtonPage.Font.Size = FontUnit.Larger
                                        LinkButtonPage2.Font.Bold = True
                                        LinkButtonPage2.Font.Size = FontUnit.Larger
                                    End If
                                    TableCellPaging.Controls.Add(LinkButtonPage)
                                    TableCellPaging.Controls.Add(New LiteralControl(" "))
                                    TableCellPaging2.Controls.Add(LinkButtonPage2)
                                    TableCellPaging2.Controls.Add(New LiteralControl(" "))
                                Next
                                If PageNumberTo < MaxPageNumber Then
                                    TableCellPaging.Controls.Add(New LiteralControl("<br/>"))
                                    TableCellPaging2.Controls.Add(New LiteralControl("<br/>"))
                                End If
                            Else
                                ' Не попало
                                Dim LinkButtonPage As New LinkButton With {.ID = "HeaderPage" & PageNumberFrom.ToString, .CommandName = "Page", .CommandArgument = PageNumberFrom.ToString}
                                AddHandler LinkButtonPage.Command, AddressOf Button_Command
                                LinkButtonPage.Text = String.Format("{0}..{1}", PageNumberFrom, PageNumberTo)
                                TableCellPaging.Controls.Add(LinkButtonPage)
                                TableCellPaging.Controls.Add(New LiteralControl(" "))
                                Dim LinkButtonPage2 As New LinkButton With {.ID = "HeaderPage2" & PageNumberFrom.ToString, .CommandName = "Page", .CommandArgument = PageNumberFrom.ToString}
                                AddHandler LinkButtonPage2.Command, AddressOf Button_Command
                                LinkButtonPage2.Text = String.Format("{0}..{1}", PageNumberFrom, PageNumberTo)
                                TableCellPaging2.Controls.Add(LinkButtonPage2)
                                TableCellPaging2.Controls.Add(New LiteralControl(" "))
                            End If
                        Next
                    End If
                Case PagingModes.PrevNextPages
                    Dim MaxPageNumber = Math.Ceiling(ProcessedRows / ViewRowsPerPage + 0.49)
                    ' Label
                    TableCellPaging.Controls.Add(New LiteralControl(GetGlobalResourceObject("eqResources", "CAP_ListDocPagingPage") & " "))
                    TableCellPaging2.Controls.Add(New LiteralControl(GetGlobalResourceObject("eqResources", "CAP_ListDocPagingPage") & " "))
                    ' Prev
                    If PageIndex > 1 Then
                        TableRowPaging.Visible = True
                        TableRowPaging2.Visible = True
                        Dim PageNumber = PageIndex - 1
                        Dim LinkButtonPage As New LinkButton With {.ID = "HeaderPage" & PageNumber.ToString, .CommandName = "Page", .CommandArgument = PageNumber.ToString}
                        AddHandler LinkButtonPage.Command, AddressOf Button_Command
                        LinkButtonPage.Style("border") = "solid 1px gray"
                        LinkButtonPage.Style("padding") = "2px 1px 1px 1px"
                        LinkButtonPage.Style("text-decoration") = "none"
                        LinkButtonPage.Style("background-color") = "white"
                        LinkButtonPage.Text = "&nbsp;<b>&lt;</b>&nbsp;"
                        LinkButtonPage.ToolTip = GetGlobalResourceObject("eqResources", "TIP_ListDocPagingPrevious")
                        TableCellPaging.Controls.Add(LinkButtonPage)
                        TableCellPaging.Controls.Add(New LiteralControl(" "))
                        Dim LinkButtonPage2 As New LinkButton With {.ID = "HeaderPage2" & PageNumber.ToString, .CommandName = "Page", .CommandArgument = PageNumber.ToString}
                        AddHandler LinkButtonPage2.Command, AddressOf Button_Command
                        LinkButtonPage2.Style("border") = "solid 1px gray"
                        LinkButtonPage2.Style("padding") = "2px 1px 1px 1px"
                        LinkButtonPage2.Style("text-decoration") = "none"
                        LinkButtonPage2.Style("background-color") = "white"
                        LinkButtonPage2.Text = "&nbsp;<b>&lt;</b>&nbsp;"
                        LinkButtonPage2.ToolTip = GetGlobalResourceObject("eqResources", "TIP_ListDocPagingPrevious")
                        TableCellPaging2.Controls.Add(LinkButtonPage2)
                        TableCellPaging2.Controls.Add(New LiteralControl(" "))
                    End If
                    ' Current
                    Dim cpTextBox As New TextBox With {.ID = "cpTextBox", .AutoPostBack = True}
                    cpTextBox.Text = PageIndex.ToString
                    cpTextBox.Columns = PageIndex.ToString.Length
                    AddHandler cpTextBox.TextChanged, AddressOf cpTextBox_TextChanged
                    TableCellPaging.Controls.Add(cpTextBox)
                    Dim cpTextBox2 As New TextBox With {.ID = "cpTextBox2", .AutoPostBack = True}
                    cpTextBox2.Text = PageIndex.ToString
                    cpTextBox2.Columns = PageIndex.ToString.Length
                    AddHandler cpTextBox2.TextChanged, AddressOf cpTextBox_TextChanged
                    TableCellPaging2.Controls.Add(cpTextBox2)
                    ' Next
                    If PageIndex < MaxPageNumber Then
                        TableRowPaging.Visible = True
                        TableRowPaging2.Visible = True
                        Dim PageNumber = PageIndex + 1
                        Dim LinkButtonPage As New LinkButton With {.ID = "HeaderPage" & PageNumber.ToString, .CommandName = "Page", .CommandArgument = PageNumber.ToString}
                        AddHandler LinkButtonPage.Command, AddressOf Button_Command
                        LinkButtonPage.Style("border") = "solid 1px gray"
                        LinkButtonPage.Style("padding") = "2px 1px 1px 1px"
                        LinkButtonPage.Style("text-decoration") = "none"
                        LinkButtonPage.Style("background-color") = "white"
                        LinkButtonPage.Text = "&nbsp;<b>&gt;</b>&nbsp;"
                        LinkButtonPage.ToolTip = GetGlobalResourceObject("eqResources", "TIP_ListDocPagingNext")
                        TableCellPaging.Controls.Add(New LiteralControl(" "))
                        TableCellPaging.Controls.Add(LinkButtonPage)
                        Dim LinkButtonPage2 As New LinkButton With {.ID = "HeaderPage2" & PageNumber.ToString, .CommandName = "Page", .CommandArgument = PageNumber.ToString}
                        AddHandler LinkButtonPage2.Command, AddressOf Button_Command
                        LinkButtonPage2.Style("border") = "solid 1px gray"
                        LinkButtonPage2.Style("padding") = "2px 1px 1px 1px"
                        LinkButtonPage2.Style("text-decoration") = "none"
                        LinkButtonPage2.Style("background-color") = "white"
                        LinkButtonPage2.Text = "&nbsp;<b>&gt;</b>&nbsp;"
                        LinkButtonPage2.ToolTip = GetGlobalResourceObject("eqResources", "TIP_ListDocPagingNext")
                        TableCellPaging2.Controls.Add(New LiteralControl(" "))
                        TableCellPaging2.Controls.Add(LinkButtonPage2)
                    End If
                    ' Max
                    TableCellPaging.Controls.Add(New LiteralControl(" " & GetGlobalResourceObject("eqResources", "CAP_ListDocPagingFrom") & " "))
                    TableCellPaging.Controls.Add(New LiteralControl(MaxPageNumber.ToString))
                    TableCellPaging2.Controls.Add(New LiteralControl(" " & GetGlobalResourceObject("eqResources", "CAP_ListDocPagingFrom") & " "))
                    TableCellPaging2.Controls.Add(New LiteralControl(MaxPageNumber.ToString))
            End Select

        End If

        TableListDoc.Rows.Add(TableRowPaging2)
        TableBuilded = True
        countt = 0
    End Sub

    Private Sub BuildFunctionsTable()
        RoleFunctionsTable.Rows.Clear()
        ViewFunctionsTable.Rows.Clear()

        ' RolesListCondSpecialByRoles
        Dim RoleFunctionsRow As New TableRow
        ' Label
        RoleFunctionsRow.Cells.Add(New TableCell With {.Text = GetGlobalResourceObject("eqResources", "CAP_RoleRestriction") & ":", .ToolTip = GetGlobalResourceObject("eqResources", "TIP_RoleRestriction")})
        ' eqfn_GetRolesListCondSpecial
        Dim Securities As New eqSecurities(AppSettings)
        Dim RolesListCondSpecialByRoles = From R As DataRow In AppSettings.UserSecurities.ListCondSpecial(DocType.DocTypeID) Group By RoleID = DirectCast(R("DocID"), Guid) Into Group
        'If RolesListCondSpecialByRoles.Count > 0 Then
        'If RoleFunctionsRow.Cells.Count > 0 Then
        '    RoleFunctionsRow.Cells.Add(New TableCell With {.Text = "и"})
        'End If
        '    RoleFunctionsRow.Cells.Add(New TableCell With {.Text = "<big>(</big>"})
        'End If
        For Each RolesListCondSpecialByRole In RolesListCondSpecialByRoles
            If RoleFunctionsRow.Cells.Count > 1 Then
                RoleFunctionsRow.Cells.Add(New TableCell With {.Text = "ИЛИ"})
            End If
            RoleFunctionsRow.Cells.Add(New TableCell With {.Text = "("})
            For Each R In RolesListCondSpecialByRole.Group
                If Not IsDBNull(R("FunctionID")) And Not IsDBNull(R("Enabled")) Then
                    Dim FunctionControlID = DirectCast(R("ID"), Guid)
                    Dim FunctionID = DirectCast(R("FunctionID"), Guid)
                    Dim FunctionEnabled = DirectCast(R("Enabled"), Boolean)
                    Dim ParameterValue = R("Parameter").ToString.Trim
                    If FunctionEnabled Then

                        Dim DocTypeFunctionInfo = DocType.GetFunctionInfo(FunctionID)

                        If DocTypeFunctionInfo IsNot Nothing Then
                            Dim ParameterName = DocTypeFunctionInfo("ParameterName").ToString.Trim
                            Dim FunctionName = DocTypeFunctionInfo("FunctionName").ToString
                            Dim FunctionText = DocTypeFunctionInfo("FunctionText").ToString
                            Dim ParameterType = DocTypeFunctionInfo("ParameterType").ToString

                            If RoleFunctionsRow.Cells(RoleFunctionsRow.Cells.Count - 1).Text <> "(" Then
                                RoleFunctionsRow.Cells.Add(New TableCell With {.Text = "И"})
                            End If

                            Dim TableFunctionsCell As New TableCell
                            TableFunctionsCell.Text = FunctionName
                            If ParameterName <> String.Empty Then
                                TableFunctionsCell.Text += "(" & ParameterValue & ")"
                            End If
                            'Dim ListDocFunctionControl As ListDocFunction = LoadControl("ListDocFunction.ascx")
                            'ListDocFunctionControl.ID = "ListDocFunctionControl" & FunctionControlID.ToString("n")
                            'ListDocFunctionControl.FunctionReadOnly = True
                            'ListDocFunctionControl.FunctionID = FunctionControlID
                            'ListDocFunctionControl.FunctionEnabled = True
                            'ListDocFunctionControl.FunctionName = FunctionName
                            'ListDocFunctionControl.FunctionType = ParameterType
                            'ListDocFunctionControl.ParameterName = ParameterName
                            'ListDocFunctionControl.ParameterValue = ParameterValue
                            'TableFunctionsCell.Controls.Add(ListDocFunctionControl)
                            RoleFunctionsRow.Cells.Add(TableFunctionsCell)

                        End If

                    End If
                End If
            Next
            RoleFunctionsRow.Cells.Add(New TableCell With {.Text = ")"})
        Next
        'If RolesListCondSpecialByRoles.Count > 0 Then
        '    RoleFunctionsRow.Cells.Add(New TableCell With {.Text = "<big>)</big>"})
        'End If

        ' ViewFunctions
        Dim ViewFunctionsRow As New TableRow
        ' Label
        ViewFunctionsRow.Cells.Add(New TableCell With {.Text = GetGlobalResourceObject("eqResources", "CAP_AdditionalConditions") & ":", .ToolTip = GetGlobalResourceObject("eqResources", "TIP_AdditionalConditions")})
        ' Functions
        For Each R As DataRowView In ViewFunctions
            Dim FunctionControlID = DirectCast(R("ID"), Guid)
            Dim FunctionID = DirectCast(R("FunctionsID"), Guid)
            Dim FunctionEnabled = DirectCast(R("Enabled"), Boolean)
            Dim ParameterValue = R("Parameter").ToString.Trim

            Dim DocTypeFunctionInfo = DocType.GetFunctionInfo(FunctionID)

            If DocTypeFunctionInfo IsNot Nothing Then
                Dim ParameterName = DocTypeFunctionInfo("ParameterName").ToString.Trim
                Dim FunctionName = DocTypeFunctionInfo("FunctionName").ToString
                Dim FunctionText = DocTypeFunctionInfo("FunctionText").ToString
                Dim ParameterType = DocTypeFunctionInfo("ParameterType").ToString
                Dim ParameterValuesScript = DocTypeFunctionInfo("ParameterValuesScript").ToString
                Dim ParameterDefaultValueScript = DocTypeFunctionInfo("ParameterDefaultValueScript").ToString

                If ViewFunctionsRow.Cells.Count > 1 Then
                    ViewFunctionsRow.Cells.Add(New TableCell With {.Text = "И"})
                End If

                Dim TableFunctionsCell As New TableCell
                Dim ListDocFunctionControl As ListDocFunction = LoadControl("ListDocFunction.ascx")
                ListDocFunctionControl.ID = "ListDocFunctionControl" & FunctionControlID.ToString("n")
                ListDocFunctionControl.FunctionID = FunctionControlID
                ListDocFunctionControl.FunctionEnabled = FunctionEnabled
                ListDocFunctionControl.FunctionName = FunctionName
                ListDocFunctionControl.FunctionType = ParameterType
                ListDocFunctionControl.ParameterName = ParameterName
                ListDocFunctionControl.ParameterValue = ParameterValue
                If ParameterName <> String.Empty Then
                    If ParameterValuesScript <> String.Empty Then
                        Dim ParameterValuesSqlCommand As New SqlClient.SqlCommand(ParameterValuesScript)
                        ParameterValuesSqlCommand.Parameters.AddWithValue("@UserID", AppSettings.UserInfo.UserID)
                        Dim ParameterValuesDataTable = AppSettings.GetDataTable(ParameterValuesSqlCommand)
                        ListDocFunctionControl.ParameterValuesDataTable = ParameterValuesDataTable
                    End If
                    'If ParameterDefaultValueScript <> String.Empty Then
                    '    Dim ParameterDefaultValueSqlCommand As New SqlClient.SqlCommand(ParameterDefaultValueScript)
                    '    ParameterDefaultValueSqlCommand.Parameters.AddWithValue("@UserID", AppSettings.UserInfo.UserID)
                    '    Dim ParameterDefaultValueDataTable = AppSettings.GetDataTable(ParameterDefaultValueSqlCommand)
                    '    Dim ParameterDefaultValue As Object = Nothing
                    '    If ParameterDefaultValueDataTable Is Nothing Then

                    '    End If
                    '    Dim ParameterDefaultValue = ParameterDefaultValueDataTable.Rows(0).Item(0)
                    '    ListDocFunctionControl.ParameterValuesDataTable = ParameterValuesDataTable
                    'End If
                End If
                AddHandler ListDocFunctionControl.FunctionChanged, AddressOf ListDocFunctionControl_FunctionChanged
                TableFunctionsCell.Controls.Add(ListDocFunctionControl)
                ViewFunctionsRow.Cells.Add(TableFunctionsCell)

            End If

        Next

        ' TableRow
        If RolesListCondSpecialByRoles.Count > 0 Then
            RoleFunctionsTable.Rows.Add(RoleFunctionsRow)
        End If
        If ViewFunctions.Count > 0 Then
            ViewFunctionsTable.Rows.Add(ViewFunctionsRow)
        End If
        ' Visible
        RoleFunctionsTable.Visible = RoleFunctionsTable.Rows.Count > 0
        ViewFunctionsTable.Visible = ViewFunctionsTable.Rows.Count > 0

    End Sub

    'Private Sub BuildListDocPreFilter()
    '    Dim PreFilterFileds = New String() {"DocNum", "Start", "Finish"}
    '    'If DocTypeURL = "CostPrice.ascx" Then
    '    '    PreFilterFileds = New String() {"DocNum"}
    '    'End If
    '    PlaceHolderListDocPreFilter.Controls.Clear()
    '    Dim PreFilterTable As New Table With {.Width = Unit.Percentage(100), .CssClass = "LogicBlockTable", .ID = "PreFilterTable"}
    '    ' Fields
    '    For Each F As DataRowView In ViewFields
    '        Dim FieldID = DirectCast(F("FieldID"), Guid)
    '        Dim FieldInfo = DocType.GetFieldInfo(FieldID)
    '        Dim ListDocFieldInfo = DocType.GetListDocFieldInfo(FieldID)
    '        Dim ShemaDataRow = DocType.GetShemaDataRow(FieldInfo("TableName"), FieldInfo("FieldName"))
    '        If PreFilterFileds.Contains(FieldInfo("FieldName")) Then
    '            Dim PreFilterTableRow As New TableRow
    '            ' Header
    '            Dim TableCellFieldHeader As New TableCell
    '            Dim LabelHeader As New Label With {.Text = F("Header").ToString}
    '            TableCellFieldHeader.Controls.Add(LabelHeader)
    '            PreFilterTableRow.Cells.Add(TableCellFieldHeader)
    '            ' Filter
    '            Dim TableCellField As New TableCell
    '            Dim ListDocFilterControl As ListDocFilter = LoadControl("ListDocFilter.ascx")
    '            ListDocFilterControl.ID = "ListDocPreFilter" & FieldID.ToString("n")
    '            ListDocFilterControl.FieldID = FieldID
    '            ListDocFilterControl.TypeName = ShemaDataRow("TypeName")
    '            ListDocFilterControl.Operator1 = F("Operator1").ToString
    '            ListDocFilterControl.Filter1 = F("Filter1").ToString
    '            ListDocFilterControl.Logic12 = F("Logic12").ToString
    '            ListDocFilterControl.Operator2 = F("Operator2").ToString
    '            ListDocFilterControl.Filter2 = F("Filter2").ToString
    '            AddHandler ListDocFilterControl.FilterChanged, AddressOf ListDocFilterControl_FilterChanged
    '            TableCellField.Controls.Add(ListDocFilterControl)
    '            If SetFocusOnFirstListDocFilterControl Then
    '                'ListDocFilterControl.Focus()
    '                'SetFocusOnFirstListDocFilterControl = False
    '            End If
    '            PreFilterTableRow.Cells.Add(TableCellField)
    '            PreFilterTable.Rows.Add(PreFilterTableRow)
    '        End If
    '    Next
    '    PlaceHolderListDocPreFilter.Controls.Add(PreFilterTable)
    'End Sub

    'Private Sub ShowListDocPreFilter()
    '    PlaceHolderShowListDocPreFilter.Visible = True
    '    ModalPopupExtenderListDocPreFilter.OnOkScript = Page.ClientScript.GetPostBackEventReference(Me, "ListDocPreFilter")
    '    ModalPopupExtenderListDocPreFilter.Show()
    'End Sub

    Private NeedToRefreshAfterFiltersChanged As Boolean = False

    Private Sub ListDocSortControl_OnSortAsc(ByVal sender As Object, ByVal e As EventArgs)
        Dim ListDocSortControl = DirectCast(sender, ListDocSort)
        Dim LS = MyBaseContainer.ListSorting.FirstOrDefault(Function(F) F.First = ListDocSortControl.FieldID)
        If LS IsNot Nothing Then
            MyBaseContainer.ListSorting.Remove(LS)
        End If
        MyBaseContainer.ListSorting.Insert(0, New Pair(ListDocSortControl.FieldID, "ASC"))
        NeedToRefreshAfterFiltersChanged = True
    End Sub

    Private Sub ListDocSortControl_OnSortDesc(ByVal sender As Object, ByVal e As EventArgs)
        Dim ListDocSortControl = DirectCast(sender, ListDocSort)
        Dim ContainerControl = DirectCast(BaseContainer, BaseContainer)
        Dim LS = MyBaseContainer.ListSorting.FirstOrDefault(Function(F) F.First = ListDocSortControl.FieldID)
        If LS IsNot Nothing Then
            MyBaseContainer.ListSorting.Remove(LS)
        End If
        MyBaseContainer.ListSorting.Insert(0, New Pair(ListDocSortControl.FieldID, "DESC"))
        NeedToRefreshAfterFiltersChanged = True
    End Sub

    Private Sub ListDocFunctionControl_FunctionChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim ListDocFunctionControl = DirectCast(sender, ListDocFunction)
        Dim VF = ViewFunctions.Single(Function(F) F("ID") = ListDocFunctionControl.FunctionID)
        VF("Enabled") = ListDocFunctionControl.FunctionEnabled
        VF("Parameter") = ListDocFunctionControl.ParameterValue
        NeedToRefreshAfterFiltersChanged = True
    End Sub

    Private Sub ListDocFilterControl_FilterChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim ListDocFilterControl = DirectCast(sender, ListDocFilter)
        Dim VF = ViewFields.Single(Function(F) F("FieldID") = ListDocFilterControl.FieldID)
        VF("Operator1") = ListDocFilterControl.Operator1
        VF("Filter1") = ListDocFilterControl.Filter1
        VF("Logic12") = ListDocFilterControl.Logic12
        VF("Operator2") = ListDocFilterControl.Operator2
        VF("Filter2") = ListDocFilterControl.Filter2
        NeedToRefreshAfterFiltersChanged = True
    End Sub

    Private Sub CheckBoxSelectBox_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim CheckBoxSelectBox = DirectCast(sender, CheckBox)
        Dim CurrentDocID As New Guid(CheckBoxSelectBox.ID.Substring(9, Guid.Empty.ToString("n").Length))
        If CheckBoxSelectBox.Checked Then
            SelectedDocuments.Add(CurrentDocID)
        Else
            SelectedDocuments.Remove(CurrentDocID)
        End If
    End Sub

    Private Sub cpTextBox_TextChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim cpTextBox = DirectCast(sender, TextBox)
        If IsNumeric(cpTextBox.Text) Then
            Dim MaxPageNumber = Math.Round(ProcessedRows / ViewRowsPerPage + 0.5)
            Dim PageNumber = CInt(cpTextBox.Text)
            If PageNumber <1 Then
                PageNumber= 1
            End If
            If PageNumber > MaxPageNumber Then
                PageNumber = MaxPageNumber
            End If
            PageIndex = PageNumber
        End If
    End Sub

    Private Sub Button_Command(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "Page"
                PageIndex = CInt(e.CommandArgument)
            Case "LoadDoc"
                DoLoadDoc(New Guid(e.CommandArgument.ToString))
            Case "SelectDoc"
                DoSelectDoc(New Guid(e.CommandArgument.ToString))
            Case "ChildDoc"
                DoChildDoc(New Guid(e.CommandArgument.ToString))
            Case "Expand"
                If ExpandedGroups.ContainsKey(e.CommandArgument) Then
                    ExpandedGroups(e.CommandArgument) = Not ExpandedGroups(e.CommandArgument)
                End If
                If TableBuilded Then
                    BuildTable()
                End If
            Case "ExpandGroup"
                ExpandedGroupsLevelExpand = CInt(e.CommandArgument)
                If TableBuilded Then
                    BuildTable()
                End If
            Case "CollapseGroup"
                ExpandedGroupsLevelCollapse = CInt(e.CommandArgument)
                If TableBuilded Then
                    BuildTable()
                End If
        End Select
    End Sub

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If NeedToRefreshAfterFiltersChanged Then
            Refresh()
            'UpdatePanelTableListDoc.Update()
        End If
    End Sub

    Public Overrides Sub RaisePostBackEvent(ByVal eventArgument As String)
        Select Case eventArgument
            Case "ListDocPreFilter"
                ModalPopupExtenderListDocPreFilter.Hide()
                PlaceHolderShowListDocPreFilter.Visible = False
                If ListDocDataTable Is Nothing Then
                    NeedToRefreshAfterFiltersChanged = True
                End If
            Case Else
                MyBase.RaisePostBackEvent(eventArgument)
        End Select
        'UpdateParentUpdatePanel()
    End Sub
  
End Class
