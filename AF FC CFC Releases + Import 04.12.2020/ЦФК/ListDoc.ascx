﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ListDoc.ascx.vb" Inherits="WebApplication.ListDoc" %>

<asp:UpdatePanel ID="UpdatePanelTableListDoc" runat="server" UpdateMode="Conditional"
    RenderMode="Inline">
    <ContentTemplate>
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td align="left">
                    <asp:Table ID="ViewFunctionsTable" runat="server" Style="margin-bottom: 1px;" />
                </td>
                <td align="right">
                    <asp:Table ID="RoleFunctionsTable" runat="server" Style="margin-bottom: 1px; font-size: 90%;" />
                </td>
            </tr>
        </table>
        <asp:Table ID="TableListDoc" runat="server" Width="100%" CssClass="Grid" />
    </ContentTemplate>
</asp:UpdatePanel>
<asp:PlaceHolder ID="PlaceHolderShowListDocPreFilter" runat="server" Visible="false">
    <asp:Panel ID="PanelListDocPreFilter" runat="server" Style="display: none;" Width="300">
        <asp:Panel ID="PanelListDocPreFilterTitle" runat="server" CssClass="PanelInfo">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="cursor: default;">
                        <asp:Label ID="LabelListDocPreFilterTitle" runat="server" Text="<%$ Resources:eqResources, CAP_PreFilter %>" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel ID="PanelListDocPreFilterContent" runat="server" ScrollBars="Auto" CssClass="PanelPlaceHolder">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td colspan="2">
                        <asp:PlaceHolder ID="PlaceHolderListDocPreFilter" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td style="padding: 2px 2px 2px 2px; width: 50%;" colspan="2">
                        <asp:Button ID="ButtonListDocPreFilterOk" runat="server" Text="<%$ Resources:eqResources, CAP_ConfirmOK %>" ToolTip="<%$ Resources:eqResources, TIP_ConfirmOK %>"
                            Width="100%" />
                    </td>
                    <%--<td style="padding: 2px 2px 2px 2px; widows: 50%;">
                        <asp:Button ID="ButtonListDocPreFilterCancel" runat="server" Text="No" ToolTip="Param-pam-pam!!! Yohu-hou!!!"
                            Width="100%" />
                    </td>--%>
                </tr>
            </table>
        </asp:Panel>
    </asp:Panel>
    <asp:Button ID="ButtonShowListDocPreFilter" Style="display: none;" runat="server" />
    <aj:ModalPopupExtender ID="ModalPopupExtenderListDocPreFilter" runat="server" PopupDragHandleControlID="PanelListDocPreFilterTitle"
        TargetControlID="ButtonShowListDocPreFilter" BackgroundCssClass="ModalBackground"
        DropShadow="true" OkControlID="ButtonListDocPreFilterOk" PopupControlID="PanelListDocPreFilter">
        <%--CancelControlID="ButtonListDocPreFilterCancel"--%>
    </aj:ModalPopupExtender>

</asp:PlaceHolder>

<aj:ModalPopupExtender ID="UploadFilePopup" runat="server" DropShadow="True" TargetControlID="hiddenBtn" BackgroundCssClass="ModalBackground" PopupControlID="UploadFilePanel"></aj:ModalPopupExtender>
<asp:Button ID="hiddenBtn" Style="display:none" runat="server"/>


<asp:Panel ID="UploadFilePanel" Style="display:none" runat="server" Height="80px" cancelControlID="ImageButtonMessagesCancel" CssClass="PanelPlaceHolder" DropShadow="true" HorizontalAlign="Center" Width="350px">
    <asp:Panel ID="PanelMessagesTitle" runat="server" CssClass="PanelInfo">
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td align="left" style="cursor: default;">
                    <asp:UpdatePanel ID="UpdatePanelUploadFile" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:FileUpload ID="FileUpload" runat="server" />
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnFileUploadOk" />
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
                <td align="right">
                    <asp:ImageButton ImageAlign="Right" ID="ImageButtonMessagesCancel" runat="server" ImageUrl="App_Themes/DefaultTheme/Images/Delete.gif"
                                        ToolTip="<%$ Resources:eqResources, CAP_Close %>" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <div style="margin-top: 15px">
        <asp:Button ID="btnFileUploadOk" style="padding-left: 15px; padding-right: 15px" runat="server" text="OK" />
        <asp:Button ID="btnFileUploadCancel" CssClass="Button" runat="server" text="Отмена" />
    </div>
</asp:Panel>

