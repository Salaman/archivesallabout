﻿Public Partial Class Login
    Inherits eqBasePage

#Region "Properties"

    Private ReadOnly Property ActiveDatabaseConnectionString() As String
        Get
            If Session("ActiveDatabaseConnectionString") IsNot Nothing Then
                Return Session("ActiveDatabaseConnectionString")
            End If
            Return My.Settings.DatabaseConnectionString
        End Get
    End Property

#End Region

#Region "Metods"

    Protected Overrides Sub ShowMessages(ByVal Messages As System.Collections.Generic.List(Of String))
        PlaceHolderMessages.Controls.Clear()
        For Each Message As String In Messages
            Dim MessagePanel As New Panel With {.CssClass = "MenuSubItemStyle"}
            Dim MessageLabel As New Label With {.Text = Message}
            MessagePanel.Controls.Add(MessageLabel)
            PlaceHolderMessages.Controls.Add(MessagePanel)
        Next
    End Sub

    Public Overrides Sub RedirectToLoginPage()
        ' This is login page
    End Sub

#End Region

#Region "Page Events"

    Private Sub Login_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Not Request.QueryString("Reason") = "Exit" Then
                If False And Not Request.Cookies(My.Settings.CookieName) Is Nothing Then
                    Try
                        Dim Login As String = Request.Cookies(My.Settings.CookieName).Item("Login")
                        Dim Cookie As String = Request.Cookies(My.Settings.CookieName).Item("Cookie")
                        AuthenticateCookie(Login, Cookie)
                    Catch ex As Exception
                        ' AddMessage("Authenticate Cookies -> " & ex.Message)
                    End Try
                End If
                If Request.IsAuthenticated Then
                    Try
                        AuthenticateWindows(Request.LogonUserIdentity.Name)
                    Catch ex As Exception
                        AddMessage("Authenticate Windows -> " & ex.Message)
                    End Try
                End If
                If Request.Form("Login") IsNot Nothing AndAlso Request.Form("Password") IsNot Nothing Then
                    Try
                        AuthenticateForms(Request.Form("Login"), Request.Form("Password"))
                    Catch ex As Exception
                        AddMessage("AutoLogin Authenticate Forms -> " & ex.Message)
                    End Try
                End If
                If Request.Form("login") IsNot Nothing AndAlso Request.Form("pass") IsNot Nothing Then
                    Try
                        AuthenticateForms(Request.Form("login"), Request.Form("pass"))
                    Catch ex As Exception
                        AddMessage("AutoLogin Authenticate Forms -> " & ex.Message)
                    End Try
                End If
                If Not String.IsNullOrEmpty(My.Settings.AnonymousUserLogin) Then
                    Try
                        AuthenticateForms(My.Settings.AnonymousUserLogin, My.Settings.AnonymousUserPassword)
                    Catch ex As Exception
                        AddMessage("Anonymous Authentication -> " & ex.Message)
                    End Try
                End If
            End If
        End If
        TextBoxLogin.Focus()
    End Sub

#End Region

#Region "Authenticate Metods"

    Sub AuthenticateWindows(ByVal Login As String)

        Dim SqlConnectionStringBuilder As New SqlClient.SqlConnectionStringBuilder(ActiveDatabaseConnectionString)
        Dim UserSqlConnection As New SqlClient.SqlConnection(SqlConnectionStringBuilder.ConnectionString)
        Try
            UserSqlConnection.Open()
        Catch ex As SqlClient.SqlException
            Throw New Exception(String.Format(GetGlobalResourceObject("eqResources", "MSG_DBConnectionFailed"), ex.Message))
        End Try

        Dim ValidateUserSqlCommand As New SqlClient.SqlCommand("SELECT TOP 1 ID FROM eqUsers WHERE Login=@Login AND Deleted=0", UserSqlConnection)
        ValidateUserSqlCommand.Parameters.AddWithValue("@Login", Login)
        Dim UserID As Object = ValidateUserSqlCommand.ExecuteScalar
        Dim AuthSuccess As Boolean = False
        If IsDBNull(UserID) Or UserID Is Nothing Then
            Throw New Exception(String.Format(GetGlobalResourceObject("eqResources", "MSG_UserNotRegistered"), Request.LogonUserIdentity.Name))
            AuthSuccess = True
        End If
        eqAppSettings.WriteActitityLog(UserSqlConnection, Login, "", AuthSuccess, Request.UserAgent.ToString(), Request.UserHostAddress, Request.ToString())


        Dim UserAppSettings As New eqAppSettings
        UserAppSettings.AdministratorUserLogin = My.Settings.AdministratorUserLogin
        UserAppSettings.RolesAllowPriority = My.Settings.RolesAllowPriority
        UserAppSettings.Manager = My.Resources.ResourceManager
        UserAppSettings.OpenSession(UserSqlConnection, Login)
        Session("AppSettings") = UserAppSettings

        RedirectFromLoginPage(UserAppSettings)

    End Sub

    Sub AuthenticateForms(ByVal Login As String, ByVal Pass As String)

        Dim SqlConnectionStringBuilder As New SqlClient.SqlConnectionStringBuilder(ActiveDatabaseConnectionString)
        Dim UserSqlConnection As New SqlClient.SqlConnection(SqlConnectionStringBuilder.ConnectionString)
        Try
            UserSqlConnection.Open()
        Catch ex As SqlClient.SqlException
            Throw New Exception(String.Format(GetGlobalResourceObject("eqResources", "MSG_DBConnectionFailed"), ex.Message))
        End Try

        Dim ValidateUserSqlCommand As New SqlClient.SqlCommand("SELECT TOP 1 ID FROM eqUsers WHERE Login=@Login AND Pass=@Pass AND Deleted=0", UserSqlConnection)
        ValidateUserSqlCommand.Parameters.AddWithValue("@Login", Login)
        ValidateUserSqlCommand.Parameters.AddWithValue("@Pass", Pass)
        Dim UserID As Object = ValidateUserSqlCommand.ExecuteScalar
        Dim AuthSuccess As Boolean = True
        If IsDBNull(UserID) Or UserID Is Nothing Then
            Throw New Exception(GetGlobalResourceObject("eqResources", "MSG_UserOrPasswordFailed"))
            AuthSuccess = False
        End If
        eqAppSettings.WriteActitityLog(UserSqlConnection, Login, Pass, AuthSuccess, Request.UserAgent.ToString(), Request.UserHostAddress, "")

        Dim UserAppSettings As New eqAppSettings
        UserAppSettings.AdministratorUserLogin = My.Settings.AdministratorUserLogin
        UserAppSettings.RolesAllowPriority = My.Settings.RolesAllowPriority
        UserAppSettings.Manager = My.Resources.ResourceManager
        UserAppSettings.OpenSession(UserSqlConnection, Login)
        Session("AppSettings") = UserAppSettings

        If False And My.Settings.CookieName <> String.Empty Then

            'Dim Cookie As String = Math.Abs(Guid.NewGuid.GetHashCode).ToString & Math.Abs(Login.GetHashCode).ToString & Math.Abs(Now.Day).ToString & Math.Abs(Guid.NewGuid.GetHashCode).ToString & Math.Abs(My.Computer.Name.GetHashCode).ToString & Math.Abs(Now.Month).ToString & Math.Abs(Guid.NewGuid.GetHashCode).ToString & Math.Abs(Now.Month).ToString & Math.Abs(Now.Day).ToString & Math.Abs(Guid.NewGuid.GetHashCode).ToString & Math.Abs(Guid.NewGuid.GetHashCode).ToString
            Dim Cookie As String = Now.Year.GetHashCode.ToString & Login.GetHashCode.ToString & Now.Month.GetHashCode.ToString & Pass.GetHashCode.ToString & Now.Day.GetHashCode.ToString
            Dim UpdateUserCookieSqlCommand As New SqlClient.SqlCommand("UPDATE eqUsers SET Cookie=@Cookie WHERE Login=@Login AND Pass=@Pass AND Deleted=0", UserSqlConnection)
            UpdateUserCookieSqlCommand.Parameters.AddWithValue("@Cookie", Cookie)
            UpdateUserCookieSqlCommand.Parameters.AddWithValue("@Login", Login)
            UpdateUserCookieSqlCommand.Parameters.AddWithValue("@Pass", Pass)
            UpdateUserCookieSqlCommand.ExecuteNonQuery()

            Response.Cookies(My.Settings.CookieName).Expires = Now.AddDays(1)
            Response.Cookies(My.Settings.CookieName).Item("Login") = Login
            Response.Cookies(My.Settings.CookieName).Item("Cookie") = Cookie

        End If

        RedirectFromLoginPage(UserAppSettings)

    End Sub

    Sub AuthenticateCookie(ByVal Login As String, ByVal Cookie As String)

        Dim SqlConnectionStringBuilder As New SqlClient.SqlConnectionStringBuilder(ActiveDatabaseConnectionString)
        Dim UserSqlConnection As New SqlClient.SqlConnection(SqlConnectionStringBuilder.ConnectionString)
        Try
            UserSqlConnection.Open()
        Catch ex As SqlClient.SqlException
            Throw New Exception(String.Format(GetGlobalResourceObject("eqResources", "MSG_DBConnectionFailed"), ex.Message))
        End Try

        Dim ValidateUserSqlCommand As New SqlClient.SqlCommand("SELECT TOP 1 ID FROM eqUsers WHERE Login=@Login AND Cookie=@Cookie AND Deleted=0", UserSqlConnection)
        ValidateUserSqlCommand.Parameters.AddWithValue("@Login", Login)
        ValidateUserSqlCommand.Parameters.AddWithValue("@Cookie", Cookie)
        Dim UserID As Object = ValidateUserSqlCommand.ExecuteScalar
        Dim AuthSuccess As Boolean = False
        If IsDBNull(UserID) Or UserID Is Nothing Then
            Throw New Exception(GetGlobalResourceObject("eqResources", "MSG_CookieDeny"))
            AuthSuccess = True
        End If
        eqAppSettings.WriteActitityLog(UserSqlConnection, Login, "", AuthSuccess, Request.UserAgent.ToString(), Request.UserHostAddress, Request.ToString())

        Dim UserAppSettings As New eqAppSettings
        UserAppSettings.AdministratorUserLogin = My.Settings.AdministratorUserLogin
        UserAppSettings.RolesAllowPriority = My.Settings.RolesAllowPriority
        UserAppSettings.Manager = My.Resources.ResourceManager
        UserAppSettings.OpenSession(UserSqlConnection, Login)
        Session("AppSettings") = UserAppSettings

        If My.Settings.CookieName <> String.Empty Then

            Response.Cookies(My.Settings.CookieName).Expires = Now.AddDays(1)
            Response.Cookies(My.Settings.CookieName).Item("Login") = Login
            Response.Cookies(My.Settings.CookieName).Item("Cookie") = Cookie

        End If

        RedirectFromLoginPage(UserAppSettings)

    End Sub

    Public Sub RedirectFromLoginPage(ByVal UserAppSettings As eqAppSettings)
        If Request.Url.ToString.ToUpper.EndsWith("DEFAULT.ASPX") OrElse Request.QueryString("ReturnURL") Is Nothing Then
            Response.Redirect(GetStartPage(UserAppSettings))
        Else
            Dim ReturnURL As String = Server.UrlDecode(Request.QueryString("ReturnURL").ToString).Trim
            Response.Redirect(ReturnURL)
        End If
    End Sub

    Private Function GetStartPage(ByVal UserAppSettings As eqAppSettings) As String
        Dim eqSecurities As New eqSecurities(UserAppSettings)
        For Each UserRole In AppSettings.UserSecurities.Roles
            If UserRole("StartPage").ToString.Trim <> "" Then
                Return UserRole("StartPage").ToString.Trim
            End If
        Next
        Return GetAppRelativeURL(DefaultPageUrl)
    End Function

#End Region

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Not Web.UI.ScriptManager.GetCurrent(Page).IsInAsyncPostBack Then
            ' Высота для XHTML 1.0 Transitional
            Dim SetPanelLoginPaddingTopSB As New StringBuilder
            SetPanelLoginPaddingTopSB.AppendLine("function SetPanelLoginPaddingTop()")
            SetPanelLoginPaddingTopSB.AppendLine("{")
            SetPanelLoginPaddingTopSB.AppendLine(" document.getElementById('" & PanelLoginPadding.ClientID & "').style.height = Math.max(0,GetWindowHeight()/2-120)+'px';")
            SetPanelLoginPaddingTopSB.AppendLine("}")
            SetPanelLoginPaddingTopSB.AppendLine("SetPanelLoginPaddingTop();")
            SetPanelLoginPaddingTopSB.AppendLine("$addHandler(window,'resize',SetPanelLoginPaddingTop);")
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "SetPanelLoginPaddingTop", SetPanelLoginPaddingTopSB.ToString, True)
        End If
        'PanelLogin
    End Sub

    Private Sub ButtonLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonLogin.Click
        Try
            AuthenticateForms(TextBoxLogin.Text, TextBoxPassword.Text)
        Catch ex As Exception
            AddMessage("Authenticate Forms -> " & ex.Message)
        End Try
        TextBoxLogin.Focus()
    End Sub

End Class