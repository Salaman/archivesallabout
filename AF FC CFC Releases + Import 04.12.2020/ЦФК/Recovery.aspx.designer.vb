﻿'------------------------------------------------------------------------------
' <автоматически создаваемое>
'     Этот код создан программой.
'
'     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
'     повторной генерации кода. 
' </автоматически создаваемое>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Recovery

    '''<summary>
    '''Head1 элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents Head1 As Global.System.Web.UI.HtmlControls.HtmlHead

    '''<summary>
    '''form1 элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm

    '''<summary>
    '''ToolkitScriptManagerDefault элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents ToolkitScriptManagerDefault As Global.AjaxControlToolkit.ToolkitScriptManager

    '''<summary>
    '''ButtonRecovery элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents ButtonRecovery As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''InvStructOptions элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents InvStructOptions As Global.System.Web.UI.HtmlControls.HtmlTable

    '''<summary>
    '''ButtonSelectChildren элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents ButtonSelectChildren As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''UpdatePanelRecovery элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents UpdatePanelRecovery As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''ListDocRecovery элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents ListDocRecovery As Global.WebApplication.ListDoc
End Class
