﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ListDocFilter.ascx.vb"
    Inherits="WebApplication.ListDocFilter" %>
<asp:UpdatePanel ID="UpdPan" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:Panel ID="DefBntPan" runat="server" DefaultButton="DefBnt">
            <table width="100%" cellpadding="0" cellspacing="1" style="background-color: white;
                border: solid 1px gray;">
                <tr>
                    <td style="width: 1%; vertical-align: middle;">
                        <asp:ImageButton ID="Show" runat="server" ImageUrl="App_Themes/DefaultTheme/Images/Filter.gif"
                            ToolTip="<%$ Resources:eqResources, TIP_Filters %>"  />
                        <asp:PlaceHolder ID="PlaceHolderShowFilters" runat="server" Visible="false">
                            <asp:Panel ID="PanelFilters" runat="server" Style="display: none;" Width="300">
                                <asp:Panel ID="PanelFiltersTitle" runat="server" CssClass="PanelInfo" Style="cursor: default;">
                                    <asp:Label ID="LabelFiltersTitle" runat="server" Text="<%$ Resources:eqResources, CAP_Filter %>" />
                                </asp:Panel>
                                <asp:Panel ID="PanelFiltersContent" runat="server" CssClass="PanelPlaceHolder" DefaultButton="ButtonConfirmOk">
                                    <table width="100%" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td colspan="2">
                                                <table width="100%" class="LogicBlockTable">
                                                    <colgroup>
                                                        <col width="50px" />
                                                        <col />
                                                    </colgroup>
                                                    <tr>
                                                        <td>
                                                            <asp:HiddenField ID="Operator1HiddenField" runat="server" Visible="false" />
                                                            <asp:HiddenField ID="Operator1HiddenFieldNew" runat="server" Visible="false" />
                                                            <asp:DropDownList ID="Operator1DropDownList" runat="server" Width="100%" OnTextChanged="Operator1DropDownList_OnTextChanged" />
                                                        </td>
                                                        <td>
                                                            <asp:HiddenField ID="Filter1HiddenField" runat="server" Visible="false" />
                                                            <asp:HiddenField ID="Filter1HiddenFieldNew" runat="server" Visible="false" />
                                                            <eq:eqTextBox ID="Filter1TextBox" runat="server" OnTextChanged="Filter1TextBox_OnTextChanged" />
                                                            <asp:DropDownList ID="Filter1DropDownList" runat="server" OnSelectedIndexChanged="Filter1DropDownList_OnSelectedIndexChanged" />
                                                            <asp:CheckBox ID="Filter1CheckBox" runat="server" OnCheckedChanged="Filter1CheckBox_OnCheckedChanged" />
                                                            <aj:CalendarExtender ID="Filter1CalendarExtender" runat="server" TargetControlID="Filter1TextBox" />
                                                            <aj:FilteredTextBoxExtender ID="Filter1FilteredTextBoxExtender" runat="server" TargetControlID="Filter1TextBox" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" align="center" style="text-align: center;">
                                                            <asp:HiddenField ID="Logic12HiddenField" runat="server" Visible="false" />
                                                            <asp:HiddenField ID="Logic12HiddenFieldNew" runat="server" Visible="false" />
                                                            <asp:RadioButtonList ID="Logic12RadioButtonList" runat="server">
                                                                <asp:ListItem Text="<%$ Resources:eqResources, CAP_And %>" Value="AND" />
                                                                <asp:ListItem Text="<%$ Resources:eqResources, CAP_Or %>" Value="OR" />
                                                            </asp:RadioButtonList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:HiddenField ID="Operator2HiddenField" runat="server" Visible="false" />
                                                            <asp:HiddenField ID="Operator2HiddenFieldNew" runat="server" Visible="false" />
                                                            <asp:DropDownList ID="Operator2DropDownList" runat="server" Width="100%" OnTextChanged="Operator2DropDownList_OnTextChanged" />
                                                        </td>
                                                        <td>
                                                            <asp:HiddenField ID="Filter2HiddenField" runat="server" Visible="false" />
                                                            <asp:HiddenField ID="Filter2HiddenFieldNew" runat="server" Visible="false" />
                                                            <eq:eqTextBox ID="Filter2TextBox" runat="server" OnTextChanged="Filter2TextBox_OnTextChanged" />
                                                            <asp:DropDownList ID="Filter2DropDownList" runat="server" OnSelectedIndexChanged="Filter2DropDownList_OnSelectedIndexChanged" />
                                                            <asp:CheckBox ID="Filter2CheckBox" runat="server" OnCheckedChanged="Filter2CheckBox_OnCheckedChanged" />
                                                            <aj:CalendarExtender ID="Filter2CalendarExtender" runat="server" TargetControlID="Filter2TextBox" />
                                                            <aj:FilteredTextBoxExtender ID="Filter2FilteredTextBoxExtender" runat="server" TargetControlID="Filter2TextBox" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 2px 2px 2px 2px; width: 50%;">
                                                <asp:Button ID="ButtonConfirmOk" runat="server" Text="<%$ Resources:eqResources, CAP_ConfirmAccept %>" Width="100%" />
                                            </td>
                                            <td style="padding: 2px 2px 2px 2px; widows: 50%;">
                                                <asp:Button ID="ButtonConfirmCancel" runat="server" Text="<%$ Resources:eqResources, CAP_ConfirmCancel %>" Width="100%" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </asp:Panel>
                            <aj:ModalPopupExtender ID="ModalPopupExtenderFilters" runat="server" TargetControlID="Show"
                                BackgroundCssClass="ModalBackground" DropShadow="true" CancelControlID="ButtonConfirmCancel"
                                PopupControlID="PanelFilters">
                            </aj:ModalPopupExtender>
                        </asp:PlaceHolder>
                    </td>
                    <td style="vertical-align: middle;">
                        <asp:DropDownList ID="qdd" runat="server" Visible="false" AutoPostBack="true" Style="min-width: 100%;
                            font-size: 95%; border: none;" />
                        <asp:TextBox ID="qtb" runat="server" autocomplete="off" AutoPostBack="false" onkeyup="if (this.value.length > this.size) {this.size=this.value.length;}"
                            Style="border: solid 0px white; width: 100%; min-width: 100%; font-size: 95%;
                            margin-right: 4px;" />
                        <aj:AutoCompleteExtender ID="qtbe" runat="server" TargetControlID="qtb" ServicePath="ListDocFilterAutoComplete.asmx"
                            ServiceMethod="GetAutoComplete" DelimiterCharacters=" " UseContextKey="true"
                            MinimumPrefixLength="2" />
                    </td>
                </tr>
            </table>
            <asp:Button ID="DefBnt" runat="server" Style="display: none;" />
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
