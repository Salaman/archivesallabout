﻿Imports System
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient


Partial Public Class RecalcFund
    Inherits BasePage

    ''Private backupThread As Threading.Thread
    ''Private worningPanel_Visible = True

    ''Private Event eWorningClosed()

    'Private Sub worningClosed() Handles Me.eWorningClosed
    '    SyncLock worningPanel
    '        worningPanel_Visible = False
    '        Me.Page_Load(Me, EventArgs.Empty)
    '    End SyncLock

    'End Sub



    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ''worningPanel.Visible = worningPanel_Visible
    End Sub

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        MultiViewTabs.ActiveViewIndex = CInt(MenuTabs.SelectedValue)
    End Sub

    Private Sub wWindowEnabled(ByVal fl As Boolean)
        worningAnsYes.Enabled = fl
        worningAnsNo.Enabled = fl
        worningAnsCancel.Enabled = fl
        worningLabel.Visible = Not fl
        If fl Then
            worningLabel.Text = ""
        Else
            worningLabel.Text = "Идет процесс резервного копирования..."
        End If

    End Sub

    Private Sub worningAnsYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles worningAnsYes.Click
        wWindowEnabled(False)

        Try
            ''backupThread = New Threading.Thread(New Threading.ThreadStart(AddressOf backupMake))



            ''backupThread.Start()
            backupMake()
            worningPanel.Visible = False
        Catch ex As Exception
            wWindowEnabled(True)
            worningLabel.Text = ex.Message
            worningAnsCancel.Enabled = True
        End Try

    End Sub

    Private Sub worningAnsNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles worningAnsNo.Click
        worningPanel.Visible = False
    End Sub

    Private Sub worningAnsCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles worningAnsCancel.Click
        CloseWindow()
    End Sub

    Private Sub bClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles bClose.Click
        CloseWindow()
    End Sub

    Private Sub bGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles bGo.Click
        Dim r As New Recalc(AppSettings)

        r.Command.Parameters.AddWithValue("@SROK_HRAN", c_SROK_HRAN.Checked)
        r.Command.Parameters.AddWithValue("@VVEDENO_O", c_VVEDENO_O.Checked)
        r.Command.Parameters.AddWithValue("@NACH_DATA", c_NACH_DATA.Checked)
        r.Command.Parameters.AddWithValue("@KON_DATA", c_KON_DATA.Checked)
        r.Command.Parameters.AddWithValue("@HAS_MUSEUM", c_HAS_MUSEUM.Checked)
        r.Command.Parameters.AddWithValue("@TREASURE_COUNT", c_TREASURE_COUNT.Checked)

        r.Command.Parameters.AddWithValue("@SUM_AA", c_SUM_AA.Checked)
        r.Command.Parameters.AddWithValue("@SUM_BO", c_SUM_BO.Checked)
        r.Command.Parameters.AddWithValue("@SUM_TN", c_SUM_TN.Checked)
        r.Command.Parameters.AddWithValue("@SUM_EN", c_SUM_EN.Checked)

        r.Command.Parameters.AddWithValue("@ERH_ALL", c_ERH_ALL.Checked)
        r.Command.Parameters.AddWithValue("@EU_ALL", c_EU_ALL.Checked)
        r.Command.Parameters.AddWithValue("@ERHO_ALL", c_ERHO_ALL.Checked)
        r.Command.Parameters.AddWithValue("@EUO_ALL", c_EUO_ALL.Checked)
        r.Command.Parameters.AddWithValue("@NEOP_EHR", c_NEOP_EHR.Checked)
        r.Command.Parameters.AddWithValue("@EHR_REG", c_EHR_REG.Checked)
        r.Command.Parameters.AddWithValue("@EU_REG", c_EU_REG.Checked)
        r.Command.Parameters.AddWithValue("@ERH_OC", c_ERH_OC.Checked)
        r.Command.Parameters.AddWithValue("@EU_OC", c_EU_OC.Checked)
        r.Command.Parameters.AddWithValue("@ERH_U", c_ERH_U.Checked)
        r.Command.Parameters.AddWithValue("@EU_U", c_EU_U.Checked)
        r.Command.Parameters.AddWithValue("@EHR_SF", c_EHR_SF.Checked)
        r.Command.Parameters.AddWithValue("@EU_SF", c_EU_SF.Checked)
        r.Command.Parameters.AddWithValue("@EHR_FP", c_EHR_FP.Checked)
        r.Command.Parameters.AddWithValue("@EU_FP", c_EU_FP.Checked)
        r.Command.Parameters.AddWithValue("@EHR_NEOB", c_EHR_NEOB.Checked)
        r.Command.Parameters.AddWithValue("@EU_NEOB", c_EU_NEOB.Checked)
        r.Command.Parameters.AddWithValue("@EHR_SEC", c_EHR_SEC.Checked)
        r.Command.Parameters.AddWithValue("@EU_SEC", c_EU_SEC.Checked)
        r.Command.Parameters.AddWithValue("@EHR_CAT", c_EHR_CAT.Checked)
        r.Command.Parameters.AddWithValue("@EU_CAT", c_EU_CAT.Checked)

        r.Command.Parameters.AddWithValue("@E_KARTED", c_E_KARTED.Checked)
        r.Command.Parameters.AddWithValue("@E_SHIFR", c_E_SHIFR.Checked)
        r.Command.Parameters.AddWithValue("@E_KART", c_E_KART.Checked)
        r.Command.Parameters.AddWithValue("@E_ZAM_OBL", c_E_ZAM_OBL.Checked)
        r.Command.Parameters.AddWithValue("@E_POVR", c_E_POVR.Checked)
        r.Command.Parameters.AddWithValue("@E_ZAT", c_E_ZAT.Checked)
        r.Command.Parameters.AddWithValue("@E_RESTAVR", c_E_RESTAVR.Checked)
        r.Command.Parameters.AddWithValue("@E_GOR", c_E_GOR.Checked)
        r.Command.Parameters.AddWithValue("@E_PODSH", c_E_PODSH.Checked)
        r.Command.Parameters.AddWithValue("@E_KPO", c_E_KPO.Checked)
        r.Command.Parameters.AddWithValue("@E_DEZINF", c_E_DEZINF.Checked)
        r.Command.Parameters.AddWithValue("@E_DEZINS", c_E_DEZINS.Checked)

        r.Command.Parameters.AddWithValue("@HRAN_FOND", c_HRAN_FOND.Checked)

        Try
            Select Case ModalDialogArgument.Argument
                Case "One"
                    r.Calc(Recalc.Type.Fund, DirectCast(ModalDialogArgument.ArgumentObject, Guid).ToString())
                Case "All"
                    r.Calc(Recalc.Type.Fund)
            End Select
        Catch ex As Exception
            AddMessage("Не удалось выполнить расчет: >> " & ex.Message)
        End Try

    End Sub

#Region "Бессмысленно-беспощадная копипаста из Service.ascx.vb"

    Private Sub backupMake()
        Dim SQL As New StringBuilder
        SQL.AppendFormat("BACKUP DATABASE [{0}]", BackupDatabaseName()).AppendLine()
        SQL.AppendFormat("TO DISK = N'{0}'", BackupDatabasePath()).AppendLine()
        SQL.AppendFormat("WITH NAME = N'{0} - Full Database Backup'", BackupDatabaseName()).AppendLine()
        'If Overwrite.Checked Then
        'SQL.AppendLine(", INIT")
        'End If

        Dim cmd = New SqlCommand With {.CommandTimeout = 3600}
        cmd.CommandText = SQL.ToString

        'Threading.Thread.Sleep(5000)

        Try
            AppSettings.ExecCommand(cmd)
            BackupDataWrite()
            'If BackupDataWrite() Then
            '    If SaveDoc() Then
            '        Response.Redirect(BasePage.GetDocURL(DocTypeURL, DocID, BasePage.IsModal))
            '    End If
            'End If

            ''RaiseEvent eWorningClosed()

        Catch ex As Exception
            'BasePage.AddMessage("Ошибка во время создания резервной копии: " & ex.Message)
            wWindowEnabled(True)
            worningLabel.Text = ex.Message
            'worningAnsCancel.Enabled = True
        End Try
    End Sub

    Private Function BackupDatabaseName() As String
        Dim cmd As New SqlCommand("SELECT db_name()")
        Return AppSettings.ExecCommandScalar(cmd)
    End Function

    Private Function BackupDatabasePath() As String
        Dim cmd As New SqlCommand("SELECT physical_name FROM sys.database_files WHERE file_id = 1")
        Dim result = AppSettings.ExecCommandScalar(cmd)
        result = Path.Combine(Path.GetFullPath(Path.Combine(result, "..\..")), String.Format("Backup\{0}.bak", BackupDatabaseName))
        Return result
    End Function

    Private Function BackupDataWrite() As Boolean
        Dim SQL As New StringBuilder
        SQL.AppendFormat("RESTORE HEADERONLY").AppendLine()
        SQL.AppendFormat("FROM DISK = N'{0}'", BackupDatabasePath()).AppendLine()
        SQL.AppendFormat("WITH NOUNLOAD").AppendLine()

        Dim cmd As New SqlCommand With {.CommandTimeout = 3600}
        cmd.CommandText = SQL.ToString

        Dim dt = AppSettings.GetDataTable(cmd)
        Dim v = dt.DefaultView
        v.Sort = "Position DESC" ' Последний бэкап

        'If Overwrite.Checked Then
        '    SQL.Remove(0, SQL.Length)
        '    SQL.AppendLine("DELETE tblServiceLog")

        '    cmd.CommandText = SQL.ToString
        '    AppSettings.ExecCommand(cmd)
        'End If

        SQL.Remove(0, SQL.Length)
        SQL.AppendLine("INSERT tblServiceLog (ID, DocID, OwnerID, CreationDateTime, RowID, BackupSetGUID, BackupPath)")
        SQL.AppendLine("VALUES (newid(), @DocID, @OwnerID, @CreationDateTime, 0, @BackupSetGUID, @BackupPath)")

        cmd.CommandText = SQL.ToString
        cmd.Parameters.AddWithValue("@DocID", "09f6b3eb-48f0-4e08-b151-15be4481c472") ' DocID из tblService
        cmd.Parameters.AddWithValue("@OwnerID", AppSettings.UserInfo.UserID)
        cmd.Parameters.AddWithValue("@CreationDateTime", v(0)("BackupFinishDate"))
        cmd.Parameters.AddWithValue("@BackupSetGUID", v(0)("BackupSetGUID"))
        cmd.Parameters.AddWithValue("@BackupPath", BackupDatabasePath())
        Dim result = AppSettings.ExecCommand(cmd)
        Return (result = 1)
    End Function

#End Region


End Class