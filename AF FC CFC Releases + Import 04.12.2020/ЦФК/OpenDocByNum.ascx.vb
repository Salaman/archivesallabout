﻿Public Partial Class OpenDocByNum
    Inherits BaseUserControl

    Public Property DocTypeURL() As String
        Get
            Return ViewState("DocTypeURL")
        End Get
        Set(ByVal value As String)
            ViewState("DocTypeURL") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        OpenDoc.ImageUrl = BasePage.GetThemeRelativeURL("Images\Search.gif")
    End Sub

    Private Sub OpenDoc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles OpenDoc.Click
        DoOpenDoc()
    End Sub

    Private Sub DoOpenDoc()
        If IsNumeric(DocNum.Text) Then
            Dim DocTyle = AppSettings.GetDocType(DocTypeURL)

            Dim FindByDocNumSqlCommand As New SqlClient.SqlCommand("SELECT ID FROM [" & DocTyle.GetHeaderTableName & "] WHERE DocNum=@DocNum AND Deleted=0")
            FindByDocNumSqlCommand.Parameters.AddWithValue("@DocNum", CInt(DocNum.Text))
            Dim FindByDocNumResult As DataTable = AppSettings.GetDataTable(FindByDocNumSqlCommand)

            Select Case FindByDocNumResult.Rows.Count
                Case 0
                    AddMessage("Документа с заданным номером не найден")
                Case 1
                    Dim DocID = DirectCast(FindByDocNumResult.Rows(0).Item("ID"), Guid)
                    BasePage.OpenWindow(BasePage.GetDocURL(DocTypeURL, DocID, False))
                Case Else
                    AddMessage("Документов с заданным номером найдено " & FindByDocNumResult.Rows.Count & " экземпляра")
            End Select

        Else
            AddMessage("Введен некорректный номер документа")
        End If
    End Sub

End Class