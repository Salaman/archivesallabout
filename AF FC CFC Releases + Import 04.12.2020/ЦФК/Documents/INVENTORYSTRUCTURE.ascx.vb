﻿Imports System.Data
Imports System.Data.SqlClient

Partial Public Class INVENTORYSTRUCTURE
    Inherits BaseDoc

#Region "События"

    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        ' AW: переназначаем ID чтоб значение правильно считывалось ядром
        If Request.QueryString("INVENTORY") IsNot Nothing Then
            Crumbs.ISN_INVENTORY.ID = "ISN_INVENTORY_REF"
        ElseIf Request.QueryString("FUND") IsNot Nothing Then
            Crumbs.ISN_FUND.ID = "ISN_FUND_REF"
        ElseIf Request.QueryString("ARCHIVE") IsNot Nothing Then
            Crumbs.ISN_ARCHIVE.ID = "ISN_ARCHIVE_REF"
        End If
    End Sub

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Crumbs.ISN_INVENTORY.DocID <> Guid.Empty AndAlso Not IsDBNull(Crumbs.ISN_INVENTORY.DataSetHeaderDataRow("ISN_FUND")) Then
                Crumbs.ISN_FUND.KeyValue = Crumbs.ISN_INVENTORY.DataSetHeaderDataRow("ISN_FUND")
            End If
            If Crumbs.ISN_FUND.DocID <> Guid.Empty AndAlso Not IsDBNull(Crumbs.ISN_FUND.DataSetHeaderDataRow("ISN_ARCHIVE")) Then
                Crumbs.ISN_ARCHIVE.KeyValue = Crumbs.ISN_FUND.DataSetHeaderDataRow("ISN_ARCHIVE")
            End If
            If Request.QueryString("ParentID") IsNot Nothing Then
                ISN_HIGH_INVENTORY_CLS.DocID = New Guid(Request.QueryString("ParentID"))
            End If
        End If
    End Sub

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        HyperLinkUNITs.Enabled = DocStateInfo.StateID <> Guid.Empty

        '(Roman) добавил "&INVENTORY=" & Request.Item("INVENTORY") чтобы исправить ошибку с невозможностью создать ед.хр.
        'в случае, когда раздел описи общий для фонда или архива
        'Подозреваю, что будет сайд-эффект в виде некорректной работы во всех оставшихся случаях
        Dim GETParamString = "INVENTORYSTRUCTURE=" & DocID.ToString
        If Request.Item("INVENTORY") <> "" Then
            GETParamString += "&INVENTORY=" & Request.Item("INVENTORY")
        End If
        HyperLinkUNITs.NavigateUrl = BasePage.GetListDocURL("UNIT.ascx", False, GETParamString)
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Dim bRoot = RelIsRoot()
        Dim bEmpty = False

        Select Case KIND.SelectedValue
            Case "700" ' архив
                Crumbs.ISN_FUND.Visible = False
                Crumbs.ISN_INVENTORY.Visible = False
                KIND.Items.FindByValue("702").Enabled = False
                KIND.Items.FindByValue("701").Enabled = False
                bEmpty = RelIsEmpty(Crumbs.ISN_ARCHIVE.KeyValue, 0, 0)
            Case "701" ' фонд
                Crumbs.ISN_INVENTORY.Visible = False
                KIND.Items.FindByValue("702").Enabled = False
                bEmpty = RelIsEmpty(0, Crumbs.ISN_FUND.KeyValue, 0)
            Case "702" ' опись
                bEmpty = RelIsEmpty(0, 0, Crumbs.ISN_INVENTORY.KeyValue)
            Case Else ' корень - структура описи
                Crumbs.ISN_ARCHIVE.Visible = False
                Crumbs.ISN_FUND.Visible = False
                Crumbs.ISN_INVENTORY.Visible = False
        End Select

        If bEmpty Then
            Dim RootISN = Nothing 'AppSettings.ExecCommandScalar(New SqlCommand("select top 1 ISN_INVENTORY_CLS from tblINVENTORY_STRUCTURE where ISN_HIGH_INVENTORY_CLS is null and Deleted = 0"))
            'If Not IsDBNull(RootISN) Then
            ISN_HIGH_INVENTORY_CLS.KeyValue = RootISN
            'End If
        End If
        If bRoot Or bEmpty Then
            ISN_HIGH_INVENTORY_CLS.ListDocMode = eqBaseContainer.ListDocModes.None
            ISN_HIGH_INVENTORY_CLS.LoadDocMode = eqBaseContainer.LoadDocModes.None
        End If
        KIND.Enabled = bRoot AndAlso GetLock(LockID)
    End Sub

#End Region

#Region "Ядро"

    Public Overrides Function NewDoc() As Boolean
        NewDoc = MyBase.NewDoc
        If NewDoc Then
            If Request.QueryString("ARCHIVE") IsNot Nothing Then
                Crumbs.ISN_ARCHIVE.DocID = New Guid(Request.QueryString("ARCHIVE"))
                KIND.SelectedValue = "700"
            ElseIf Request.QueryString("FUND") IsNot Nothing Then
                Crumbs.ISN_FUND.DocID = New Guid(Request.QueryString("FUND"))
                DataSetHeaderDataRow("ISN_FUND") = Crumbs.ISN_FUND.KeyValue
                KIND.SelectedValue = "701"
            ElseIf Request.QueryString("INVENTORY") IsNot Nothing Then
                Crumbs.ISN_INVENTORY.DocID = New Guid(Request.QueryString("INVENTORY"))
                DataSetHeaderDataRow("ISN_INVENTORY") = Crumbs.ISN_INVENTORY.KeyValue
                KIND.SelectedValue = "702"
            Else
                Dim ArchiveID = AppSettings.ExecCommandScalar(New SqlCommand("select top 1 ID from tblARCHIVE where Deleted = 0"))
                If Not IsDBNull(ArchiveID) Then
                    Crumbs.ISN_ARCHIVE.DocID = ArchiveID
                End If
                KIND.SelectedValue = "700"
            End If
        End If
    End Function

    Public Overrides Function SaveDoc() As Boolean
        Dim IsnArchive, IsnFund, IsnInventory
        Select Case KIND.SelectedValue
            Case "700" ' архив
                IsnArchive = Crumbs.ISN_ARCHIVE.KeyValue
                IsnFund = DBNull.Value
                IsnInventory = DBNull.Value
                DataSetHeaderDataRow("DocID") = Crumbs.ISN_ARCHIVE.DocID
            Case "701" ' фонд
                IsnArchive = DBNull.Value
                IsnFund = Crumbs.ISN_FUND.KeyValue
                IsnInventory = DBNull.Value
                DataSetHeaderDataRow("DocID") = Crumbs.ISN_FUND.DocID
            Case "702" ' опись
                IsnArchive = DBNull.Value
                IsnFund = DBNull.Value
                IsnInventory = Crumbs.ISN_INVENTORY.KeyValue
                DataSetHeaderDataRow("DocID") = Crumbs.ISN_INVENTORY.DocID
        End Select
        SaveDoc = MyBase.SaveDoc
        If SaveDoc Then
            RelUpdate(IsnArchive, IsnFund, IsnInventory)
            Return LoadDoc(DocID)
        End If
    End Function

#End Region

#Region "Методы"

    Private Function RelIsRoot() As Boolean
        Dim SQL = <sql>
                        select count(ID)
                        from tblINVENTORY_STRUCTURE
                        where ID = @ID 
                            and ISN_HIGH_INVENTORY_CLS IS NULL

                            --IN 
                            --(select ISN_INVENTORY_CLS from tblINVENTORY_STRUCTURE where ISN_HIGH_INVENTORY_CLS IS NULL and Deleted = 0)
                            --and Deleted = 0
                </sql>
        Dim cmd As New SqlCommand(SQL.Value)
        cmd.Parameters.AddWithValue("@ID", DocID)

        Dim Result = AppSettings.ExecCommandScalar(cmd)
        Return Result > 0
    End Function

    Private Function RelIsEmpty(ByVal ARCHIVE As Object, ByVal FUND As Object, ByVal INVENTORY As Object) As Boolean
        Dim SQL = <sql>
                        select count(ID)
                        from vInventoryStructure
                        where isnull(ISN_ARCHIVE_REF,0) = @ARCHIVE and isnull(ISN_FUND_REF,0) = @FUND and isnull(ISN_INVENTORY_REF,0) = @INVENTORY 
                            and Deleted = 0
                </sql>
        Dim cmd As New SqlCommand(SQL.Value)
        cmd.Parameters.AddWithValue("@ARCHIVE", ARCHIVE)
        cmd.Parameters.AddWithValue("@FUND", FUND)
        cmd.Parameters.AddWithValue("@INVENTORY", INVENTORY)

        Dim Result = AppSettings.ExecCommandScalar(cmd)
        Return Result = 0
    End Function

    Private Function RelUpdate(ByVal ARCHIVE As Object, ByVal FUND As Object, ByVal INVENTORY As Object) As Boolean
        Dim SQL = <sql>
                        with cte AS (
                            select ID, ISN_INVENTORY_CLS, ISN_HIGH_INVENTORY_CLS
                            from tblINVENTORY_STRUCTURE
                            where ID = @ID
                        union all
                            select T.ID, T.ISN_INVENTORY_CLS, T.ISN_HIGH_INVENTORY_CLS
                            from tblINVENTORY_STRUCTURE T
                            inner join cte on T.ISN_HIGH_INVENTORY_CLS = CTE.ISN_INVENTORY_CLS
                        )
                        update tblINVENTORY_STRUCTURE
                        set ISN_ARCHIVE = @ARCHIVE, ISN_FUND = @FUND, ISN_INVENTORY = @INVENTORY
                        from tblINVENTORY_STRUCTURE
                        inner join cte on CTE.ID = tblINVENTORY_STRUCTURE.ID
                  </sql>
        Dim cmd As New SqlCommand(SQL.Value)
        cmd.Parameters.AddWithValue("@ID", DocID)
        cmd.Parameters.AddWithValue("@ARCHIVE", ARCHIVE)
        cmd.Parameters.AddWithValue("@FUND", FUND)
        cmd.Parameters.AddWithValue("@INVENTORY", INVENTORY)

        Dim Result = AppSettings.ExecCommand(cmd)
        Return Result > 0
    End Function

#End Region

#Region "Документ"

    Public Overrides Function DeleteDoc() As Boolean
        Dim DeleteRecursivelyCommand As New SqlClient.SqlCommand("dbo.DeleteRecursively") With {.CommandType = CommandType.StoredProcedure}
        DeleteRecursivelyCommand.Parameters.AddWithValue("@ISN", DataSetHeaderDataRow("ISN_INVENTORY_CLS"))
        DeleteRecursivelyCommand.Parameters.AddWithValue("@OBJ", "inventorystructure")
        AppSettings.ExecCommand(DeleteRecursivelyCommand)

        Return MyBase.DeleteDoc()
    End Function

#End Region

End Class