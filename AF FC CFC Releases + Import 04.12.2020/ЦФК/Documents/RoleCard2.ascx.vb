﻿Public Partial Class RoleCard2
    Inherits BaseDoc

    Public Overrides Function LoadDoc(ByVal DocID As System.Guid) As Boolean
        LoadDoc = MyBase.LoadDoc(DocID)
        If LoadDoc Then
            RolesDocTypeID.Items.Clear()
            Dim SqlCommand As New SqlClient.SqlCommand("SELECT ID, DocType FROM eqDocTypes WHERE Deleted=0 ORDER BY DocType")
            Dim DocTypesDataTable = AppSettings.GetDataTable(SqlCommand)
            Dim DocTypes = From DT As DataRow In DocTypesDataTable.Rows _
                           Select ID = DirectCast(DT("ID"), Guid), Name = DT("DocType").ToString
            For Each DT In DocTypes
                Dim DTRows = From DA In DataSetSpecificationDataTable("eqRolesDocumentsAccess").DefaultView _
                             Where DA("DocTypeID") = DT.ID
                If DTRows.Count > 0 Then

                End If
                'DataSetSpecificationDataTable("eqRolesDocumentsAccess").DefaultView 
                Dim Text = DT.Name
                RolesDocTypeID.Items.Add(New ListItem(Text, DT.ID.ToString))
            Next
        End If
    End Function

End Class