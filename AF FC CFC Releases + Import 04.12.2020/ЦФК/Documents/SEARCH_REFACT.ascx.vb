﻿Partial Public Class SEARCH_REFACT
    Inherits BaseDoc

    Private ListDocRESULT As ListDoc

    Private Property SelectedAct() As Guid
        Get
            If ViewState("SelectedAct") Is Nothing Then
                Return Guid.Empty
            Else
                Return ViewState("SelectedAct")
            End If
        End Get
        Set(ByVal value As Guid)
            ViewState("SelectedAct") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        LoadListDocRESULT()
    End Sub

    Private Sub LoadListDocRESULT()
        PlaceHolderRESULT.Controls.Clear()
        ListDocRESULT = DirectCast(LoadControl("../ListDoc.ascx"), ListDoc)
        ListDocRESULT.ID = MenuRESULT.SelectedValue.Replace(".ascx", "")
        ListDocRESULT.DocTypeURL = MenuRESULT.SelectedValue
        ListDocRESULT.LoadDocMode = eqBaseContainer.LoadDocModes.NewWindow
        ListDocRESULT.SelectDocMode = eqBaseContainer.SelectDocModes.Multi
        FormWhereStrings()
        PlaceHolderRESULT.Controls.Add(ListDocRESULT)
    End Sub

    Private Sub ListDocActs_SelectDoc(ByVal DocID As System.Guid) Handles ListDocActs.SelectDoc
        SelectedAct = DocID
        FormWhereStrings()
        ListDocRESULT.Refresh()
    End Sub

    Private Sub FormWhereStrings()
        If SelectedAct <> Guid.Empty Then
            Select Case ListDocRESULT.DocTypeURL
                Case "FUND.ascx"
                    ListDocRESULT.AdditionalFromString = "LEFT JOIN tblREF_ACT SR ON SR.ISN_OBJ=[" & ListDocRESULT.DocType.GetHeaderTableName & "].ISN_FUND" & vbCrLf & _
                                                         "LEFT JOIN tblACT SA ON SA.ISN_ACT=SR.ISN_ACT"
                    ListDocRESULT.AdditionalWhereString = "AND SR.KIND=701 AND SA.ID='" & SelectedAct.ToString & "'"
                Case "INVENTORY.ascx"
                    ListDocRESULT.AdditionalFromString = "LEFT JOIN tblREF_ACT SR ON SR.ISN_OBJ=[" & ListDocRESULT.DocType.GetHeaderTableName & "].ISN_INVENTORY" & vbCrLf & _
                                                         "LEFT JOIN tblACT SA ON SA.ISN_ACT=SR.ISN_ACT"
                    ListDocRESULT.AdditionalWhereString = "AND SR.KIND=702 AND SA.ID='" & SelectedAct.ToString & "'"
                Case "DEPOSIT.ascx"
                    ListDocRESULT.AdditionalFromString = "LEFT JOIN tblREF_ACT SR ON SR.ISN_OBJ=[" & ListDocRESULT.DocType.GetHeaderTableName & "].ISN_DEPOSIT" & vbCrLf & _
                                                         "LEFT JOIN tblACT SA ON SA.ISN_ACT=SR.ISN_ACT"
                    ListDocRESULT.AdditionalWhereString = "AND SR.KIND=707 AND SA.ID='" & SelectedAct.ToString & "'"
                Case "UNIT.ascx"
                    ListDocRESULT.AdditionalFromString = "LEFT JOIN tblREF_ACT SR ON SR.ISN_OBJ=[" & ListDocRESULT.DocType.GetHeaderTableName & "].ISN_UNIT" & vbCrLf & _
                                                         "LEFT JOIN tblACT SA ON SA.ISN_ACT=SA.ISN_ACT"
                    ListDocRESULT.AdditionalWhereString = "AND (SR.KIND=703 OR SR.KIND=704) AND SA.ID='" & SelectedAct.ToString & "'"
            End Select
        Else
            ListDocRESULT.ListDocRefreshOnLoad = False
        End If
    End Sub

    Private Sub MenuRESULT_MenuItemClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MenuEventArgs) Handles MenuRESULT.MenuItemClick
        LoadListDocRESULT()
    End Sub

    Public Sub ButtonDeattach_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonDeattach.Click
        If SelectedAct <> Guid.Empty Then
            Dim cmdSB As New Text.StringBuilder
            cmdSB.AppendLine("DECLARE @ISN_ACT bigint")
            cmdSB.AppendLine("SELECT @ISN_ACT=ISN_ACT FROM tblACT WHERE ID='" & SelectedAct.ToString & "'")
            For Each doc In ListDocRESULT.SelectedDocuments
                Select Case ListDocRESULT.DocTypeURL
                    Case "FUND.ascx"
                        cmdSB.AppendLine("DELETE FROM tblREF_ACT WHERE ISN_ACT=@ISN_ACT AND KIND=701 AND DocID='" & doc.ToString & "'")
                    Case "INVENTORY.ascx"
                        cmdSB.AppendLine("DELETE FROM tblREF_ACT WHERE ISN_ACT=@ISN_ACT AND KIND=702 AND DocID='" & doc.ToString & "'")
                    Case "DEPOSIT.ascx"
                        cmdSB.AppendLine("DELETE FROM tblREF_ACT WHERE ISN_ACT=@ISN_ACT AND KIND=707 AND DocID='" & doc.ToString & "'")
                    Case "UNIT.ascx"
                        cmdSB.AppendLine("DELETE FROM tblREF_ACT WHERE ISN_ACT=@ISN_ACT AND (KIND=703 OR KIND=704) AND DocID='" & doc.ToString & "'")
                End Select
            Next
            AppSettings.ExecCommand(New SqlClient.SqlCommand(cmdSB.ToString))
            FormWhereStrings()
            ListDocRESULT.Refresh()
        End If
    End Sub

End Class