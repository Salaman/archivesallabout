﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Categories.ascx.vb" Inherits="WebApplication.Categories" %>
<div class="LogicBlockCaption">
    <%--Категория--%>
    <asp:Localize ID="LocalizeCategory" runat="server" Text="<%$ Resources:eqResources, CAP_Category %>" />
</div>
<table width="100%" class="LogicBlockTable">
    <colgroup>
        <col style="width: 120px;" />
        <col />
    </colgroup>
    <tr>
        <td>
            <%--Название--%>
            <asp:Localize ID="LocalizeCaption" runat="server" Text="<%$ Resources:eqResources, CAP_Caption %>" />
        </td>
        <td>
            <eq:eqTextBox ID="CategoryName" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            ImageURL
        </td>
        <td>
            <eq:eqTextBox ID="ImageURL" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            NavigateUrl
        </td>
        <td>
            <eq:eqTextBox ID="NavigateUrl" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            PopOutImageUrl
        </td>
        <td>
            <eq:eqTextBox ID="PopOutImageUrl" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            SeparatorImageUrl
        </td>
        <td>
            <eq:eqTextBox ID="SeparatorImageUrl" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            ToolTip
        </td>
        <td>
            <eq:eqTextBox ID="ToolTip" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            SortOrder
        </td>
        <td>
            <eq:eqTextBox ID="SortOrder" runat="server" />
        </td>
    </tr>
</table>
