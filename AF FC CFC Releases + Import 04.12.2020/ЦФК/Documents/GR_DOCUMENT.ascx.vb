﻿Public Partial Class GR_DOCUMENT
    Inherits BaseDoc
    Const STORAGE_GUID = "84E12153-532F-46AD-A0B7-0541450978E5"
    '<asp:Image Id="imgMyImage" ImageUrl="images/my_image.jpg" runat="server" />
    'вставить картинку надо будет типа так
    '<asp:Image runat="server" SkinID="UpdateProgress" ImageUrl="~/App_Themes/PrintTheme/Images/UpdateProgress.gif"/>


    'Public Overrides Function PutValuesFromDataSetToSpecificationControls(ByVal SpecificationTableName As String) As Boolean
    '    If SYS_STATUS.Text = "Новый" Or Len(SYS_STATUS.Text) = 0 Then
    '        Select Case SpecificationTableName
    '            Case "GR_vDOC_DESCRIPT" 
    '                Dim LastDescript As String
    '                If SYS_STATUS.Text = "Новый" Then
    '                    LastDescript = GetLastIDDocDescript()
    '                    SetFilterDocDescript(0, LastDescript)
    '                    SYS_STATUS.Text = "Сохранен"
    '                    ID_DOC_DESCRIPT_CURRENT.Text = LastDescript
    '                End If

    '                If Len(SYS_STATUS.Text) = 0 Then
    '                    LastDescript = GetLastIDDocDescript()
    '                    SetFilterDocDescript(0, LastDescript)
    '                    ID_DOC_DESCRIPT_CURRENT.Text = LastDescript
    '                End If

    '            Case Else

    '        End Select
    '    End If

    '    Return MyBase.PutValuesFromDataSetToSpecificationControls(SpecificationTableName)

    'End Function
    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender

        HyperLink_DOC_IMAGE.Enabled = DocStateInfo.StateID <> Guid.Empty
        '   HyperLink_DOC_IMAGE.NavigateUrl = BasePage.GetListDocURL("GR_DOC_IMAGE.ascx", False, "GR_DOCUMENT=" & DocID.ToString)
        HyperLink_DOC_IMAGE.NavigateUrl = BasePage.GetDocURL("GR_DOC_IMAGE.ascx", DocID, False)

    End Sub

#Region "Doc Events"
    Public Overrides Function NewDoc() As Boolean
        NewDoc = MyBase.NewDoc()
        If NewDoc Then

            'надо установить архив
            Dim archTable = AppSettings.GetDataTable(New SqlClient.SqlCommand("SELECT ID, OKPO, FULL_SUBJ, ARCHIVE_LEVEL FROM GR_vARCHIVE_SUBJECT"))
            If archTable.Rows.Count = 1 Then
                Debug.Print(archTable.Rows(0).Item("ID").ToString)
                'а тут по умолчанию всегда одна строка
                Dim row1 = GR_tblSTORE_PLACE.InsertRow()
                For Each r In GR_tblSTORE_PLACE.Rows
                    DirectCast(r.FindControl("ID_ARCHIVE"), eqDocument).DocID = archTable.Rows(0).Item("ID")
                    DirectCast(r.FindControl("OKPO"), TextBox).Text = archTable.Rows(0).Item("OKPO")
                    DirectCast(r.FindControl("SUBJECT"), TextBox).Text = archTable.Rows(0).Item("FULL_SUBJ")
                    DirectCast(r.FindControl("ARCHIVE_LEVEL"), RadioButtonList).Text = archTable.Rows(0).Item("ARCHIVE_LEVEL")
                Next

            End If

        End If

        SYS_STATUS.Text = "Новый полностью"
        'видна только одна строка
        Dim row = GR_vDOC_DESCRIPT.InsertRow()

        GR_tblPRECIOUS_METAL.Enabled = False
        GR_tblPRECIOUS_STONES.Enabled = False
        GR_tblRESTORATION.Enabled = False
        GR_tblDOC_SIZE.Enabled = False

        [PUBLIC].Checked = True
        NUM.Enabled = False
        DOC_DATE.Enabled = False
    End Function
    Public Overrides Function SaveDoc() As Boolean

        If SYS_STATUS.Text = "Новый" Then
            SYS_STATUS.Text = "Новый сохранение"
        End If
        Try

            If Len(ID_OWNER.Text) = 0 Then
                AddMessage("Необходимо заполнить обязательное поле Тип владельца")

                Return False
            End If
            For Each r In GR_vDOC_DESCRIPT.Rows
                'тут только одна строка . Т.е. при сохранении не фильтруем, строка и так одна
                'тут происходит ошибка при первом сохранении листа описания
                If Len(DirectCast(r.FindControl("ID_VOLUME"), eqDocument).Text) = 0 Then
                    AddMessage("Необходимо заполнить обязательное поле Дело, том")
                    Return False
                End If
            Next
            'данные по архиву проставить, если изменялся
            'проставляем значения для Архива
            'а тут по умолчанию всегда одна строка
            For Each r In GR_tblSTORE_PLACE.Rows
                'нашли код архива и сделаем еще одну выборку
                Dim archTable1 = AppSettings.GetDataTable(New SqlClient.SqlCommand("SELECT ID, ISNULL(OKPO,'') AS OKPO, isnull(FULL_SUBJ,'') AS FULL_SUBJ, isnull(ARCHIVE_LEVEL,'a') AS ARCHIVE_LEVEL FROM GR_vARCHIVE_SUBJECT WHERE ID='" & DirectCast(r.FindControl("ID_ARCHIVE"), eqDocument).DocID.ToString & "'"))
                DirectCast(r.FindControl("OKPO"), TextBox).Text = archTable1.Rows(0).Item("OKPO")
                DirectCast(r.FindControl("SUBJECT"), TextBox).Text = archTable1.Rows(0).Item("FULL_SUBJ")
                DirectCast(r.FindControl("ARCHIVE_LEVEL"), RadioButtonList).Text = archTable1.Rows(0).Item("ARCHIVE_LEVEL")
            Next


            MyBase.SaveDoc()
            GR_tblPRECIOUS_METAL.Enabled = True
            GR_tblPRECIOUS_STONES.Enabled = True
            GR_tblRESTORATION.Enabled = True
            GR_tblDOC_SIZE.Enabled = True

            ' If Len(NUM.Text) = 0 Then
            'проставить дату и номер
            Dim descTable = AppSettings.GetDataTable(New SqlClient.SqlCommand("SELECT TOP 1 NUM  FROM GR_vDOC_DESCRIPT WHERE DocID='" & DocID.ToString & "' ORDER BY DESCRIPT_DATE"))
            If descTable.Rows.Count > 0 Then
                NUM.Text = descTable.Rows(0).Item("NUM")

                Dim descTable1 = AppSettings.GetDataTable(New SqlClient.SqlCommand("SELECT TOP 1 DESCRIPT_DATE  FROM GR_vDOC_DESCRIPT WHERE DocID='" & DocID.ToString & "' ORDER BY DESCRIPT_DATE"))
                DOC_DATE.Text = descTable1.Rows(0).Item("DESCRIPT_DATE")
                MyBase.SaveDoc()
            End If
            ' End If

            If SYS_STATUS.Text = "Новый сохранение" Then
                SetFilterDocDescript(0, GetLastIDDocDescript())
                ID_DOC_DESCRIPT_CURRENT.Text = GetLastIDDocDescript()
            End If
            
            'проставим GUID для doc_description
            For Each r In GR_vDOC_DESCRIPT.Rows
                'тут только одна строка . Т.е. при сохранении не фильтруем, строка и так одна
                'тут происходит ошибка при первом сохранении листа описания
            Next
            SYS_STATUS.Text = "Сохранен"


            NUM.Enabled = False
            DOC_DATE.Enabled = False

        Catch ex As Exception
            If InStr(1, ex.Message, "FindControl") Then
                SYS_STATUS.Text = "Сохранен"
            Else
                AddMessage(ex.Message)
            End If
        End Try
        ' Return MyBase.SaveDoc 'перенесла в начало
        Return True
    End Function


    Public Overrides Function LoadDoc(ByVal DocID As System.Guid) As Boolean
        LoadDoc = MyBase.LoadDoc(DocID)
        'сюда попадаем из списка или после сохранения документа
        Try
            'открыть заново

            Dim LastDescript As String

            If Len(SYS_STATUS.Text) = 0 Then
                LastDescript = GetLastIDDocDescript()
                SetFilterDocDescript(0, LastDescript)
                ID_DOC_DESCRIPT_CURRENT.Text = LastDescript
            End If

            'If SYS_STATUS.Text = "Новый сохранение" Then
            '    SetFilterDocDescript(0)
            '    SetFilterDocDescript(0, GetLastIDDocDescript())
            '    SYS_STATUS.Text = "Сохранен"
            'End If

            NUM.Enabled = False
            DOC_DATE.Enabled = False

            'проставляем значения для Архива
            'а тут по умолчанию всегда одна строка
            For Each r In GR_tblSTORE_PLACE.Rows
                'нашли код архива и сделаем еще одну выборку
                Dim archTable1 = AppSettings.GetDataTable(New SqlClient.SqlCommand("SELECT ID, ISNULL(OKPO,'') AS OKPO, isnull(FULL_SUBJ,'') AS FULL_SUBJ, isnull(ARCHIVE_LEVEL,'a') AS ARCHIVE_LEVEL FROM GR_vARCHIVE_SUBJECT WHERE ID='" & DirectCast(r.FindControl("ID_ARCHIVE"), eqDocument).DocID.ToString & "'"))
                DirectCast(r.FindControl("OKPO"), TextBox).Text = archTable1.Rows(0).Item("OKPO")
                DirectCast(r.FindControl("SUBJECT"), TextBox).Text = archTable1.Rows(0).Item("FULL_SUBJ")
                DirectCast(r.FindControl("ARCHIVE_LEVEL"), RadioButtonList).Text = archTable1.Rows(0).Item("ARCHIVE_LEVEL")
            Next


            If SYS_STATUS.Text = "Новый сохранение" Then
            Else
                SYS_STATUS.Text = "Загружен"
            End If


        Catch ex As Exception
            AddMessage(ex.Message)
        End Try
    End Function


    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        RegisterSpecificationAsInline(GR_tblSTORE_PLACE)
        RegisterSpecificationAsInline(GR_tblOFFICIAL_INFO)
        RegisterSpecificationAsInline(GR_vDOC_DESCRIPT)

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        NUM.Enabled = False
        DOC_DATE.Enabled = False
    End Sub


    Private Sub Page_PreRender2(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender

        MultiViewTabs.ActiveViewIndex = CInt(MenuTabs.SelectedValue)

    End Sub

    Public Sub AddDocDescription()
        'надо оставить только одну строку
        GR_tblPRECIOUS_METAL.Enabled = False
        GR_tblPRECIOUS_STONES.Enabled = False
        GR_tblRESTORATION.Enabled = False
        GR_tblDOC_SIZE.Enabled = False

        SetFilterDocDescript(0)

        Dim row = GR_vDOC_DESCRIPT.InsertRow()

        DirectCast(row.FindControl("DocID"), TextBox).Text = DocID.ToString
        DirectCast(row.FindControl("RowID"), TextBox).Text = GetLastRowIDDocDescript()

        ID_DOC_DESCRIPT_CURRENT.Text = Nothing
        SYS_STATUS.Text = "Новый"

    End Sub

    Public Sub OpenDocDescription()
        '  BasePage.OpenModalDialogWindow(Me, BasePage.GetListDocURL("GR_DOC_DESCRIPT_LIST.ascx", False, "DocID=" & DocID.ToString), "SelectDoc")
        BasePage.OpenModalDialogWindow(Me, BasePage.GetListDocURL("GR_DOC_DESCRIPT_LIST.ascx", False, "GR_DOCUMENT=" & DocID.ToString), "SelectDoc")
    End Sub

    Public Sub DeleteDocDescription()
        Dim FirstDescript As String
        Dim CurrentDescript As String
        'проверяем, если первый лист, то нельзя удалять 
        'если последующие, то открываем первый и удаляем выбранный
        FirstDescript = GetFirstIDDocDescript()
        CurrentDescript = ID_DOC_DESCRIPT_CURRENT.Text

        If FirstDescript = ID_DOC_DESCRIPT_CURRENT.Text Then
            'нельзя удалять
            AddMessage("Открытый лист описания является первым, его удалить нельзя. Можно только редактировать.")
        Else
            ''переоткрываем на первый документ и удаляем
            SetFilterDocDescript(0, FirstDescript)

            'удаление
            DeleteDocDescript(CurrentDescript)
            AddMessage("Удалено.")
        End If


    End Sub

    Private Sub Page_ModalDialogClosed(ByVal Argument As Equipage.WebUI.eqModalDialogArgument) Handles Me.ModalDialogClosed

        SetFilterDocDescript(0, Argument.ArgumentObject.ToString)
        ID_DOC_DESCRIPT_CURRENT.Text = Argument.ArgumentObject.ToString
        SYS_STATUS.Text = "Загружен"

    End Sub



    Public Sub ISN_VOLUME_SelectDoc(ByVal sender As Object, ByVal e As Equipage.WebUI.eqBaseContainer.SelectDocEventArgs)
        Dim a = e.DocID
    End Sub



    Private Sub GR_tblPRECIOUS_METAL_RowInserted(ByVal SpecificationRow As Equipage.WebUI.eqSpecificationRow) Handles GR_tblPRECIOUS_METAL.RowInserted
        DirectCast(SpecificationRow.FindControl("DocID"), TextBox).Text = DocID.ToString
        DirectCast(SpecificationRow.FindControl("ID_DOC_DESCRIPT"), TextBox).Text = ID_DOC_DESCRIPT_CURRENT.Text
        DirectCast(SpecificationRow.FindControl("RowID"), TextBox).Text = "1"
    End Sub


    Private Sub GR_tblPRECIOUS_STONES_RowInserted(ByVal SpecificationRow As Equipage.WebUI.eqSpecificationRow) Handles GR_tblPRECIOUS_STONES.RowInserted
        DirectCast(SpecificationRow.FindControl("DocID"), TextBox).Text = DocID.ToString
        DirectCast(SpecificationRow.FindControl("ID_DOC_DESCRIPT"), TextBox).Text = ID_DOC_DESCRIPT_CURRENT.Text
        DirectCast(SpecificationRow.FindControl("RowID"), TextBox).Text = "1"
    End Sub

    Private Sub GR_tblRESTORATION_RowInserted(ByVal SpecificationRow As Equipage.WebUI.eqSpecificationRow) Handles GR_tblRESTORATION.RowInserted
        DirectCast(SpecificationRow.FindControl("DocID"), TextBox).Text = DocID.ToString
        DirectCast(SpecificationRow.FindControl("ID_DOC_DESCRIPT"), TextBox).Text = ID_DOC_DESCRIPT_CURRENT.Text
        DirectCast(SpecificationRow.FindControl("RowID"), TextBox).Text = "1"
    End Sub


    Private Sub GR_tblDOC_SIZE_RowInserted(ByVal SpecificationRow As Equipage.WebUI.eqSpecificationRow) Handles GR_tblDOC_SIZE.RowInserted

        DirectCast(SpecificationRow.FindControl("DocID"), TextBox).Text = DocID.ToString
        DirectCast(SpecificationRow.FindControl("ID_DOC_DESCRIPT"), TextBox).Text = ID_DOC_DESCRIPT_CURRENT.Text
        DirectCast(SpecificationRow.FindControl("RowID"), TextBox).Text = "1"

    End Sub


    Public Function GetFileImageName(ByVal ID As Guid) As String

        'в поле Name храниться весь путь
        Dim cmd As New SqlClient.SqlCommand("select NAME from GR_tblDOC_IMAGE where ID = @ID")
        cmd.Parameters.AddWithValue("@ID", ID)
        Dim DataTable = AppSettings.GetDataTable(cmd)
        If DataTable.Rows.Count = 0 Then
            GetFileImageName = Nothing
        Else
            GetFileImageName = GetStorgePath() + "\" + DataTable.Rows(0)("NAME")
        End If

    End Function

    Public Sub SetFilterDocDescript(ByVal CodeDocDescript As Integer, Optional ByVal ID As String = "")
        Dim IDDocDescript As String
        If Len(ID) > 0 Then
            IDDocDescript = ID
        Else
            IDDocDescript = "00000000-0000-0000-0000-000000000000"
        End If

        Dim a = DataSetSpecificationDataTable("GR_vDOC_DESCRIPT")
        a.DefaultView.RowFilter = "ID='" & IDDocDescript & "'"
        MyBase.PutValuesFromDataSetToSpecificationControls("GR_vDOC_DESCRIPT")

        'все остальные связанные таблицы
        Dim b = DataSetSpecificationDataTable("GR_tblPRECIOUS_METAL")
        b.DefaultView.RowFilter = "ID_DOC_DESCRIPT ='" & IDDocDescript & "'"
        MyBase.PutValuesFromDataSetToSpecificationControls("GR_tblPRECIOUS_METAL")

        'драг. камни
        Dim c = DataSetSpecificationDataTable("GR_tblPRECIOUS_STONES")
        c.DefaultView.RowFilter = "ID_DOC_DESCRIPT ='" & IDDocDescript & "'"
        MyBase.PutValuesFromDataSetToSpecificationControls("GR_tblPRECIOUS_STONES")

        'реставрация
        Dim d = DataSetSpecificationDataTable("GR_tblRESTORATION")
        d.DefaultView.RowFilter = "ID_DOC_DESCRIPT ='" & IDDocDescript & "'"
        MyBase.PutValuesFromDataSetToSpecificationControls("GR_tblRESTORATION")

        'размеры
        Dim f = DataSetSpecificationDataTable("GR_tblDOC_SIZE")
        f.DefaultView.RowFilter = "ID_DOC_DESCRIPT ='" & IDDocDescript & "'"
        MyBase.PutValuesFromDataSetToSpecificationControls("GR_tblDOC_SIZE")

    End Sub
    Private Function GetLastRowIDDocDescript() As Integer
        Dim cmd As New SqlClient.SqlCommand("select MAX(RowID)+1 AS RowID from GR_vDOC_DESCRIPT where DocID = @ID")
        cmd.Parameters.AddWithValue("@ID", DocID)
        Dim DataTable = AppSettings.GetDataTable(cmd)
        If DataTable.Rows.Count = 0 Then
            GetLastRowIDDocDescript = 0
        Else
            GetLastRowIDDocDescript = DataTable.Rows(0)("RowID")
        End If
        Return GetLastRowIDDocDescript
    End Function

    Private Function GetFirstIDDocDescript() As String

        Dim cmd As New SqlClient.SqlCommand("SELECT TOP 1 ID AS FIRST_ID  FROM GR_vDOC_DESCRIPT WHERE DocID=@ID ORDER BY DESCRIPT_DATE")
        cmd.Parameters.AddWithValue("@ID", DocID)
        Dim DataTable = AppSettings.GetDataTable(cmd)
        If DataTable.Rows.Count = 0 Then
            GetFirstIDDocDescript = "00000000-0000-0000-0000-000000000000"
        Else
            GetFirstIDDocDescript = DataTable.Rows(0)("FIRST_ID").ToString
        End If
        Return GetFirstIDDocDescript
    End Function
    Private Function GetLastIDDocDescript() As String

        Dim cmd As New SqlClient.SqlCommand("SELECT TOP 1 ID AS FIRST_ID  FROM GR_vDOC_DESCRIPT WHERE DocID=@ID ORDER BY DESCRIPT_DATE DESC")
        cmd.Parameters.AddWithValue("@ID", DocID)
        Dim DataTable = AppSettings.GetDataTable(cmd)
        If DataTable.Rows.Count = 0 Then
            GetLastIDDocDescript = "00000000-0000-0000-0000-000000000000"
        Else
            GetLastIDDocDescript = DataTable.Rows(0)("FIRST_ID").ToString
        End If
        Return GetLastIDDocDescript

    End Function

    Private Sub DeleteDocDescript(ByVal ID As String)

        Dim cmd As New SqlClient.SqlCommand("DELETE FROM GR_tblPRECIOUS_METAL WHERE ID_DOC_DESCRIPT=@ID")
        'cmd.Parameters.Add(New SqlClient.SqlParameter("@ISN_DOC_DESCRIPT", SqlDbType.BigInt) With {.Value = FirstDescript})
        cmd.Parameters.AddWithValue("@ID", ID)
        AppSettings.ExecCommand(cmd)

        Dim cmd1 As New SqlClient.SqlCommand("DELETE FROM GR_tblPRECIOUS_STONES WHERE ID_DOC_DESCRIPT=@ID")
        cmd1.Parameters.AddWithValue("@ID", ID)
        AppSettings.ExecCommand(cmd1)

        Dim cmd2 As New SqlClient.SqlCommand("DELETE FROM GR_tblPRECIOUS_STONES WHERE ID_DOC_DESCRIPT=@ID")
        cmd2.Parameters.AddWithValue("@ID", ID)
        AppSettings.ExecCommand(cmd2)

        Dim cmd3 As New SqlClient.SqlCommand("DELETE FROM GR_tblPRECIOUS_STONES WHERE ID_DOC_DESCRIPT=@ID")
        cmd3.Parameters.AddWithValue("@ID", ID)
        AppSettings.ExecCommand(cmd3)

        Dim cmd4 As New SqlClient.SqlCommand("DELETE FROM GR_tblDOC_DESCRIPT WHERE ID=@ID")
        cmd4.Parameters.AddWithValue("@ID", ID)
        AppSettings.ExecCommand(cmd4)
    End Sub
#End Region

#Region "File Events"

    'Private Sub RefFileUploadFileS_Uploaded(ByVal sender As Object, ByVal e As RefFileArgs) Handles RefFileUploadFileS.Uploaded
    '    'добавление строки в спецификацию
    '    Dim DOC_ATTR As String
    '    DOC_ATTR = CreateStorgePath()
    '    Dim RowID As Integer
    '    RowID = CreateRowID(e.FileName)
    '    If DOC_ATTR IsNot Nothing Then
    '        Dim row = GR_tblDOC_IMAGE.InsertRow(e.ID)
    '        DataSetSpecificationDataRow(GR_tblDOC_IMAGE.ID, e.ID)("CreationDateTime") = e.TransferTime
    '        DirectCast(row.FindControl("NAME"), TextBox).Text = DOC_ATTR + "\" + System.IO.Path.GetFileName(e.FileName)
    '        DirectCast(row.FindControl("PUBLIC"), CheckBox).Checked = True
    '        '  DirectCast(row.FindControl("RowID"), TextBox).Text = RowID.ToString
    '    Else
    '        AddMessage("Заполните поля Архив, Номер фонда, Опись и Ед.хр. на вкладке Место хранения")
    '        e.Cancel = True
    '    End If

    'End Sub

    'Private Sub RefFileUploadFileS_Uploading(ByVal sender As Object, ByVal e As RefFileArgs) Handles RefFileUploadFileS.Uploading
    '    'сохраняем файл 
    '    'надо блокировать вкладку, пока не будет сохранено место хранения
    '    Dim DOC_ATTR As String
    '    DOC_ATTR = CreateStorgePath()
    '    If DOC_ATTR IsNot Nothing Then
    '        ' e.FileName = GetFileSystemAttachFileNameGR(e.ID, e.TransferTime, 709, SUBJECT, ARCHIVE, DOC_ATTR, GetStorgePath(), e.FileName)
    '        e.FileName = GetStorgePath() + "\" + DOC_ATTR + "\" + System.IO.Path.GetFileName(e.FileName)
    '    Else
    '        AddMessage("Заполните поля Архив, Номер фонда, Опись и Ед.хр. на вкладке Место хранения")
    '        e.Cancel = True
    '    End If
    'End Sub

    'Public Sub RefFileS_Requesting(ByVal sender As Object, ByVal e As RefFileArgs)
    '    'открываем файл на просмотр
    '    Try
    '        Dim FileName As String
    '        FileName = GetFileImageName(e.ID)
    '        If FileName IsNot Nothing Then
    '            e.FileName = FileName
    '        Else
    '            AddMessage("Сохраните документ")
    '        End If

    '    Catch ex As Exception
    '        AddMessage(ex.Message)
    '    End Try
    'End Sub
    ''' <summary>
    ''' ''для изображений Экспертной оценки
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub RefFileUploadFileE_Uploaded(ByVal sender As Object, ByVal e As RefFileArgs) Handles RefFileUploadFileE.Uploaded
        Dim row = GR_tblEXPERT_REPORT_IMAGE.InsertRow(e.ID)
        DataSetSpecificationDataRow(GR_tblEXPERT_REPORT_IMAGE.ID, e.ID)("CreationDateTime") = e.TransferTime
        DirectCast(row.FindControl("NAME"), TextBox).Text = System.IO.Path.GetFileName(e.FileName)
    End Sub

    Private Sub RefFileUploadFileE_Uploading(ByVal sender As Object, ByVal e As RefFileArgs) Handles RefFileUploadFileE.Uploading
        'сохраняем файл 
        'надо блокировать вкладку, пока не будет сохранено место хранения
        Dim ID_ARCHIVE As Guid = Nothing
        Dim ARCHIVE As String = Nothing
        Dim SUBJECT As String = Nothing
        Dim DOC_ATTR As String = Nothing

        For Each r In GR_tblSTORE_PLACE.Rows
            'тут только одна строка
            ID_ARCHIVE = DirectCast(r.FindControl("ID_ARCHIVE"), eqDocument).DocID
            ARCHIVE = DirectCast(r.FindControl("ID_ARCHIVE"), eqDocument).Text
            SUBJECT = DirectCast(r.FindControl("SUBJECT"), TextBox).Text
        Next
        DOC_ATTR = "№ " + NUM.Text + " Дата " + CStr(DatePart(DateInterval.Year, CDate(DOC_DATE.Text))) + "-" + CStr(DatePart(DateInterval.Month, CDate(DOC_DATE.Text))) + "-" + CStr(DatePart(DateInterval.Day, CDate(DOC_DATE.Text)))

        If ID_ARCHIVE.ToString IsNot Nothing Then
            e.FileName = GetFileSystemAttachFileNameGR(e.ID, e.TransferTime, 710, SUBJECT, ARCHIVE, DOC_ATTR, GetStorgePath(), e.FileName)
        Else
            AddMessage("Выберите Архив на вкладке Место хранения и сохраните документ")
        End If
    End Sub

    Public Sub RefFileE_Requesting(ByVal sender As Object, ByVal e As RefFileArgs)
        'открываем 
        Dim ID_ARCHIVE As Guid = Nothing
        Dim SUBJECT As String = Nothing
        Dim DOC_ATTR As String = Nothing
        Dim FileName As String = Nothing
        Dim ARCHIVE As String = Nothing

        For Each r In GR_tblSTORE_PLACE.Rows
            'тут только одна строка
            ID_ARCHIVE = DirectCast(r.FindControl("ID_ARCHIVE"), eqDocument).DocID
            ARCHIVE = DirectCast(r.FindControl("ID_ARCHIVE"), eqDocument).Text
            SUBJECT = DirectCast(r.FindControl("SUBJECT"), TextBox).Text
        Next
        DOC_ATTR = "№ " + NUM.Text + " Дата " + CStr(DatePart(DateInterval.Year, CDate(DOC_DATE.Text))) + "-" + CStr(DatePart(DateInterval.Month, CDate(DOC_DATE.Text))) + "-" + CStr(DatePart(DateInterval.Day, CDate(DOC_DATE.Text)))
        If ID_ARCHIVE.ToString IsNot Nothing Then
            e.TransferTime = DataSetSpecificationDataRow(GR_tblEXPERT_REPORT_IMAGE.ID, e.ID)("CreationDateTime")
            FileName = GetFileImageName(e.ID)
            If FileName IsNot Nothing Then
                e.FileName = GetFileSystemAttachFileNameGR(e.ID, e.TransferTime, 710, SUBJECT, ARCHIVE, DOC_ATTR, GetStorgePath(), FileName)
            Else
                AddMessage("Сохраните документ")
            End If
        Else
            AddMessage("Выберите Архив на вкладке Место хранения и сохраните документ")
        End If
    End Sub

    Private Function GetStorgePath() As String
        Dim cmd As New SqlClient.SqlCommand("select Text from tblConstantsSpec where ID = @ID")
        cmd.Parameters.AddWithValue("@ID", New Guid(STORAGE_GUID))
        Dim DataTable = AppSettings.GetDataTable(cmd)
        If DataTable.Rows.Count = 0 Then
            GetStorgePath = "C:\Госреестр-Файлы"
        Else
            GetStorgePath = DataTable.Rows(0)("Text")
        End If
        Return GetStorgePath
    End Function

    Private Function GetArchiveNameShort(ByVal ID_ARCHIVE As Guid) As String
        Dim cmd As New SqlClient.SqlCommand("select NAME_SHORT from tblARCHIVE where ID = @ID")
        cmd.Parameters.AddWithValue("@ID", ID_ARCHIVE)
        Dim DataTable = AppSettings.GetDataTable(cmd)
        If DataTable.Rows.Count = 0 Then
            GetArchiveNameShort = "Архив не найден"
        Else
            GetArchiveNameShort = DataTable.Rows(0)("NAME_SHORT")
        End If
        Return GetArchiveNameShort
    End Function

    Private Function CreateStorgePath() As String
        'опеределяем путь к файлу и сохранем его в БД
        Dim ID_ARCHIVE As Guid = Nothing
        Dim ARCHIVE As String = Nothing
        Dim SUBJECT As String = Nothing
        Dim FUND_NUM As String = Nothing
        Dim INVENTORY_NUM As String = Nothing
        Dim UNIT_NUM As String = Nothing

        'проверка на заполненность обязательных полей номер фонда, номер описи и номер единицы хранения
        For Each r In GR_tblSTORE_PLACE.Rows
            FUND_NUM = DirectCast(r.FindControl("FUND_NUM_1"), TextBox).Text + DirectCast(r.FindControl("FUND_NUM_2"), TextBox).Text + DirectCast(r.FindControl("FUND_NUM_3"), TextBox).Text
            INVENTORY_NUM = DirectCast(r.FindControl("INVENTORY_NUM_1"), TextBox).Text + DirectCast(r.FindControl("INVENTORY_NUM_2"), TextBox).Text + DirectCast(r.FindControl("INVENTORY_NUM_3"), TextBox).Text
            UNIT_NUM = DirectCast(r.FindControl("UNIT_NUM_1"), TextBox).Text + DirectCast(r.FindControl("UNIT_NUM_2"), TextBox).Text
        Next

        For Each r In GR_tblSTORE_PLACE.Rows
            'тут только одна строка
            ID_ARCHIVE = DirectCast(r.FindControl("ID_ARCHIVE"), eqDocument).DocID
            'надо брать короткое имя архива
            ARCHIVE = GetArchiveNameShort(ID_ARCHIVE)
            ' ARCHIVE = DirectCast(r.FindControl("ID_ARCHIVE"), eqDocument).Text
            SUBJECT = DirectCast(r.FindControl("SUBJECT"), TextBox).Text
        Next


        If ID_ARCHIVE.ToString IsNot Nothing And Len(FUND_NUM.ToString) > 0 And Len(INVENTORY_NUM.ToString) > 0 And Len(UNIT_NUM.ToString) > 0 Then
            'дополним ведущими нулями
            While Len(FUND_NUM) < 4
                FUND_NUM = "0" & FUND_NUM
            End While
            While Len(INVENTORY_NUM) < 3
                INVENTORY_NUM = "0" & INVENTORY_NUM
            End While
            While Len(UNIT_NUM) < 5
                UNIT_NUM = "0" & UNIT_NUM
            End While
            'формируем путь  (удалить все непотребные символы от 1 до 31)
            '  CreateStorgePath = GetStorgePath() + "\" + ARCHIVE + "\" + FUND_NUM + "\" + INVENTORY_NUM + "\" + UNIT_NUM
            CreateStorgePath = ARCHIVE + "\" + FUND_NUM + "\" + INVENTORY_NUM + "\" + UNIT_NUM
        Else
            CreateStorgePath = Nothing
        End If

        Return CreateStorgePath
    End Function

    Private Function CreateRowID(ByVal FileName As String) As Integer
        Dim RowID As Integer = 0
        Return RowID
    End Function
#End Region

End Class