﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="WebApplication.eqDocAreasList"
    CodeBehind="eqDocAreasList.ascx.vb" %>
<link href="Styles/Default.css" rel="stylesheet" type="text/css" />
<table>
    <tr>
        <td>
            <p>
                Тип документа</p>
        </td>
        <td>
            <asp:DropDownList ID="FieldDocTypeID" runat="server" CssClass="txtfld1" />
        </td>
    </tr>
    <tr>
        <td>
            <p>
                Комментарий</p>
        </td>
        <td>
            <asp:TextBox ID="Comment" runat="server" CssClass="txtfld1" Width="500px" />
        </td>
    </tr>
</table>
<p>
</p>
<eq:eqSpecification ID="eqDocAreasListSpec" runat="server">
    <eq:eqSpecificationColumn HeaderText="Имя панели в ascx контроле">
        <ItemTemplate>
            <asp:TextBox ID="PanelName" runat="server" Width="220px" />
        </ItemTemplate>
    </eq:eqSpecificationColumn>
    <eq:eqSpecificationColumn HeaderText="Имя панели логическое">
        <ItemTemplate>
            <asp:TextBox ID="AreaName" runat="server" Width="320px" />
        </ItemTemplate>
    </eq:eqSpecificationColumn>
</eq:eqSpecification>
