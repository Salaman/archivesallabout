﻿Public Partial Class FotoFiles
    Inherits BaseDoc

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim FotoFileSQL = <sql>
                                  Select *
                                    From vFotoFile
                                    Where ID = @ID
                              </sql>
            Dim FotoFileCommand As New SqlClient.SqlCommand(FotoFileSQL.Value)
            FotoFileCommand.Parameters.AddWithValue("@ID", DocID)
            Dim FotoFileDataTable = AppSettings.GetDataTable(FotoFileCommand)

            If FotoFileDataTable.Rows.Count > 0 Then
                File_DocID.Text = FotoFileDataTable.Rows(0)("NAME").ToString
                Crumbs.ISN_UNIT.DocID = FotoFileDataTable.Rows(0)("UnitID")
            Else
                ID.Visible = False
                AddMessage("Файл не найден")
            End If
            File_DocID.Enabled = False
        End If
    End Sub

    Private Sub Page_PreRender2(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Crumbs.ISN_UNIT.DocID <> Guid.Empty AndAlso Not IsDBNull(Crumbs.ISN_UNIT.DataSetHeaderDataRow("ISN_INVENTORY")) Then
            Crumbs.ISN_INVENTORY.KeyValue = Crumbs.ISN_UNIT.DataSetHeaderDataRow("ISN_INVENTORY")
            If Not IsDBNull(Crumbs.ISN_INVENTORY.DataSetHeaderDataRow("ISN_FUND")) Then
                Crumbs.ISN_FUND.KeyValue = Crumbs.ISN_INVENTORY.DataSetHeaderDataRow("ISN_FUND")
                If Not IsDBNull(Crumbs.ISN_FUND.DataSetHeaderDataRow("ISN_ARCHIVE")) Then
                    Crumbs.ISN_ARCHIVE.KeyValue = Crumbs.ISN_FUND.DataSetHeaderDataRow("ISN_ARCHIVE")
                End If
            End If
        End If
    End Sub

    Public Sub RequestFotoFile(ByVal sender As Object, ByVal e As RefFileArgs)
        Dim FotoFileSQL = <sql>
                                  Select *
                                    From vFotoFile
                                    Where ID = @ID
                              </sql>
        Dim FotoFileCommand As New SqlClient.SqlCommand(FotoFileSQL.Value)
        FotoFileCommand.Parameters.AddWithValue("@ID", DocID)
        Dim FotoFileDataTable = AppSettings.GetDataTable(FotoFileCommand)

        If FotoFileDataTable.Rows.Count > 0 Then
            e.TransferTime = FotoFileDataTable.Rows(0)("CreationDateTime")
        End If

        e.FileName = GetFileSystemAttachFileName(DocID, e.TransferTime, 703, 100)
    End Sub
End Class