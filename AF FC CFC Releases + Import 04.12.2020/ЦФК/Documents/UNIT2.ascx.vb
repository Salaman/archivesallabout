﻿Public Partial Class UNIT2
    Inherits BaseDoc

    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        RegisterSpecificationAsInline(tblUNIT_FOTO)
        RegisterSpecificationAsInline(tblUNIT_FOTO_EX)
        RegisterSpecificationAsInline(tblUNIT_MOVIE)
        RegisterSpecificationAsInline(tblUNIT_MOVIE_EX)
        RegisterSpecificationAsInline(tblUNIT_VIDEO)
        RegisterSpecificationAsInline(tblUNIT_VIDEO_EX)
    End Sub

    Public Overrides Function NewDoc() As Boolean
        NewDoc = MyBase.NewDoc()
        If NewDoc Then
            If Request.QueryString("INVENTORY") IsNot Nothing Then
                Crumbs.ISN_INVENTORY.DocID = New Guid(Request.QueryString("INVENTORY"))
                DataSetHeaderDataRow("ISN_INVENTORY") = Crumbs.ISN_INVENTORY.KeyValue
                'PutValueFromDataSetToHeaderControl(ISN_SECURLEVEL)
            End If
            If Request.QueryString("INVENTORYSTRUCTURE") IsNot Nothing Then
                ISN_INVENTORY_CLS.DocID = New Guid(Request.QueryString("INVENTORYSTRUCTURE"))
                DataSetHeaderDataRow("ISN_INVENTORY") = ISN_INVENTORY_CLS.DataSetHeaderDataRow("ISN_INVENTORY")
                'PutValueFromDataSetToHeaderControl(ISN_SECURLEVEL)
            End If
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim bFOTO = False
            Dim bMOVIE = False
            Dim bVIDEO = False

            'Select Case DataSetHeaderDataRow("UNIT_DOC_TYPE").ToString
            Select Case ISN_DOC_TYPE.SelectedItem.Text
                Case "Фотодокументы"
                    FOTO.DataSource = DataSetHeaderDataRow.ItemArray
                    FOTO.DataBind()
                    bFOTO = True
                Case "Кинодокументы"
                    MOVIE.DataSource = DataSetHeaderDataRow.ItemArray
                    MOVIE.DataBind()
                    bMOVIE = True
                Case "Видеодокументы"
                    VIDEO.DataSource = DataSetHeaderDataRow.ItemArray
                    VIDEO.DataBind()
                    bVIDEO = True
            End Select

            FOTO.Visible = bFOTO
            tblUNIT_FOTO.Visible = bFOTO
            tblUNIT_FOTO_EX.Visible = bFOTO
            MOVIE.Visible = bMOVIE
            tblUNIT_MOVIE.Visible = bMOVIE
            tblUNIT_MOVIE_EX.Visible = bMOVIE
            VIDEO.Visible = bVIDEO
            tblUNIT_VIDEO.Visible = bVIDEO
            tblUNIT_VIDEO_EX.Visible = bVIDEO

            PutValuesFromDataSetToControls()
        End If
        If ISN_DOC_TYPE.SelectedItem.Text = "" Then
            ISN_DOC_TYPE.Enabled = True
        Else
            ISN_DOC_TYPE.Enabled = False
        End If
    End Sub

    Private Sub RefreshUnit()
        Dim bFOTO = False
        Dim bMOVIE = False
        Dim bVIDEO = False

        Select Case ISN_DOC_TYPE.SelectedItem.Text
            Case "Фотодокументы"
                FOTO.DataSource = DataSetHeaderDataRow.ItemArray
                FOTO.DataBind()
                bFOTO = True
                UNIT_KIND.SelectedItem.Value = 703
                UNIT_KIND.SelectedItem.Text = "Единица хранения"
            Case "Кинодокументы"
                MOVIE.DataSource = DataSetHeaderDataRow.ItemArray
                MOVIE.DataBind()
                bMOVIE = True
                UNIT_KIND.SelectedItem.Value = 704
                UNIT_KIND.SelectedItem.Text = "Единица учета"
            Case "Видеодокументы"
                VIDEO.DataSource = DataSetHeaderDataRow.ItemArray
                VIDEO.DataBind()
                bVIDEO = True
                UNIT_KIND.SelectedItem.Value = 704
                UNIT_KIND.SelectedItem.Text = "Единица учета"
        End Select

        FOTO.Visible = bFOTO
        tblUNIT_FOTO.Visible = bFOTO
        tblUNIT_FOTO_EX.Visible = bFOTO
        MOVIE.Visible = bMOVIE
        tblUNIT_MOVIE.Visible = bMOVIE
        tblUNIT_MOVIE_EX.Visible = bMOVIE
        VIDEO.Visible = bVIDEO
        tblUNIT_VIDEO.Visible = bVIDEO
        tblUNIT_VIDEO_EX.Visible = bVIDEO
        If ISN_DOC_TYPE.SelectedItem.Text = "" Then
            ISN_DOC_TYPE.Enabled = True
        Else
            ISN_DOC_TYPE.Enabled = False
        End If
    End Sub

    Private Sub Page_PreRender2(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        HyperLinkDOCUMENTs.Enabled = DocStateInfo.StateID <> Guid.Empty
        HyperLinkDOCUMENTs.NavigateUrl = BasePage.GetListDocURL("DOCUMENT.ascx", False, "UNIT=" & DocID.ToString)

        If ISN_DOC_TYPE.Enabled Then
            RefreshUnit()
        End If

        If ISN_INVENTORY_CLS.DocID <> Guid.Empty AndAlso Not IsDBNull(ISN_INVENTORY_CLS.DataSetHeaderDataRow("ISN_INVENTORY")) Then
            Crumbs.ISN_INVENTORY.KeyValue = ISN_INVENTORY_CLS.DataSetHeaderDataRow("ISN_INVENTORY")
            Crumbs.ISN_FUND.KeyValue = Crumbs.ISN_INVENTORY.DataSetHeaderDataRow("ISN_FUND")
            If Not IsDBNull(Crumbs.ISN_FUND.DataSetHeaderDataRow("ISN_ARCHIVE")) Then
                Crumbs.ISN_ARCHIVE.KeyValue = Crumbs.ISN_FUND.DataSetHeaderDataRow("ISN_ARCHIVE")
            End If
        End If
        If Crumbs.ISN_INVENTORY.DocID <> Guid.Empty AndAlso Not IsDBNull(Crumbs.ISN_INVENTORY.DataSetHeaderDataRow("ISN_FUND")) Then
            Crumbs.ISN_INVENTORY.KeyValue = Crumbs.ISN_INVENTORY.DataSetHeaderDataRow("ISN_INVENTORY")
            Crumbs.ISN_FUND.KeyValue = Crumbs.ISN_INVENTORY.DataSetHeaderDataRow("ISN_FUND")
            If Not IsDBNull(Crumbs.ISN_FUND.DataSetHeaderDataRow("ISN_ARCHIVE")) Then
                Crumbs.ISN_ARCHIVE.KeyValue = Crumbs.ISN_FUND.DataSetHeaderDataRow("ISN_ARCHIVE")
            End If
        End If
    End Sub

    Public Overrides Function FindControl(ByVal id As String) As System.Web.UI.Control
        Dim v As Control = Controls.OfType(Of FormView)().FirstOrDefault(Function(F) F.DataSource IsNot Nothing)
        If v IsNot Nothing Then
            Dim c As Control = v.FindControl(id)
            If c IsNot Nothing Then
                Return c
            End If
        End If
        Return MyBase.FindControl(id)
    End Function

#Region "File Events"

    Public Sub UploadedFoto(ByVal sender As Object, ByVal e As RefFileArgs)
        Dim row = DirectCast(FOTO.FindControl("vREF_FILE_UNIT_S"), eqSpecification).InsertRow(e.ID)
        DataSetSpecificationDataRow(DirectCast(FOTO.FindControl("vREF_FILE_UNIT_S"), eqSpecification).ID, e.ID)("CreationDateTime") = e.TransferTime
        DirectCast(row.FindControl("NAME"), TextBox).Text = System.IO.Path.GetFileName(e.FileName)
    End Sub

    Public Sub UploadingFoto(ByVal sender As Object, ByVal e As RefFileArgs)
        Dim Ext = Mid(System.IO.Path.GetFileName(e.FileName), InStr(System.IO.Path.GetFileName(e.FileName), ".") + 1)
        Select Case Ext.ToLower()
            Case "jpg", "jpeg", "bmp", "png"
                e.FileName = GetFileSystemAttachFileName(e.ID, e.TransferTime, 703, 100, e.FileName)
            Case Else
                e.Cancel = True
                AddMessage("Файл не является изображением")
        End Select
    End Sub

    Public Sub RefFileS_Requesting(ByVal sender As Object, ByVal e As RefFileArgs)
        e.TransferTime = DataSetSpecificationDataRow(DirectCast(FOTO.FindControl("vREF_FILE_UNIT_S"), eqSpecification).ID, e.ID)("CreationDateTime")
        e.FileName = GetFileSystemAttachFileName(e.ID, e.TransferTime, 703, 100)
    End Sub
#End Region

End Class