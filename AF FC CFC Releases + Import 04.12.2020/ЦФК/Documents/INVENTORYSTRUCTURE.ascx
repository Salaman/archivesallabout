﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="INVENTORYSTRUCTURE.ascx.vb"
    Inherits="WebApplication.INVENTORYSTRUCTURE" %>
<table style="width: 97%; margin-bottom: 4px;"> 
    <tr>
        <td>
            <eq2:ArchiveCrumbs ID="Crumbs" runat="server" Level="INVENTORY" />
        </td>
        <td style="text-align: right;">
            Содержимое:
            <asp:HyperLink ID="HyperLinkUNITs" runat="server" Text="Единицы хранения / Единицы учета"
                Target="_blank" NavigateUrl="~/Default.aspx?DocTypeURL=UNIT.ascx&ListDoc=True"
                Style="font-weight: bold; padding: 2px;" />
        </td>
    </tr>
</table>
<table style="width: 700px;">
    <colgroup>
        <col width="150px" />
        <col />
    </colgroup>
    <tr>
        <td>
            Раздел общий для:
        </td>
        <td>
            <asp:RadioButtonList ID="KIND" runat="server">
                <asp:ListItem Text="архива" Value="700" />
                <asp:ListItem Text="фонда" Value="701" />
                <asp:ListItem Text="описи" Value="702" />
            </asp:RadioButtonList>
        </td>
    </tr>
    <tr>
        <td> 
            Верхний уровень:
        </td>
        <td>
            <eq2:eqDocument ID="ISN_HIGH_INVENTORY_CLS" KeyName="ISN_INVENTORY_CLS" runat="server"
                DocTypeURL="INVENTORYSTRUCTURE.ascx" ListDocMode="NewWindowAndSelect" LoadDocMode="NewWindowAndSelect"
                ShowingFields="NAME" ShowingFieldsFormat="{0}" ValidateHierarchy="true" />
        </td>
    </tr>
    <tr>
        <td>
            Наименование:*
        </td>
        <td>
            <eq:eqTextBox ID="NAME" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            Код:
        </td>
        <td>
            <eq:eqTextBox ID="CODE" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            Количество экземпляров описи:*
        </td>
        <td>
            <eq:eqTextBox ID="COPY_COUNT" Width="40px" runat="server" />
        </td>
    </tr>
    <tr>
        <td valign="top">
            Примечание:
        </td>
        <td>
            <eq:eqTextBox ID="NOTE" runat="server" TextMode="MultiLine" Rows="4" />
        </td>
    </tr>
</table>
