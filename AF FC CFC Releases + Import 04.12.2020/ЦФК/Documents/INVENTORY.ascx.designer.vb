﻿'------------------------------------------------------------------------------
' <автоматически создаваемое>
'     Этот код создан программой.
'
'     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
'     повторной генерации кода. 
' </автоматически создаваемое>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class INVENTORY

    '''<summary>
    '''Crumbs элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents Crumbs As Global.WebApplication.WebUI.ArchiveCrumbs

    '''<summary>
    '''HyperLinkUNITs элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents HyperLinkUNITs As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''HyperLinkUNIT2s элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents HyperLinkUNIT2s As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''HyperLinkSTRs элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents HyperLinkSTRs As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''MenuTabs элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents MenuTabs As Global.Equipage.WebUI.eqMenu

    '''<summary>
    '''MultiViewTabs элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents MultiViewTabs As Global.System.Web.UI.WebControls.MultiView

    '''<summary>
    '''ForbidRecalc элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents ForbidRecalc As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''INVENTORY_NUM_1 элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents INVENTORY_NUM_1 As Global.Equipage.WebUI.eqTextBox

    '''<summary>
    '''INVENTORY_NUM_2 элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents INVENTORY_NUM_2 As Global.Equipage.WebUI.eqTextBox

    '''<summary>
    '''INVENTORY_NUM_3 элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents INVENTORY_NUM_3 As Global.Equipage.WebUI.eqTextBox

    '''<summary>
    '''COPY_COUNT элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents COPY_COUNT As Global.Equipage.WebUI.eqTextBox

    '''<summary>
    '''INVENTORY_KEEP_PERIOD элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents INVENTORY_KEEP_PERIOD As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''INVENTORY_NAME элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents INVENTORY_NAME As Global.Equipage.WebUI.eqTextBox

    '''<summary>
    '''INVENTORY_KIND элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents INVENTORY_KIND As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ISN_INVENTORY_TYPE элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents ISN_INVENTORY_TYPE As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ISN_DOC_KIND элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents ISN_DOC_KIND As Global.WebApplication.WebUI.eqDocument

    '''<summary>
    '''tblINVENTORY_DOC_TYPE элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents tblINVENTORY_DOC_TYPE As Global.Equipage.WebUI.eqSpecification

    '''<summary>
    '''tblINVENTORY_DOC_STORAGE элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents tblINVENTORY_DOC_STORAGE As Global.Equipage.WebUI.eqSpecification

    '''<summary>
    '''CARRIER_TYPE элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents CARRIER_TYPE As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''HAS_TRADITIONAL_DOCS элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents HAS_TRADITIONAL_DOCS As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''HAS_ELECTRONIC_DOCS элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents HAS_ELECTRONIC_DOCS As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''ISN_REPRODUCTION_METHOD элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents ISN_REPRODUCTION_METHOD As Global.WebApplication.WebUI.eqDocument

    '''<summary>
    '''ISN_RECEIPT_SOURCE элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents ISN_RECEIPT_SOURCE As Global.WebApplication.WebUI.eqDocument

    '''<summary>
    '''ISN_RECEIPT_REASON элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents ISN_RECEIPT_REASON As Global.WebApplication.WebUI.eqDocument

    '''<summary>
    '''MovePanel элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents MovePanel As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''PRESENCE_FLAG элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents PRESENCE_FLAG As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ABSENCE_REASON элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents ABSENCE_REASON As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''SECURITY_CHAR элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents SECURITY_CHAR As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ISN_SECURLEVEL элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents ISN_SECURLEVEL As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ISN_SECURITY_REASON элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents ISN_SECURITY_REASON As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''MOVEMENT_NOTE элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents MOVEMENT_NOTE As Global.Equipage.WebUI.eqTextBox

    '''<summary>
    '''DOC_START_YEAR элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents DOC_START_YEAR As Global.Equipage.WebUI.eqTextBox

    '''<summary>
    '''DOC_START_YEAR_INEXACT элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents DOC_START_YEAR_INEXACT As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''DOC_END_YEAR элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents DOC_END_YEAR As Global.Equipage.WebUI.eqTextBox

    '''<summary>
    '''DOC_END_YEAR_INEXACT элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents DOC_END_YEAR_INEXACT As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''tblINVENTORY_GROUPING_ATTRIBUTE элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents tblINVENTORY_GROUPING_ATTRIBUTE As Global.Equipage.WebUI.eqSpecification

    '''<summary>
    '''NOTE элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents NOTE As Global.Equipage.WebUI.eqTextBox

    '''<summary>
    '''vREF_FILE_INVENTORY_S элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents vREF_FILE_INVENTORY_S As Global.Equipage.WebUI.eqSpecification

    '''<summary>
    '''RefFileUploadFileS элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents RefFileUploadFileS As Global.WebApplication.RefFileUpload

    '''<summary>
    '''Panel_T элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents Panel_T As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''vINVENTORY_DOCUMENT_STATS элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents vINVENTORY_DOCUMENT_STATS As Global.Equipage.WebUI.eqSpecification

    '''<summary>
    '''vINVENTORY_DOCUMENT_STATS_P элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents vINVENTORY_DOCUMENT_STATS_P As Global.Equipage.WebUI.eqSpecification

    '''<summary>
    '''UNDESCRIBED_DOC_COUNT элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents UNDESCRIBED_DOC_COUNT As Global.Equipage.WebUI.eqTextBox

    '''<summary>
    '''PERSONAL_UNDESCRIBED_DOC_COUNT элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents PERSONAL_UNDESCRIBED_DOC_COUNT As Global.Equipage.WebUI.eqTextBox

    '''<summary>
    '''UNDECSRIBED_PAGE_COUNT элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents UNDECSRIBED_PAGE_COUNT As Global.Equipage.WebUI.eqTextBox

    '''<summary>
    '''vINVENTORY_DOCUMENT_STATS_A элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents vINVENTORY_DOCUMENT_STATS_A As Global.Equipage.WebUI.eqSpecification

    '''<summary>
    '''vINVENTORY_DOCUMENT_STATS_M элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents vINVENTORY_DOCUMENT_STATS_M As Global.Equipage.WebUI.eqSpecification

    '''<summary>
    '''Panel_E элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents Panel_E As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''vINVENTORY_DOCUMENT_STATS_E элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents vINVENTORY_DOCUMENT_STATS_E As Global.Equipage.WebUI.eqSpecification

    '''<summary>
    '''ANNOTATE элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents ANNOTATE As Global.Equipage.WebUI.eqTextBox

    '''<summary>
    '''ADDITIONAL_NSA элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents ADDITIONAL_NSA As Global.Equipage.WebUI.eqTextBox

    '''<summary>
    '''vREF_FILE_INVENTORY элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents vREF_FILE_INVENTORY As Global.Equipage.WebUI.eqSpecification

    '''<summary>
    '''RefFileUploadFile элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents RefFileUploadFile As Global.WebApplication.RefFileUpload

    '''<summary>
    '''vREF_FEATURE_INVENTORY элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents vREF_FEATURE_INVENTORY As Global.Equipage.WebUI.eqSpecification

    '''<summary>
    '''vREF_LANGUAGE_INVENTORY элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents vREF_LANGUAGE_INVENTORY As Global.Equipage.WebUI.eqSpecification

    '''<summary>
    '''MUSEUM_UNITS_COUNT элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents MUSEUM_UNITS_COUNT As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''UNITS_WITH_TREASURES_COUNT элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents UNITS_WITH_TREASURES_COUNT As Global.Equipage.WebUI.eqTextBox

    '''<summary>
    '''vREF_CLS701_INVENTORY элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents vREF_CLS701_INVENTORY As Global.Equipage.WebUI.eqSpecification

    '''<summary>
    '''vREF_CLS702_INVENTORY элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents vREF_CLS702_INVENTORY As Global.Equipage.WebUI.eqSpecification

    '''<summary>
    '''vREF_CLS703_INVENTORY элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents vREF_CLS703_INVENTORY As Global.Equipage.WebUI.eqSpecification

    '''<summary>
    '''tblINVENTORY_PAPER_CLS элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents tblINVENTORY_PAPER_CLS As Global.Equipage.WebUI.eqSpecification

    '''<summary>
    '''vREF_ACT_FOR_INVENTORY_UNIT элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents vREF_ACT_FOR_INVENTORY_UNIT As Global.Equipage.WebUI.eqSpecification

    '''<summary>
    '''CATALOGUING элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents CATALOGUING As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''tblINVENTORY_REQUIRED_WORK элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents tblINVENTORY_REQUIRED_WORK As Global.Equipage.WebUI.eqSpecification

    '''<summary>
    '''tblINVENTORY_CHECK элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents tblINVENTORY_CHECK As Global.Equipage.WebUI.eqSpecification

    '''<summary>
    '''INVENTORY_DOC_WORK элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents INVENTORY_DOC_WORK As Global.Equipage.WebUI.eqTextBox

    '''<summary>
    '''vREF_ACT_FOR_INVENTORY элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents vREF_ACT_FOR_INVENTORY As Global.Equipage.WebUI.eqSpecification

    '''<summary>
    '''vREF_LOCATION_INVENTORY элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents vREF_LOCATION_INVENTORY As Global.Equipage.WebUI.eqSpecification
End Class
