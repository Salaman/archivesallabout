﻿Imports Equipage.WebUI.eqMath

Partial Class eqRolesAreasList
    Inherits BaseDoc


#Region "Константы, переменные"
    Private Enum GridFillMode
        gfmDataTable = 1
        gfmArray = 2
        gfmNew = 3
    End Enum
#End Region '>>"Константы, переменные"

#Region "Математические функции и рассчет даты, времени"

#End Region '>>"Математические функции и рассчет времени"

#Region "Events, события страницы и контролов"
    ''' <summary>
    ''' Обработка по событию загрузка страницы
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        FillFieldDocTypeID()
        DrawGrid()
    End Sub

    ''' <summary>
    ''' Изменение типа документа
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub FieldDocTypeID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FieldDocTypeID.SelectedIndexChanged
        If FieldDocTypeID.SelectedValue <> "" Then
            LoadSpec()
        End If
    End Sub



#End Region '>>"Events, события страницы и контролов"

#Region "Переопределенные функции"
    Public Overrides Function SaveDoc() As Boolean
        Try

            If MyBase.SaveDoc() Then
                'GridSaveState(TblDocAreasListSpec)
                SaveGrid1(TblDocAreasListSpec)
                'GridLoadState(TblDocAreasListSpec)

                MyBase.DocType.sGrids = "eqRolesDocAreasList;"
                MyBase.DocType.SaveDoc(DS)
            End If
            Return True

        Catch ex As Exception
            ShowErrorMessage(Err.Description)
            Return False
        End Try

    End Function
#End Region '>>"Переопределенные функции"

#Region "Функции переходов по статусам"

#End Region '>>"Функции переходов по статусам"

#Region "Работа с БД"
    ''' <summary>
    ''' Получить запрос, выводящий колонки
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetSqlX() As String
        Get
            Dim gFieldDocTypeID As New Guid(FieldDocTypeID.SelectedValue)
            Return "select * from (SELECT ID, StateName, StateDescription, DocTypeID, RowID FROM eqDocStates where DocTypeID = '" & gFieldDocTypeID.ToString & "' and StateName<>'Удален' union all SELECT '00000000-0000-0000-0000-000000000000', 'Новый', 'Новый', '" & gFieldDocTypeID.ToString & "', -10) A order by RowID"
        End Get
    End Property

    ''' <summary>
    ''' Запрос выводящий строки
    ''' </summary>
    ''' <param name="DocID"></param>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetSqlY(ByVal DocID As Guid) As String
        Get
            Dim gFieldDocTypeID As New Guid(FieldDocTypeID.SelectedValue)
            Dim sSql As String = "" & _
      "select " & _
      "eqDocAreasListSpec.RowID AreaRowID, " & _
      "eqDocAreasListSpec.ID AreaID, " & _
      "eqDocTypes.ID DocTypeID, " & _
      "DS.ID StatusID, " & _
      "DS.StateName, " & _
      "eqDocTypes.DocType, " & _
      "eqDocAreasListSpec.AreaName, " & _
      "eqDocAreasListSpec.PanelName, " & _
      "eqRolesDocAreasList.Disable, " & _
      "eqRolesDocAreasList.Hidden, " & _
      "            eqRolesDocAreasList.Collapsed, CollapsedPriority, eqRolesDocAreasList.Priority " & _
      "            from " & _
      "dbo.eqDocAreasList join dbo.eqDocAreasListSpec on eqDocAreasList.ID=eqDocAreasListSpec.DocID " & _
      "join eqDocTypes on eqDocTypes.ID=eqDocAreasList.FieldDocTypeID " & _
      "left join eqRolesDocAreasList on eqDocAreasListSpec.ID=eqRolesDocAreasList.IDArea and eqRolesDocAreasList.DocID='" & DocID.ToString & "'" & _
      "left join (SELECT     ID, StateName, StateDescription, DocTypeID FROM eqDocStates where DocTypeID = '" & gFieldDocTypeID.ToString & "' union all SELECT     '00000000-0000-0000-0000-000000000000', 'Новый', 'Новый', '" & gFieldDocTypeID.ToString & "'  ) as DS on eqRolesDocAreasList.IDDocStates=DS.ID " & _
      "where (eqRolesDocAreasList.DocID is null or eqRolesDocAreasList.DocID='" & DocID.ToString & "' )and eqDocAreasList.FieldDocTypeID='" & gFieldDocTypeID.ToString & "' " & _
      "order by eqDocTypes.DocType, eqDocAreasListSpec.AreaName "
            '"left join (SELECT     ID, StateName, StateDescription, DocTypeID, SortOrder FROM eqDocStates where DocTypeID = '" & gFieldDocTypeID.ToString & "' union all SELECT     '00000000-0000-0000-0000-000000000000', 'Новый', 'Новый', '" & gFieldDocTypeID.ToString & "', 5  ) as DS on eqRolesDocAreasList.IDDocStates=DS.ID " & _
            '"where (eqRolesDocAreasList.DocID is null or eqRolesDocAreasList.DocID='" & DocID.ToString & "' )and eqDocAreasList.FieldDocTypeID='" & gFieldDocTypeID.ToString & "' " & _
            '"order by eqDocTypes.DocType, eqDocAreasListSpec.AreaName, DS.SortOrder "

            Return sSql
        End Get
    End Property

    ''' <summary>
    ''' Заполнение выпадающего списка типов документов
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub FillFieldDocTypeID()
        If FieldDocTypeID.Items.Count = 0 Then
            Dim xSql = <xsql>
select FieldDocTypeID ID, eqDocTypes.DocType from
dbo.eqDocAreasList join eqDocTypes on FieldDocTypeID=eqDocTypes.ID
order by 2
                      </xsql>
            Dim cmd = New SqlClient.SqlCommand(xSql.Value)
            Dim tbl = AppSettings.GetDataTable(cmd, "eqDocTypes")
            If tbl.Rows.Count = 0 Then
                FieldDocTypeID.Visible = False
                Dim NavURL = New HyperLink
                NavURL.Text = "Заполните карточку роли-области"
                NavURL.NavigateUrl = BasePage.GetDocURL("eqDocAreasList.ascx", Guid.Empty, False) '("~Default.aspx?DocTypeURL=eqDocAreasList.ascx")
                FieldDocTypeID.Parent.Controls.AddAt(FieldDocTypeID.Parent.Controls.IndexOf(FieldDocTypeID) + 1, NavURL)
            End If
            For Each r In tbl.Rows
                Dim itm As New ListItem(r(1).ToString, r(0).ToString)
                FieldDocTypeID.Items.Add(itm)
            Next

        End If
    End Sub
#End Region '>>"Работа с БД"

#Region "Права доступа, показать/скрыть коптрол, разрешить/запретить редактирование"

#End Region '>>"Права доступа, показать/скрыть коптрол, разрешить/запретить редактирование"

#Region "Специальные функции для Help-файла"

    Public Function IsAccountWorkClear() As Boolean

    End Function

    Public Function IsAccountWorkFilled() As Boolean

    End Function

#End Region '>>"Специальные функции для Help-файла"

#Region "Прочие"

#End Region '>>"Прочие"

#Region "Нарисовать таблицу, сохранить данные из таблицы"
    ''' <summary>
    ''' Подготовка данных и вызов функции рисования таблицы 
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub DrawGrid()
        Dim sSQL As String = ""
        If Me.FieldDocTypeID.SelectedValue = "" Then
            FillFieldDocTypeID()
        End If
        If Me.FieldDocTypeID.SelectedValue <> "" Then
            sSQL = GetSqlY(DocID)
            Dim Y As DataTable = AppSettings.GetDataTable(New SqlClient.SqlCommand(sSQL))

            If Y.Rows.Count > 0 Then ' если в спецификации не содержится записей, то грид не формируется
                sSQL = GetSqlX
                Dim X As DataTable = AppSettings.GetDataTable(New SqlClient.SqlCommand(sSQL))
                GridFill1(TblDocAreasListSpec, X, Y)
            End If
        End If
    End Sub

    ''' <summary>
    ''' продедура формирования пустой спецификации (в частности в новом документе)
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub LoadSpec()
        Dim X As DataTable = AppSettings.GetDataTable(New SqlClient.SqlCommand(GetSqlX)) 'X - таблица заголовков
        Dim Y As DataTable = AppSettings.GetDataTable(New SqlClient.SqlCommand(GetSqlY(DocID))) 'Y - таблица с данными

        If X.Rows.Count > 0 And Y.Rows.Count > 0 Then
            GridFill1(TblDocAreasListSpec, X, Y)
        End If

    End Sub

    ''' <summary>
    ''' Заполняет таблицу строками
    ''' </summary>
    ''' <param name="T">Контрол asp:table</param>
    ''' <param name="X">Таблица с заголовками колонок</param>
    ''' <param name="Y">Таблица с данными</param>
    ''' <remarks>Create by Anatoly Mel'kov on 2008-05-12</remarks>
    Private Sub GridFill1(ByRef T As Table, ByVal X As DataTable, ByVal Y As DataTable)
        Dim iAreaRowID = -1
        Dim iRowNumber = -1
        Dim iColNumber = -1
        Dim tc As TableCell 'это переменная используется как ячейка контрола Table

        AddHeaderTable(T, X) 'заполняем заголовок

        For Each YRow As DataRow In Y.Rows
            If iAreaRowID <> CInt(YRow("AreaRowID")) Then 'Исначально в таблице области отсортированы по RowID, если обнаружена новая область добавляем для неё новую строку
                iAreaRowID = CInt(YRow("AreaRowID"))
                Dim NewRow As New TableRow 'создает новую строку
                iRowNumber = T.Rows.Count  'номер новой строки для именования контролов
                iColNumber = 0 'номер колонки
                tc = New TableCell 'в первую ячейка строки (колонку) пишем текст

                tc.Text = YRow("AreaName") ' название области
                Dim lblLabel = New Label()
                lblLabel.Text = YRow("AreaName") ' название области
                tc.Controls.Add(lblLabel)
                Dim HiddenValue = New HiddenField
                HiddenValue.ID = "HiddenValueRow" & iRowNumber
                HiddenValue.Value = YRow("AreaID").ToString
                tc.Controls.Add(HiddenValue)

                NewRow.Cells.Add(tc)
                NewRow.ID = "Row" & iRowNumber 'пристаиваем строке ID, чтобы легче было воспринимать HTML
                For iColNumber = 1 To T.Rows(0).Cells.Count - 1 'добавляем остальные ячейки с контролами
                    tc = New TableCell
                    '<<==тут добавляем контролы в ячейку
                    Dim cbxHidden As CheckBox = New CheckBox()
                    cbxHidden.ID = "cbxHidden_r" & iRowNumber & "_c" & iColNumber
                    Dim cbxDisable As CheckBox = New CheckBox()
                    cbxDisable.ID = "cbxDisable_r" & iRowNumber & "_c" & iColNumber

                    Dim cbxCollapsed As CheckBox = New CheckBox()
                    cbxCollapsed.ID = "cbxCollapsed_r" & iRowNumber & "_c" & iColNumber

                    Dim cbxPriority As CheckBox = New CheckBox()
                    cbxPriority.ID = "cbxPriority_r" & iRowNumber & "_c" & iColNumber

                    Dim tbxPriority As TextBox = New TextBox()
                    tbxPriority.ID = "tbxPriority_r" & iRowNumber & "_c" & iColNumber
                    tbxPriority.Columns = 1 : tbxPriority.MaxLength = 1
                    tbxPriority.Width = 16

                    tc.Controls.Add(cbxHidden)
                    tc.Controls.Add(cbxDisable)
                    tc.Controls.Add(cbxCollapsed)
                    tc.Controls.Add(cbxPriority)
                    tc.Controls.Add(tbxPriority)

                    'запишем значения в созданные контролы, из того, что хранится в базе
                    Dim rws As DataRow() = Y.Select("AreaRowID='" & iAreaRowID & "' and StateName='" & T.Rows(0).Cells(iColNumber).Text & "'")
                    If rws.Length > 0 Then
                        Dim rw As DataRow = rws(0)
                        cbxHidden.Checked = IsNull(rw("Hidden"), 0) 'проставляем значания в контрол, значение берем мз таблицы
                        cbxDisable.Checked = IsNull(rw("Disable"), 0)
                        cbxPriority.Checked = IsNull(rw("Priority"), 0)
                        cbxCollapsed.Checked = IsNull(rw("Collapsed"), 0)
                        tbxPriority.Text = IsNull(rw("CollapsedPriority"), "")
                    End If

                    '>>==добавили контролы

                    '<== проставляем цвета и стили
                    cbxHidden.BackColor = cbxHiddenSmpl.BackColor
                    cbxHidden.Visible = cbxHiddenSmpl.Checked

                    cbxDisable.BackColor = cbxDisableSmpl.BackColor
                    cbxDisable.Visible = cbxDisableSmpl.Checked

                    cbxCollapsed.BackColor = cbxCollapsedSmpl.BackColor
                    cbxCollapsed.Visible = cbxCollapsedSmpl.Checked

                    cbxPriority.BackColor = cbxPrioritySmpl.BackColor
                    cbxPriority.Visible = cbxPrioritySmpl.Checked

                    '>==
                    NewRow.Cells.Add(tc)
                Next
                T.Rows.Add(NewRow)
            End If
            AddFooterTable(T, X) 'заполняем подвал водой
        Next
    End Sub

    ''' <summary>
    ''' добавление заголовка
    ''' </summary>
    ''' <param name="T"></param>
    ''' <param name="X"></param>
    ''' <remarks>Create by Anatoly Melkov on 2008-05-12</remarks>
    Private Sub AddHeaderTable(ByRef T As Table, ByVal X As DataTable)
        Dim tc = New TableCell() 'переменная, обозначающая ячейку в таблице
        Dim iCol As Integer = 0
        tc.Text = "панели\статусы"
        Dim NewRow As New TableRow
        NewRow.ID = "Row0"
        NewRow.Cells.Add(tc)
        For Each r As DataRow In X.Rows
            tc = New TableCell
            tc.Text = r("StateName").ToString
            Dim lblLabel = New Label
            lblLabel.Text = r("StateName").ToString
            tc.Controls.Add(lblLabel)
            'запишем идентификаторы статусов
            Dim HiddenValue = New HiddenField
            HiddenValue.ID = "HiddenValueCol" & iCol
            HiddenValue.Value = r("ID").ToString()
            tc.Controls.Add(HiddenValue)
            tc.Width = 150
            NewRow.Cells.Add(tc)
            iCol += 1
        Next
        T.Rows.Add(NewRow)
    End Sub


    ''' <summary>
    ''' Добавляем footer
    ''' пока пустой
    ''' </summary>
    ''' <param name="T"></param>
    ''' <param name="X"></param>
    ''' <remarks>created by Anatoly Mel'kov on 2008-05-12</remarks>
    Private Sub AddFooterTable(ByRef T As Table, ByVal X As DataTable)

    End Sub

    ''' <summary>
    ''' Считываем данные из Table в таблицу спецификации
    ''' </summary>
    ''' <param name="T"></param>
    ''' <remarks>Created by Anatoly Mel'kov on 2008-05-13</remarks>
    Private Sub SaveGrid1(ByVal T As Table)
        Dim iRowsCount = T.Rows.Count
        Dim iCellsCount = T.Rows(0).Cells.Count
        If T.Rows.Count > 1 AndAlso T.Rows(0).Cells.Count > 1 Then

            Dim gDocTypeID As Guid = New Guid(Me.FieldDocTypeID.SelectedValue) 'гуид типа документа, необходим для работы конкретного алгоритма

            For iRow = 1 To T.Rows.Count - 1 'читаем иnфу из контролов Table
                Dim tcY = T.Rows(iRow).Cells(0)
                Dim hfY = CType(tcY.Controls(1), HiddenField) 'первый контрол - текст, второй контрол HiddenField
                Dim gYID As Guid = New Guid(hfY.Value.ToString) 'в данном слечае получаем гуид области
                For iCol = 1 To iCellsCount - 1
                    Dim tcX = T.Rows(0).Cells(iCol)
                    Dim hfX = CType(tcX.Controls(1), HiddenField) 'первый контрол - текст, второй контрол HiddenField
                    Dim gXID = New Guid(hfX.Value.ToString) 'в данном случае получаем гуид статуса

                    '<<==получим значения из контролах в ячейках. Контролы в ячайках распологаются в том же порядке, в каком их добавляли
                    Dim tc = T.Rows(iRow).Cells(iCol)
                    Dim cbxHidden As CheckBox = CType(tc.Controls(0), CheckBox)
                    Dim cbxDisable As CheckBox = CType(tc.Controls(1), CheckBox)
                    Dim cbxCollapsed As CheckBox = CType(tc.Controls(2), CheckBox)
                    Dim cbxPriority As CheckBox = CType(tc.Controls(3), CheckBox)
                    Dim CollapsedPriority As TextBox = CType(tc.Controls(4), TextBox)
                    Dim iCollapsedPriority As Integer = 0
                    If IsNumeric(CollapsedPriority.Text) Then
                        iCollapsedPriority = CollapsedPriority.Text
                    End If

                    '>>==получим значения из контролах в ячейках

                    '<<==Изменим записи в таблице спецификации
                    Dim tSpec As DataTable = DS.Tables("eqRolesDocAreasList")
                    Dim tSpecRow As DataRow = Nothing
                    Dim tSpecRows As DataRow() = tSpec.Select("IDDocType='" & gDocTypeID.ToString & "' and IDArea='" & gYID.ToString & "' and IDDocStates='" & gXID.ToString & "'")
                    If tSpecRows.Length > 0 Then
                        tSpecRow = tSpecRows(0)
                    End If

                    If tSpecRow IsNot Nothing Then
                        tSpecRow("Hidden") = cbxHidden.Checked
                        tSpecRow("Disable") = cbxDisable.Checked
                        tSpecRow("Priority") = cbxPriority.Checked
                        tSpecRow("Collapsed") = cbxCollapsed.Checked
                        tSpecRow.Item("RowID") = (iRow * 10000) + iCol * 10
                        tSpecRow.Item("CollapsedPriority") = iCollapsedPriority
                    Else
                        tSpec.Columns("ID").AllowDBNull = True
                        Dim NewRow = tSpec.Rows.Add()
                        NewRow.Item("ID") = eqMath.NewNextDateTimeGuid
                        NewRow.Item("DocID") = Me.DocID
                        NewRow.Item("RowID") = (iRow * 10000) + iCol * 10
                        NewRow.Item("ownerid") = AppSettings.UserInfo.UserID
                        NewRow.Item("creationdatetime") = Now()
                        NewRow.Item("iddoctype") = gDocTypeID
                        NewRow.Item("idarea") = gYID
                        NewRow.Item("iddocstates") = gXID
                        NewRow.Item("hidden") = cbxHidden.Checked
                        NewRow.Item("disable") = cbxDisable.Checked
                        NewRow.Item("Priority") = cbxPriority.Checked
                        NewRow.Item("Collapsed") = cbxCollapsed.Checked
                        NewRow.Item("CollapsedPriority") = iCollapsedPriority
                    End If
                    '>>==Изменим записи в таблице спецификации
                Next
            Next
        End If
    End Sub
#End Region '>>"Нарисовать таблицу, сохранить данные из таблицы"





End Class
