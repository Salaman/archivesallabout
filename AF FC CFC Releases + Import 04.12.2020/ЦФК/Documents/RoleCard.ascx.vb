﻿Public Partial Class RoleCard
    Inherits BaseDoc

    Public Overrides Function DeleteDoc() As Boolean
        If BUILD_IN_ROLE.Checked Then
            AddMessage("Данная роль не может быть удалена т.к. роль встроенная")
        Else
            Return MyBase.DeleteDoc()
        End If
    End Function

    Public Overrides Function LoadDoc(ByVal DocID As System.Guid) As Boolean
        LoadDoc = MyBase.LoadDoc(DocID)
        If LoadDoc Then
            RolesDocTypeID.Items.Clear()
            RolesDocTypeID.Items.Add(New ListItem("", ""))
            RolesDocTypeID.Items.Add(New ListItem(" * Все", "All"))
            Dim SqlCommand As New SqlClient.SqlCommand("SELECT * FROM eqDocTypes WHERE Deleted=0 ORDER BY DocType")
            Dim DocTypesDataTable = AppSettings.GetDataTable(SqlCommand)
            For Each DocTypesDataRow As DataRow In DocTypesDataTable.Rows
                RolesDocTypeID.Items.Add(New ListItem(DocTypesDataRow("DocType").ToString, DocTypesDataRow("ID").ToString))
            Next
        End If
    End Function

    Public Sub Function_OnFunctionChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim ldFunction = DirectCast(sender, ListDocFunction)
        Dim Parameter = DirectCast(ldFunction.NamingContainer.FindControl("Parameter"), HiddenField)
        Dim Enabled = DirectCast(ldFunction.NamingContainer.FindControl("Enabled"), CheckBox)
        Parameter.Value = ldFunction.ParameterValue
        Enabled.Checked = ldFunction.FunctionEnabled
    End Sub

    Public ReadOnly Property DocTypeDocAreas(ByVal DocTypeID As Guid) As Dictionary(Of String, String)
        Get
            If _DocTypeDocAreas.ContainsKey(DocTypeID) Then
                Return _DocTypeDocAreas(DocTypeID)
            Else
                Dim ThisDocTypeDocAreas As New Dictionary(Of String, String)
                Try
                    Dim ThisDocType = AppSettings.GetDocType(DocTypeID)
                    Dim ThisDocTypeControl = LoadControl(ThisDocType.URL)
                    FillDocTypeDocAreasRecursive(ThisDocTypeControl, ThisDocTypeDocAreas)
                Catch ex As Exception
                    AddMessage("При выборке панелей из контрола произошла ошибка -> " & ex.Message)
                End Try
                _DocTypeDocAreas.Add(DocTypeID, ThisDocTypeDocAreas)
                Return ThisDocTypeDocAreas
            End If
        End Get
    End Property
    Private _DocTypeDocAreas As New Dictionary(Of Guid, Dictionary(Of String, String))

    Private Sub FillDocTypeDocAreasRecursive(ByVal ParentControl As Control, ByVal ControlDocAreas As Dictionary(Of String, String), Optional ByVal ParentName As String = "")
        If ParentControl.HasControls() Then
            For Each C As Control In ParentControl.Controls
                If TypeOf C Is eqCollapsiblePanel Then
                    Dim Name As String = ParentName & DirectCast(C, eqCollapsiblePanel).Name
                    ControlDocAreas.Add(C.ClientID, Name)
                    FillDocTypeDocAreasRecursive(C, ControlDocAreas, Name & " > ")
                ElseIf TypeOf C Is eqPanel Then
                    Dim Name As String = ParentName & DirectCast(C, eqPanel).Name
                    ControlDocAreas.Add(C.ClientID, Name)
                    FillDocTypeDocAreasRecursive(C, ControlDocAreas, Name & " > ")
                ElseIf TypeOf C Is System.Web.UI.WebControls.Panel Then
                    Dim Name As String = ParentName & DirectCast(C, System.Web.UI.WebControls.Panel).ID
                    ControlDocAreas.Add(C.ClientID, Name)
                    FillDocTypeDocAreasRecursive(C, ControlDocAreas, Name & " > ")
                ElseIf C.GetType().FullName = "AjaxControlToolkit.TabContainer" Then
                    Dim ControlProperty As System.Reflection.PropertyInfo = C.GetType.GetProperty("ID") '("ToolTip")
                    If ControlProperty Is Nothing Then
                        'Throw New Exception("Не найдено Property Visible у контрола " & TargetControl.GetType.FullName)
                    Else
                        Try
                            Dim Name As String = ParentName & ControlProperty.GetValue(C, Nothing)
                            ControlDocAreas.Add(C.ClientID, Name)
                            FillDocTypeDocAreasRecursive(C, ControlDocAreas, Name & " > ")
                        Catch ex As Exception
                            'Throw New Exception("Раскажи об этой ошибке! Control: " & TargetControl.GetType.FullName & " Property: Visible")
                        End Try
                    End If
                ElseIf C.GetType().FullName = "AjaxControlToolkit.TabPanel" Then
                    Dim ControlProperty_ID As System.Reflection.PropertyInfo = C.GetType.GetProperty("ID") '("HeaderText")
                    Dim ControlProperty As System.Reflection.PropertyInfo = C.GetType.GetProperty("HeaderText")
                    If ControlProperty Is Nothing Then
                        'Throw New Exception("Не найдено Property Visible у контрола " & TargetControl.GetType.FullName)
                    Else
                        Try
                            Dim IDN As String = ControlProperty_ID.GetValue(C, Nothing)
                            Dim Name As String = ParentName & ControlProperty.GetValue(C, Nothing)
                            ControlDocAreas.Add(IDN, Name)
                            FillDocTypeDocAreasRecursive(C, ControlDocAreas, Name & " > ")
                        Catch ex As Exception
                            'Throw New Exception("Раскажи об этой ошибке! Control: " & TargetControl.GetType.FullName & " Property: Visible")
                        End Try
                    End If
                Else
                    FillDocTypeDocAreasRecursive(C, ControlDocAreas, ParentName)
                End If
            Next
        End If
    End Sub

    Public Overrides Function PutValuesFromDataSetToSpecificationRowControls(ByVal SpecificationRow As Equipage.WebUI.eqSpecificationRow) As Boolean
        If SpecificationRow.Specification.ID = "eqRolesListCondSpecial" Then
            PutValuesFromDataSetToSpecificationRowControls = MyBase.PutValuesFromDataSetToSpecificationRowControls(SpecificationRow)

            Dim DocTypeIDDropDownList = DirectCast(SpecificationRow.FindControl("DocTypeID"), DropDownList)
            Dim FunctionsIDDropDownList = DirectCast(SpecificationRow.FindControl("FunctionID"), DropDownList)
            Dim FunctionControl = DirectCast(SpecificationRow.FindControl("Function"), ListDocFunction)
            Dim Parameter = DirectCast(SpecificationRow.FindControl("Parameter"), HiddenField)
            Dim Enabled = DirectCast(SpecificationRow.FindControl("Enabled"), CheckBox)
            If FunctionsIDDropDownList.SelectedValue <> String.Empty Then
                Dim FunctionID As New Guid(FunctionsIDDropDownList.SelectedValue)
                Dim DocTypeID As New Guid(DocTypeIDDropDownList.SelectedValue)

                Dim SelectedDocType = AppSettings.GetDocType(DocTypeID)
                Dim DocTypeFunctionInfo = SelectedDocType.GetFunctionInfo(FunctionID)

                If DocTypeFunctionInfo IsNot Nothing Then
                    FunctionControl.Visible = True
                    ' Function
                    FunctionControl.FunctionID = FunctionID
                    FunctionControl.FunctionEnabled = Enabled.Checked
                    FunctionControl.FunctionName = DocTypeFunctionInfo("FunctionName").ToString
                    FunctionControl.FunctionType = DocTypeFunctionInfo("ParameterType").ToString
                    FunctionControl.ParameterName = DocTypeFunctionInfo("ParameterName").ToString.Trim
                    FunctionControl.ParameterValue = Parameter.Value
                Else
                    FunctionControl.Visible = False
                End If

            Else
                FunctionControl.Visible = False
            End If

        Else
            Return MyBase.PutValuesFromDataSetToSpecificationRowControls(SpecificationRow)
        End If
    End Function

    Public Overrides Function PutValueFromDataSetToSpecificationRowControl(ByVal SpecificationRow As Equipage.WebUI.eqSpecificationRow, ByVal FieldName As String) As Boolean
        If SpecificationRow.Specification.ID = "eqRolesDocAreasList" AndAlso FieldName = "IDArea" Then
            Dim DocAreasIDArea = DirectCast(SpecificationRow.FindControl("IDArea"), DropDownList)
            Dim CurrentDataSetSpecificationDataRow = DataSetSpecificationDataRow("eqRolesDocAreasList", SpecificationRow.RowGuid)
            DocAreasIDArea.Items.Clear()
            If Not IsDBNull(CurrentDataSetSpecificationDataRow("IDDocType")) Then
                Dim DocAreasDocTypeID As Guid = CurrentDataSetSpecificationDataRow("IDDocType")
                For Each IDArea In DocTypeDocAreas(DocAreasDocTypeID)
                    DocAreasIDArea.Items.Add(New ListItem(IDArea.Value, IDArea.Key))
                Next

                ' Добавляем список областей из таблицы
                Dim sqlAreaList As String = ""
                sqlAreaList &= "SELECT ID, '> ' + AreaName + ' [' + PanelName + ']' AS PanelData "
                sqlAreaList &= "FROM eqDocAreasListSpec "
                sqlAreaList &= "WHERE (DocID = (SELECT TOP 1 ID FROM eqDocAreasList WHERE Deleted=0 and (FieldDocTypeID = @DocTypeID))) " '

                Dim cmdAreaList As New SqlClient.SqlCommand(sqlAreaList, AppSettings.Cnn)
                cmdAreaList.Parameters.AddWithValue("@DocTypeID", DocAreasDocTypeID)

                Dim tblCurrentAreaList As DataTable = AppSettings.GetDataTable(cmdAreaList)

                If tblCurrentAreaList.Rows.Count > 0 Then
                    For Each dr As DataRow In tblCurrentAreaList.Rows
                        DocAreasIDArea.Items.Add(New ListItem(dr("PanelData"), dr("ID").ToString()))
                    Next
                End If


                If DocAreasIDArea.Items.FindByValue(CurrentDataSetSpecificationDataRow("IDArea").ToString) IsNot Nothing Then
                    DocAreasIDArea.SelectedValue = CurrentDataSetSpecificationDataRow("IDArea").ToString
                End If
            End If
            Return True
        Else
            Return MyBase.PutValueFromDataSetToSpecificationRowControl(SpecificationRow, FieldName)
        End If
    End Function

    Public Overrides Function ReadValueFromSpecificationRowControlToDataSet(ByVal SpecificationRow As Equipage.WebUI.eqSpecificationRow, ByVal FieldName As String) As Boolean
        If SpecificationRow.Specification.ID = "eqRolesListCondSpecial" AndAlso FieldName = "Parameter" Then
            Dim FunctionControl = DirectCast(SpecificationRow.FindControl("Function"), ListDocFunction)
            Dim Parameter = DirectCast(SpecificationRow.FindControl("Parameter"), HiddenField)
            Dim Enabled = DirectCast(SpecificationRow.FindControl("Enabled"), CheckBox)
            Dim SrcParameter = DirectCast(FunctionControl.FindControl("FunctionTextBox"), eqTextBox)
            If SrcParameter.Visible Then
                Parameter.Value = SrcParameter.Text
            End If
            Return MyBase.ReadValueFromSpecificationRowControlToDataSet(SpecificationRow, FieldName)
        ElseIf SpecificationRow.Specification.ID = "eqRolesListCondSpecial" AndAlso FieldName = "Enabled" Then
            Dim FunctionControl = DirectCast(SpecificationRow.FindControl("Function"), ListDocFunction)
            Dim Enabled = DirectCast(SpecificationRow.FindControl("Enabled"), CheckBox)
            Enabled.Checked = FunctionControl.FunctionEnabled
            Return MyBase.ReadValueFromSpecificationRowControlToDataSet(SpecificationRow, FieldName)
        Else
            Return MyBase.ReadValueFromSpecificationRowControlToDataSet(SpecificationRow, FieldName)
        End If
    End Function

    Private Sub eqRolesListCondSpecial_RowInserted(ByVal SpecificationRow As Equipage.WebUI.eqSpecificationRow) Handles eqRolesListCondSpecial.RowInserted
        Dim Enabled = DirectCast(SpecificationRow.FindControl("Enabled"), CheckBox)
        Enabled.Checked = True
    End Sub

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender

    If RolesDocTypeIDViewState IsNot Nothing Then
      RolesDocTypeID.ClearSelection()
      For i As Integer = 0 To RolesDocTypeID.Items.Count - 1
        If RolesDocTypeID.Items(i).Value.ToString().ToUpper() = RolesDocTypeIDViewState.ToUpper() Then
          RolesDocTypeID.Items(i).Selected = True
        End If
      Next
    End If

    FilterSpecification(eqRolesControlDenied)
    FilterSpecification(eqRolesDocAreasList)
    FilterSpecification(eqRolesDocumentDisableByStatus)
    FilterSpecification(eqRolesListCondSpecial)
    FilterSpecification(eqRolesStatus)
    FilterSpecification(eqRolesToolBarDenied)
    FilterSpecification(eqRolesToolBarDeniedList)
    TabContainerRestrictions.Visible = RolesDocTypeID.SelectedItem IsNot Nothing AndAlso RolesDocTypeID.SelectedValue <> ""
  End Sub

    Private Sub FilterSpecification(ByVal spec As eqSpecification)
        For Each SpecificationRow In spec.Rows
            SpecificationRow.Visible = False
            Dim DocTypeID = DirectCast(SpecificationRow.FindControl(GetDocTypeIdName(spec)), DropDownList)
            If RolesDocTypeID.SelectedItem Is Nothing _
                    OrElse DocTypeID.SelectedItem Is Nothing _
                    OrElse RolesDocTypeID.SelectedValue = "All" _
                    OrElse DocTypeID.SelectedValue = "" Then
                SpecificationRow.Visible = True
            Else
                If DocTypeID.SelectedValue = RolesDocTypeID.SelectedValue Then
                    SpecificationRow.Visible = True
                End If
            End If
        Next
    End Sub

    Public Sub FilterSpecification_RowInserted(ByVal SpecificationRow As Equipage.WebUI.eqSpecificationRow)
        Dim DocTypeID = DirectCast(SpecificationRow.FindControl(GetDocTypeIdName(SpecificationRow.Specification)), DropDownList)
        If RolesDocTypeID.SelectedItem IsNot Nothing _
                AndAlso RolesDocTypeID.SelectedValue <> "" Then
            DocTypeID.SelectedValue = RolesDocTypeID.SelectedValue
            RefreshSpecificationRow(DocTypeID, EventArgs.Empty)
        End If
    End Sub

    Private Function GetDocTypeIdName(ByVal specCtrl As eqSpecification) As String
        Select Case specCtrl.ID
            Case "eqRolesDocAreasList", "eqRolesControlDenied"
                Return "IDDocType"
            Case Else
                Return "DocTypeID"
        End Select
    End Function

    Private Sub eqRolesDocumentsAccess_RowInserted(ByVal SpecificationRow As Equipage.WebUI.eqSpecificationRow) Handles eqRolesDocumentsAccess.RowInserted
        DirectCast(SpecificationRow.FindControl("Menu"), CheckBox).Checked = True
        DirectCast(SpecificationRow.FindControl("List"), CheckBox).Checked = True
        DirectCast(SpecificationRow.FindControl("Open"), CheckBox).Checked = True
        DirectCast(SpecificationRow.FindControl("New"), CheckBox).Checked = True
  End Sub

  ''' <summary>
  ''' Запоминание нового выбранного значения
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub RolesDocTypeID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RolesDocTypeID.SelectedIndexChanged
    If RolesDocTypeID.SelectedIndex <> -1 Then
      RolesDocTypeIDViewState = RolesDocTypeID.SelectedValue
    End If
  End Sub


  ''' <summary>
  ''' Сохранение и получение данных о состоянии выбора в списке RolesDocTypeIDViewState
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Property RolesDocTypeIDViewState() As String
    Get
      If ViewState("RolesDocTypeIDViewState") Is Nothing Then
        Return String.Empty
      Else
        Return ViewState("RolesDocTypeIDViewState")
      End If
    End Get
    Set(ByVal value As String)
      ViewState("RolesDocTypeIDViewState") = value
    End Set
  End Property

End Class