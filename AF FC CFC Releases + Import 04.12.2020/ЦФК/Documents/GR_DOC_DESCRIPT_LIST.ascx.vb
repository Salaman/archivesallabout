﻿Public Partial Class GR_DOC_DESCRIPT_LIST
    Inherits BaseDoc

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Dim Argument = New eqModalDialogArgument("")
        'Argument.ArgumentObject = GetIDDocument()
        'BasePage.CloseModalDialogWindow(Argument)
        BasePage.CloseWindow()
        ''''' Response.Redirect(BasePage.GetDocURL("GR_DOCUMENT.ascx", GetIDDocument(), BasePage.IsModal))

    End Sub

    Private Function GetIDDocument() As System.Guid

        Dim cmd As New SqlClient.SqlCommand("SELECT TOP 1 DocID AS FIRST_ID  FROM GR_tblDOC_DESCRIPT_LIST WHERE ID=@ID ORDER BY DESCRIPT_DATE")
        cmd.Parameters.AddWithValue("@ID", DocID.ToString)
        Dim DataTable = AppSettings.GetDataTable(cmd)
        If DataTable.Rows.Count = 0 Then
            'GetIDDocument = "00000000-0000-0000-0000-000000000000"
        Else
            GetIDDocument = DataTable.Rows(0)("FIRST_ID")
        End If
        Return GetIDDocument
    End Function

End Class