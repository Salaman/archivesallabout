﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="WordReportsFC.ascx.vb"
    Inherits="WebApplication.WordReportsFC" %>
<eq:eqPanel ID="TestReportPanel" runat="server">
    <eq:eqPanel ID="CapPanel" runat="server" Visible="true">
        <strong>Общие параметры для всех отчетов:</strong>
        <table>
            <tr>
                <td>
                    Архив:
                </td>
                <td>
                    <asp:DropDownList ID="ISN_ARCHIVE" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    Год:
                </td>
                <td>
                    <asp:TextBox ID="tbYear" runat="server" Text="" />
                </td>
            </tr>
        </table>
    </eq:eqPanel>
    <table>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:RadioButton runat="server" GroupName="FC" Text="Сеть архивов" ID="rb1" />
            </td>
            <td>
                Год:
            </td>
            <td>
                <asp:TextBox ID="tb1" runat="server" Text="" />
            </td>
        </tr>
        <tr>
            <td>
                Паспорт
            </td>
            <td>
                <asp:RadioButton runat="server" GroupName="FC" Text="Паспорт архива" ID="rb2" Enabled="true" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:RadioButton runat="server" GroupName="FC" Text="Сводный паспорт" ID="rb3" Enabled="False" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:RadioButton runat="server" GroupName="FC" Text="Динамика (абс.) по каждому виду паспортов"
                    ID="rb4" Enabled="False" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:RadioButton runat="server" GroupName="FC" Text="Динамика (%%)  по каждому виду паспортов"
                    ID="rb5" Enabled="False" />
            </td>
        </tr>
        <tr>
            <td>
                Статистика по архивам
            </td>
            <td>
                <asp:RadioButton runat="server" GroupName="FC" Text="Поступление" ID="rb6" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:RadioButton runat="server" GroupName="FC" Text="Собственность" ID="rb7" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:RadioButton runat="server" GroupName="FC" Text="Условия хранения" ID="rb8" />
            </td>
            <td>
                Год:
            </td>
            <td>
                <asp:TextBox ID="tb8" runat="server" Text="" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:RadioButton runat="server" GroupName="FC" Text="Оснащенность сигнализацией"
                    ID="rb9" />
            </td>
            <td>
                Год:
            </td>
            <td>
                <asp:TextBox ID="tb9" runat="server" Text="" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:RadioButton runat="server" GroupName="FC" Text="Необнаруженные дела" ID="rb10" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:RadioButton runat="server" GroupName="FC" Text="Физическое состояние документов на бумажной основе"
                    ID="rb11" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:RadioButton runat="server" GroupName="FC" Text="Физическое состояние спецдокументации"
                    ID="rb12" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:RadioButton runat="server" GroupName="FC" Text="СФ и ФП документов на бумажной основе"
                    ID="rb13" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:RadioButton runat="server" GroupName="FC" Text="СФ и ФП спецдокументации" ID="rb14" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:RadioButton runat="server" GroupName="FC" Text="НСА к фондам архива" ID="rb15"
                    Enabled="False" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:RadioButton runat="server" GroupName="FC" Text="НСА к фондам группы архивов"
                    ID="rb16" Enabled="False" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:RadioButton runat="server" GroupName="FC" Text="Комплектность описей" ID="rb17" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:RadioButton runat="server" GroupName="FC" Text="Неописанные документы" ID="rb18" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:RadioButton runat="server" GroupName="FC" Text="Каталогизация документов на бумажной основе"
                    ID="rb19" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:RadioButton runat="server" GroupName="FC" Text="Каталогизация спецдокументации"
                    ID="rb20" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:RadioButton runat="server" GroupName="FC" Text="Базы данных" ID="rb21" />
            </td>
        </tr>
        <tr>
            <td>
                Перечни фондов
            </td>
            <td>
                <asp:RadioButton runat="server" GroupName="FC" Text="Фонды в наличии" ID="rb22" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:RadioButton runat="server" GroupName="FC" Text="Фонды Федеральной собственности"
                    ID="rb23" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:RadioButton runat="server" GroupName="FC" Text="Фонды собственности субъектов Федерации"
                    ID="rb24" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:RadioButton runat="server" GroupName="FC" Text="Фонды муниципальной собственности"
                    ID="rb25" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:RadioButton runat="server" GroupName="FC" Text="Фонды собственности физических лиц"
                    ID="rb26" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:RadioButton runat="server" GroupName="FC" Text="Фонды собственности юридических лиц"
                    ID="rb27" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:RadioButton runat="server" GroupName="FC" Text="Фонды «совместной» собственности"
                    ID="rb28" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:RadioButton runat="server" GroupName="FC" Text="Фонды, находящиеся в розыске"
                    ID="rb29" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:RadioButton runat="server" GroupName="FC" Text="Секретные фонды" ID="rb30" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:RadioButton runat="server" GroupName="FC" Text="Частично секретные фонды" ID="rb31" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:RadioButton runat="server" GroupName="FC" Text="Фонды, по которым поступили документы за указываемый период"
                    ID="rb32" Enabled="False" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:RadioButton runat="server" GroupName="FC" Text="Фонды, по которым выбыли документы за указываемый период"
                    ID="rb33" Enabled="False" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:RadioButton runat="server" GroupName="FC" Text="Поступившие за указываемый период фонды"
                    ID="rb34" Enabled="False" />
            </td>
        </tr>
        <tr>
            <td>
                Справочники
            </td>
            <td>
                <asp:RadioButton runat="server" GroupName="FC" Text="Межархивный тематический" ID="rb35"
                    Enabled="False" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:RadioButton runat="server" GroupName="FC" Text="Межархивный структурный" ID="rb36"
                    Enabled="False" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:RadioButton runat="server" GroupName="FC" Text="Именной указатель" ID="rb37"
                    Enabled="False" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:RadioButton runat="server" GroupName="FC" Text="Географический указатель" ID="rb38"
                    Enabled="False" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:RadioButton runat="server" GroupName="FC" Text="Предметный указатель" ID="rb39"
                    Enabled="False" />
            </td>
        </tr>
    </table>
    <br />
    <asp:Button runat="server" ID="btMakeReport" Text="Построить отчет" />
</eq:eqPanel>
