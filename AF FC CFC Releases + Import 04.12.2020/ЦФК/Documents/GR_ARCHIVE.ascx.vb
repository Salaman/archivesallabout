﻿Public Partial Class GR_ARCHIVE
    Inherits BaseDoc
    Public Overrides Function SaveDoc() As Boolean


        Try

            If Len(OKPO.Text) = 0 Then
                'ChkMsg = "Поле 'Протокол ЭПК от' обязательно для заполнения."
                'AddMessage(ChkMsg)
                OKPO.BackColor = Drawing.Color.Pink
            End If

            MyBase.SaveDoc()
        Catch ex As Exception
            AddMessage(ex.Message)
        End Try
    End Function

    Public Overrides Function NewDoc() As Boolean
        'Dim archTable = AppSettings.GetDataTable(New SqlClient.SqlCommand("SELECT ID FROM tblArchive WHERE Deleted=0"))
        'If archTable.Rows.Count = 1 Then
        '    Response.Redirect(BasePage.GetDocURL(DocTypeURL, DirectCast(archTable.Rows(0).Item("ID"), Guid), BasePage.IsModal))
        '    Return True
        'ElseIf archTable.Rows.Count > 1 Then
        '    Response.Redirect(BasePage.GetListDocURL(DocTypeURL, BasePage.IsModal))
        '    Return True
        'End If
        Return MyBase.NewDoc()
    End Function

    Public Overrides Function DeleteDoc() As Boolean
        Dim DeleteRecursivelyCommand As New SqlClient.SqlCommand("dbo.DeleteRecursively") With {.CommandType = CommandType.StoredProcedure}
        DeleteRecursivelyCommand.Parameters.AddWithValue("@ISN", DataSetHeaderDataRow("ISN_ARCHIVE"))
        DeleteRecursivelyCommand.Parameters.AddWithValue("@OBJ", "archive")
        AppSettings.ExecCommand(DeleteRecursivelyCommand)

        Return MyBase.DeleteDoc()
    End Function

    Public Overrides Sub PutValueToControl(ByVal Value As Object, ByVal TargetControl As System.Web.UI.Control, Optional ByVal FieldFormat As String = "")
        If TypeOf TargetControl Is CheckBox Then
            DirectCast(TargetControl, CheckBox).Checked = (Value Is Nothing OrElse Value.ToString.Trim.ToUpper = "Y")
        Else
            MyBase.PutValueToControl(Value, TargetControl, FieldFormat)
        End If
    End Sub

    Public Overrides Function GetValueFromControl(ByVal SourceControl As System.Web.UI.Control) As Object
        If TypeOf SourceControl Is CheckBox Then
            Return IIf(DirectCast(SourceControl, CheckBox).Checked, "Y", "N")
        Else
            Return MyBase.GetValueFromControl(SourceControl)
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    

    Private Sub ChooseArchives_Click(ByVal sender As Object, ByVal e As System.EventArgs)


    End Sub
End Class