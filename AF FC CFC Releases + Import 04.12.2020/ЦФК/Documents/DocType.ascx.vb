﻿Public Partial Class DocType
    Inherits BaseDoc

    Public Overrides Function FindControl(ByVal id As String) As System.Web.UI.Control
        If id = "DocType" Then
            Return MyBase.FindControl("dtDocType")
        Else
            Return MyBase.FindControl(id)
        End If
    End Function

    Public Overrides Function CheckDataSetHeaderValuesBeforeSaveDoc() As Boolean
        CheckDataSetHeaderValuesBeforeSaveDoc = MyBase.CheckDataSetHeaderValuesBeforeSaveDoc
        If SavedState.SelectedValue = DeletedState.SelectedValue Then
            AddMessage("SavedState должен отличаться от DeletedState")
            CheckDataSetHeaderValuesBeforeSaveDoc = False
        End If
    End Function

    Public Overrides Function PutValuesFromDataSetToHeaderControls() As Boolean
        ' States
        SavedState.Items.Clear()
        DeletedState.Items.Clear()
        If IsDBNull(DataSetHeaderDataRow("BaseDocTypeID")) OrElse DataSetHeaderDataRow("BaseDocTypeID") = Nothing Then
            SavedState.Items.Add("")
            DeletedState.Items.Add("")
            For Each R In DataSetSpecificationDataTable("eqvDocStates").DefaultView
                Dim StateID = R("ID").ToString
                Dim StateName = R("StateName").ToString
                SavedState.Items.Add(New ListItem(StateName, StateID))
                DeletedState.Items.Add(New ListItem(StateName, StateID))
            Next
        Else
            Dim BaseDocTypeGuid = DirectCast(DataSetHeaderDataRow("BaseDocTypeID"), Guid)
            Dim BaseDocType = AppSettings.GetDocType(BaseDocTypeGuid)
            DataSetHeaderDataRow("SavedState") = BaseDocType.GetSavedState.StateID
            DataSetHeaderDataRow("DeletedState") = BaseDocType.GetDeletedState.StateID
            Dim GetStatesCmd As New SqlClient.SqlCommand("SELECT * FROM eqDocStates WHERE DocTypeID=@DocTypeID")
            GetStatesCmd.Parameters.AddWithValue("@DocTypeID", BaseDocTypeGuid)
            Dim BaseDocTypeStatesTable = AppSettings.GetDataTable(GetStatesCmd)
            For Each R As DataRow In BaseDocTypeStatesTable.Rows
                Dim StateID = R("ID").ToString
                Dim StateName = R("StateName").ToString
                If StateID = BaseDocType.GetSavedState.StateID.ToString Then
                    SavedState.Items.Add(New ListItem(StateName, StateID) With {.Selected = True})
                End If
                If StateID = BaseDocType.GetDeletedState.StateID.ToString Then
                    DeletedState.Items.Add(New ListItem(StateName, StateID) With {.Selected = True})
                End If
            Next
            BaseDocTypeStates.DataSource = BaseDocTypeStatesTable
            BaseDocTypeStates.DataBind()
        End If
        Return MyBase.PutValuesFromDataSetToHeaderControls()
    End Function

    Public Overrides Function SaveDoc() As Boolean
        Try
            AppSettings.BeginTransaction()
            SaveDoc = MyBase.SaveDoc()
            If SaveDoc Then
                ReloadAppSettings()
                RefreshDefault()
            End If
            AppSettings.CommitTransaction()
        Catch ex As Exception
            AppSettings.RollbackTransaction()
            AddMessage("Сохранение отменено -> " & ex.Message)
        End Try
    End Function

    Public Sub ReloadAppSettings()
        Try
            Dim SampleAppSettings As New eqAppSettings
            SampleAppSettings.OpenSession(AppSettings.Cnn, AppSettings.UserInfo.UserLogin, AppSettings.Trn)
            SampleAppSettings.Manager = My.Resources.ResourceManager
            Session("AppSettings") = SampleAppSettings
        Catch ex As Exception
            Throw New Exception("Ошибка при перегрузке AppSettings -> " & ex.Message)
        End Try
    End Sub

    Public Sub RefreshDefault()
        If TypeOf Page Is _Default Then
            DirectCast(Page, _Default).UpdateSystemMenu()
        End If
    End Sub

    Private Sub RefreshStates()
        ReadValuesFromControlsToDataSet()
        PutValuesFromDataSetToControls()
    End Sub

    Private Sub eqvDocStates_RowDeleted(ByVal SpecificationRow As Equipage.WebUI.eqSpecificationRow) Handles eqvDocStates.RowDeleted
        RefreshStates()
    End Sub

    Private Sub eqvDocStates_RowInserted(ByVal SpecificationRow As Equipage.WebUI.eqSpecificationRow) Handles eqvDocStates.RowInserted
        RefreshStates()
    End Sub

    Public Sub StateName_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        RefreshStates()
    End Sub

    Private Sub BaseDocTypeID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles BaseDocTypeID.SelectedIndexChanged
        RefreshStates()
    End Sub

    Private Sub CreateRenameButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CreateRenameButton.Click
        Try
            AppSettings.BeginTransaction()
            Dim Specification As Boolean
            If TableNameList.SelectedValue = "" And CalcControlledDocType.GetHeaderTableName = "" Then
                Specification = False
            ElseIf TableNameList.SelectedValue = "" Then
                Specification = True
            ElseIf TableNameList.SelectedValue = CalcControlledDocType.GetHeaderTableName Then
                Specification = False
            Else
                Specification = True
            End If
            If ValidateTable(NewTableName.Text, Specification) Then
                If TableNameList.SelectedValue = "" Then
                    ' Create
                    If Specification Then
                        Dim SqlCommand As New SqlClient.SqlCommand("ADD_COMMON_FIELDS_SPECIFICATION")
                        SqlCommand.CommandType = CommandType.StoredProcedure
                        SqlCommand.Parameters.AddWithValue("@DocTypeID", DocID)
                        SqlCommand.Parameters.AddWithValue("@SPECTABLE", NewTableName.Text)
                        AppSettings.ExecCommand(SqlCommand)
                    Else
                        Dim SqlCommand As New SqlClient.SqlCommand("ADD_COMMON_FIELDS_HEADER")
                        SqlCommand.CommandType = CommandType.StoredProcedure
                        SqlCommand.Parameters.AddWithValue("@DocTypeID", DocID)
                        SqlCommand.Parameters.AddWithValue("@HEADERTABLE", NewTableName.Text)
                        AppSettings.ExecCommand(SqlCommand)
                    End If
                    ClearingDocStructure()
                    ReloadAppSettings()
                    RefreshDefault()
                Else
                    If TableNameList.Items.FindByValue(NewTableName.Text) Is Nothing Then
                        ' Rename
                        Dim SqlCommand As New SqlClient.SqlCommand("UPDATE eqDocStructure SET TableName=@NewTableName WHERE DocTypeID=@DocTypeID AND TableName=@OldTableName")
                        SqlCommand.Parameters.AddWithValue("@DocTypeID", DocID)
                        SqlCommand.Parameters.AddWithValue("@OldTableName", TableNameList.SelectedValue)
                        SqlCommand.Parameters.AddWithValue("@NewTableName", NewTableName.Text)
                        AppSettings.ExecCommand(SqlCommand)
                        ClearingDocStructure()
                        ReloadAppSettings()
                        RefreshDefault()
                    Else
                        AddMessage("И че за нах?")
                    End If
                End If
            End If
            AppSettings.CommitTransaction()
        Catch ex As Exception
            AppSettings.RollbackTransaction()
            AddMessage("Ошибка -> ")
        End Try
    End Sub

    Public Function ClearingDocStructure() As Boolean
        Try
            Dim ClearingDocStructureSB As New StringBuilder
            ClearingDocStructureSB.AppendLine("DELETE FROM dbo.eqDocStructure WHERE dbo.ExistsColumn(TableName,FieldName) = 0 OR Deleted=1")
            ClearingDocStructureSB.AppendLine("DELETE FROM dbo.eqListDoc WHERE Deleted=1")
            ClearingDocStructureSB.AppendLine("DELETE FROM dbo.eqDocTypes WHERE Deleted=1")
            Dim SqlCommand As New SqlClient.SqlCommand(ClearingDocStructureSB.ToString)
            AppSettings.ExecCommand(SqlCommand)
            Return True
        Catch ex As Exception
            AddMessage("ClearingDocStructure Error -> " & ex.Message)
            Return False
        End Try
    End Function

    Private Function ValidateTable(ByVal TableName As String, Optional ByVal Specification As Boolean = False) As Boolean
        ValidateTable = True
        If CalcTablesDictionary.ContainsKey(TableName) Then
            Dim TableColumns = CalcTablesDictionary(NewTableName.Text)
            If TableColumns.ContainsKey("ID") Then
                If TableColumns("ID")("TypeName") <> "uniqueidentifier" Then
                    AddMessage("ID должно быть uniqueidentifier")
                    ValidateTable = False
                End If
            Else
                AddMessage("Колонки ID не нашел")
                ValidateTable = False
            End If
            If TableColumns.ContainsKey("OwnerID") Then
                If TableColumns("OwnerID")("TypeName") <> "uniqueidentifier" Then
                    AddMessage("OwnerID должно быть uniqueidentifier")
                    ValidateTable = False
                End If
            Else
                AddMessage("Колонки OwnerID не нашел")
                ValidateTable = False
            End If
            If TableColumns.ContainsKey("CreationDateTime") Then
                If TableColumns("CreationDateTime")("TypeName") <> "smalldatetime" And TableColumns("CreationDateTime")("TypeName") <> "datetime" Then
                    AddMessage("CreationDateTime должно быть smalldatetime или datetime")
                    ValidateTable = False
                End If
            Else
                AddMessage("Колонки CreationDateTime не нашел")
                ValidateTable = False
            End If
            If Not Specification Then
                If TableColumns.ContainsKey("StatusID") Then
                    If TableColumns("StatusID")("TypeName") <> "uniqueidentifier" Then
                        AddMessage("StatusID должно быть uniqueidentifier")
                        ValidateTable = False
                    End If
                Else
                    AddMessage("Колонки StatusID не нашел")
                    ValidateTable = False
                End If
                If TableColumns.ContainsKey("Deleted") Then
                    If TableColumns("Deleted")("TypeName") <> "int" And TableColumns("Deleted")("TypeName") <> "bit" Then
                        AddMessage("Deleted должно быть int или bit")
                        ValidateTable = False
                    End If
                Else
                    AddMessage("Колонки Deleted не нашел")
                    ValidateTable = False
                End If
            Else
                If TableColumns.ContainsKey("DocID") Then
                    If TableColumns("DocID")("TypeName") <> "uniqueidentifier" Then
                        AddMessage("DocID должно быть uniqueidentifier")
                        ValidateTable = False
                    End If
                Else
                    AddMessage("Колонки DocID не нашел")
                    ValidateTable = False
                End If
                If TableColumns.ContainsKey("RowID") Then
                    If TableColumns("RowID")("TypeName") <> "int" Then
                        AddMessage("RowID должно быть int")
                        ValidateTable = False
                    End If
                Else
                    AddMessage("Колонки RowID не нашел")
                    ValidateTable = False
                End If
            End If
        Else
            AddMessage("Я не нашел такой таблицы... Придумай ченить поумнее ;)")
            ValidateTable = False
        End If
    End Function

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        NewTableName.Visible = False
        CreateRenameButton.Visible = False
        RepeaterColumns.Visible = False
        If DocID <> Guid.Empty Then
            If CalcControlledDocType Is Nothing Then
                CreateRenameButton.Visible = False
            Else
                ' Tables
                Dim SelectedSpecificationTableName = TableNameList.SelectedValue
                TableNameList.Items.Clear()
                TableNameList.Items.Add("")
                If CalcControlledDocType.GetHeaderTableName <> "" Then
                    TableNameList.Items.Add(New ListItem("HEADER: " & CalcControlledDocType.GetHeaderTableName, CalcControlledDocType.GetHeaderTableName))
                End If
                For Each SpecTableName In CalcControlledDocType.GetSpecTableNames
                    TableNameList.Items.Add(New ListItem("SPECIFICATION: " & SpecTableName, SpecTableName))
                    If SpecTableName = SelectedSpecificationTableName Then
                        TableNameList.SelectedValue = SpecTableName
                    End If
                Next
                If TableNameList.Items.FindByValue(SelectedSpecificationTableName) IsNot Nothing Then
                    TableNameList.SelectedValue = SelectedSpecificationTableName
                End If
                ' RepeaterColumns
                If TableNameList.SelectedValue = "" And CalcControlledDocType.GetHeaderTableName = "" Then
                    CreateRenameButton.Text = "Фпендюрить HEADER"
                ElseIf TableNameList.SelectedValue = "" Then
                    CreateRenameButton.Text = "Фпендюрить SPECIFICATION"
                Else
                    CreateRenameButton.Text = "Подменить"
                End If
                CreateRenameButton.Visible = True
                NewTableName.Visible = True
                ' RepeaterColumns
                _CalcTablesDictionary = Nothing
                If CalcTablesDictionary.ContainsKey(TableNameList.SelectedValue) Then
                    RepeaterColumns.DataSource = CalcTablesDictionary(TableNameList.SelectedValue).Values
                    RepeaterColumns.DataBind()
                    RepeaterColumns.Visible = True
                End If
            End If
        End If
        If BaseDocTypeID.SelectedValue = String.Empty Then
            PlaceHolderDocStates.Visible = True
            DocStatesUpdatePanelAddStandart.Visible = True
            BaseDocTypeStates.Visible = False
        Else
            PlaceHolderDocStates.Visible = False
            DocStatesUpdatePanelAddStandart.Visible = False
            BaseDocTypeStates.Visible = True
        End If
    End Sub

    Public Sub ModifyButton_OnCommand(ByVal sender As Object, ByVal e As CommandEventArgs)
        Dim CommandName = e.CommandName
        Dim CommandArguments = e.CommandArgument.ToString.Split(New Char() {","})
        Select Case CommandName
            Case "OpenFieldInfo"
                Dim FieldInfoID As New Guid(CommandArguments(0))
                BasePage.OpenWindow(BasePage.GetDocURL("DocStructure.ascx", FieldInfoID, True))
            Case "CreateFieldInfo"
                Dim ColumnName = CommandArguments(0)
                If TableNameList.SelectedValue = CalcControlledDocType.GetHeaderTableName Then
                    Dim SqlCommand As New SqlClient.SqlCommand("ADD_HEADER_FIELD")
                    SqlCommand.CommandType = CommandType.StoredProcedure
                    SqlCommand.Parameters.AddWithValue("@DocTypeID", DocID)
                    SqlCommand.Parameters.AddWithValue("@FieldNameDisplay", ColumnName)
                    SqlCommand.Parameters.AddWithValue("@FieldDescription", ColumnName)
                    SqlCommand.Parameters.AddWithValue("@TableName", TableNameList.SelectedValue)
                    SqlCommand.Parameters.AddWithValue("@FieldName", ColumnName)
                    AppSettings.ExecCommand(SqlCommand)
                Else
                    Dim SqlCommand As New SqlClient.SqlCommand("ADD_SPECIFICATION_FIELD")
                    SqlCommand.CommandType = CommandType.StoredProcedure
                    SqlCommand.Parameters.AddWithValue("@DocTypeID", DocID)
                    SqlCommand.Parameters.AddWithValue("@FieldNameDisplay", ColumnName)
                    SqlCommand.Parameters.AddWithValue("@FieldDescription", ColumnName)
                    SqlCommand.Parameters.AddWithValue("@TableName", TableNameList.SelectedValue)
                    SqlCommand.Parameters.AddWithValue("@FieldName", ColumnName)
                    AppSettings.ExecCommand(SqlCommand)
                End If
                ClearingDocStructure()
            Case "DeleteFieldInfo"
                Dim FieldInfoID As New Guid(CommandArguments(0))
                Dim SqlCommand As New SqlClient.SqlCommand("DELETE FROM dbo.eqDocStructure WHERE ID=@ID")
                SqlCommand.Parameters.AddWithValue("@ID", FieldInfoID)
                AppSettings.ExecCommand(SqlCommand)
                ClearingDocStructure()
            Case "OpenListDocFieldInfo"
                Dim FieldInfoID As New Guid(CommandArguments(0))
                BasePage.OpenWindow(BasePage.GetDocURL("DocListDoc.ascx", FieldInfoID, True))
            Case "CreateListDocFieldInfo"
                Dim ColumnName = CommandArguments(0)
                Dim FieldInfoID As New Guid(CommandArguments(1))
                Dim SqlCommand As New SqlClient.SqlCommand("ADD_FIELD_TO_DISPLAY")
                SqlCommand.CommandType = CommandType.StoredProcedure
                SqlCommand.Parameters.AddWithValue("@DocTypeID", DocID)
                SqlCommand.Parameters.AddWithValue("@FieldID", FieldInfoID)
                ' << added by Anatoly Melkov on 2009-03-12 
                Dim GetDisplaySqlCommand As New SqlClient.SqlCommand("SELECT FieldNameDisplay FROM eqDocStructure WHERE ID=@ID")
                GetDisplaySqlCommand.Parameters.AddWithValue("@ID", FieldInfoID)
                Dim FieldDesc = AppSettings.GetDataTable(GetDisplaySqlCommand).Rows(0)(0).ToString()
                ' >>
                If IsDBNull(FieldDesc) OrElse FieldDesc = String.Empty Then
                    SqlCommand.Parameters.AddWithValue("@FieldDesc", ColumnName)
                Else
                    SqlCommand.Parameters.AddWithValue("@FieldDesc", FieldDesc)
                End If
                AppSettings.ExecCommand(SqlCommand)
                ClearingDocStructure()
            Case "DeleteListDocFieldInfo"
                Dim FieldInfoID As New Guid(CommandArguments(0))
                Dim SqlCommand As New SqlClient.SqlCommand("DELETE FROM dbo.eqListDoc WHERE ID=@ID")
                SqlCommand.Parameters.AddWithValue("@ID", FieldInfoID)
                AppSettings.ExecCommand(SqlCommand)
                ClearingDocStructure()
        End Select
    End Sub

#Region "Calc Properties"

    Public ReadOnly Property CalcControlledDocType() As eqDocType
        Get
            If DocID <> Guid.Empty Then
                Return AppSettings.GetDocType(DocID)
            Else
                Return Nothing
            End If
        End Get
    End Property

    Private _CalcTablesDictionary As Dictionary(Of String, Dictionary(Of String, DataRow))
    Public ReadOnly Property CalcTablesDictionary() As Dictionary(Of String, Dictionary(Of String, DataRow))
        Get
            If _CalcTablesDictionary Is Nothing Then
                _CalcTablesDictionary = New Dictionary(Of String, Dictionary(Of String, DataRow))
                Dim SqlCommand As New SqlClient.SqlCommand("SELECT T.name TableName, C.name ColumnName, CT.name TypeName, DS.ID FieldID, LD.ID ListDocFieldID" & vbCrLf & _
                                                                       "FROM (SELECT object_id,name FROM sys.tables UNION SELECT object_id,name FROM sys.views) T" & vbCrLf & _
                                                                       "JOIN (SELECT * FROM sys.columns) C ON T.object_id=C.object_id" & vbCrLf & _
                                                                       "JOIN (SELECT * FROM sys.types) CT ON C.system_type_id=CT.system_type_id" & vbCrLf & _
                                                                       "LEFT JOIN eqDocStructure DS ON T.name=DS.TableName AND C.name=DS.FieldName AND DS.Deleted=0 AND DS.DocTypeID=@DocTypeID" & vbCrLf & _
                                                                       "LEFT JOIN eqListDoc LD ON DS.ID=LD.FieldID AND LD.Deleted=0" & vbCrLf & _
                                                                       "WHERE NOT CT.name='sysname'")
                SqlCommand.Parameters.AddWithValue("@DocTypeID", DocID)
                Dim TablesColumns = From R As DataRow In AppSettings.GetDataTable(SqlCommand).Rows Group By TableName = DirectCast(R("TableName"), String) Into Group Order By TableName
                For Each TableColumn In TablesColumns
                    _CalcTablesDictionary.Add(TableColumn.TableName, TableColumn.Group.ToDictionary(Function(R) DirectCast(R("ColumnName"), String), Function(R) R))
                Next
            End If
            Return _CalcTablesDictionary
        End Get
    End Property

#End Region

    Private Sub DocStatesUpdatePanelAddStandart_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles DocStatesUpdatePanelAddStandart.Click
        Dim SavedStateID = AddDocState("Сохранен", "Сохранен").ToString
        Dim DeletedStateID = AddDocState("Удален", "Удален").ToString
        RefreshStates()
        SavedState.SelectedValue = SavedStateID
        DeletedState.SelectedValue = DeletedStateID
    End Sub

    Public Function AddDocState(ByVal StateName As String, ByVal StateDescription As String) As Guid
        Dim NewStatesRow = eqvDocStates.InsertRow()
        Dim StateNameTB = DirectCast(NewStatesRow.FindControl("StateName"), TextBox)
        Dim StateDescriptionTB = DirectCast(NewStatesRow.FindControl("StateDescription"), TextBox)
        StateNameTB.Text = StateName
        StateDescriptionTB.Text = StateDescription
        Return NewStatesRow.RowGuid
    End Function

    Private Sub DocToolBarUpdatePanelAddStandart_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles DocToolBarUpdatePanelAddStandart.Click
        AddDocToolBar("Новый", "Images\New.gif", "NewDoc", "Новый элемент", "L")
        AddDocToolBar("Открыть", "Images\Open.gif", "LoadDoc", "Открыть элемент", "L")
        AddDocToolBar("Новый", "Images\New.gif", "NewDoc", "Новый элемент", "D")
        AddDocToolBar("Список", "Images\Folder.gif", "ListDoc", "Перейти в список", "D")
        AddDocToolBar("Сохранить", "Images\Save.gif", "SaveDoc", "Сохранить элемент", "D")
        AddDocToolBar("Удалить", "Images\Delete.gif", "DeleteDoc", "Удалить элемент", "D")
    End Sub

    Public Function AddDocToolBar(ByVal Caption As String, ByVal ImageURL As String, ByVal CommandName As String, ByVal ToolTip As String, ByVal ShowInListOrDocument As Char) As Guid
        Dim NewToolBarRow = eqvDocToolBar.InsertRow()
        Dim CaptionTB = DirectCast(NewToolBarRow.FindControl("Caption"), TextBox)
        Dim ImageURLTB = DirectCast(NewToolBarRow.FindControl("ImageURL"), TextBox)
        Dim CommandNameTB = DirectCast(NewToolBarRow.FindControl("CommandName"), TextBox)
        Dim ToolTipTB = DirectCast(NewToolBarRow.FindControl("ToolTip"), TextBox)
        Dim ShowInListOrDocumentRBL = DirectCast(NewToolBarRow.FindControl("ShowInListOrDocument"), RadioButtonList)
        CaptionTB.Text = Caption
        ImageURLTB.Text = ImageURL
        CommandNameTB.Text = CommandName
        ToolTipTB.Text = ToolTip
        ShowInListOrDocumentRBL.SelectedValue = ShowInListOrDocument
        Return NewToolBarRow.RowGuid
    End Function

    Public Function GetOpenFieldInfoUrl(ByVal DataItem As DataRow) As String
        If IsDBNull(DataItem("FieldID")) Then
            Return String.Empty
        Else
            Return BasePage.GetDocURL("DocStructure.ascx", DataItem("FieldID"), True)
        End If
    End Function

    Public Function GetOpenListDocFieldInfoUrl(ByVal DataItem As DataRow) As String
        If IsDBNull(DataItem("ListDocFieldID")) Then
            Return String.Empty
        Else
            Return BasePage.GetDocURL("DocListDoc.ascx", DataItem("ListDocFieldID"), True)
        End If
    End Function

    Private Sub TableNameList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TableNameList.SelectedIndexChanged
        RepeaterColumns.Focus()
    End Sub

End Class