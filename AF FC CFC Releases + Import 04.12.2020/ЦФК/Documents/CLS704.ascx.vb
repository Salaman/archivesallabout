﻿Partial Public Class CLS704
    Inherits BaseDoc

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Request.QueryString("ParentID") IsNot Nothing Then
                ISN_HIGH_CLS.DocID = New Guid(Request.QueryString("ParentID"))
            End If
        End If
    End Sub

    Private Sub clearISN_HIGH_CLS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles clearISN_HIGH_CLS.Click
        ISN_HIGH_CLS.DocID = Guid.Empty
    End Sub

End Class