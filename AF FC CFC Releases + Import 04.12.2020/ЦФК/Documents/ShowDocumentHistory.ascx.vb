﻿Partial Public Class ShowDocumentHistory
    Inherits BaseDoc

#Region "Обработка событий"
    ''' <summary>
    ''' Загрузка страницы
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then

            If Request.QueryString("ForDocID") IsNot Nothing Then
                LabelDoc.Text = "Документ: " & Request.QueryString("ForDocID")
            End If

            Dim d As Date
            d = Now

            If "" & Request.QueryString.Item("ReportDocID") = "" Then
                FillddlDocTypeID()
                DocTypeIDPanel.Visible = True
            Else
                DocTypeIDPanel.Visible = False
            End If

            StartDate.Text = Format(d.AddDays(-1), "dd-MM-yyyy")
            FinishDate.Text = Format(d.AddDays(1), "dd-MM-yyyy")
            GenerateReport()

        End If
        ' ScriptManager.GetCurrent(Page).RegisterPostBackControl(Me)
    End Sub

    ''' <summary>
    ''' вызов построителя отчета
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        GenerateReport()
    End Sub

    ''' <summary>
    ''' Переключение страницы 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub GridViewFieldHistory_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridViewFieldHistory.PageIndexChanging
        Dim dtGridViewFieldHistory As DataTable
        dtGridViewFieldHistory = DirectCast(Session("dtGridViewFieldHistory"), DataTable)
        GridViewFieldHistory.DataSource = dtGridViewFieldHistory
        GridViewFieldHistory.DataBind()
        GridViewFieldHistory.PageIndex = e.NewPageIndex
    End Sub

    ''' <summary>
    ''' Переключение страницы 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub GridViewStatusHistory_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridViewStatusHistory.PageIndexChanging
        Dim dtGridViewStatusHistory As DataTable
        dtGridViewStatusHistory = DirectCast(Session("dtGridViewStatusHistory"), DataTable)
        GridViewStatusHistory.DataSource = dtGridViewStatusHistory
        GridViewStatusHistory.DataBind()
        GridViewStatusHistory.PageIndex = e.NewPageIndex
    End Sub

#End Region

#Region "Построение отчета по изменениям"

    ''' <summary>
    ''' Построение отчета по изменениям
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub GenerateReport()
        Dim SQLGridViewStatusHistory = <sql>
select top 1000 EventTime CreationDateTime, UserName as Author, StatusName as StatusName
from 
    eqDocStatesLOG 
where 
(DocID=@DocID or @DocID='00000000-0000-0000-0000-000000000000')
  and
(DocTypeID=@DocTypeID or @DocTypeID='00000000-0000-0000-0000-000000000000')
  and
 (UserName=@Author or @Author='')
 and EventTime between @StartDate and @FinishDate
order by EventTime desc
                                   </sql>

        Dim SQLGridViewFieldHistory = <sql>
select top 10000 EventTime CreationDateTime, UserName Author, FieldNameDisplay, PrevValue, Value
from 
    eqDocFieldLog
where 
(DocID=@DocID or @DocID='00000000-0000-0000-0000-000000000000')
  and
(DocTypeID=@DocTypeID or @DocTypeID='00000000-0000-0000-0000-000000000000')
  and
 (UserName=@Author or @Author='')
  and
 (FieldNameDisplay=@FieldNameDisplay or @FieldNameDisplay='')
 and EventTime between @StartDate and @FinishDate
order by EventTime desc
                                  </sql>

        Try
            Dim sDocID As String = ""
            Dim sDocTypeID As String = ""
            Dim sFieldNameDisplay As String = ""
            Dim sAuthor As String = ""

            sDocID = Request.QueryString.Item("ReportDocID")
            Dim ReportDocID As Guid
            Dim gDocTypeID As Guid

            If sDocID Is Nothing OrElse sDocID = "" Then
                sDocTypeID = Me.ddlDocTypeID.SelectedValue
            End If

            If "" & sDocID = "" And "" & sDocTypeID = "" Then
                Exit Sub
            End If

            If sDocTypeID = "" Then
                sDocTypeID = Guid.Empty.ToString
            End If

            If sDocID = "" Then
                sDocID = Guid.Empty.ToString
            End If

            sFieldNameDisplay = ddlField.SelectedValue
            sAuthor = ddlAuthor.SelectedValue

            gDocTypeID = New Guid(sDocTypeID)

            ReportDocID = New Guid(sDocID)

            Dim cmd As New SqlClient.SqlCommand()

            If Request.QueryString("ForDocID") IsNot Nothing Then
                cmd.Parameters.AddWithValue("@DocID", New Guid(Request.QueryString("ForDocID")))
            Else
                cmd.Parameters.AddWithValue("@DocID", ReportDocID)
            End If
            cmd.Parameters.AddWithValue("@DocTypeID", gDocTypeID)
            cmd.Parameters.AddWithValue("@StartDate", CDate(StartDate.Text))
            cmd.Parameters.AddWithValue("@FinishDate", CDate(FinishDate.Text))
            cmd.Parameters.AddWithValue("@FieldNameDisplay", sFieldNameDisplay)
            cmd.Parameters.AddWithValue("@Author", sAuthor)

            cmd.CommandText = SQLGridViewStatusHistory.Value
            Dim dtGridViewStatusHistory As DataTable
            dtGridViewStatusHistory = AppSettings.GetDataTable(cmd, "dtGridViewStatusHistory")

            GridViewStatusHistory.DataSource = dtGridViewStatusHistory
            GridViewStatusHistory.DataBind()

            cmd.CommandText = SQLGridViewFieldHistory.Value
            Dim dtGridViewFieldHistory As DataTable
            dtGridViewFieldHistory = AppSettings.GetDataTable(cmd, "dtGridViewFieldHistory")
            GridViewFieldHistory.DataSource = dtGridViewFieldHistory
            GridViewFieldHistory.DataBind()

            Session("dtGridViewFieldHistory") = dtGridViewFieldHistory
            Session("dtGridViewStatusHistory") = dtGridViewStatusHistory

            If dtGridViewFieldHistory.Rows.Count > 0 Or dtGridViewStatusHistory.Rows.Count > 0 Then
                ChooseUserAndFieldsPanel.Visible = True
                If ddlField.Items.Count = 0 Then
                    Dim Fields = From r In dtGridViewFieldHistory Order By r("FieldNameDisplay") Select r("FieldNameDisplay") Distinct
                    ddlField.Items.Add("")
                    For Each f In Fields
                        ddlField.Items.Add(f.ToString)
                    Next
                End If
                If ddlAuthor.Items.Count = 0 Then
                    Dim Authors = From r In dtGridViewStatusHistory Order By r("Author") Select r("Author") Distinct

                    ddlAuthor.Items.Add("")
                    For Each f In Authors
                        ddlAuthor.Items.Add(f.ToString)
                    Next
                End If
            End If

        Catch ex As Exception
            AddMessage(ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' Загрузка списка типов документов
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub FillddlDocTypeID()
        Try
            If Request.QueryString("ForDocTypeURL") IsNot Nothing Then
                Dim dt = AppSettings.GetDocType(Request.QueryString("ForDocTypeURL"))
                ddlDocTypeID.Items.Add(New ListItem(dt.DocTypeName, dt.DocTypeID.ToString) With {.Selected = True})
                ddlDocTypeID.Enabled = False
            Else
                Dim cmd As New SqlClient.SqlCommand("select cast(ID as varchar(40)) ID, DocType from eqDocTypes order by 2")
                Dim tblDocTypes = AppSettings.GetDataTable(cmd, "eqDocTypes")
                For Each r As DataRow In tblDocTypes.Rows
                    ddlDocTypeID.Items.Add(New ListItem(r(1), r(0)))
                Next r
            End If
        Catch ex As Exception
            AddMessage(ex.Message)
        End Try
    End Sub
#End Region '>>"Построение отчета по изменениям"

End Class