﻿Imports System.Data
Imports System.Data.SqlClient

Partial Public Class _UNIT
    Inherits BaseDoc

#Region "События"

    Public Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If ISN_INVENTORY_CLS.DocID = Guid.Empty Then
            ISN_INVENTORY_CLS.AdditionalListDocWhereString = Crumbs.ISN_INVENTORY.DocID.ToString
        End If

        '<Roman 20131119>
        'В случае отсутствия параметра "INVENTORY", формируется ссылка на раздел описи с параметром "DocType",
        ' чьё значение не является значением ID описи (видимо, берется ID раздела описи). В этом случае возникает
        ' ошибка в получении значения параметра @INVENTORY sql-запроса ф-йй RelIsEmpty в INVENTORYSTRUCTURE.ascx.vb.
        ' Было две возможности решить эту проблему: либо как сделано ниже, либо поменять значение параметра DocType
        ' на ID описи.
        'Весьма вероятно, что такая же ошибка возникнет и при открытии разделов описи, пренадлежащих фонду или архиву.

        'If Crumbs.ISN_INVENTORY.DocID <> Guid.Empty Then
        'ISN_INVENTORY_CLS.OptionalQueryString = "&INVENTORY=" + Crumbs.ISN_INVENTORY.DocID.ToString
        'End If

        '</Roman 20131119>

        '<Roman 20140210>
        'Если раздел описи удален, то не показываем ссылку на него. Проще реализовать здесь, чем в БД, т.к.
        ' там нужно вписывать проверку с рекурсией (обходить дерево вложенных разделов) внутрь вьюхи vUnitPlain, которая используется как хедер,
        ' а посторение списка разделов описи и так является узким местом в плане производительности
        Dim cmdCheck As New SqlCommand("SELECT TOP 1 ISN_INVENTORY_CLS FROM tblUNIT WHERE ID = @UNIT_ID")
        cmdCheck.Parameters.AddWithValue("@UNIT_ID", DocID)

        If Not (AppSettings.ExecCommandScalar(cmdCheck) Is DBNull.Value Or AppSettings.ExecCommandScalar(cmdCheck) Is Nothing) Then
            Dim invStrQuery As String = <sql>
                            declare @isn_inventory bigint;
                            set @isn_inventory = (SELECT ISN_INVENTORY FROM tblUNIT WHERE ID = @UNIT_ID AND Deleted = 0);
                            declare	@isn_fund bigint;
                            set @isn_fund = (SELECT TOP 1 ISN_FUND FROM tblINVENTORY WHERE ISN_INVENTORY = @isn_inventory AND Deleted = 0);
                            declare	@isn_archive bigint;
                            set @isn_archive = (SELECT TOP 1 ISN_ARCHIVE FROM tblFUND WHERE ISN_FUND = @isn_fund AND Deleted = 0);
                            WITH A as (
                                SELECT tIS.ISN_INVENTORY_CLS, tIS.ISN_HIGH_INVENTORY_CLS, ISN_ARCHIVE, ISN_FUND, ISN_INVENTORY
                                FROM tblINVENTORY_STRUCTURE as tIS
                                WHERE (ISN_INVENTORY = @isn_inventory OR ISN_FUND = @isn_fund OR ISN_ARCHIVE = @isn_archive)
                                AND Deleted = 0
                            	
                                UNION ALL
                            	
                                SELECT ttIS.ISN_INVENTORY_CLS, ttIS.ISN_HIGH_INVENTORY_CLS, ttIS.ISN_ARCHIVE, ttIS.ISN_FUND, ttIS.ISN_INVENTORY
                                FROM tblINVENTORY_STRUCTURE as ttIS
                                JOIN A ON A.ISN_INVENTORY_CLS = ttIS.ISN_HIGH_INVENTORY_CLS
                                WHERE ttIS.Deleted = 0
                            )
                            SELECT * 
                            FROM A 
                            WHERE ISN_INVENTORY_CLS = (
                                SELECT TOP 1 ISN_INVENTORY_CLS
                                FROM tblUNIT
                                WHERE ID = @UNIT_ID
                            )
                          </sql>
            Dim cmdInvStrQuery As New SqlCommand(invStrQuery)
            cmdInvStrQuery.Parameters.AddWithValue("@UNIT_ID", DocID)
            If AppSettings.ExecCommandScalar(cmdInvStrQuery) = Nothing Then
                ISN_INVENTORY_CLS.Visible = False
            End If
        End If



        '</Roman 20140210>

        ' ACT FILTER
        For Each r In vREF_ACT_FOR_UNIT.Rows
            DirectCast(r.FindControl("ISN_ACT"), eqDocument).OptionalQueryString = "&UNIT=" & DocID.ToString & "&ACT_OBJ=" & UNIT_KIND.SelectedItem.Value
        Next

        If Not IsPostBack Then
            If DocStateInfo.StateID = Guid.Empty Then
                Dim InventorySQL = <sql>
                    select tblINVENTORY.*, tblINVENTORY_DOC_STORAGE.ISN_STORAGE_MEDIUM
                    from tblINVENTORY
                    left join tblINVENTORY_DOC_STORAGE on tblINVENTORY.ID = tblINVENTORY_DOC_STORAGE.DocID
                    where tblINVENTORY.ID = @ID
                </sql>
                Dim InventoryCommand As New SqlClient.SqlCommand(InventorySQL.Value)
                'InventoryCommand.Parameters.AddWithValue("@ID", Crumbs.ISN_INVENTORY.DocID)

                Dim InventoryDataTable



                'InventoryCommand.Parameters.AddWithValue("@ID", Crumbs.ISN_INVENTORY.DocID)
                'InventoryDataTable = AppSettings.GetDataTable(InventoryCommand)

                InventoryCommand.Parameters.AddWithValue("@ID", Request.Item("INVENTORY"))
                InventoryDataTable = AppSettings.GetDataTable(InventoryCommand)
                _R_BindInventaryToNewUnitCard(Request.Item("INVENTORY"))


                'Dim InventoryDataTable = AppSettings.GetDataTable(InventoryCommand)


                '_R_BindInventaryToNewUnitCard(Request.Item("INVENTORY"))


                If InventoryDataTable.Rows.Count > 0 Then
                    ISN_DOC_TYPE.SelectedValue = InventoryDataTable.Rows(0)("ISN_INVENTORY_TYPE").ToString
                    SECURITY_CHAR.SelectedValue = InventoryDataTable.Rows(0)("SECURITY_CHAR").ToString
                    ISN_SECURLEVEL.SelectedValue = InventoryDataTable.Rows(0)("ISN_SECURLEVEL").ToString
                    MEDIUM_TYPE.SelectedValue = InventoryDataTable.Rows(0)("CARRIER_TYPE").ToString

                    Select Case InventoryDataTable.Rows(0)("ISN_SECURLEVEL").ToString
                        Case "1", "2" ' откр, с
                            ISN_SECURLEVEL.SelectedValue = InventoryDataTable.Rows(0)("ISN_SECURLEVEL")
                    End Select

                    If Not IsDBNull(InventoryDataTable.Rows(0)("ISN_STORAGE_MEDIUM")) Then
                        If InventoryDataTable.Rows.Count = 1 Then
                            ISN_STORAGE_MEDIUM.KeyValue = InventoryDataTable.Rows(0)("ISN_STORAGE_MEDIUM")
                        End If
                    End If

                    Select Case InventoryDataTable.Rows(0)("ISN_INVENTORY_TYPE").ToString
                        Case "1", "2", "3", "4", "9"
                            UNIT_KIND.SelectedValue = 703
                    End Select
                Else
                    Dim InventoryStructureSQL = <sql>
                        select tblINVENTORY.*, tblINVENTORY_DOC_STORAGE.ISN_STORAGE_MEDIUM
                        from tblINVENTORY_STRUCTURE
                        left join tblINVENTORY on tblINVENTORY.ID = tblINVENTORY_STRUCTURE.DocID
                        left join tblINVENTORY_DOC_STORAGE on tblINVENTORY.ID = tblINVENTORY_DOC_STORAGE.DocID
                        where tblINVENTORY_STRUCTURE.ID = @ID and tblINVENTORY_STRUCTURE.Deleted = 0
                </sql>
                    Dim InventoryStructureCommand As New SqlClient.SqlCommand(InventoryStructureSQL.Value)
                    InventoryStructureCommand.Parameters.AddWithValue("@ID", ISN_INVENTORY_CLS.DocID)
                    Dim InventoryStructureDataTable = AppSettings.GetDataTable(InventoryStructureCommand)
                    If InventoryStructureDataTable.Rows.Count > 0 Then

                        ISN_DOC_TYPE.SelectedValue = InventoryStructureDataTable.Rows(0)("ISN_INVENTORY_TYPE").ToString
                        SECURITY_CHAR.SelectedValue = InventoryStructureDataTable.Rows(0)("SECURITY_CHAR").ToString
                        ISN_SECURLEVEL.SelectedValue = InventoryStructureDataTable.Rows(0)("ISN_SECURLEVEL").ToString
                        MEDIUM_TYPE.SelectedValue = InventoryStructureDataTable.Rows(0)("CARRIER_TYPE").ToString

                        Select Case InventoryStructureDataTable.Rows(0)("ISN_SECURLEVEL").ToString
                            Case "1", "2" ' откр, с
                                ISN_SECURLEVEL.SelectedValue = InventoryStructureDataTable.Rows(0)("ISN_SECURLEVEL")
                        End Select

                        If Not IsDBNull(InventoryStructureDataTable.Rows(0)("ISN_STORAGE_MEDIUM")) Then
                            If InventoryStructureDataTable.Rows.Count = 1 Then
                                ISN_STORAGE_MEDIUM.KeyValue = InventoryStructureDataTable.Rows(0)("ISN_STORAGE_MEDIUM")
                            End If
                        End If

                        Select Case InventoryStructureDataTable.Rows(0)("ISN_INVENTORY_TYPE").ToString
                            Case "1", "2", "3", "4", "9"
                                UNIT_KIND.SelectedValue = 703
                        End Select
                    End If
                End If
            End If
        End If

        '<Roman> 26.08.2013 фильтрует список типов документов, оставляя только те, которые указаны в родительской описи
        Dim invId As String = Request.QueryString("INVENTORY")
        If Not invId Is Nothing Then


            Dim cmdString As String = <sql>
                                        SELECT ISN_DOC_TYPE 
                                        FROM tblDOC_TYPE_CL 
                                        WHERE deleted=0
                                        AND ISN_DOC_TYPE NOT IN (
                                            SELECT ISN_DOC_TYPE
                                            FROM tblINVENTORY_DOC_TYPE
                                            WHERE 
                                            ISN_INVENTORY = (
                                                SELECT ISN_INVENTORY
                                                FROM tblINVENTORY
                                                WHERE ID=@ID
                                            )
                                        )
                                      </sql>
            Dim cmd As New SqlCommand(cmdString)
            cmd.Parameters.AddWithValue("@ID", invId)
            For Each row As DataRow In AppSettings.GetDataTable(cmd).Rows
                ISN_DOC_TYPE.Items.Remove(ISN_DOC_TYPE.Items.FindByValue(row.Item(0).ToString))
            Next
        End If
        '<\Roman>

    End Sub

    ''' <summary>
    ''' (Roman)
    ''' Страшный неэстетичный костыль, который исправляет проблемы, возникающие при создании новой Ед.Хр через
    ''' список ед.хр. в разделе описи, который был создан не из описи, а из фонда (не привязан ни к одной описи).
    ''' По смыслу опись должна браться та, из которой вызвали список разделов описей. Ввиду отсутствия информации
    ''' о том, к какой описи привязать ед.хр., карточка новой описи неправильно инициализируется, что приводит к
    ''' невозможности её сохранить. Эта процедура доинициализирует карточку до нужного состояния.
    ''' </summary>
    ''' <param name="InventaryID"></param>
    ''' <remarks></remarks>
    Private Sub _R_BindInventaryToNewUnitCard(ByVal InventaryID)
        If Request.Item("INVENTORYSTRUCTURE") Is Nothing Then 'если создаем юнит не в структуре, то не надо вмешиваться и всё отработает корректно
            Return
        End If
        Dim CheckSQL = <sql>SELECT ISN_INVENTORY
            FROM tblINVENTORY_STRUCTURE
            WHERE ID = @ID and
            ISN_INVENTORY is NULL
            </sql>
        Dim CheckCommand As New SqlClient.SqlCommand(CheckSQL.Value)
        CheckCommand.Parameters.AddWithValue("@ID", Request.Item("INVENTORYSTRUCTURE"))



        If AppSettings.GetDataTable(CheckCommand).Rows.Count = 0 Then 'если нет подраздела с данным индексом, созданного не из описи
            'т.е.  если подраздел создан из описи, то не надо вмешиваться и всё отработает корректно
            Return
        Else
            Dim InventorySQL = <sql>select ISN_SECURLEVEL, NAME 
            from tblSECURLEVEL
            where Deleted = 0 and 	(ISN_SECURLEVEL = (select t.ISN_SECURLEVEL from tblINVENTORY t where t.ID = @ID) 	
            or (select t.ISN_SECURLEVEL from tblINVENTORY t where t.ID = @ID) = 3)
            </sql>
            Dim InventoryCommand As New SqlClient.SqlCommand(InventorySQL.Value)
            InventoryCommand.Parameters.AddWithValue("@ID", InventaryID)
            Dim InventoryDataTable = AppSettings.GetDataTable(InventoryCommand)
            If InventoryDataTable.Rows.Count > 0 Then
                For Each row In InventoryDataTable.Rows
                    ISN_SECURLEVEL.Items.Add(New System.Web.UI.WebControls.ListItem(row(1), row(0)))
                Next
            End If
        End If



    End Sub



    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        HyperLinkDOCUMENTs.Enabled = DocStateInfo.StateID <> Guid.Empty
        HyperLinkDOCUMENTs.NavigateUrl = BasePage.GetListDocURL("DOCUMENT.ascx", False, "UNIT=" & DocID.ToString)

        MultiViewTabs.ActiveViewIndex = CInt(Val(MenuTabs.SelectedValue))
        RefreshMenuTabs()

        UNIT_KIND.Enabled = GetLock(LockID) And DocStateInfo.StateID = Guid.Empty

        If ISN_INVENTORY_CLS.DocID <> Guid.Empty AndAlso Not IsDBNull(ISN_INVENTORY_CLS.DataSetHeaderDataRow("ISN_INVENTORY")) Then
            Crumbs.ISN_INVENTORY.KeyValue = ISN_INVENTORY_CLS.DataSetHeaderDataRow("ISN_INVENTORY")
            Crumbs.ISN_FUND.KeyValue = Crumbs.ISN_INVENTORY.DataSetHeaderDataRow("ISN_FUND")
            If Not IsDBNull(Crumbs.ISN_FUND.DataSetHeaderDataRow("ISN_ARCHIVE")) Then
                Crumbs.ISN_ARCHIVE.KeyValue = Crumbs.ISN_FUND.DataSetHeaderDataRow("ISN_ARCHIVE")
            End If
        End If
        If Crumbs.ISN_INVENTORY.DocID <> Guid.Empty AndAlso Not IsDBNull(Crumbs.ISN_INVENTORY.DataSetHeaderDataRow("ISN_FUND")) Then
            Crumbs.ISN_INVENTORY.KeyValue = Crumbs.ISN_INVENTORY.DataSetHeaderDataRow("ISN_INVENTORY")
            Crumbs.ISN_FUND.KeyValue = Crumbs.ISN_INVENTORY.DataSetHeaderDataRow("ISN_FUND")
            If Not IsDBNull(Crumbs.ISN_FUND.DataSetHeaderDataRow("ISN_ARCHIVE")) Then
                Crumbs.ISN_ARCHIVE.KeyValue = Crumbs.ISN_FUND.DataSetHeaderDataRow("ISN_ARCHIVE")
            End If
        End If

        SECURITY_CHAR.Enabled = GetLock(LockID) AndAlso ISN_SECURLEVEL.SelectedValue <> "" AndAlso ISN_SECURLEVEL.SelectedValue Mod 10000000000 = "1"
        If Not (ISN_SECURLEVEL.SelectedValue <> "" AndAlso ISN_SECURLEVEL.SelectedValue Mod 10000000000 = "1") Then
            SECURITY_CHAR.SelectedValue = ""
        End If
        ISN_SECURITY_REASON.Enabled = GetLock(LockID) AndAlso (ISN_SECURLEVEL.SelectedValue <> "" AndAlso ISN_SECURLEVEL.SelectedValue Mod 10000000000 = "1") And SECURITY_CHAR.SelectedValue = "p"
        If Not ((ISN_SECURLEVEL.SelectedValue <> "" AndAlso ISN_SECURLEVEL.SelectedValue Mod 10000000000 = "1") And SECURITY_CHAR.SelectedValue = "p") Then
            ISN_SECURITY_REASON.SelectedValue = ""
        End If

        vREF_ACT_FOR_UNIT.Enabled = DocStateInfo.StateID <> Guid.Empty
    End Sub

    Private Sub MultiViewTabs_ActiveViewChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles MultiViewTabs.ActiveViewChanged
        Dim ActiveView = MultiViewTabs.Views(MultiViewTabs.ActiveViewIndex)
        Dim Root = ActiveView.Controls.OfType(Of eqSpecification)()

        If Root.Count > 0 AndAlso Root.First.Rows.Count > 0 Then
            Dim ph_ALL = Root.First.Rows(0).FindControl("ph_ALL")
            Dim ph_EH = Root.First.Rows(0).FindControl("ph_EH")
            Dim ph_EU = Root.First.Rows(0).FindControl("ph_EU")

            If ph_ALL IsNot Nothing Then
                Dim kind = UNIT_KIND.SelectedValue

                If kind = "703" Then
                    ph_EH.Visible = True
                    ph_EU.Visible = False
                ElseIf kind = "704" Then
                    ph_EH.Visible = False
                    ph_EU.Visible = True

                    Dim ph_HH = ph_EU.FindControl("ph_HH")
                    Dim ph_VV = ph_EU.FindControl("ph_VV")
                    Dim tb_YEAR = ph_EU.FindControl("tb_YEAR")

                    If ph_HH IsNot Nothing Then
                        ph_HH.Visible = (MEDIUM_TYPE.SelectedValue = "T")
                    End If
                    If ph_VV IsNot Nothing Then
                        ph_VV.Visible = (MEDIUM_TYPE.SelectedValue = "E")
                    End If
                    If tb_YEAR IsNot Nothing Then
                        DirectCast(tb_YEAR, eqTextBox).Text = END_YEAR.Text
                    End If
                End If
            End If
        End If
    End Sub

#End Region

#Region "Методы"

    Private Sub RefreshMenuTabs()
        MenuTabs.Items.Clear()
        MenuTabs.Items.Add(New MenuItem("Общие сведения", "0"))
        MenuTabs.Items.Add(New MenuItem("НСА", "1"))
        MenuTabs.Items.Add(New MenuItem("Физ. состояние", "2"))
        MenuTabs.Items.Add(New MenuItem("Акты", "3"))

        Select Case ISN_DOC_TYPE.SelectedItem.Text
            Case "Фотодокументы"
                MenuTabs.Items.Add(New MenuItem("Фотодокументы", "4"))
            Case "Кинодокументы"
                MenuTabs.Items.Add(New MenuItem("Кинодокументы", "6"))
            Case "Видеодокументы"
                MenuTabs.Items.Add(New MenuItem("Видеодокументы", "7"))
            Case "Фонодокументы"
                MenuTabs.Items.Add(New MenuItem("Фонодокументы", "8"))
            Case "НТД (научно-техническая документация)"
                MenuTabs.Items.Add(New MenuItem("НТД", "9"))
            Case "Микроформы на правах подлинника"
                MenuTabs.Items.Add(New MenuItem("М/ф-подл.", "10"))
            Case Else
                If MEDIUM_TYPE.SelectedValue = "E" Then
                    MenuTabs.Items.Add(New MenuItem("Электронные документы", "5"))
                End If
        End Select
        MenuTabs.Items(Math.Min(MultiViewTabs.ActiveViewIndex, 4)).Selected = True

        lblISN_HIGH_UNIT.Visible = InStr("Фонодокументы Кинодокументы Видеодокументы", ISN_DOC_TYPE.SelectedItem.Text) > 0 Or _
            (ISN_DOC_TYPE.SelectedItem.Text = "Фотодокументы" And MEDIUM_TYPE.SelectedValue = "E")
        valISN_HIGH_UNIT.Visible = InStr("Фонодокументы Кинодокументы Видеодокументы", ISN_DOC_TYPE.SelectedItem.Text) > 0 Or _
            (ISN_DOC_TYPE.SelectedItem.Text = "Фотодокументы" And MEDIUM_TYPE.SelectedValue = "E")
        ISN_HIGH_UNIT.AdditionalListDocWhereString = "UNIT_KIND=" & UNIT_KIND.SelectedItem.Value

        VerifySpecification(tblUNIT_ELECTRONIC, MEDIUM_TYPE.SelectedValue = "E")
        VerifySpecification(tblUNIT_FOTO, ISN_DOC_TYPE.SelectedItem.Text = "Фотодокументы")
        VerifySpecification(tblUNIT_MOVIE, ISN_DOC_TYPE.SelectedItem.Text = "Кинодокументы")
        VerifySpecification(tblUNIT_VIDEO, ISN_DOC_TYPE.SelectedItem.Text = "Видеодокументы")
        VerifySpecification(tblUNIT_PHONO, ISN_DOC_TYPE.SelectedItem.Text = "Фонодокументы")
        VerifySpecification(tblUNIT_NTD, ISN_DOC_TYPE.SelectedItem.Text = "НТД (научно-техническая документация)")
        VerifySpecification(tblUNIT_MICROFORM, ISN_DOC_TYPE.SelectedItem.Text = "Микроформы на правах подлинника")
    End Sub

    Private Sub VerifySpecification(ByVal spec As eqSpecification, ByVal enabled As Boolean)
        If enabled Then
            If spec.Rows.Count < 1 Then
                spec.InsertRow()
            Else
                While spec.Rows.Count > 1
                    spec.DeleteRow(spec.Rows.Count - 1)
                End While
            End If
        Else
            If spec.Rows.Count > 0 Then
                spec.ClearRows()
            End If
        End If
    End Sub

    Public Sub CalcOneUnit()
        If GetLock(LockID) Then
            BasePage.OpenModalDialogWindow(Me, "RecalcUnit.aspx?DocTypeURL=" & DocTypeURL, "One", DataSetHeaderDataRow("ID"))
        End If
    End Sub

    Public Sub AddAct()
        If DocID <> Guid.Empty Then
            BasePage.OpenModalDialogWindow(Me, BasePage.GetNewDocURL("ACT.ascx", True), "UNIT.ascx", DataSetHeaderDataRow("ISN_UNIT"))
        End If
    End Sub

#End Region

#Region "Документ"

    Public Overrides Function NewDoc() As Boolean
        NewDoc = MyBase.NewDoc()
        If NewDoc Then
            If Request.QueryString("INVENTORY") IsNot Nothing Then
                Crumbs.ISN_INVENTORY.DocID = New Guid(Request.QueryString("INVENTORY"))
                DataSetHeaderDataRow("ISN_INVENTORY") = Crumbs.ISN_INVENTORY.KeyValue
                PutValueFromDataSetToHeaderControl(ISN_SECURLEVEL)
            End If
            If Request.QueryString("INVENTORYSTRUCTURE") IsNot Nothing Then
                ISN_INVENTORY_CLS.DocID = New Guid(Request.QueryString("INVENTORYSTRUCTURE"))
                DataSetHeaderDataRow("ISN_INVENTORY") = ISN_INVENTORY_CLS.DataSetHeaderDataRow("ISN_INVENTORY")
                PutValueFromDataSetToHeaderControl(ISN_SECURLEVEL)
            End If
            Dim NewRow = vREF_LANGUAGE_UNIT.InsertRow()
            DirectCast(NewRow.FindControl("ISN_LANGUAGE"), eqDropDownList).Text = "1"
        End If
    End Function

    Public Overrides Function SaveDoc() As Boolean
        If UNIT_KIND.SelectedValue = "704" Then
            Dim InventorySQL = <sql>
                select tblINVENTORY.ID, tblINVENTORY_DOC_TYPE.ISN_DOC_TYPE
                from tblINVENTORY
                left join tblINVENTORY_DOC_TYPE
                on tblINVENTORY_DOC_TYPE.ISN_INVENTORY = tblINVENTORY.ISN_INVENTORY
                where (tblINVENTORY_DOC_TYPE.ISN_DOC_TYPE in (5,6,7,8))
                or (tblINVENTORY_DOC_TYPE.ISN_DOC_TYPE = 4 and (tblINVENTORY.HAS_ELECTRONIC_DOCS = 'Y' or tblINVENTORY.CARRIER_TYPE = 'E'))
                and tblINVENTORY.ID = @ID
            </sql>
            Dim InventoryCommand = New SqlClient.SqlCommand(InventorySQL.Value)
            InventoryCommand.Parameters.AddWithValue("@ID", Crumbs.ISN_INVENTORY.DocID)
            Dim InventoryDataTable = AppSettings.GetDataTable(InventoryCommand)

            If InventoryDataTable.Rows.Count = 0 Then
                AddMessage("Опись не содержит необходимых типов документации. Создание единицы учёта невозможно")
                Return False
            End If
        End If

        If ISN_SECURLEVEL.SelectedValue <> "" AndAlso ISN_SECURLEVEL.SelectedValue Mod 10000000000 = "1" Then
            If SECURITY_CHAR.SelectedValue = "" Then
                SetControlAlertStyle(DocType.GetFieldInfo(DocType.GetHeaderTableName, SECURITY_CHAR.ID), True)
                AddMessage("Поле: Доступ -> Обязательно для заполнения")
                Return False
            End If
        End If

        If ALL_DATE.Text = "" Then
            Dim falt As Boolean
            falt = False
            If (START_YEAR.Text = "") Then
                SetControlAlertStyle(DocType.GetFieldInfo(DocType.GetHeaderTableName, START_YEAR.ID), True)
                AddMessage("Поле: Начальная дата документов -> Не может быть пустым при пустом поле Точные даты")
                falt = True
            End If
            If (END_YEAR.Text = "") Then
                SetControlAlertStyle(DocType.GetFieldInfo(DocType.GetHeaderTableName, END_YEAR.ID), True)
                AddMessage("Поле: Конечная дата документов -> Не может быть пустым при пустом поле Точные даты")
                falt = True
            End If
            If falt Then
                Return False
            End If
        End If
        If (START_YEAR.Text <> "") And (END_YEAR.Text <> "") Then
            If Convert.ToInt32(START_YEAR.Text) > Convert.ToInt32(END_YEAR.Text) Then
                SetControlAlertStyle(DocType.GetFieldInfo(DocType.GetHeaderTableName, START_YEAR.ID), True)
                SetControlAlertStyle(DocType.GetFieldInfo(DocType.GetHeaderTableName, END_YEAR.ID), True)
                AddMessage("Поле: Даты документов -> Начальная дата не позже конечной")
                Return False
            End If
        End If
        If UNIT_NUM_1.Text = "" And UNIT_NUM_TXT.Text = "" Then
            SetControlAlertStyle(DocType.GetFieldInfo(DocType.GetHeaderTableName, UNIT_NUM_1.ID), True)
            SetControlAlertStyle(DocType.GetFieldInfo(DocType.GetHeaderTableName, UNIT_NUM_TXT.ID), True)
            AddMessage("Номер документа -> Заполните поле 'Номер' или 'Номер в старом формате'")
            Return False
        End If

        Dim UniqueSQL = <sql>
                            if (isnull(@UNIT_NUM_TXT, '') = '')
                                select I.ID
                                from tblUNIT U 
                                join tblINVENTORY I on U.ISN_INVENTORY = I.ISN_INVENTORY
                                where (@UNIT_NUM_1 = isnull(U.UNIT_NUM_1,'')
		                            or (U.UNIT_NUM_1 IS NULL AND (@UNIT_NUM_1='' or @UNIT_NUM_1='0')))
                                and @UNIT_NUM_2 = isnull(U.UNIT_NUM_2,'')
                                and (@VOL_NUM = isnull(cast(U.VOL_NUM as varchar(10)), '') 
		                            or (U.VOL_NUM IS NULL AND (@VOL_NUM='' or @VOL_NUM='0'))
									or (U.VOL_NUM = '0' AND @VOL_NUM='')
									or (U.VOL_NUM = '' AND @VOL_NUM='0'))
                                and I.ISN_INVENTORY = @ISN_INVENTORY 
                                and U.ID != @ID and U.Deleted = 0 and I.Deleted = 0
                            else
                                select I.ID
                                from tblUNIT U 
                                join tblINVENTORY I on U.ISN_INVENTORY = I.ISN_INVENTORY
                                where (@UNIT_NUM_TXT = U.UNIT_NUM_TXT 
		                            or (U.UNIT_NUM_TXT IS NULL AND @UNIT_NUM_TXT=''))
                                and (@VOL_NUM = isnull(cast(U.VOL_NUM as varchar(10)), '') 
		                            or (U.VOL_NUM IS NULL AND (@VOL_NUM='' or @VOL_NUM='0'))
									or (U.VOL_NUM = '0' AND @VOL_NUM='')
									or (U.VOL_NUM = '' AND @VOL_NUM='0'))
                                and I.ISN_INVENTORY = @ISN_INVENTORY 
                                and U.ID != @ID and U.Deleted = 0 and I.Deleted = 0
                        </sql>
        Dim UniqueCommand = New SqlCommand(UniqueSQL.Value)
        UniqueCommand.Parameters.AddWithValue("@UNIT_NUM_1", UNIT_NUM_1.Text)
        UniqueCommand.Parameters.AddWithValue("@UNIT_NUM_2", UNIT_NUM_2.Text)
        UniqueCommand.Parameters.AddWithValue("@UNIT_NUM_TXT", UNIT_NUM_TXT.Text)
        UniqueCommand.Parameters.AddWithValue("@VOL_NUM", VOL_NUM.Text)

        If Crumbs.ISN_INVENTORY.KeyValue Is Nothing Then 'Roman: такая ситуация возникает при добавлении ед.хр. из списка раздела описи, созданого через карточку фонда
            UniqueCommand.Parameters.AddWithValue("@ISN_INVENTORY", "")
            'Crumbs.ISN_INVENTORY.KeyValue = "00000000-0100-0000-0000-000000000005"
        Else
            UniqueCommand.Parameters.AddWithValue("@ISN_INVENTORY", Crumbs.ISN_INVENTORY.KeyValue)
        End If

        UniqueCommand.Parameters.AddWithValue("@ID", DocID)

        Try
            Dim UniqueDataTable = AppSettings.GetDataTable(UniqueCommand)
            If UniqueDataTable.Rows.Count > 0 Then
                AddMessage("Единица хранения (учета) с таким номером и томом уже существует")
                Return False
            End If
        Catch ex As SqlException
            AddMessage("Ошибка базы данных! Попробуйте обновить базу данных.")
            Return False
        Catch ex As Exception
            Throw ex
        End Try

        Return MyBase.SaveDoc()
    End Function

#End Region

#Region "Контролы"

    Public Overrides Sub PutValueToControl(ByVal Value As Object, ByVal TargetControl As System.Web.UI.Control, Optional ByVal FieldFormat As String = "")
        If TypeOf TargetControl Is CheckBox Then
            DirectCast(TargetControl, CheckBox).Checked = (Value Is Nothing OrElse Value.ToString.Trim.ToUpper = "Y")
        Else
            MyBase.PutValueToControl(Value, TargetControl, FieldFormat)
        End If
    End Sub

    Public Overrides Function GetValueFromControl(ByVal SourceControl As System.Web.UI.Control) As Object
        If TypeOf SourceControl Is CheckBox Then
            Return IIf(DirectCast(SourceControl, CheckBox).Checked, "Y", "N")
        Else
            Return MyBase.GetValueFromControl(SourceControl)
        End If
    End Function

#End Region

#Region "Файлы"

    Private Sub RefFileUploadFile_Uploaded(ByVal sender As Object, ByVal e As RefFileArgs) Handles RefFileUploadFile.Uploaded
        Dim row = vREF_FILE_UNIT.InsertRow(e.ID)
        DataSetSpecificationDataRow(vREF_FILE_UNIT.ID, e.ID)("CreationDateTime") = e.TransferTime
        DirectCast(row.FindControl("NAME"), TextBox).Text = System.IO.Path.GetFileName(e.FileName)
    End Sub

    Private Sub RefFileUploadFile_Uploading(ByVal sender As Object, ByVal e As RefFileArgs) Handles RefFileUploadFile.Uploading
        e.FileName = GetFileSystemAttachFileName(e.ID, e.TransferTime, 703, DBNull.Value, e.FileName)
    End Sub

    Public Sub RefFile_Requesting(ByVal sender As Object, ByVal e As RefFileArgs)
        e.TransferTime = DataSetSpecificationDataRow(vREF_FILE_UNIT.ID, e.ID)("CreationDateTime")
        e.FileName = GetFileSystemAttachFileName(e.ID, e.TransferTime, 703, DBNull.Value)
    End Sub

    Private Sub RefFileUploadFileS_Uploaded(ByVal sender As Object, ByVal e As RefFileArgs) Handles RefFileUploadFileS.Uploaded
        Dim row = vREF_FILE_UNIT_S.InsertRow(e.ID)
        DataSetSpecificationDataRow(vREF_FILE_UNIT_S.ID, e.ID)("CreationDateTime") = e.TransferTime
        DirectCast(row.FindControl("NAME"), TextBox).Text = System.IO.Path.GetFileName(e.FileName)
    End Sub

    Private Sub RefFileUploadFileS_Uploading(ByVal sender As Object, ByVal e As RefFileArgs) Handles RefFileUploadFileS.Uploading
        e.FileName = GetFileSystemAttachFileName(e.ID, e.TransferTime, 703, 100, e.FileName)
    End Sub

    Public Sub RefFileS_Requesting(ByVal sender As Object, ByVal e As RefFileArgs)
        e.TransferTime = DataSetSpecificationDataRow(vREF_FILE_UNIT_S.ID, e.ID)("CreationDateTime")
        e.FileName = GetFileSystemAttachFileName(e.ID, e.TransferTime, 703, 100)
    End Sub

#End Region

End Class