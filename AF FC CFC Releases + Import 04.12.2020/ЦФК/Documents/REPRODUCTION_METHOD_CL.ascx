﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="REPRODUCTION_METHOD_CL.ascx.vb"
    Inherits="WebApplication.REPRODUCTION_METHOD_CL" %>
<table style="width: 700px;">
    <colgroup>
        <col style="width: 150px;" />
        <col />
    </colgroup>
    <tr>
        <td>
            Наименование
        </td>
        <td>
            <eq:eqTextBox ID="NAME" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            Код:
        </td>
        <td>
            <eq:eqTextBox ID="CODE" runat="server" />
        </td>
    </tr>
    <tr>
        <td valign="top">
            Примечание:
        </td>
        <td>
            <eq:eqTextBox ID="NOTE" runat="server" TextMode="MultiLine" Rows="4" />
        </td>
    </tr>
</table>
