﻿'------------------------------------------------------------------------------
' <auto-generated>
'     Этот код создан программой.
'     Исполняемая версия:2.0.50727.5485
'
'     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
'     повторной генерации кода.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On



Partial Public Class GR_DOC_IMAGE

    '''<summary>
    '''NUM control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents NUM As Global.Equipage.WebUI.eqTextBox

    '''<summary>
    '''DOC_DATE control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents DOC_DATE As Global.Equipage.WebUI.eqTextBox

    '''<summary>
    '''ID_ARCHIVE control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ID_ARCHIVE As Global.WebApplication.WebUI_G.eqDocument

    '''<summary>
    '''NAME_SHORT control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents NAME_SHORT As Global.Equipage.WebUI.eqTextBox

    '''<summary>
    '''FUND_NUM_1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents FUND_NUM_1 As Global.Equipage.WebUI.eqTextBox

    '''<summary>
    '''FUND_NUM_2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents FUND_NUM_2 As Global.Equipage.WebUI.eqTextBox

    '''<summary>
    '''FUND_NUM_3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents FUND_NUM_3 As Global.Equipage.WebUI.eqTextBox

    '''<summary>
    '''INVENTORY_NUM_1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents INVENTORY_NUM_1 As Global.Equipage.WebUI.eqTextBox

    '''<summary>
    '''INVENTORY_NUM_2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents INVENTORY_NUM_2 As Global.Equipage.WebUI.eqTextBox

    '''<summary>
    '''INVENTORY_NUM_ALTERNATIVE control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents INVENTORY_NUM_ALTERNATIVE As Global.Equipage.WebUI.eqTextBox

    '''<summary>
    '''INVENTORY_NUM_3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents INVENTORY_NUM_3 As Global.Equipage.WebUI.eqTextBox

    '''<summary>
    '''UNIT_NUM_1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UNIT_NUM_1 As Global.Equipage.WebUI.eqTextBox

    '''<summary>
    '''UNIT_NUM_2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UNIT_NUM_2 As Global.Equipage.WebUI.eqTextBox

    '''<summary>
    '''UNIT_REG_NUM_1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UNIT_REG_NUM_1 As Global.Equipage.WebUI.eqTextBox

    '''<summary>
    '''UNIT_REG_NUM_2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UNIT_REG_NUM_2 As Global.Equipage.WebUI.eqTextBox

    '''<summary>
    '''UNIT_VOL_NUM control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UNIT_VOL_NUM As Global.Equipage.WebUI.eqTextBox

    '''<summary>
    '''SHEETS control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents SHEETS As Global.Equipage.WebUI.eqTextBox

    '''<summary>
    '''GR_vDOC_IMAGE control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents GR_vDOC_IMAGE As Global.Equipage.WebUI.eqSpecification

    '''<summary>
    '''RefFileUploadFileS control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RefFileUploadFileS As Global.WebApplication.RefFileUpload
End Class
