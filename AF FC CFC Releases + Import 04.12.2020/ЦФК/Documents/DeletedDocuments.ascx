﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="DeletedDocuments.ascx.vb"
    Inherits="WebApplication.DeletedDocuments" %>
<div class="LogicBlockCaption">
    <%--Удаленный документ--%>
    <asp:Localize ID="LocalizeDeletedDocument" runat="server" Text="<%$ Resources:eqResources, CAP_DeletedDocument %>" />
</div>
<table width="100%" class="LogicBlockTable">
    <colgroup>
        <col style="width: 120px;" />
        <col />
    </colgroup>
    <tr>
        <td>
            <%--Дата удаления--%>
            <asp:Localize ID="LocalizeDeletedDate" runat="server" Text="<%$ Resources:eqResources, CAP_DeletedDate %>" />
        </td>
        <td>
            <eq:eqTextBox ID="CreationDateTime" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            <%--Кто удалил--%>
            <asp:Localize ID="LocalizeDeletedInitiator" runat="server" Text="<%$ Resources:eqResources, CAP_DeletedInitiator %>" />
        </td>
        <td>
            <eq:eqTextBox ID="OwnerID" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            <%--Документ--%>
            <asp:Localize ID="LocalizeDocuemnt" runat="server" Text="<%$ Resources:eqResources, CAP_Document %>" />
        </td>
        <td>
            <asp:Literal ID="DocumentForDisplay" runat="server"></asp:Literal>
        </td>
    </tr>
</table>

