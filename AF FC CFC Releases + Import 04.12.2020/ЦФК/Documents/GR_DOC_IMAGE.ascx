﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="GR_DOC_IMAGE.ascx.vb"
    Inherits="WebApplication.GR_DOC_IMAGE" %>
<table style="width: 700px;">
    <colgroup>
        <col width="150px" />
        <col />
    </colgroup>
    <tr>
        <%--<td>
      <img id="ImageGet" src="ImageHandler.ashx" alt="Изображение 1" />

</td>--%>
        <td>
            Номер документа
        </td>
        <td>
            <eq:eqTextBox ID="NUM" runat="server" Height="20px" Width="50px" Enabled="False"></eq:eqTextBox>
        </td>
        <td>
            Дата включения документа в Государственный реестр
        </td>
        <td>
            <eq:eqTextBox ID="DOC_DATE" runat="server" Height="20px" Width="70px" Enabled="False"></eq:eqTextBox>
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            Архив
        </td>
        <td>
            <eq3:eqDocument ID="ID_ARCHIVE" runat="server" DocTypeURL="GR_ARCHIVE.ascx" ListDocMode="NewWindowAndSelect"
                LoadDocMode="ListDocAndSelect" ShowingFields="NAME" ShowingFieldsFormat="{0}"
                KeyName="ID" Width="500px" OnRequesting="Archive_Requesting" />
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            Архивный шифр*: Архив:
        </td>
        <td>
            <eq:eqTextBox ID="NAME_SHORT" runat="server" Width="40px" Height="20px" Enabled="False" />
        </td>
         <td>
            , фонд №
        </td>
        <td>
            <eq:eqTextBox ID="FUND_NUM_1" runat="server" Width="8px" Height="20px" Enabled="False" />
            <eq:eqTextBox ID="FUND_NUM_2" runat="server" Width="40px" Height="20px" Enabled="False" />
            <eq:eqTextBox ID="FUND_NUM_3" runat="server" Width="8px" Height="20px" Enabled="False" />
        </td>
        <td>
            , оп.№
        </td>
        <td>
            <eq:eqTextBox ID="INVENTORY_NUM_1" runat="server" Width="24px" Height="20px" Enabled="False" />
            <eq:eqTextBox ID="INVENTORY_NUM_2" runat="server" Width="16px" Height="20px" Enabled="False" />
        </td>
        <td>
            , оп.№ (альтернативный)
        </td>
        <td>
            <eq:eqTextBox ID="INVENTORY_NUM_ALTERNATIVE" runat="server" Width="200px" Height="20px"
                Enabled="False" />
        </td>
        <td>
            том
        </td>
        <td>
            <eq:eqTextBox ID="INVENTORY_NUM_3" runat="server" Width="24px" Height="20px" Enabled="False" />
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            ед.хр.№
        </td>
        <td>
            <eq:eqTextBox ID="UNIT_NUM_1" runat="server" Width="64px" Height="20px" Enabled="False" />
            <eq:eqTextBox ID="UNIT_NUM_2" runat="server" Width="16px" Height="20px" Enabled="False" />
        </td>
        <td>
            / ед.уч.№
        </td>
        <td>
            <eq:eqTextBox ID="UNIT_REG_NUM_1" runat="server" Width="64px" Height="20px" Enabled="False" />
            <eq:eqTextBox ID="UNIT_REG_NUM_2" runat="server" Width="16px" Height="20px" Enabled="False" />
        </td>
        <td>
            том
        </td>
        <td>
            <eq:eqTextBox ID="UNIT_VOL_NUM" runat="server" Width="24px" Height="20px" Enabled="False" />
        </td>
        <td>
            , лл.
        </td>
        <td>
            <eq:eqTextBox ID="SHEETS" runat="server" Width="200px" Height="20px" Enabled="False" />
        </td>
    </tr>
</table>
<table style="width: 700px;">
    <tr>
        <td>
            <fieldset>
                <legend>Файлы электронного образа</legend>
                <eq:eqSpecification ID="GR_vDOC_IMAGE" runat="server" Width="100%" AllowInsert="false">
                    <eq:eqSpecificationColumn HeaderText="RowID" Visible="false">
                        <ItemTemplate>
                            <asp:TextBox ID="RowID" runat="server" Width="40px" />
                        </ItemTemplate>
                    </eq:eqSpecificationColumn>
                    <eq:eqSpecificationColumn HeaderText="Номер образа">
                        <ItemTemplate>
                            <asp:TextBox ID="NUMBER" runat="server" Width="50px" />
                        </ItemTemplate>
                    </eq:eqSpecificationColumn>
                    <eq:eqSpecificationColumn HeaderText="Номер образа" Visible="false">
                        <ItemTemplate>
                            <asp:TextBox ID="CHAR_NUMBER" runat="server" Width="40px" />
                        </ItemTemplate>
                    </eq:eqSpecificationColumn>
                    <eq:eqSpecificationColumn HeaderText="Описание">
                        <ItemTemplate>
                            <asp:TextBox ID="NAME" runat="server" Width="500px" />
                        </ItemTemplate>
                    </eq:eqSpecificationColumn>
                    <eq:eqSpecificationColumn HeaderText="Файл">
                        <ItemTemplate>
                            <eq2:RefFile ID="ID" runat="server" Mode="FileSystem" OnRequesting="RefFileS_Requesting" />
                        </ItemTemplate>
                    </eq:eqSpecificationColumn>
                    <eq:eqSpecificationColumn HeaderText="Доступен для интернет-поиска">
                        <ItemTemplate>
                            <asp:CheckBox ID="PUBLIC" runat="server" Width="50px" Checked="true" />
                        </ItemTemplate>
                    </eq:eqSpecificationColumn>
                    <eq:eqSpecificationColumn HeaderText="Изображение">
                        <ItemTemplate>
                            <asp:Image ID="IMF" runat="server" ImageUrl="ImageHandler.ashx" />
                        </ItemTemplate>
                    </eq:eqSpecificationColumn>
                </eq:eqSpecification>
                <table>
                    <tr>
                        <td>
                            Добавить файл:
                        </td>
                        <td>
                            <eq2:RefFileUpload ID="RefFileUploadFileS" runat="server" Mode="FileSystem" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </td>
    </tr>
</table>
