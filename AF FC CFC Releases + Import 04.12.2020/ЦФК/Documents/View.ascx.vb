﻿Public Partial Class View
    Inherits BaseDoc

    Public Overrides Function NewDoc() As Boolean
        If BasePage.IsModalDialog Then
            Select Case BasePage.ModalDialogArgument.Argument
                Case "SetupView"
                    Dim ViewDataSet As DataSet = BasePage.ModalDialogArgument.ArgumentObject
                    If ImportDataSet(ViewDataSet) Then
                        BasePage.ModalDialogArgument.Argument = "SelectView"
                        eqView.VeriflyViewDataSet(DataSet, AppSettings, True)
                        Return PutValuesFromDataSetToControls()
                    Else
                        Return False
                    End If
                Case "SelectView"
                    Dim ViewDataSet As DataSet = BasePage.ModalDialogArgument.ArgumentObject
                    If MyBase.NewDoc Then
                        'DataSetHeaderDataRow("ViewName") = "Настройка от " & Now.ToString
                        DataSetHeaderDataRow("ViewName") = String.Format(GetGlobalResourceObject("eqResources", "CAP_ViewFrom"), Now.ToString)
                        DataSetHeaderDataRow("Access") = "Личное"
                        DataSetHeaderDataRow("IsDefault") = False
                        DataSetHeaderDataRow("ParentID") = ViewDataSet.Tables(0).Rows(0).Item("ParentID")
                        eqView.VeriflyViewDataSet(DataSet, AppSettings, True)
                        Return PutValuesFromDataSetToControls()
                    Else
                        Return False
                    End If
                Case "SetupViewSimple" 'Added by Anatoly Melkov on 2009-08-18
                    Dim ViewDataSet As DataSet = BasePage.ModalDialogArgument.ArgumentObject
                    If ImportDataSet(ViewDataSet) Then
                        BasePage.ModalDialogArgument.Argument = "SetupViewSimple"
                        eqView.VeriflyViewDataSet(DataSet, AppSettings, True)
                        Return PutValuesFromDataSetToControls()
                    Else
                        Return False
                    End If
            End Select
        End If
        Return MyBase.NewDoc()
    End Function

    Public Overrides Sub OpenDocCopy()
        MyBase.OpenDocCopy()
        'DataSetHeaderDataRow("ViewName") = "Настройка от " & Now.ToString
        DataSetHeaderDataRow("ViewName") = String.Format(GetGlobalResourceObject("eqResources", "CAP_ViewFrom"), Now.ToString)
        DataSetHeaderDataRow("Access") = "Личное"
        DataSetHeaderDataRow("IsDefault") = False
        eqView.VeriflyViewDataSet(DataSet, AppSettings, True)
        PutValuesFromDataSetToControls()
    End Sub

    Public Overrides Function LoadDoc(ByVal DocID As System.Guid) As Boolean
        If MyBase.LoadDoc(DocID) Then
            eqView.VeriflyViewDataSet(DataSet, AppSettings)
            Return PutValuesFromDataSetToControls()
        End If
    End Function

    Public Overrides Sub VeriflySecurities()
        'MyBase.VeriflySecurities()
    End Sub

    Public Overrides Sub LoadDocumentMenu()
        MyBase.LoadDocumentMenu()
        BaseContainer.RemoveMenuItem("SelectDoc")
        If Not AppSettings.UserSecurities.HasRole("Администраторы") AndAlso AppSettings.UserInfo.UserLogin <> "sa" AndAlso Not IsDBNull(DataSetHeaderDataRow("OwnerID")) AndAlso DataSetHeaderDataRow("OwnerID") <> AppSettings.UserInfo.UserID Then
            BaseContainer.RemoveMenuItem("SaveDoc")
            BaseContainer.RemoveMenuItem("DeleteDoc")
        End If
        If BasePage.IsModalDialog Then
            Select Case BasePage.ModalDialogArgument.Argument
                Case "SetupView", "SelectView"
                    BaseContainer.AddMenuItem(GetGlobalResourceObject("eqResources", "MNU_ApplyView"), "ApplyView", "Images/Close.gif", AddAtIndex:=0)
                Case "SetupViewSimple"
                    BaseContainer.AddMenuItem(GetGlobalResourceObject("eqResources", "MNU_ApplyView"), "ApplyView", "Images/Close.gif", AddAtIndex:=0)
                    BaseContainer.RemoveMenuItem("SaveDoc")
                    BaseContainer.RemoveMenuItem("DeleteDoc")
                    BaseContainer.RemoveMenuItem("SelectDoc")
                    BaseContainer.RemoveMenuItem("NewDoc")
                    BaseContainer.RemoveMenuItem("ListDoc")
                    BaseContainer.RemoveMenuItem("LoadDoc")
                    BaseContainer.RemoveMenuItem("OpenDocCopy")
            End Select
        End If
    End Sub

    'Public Overrides Function PutValuesFromDataSetToControls() As Boolean
    '    PutValuesFromDataSetToControls = MyBase.PutValuesFromDataSetToControls()
    '    If PutValuesFromDataSetToControls Then
    '        VeriflyFields()
    '    End If
    'End Function

#Region "Events"

    Public Sub HeaderEnabled_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim HeaderEnabled = DirectCast(sender, CheckBox)
        For Each R In eqViewFields.Rows
            Dim Enabled = DirectCast(R.FindControl("Enabled"), CheckBox)
            Enabled.Checked = HeaderEnabled.Checked
        Next
    End Sub

    Private Sub ParentID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ParentID.SelectedIndexChanged
        If ReadValuesFromControlsToDataSet() Then
            eqView.VeriflyViewDataSet(DataSet, AppSettings, True)
            PutValuesFromDataSetToControls()
        End If
    End Sub

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If BasePage.IsModalDialog Then
            If BasePage.ModalDialogArgument.Argument = "SetupViewSimple" Then

                eqViewFields.GetColumn("AggregateColumn").Visible = False
                eqViewFields.GetColumn("WidthColumn").Visible = False
                eqViewFields.GetColumn("VisibleColumn").Visible = True

                PlaceHolderView.Visible = False
                ColPanSetup.Visible = False

                diveqViewFunctions.Visible = False
                eqViewFunctions.Visible = False
                divSort.Visible = False
                eqViewSorting.Visible = False
                divGroup.Visible = False
                eqViewGroupping.Visible = False
                divColorSelect.Visible = False
                eqViewPainting.Visible = False

            End If
        End If
    End Sub

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender

        VeriflyFields()

        If BasePage.IsModalDialog Then
            ParentID.Enabled = False
        End If

        If Not AppSettings.UserSecurities.HasRole("Администраторы") AndAlso AppSettings.UserInfo.UserLogin <> "sa" Then
            Access.Enabled = False
            trOwnerID.Visible = False
        End If

        eqViewFields.Enabled = ParentID.SelectedValue <> ""
        eqViewSorting.Enabled = ParentID.SelectedValue <> ""
        eqViewGroupping.Enabled = ParentID.SelectedValue <> ""
        eqViewFunctions.Enabled = ParentID.SelectedValue <> ""
        eqViewPainting.Enabled = ParentID.SelectedValue <> ""

        ' eqViewFields
        Dim HeaderEnabled = DirectCast(eqViewFields.HeaderRow.FindControl("HeaderEnabled"), CheckBox)
        HeaderEnabled.Checked = eqViewFields.Rows.Count > 0
        For Each R In eqViewFields.Rows
            Dim Enabled = DirectCast(R.FindControl("Enabled"), CheckBox)
            If Not Enabled.Checked Then
                HeaderEnabled.Checked = False
                Exit For
            End If
        Next

        ' TreeSupport
        If ParentID.SelectedValue <> "" Then
            Dim ParentDocType = AppSettings.GetDocType(New Guid(ParentID.SelectedValue))
            divGroup.Visible = Not ParentDocType.HasTreeSupport
            eqViewGroupping.Visible = Not ParentDocType.HasTreeSupport
        End If

    End Sub

    'Public Sub OperatorDropDownList_OnTextChanged(ByVal sender As Object, ByVal e As EventArgs)
    '    Dim OperatorDropDownList = DirectCast(sender, DropDownList)
    '    Dim [Operator] = DirectCast(OperatorDropDownList.NamingContainer.FindControl("Operator"), HiddenField)
    '    [Operator].Value = OperatorDropDownList.SelectedValue.ToString
    'End Sub

    'Public Sub FilterTextBox_OnTextChanged(ByVal sender As Object, ByVal e As EventArgs)
    '    Dim FilterTextBox = DirectCast(sender, eqTextBox)
    '    Dim Filter = DirectCast(FilterTextBox.NamingContainer.FindControl("Filter"), HiddenField)
    '    Filter.Value = FilterTextBox.Text
    'End Sub

    'Public Sub FilterDropDownList_OnSelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
    '    Dim FilterDropDownList = DirectCast(sender, DropDownList)
    '    Dim Filter = DirectCast(FilterDropDownList.NamingContainer.FindControl("Filter"), HiddenField)
    '    Filter.Value = FilterDropDownList.SelectedValue.ToString
    'End Sub

    'Public Sub FilterCheckBox_OnCheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
    '    Dim FilterCheckBox = DirectCast(sender, CheckBox)
    '    Dim Filter = DirectCast(FilterCheckBox.NamingContainer.FindControl("Filter"), HiddenField)
    '    Filter.Value = FilterCheckBox.Checked.ToString
    'End Sub

    Public Sub Function_OnFunctionChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim ldFunction = DirectCast(sender, ListDocFunction)
        Dim Parameter = DirectCast(ldFunction.NamingContainer.FindControl("Parameter"), HiddenField)
        Dim Enabled = DirectCast(ldFunction.NamingContainer.FindControl("Enabled"), CheckBox)
        Parameter.Value = ldFunction.ParameterValue
        Enabled.Checked = ldFunction.FunctionEnabled
    End Sub

    Public Sub Filter_OnFilterChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim Filter = DirectCast(sender, ListDocFilter)
        Dim Operator1 = DirectCast(Filter.NamingContainer.FindControl("Operator1"), HiddenField)
        Dim Filter1 = DirectCast(Filter.NamingContainer.FindControl("Filter1"), HiddenField)
        Dim Logic12 = DirectCast(Filter.NamingContainer.FindControl("Logic12"), HiddenField)
        Dim Operator2 = DirectCast(Filter.NamingContainer.FindControl("Operator2"), HiddenField)
        Dim Filter2 = DirectCast(Filter.NamingContainer.FindControl("Filter2"), HiddenField)
        Operator1.Value = Filter.Operator1
        Filter1.Value = Filter.Filter1
        Logic12.Value = Filter.Logic12
        Operator2.Value = Filter.Operator2
        Filter2.Value = Filter.Filter2
    End Sub

    Public Sub FunctionsID_OnSelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        VeriflyFields()
    End Sub

#End Region

#Region "Metods"

    Public Sub ApplyView()
        If BeforeSaveDoc() Then
            BasePage.CloseModalDialogWindow(BasePage.ModalDialogArgument.Argument, DataSet)
        End If
    End Sub

    Public Sub VeriflyFields()
        ' eqViewConditions
        If ParentID.SelectedValue <> String.Empty Then
            Dim SelectedDocType As eqDocType = AppSettings.GetDocType(New Guid(ParentID.SelectedValue))
            Dim firstRow As Boolean = True
            For Each R In eqViewConditions.Rows
                Dim FieldIDDD = DirectCast(R.FindControl("FieldID"), DropDownList)

                Dim Filter = DirectCast(R.FindControl("Filter"), ListDocFilter)
                Dim Logic = DirectCast(R.FindControl("Logic"), DropDownList)

                Logic.Visible = Not firstRow
                firstRow = False

                If FieldIDDD.SelectedValue IsNot Nothing AndAlso FieldIDDD.SelectedValue <> "" Then
                    Dim FieldID As New Guid(FieldIDDD.SelectedValue)

                    Dim Operator1 = DirectCast(R.FindControl("Operator1"), HiddenField)
                    Dim Filter1 = DirectCast(R.FindControl("Filter1"), HiddenField)
                    Dim Logic12 = DirectCast(R.FindControl("Logic12"), HiddenField)
                    Dim Operator2 = DirectCast(R.FindControl("Operator2"), HiddenField)
                    Dim Filter2 = DirectCast(R.FindControl("Filter2"), HiddenField)

                    Dim FieldInfo = SelectedDocType.GetFieldInfo(FieldID)
                    Dim ListDocFieldInfo = SelectedDocType.GetListDocFieldInfo(FieldID)
                    Dim ShemaDataRow = SelectedDocType.GetShemaDataRow(FieldInfo("TableName"), FieldInfo("FieldName"))

                    ' Filter
                    Filter.FieldID = FieldID
                    Filter.TypeName = ShemaDataRow("TypeName")
                    Filter.Operator1 = Operator1.Value
                    Filter.Filter1 = Filter1.Value
                    Filter.Logic12 = Logic12.Value
                    Filter.Operator2 = Operator2.Value
                    Filter.Filter2 = Filter2.Value

                    If ListDocFieldInfo("ValuesScript").ToString.Trim <> String.Empty Then
                        Filter.ValuesScript = ListDocFieldInfo("ValuesScript").ToString
                    End If

                    ' Visible
                    Filter.Visible = True

                Else

                    ' Visible
                    Filter.Visible = False

                End If
            Next
        End If
        ' eqViewPainting
        If ParentID.SelectedValue <> String.Empty Then
            Dim SelectedDocType As eqDocType = AppSettings.GetDocType(New Guid(ParentID.SelectedValue))
            For Each R In eqViewPainting.Rows
                Dim FieldIDDD = DirectCast(R.FindControl("FieldID"), DropDownList)

                Dim Filter = DirectCast(R.FindControl("Filter"), ListDocFilter)
                Dim Color = DirectCast(R.FindControl("Color"), eqColorPicker)

                If FieldIDDD.SelectedValue IsNot Nothing AndAlso FieldIDDD.SelectedValue <> "" Then
                    Dim FieldID As New Guid(FieldIDDD.SelectedValue)

                    Dim Operator1 = DirectCast(R.FindControl("Operator1"), HiddenField)
                    Dim Filter1 = DirectCast(R.FindControl("Filter1"), HiddenField)
                    Dim Logic12 = DirectCast(R.FindControl("Logic12"), HiddenField)
                    Dim Operator2 = DirectCast(R.FindControl("Operator2"), HiddenField)
                    Dim Filter2 = DirectCast(R.FindControl("Filter2"), HiddenField)

                    Dim FieldInfo = SelectedDocType.GetFieldInfo(FieldID)
                    Dim ListDocFieldInfo = SelectedDocType.GetListDocFieldInfo(FieldID)
                    Dim ShemaDataRow = SelectedDocType.GetShemaDataRow(FieldInfo("TableName"), FieldInfo("FieldName"))

                    ' Filter
                    Filter.FieldID = FieldID
                    Filter.TypeName = ShemaDataRow("TypeName")
                    Filter.Operator1 = Operator1.Value
                    Filter.Filter1 = Filter1.Value
                    Filter.Logic12 = Logic12.Value
                    Filter.Operator2 = Operator2.Value
                    Filter.Filter2 = Filter2.Value

                    If ListDocFieldInfo("ValuesScript").ToString.Trim <> String.Empty Then
                        Filter.ValuesScript = ListDocFieldInfo("ValuesScript").ToString
                    End If

                    ' Visible
                    Filter.Visible = True
                    Color.Visible = True

                Else

                    ' Visible
                    Filter.Visible = False
                    Color.Visible = False

                End If
            Next
        End If
        ' eqViewFields
        If ParentID.SelectedValue <> String.Empty Then
            Dim SelectedDocType As eqDocType = AppSettings.GetDocType(New Guid(ParentID.SelectedValue))
            For Each R In eqViewFields.Rows
                Dim FieldID As New Guid(DirectCast(R.FindControl("FieldID"), HiddenField).Value)

                Dim Header = DirectCast(R.FindControl("Header"), eqTextBox)
                Dim Operator1 = DirectCast(R.FindControl("Operator1"), HiddenField)
                Dim Filter1 = DirectCast(R.FindControl("Filter1"), HiddenField)
                Dim Logic12 = DirectCast(R.FindControl("Logic12"), HiddenField)
                Dim Operator2 = DirectCast(R.FindControl("Operator2"), HiddenField)
                Dim Filter2 = DirectCast(R.FindControl("Filter2"), HiddenField)
                Dim Filter = DirectCast(R.FindControl("Filter"), ListDocFilter)

                Dim FieldInfo = SelectedDocType.GetFieldInfo(FieldID)
                Dim ListDocFieldInfo = SelectedDocType.GetListDocFieldInfo(FieldID)
                Dim ShemaDataRow = SelectedDocType.GetShemaDataRow(FieldInfo("TableName"), FieldInfo("FieldName"))

                ' Filter
                Filter.FieldID = FieldID
                Filter.TypeName = ShemaDataRow("TypeName")
                Filter.Operator1 = Operator1.Value
                Filter.Filter1 = Filter1.Value
                Filter.Logic12 = Logic12.Value
                Filter.Operator2 = Operator2.Value
                Filter.Filter2 = Filter2.Value

                If ListDocFieldInfo("ValuesScript").ToString.Trim <> String.Empty Then
                    Filter.ValuesScript = ListDocFieldInfo("ValuesScript").ToString
                End If

                Header.ToolTip = ListDocFieldInfo("FieldDesc")

            Next
        End If
        ' eqFunctions
        Dim hasFunctions As Boolean = False
        If ParentID.SelectedValue <> String.Empty Then
            Dim SelectedDocType As eqDocType = AppSettings.GetDocType(New Guid(ParentID.SelectedValue))
            hasFunctions = SelectedDocType.GetFunctions.Count > 0
            For Each R In eqViewFunctions.Rows
                Dim FunctionsIDDropDownList = DirectCast(R.FindControl("FunctionsID"), DropDownList)
                Dim FunctionControl = DirectCast(R.FindControl("Function"), ListDocFunction)
                Dim Parameter = DirectCast(R.FindControl("Parameter"), HiddenField)
                Dim Enabled = DirectCast(R.FindControl("Enabled"), CheckBox)
                If FunctionsIDDropDownList.SelectedValue <> String.Empty Then
                    Dim FunctionID As New Guid(FunctionsIDDropDownList.SelectedValue)

                    Dim DocTypeFunctionInfo = SelectedDocType.GetFunctionInfo(FunctionID)

                    If DocTypeFunctionInfo IsNot Nothing Then
                        FunctionControl.Visible = True
                        ' Function
                        FunctionControl.FunctionID = FunctionID
                        FunctionControl.FunctionEnabled = Enabled.Checked
                        FunctionControl.FunctionName = DocTypeFunctionInfo("FunctionName").ToString
                        FunctionControl.FunctionType = DocTypeFunctionInfo("ParameterType").ToString
                        FunctionControl.ParameterName = DocTypeFunctionInfo("ParameterName").ToString.Trim
                        FunctionControl.ParameterValue = Parameter.Value
                    Else
                        FunctionControl.Visible = False
                    End If
                Else
                    FunctionControl.Visible = False
                End If
                hasFunctions = True
            Next
        End If
        diveqViewFunctions.Visible = hasFunctions
        eqViewFunctions.Visible = hasFunctions
    End Sub

#End Region

End Class