﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UNIT.ascx.vb" Inherits="WebApplication._UNIT" %>
<asp:HiddenField ID="ISN_UNIT" runat="server" />
<%--Номер--%>
<%--Опись--%>
<%--Опись--%>
<asp:HiddenField ID="WEIGHT" runat="server" />
<%--Сортировка--%>
<table style="width: 97%; margin-bottom: 4px;">
    <tr>
        <td>
            <eq2:ArchiveCrumbs ID="Crumbs" runat="server" Level="INVENTORY" />
        </td>
        <td style="text-align: right;">
            Раздел описи:
            <eq2:eqDocument ID="ISN_INVENTORY_CLS" runat="server" DocTypeURL="INVENTORYSTRUCTURE.ascx"
                ListDocMode="NewWindowAndSelect" LoadDocMode="NewWindowAndSelect" ShowingFields="NAME,ISN_INVENTORY"
                ShowingFieldsFormat="{0}" KeyName="ISN_INVENTORY_CLS" />
        </td>
        <td style="text-align: right;">
            Тип:
            <asp:DropDownList ID="UNIT_KIND" runat="server" Width="150" />
        </td>
        <td style="text-align: right;">
            Содержимое:
            <asp:HyperLink ID="HyperLinkDOCUMENTs" runat="server" Text="Документы" Target="_blank"
                NavigateUrl="~/Default.aspx?DocTypeURL=DOCUMENT.ascx&ListDoc=True" Style="font-weight: bold;
                padding: 2px;" />
        </td>
    </tr>
</table>
<eq:eqMenu ID="MenuTabs" runat="server" Orientation="Horizontal" StaticEnableDefaultPopOutImage="False"
    ItemWrap="false">
    <Items>
        <asp:MenuItem Text="Общие сведения" Value="0" Selected="true" />
        <asp:MenuItem Text="НСА" Value="1" />
        <asp:MenuItem Text="Физ. состояние" Value="2" />
        <asp:MenuItem Text="Акты" Value="3" />
        <%--<asp:MenuItem Text="Фотодокументы" Value="4" Enabled="false" />
        <asp:MenuItem Text="Электронные документы" Value="5" />
        <asp:MenuItem Text="Кинодокументы" Value="6" />
        <asp:MenuItem Text="Видеодокументы" Value="7" />
        <asp:MenuItem Text="Фонодокументы" Value="8" />
        <asp:MenuItem Text="НТД" Value="9" />
        <asp:MenuItem Text="М/ф-подл." Value="10" />--%>
    </Items>
    <StaticSelectedStyle CssClass="MenuItemStyleSelected" />
</eq:eqMenu>
<div style="border: solid 1px black; padding: 5px;">
    <asp:MultiView ID="MultiViewTabs" runat="server" ActiveViewIndex="0">
        <%--! Общие сведения--%>
        <asp:View runat="server">
            <table style="width: 700px;">
                <tr>
                    <td style="width: 280px;">
                        <fieldset>
                            <legend>Номер ед.хр./ед.уч.*</legend>
                                <table>
                                    <tr>
                                        <td style="width: 100px; text-align: right;">
                                            Номер:
                                        </td>
                                        <td style="width: 80px;">
                                            <eq:eqTextBox ID="UNIT_NUM_1" runat="server" MaxLength="8" />
                                            <aj:FilteredTextBoxExtender runat="server" TargetControlID="UNIT_NUM_1" FilterType="Numbers" />
                                        </td>
                                        <td style="width: 30px;">
                                            <eq:eqTextBox ID="UNIT_NUM_2" runat="server" MaxLength="2" />
                                            <aj:FilteredTextBoxExtender runat="server" TargetControlID="UNIT_NUM_2" FilterType="Custom"
                                                ValidChars="АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЬЫЪЭЮЯ" />
                                        </td>
                                    </tr>
                                </table>
                                <table>
                                    <tr>
                                        <td style="width: 100px; text-align: right;">
                                            Номер в старом формате: 
                                        </td>
                                        <td style="width: 150px;">
                                            <eq:eqTextBox ID="UNIT_NUM_TXT" runat="server" MaxLength="25"></eq:eqTextBox>
                                        </td>
                                    </tr>
                                </table>
                        </fieldset>
                    </td>
                    <td style="vertical-align: text-top; padding-top: 14px;">
                        <table>
                            <tr>
                                <td style="width: 50px; text-align: right;">
                                    Том:
                                </td>
                                <td style="width: 50px;">
                                    <eq:eqTextBox ID="VOL_NUM" runat="server" />
                                </td>
                                <td style="width: 70px; text-align: right;">
                                    Листов:
                                </td>
                                <td style="width: 60px;">
                                    <eq:eqTextBox ID="PAGE_COUNT" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>   
            <table style="width: 700px;">
                <tr>
                    <td style="width: 100px; text-align: right;">
                        Заголовок*:
                    </td>
                    <td>
                        <eq:eqTextBox ID="NAME" runat="server" TextMode="MultiLine" Rows="2" />
                    </td>
                </tr>
            </table>
            <table style="width: 700px;">
                <tr>
                    <td style="width: 100px; text-align: right;">
                        Тип документов*:
                    </td>
                    <td style="width: 250px;">
                        <asp:DropDownList ID="ISN_DOC_TYPE" runat="server" Width="100%" AutoPostBack="true" />
                    </td>
                    <td id="lblISN_HIGH_UNIT" runat="server" style="text-align: right;">
                        Входит в:
                    </td>
                    <td id="valISN_HIGH_UNIT" runat="server" style="width: 150px;">
                        <eq2:eqDocument ID="ISN_HIGH_UNIT" runat="server" DocTypeURL="UNIT.ascx" ListDocMode="NewWindowAndSelect"
                            LoadDocMode="NewWindowAndSelect" ShowingFields="UNIT_NUM_1,UNIT_NUM_2" ShowingFieldsFormat="{0} {1}"
                            KeyName="ISN_UNIT" />
                        <%--ISN_HIGH_UNIT--%>
                        <%--Фоно, Кино, Видео--%>
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
            <table style="width: 700px;">
                <tr>
                    <td style="width: 100px; text-align: right;">
                        Тип носителя*:
                    </td>
                    <td style="width: 150px;">
                        <asp:DropDownList ID="MEDIUM_TYPE" runat="server" Width="100%" AutoPostBack="true" />
                    </td>
                    <td style="width: 100px; text-align: right;">
                        Носитель:
                    </td>
                    <td style="width: 150px;">
                        <eq2:eqDocument ID="ISN_STORAGE_MEDIUM" runat="server" ListDocMode="NewWindowAndSelect"
                            LoadDocMode="NewWindowAndSelect" DocTypeURL="STORAGE_MEDIUM_CL.ascx" ShowingFields="Name"
                            KeyName="ISN_STORAGE_MEDIUM" ShowingFieldsFormat="{0}" />
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
            <table style="width: 700px;">
                <tr>
                    <td style="width: 100px; text-align: right;">
                        Характеристика секретности*:
                    </td>
                    <td style="width: 90px;">
                        <asp:DropDownList ID="ISN_SECURLEVEL" runat="server" Width="100%" AutoPostBack="true" />
                    </td>
                    <td style="width: 80px; text-align: right;">
                        Доступ*:
                    </td>
                    <td style="width: 90px;">
                        <asp:DropDownList ID="SECURITY_CHAR" runat="server" Width="100%" AutoPostBack="true" />
                    </td>
                    <td style="width: 200px; text-align: right;">
                        Причина ограничения доступа:
                    </td>
                    <td>
                        <asp:DropDownList ID="ISN_SECURITY_REASON" runat="server" Width="100%" />
                    </td>
                </tr>
            </table>
            <table style="width: 700px;">
                <tr>
                    <td style="width: 100px; text-align: right;">
                        Производственный номер:
                    </td>
                    <td style="width: 80px;">
                        <eq:eqTextBox ID="PRODUCTION_NUM" runat="server" />
                    </td>
                    <td style="width: 100px; text-align: right;">
                        Делопроизводственный индекс:
                    </td>
                    <td style="width: 150px;">
                        <eq:eqTextBox ID="DELO_INDEX" runat="server" />
                    </td>
                    <td style="width: 70px; text-align: right;">
                        Категория:
                    </td>
                    <td>
                        <asp:DropDownList ID="UNIT_CATEGORY" runat="server" Width="100%" />
                    </td>
                </tr>
            </table>
            <table style="width: 700px;">
                <tr>
                    <td style="width: 400px;">
                        <table style="width: 100%;">
                            <tr>
                                <td style="width: 100px; text-align: right;">
                                    Топографический указатель:
                                </td>
                                <td>
                                    <eq2:eqDocument ID="ISN_LOCATION" runat="server" DocTypeURL="LOCATION.ascx" ShowingFields="PATH_STR"
                                        KeyName="ISN_LOCATION" ShowingFieldsFormat="{0}" ListDocMode="NewWindowAndSelect"
                                        LoadDocMode="NewWindowAndSelect" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100px; text-align: right;">
                                    Раздел описи:
                                </td>
                                <td>
                                    <eq:eqDocument runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100px; text-align: right;">
                                    Язык:
                                </td>
                                <td>
                                    <eq:eqSpecification ID="vREF_LANGUAGE_UNIT" runat="server">
                                        <eq:eqSpecificationColumn>
                                            <ItemTemplate>
                                                <eq:eqDropDownList ID="ISN_LANGUAGE" runat="server" Width="100%" UniqueGroupName="REF_LANGUAGE" />
                                            </ItemTemplate>
                                        </eq:eqSpecificationColumn>
                                    </eq:eqSpecification>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <fieldset>
                            <legend>Даты документов*</legend>
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 100px; text-align: right;">
                                        Точные даты:
                                    </td>
                                    <td colspan="2">
                                        <eq:eqTextBox ID="ALL_DATE" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 100px; text-align: right;">
                                        Начальная дата:
                                    </td>
                                    <td style="width: 80px;">
                                        <eq:eqTextBox ID="START_YEAR" runat="server" />
                                        <aj:FilteredTextBoxExtender runat="server" TargetControlID="START_YEAR" FilterType="Numbers" />
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="START_YEAR_INEXACT" runat="server" Text="неточная" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 100px; text-align: right;">
                                        Конечная дата:
                                    </td>
                                    <td style="width: 80px;">
                                        <eq:eqTextBox ID="END_YEAR" runat="server" />
                                        <aj:FilteredTextBoxExtender runat="server" TargetControlID="END_YEAR" FilterType="Numbers" />
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="END_YEAR_INEXACT" runat="server" Text="неточная" />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
            </table>
            <table style="width: 700px;">
                <tr>
                    <td>
                        <fieldset>
                            <legend>Особенности</legend>
                            <eq:eqSpecification ID="vREF_FEATURE_UNIT" runat="server">
                                <eq:eqSpecificationColumn>
                                    <ItemTemplate>
                                        <eq:eqDropDownList ID="ISN_FEATURE" runat="server" Width="100%" UniqueGroupName="REF_FEATURE" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                            <%--REF_FEATURE--%>
                        </fieldset>
                    </td>
                </tr>
            </table>
            <table style="width: 700px;">
                <tr>
                    <td style="width: 90px; text-align: right;">
                        <asp:CheckBox ID="IS_IN_SEARCH" runat="server" Text="В розыске" />
                    </td>
                    <td style="width: 90px; text-align: right;">
                        <asp:CheckBox ID="IS_LOST" runat="server" Text="Выбыла" />
                    </td>
                    <td style="width: 100px; text-align: right;">
                        <asp:CheckBox ID="HAS_SF" runat="server" Text="Имеет СФ" />
                    </td>
                    <td style="width: 100px; text-align: right;">
                        <asp:CheckBox ID="HAS_FP" runat="server" Text="Имеет ФП" />
                    </td>
                    <td style="width: 130px; text-align: right;">
                        <asp:CheckBox ID="HAS_DEFECTS" runat="server" Text="Наличие дефектов" />
                    </td>
                    <td style="text-align: right;">
                        <asp:CheckBox ID="CATALOGUED" runat="server" Text="Закаталогизирована" />
                    </td>
                </tr>
            </table>
            <table style="width: 700px;">
                <tr>
                    <td style="width: 130px; text-align: right;">
                        <asp:CheckBox ID="CARDBOARDED" runat="server" Text="Закартонирована" />
                    </td>
                    <td style="width: 250px; text-align: right;">
                        <asp:CheckBox ID="HAS_TREASURES" runat="server" Text="Имеет в оформлении драг. камни" />
                    </td>
                    <td style="width: 250px; text-align: right;">
                        <asp:CheckBox ID="IS_MUSEUM_ITEM" runat="server" Text="Является музейным предметом" />
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
            <table style="width: 700px;">
                <tr>
                    <td style="width: 100px; text-align: right;">
                        Примечание:
                    </td>
                    <td>
                        <eq:eqTextBox ID="NOTE" runat="server" TextMode="MultiLine" Rows="2" />
                    </td>
                </tr>
            </table>
            <table style="width: 700px;">
                <tr>
                    <td>
                        <fieldset>
                            <legend>Файлы электронного образа</legend>
                            <eq:eqSpecification ID="vREF_FILE_UNIT_S" runat="server" Width="100%" AllowInsert="false">
                                <eq:eqSpecificationColumn HeaderText="Описание">
                                    <ItemTemplate>
                                        <asp:TextBox ID="NAME" runat="server" Width="99%" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Файл">
                                    <ItemTemplate>
                                        <eq2:RefFile ID="ID" runat="server" Mode="FileSystem" OnRequesting="RefFileS_Requesting" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                            <table>
                                <tr>
                                    <td>
                                        Добавить файл:
                                    </td>
                                    <td>
                                        <eq2:RefFileUpload ID="RefFileUploadFileS" runat="server" Mode="FileSystem" />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
            </table>
        </asp:View>
        <%--НСА--%>
        <asp:View runat="server">
            <table style="width: 700px;">
                <tr>
                    <td style="width: 100px; text-align: right;">
                        Аннотация:
                    </td>
                    <td>
                        <eq:eqTextBox ID="ANNOTATE" runat="server" TextMode="MultiLine" Rows="2" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 100px; text-align: right;">
                        Вспомагательный НСА:
                    </td>
                    <td>
                        <eq:eqTextBox ID="ADDITIONAL_CLS" runat="server" TextMode="MultiLine" Rows="2" />
                    </td>
                </tr>
            </table>
            <table style="width: 700px;">
                <tr>
                    <td>
                        <fieldset>
                            <legend>Файлы</legend>
                            <eq:eqSpecification ID="vREF_FILE_UNIT" runat="server" Width="100%" AllowInsert="false">
                                <eq:eqSpecificationColumn HeaderText="Описание">
                                    <ItemTemplate>
                                        <asp:TextBox ID="NAME" runat="server" Width="99%" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Файл">
                                    <ItemTemplate>
                                        <eq2:RefFile ID="ID" runat="server" Mode="FileSystem" OnRequesting="RefFile_Requesting" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                            <table>
                                <tr>
                                    <td>
                                        Добавить файл:
                                    </td>
                                    <td>
                                        <eq2:RefFileUpload ID="RefFileUploadFile" runat="server" Mode="FileSystem" />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
            </table>
            <table style="width: 700px;">
                <tr>
                    <td>
                        <fieldset>
                            <legend>Межфондовый АНСА</legend>
                            <eq:eqSpecification ID="vREF_CLS701_UNIT" runat="server">
                                <eq:eqSpecificationColumn>
                                    <ItemTemplate>
                                        <eq2:eqDocument ID="ISN_CLS" runat="server" DocTypeURL="CLS701.ascx" ListDocMode="NewWindowAndSelect"
                                            KeyName="ISN_CLS" LoadDocMode="NewWindowAndSelect" ShowingFields="Level2, NAME" ShowingFieldsFormat="{0} : {1}" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
            </table>
            <table style="width: 700px;">
                <tr>
                    <td>
                        <fieldset>
                            <legend>Внутрифондовый АНСА</legend>
                            <eq:eqSpecification ID="vREF_CLS702_UNIT" runat="server">
                                <eq:eqSpecificationColumn>
                                    <ItemTemplate>
                                        <eq2:eqDocument ID="ISN_CLS" runat="server" DocTypeURL="CLS702.ascx" ListDocMode="NewWindowAndSelect"
                                            KeyName="ISN_CLS" LoadDocMode="NewWindowAndSelect" ShowingFields="NAME" ShowingFieldsFormat="{0}" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
            </table>
            <table style="width: 700px;">
                <tr>
                    <td>
                        <fieldset>
                            <legend>АНСА в рамках описи</legend>
                            <eq:eqSpecification ID="vREF_CLS703_UNIT" runat="server">
                                <eq:eqSpecificationColumn>
                                    <ItemTemplate>
                                        <eq2:eqDocument ID="ISN_CLS" runat="server" DocTypeURL="CLS703.ascx" ListDocMode="NewWindowAndSelect"
                                            KeyName="ISN_CLS" LoadDocMode="NewWindowAndSelect" ShowingFields="NAME" ShowingFieldsFormat="{0}" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
            </table>
        </asp:View>
        <%--Физ. состояние--%>
        <asp:View runat="server">
            <table style="width: 900px;">
                <tr>
                    <td>
                        <fieldset>
                            <legend>Физ. состояние</legend>
                            <eq:eqSpecification ID="tblUNIT_STATE" runat="server">
                                <eq:eqSpecificationColumn HeaderText="Дата проверки">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="STATE_DATE" runat="server" />
                                        <aj:MaskedEditExtender ID = "STATE_DATE_MASK" TargetControlID = "STATE_DATE" runat="server" MaskType="Date" Mask="99/99/9999" ClearTextOnInvalid="true"  />
                                        <aj:CalendarExtender runat="server" TargetControlID="STATE_DATE" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Актуальность физ. состояния">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="IS_ACTUAL" runat="server" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Номер акта">
                                    <ItemTemplate>
                                        <eq:eqDropDownList ID="ISN_REF_ACT" runat="server" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Физ. состояние">
                                    <ItemTemplate>
                                        <eq2:eqDocument ID="ISN_STATE" runat="server" DocTypeURL="STATE_CL.ascx" ListDocMode="NewWindowAndSelect"
                                            KeyName="ISN_STATE" LoadDocMode="NewWindowAndSelect" ShowingFields="Name" ShowingFieldsFormat="{0}" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Номера листов">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="PAGE_NUMS" runat="server" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Количество листов">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="PAGE_COUNT" runat="server" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Примечание">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="NOTE" runat="server" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
            </table>
            <table style="width: 900px;">
                <tr>
                    <td>
                        <fieldset>
                            <legend>Требуемые работы</legend>
                            <eq:eqSpecification ID="tblUNIT_REQUIRED_WORK" runat="server">
                                <eq:eqSpecificationColumn HeaderText="Дата проверки">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="CHECK_DATE" runat="server" />
                                        <aj:MaskedEditExtender ID = "CHECK_DATE_MASK" TargetControlID = "CHECK_DATE" runat="server" MaskType="Date" Mask="99/99/9999" ClearTextOnInvalid="true"  />
                                        <aj:CalendarExtender runat="server" TargetControlID="CHECK_DATE" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Актуальность треб. работы">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="IS_ACTUAL" runat="server" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Номер акта">
                                    <ItemTemplate>
                                        <eq:eqDropDownList ID="ISN_REF_ACT" runat="server" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Требуемая работа">
                                    <ItemTemplate>
                                        <eq:eqDropDownList ID="ISN_WORK" runat="server" UniqueGroupName="UNIT_REQUIRED_WORK" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Примечание">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="NOTE" runat="server" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
            </table>
            <table style="width: 900px;">
                <tr>
                    <td>
                        <fieldset>
                            <legend>Проводимые работы</legend>
                            <eq:eqSpecification ID="tblUNIT_WORK" runat="server">
                                <eq:eqSpecificationColumn HeaderText="Дата проведения">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="WORK_DATE" runat="server" />
                                        <aj:MaskedEditExtender ID = "WORK_DATE_MASK" TargetControlID = "WORK_DATE" runat="server" MaskType="Date" Mask="99/99/9999" ClearTextOnInvalid="true"  />
                                        <aj:CalendarExtender runat="server" TargetControlID="WORK_DATE" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Проводимая работа">
                                    <ItemTemplate>
                                        <eq:eqDropDownList ID="ISN_WORK" runat="server" UniqueGroupName="UNIT_WORK" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Примечание">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="NOTE" runat="server" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
            </table>
        </asp:View>
        <%--! Акты--%>
        <asp:View runat="server">
            <table style="width: 900px;">
                <tr>
                    <td>
                        <fieldset>
                            <legend>Акты</legend>
                            <%--СПЕЦИФИКАЦИЯ vREF_ACT--%>
                            <eq:eqSpecification ID="vREF_ACT_FOR_UNIT" runat="server" ShowNumering="false">
                                <eq:eqSpecificationColumn HeaderText="Акт">
                                    <ItemTemplate>
                                        <eq2:eqDocument ID="ISN_ACT" runat="server" DocTypeURL="ACT.ascx" ListDocMode="NewWindowAndSelect"
                                            LoadDocMode="NewWindowAndSelect" ShowingFields="ACT_NUM" ShowingFieldsFormat="{0}"
                                            KeyName="ISN_ACT" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Дата">
                                    <ItemTemplate>
                                        <asp:Label ID="ACT_DATE" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Название">
                                    <ItemTemplate>
                                        <asp:Label ID="ACT_NAME" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Тип акта">
                                    <ItemTemplate>
                                        <asp:Label ID="ACTTYPENAME" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Движение">
                                    <ItemTemplate>
                                        <asp:Label ID="MOVEMENT_FLAG" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="проведённая работа">
                                    <ItemTemplate>
                                        <asp:Label ID="ACT_WORK" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Примечание">
                                    <ItemTemplate>
                                        <asp:Label ID="NOTE" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                            <%--REF_ACT--%>
                        </fieldset>
                    </td>
                </tr>
            </table>
        </asp:View>
        <%--Фотодокументы--%>
        <asp:View runat="server">
            <eq:eqSpecification ID="tblUNIT_FOTO" runat="server" ShowHeader="false" ShowNumering="false"
                AllowDelete="false" AllowInsert="false" AllowReorder="false" SkinID="Empty">
                <eq:eqSpecificationColumn>
                    <ItemTemplate>
                        <asp:PlaceHolder ID="ph_ALL" runat="server">
                            <table style="width: 900px;">
                                <tr>
                                    <td style="width: 120px; text-align: right;">
                                        Место съемки:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="PLACE" runat="server" TextMode="MultiLine" Rows="2" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 120px; text-align: right;">
                                        Автор съемки:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="AUTHOR" runat="server" TextMode="MultiLine" Rows="2" />
                                    </td>
                                </tr>
                            </table>
                            <table style="width: 900px;">
                                <tr>
                                    <td style="width: 120px; text-align: right;">
                                        Цветность:
                                    </td>
                                    <td style="width: 200px;">
                                        <asp:DropDownList ID="COLOR" runat="server" Width="100%" />
                                    </td>
                                    <td style="text-align: right;">
                                        Размер:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="SIZE" runat="server" Width="200px" />
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                        </asp:PlaceHolder>
                        <asp:PlaceHolder ID="ph_EH" runat="server">
                            <table style="width: 900px;">
                                <tr>
                                    <td style="width: 120px; text-align: right;">
                                        Видовой состав:
                                    </td>
                                    <td style="width: 200px;">
                                        <asp:DropDownList ID="ISN_DOC_KIND" runat="server" Width="100%" />
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                            <table style="width: 900px;">
                                <tr>
                                    <td style="width: 120px; text-align: right;">
                                        Внешние особенности:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="STORAGE_INFO" runat="server" TextMode="MultiLine" Rows="2" />
                                    </td>
                                </tr>
                            </table>
                            <table style="width: 900px;">
                                <tr>
                                    <td style="width: 120px; text-align: right;">
                                        Кол-во негативов:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="NEGATIVE_COUNT" runat="server" Width="60px" />
                                    </td>
                                    <td style="text-align: right;">
                                        Кол-во позитивов:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="POSITIVE_COUNT" runat="server" Width="60px" />
                                    </td>
                                    <td style="text-align: right;">
                                        Кол-во фотоотпечатков:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="PRINT_COUNT" runat="server" Width="60px" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 120px; text-align: right;">
                                        Кол-во дубль-негативов:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="DUP_NEGATIVE_COUNT" runat="server" Width="60px" />
                                    </td>
                                    <td style="text-align: right;">
                                        Кол-во слайдов (диапозитовов):
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="SLIDE_COUNT" runat="server" Width="60px" />
                                    </td>
                                    <td style="text-align: right;">
                                        Кол-во страховых копий:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="BACKUP_COUNT" runat="server" Width="60px" />
                                    </td>
                                </tr>
                            </table>
                            <table style="width: 900px;">
                                <tr>
                                    <td style="width: 120px; text-align: right;">
                                        Кол-во кадров панорамной съемки:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="SHOT_COUNT" runat="server" Width="60px" />
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                            <table style="width: 900px;">
                                <tr>
                                    <td style="width: 120px; text-align: right;">
                                        Оригинальность:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="ORIGINAL" runat="server" Width="150px" />
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                            <table style="width: 900px;">
                                <tr>
                                    <td style="width: 120px; text-align: right;">
                                        Сопроводительная документация:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="ACCOMP_DOC" runat="server" TextMode="MultiLine" Rows="2" />
                                    </td>
                                </tr>
                            </table>
                        </asp:PlaceHolder>
                        <asp:PlaceHolder ID="ph_EU" runat="server">
                            <table style="width: 900px;">
                                <tr>
                                    <td style="width: 120px; text-align: right;">
                                        Дата съемки:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="FOTO_DATE" runat="server" Width="100px" />
                                        <aj:CalendarExtender runat="server" TargetControlID="FOTO_DATE" />
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                        </asp:PlaceHolder>
                    </ItemTemplate>
                </eq:eqSpecificationColumn>
            </eq:eqSpecification>
        </asp:View>
        <%--Электронные документы--%>
        <asp:View runat="server">
            <eq:eqSpecification ID="tblUNIT_ELECTRONIC" runat="server" ShowHeader="false" ShowNumering="false"
                AllowDelete="false" AllowInsert="false" AllowReorder="false" SkinID="Empty">
                <eq:eqSpecificationColumn>
                    <ItemTemplate>
                        <table style="width: 900px;">
                            <tr>
                                <td style="width: 120px; text-align: right;">
                                    Количество ед. учета:
                                </td>
                                <td style="width: 90px;">
                                    <eq:eqTextBox ID="UNIT_CNT" runat="server" />
                                </td>
                                <td>
                                </td>
                            </tr>
                        </table>
                        <table style="width: 900px;">
                            <tr>
                                <td style="width: 120px; text-align: right;">
                                    Формат данных:
                                </td>
                                <td>
                                    <eq:eqTextBox ID="DATA_FORMAT" runat="server" TextMode="MultiLine" Rows="2" />
                                </td>
                            </tr>
                        </table>
                        <table style="width: 900px;">
                            <tr>
                                <td style="width: 120px; text-align: right;">
                                    Емкость:
                                </td>
                                <td style="width: 150px;">
                                    <eq:eqTextBox ID="SIZE" runat="server" />
                                </td>
                                <td>
                                </td>
                            </tr>
                        </table>
                        <table style="width: 900px;">
                            <tr>
                                <td style="width: 120px; text-align: right;">
                                    Характеристики носителя:
                                </td>
                                <td>
                                    <eq:eqTextBox ID="CARRIER_DESC" runat="server" TextMode="MultiLine" Rows="2" />
                                </td>
                            </tr>
                        </table>
                        <table style="width: 900px;">
                            <tr>
                                <td style="width: 120px; text-align: right;">
                                    Сопроводительная документация:
                                </td>
                                <td>
                                    <eq:eqTextBox ID="ACCOMP_DOC" runat="server" TextMode="MultiLine" Rows="2" />
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </eq:eqSpecificationColumn>
            </eq:eqSpecification>
        </asp:View>
        <%--Кинодокументы--%>
        <asp:View runat="server">
            <eq:eqSpecification ID="tblUNIT_MOVIE" runat="server" ShowHeader="false" ShowNumering="false"
                AllowDelete="false" AllowInsert="false" AllowReorder="false" SkinID="Empty">
                <eq:eqSpecificationColumn>
                    <ItemTemplate>
                        <asp:PlaceHolder ID="ph_ALL" runat="server">
                            <table style="width: 900px;">
                                <tr>
                                    <td style="width: 120px; text-align: right;">
                                        Сопроводительная документация:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="ACCOMP_DOC" runat="server" TextMode="MultiLine" Rows="2" />
                                    </td>
                                </tr>
                            </table>
                        </asp:PlaceHolder>
                        <asp:PlaceHolder ID="ph_EH" runat="server">
                            <table style="width: 900px;">
                                <tr>
                                    <td style="width: 120px; text-align: right;">
                                        Тип:
                                    </td>
                                    <td style="width: 250px;">
                                        <asp:DropDownList ID="UNIT_TYPE" runat="server" Width="100%" />
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                            <table style="width: 900px;">
                                <tr>
                                    <td style="width: 120px; text-align: right;">
                                        Тип пленки:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="FILM_TYPE" runat="server" TextMode="MultiLine" Rows="2" />
                                    </td>
                                </tr>
                            </table>
                            <table style="width: 900px;">
                                <tr>
                                    <td style="width: 120px; text-align: right;">
                                        Формат пленки:
                                    </td>
                                    <td style="width: 250px;">
                                        <asp:DropDownList ID="FILM_FORMAT" runat="server" Width="100%" />
                                    </td>
                                    <td style="text-align: right;">
                                        Метраж:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="FOOTAGE" runat="server" Width="100px" />
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                            <table style="width: 900px;">
                                <tr>
                                    <td style="width: 120px; text-align: right;">
                                        Характеристики носителя:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="CARRIER_DESC" runat="server" TextMode="MultiLine" Rows="2" />
                                    </td>
                                </tr>
                            </table>
                        </asp:PlaceHolder>
                        <asp:PlaceHolder ID="ph_EU" runat="server">
                            <table style="width: 900px;">
                                <tr>
                                    <td style="width: 120px; text-align: right;">
                                        Вид кинодокумента:
                                    </td>
                                    <td style="width: 250px;">
                                        <asp:DropDownList ID="ISN_DOC_KIND" runat="server" Width="100%" />
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                            <asp:PlaceHolder ID="ph_VV" runat="server">
                                <table style="width: 900px;">
                                    <tr>
                                        <td style="width: 120px; text-align: right;">
                                            Количество ед. хранения:
                                        </td>
                                        <td>
                                            <eq:eqTextBox ID="UNIT_CNT" runat="server" Width="100px" />
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                            </asp:PlaceHolder>
                            <table style="width: 900px;">
                                <tr>
                                    <td>
                                        <fieldset>
                                            <legend>Крайние даты съемки</legend>
                                            <table>
                                                <tr>
                                                    <td style="width: 120px; text-align: right;">
                                                        Дата начала:
                                                    </td>
                                                    <td>
                                                        <eq:eqTextBox ID="START_DATE" runat="server" Width="100px" />
                                                        <aj:CalendarExtender runat="server" TargetControlID="START_DATE" />
                                                    </td>
                                                    <td style="width: 120px; text-align: right;">
                                                        Дата окончания:
                                                    </td>
                                                    <td>
                                                        <eq:eqTextBox ID="END_DATE" runat="server" Width="100px" />
                                                        <aj:CalendarExtender runat="server" TargetControlID="END_DATE" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </td>
                                </tr>
                            </table>
                            <table style="width: 900px;">
                                <tr>
                                    <td style="width: 120px; text-align: right;">
                                        Год изготовления:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="tb_YEAR" runat="server" Width="80px" ReadOnly="true" />
                                    </td>
                                    <td style="text-align: right;">
                                        Время воспроизведения:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="PLAYBACK_TIME" runat="server" Width="100px" />
                                    </td>
                                    <td style="text-align: right;">
                                        Кол-во частей:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="PART_COUNT" runat="server" Width="80px" />
                                    </td>
                                </tr>
                            </table>
                            <table style="width: 900px;">
                                <tr>
                                    <td style="width: 120px; text-align: right;">
                                        Звук:
                                    </td>
                                    <td style="width: 200px;">
                                        <asp:DropDownList ID="SOUND" runat="server" Width="100%" />
                                    </td>
                                    <td style="text-align: right;">
                                        Цветность:
                                    </td>
                                    <td style="width: 200px;">
                                        <asp:DropDownList ID="COLOR" runat="server" Width="100%" />
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                            <table style="width: 900px;">
                                <tr>
                                    <td style="width: 120px; text-align: right;">
                                        Формат:
                                    </td>
                                    <td style="width: 250px;">
                                        <asp:DropDownList ID="FORMAT" runat="server" Width="100%" />
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                            <table style="width: 900px;">
                                <tr>
                                    <td style="width: 120px; text-align: right;">
                                        Автор:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="AUTHOR" runat="server" TextMode="MultiLine" Rows="2" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 120px; text-align: right;">
                                        Место съемки:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="SHOOTING_PLACE" runat="server" TextMode="MultiLine" Rows="2" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 120px; text-align: right;">
                                        Место изготовления:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="PRODUCTION_PLACE" runat="server" TextMode="MultiLine" Rows="2" />
                                    </td>
                                </tr>
                            </table>
                            <asp:PlaceHolder ID="ph_HH" runat="server">
                                <table style="width: 900px;">
                                    <tr>
                                        <td>
                                            <fieldset>
                                                <legend>Количиственные характеристики</legend>
                                                <table>
                                                    <tr>
                                                        <td style="width: 120px; text-align: right;">
                                                            &nbsp;
                                                        </td>
                                                        <td style="width: 100px;">
                                                            Негатив:
                                                        </td>
                                                        <td style="width: 100px;">
                                                            Дубль - негатив:
                                                        </td>
                                                        <td style="width: 100px;">
                                                            Фонограмма - негатив:
                                                        </td>
                                                        <td style="width: 100px;">
                                                            Магнитная - фонограмма:
                                                        </td>
                                                        <td style="width: 100px;">
                                                            Магнитная - фонограмма (совмещен.):
                                                        </td>
                                                        <td style="width: 100px;">
                                                            Промежут. позитив:
                                                        </td>
                                                        <td style="width: 100px;">
                                                            Позитив:
                                                        </td>
                                                        <td style="width: 100px;">
                                                            Страховые копии (контратип):
                                                        </td>
                                                        <td style="width: 100px;">
                                                            Страховые копии (промежут. позитив):
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 120px; text-align: right;">
                                                            Кол-во ед.хр.:
                                                        </td>
                                                        <td>
                                                            <eq:eqTextBox ID="NEGATIVE_COUNT" runat="server" Width="60px" />
                                                        </td>
                                                        <td>
                                                            <eq:eqTextBox ID="DUP_NEGATIVE_COUNT" runat="server" Width="60px" />
                                                        </td>
                                                        <td>
                                                            <eq:eqTextBox ID="PHONO_NEGATIVE_COUNT" runat="server" Width="60px" />
                                                        </td>
                                                        <td>
                                                            <eq:eqTextBox ID="PHONO_MAG_COUNT" runat="server" Width="60px" />
                                                        </td>
                                                        <td>
                                                            <eq:eqTextBox ID="PHONO_COMBINED_COUNT" runat="server" Width="60px" />
                                                        </td>
                                                        <td>
                                                            <eq:eqTextBox ID="INTERPOSITIVE_COUNT" runat="server" Width="60px" />
                                                        </td>
                                                        <td>
                                                            <eq:eqTextBox ID="POSITIVE_COUNT" runat="server" Width="60px" />
                                                        </td>
                                                        <td>
                                                            <eq:eqTextBox ID="DUP_BACKUP_COUNT" runat="server" Width="60px" />
                                                        </td>
                                                        <td>
                                                            <eq:eqTextBox ID="INTERPOSITIVE_BACKUP_COUNT" runat="server" Width="60px" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 120px; text-align: right;">
                                                            Метраж:
                                                        </td>
                                                        <td>
                                                            <eq:eqTextBox ID="NEGATIVE_LENGTH" runat="server" Width="60px" />
                                                        </td>
                                                        <td>
                                                            <eq:eqTextBox ID="DUP_NEGATIVE_LENGTH" runat="server" Width="60px" />
                                                        </td>
                                                        <td>
                                                            <eq:eqTextBox ID="PHONO_NEGATIVE_LENGTH" runat="server" Width="60px" />
                                                        </td>
                                                        <td>
                                                            <eq:eqTextBox ID="PHONO_MAG_LENGTH" runat="server" Width="60px" />
                                                        </td>
                                                        <td>
                                                            <eq:eqTextBox ID="PHONO_COMBINED_LENGTH" runat="server" Width="60px" />
                                                        </td>
                                                        <td>
                                                            <eq:eqTextBox ID="INTERPOSITIVE_LENGTH" runat="server" Width="60px" />
                                                        </td>
                                                        <td>
                                                            <eq:eqTextBox ID="POSITIVE_LENGTH" runat="server" Width="60px" />
                                                        </td>
                                                        <td>
                                                            <eq:eqTextBox ID="DUP_BACKUP_LENGTH" runat="server" Width="60px" />
                                                        </td>
                                                        <td>
                                                            <eq:eqTextBox ID="INTERPOSITIVE_BACKUP_LENGTH" runat="server" Width="60px" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                        </td>
                                    </tr>
                                </table>
                                <table style="width: 900px;">
                                    <tr>
                                        <td style="width: 120px; text-align: right;">
                                            Установочные ролики:
                                        </td>
                                        <td>
                                            <eq:eqTextBox ID="CINEX_COUNT" runat="server" Width="100px" />
                                        </td>
                                        <td style="text-align: right;">
                                            Цветовые паспорта:
                                        </td>
                                        <td>
                                            <eq:eqTextBox ID="COLOR_PASS_COUNT" runat="server" Width="100px" />
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                            </asp:PlaceHolder>
                        </asp:PlaceHolder>
                    </ItemTemplate>
                </eq:eqSpecificationColumn>
            </eq:eqSpecification>
        </asp:View>
        <%--Видеодокументы--%>
        <asp:View runat="server">
            <eq:eqSpecification ID="tblUNIT_VIDEO" runat="server" ShowHeader="false" ShowNumering="false"
                AllowDelete="false" AllowInsert="false" AllowReorder="false" SkinID="Empty">
                <eq:eqSpecificationColumn>
                    <ItemTemplate>
                        <asp:PlaceHolder ID="ph_ALL" runat="server">
                            <table style="width: 900px;">
                                <tr>
                                    <td style="width: 120px; text-align: right;">
                                        Сопроводительная документация:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="ACCOMP_DOC" runat="server" TextMode="MultiLine" Rows="2" />
                                    </td>
                                </tr>
                            </table>
                            <table style="width: 900px;">
                                <tr>
                                    <td style="width: 120px; text-align: right;">
                                        Количество ед. учета/хранения:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="UNIT_CNT" runat="server" Width="100px" />
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                        </asp:PlaceHolder>
                        <asp:PlaceHolder ID="ph_EH" runat="server">
                            <table style="width: 900px;">
                                <tr>
                                    <td style="width: 120px; text-align: right;">
                                        Дата записи (перезаписи):
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="REC_DATE" runat="server" Width="100px" />
                                        <aj:CalendarExtender runat="server" TargetControlID="REC_DATE" />
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                            <table style="width: 900px;">
                                <tr>
                                    <td style="width: 120px; text-align: right;">
                                        Тип записи:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="REC_TYPE" runat="server" Width="250px" />
                                    </td>
                                    <td style="text-align: right;">
                                        Оригинал/копия:
                                    </td>
                                    <td style="width: 100px;">
                                        <asp:DropDownList ID="ORIGINAL" runat="server" Width="100%" />
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                            <table style="width: 900px;">
                                <tr>
                                    <td style="width: 120px; text-align: right;">
                                        Формат записи:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="REC_FORMAT" runat="server" />
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                            <table style="width: 900px;">
                                <tr>
                                    <td style="width: 120px; text-align: right;">
                                        Характеристики носителя:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="CARRIER_DESC" runat="server" TextMode="MultiLine" Rows="2" />
                                    </td>
                                </tr>
                            </table>
                        </asp:PlaceHolder>
                        <asp:PlaceHolder ID="ph_EU" runat="server">
                            <table style="width: 900px;">
                                <tr>
                                    <td style="width: 120px; text-align: right;">
                                        Вид видеодокумента:
                                    </td>
                                    <td style="width: 200px;">
                                        <asp:DropDownList ID="ISN_DOC_KIND" runat="server" Width="100%" />
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                            <table style="width: 900px;">
                                <tr>
                                    <td>
                                        <fieldset>
                                            <legend>Крайние даты съемки</legend>
                                            <table>
                                                <tr>
                                                    <td style="width: 120px; text-align: right;">
                                                        Дата начала:
                                                    </td>
                                                    <td>
                                                        <eq:eqTextBox ID="START_DATE" runat="server" Width="100px" />
                                                        <aj:CalendarExtender runat="server" TargetControlID="START_DATE" />
                                                    </td>
                                                    <td style="width: 120px; text-align: right;">
                                                        Дата окончания:
                                                    </td>
                                                    <td>
                                                        <eq:eqTextBox ID="END_DATE" runat="server" Width="100px" />
                                                        <aj:CalendarExtender runat="server" TargetControlID="END_DATE" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </td>
                                </tr>
                            </table>
                            <table style="width: 900px;">
                                <tr>
                                    <td style="width: 120px; text-align: right;">
                                        Год изготовления:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="tb_YEAR" runat="server" Width="60px" ReadOnly="true" />
                                    </td>
                                    <td style="text-align: right;">
                                        Время воспроизведения:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="PLAYBACK_TIME" runat="server" Width="100px" />
                                    </td>
                                    <td style="text-align: right;">
                                        Кол-во частей:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="PART_COUNT" runat="server" Width="100px" />
                                    </td>
                                </tr>
                            </table>
                            <table style="width: 900px;">
                                <tr>
                                    <td style="width: 120px; text-align: right;">
                                        Звук:
                                    </td>
                                    <td style="width: 200px;">
                                        <asp:DropDownList ID="SOUND" runat="server" Width="100%" />
                                    </td>
                                    <td style="text-align: right;">
                                        Цветность:
                                    </td>
                                    <td style="width: 200px;">
                                        <asp:DropDownList ID="COLOR" runat="server" Width="100%" />
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                            <table style="width: 900px;">
                                <tr>
                                    <td style="width: 120px; text-align: right;">
                                        Автор:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="AUTHOR" runat="server" TextMode="MultiLine" Rows="2" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 120px; text-align: right;">
                                        Исполнитель:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="PERFORMER" runat="server" TextMode="MultiLine" Rows="2" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 120px; text-align: right;">
                                        Место съемки:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="SHOOTING_PLACE" runat="server" TextMode="MultiLine" Rows="2" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 120px; text-align: right;">
                                        Место изготовления:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="PRODUCTION_PLACE" runat="server" TextMode="MultiLine" Rows="2" />
                                    </td>
                                </tr>
                            </table>
                            <table style="width: 900px;">
                                <tr>
                                    <td>
                                        <fieldset>
                                            <legend>Количиственные характеристики</legend>
                                            <table>
                                                <tr>
                                                    <td style="width: 120px; text-align: right;">
                                                        &nbsp;
                                                    </td>
                                                    <td style="width: 100px;">
                                                        Оригиналы:
                                                    </td>
                                                    <td style="width: 100px;">
                                                        Копии:
                                                    </td>
                                                    <td style="width: 100px;">
                                                        Страховые копии:
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 120px; text-align: right;">
                                                        Кол-во ед.хр.:
                                                    </td>
                                                    <td>
                                                        <eq:eqTextBox ID="ORIGINAL_COUNT" runat="server" Width="60px" />
                                                    </td>
                                                    <td>
                                                        <eq:eqTextBox ID="COPY_COUNT" runat="server" Width="60px" />
                                                    </td>
                                                    <td>
                                                        <eq:eqTextBox ID="BACKUP_COUNT" runat="server" Width="60px" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </td>
                                </tr>
                            </table>
                        </asp:PlaceHolder>
                    </ItemTemplate>
                </eq:eqSpecificationColumn>
            </eq:eqSpecification>
        </asp:View>
        <%--Фонодокументы--%>
        <asp:View runat="server">
            <eq:eqSpecification ID="tblUNIT_PHONO" runat="server" ShowHeader="false" ShowNumering="false"
                AllowDelete="false" AllowInsert="false" AllowReorder="false" SkinID="Empty">
                <eq:eqSpecificationColumn>
                    <ItemTemplate>
                        <asp:PlaceHolder ID="ph_ALL" runat="server">
                            <table style="width: 900px;">
                                <tr>
                                    <td style="width: 120px; text-align: right;">
                                        Сопроводительная документация:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="ACCOMP_DOC" runat="server" TextMode="MultiLine" Rows="2" />
                                    </td>
                                </tr>
                            </table>
                        </asp:PlaceHolder>
                        <asp:PlaceHolder ID="ph_EH" runat="server">
                            <table style="width: 900px;">
                                <tr>
                                    <td style="width: 120px; text-align: right;">
                                        Количество ед. учета/хранения:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="UNIT_CNT" runat="server" Width="70px" />
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                            <table style="width: 900px;">
                                <tr>
                                    <td style="width: 120px; text-align: right;">
                                        Скорость записи:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="REC_SPEED" runat="server" Width="200px" />
                                    </td>
                                    <td style="width: 250px; text-align: right;">
                                        Дата записи (перезаписи):
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="REC_DATE" runat="server" Width="100px" />
                                        <aj:CalendarExtender runat="server" TargetControlID="REC_DATE" />
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                            <table style="width: 900px;">
                                <tr>
                                    <td style="width: 120px; text-align: right;">
                                        Тип:
                                    </td>
                                    <td style="width: 200px;">
                                        <asp:DropDownList ID="PHONO_TYPE" runat="server" Width="100%" />
                                    </td>
                                    <td style="text-align: right;">
                                        Метраж:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="LENGTH" runat="server" Width="100px" />
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                            <table style="width: 900px;">
                                <tr>
                                    <td style="width: 120px; text-align: right;">
                                        Место записи (перезаписи):
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="REC_PLACE" runat="server" TextMode="MultiLine" Rows="2" />
                                    </td>
                                </tr>
                            </table>
                            <table style="width: 900px;">
                                <tr>
                                    <td style="width: 120px; text-align: right;">
                                        Характеристики носителя:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="CARRIER_DESC" runat="server" TextMode="MultiLine" Rows="2" />
                                    </td>
                                </tr>
                            </table>
                        </asp:PlaceHolder>
                        <asp:PlaceHolder ID="ph_EU" runat="server">
                            <table style="width: 900px;">
                                <tr>
                                    <td style="width: 120px; text-align: right;">
                                        Вид фонодокумента:
                                    </td>
                                    <td style="width: 200px;">
                                        <asp:DropDownList ID="ISN_DOC_KIND" runat="server" Width="100%" />
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                            <table style="width: 900px;">
                                <tr>
                                    <td style="width: 120px; text-align: right;">
                                        Год изготовления:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="tb_YEAR" runat="server" Width="100px" ReadOnly="true" />
                                    </td>
                                    <td style="text-align: right;">
                                        Время звучания, мин:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="PLAYBACK_TIME" runat="server" Width="100px" />
                                    </td>
                                </tr>
                            </table>
                            <table style="width: 900px;">
                                <tr>
                                    <td style="width: 120px; text-align: right;">
                                        Место изготовления:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="PRODUCTION_PLACE" runat="server" TextMode="MultiLine" Rows="2" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 120px; text-align: right;">
                                        Автор:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="AUTHOR" runat="server" TextMode="MultiLine" Rows="2" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 120px; text-align: right;">
                                        Исполнитель:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="PERFORMER" runat="server" TextMode="MultiLine" Rows="2" />
                                    </td>
                                </tr>
                            </table>
                            <asp:PlaceHolder ID="ph_HH" runat="server">
                                <table style="width: 900px;">
                                    <tr>
                                        <td>
                                            <fieldset>
                                                <legend>Количиственные характеристики</legend>
                                                <table>
                                                    <tr>
                                                        <td style="width: 120px; text-align: right">
                                                            &nbsp;
                                                        </td>
                                                        <td style="width: 100px;">
                                                            Граммафонные оригиналы:
                                                        </td>
                                                        <td style="width: 100px;">
                                                            Грампластиники:
                                                        </td>
                                                        <td style="width: 100px;">
                                                            Оригиналы (негативы):
                                                        </td>
                                                        <td style="width: 100px;">
                                                            Копии (позитивы):
                                                        </td>
                                                        <td style="width: 100px;">
                                                            Страховые копии:
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 120px; text-align: right">
                                                            Количество ед. хранения:
                                                        </td>
                                                        <td>
                                                            <eq:eqTextBox ID="ORIGINAL_COUNT" runat="server" Width="60px" />
                                                        </td>
                                                        <td>
                                                            <eq:eqTextBox ID="DISK_COUNT" runat="server" Width="60px" />
                                                        </td>
                                                        <td>
                                                            <eq:eqTextBox ID="NEGATIVE_COUNT" runat="server" Width="60px" />
                                                        </td>
                                                        <td>
                                                            <eq:eqTextBox ID="COPY_COUNT" runat="server" Width="60px" />
                                                        </td>
                                                        <td>
                                                            <eq:eqTextBox ID="BACKUP_COUNT" runat="server" Width="60px" />
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 120px; text-align: right">
                                                            Метраж:
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            <eq:eqTextBox ID="NEGATIVE_LENGTH" runat="server" Width="60px" />
                                                        </td>
                                                        <td>
                                                            <eq:eqTextBox ID="COPY_LENGTH" runat="server" Width="60px" />
                                                        </td>
                                                        <td>
                                                            <eq:eqTextBox ID="BACKUP_LENGTH" runat="server" Width="60px" />
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                        </td>
                                    </tr>
                                </table>
                            </asp:PlaceHolder>
                        </asp:PlaceHolder>
                    </ItemTemplate>
                </eq:eqSpecificationColumn>
            </eq:eqSpecification>
        </asp:View>
        <%--НТД--%>
        <asp:View runat="server">
            <eq:eqSpecification ID="tblUNIT_NTD" runat="server" ShowHeader="false" ShowNumering="false"
                AllowDelete="false" AllowInsert="false" AllowReorder="false" SkinID="Empty">
                <eq:eqSpecificationColumn>
                    <ItemTemplate>
                        <table style="width: 900px;">
                            <tr>
                                <td style="width: 120px; text-align: right;">
                                    Номер комплекса:
                                </td>
                                <td style="width: 50px;">
                                    <eq:eqTextBox ID="COMPLEX_NUM_1" runat="server" />
                                </td>
                                <td style="width: 40px;">
                                    <eq:eqTextBox ID="COMPLEX_NUM_2" runat="server" />
                                </td>
                                <td>
                                </td>
                            </tr>
                        </table>
                        <table style="width: 900px;">
                            <tr>
                                <td style="width: 120px; text-align: right;">
                                    Стадия (этап) разработки:
                                </td>
                                <td>
                                    <eq:eqTextBox ID="DEV_STAGE" runat="server" />
                                </td>
                            </tr>
                        </table>
                        <table style="width: 900px;">
                            <tr>
                                <td style="width: 120px; text-align: right;">
                                    Автор, руководитель, ответственный исполнители:
                                </td>
                                <td>
                                    <eq:eqTextBox ID="AUTHOR" runat="server" TextMode="MultiLine" Rows="2" />
                                </td>
                            </tr>
                        </table>
                        <table style="width: 900px;">
                            <tr>
                                <td style="width: 120px; text-align: right;">
                                    Организация, разработчик, соисполнители:
                                </td>
                                <td>
                                    <eq:eqTextBox ID="ORGANIZ" runat="server" TextMode="MultiLine" Rows="2" />
                                </td>
                            </tr>
                        </table>
                        <table style="width: 900px;">
                            <tr>
                                <td style="width: 120px; text-align: right;">
                                    Вид документации:
                                </td>
                                <td style="width: 200px;">
                                    <asp:DropDownList ID="ISN_NTD_KIND" runat="server" Width="100%" />
                                </td>
                                <td>
                                </td>
                            </tr>
                        </table>
                        <table style="width: 900px;">
                            <tr>
                                <td style="width: 120px; text-align: right;">
                                    Дата подачи заявки:
                                </td>
                                <td style="width: 100px;">
                                    <eq:eqTextBox ID="REQUEST_DATE" runat="server" />
                                    <aj:CalendarExtender runat="server" TargetControlID="REQUEST_DATE" />
                                </td>
                                <td style="width: 220px; text-align: right;">
                                    Номер патента:
                                </td>
                                <td style="width: 200px;">
                                    <eq:eqTextBox ID="PATENT_NUM" runat="server" />
                                </td>
                                <td>
                                </td>
                            </tr>
                        </table>
                        <table style="width: 900px;">
                            <tr>
                                <td style="width: 120px; text-align: right;">
                                    Номер заявки:
                                </td>
                                <td style="width: 120px;">
                                    <eq:eqTextBox ID="REQUEST_NUM" runat="server" />
                                </td>
                                <td style="width: 200px; text-align: right;">
                                    Номер авторского свидетельства:
                                </td>
                                <td style="width: 200px;">
                                    <eq:eqTextBox ID="COPYRIGHT_NUM" runat="server" />
                                </td>
                                <td>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </eq:eqSpecificationColumn>
            </eq:eqSpecification>
        </asp:View>
        <%--М/ф-подл.--%>
        <asp:View runat="server">
            <eq:eqSpecification ID="tblUNIT_MICROFORM" runat="server" ShowHeader="false" ShowNumering="false"
                AllowDelete="false" AllowInsert="false" AllowReorder="false" SkinID="Empty">
                <eq:eqSpecificationColumn>
                    <ItemTemplate>
                        <table style="width: 900px;">
                            <tr>
                                <td style="width: 120px; text-align: right;">
                                    Количество кадров:
                                </td>
                                <td style="width: 50px;">
                                    <eq:eqTextBox ID="FRAME_COUNT" runat="server" />
                                </td>
                                <td>
                                </td>
                            </tr>
                        </table>
                        <table style="width: 900px;">
                            <tr>
                                <td style="width: 120px; text-align: right;">
                                    Характеристики носителя:
                                </td>
                                <td>
                                    <eq:eqTextBox ID="CARRIER_DESC" runat="server" TextMode="MultiLine" Rows="2" />
                                </td>
                            </tr>
                        </table>
                        <table style="width: 900px;">
                            <tr>
                                <td style="width: 120px; text-align: right;">
                                    Количество страховых копий:
                                </td>
                                <td style="width: 50px;">
                                    <eq:eqTextBox ID="BACKUP_COUNT" runat="server" />
                                </td>
                                <td>
                                </td>
                            </tr>
                        </table>
                        <table style="width: 900px;">
                            <tr>
                                <td style="width: 120px; text-align: right;">
                                    Сопроводительная документация:
                                </td>
                                <td>
                                    <eq:eqTextBox ID="ACCOMP_DOC" runat="server" TextMode="MultiLine" Rows="2" />
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </eq:eqSpecificationColumn>
            </eq:eqSpecification>
        </asp:View>
    </asp:MultiView>
</div>
