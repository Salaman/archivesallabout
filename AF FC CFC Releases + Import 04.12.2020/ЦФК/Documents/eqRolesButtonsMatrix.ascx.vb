﻿Partial Class eqRolesButtonsMatrix
  Inherits BaseDoc


#Region "Константы, переменные"
    Private Enum GridFillMode
        gfmDataTable = 1
        gfmArray = 2
        gfmNew = 3
    End Enum
#End Region '>>"Константы, переменные"

#Region "Математические функции и рассчет даты, времени"

#End Region '>>"Математические функции и рассчет времени"

#Region "Events, события страницы и контролов"
    ''' <summary>
    ''' Обработка по событию загрузка страницы
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        FillFieldDocTypeID()
        DrawGrid()
    End Sub

    ''' <summary>
    ''' Изменения типа документа
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub FieldDocTypeID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FieldDocTypeID.SelectedIndexChanged
        If FieldDocTypeID.SelectedValue <> "" Then
            LoadSpec()
        End If
    End Sub
#End Region '>>"Events, события страницы и контролов"

#Region "Переопределенные функции"
    ''' <summary>
    ''' Сохранение документа
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overrides Function SaveDoc() As Boolean
        Try

            If MyBase.SaveDoc() Then
                SaveGrid1(TRolesToolBarDenied)

                MyBase.DocType.sGrids = "eqRolesToolBarDenied;"
                MyBase.DocType.SaveDoc(DS)
            End If
            Return True

        Catch ex As Exception
            ShowErrorMessage(Err.Description)
            Return False
        End Try

    End Function
#End Region '>>"Переопределенные функции"

#Region "Функции переходов по статусам"

#End Region '>>"Функции переходов по статусам"

#Region "Работа с БД"
    ''' <summary>
    ''' Заполнение выпадающего списка типов документов
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub FillFieldDocTypeID()
        If FieldDocTypeID.Items.Count = 0 Then
            Dim sSql = <text>SELECT ID, DocType FROM eqDocTypes
WHERE 1=1
 AND ID IN
(SELECT RDA.DocTypeID
FROM eqRolesDocumentsAccess RDA
JOIN eqRoles R ON R.ID=RDA.DocID
JOIN eqDocTypes DT ON RDA.DocTypeID=DT.ID
WHERE R.RoleName=@RoleName AND R.Deleted=0 AND DT.Deleted=0)
ORDER BY 2</text>

            ' WHERE (Category IN ('Закупки','Логистика и склад','Контрагенты','Работы','Финансы','Касса','Закупки и логистика','Продажи','Документы') OR DocURL='View.ascx')

            Dim cmd = New SqlClient.SqlCommand(sSql.Value)
            cmd.Parameters.AddWithValue("@RoleName", DataSetHeaderDataRow("RoleName").ToString)
            Dim tbl = AppSettings.GetDataTable(cmd, "eqDocTypes")
            For Each r In tbl.Rows
                Dim itm As New ListItem(r(1).ToString, r(0).ToString)
                FieldDocTypeID.Items.Add(itm)
            Next
        End If
    End Sub

    ''' <summary>
    ''' запрос выводящий колонки
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetSqlX() As String
        Get
            Dim gFieldDocTypeID As New Guid(FieldDocTypeID.SelectedValue)
            Return "SELECT ID, StateName, StateDescription, DocTypeID FROM eqDocStates where DocTypeID = @DocTypeID and StateName<>'Удален' union all SELECT '00000000-0000-0000-0000-000000000000', 'Новый', 'Новый', @DocTypeID"
        End Get
    End Property

    ''' <summary>
    ''' запрос выводящий строки
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetSqlY() As String
        Get
            Dim gFieldDocTypeID As New Guid(FieldDocTypeID.SelectedValue)
            Dim xmlSql = <sql>
                             select ID, SortOrder RowID, Caption Text
                                from dbo.eqDocToolBar 
                                where ShowInListOrDocument='D'
                                and DocTypeID=@DocTypeID
                         </sql>

            Dim sSql As String = xmlSql.Value
            Return sSql
        End Get
    End Property


    Private ReadOnly Property GetSqlData() As String
        Get
            Dim gFieldDocTypeID As New Guid(FieldDocTypeID.SelectedValue)
            Dim xmlSql = <sql>
                    select ID, DocTypeID, IDButton, StatusID, Hide
                    from dbo.eqRolesToolBarDenied
                    where DocID=@DocID
                         </sql>
            Dim sSql As String = xmlSql.Value
            Return sSql
        End Get
    End Property

#End Region '>>"Работа с БД"

#Region "Права доступа, показать/скрыть коптрол, разрешить/запретить редактирование"

#End Region '>>"Права доступа, показать/скрыть коптрол, разрешить/запретить редактирование"

#Region "Специальные функции для Help-файла"

    Public Function IsAccountWorkClear() As Boolean

    End Function

    Public Function IsAccountWorkFilled() As Boolean

    End Function

#End Region '>>"Специальные функции для Help-файла"

#Region "Свойства"
    ''' <summary>
    ''' получение таблицы с колонками
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property GetCmdX() As DataTable
        Get
            Dim sSQL = GetSqlX
            Dim cmdX = New SqlClient.SqlCommand(sSQL)
            cmdX.Parameters.AddWithValue("@DocTypeID", GetChoosenDocTypeID)
            Dim X As DataTable = AppSettings.GetDataTable(cmdX)
            Return X
        End Get
    End Property

    ''' <summary>
    ''' получение таблицы со строками
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property GetCmdY() As DataTable
        Get
            Dim sSQL = GetSqlY
            Dim cmdY = New SqlClient.SqlCommand(sSQL)
            cmdY.Parameters.AddWithValue("@DocID", DocID)
            cmdY.Parameters.AddWithValue("@DocTypeID", GetChoosenDocTypeID)
            Dim Y As DataTable = AppSettings.GetDataTable(cmdY)
            Return Y
        End Get
    End Property

    ''' <summary>
    ''' получение таблицы с данными
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property GetCmdData() As DataTable
        Get
            Dim sSQL = GetSqlData
            Dim cmdD = New SqlClient.SqlCommand(sSQL)
            cmdD.Parameters.AddWithValue("@DocID", DocID)
            cmdD.Parameters.AddWithValue("@DocTypeID", GetChoosenDocTypeID)
            Dim D As DataTable = AppSettings.GetDataTable(cmdD)
            Return D
        End Get
    End Property

    ''' <summary>
    ''' выбранный тип документа
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property GetChoosenDocTypeID() As Guid
        Get
            Return New Guid(FieldDocTypeID.SelectedValue)
        End Get
    End Property

    ''' <summary>
    ''' Имя 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property GetTableControlName()
        Get
            Return "TRolesToolBarDenied"
        End Get
    End Property

    ''' <summary>
    ''' Имя таблицы спецификации в датасете
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property GetDataTableName()
        Get
            Return "eqRolesToolBarDenied"
        End Get
    End Property

#End Region '>>"Свойства"

#Region "Прочие"

#End Region '>>"Прочие"

#Region "Нарисовать таблицу, сохранить данные из таблицы"

    ''' <summary>
    ''' Вызов функции построения таблицы, подготовка данных
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub DrawGrid()
        Dim sSQL As String = ""
        If Me.FieldDocTypeID.SelectedValue = "" Then
            FillFieldDocTypeID()
        End If
        If Me.FieldDocTypeID.SelectedValue <> "" Then
            sSQL = GetSqlY()
            Dim Y = GetCmdY
            If Y.Rows.Count > 0 Then ' если в спецификации не содержится записей, то грид не формируется
                Dim X = GetCmdX
                GridFill1(TRolesToolBarDenied, X, Y, GetCmdData)
            End If
        End If
    End Sub

    ''' <summary>
    ''' продедура формирования пустой спецификации (в частности в новом документе)
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub LoadSpec()
        Dim X As DataTable = GetCmdX  'X - таблица заголовков
        Dim Y As DataTable = GetCmdY 'Y - таблица заголовков
        Dim D As DataTable = GetCmdData  'Y - таблица с данными

        If X.Rows.Count > 0 And Y.Rows.Count > 0 Then
            GridFill1(TRolesToolBarDenied, X, Y, D)
        End If

    End Sub

    ''' <summary>
    ''' Заполняет таблицу строками
    ''' </summary>
    ''' <param name="T">Контрол asp:table</param>
    ''' <param name="X">Таблица с заголовками колонок</param>
    ''' <param name="Y">Таблица с данными</param>
    ''' <remarks>Create by Anatoly Mel'kov on 2008-05-12</remarks>
    Private Sub GridFill1(ByRef T As Table, ByVal X As DataTable, ByVal Y As DataTable, ByVal D As DataTable)
        Dim iRowID = -1
        Dim iRowNumber = -1
        Dim iColNumber = -1
        Dim tc As TableCell 'это переменная используется как ячейка контрола Table

        T.Rows.Clear()

        AddHeaderTable(T, X) 'заполняем заголовок

        For Each YRow As DataRow In Y.Rows
            iRowNumber += 1
            Dim NewRow As New TableRow
            tc = New TableCell
            Dim L As New Label
            L.Text = YRow("Text")
            tc.Controls.Add(L)
            Dim HiddenValue = New HiddenField
            HiddenValue.ID = "Y" & CType(YRow("ID"), Guid).ToString("n")
            HiddenValue.Value = CType(YRow("ID"), Guid).ToString()
            tc.Controls.Add(HiddenValue)
            NewRow.Cells.Add(tc)
            Dim sYID = YRow("ID").ToString
            iColNumber = 0 'со второй колонки
            For Each XCells As DataRow In X.Rows
                iColNumber += 1
                Dim sXID = XCells("ID").ToString
                Dim rows = D.Select("IDButton='" & sYID & "' and StatusID='" & sXID & "'")
                Dim cbxHidden As CheckBox = New CheckBox()
                cbxHidden.ID = "cbxHidden_r" & iRowNumber & "_c" & iColNumber
                If rows.Count > 0 Then
                    If Not IsDBNull(rows(0)("Hide")) Then
                        cbxHidden.Checked = rows(0)("Hide")
                    End If
                End If
                tc = New TableCell()
                tc.Controls.Add(cbxHidden)
                NewRow.Cells.Add(tc)
            Next XCells
            T.Rows.Add(NewRow)
        Next YRow
        AddFooterTable(T, X) 'заполняем подвал водой
        SetCellsStyte(T)
    End Sub

    ''' <summary>
    ''' Нарисовать границы ячеек
    ''' </summary>
    ''' <param name="T"></param>
    ''' <remarks></remarks>
    Sub SetCellsStyte(ByVal T As Table)
        'T.BorderStyle = BorderStyle.Dotted
        'For Each row As TableRow In T.Rows
        '    For Each C As TableCell In row.Cells
        '        C.BorderStyle = BorderStyle.Double
        '        C.BorderWidth = New Unit(1, UnitType.Pixel)
        '        C.BorderColor = Drawing.Color.Black
        '    Next C
        'Next row
    End Sub

    ''' <summary>
    ''' добавление заголовка
    ''' </summary>
    ''' <param name="T"></param>
    ''' <param name="X"></param>
    ''' <remarks>Create by Anatoly Melkov on 2008-05-12</remarks>
    Private Sub AddHeaderTable(ByRef T As Table, ByVal X As DataTable)
        Dim tc = New TableCell() 'переменная, обозначающая ячейку в таблице
        Dim iCol As Integer = 0
        tc.Text = "Кнопки\статусы"
        Dim NewRow As New TableRow
        NewRow.ID = "Row0"
        NewRow.Cells.Add(tc)
        For Each r As DataRow In X.Rows
            tc = New TableCell
            tc.Text = r("StateName").ToString
            Dim lblLabel = New Label
            lblLabel.Text = r("StateName").ToString
            tc.Controls.Add(lblLabel)
            'запишем идентификаторы статусов
            Dim HiddenValue = New HiddenField
            HiddenValue.ID = "HiddenValueCol" & iCol
            HiddenValue.Value = r("ID").ToString()
            tc.Controls.Add(HiddenValue)
            tc.Width = 150
            NewRow.Cells.Add(tc)
            iCol += 1
        Next
        T.Rows.Add(NewRow)
    End Sub

    ''' <summary>
    ''' Добавляем footer
    ''' пока пустой
    ''' </summary>
    ''' <param name="T"></param>
    ''' <param name="X"></param>
    ''' <remarks>created by Anatoly Mel'kov on 2008-05-12</remarks>
    Private Sub AddFooterTable(ByRef T As Table, ByVal X As DataTable)

    End Sub

    ''' <summary>
    ''' Считываем данные из Table в таблицу спецификации
    ''' </summary>
    ''' <param name="T"></param>
    ''' <remarks>Created by Anatoly Mel'kov on 2008-05-13</remarks>
    Private Sub SaveGrid1(ByVal T As Table)
        Dim iRowsCount = T.Rows.Count
        Dim iCellsCount = T.Rows(0).Cells.Count
        If T.Rows.Count > 1 AndAlso T.Rows(0).Cells.Count > 1 Then

            Dim gDocTypeID As Guid = GetChoosenDocTypeID() 'гуид типа документа, необходим для работы конкретного алгоритма

            For iRow = 1 To T.Rows.Count - 1 'читаем иnфу из контролов Table
                Dim tcY = T.Rows(iRow).Cells(0)
                Dim hfY = CType(tcY.Controls(1), HiddenField) 'первый контрол - текст, второй контрол HiddenField
                Dim gYID As Guid = New Guid(hfY.Value.ToString)
                For iCol = 1 To iCellsCount - 1
                    Dim tcX = T.Rows(0).Cells(iCol)
                    Dim hfX = CType(tcX.Controls(1), HiddenField) 'первый контрол - текст, второй контрол HiddenField
                    Dim gXID = New Guid(hfX.Value.ToString) 'в данном случае получаем гуид статуса

                    '<<==получим значения из контролах в ячейках. Контролы в ячейках распологаются в том же порядке, в каком их добавляли
                    Dim tc = T.Rows(iRow).Cells(iCol)
                    Dim cbxHidden As CheckBox = CType(tc.Controls(0), CheckBox)
                    '>>==получим значения из контролах в ячейках

                    '<<==Изменим записи в таблице спецификации
                    Dim tSpec As DataTable = DS.Tables(GetDataTableName)
                    Dim tSpecRow As DataRow = Nothing
                    Dim tSpecRows As DataRow() = tSpec.Select("DocTypeID='" & gDocTypeID.ToString & "' and IDButton='" & gYID.ToString & "' and StatusID='" & gXID.ToString & "'")
                    If tSpecRows.Length > 0 Then
                        tSpecRow = tSpecRows(0)
                    End If

                    If tSpecRow IsNot Nothing Then
                        If cbxHidden.Checked Then
                            tSpecRow("Hide") = cbxHidden.Checked
                            tSpecRow.Item("RowID") = (iRow * 10000) + iCol * 10
                        Else
                            tSpecRow.Delete()
                        End If
                    Else
                        If cbxHidden.Checked = True Then
                            tSpec.Columns("ID").AllowDBNull = True
                            Dim NewRow = tSpec.Rows.Add()
                            NewRow.Item("ID") = Guid.NewGuid()
                            NewRow.Item("DocID") = Me.DocID
                            NewRow.Item("RowID") = (iRow * 10000) + iCol * 10
                            NewRow.Item("OwnerID") = AppSettings.UserInfo.UserID
                            NewRow.Item("CreationDateTime") = Now()
                            NewRow.Item("DocTypeID") = gDocTypeID
                            NewRow.Item("IDButton") = gYID
                            NewRow.Item("StatusID") = gXID
                            NewRow.Item("Hide") = True
                        End If
                    End If
                    'End If
                    '>>==Изменим записи в таблице спецификации
                Next
            Next
        End If
    End Sub
#End Region '>>"Нарисовать таблицу, сохранить данные из таблицы"

    Private Sub fffff_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles fffff.Click
        SaveGrid1(TRolesToolBarDenied)
    End Sub
End Class
