﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CLS701.ascx.vb" Inherits="WebApplication.CLS701" %>
<table style="width: 700px;">
    <colgroup>
        <col width="150px" />
        <col />
    </colgroup>
    <tr>
        <td>
            Верхний уровень
        </td>
        <td> 
            <eq2:eqDocument ID="ISN_HIGH_CLS" KeyName="ISN_CLS" runat="server" DocTypeURL="CLS.ascx"
                ListDocMode="NewWindowAndSelect" LoadDocMode="NewWindowAndSelect" ShowingFields="NAME"
                ShowingFieldsFormat="{0}" ValidateHierarchy="true" />
            <asp:LinkButton ID="clearISN_HIGH_CLS" runat="server" Text="Удалить" ToolTip="Сделать корневым элементом" />
        </td>
    </tr>
    <tr>
        <td>
            Наименование
        </td>
        <td>
            <eq:eqTextBox ID="NAME" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            Код:
        </td>
        <td>
            <eq:eqTextBox ID="CODE" runat="server" />
        </td>
    </tr>
    <tr>
        <td valign="top">
            Примечание:
        </td>
        <td>
            <eq:eqTextBox ID="NOTE" runat="server" TextMode="MultiLine" Rows="4" />
        </td>
    </tr>
</table>
