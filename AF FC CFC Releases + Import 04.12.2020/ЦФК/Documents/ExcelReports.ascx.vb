﻿Public Partial Class ExcelReports
    Inherits BaseDoc


    Public Sub Rep_Changed(ByVal sender As Object, ByVal e As System.EventArgs)
        For Each c In Panel_Properties.Controls
            If TypeOf c Is Panel Then
                DirectCast(c, Panel).Visible = False
            End If
        Next
        Panel_SecurityFlags.Visible = True

        If Rep_PassportArchiveStore.Checked Then ' Паспорт архивохранилища
            Panel_ISN_LOCATION.Visible = True
        ElseIf Rep_LocationStory.Checked Then ' Номерник архивохранилища
            Panel_ISN_LOCATION.Visible = True
        ElseIf Rep_PassportArchive.Checked Then ' Паспорт архива
            Panel_ISN_PASSPORT.Visible = True
        ElseIf Rep_ArchStory.Checked Then ' Номерник архива
        ElseIf Rep_ArchiveDynamicsa_Abs.Checked Then ' Динамика паспорта архива (абсолютная)
            Panel_ISN_PASSPORT2.Visible = True
        ElseIf Rep_ArchiveDynamics_Per.Checked Then ' Динамика  паспорта архива (в процентах)
            Panel_ISN_PASSPORT2.Visible = True
        ElseIf Rep_TraceableInfoArchiveList.Checked Then ' Сводные сведения по архиву
        ElseIf Rep_PhysicalStateDoc.Checked Then ' Физическое состояние документов фонда
            Panel_ISN_FUND.Visible = True
        ElseIf Rep_PhysicalStateDocStorage.Checked Then ' Физическое состояние документов хранилища
            Panel_ISN_LOCATION.Visible = True
        ElseIf Rep_PhysicalStateDocArchive.Checked Then ' Физическое состояние документов архива
        ElseIf Rep_UndetectedUnitStorage.Checked Then ' Необнаруженные единицы хранения

        ElseIf Rep_NsaContent.Checked Then ' Состав НСА к документам на бумажной основе
        ElseIf Rep_CompositionFund_1.Checked Then ' Состав фондов 1 категории
        ElseIf Rep_CompositionFund_2.Checked Then ' Состав фондов 2 категории
        ElseIf Rep_CompositionFund_3.Checked Then ' Состав фондов 3 категории
        ElseIf Rep_CompositionFund_None.Checked Then ' Состав некатегорированных фондов
        ElseIf Rep_CompositionFund_Any.Checked Then ' Состав фондов архива
        ElseIf Rep_FundList.Checked Then ' Список фондов
            Panel_Year.Visible = True
            Prop_Year.Enabled = True
            'Panel_Year2.Visible = True
        ElseIf Rep_FundListImportantPlace.Checked Then ' Список фондов, содержащих ОЦД
        ElseIf Rep_CompositionFundList.Checked Then ' Состав фонда
            Panel_ISN_FUND.Visible = True
        ElseIf Rep_InfoChangeFundsList.Checked Then ' Сведения об изменениях в составе и объеме фондов
            Panel_Period.Visible = True
            Panel_Year.Visible = True
            Prop_Year.Enabled = True
        ElseIf Rep_ArchList.Checked Then ' Архивная опись
            Panel_ISN_INVENTORY.Visible = True
        ElseIf Rep_InvVideoDok.Checked Then ' Опись видеодокументов
            Panel_ISN_INVENTORY.Visible = True
        ElseIf Rep_InvKinoDok.Checked Then ' Опись кинодокументов
            Panel_ISN_INVENTORY.Visible = True
        ElseIf Rep_InvFonoDokGramophoneRec.Checked Then ' Опись фонодокументов граммофонной записи (грампластинок)
            Panel_ISN_INVENTORY.Visible = True
        ElseIf Rep_InvFonoDokMagneticRec.Checked Then ' Опись фонодокументов магнитной записи
            Panel_ISN_INVENTORY.Visible = True
        ElseIf Rep_InvPhotographAlbum.Checked Then ' Опись фотоальбомов
            Panel_ISN_INVENTORY.Visible = True
        ElseIf Rep_InvPhotoDok.Checked Then ' Опись фотодокументов
            Panel_ISN_INVENTORY.Visible = True
        ElseIf Rep_InvElectronDok.Checked Then ' Опись электронных документов
            Panel_ISN_INVENTORY.Visible = True
        ElseIf Rep_ReestrOpisDel.Checked Then ' Реестр описей дел, документов
            Panel_Year.Visible = True
            Prop_Year.Enabled = True
        ElseIf Rep_InvImportantPlace.Checked Then ' Опись ОЦД
            Panel_ISN_FUND.Visible = True
        ElseIf Rep_ListImportantPlaceFund.Checked Then ' Перечень ОЦД в фонде (аналог описи ОЦД без заголовков)
            Panel_ISN_FUND.Visible = True
        ElseIf Rep_DocumDeclassifyList.Checked Then ' Перечень рассекреченных документов
        ElseIf Rep_CardDeclassifyDoc.Checked Then ' Карточка рассекреченного документа
            Panel_ISN_UNIT.Visible = True
        ElseIf Rep_InnerList.Checked Then ' Внутренняя опись документов дела
            Panel_ISN_UNIT.Visible = True
        ElseIf Rep_CompositionDoc_1.Checked Then ' Состав документов 1 категории
        ElseIf Rep_CompositionDoc_2.Checked Then ' Состав документов 2 категории
        ElseIf Rep_CompositionDoc_3.Checked Then ' Состав документов 3 категории
        ElseIf Rep_CompositionDoc_None.Checked Then ' Состав документов в некатегорированных фондах
        ElseIf Rep_CompositionDoc_Any.Checked Then ' Состав документов архива
        ElseIf Rep_FundGuide.Checked Then ' Путеводитель по фондам архива
            Panel_ISN_CLS.Visible = True
            Panel_Children.Visible = True
            Panel_Kind2.Visible = True
            Panel_Year.Visible = True
            If Prop_Kind2.SelectedValue = 1 Then
                Panel_Year.Visible = False
            Else
                Panel_Year.Visible = True
            End If
        ElseIf Rep_ShortGuide.Checked Then ' Краткий справочник по фондам архива
            Panel_ISN_CLS.Visible = True
            Panel_Children.Visible = True
            Panel_Kind2.Visible = True
            Panel_Year.Visible = True
            If Prop_Kind2.SelectedValue = 1 Then
                Panel_Year.Visible = False
            Else
                Panel_Year.Visible = True
            End If
        ElseIf Rep_ShortGuideListFund.Checked Then ' Краткий справочник (список фондов) по фондам архива
            Panel_ISN_CLS.Visible = True
            Panel_Children.Visible = True
            Panel_Kind2.Visible = True
            Panel_Year.Visible = True
            If Prop_Kind2.SelectedValue = 1 Then
                Panel_Year.Visible = False
            Else
                Panel_Year.Visible = True
            End If
            ElseIf Rep_Index.Checked Then ' Указатель
                Panel_ISN_FUND.Visible = False
                Panel_ISN_CLS.Visible = True
                Panel_Children.Visible = True
                Panel_Kind2.Visible = False
            ElseIf Rep_TopIndex.Checked Then ' Постеллажный топографический указатель
                Panel_ISN_LOCATION.Visible = True
            ElseIf Rep_FundTopIndex.Checked Then ' Пофондовый топографический указатель
                Panel_ISN_FUND.Visible = True
            ElseIf Rep_Check.Checked Then ' Проверка правильности и полноты заполнения БД «Архивный фонд»
                Panel_Check.Visible = True
            End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'проверим права пользователя и заблокируем флажки уровней секретности
      SetSecurityFlags
       
        If Not IsPostBack Then
            Rep_Changed(Nothing, EventArgs.Empty)
        End If
    End Sub

    Public Sub MakeReport()
        Dim cmd As New SqlClient.SqlCommand("select 1")
        AppSettings.ExecCommand(cmd)

        Try
            Dim ExcelReportGeneratorWork As New ExcelReportGenerator.ExcelReportGenerator
            Dim ewMemoryStream As New IO.MemoryStream
            Dim sReportFileName As String
            Dim sReportFileNameSave As String
            Dim IsHTML As Boolean
            Dim noCalc As Boolean = False  ' указывает, что не следует пересчитывать все формулы в документе после обновления значения каждой ячейки (ускоряет время формирования отчета примерно в 100 раз)

            sReportFileName = vbNullString
            sReportFileNameSave = vbNullString

            If Rep_PassportArchive.Checked Then 'паспорт архива
                sReportFileName = "tplArchivePassport.xls"
                sReportFileNameSave = "ПаспортАрхива"
            End If

            If Rep_ArchiveDynamicsa_Abs.Checked Then 'динамика архива абсолютная
                sReportFileName = "tplArchiveDynamicAbs.xls"
                sReportFileNameSave = "ДинамикаПаспортаАрхиваАбс"
            End If

            If Rep_ArchiveDynamics_Per.Checked Then 'динамика архива процентная
                sReportFileName = "tplArchiveDynamicPct.xls"
                sReportFileNameSave = "ДинамикаПаспортаАрхиваПроцент"
            End If

            If Rep_ArchList.Checked Then 'Архивная опись, еще передать параметром код документа
                sReportFileName = "tplArchiveList.xls"
                sReportFileNameSave = "АрхивнаяОпись"
                noCalc = True
            End If

            If Rep_FundList.Checked Then 'Список фондов
                sReportFileName = "tplFundList.xls"
                sReportFileNameSave = "СписокФондов"
                noCalc = True
            End If

            If Rep_FundListImportantPlace.Checked Then 'Список фондов, содержащих ОЦД
                sReportFileName = "tplFundListImportantPlace.xls"
                sReportFileNameSave = "СписокФондовСОЦД"
                noCalc = True
            End If
            If Rep_CompositionFundList.Checked Then 'Состав фонда
                sReportFileName = "tplCompositionFundList.xls"
                sReportFileNameSave = "СоставФонда"
                noCalc = True
            End If

            If Rep_InfoChangeFundsList.Checked Then 'Сведения об изменениях в составе и объеме фондов
                sReportFileName = "tplInfoChangeFundsList.xls"
                sReportFileNameSave = "СведенияОбИзмСостОбъемФондов"
                'noCalc = True
            End If

            If Rep_TraceableInfoArchiveList.Checked Then 'сводные сведения по архиву
                sReportFileName = "tplTraceableInfoArchiveList.xls"
                sReportFileNameSave = "СводныеСведенияПоАрхиву"
                noCalc = True
            End If

            If Rep_PhysicalStateDoc.Checked Then 'физическое состояние документов фонда
                sReportFileName = "tplPhysicalStateDoc.xls"
                sReportFileNameSave = "ФизСостДокФонда"
                noCalc = True

            End If
            If Rep_PhysicalStateDocStorage.Checked Then 'физическое состояние документов ХРАНИЛИЩА
                sReportFileName = "tplPhysicalStateDocStorage.xls"
                sReportFileNameSave = "ФизСостДокХранилища"
                noCalc = True
            End If

            If Rep_PhysicalStateDocArchive.Checked Then 'физическое состояние документов архива
                sReportFileName = "tplPhysicalStateDocArchive.xls"
                sReportFileNameSave = "ФизСостДокАрхива"
                noCalc = True
            End If

            If Rep_ReestrOpisDel.Checked Then 'реестр описей дел, документов
                sReportFileName = "tplReestrOpisDel.xls"
                sReportFileNameSave = "РеестрОписейДелДокументов"
                noCalc = True
            End If

            If Rep_InvImportantPlace.Checked Then 'Опись ОЦД
                sReportFileName = "tplInvImportantPlace.xls"
                sReportFileNameSave = "ОписьОЦД"
                noCalc = True
            End If

            If Rep_InnerList.Checked Then 'Внутренняя опись документов дела
                sReportFileName = "tplInnerList.xls"
                sReportFileNameSave = "ВнутренняяОписьДокументовДела"
                noCalc = True
            End If

            If Rep_FundTopIndex.Checked Then 'Пофондовый топографический указатель
                sReportFileName = "tplFundTopIndex.xls"
                sReportFileNameSave = "ТопографичУказательПофондовый"
                noCalc = True
            End If

            If Rep_TopIndex.Checked Then 'Постеллажный топографический указатель
                sReportFileName = "tplTopIndex.xls"
                sReportFileNameSave = "ТопографичУказательПостеллажный"
                noCalc = True
            End If


            If Rep_DocumDeclassifyList.Checked Then 'Перечень рассекреченных документов
                sReportFileName = "tplDocumDeclassifyList.xls"
                sReportFileNameSave = "ПереченьРассекречДок"
                noCalc = True
            End If

            If Rep_CardDeclassifyDoc.Checked Then 'Карточка рассекреченного документа
                sReportFileName = "tplCardDeclassifyDoc.xls"
                sReportFileNameSave = "КарточкаРассекречДок"
                noCalc = True
            End If

            If Rep_CompositionDoc_Any.Checked Then 'Состав документов архива
                sReportFileName = "tplCompositionDoc.xls"
                sReportFileNameSave = "СоставДокументовАрхива"
                noCalc = True
            End If
            If Rep_CompositionDoc_None.Checked Then 'Состав документов архива без категории
                sReportFileName = "tplCompositionDoc_None.xls"
                sReportFileNameSave = "СоставДокАрхиваБезКатегории"
                noCalc = True
            End If
            If Rep_CompositionDoc_1.Checked Then 'Состав документов архива 1 категории
                sReportFileName = "tplCompositionDoc_1.xls"
                sReportFileNameSave = "СоставДокАрхиваКатегория1"
                noCalc = True
            End If
            If Rep_CompositionDoc_2.Checked Then 'Состав документов архива 2 категории
                sReportFileName = "tplCompositionDoc_2.xls"
                sReportFileNameSave = "СоставДокАрхиваКатегория2"
                noCalc = True
            End If
            If Rep_CompositionDoc_3.Checked Then 'Состав документов архива 3 категории
                sReportFileName = "tplCompositionDoc_3.xls"
                sReportFileNameSave = "СоставДокАрхиваКатегория3"
                noCalc = True
            End If

            If Rep_InvVideoDok.Checked Then 'Опись видеодокументов
                sReportFileName = "tplInvVideoDok.xls"
                sReportFileNameSave = "ОписьВидеодокументов"
                noCalc = True
            End If

            If Rep_InvKinoDok.Checked Then 'Опись кинодокументов
                sReportFileName = "tplInvKinoDok.xls"
                sReportFileNameSave = "ОписьКинодокументов"
                noCalc = True
            End If
            If Rep_InvFonoDokGramophoneRec.Checked Then 'Опись фонодокументов (граммпластинки)
                sReportFileName = "tplInvFonoDokGramophoneRec.xls"
                sReportFileNameSave = "ОписьФонодокументовГраммпластинки"
                noCalc = True
            End If
            If Rep_InvFonoDokMagneticRec.Checked Then 'Опись фонодокументов (магнитная запись)
                sReportFileName = "tplInvFonoDokMagneticRec.xls"
                sReportFileNameSave = "ОписьФонодокументовМагнит"
                noCalc = True
            End If
            If Rep_InvPhotographAlbum.Checked Then 'Опись фотоальбомов
                sReportFileName = "tplInvPhotographAlbum.xls"
                sReportFileNameSave = "ОписьФотоальбомов"
                noCalc = True
            End If
            If Rep_InvPhotoDok.Checked Then 'Опись фотодокументов
                sReportFileName = "tplInvPhotoDok.xls"
                sReportFileNameSave = "ОписьФотодокументов"
                noCalc = True
            End If
            If Rep_InvElectronDok.Checked Then 'Опись электронных документов
                sReportFileName = "tplInvElectronDok.xls"
                sReportFileNameSave = "ОписьЭлектронныхДокументов"
                noCalc = True
            End If
            If Rep_ListImportantPlaceFund.Checked Then 'Перечень ОЦД в фонде
                sReportFileName = "tplListImportantPlaceFund.xls"
                sReportFileNameSave = "ПереченьОЦДвФонде"
                noCalc = True
            End If
            If Rep_CompositionFund_Any.Checked Then 'Состав фондов архива
                sReportFileName = "tplCompositionFund.xls"
                sReportFileNameSave = "СоставФондовАрхива"
                noCalc = True
            End If
            If Rep_CompositionFund_1.Checked Then 'Состав фондов архива к.1
                sReportFileName = "tplCompositionFund_1.xls"
                sReportFileNameSave = "СоставФондовКатегория1"
                noCalc = True
            End If
            If Rep_CompositionFund_2.Checked Then 'Состав фондов архива к.2
                sReportFileName = "tplCompositionFund_2.xls"
                sReportFileNameSave = "СоставФондовКатегория2"
                noCalc = True
            End If
            If Rep_CompositionFund_3.Checked Then 'Состав фондов архива к.3
                sReportFileName = "tplCompositionFund_3.xls"
                sReportFileNameSave = "СоставФондовКатегория3"
                noCalc = True
            End If
            If Rep_CompositionFund_None.Checked Then 'Состав фондов архива без категории
                sReportFileName = "tplCompositionFund_None.xls"
                sReportFileNameSave = "СоставФондовБезКатегории"
                noCalc = True
            End If
            If Rep_NsaContent.Checked Then 'Состав НСА к документам на бум.основе
                sReportFileName = "tplNsaContent.xls"
                sReportFileNameSave = "СоставНСА"
                noCalc = True
            End If
            If Rep_UndetectedUnitStorage.Checked Then 'Необнаруженные ед.хр.
                sReportFileName = "tplUndetectedUnitStorage.xls"
                sReportFileNameSave = "НеобнаруженныеЕдХр"
                noCalc = True
            End If
            If Rep_ArchStory.Checked Then 'Номерник архива
                sReportFileName = "tplReportArchStory.xls"
                sReportFileNameSave = "НомерникАрхива"
            End If
            If Rep_LocationStory.Checked Then 'Номерник архивохранилища
                sReportFileName = "tplLocationStory.xls"
                sReportFileNameSave = "НомерникАрхивохранилища"
            End If
            If Rep_PassportArchiveStore.Checked Then 'Паспорт архивохранилища
                sReportFileName = "tplPassportArchiveStore.xls"
                sReportFileNameSave = "ПаспортАрхивохранилища"
            End If
            If Rep_Check.Checked Then 'Проверка правильности и полноты заполнения БД
                sReportFileName = "tplReportCheck.xls"
                sReportFileNameSave = "ПроверкаПравильностиЗаполнения"
                noCalc = True
            End If

            If Rep_FundGuide.Checked Then ' Путеводитель по фондам архива
                sReportFileName = "tplFundGuide.xls"
                sReportFileNameSave = "ПутеводительПоФондам"
                IsHTML = True
            End If

            If Rep_ShortGuide.Checked Then ' Краткий справочник по фондам архива
                sReportFileName = "tplShortGuide.xls"
                sReportFileNameSave = "КраткийСправочникПоФондам"
                IsHTML = True
            End If

            If Rep_ShortGuideListFund.Checked Then ' Краткий справочник (список фондов) по фондам архива
                sReportFileName = "tplShortGuideListFund.xls"
                sReportFileNameSave = "КраткийСправочникСписок"
                IsHTML = True
            End If
            If Rep_Index.Checked Then ' Указатель
                sReportFileName = "tplIndex.xls"
                sReportFileNameSave = "Указатель"
                IsHTML = True
            End If

            If Not IsDBNull(sReportFileName) Then
                If IsDBNull(sReportFileNameSave) Then
                    sReportFileNameSave = "НазваниеНеОпределено"
                End If

                Dim arrParameterValueBox(24) As String
                Dim arrParameterWebBox(24) As String
                SetParameterValueBox(arrParameterValueBox, arrParameterWebBox)

                ExcelReportGeneratorWork.ExcelReportGeneration(AppSettings, ewMemoryStream, _
                                                                        sReportFileName, _
                                                                        arrParameterValueBox, arrParameterWebBox, noCalc)
                If IsHTML = True Then
                    BasePage.AttachFile(sReportFileNameSave & "_" & Now.ToString("yyyy.MM.dd_HH.mm.ss") & ".doc", ewMemoryStream.ToArray())
                Else
                    BasePage.AttachFile(sReportFileNameSave & "_" & Now.ToString("yyyy.MM.dd_HH.mm.ss") & ".xls", ewMemoryStream.ToArray())
                End If
                ewMemoryStream.Close()
            End If

        Catch ex As Exception
            AddMessage(ex.Message)

        End Try
    End Sub

    Private ReadOnly Property Secur() As Integer
        Get
            Dim s = 0
            If Prop_SecurityFlags.Items(0).Selected Then s += 1
            If Prop_SecurityFlags.Items(1).Selected Then s += 2
            If Prop_SecurityFlags.Items(2).Selected Then s += 4
            Return s
        End Get
    End Property

    Public Sub SetParameterValueBox(ByVal ParameterValueBox() As String, ByVal ParameterNameBox() As String)
        Dim i As Integer
        For i = 0 To 24
            ParameterValueBox(i) = -1
        Next
        For i = 0 To 24
            ParameterNameBox(i) = ""
        Next

        i = 0
        If Prop_SecurityFlags.Items(0).Selected Then
            ParameterValueBox(i) = Prop_SecurityFlags.Items(0).Value
            ParameterNameBox(i) = "Prop_ISN_SECURLEVEL"
        End If
        i = i + 1
        If Prop_SecurityFlags.Items(1).Selected Then
            ParameterValueBox(i) = Prop_SecurityFlags.Items(1).Value
            ParameterNameBox(i) = "Prop_ISN_SECURLEVEL1"
        End If
        i = i + 1
        If Prop_SecurityFlags.Items(2).Selected Then
            ParameterValueBox(i) = Prop_SecurityFlags.Items(2).Value
            ParameterNameBox(i) = "Prop_ISN_SECURLEVEL2"
        End If
        i = i + 1

        If Prop_ISN_LOCATION.KeyValue Then
            ParameterValueBox(i) = Prop_ISN_LOCATION.KeyValue
            ParameterNameBox(i) = "Prop_ISN_LOCATION"
        End If
        i = i + 1

        If Prop_ISN_FUND.KeyValue Then
            ParameterValueBox(i) = Prop_ISN_FUND.KeyValue
            ParameterNameBox(i) = "Prop_ISN_FUND"
        End If
        i = i + 1
        If Prop_ISN_INVENTORY.KeyValue Then
            ParameterValueBox(i) = Prop_ISN_INVENTORY.KeyValue
            ParameterNameBox(i) = "Prop_ISN_INVENTORY"
        End If
        i = i + 1
        If Prop_ISN_UNIT.KeyValue Then
            ParameterValueBox(i) = Prop_ISN_UNIT.KeyValue
            ParameterNameBox(i) = "Prop_ISN_UNIT"
        End If
        i = i + 1
        If Prop_ISN_PASSPORT.KeyValue Then
            ParameterValueBox(i) = Prop_ISN_PASSPORT.KeyValue
            ParameterNameBox(i) = "Prop_ISN_PASSPORT"
        End If
        i = i + 1
        If Prop_ISN_PASSPORT1.KeyValue Then
            ParameterValueBox(i) = Prop_ISN_PASSPORT1.KeyValue
            ParameterNameBox(i) = "Prop_ISN_PASSPORT1"
        End If
        i = i + 1
        If Prop_ISN_PASSPORT2.KeyValue Then
            ParameterValueBox(i) = Prop_ISN_PASSPORT2.KeyValue
            ParameterNameBox(i) = "Prop_ISN_PASSPORT2"
        End If
        i = i + 1
        If Prop_Year.Text Then
            ParameterValueBox(i) = CLng(Prop_Year.Text)
            ParameterNameBox(i) = "Prop_Year"
        End If
        i = i + 1
        If Prop_Year1.Text Then
            ParameterValueBox(i) = CLng(Prop_Year1.Text)
            ParameterNameBox(i) = "Prop_Year1"
        End If
        i = i + 1
        If Prop_Year2.Text Then
            ParameterValueBox(i) = CLng(Prop_Year2.Text)
            ParameterNameBox(i) = "Prop_Year2"
        End If
        i = i + 1
        If Prop_ISN_CLS.KeyValue Then
            ParameterValueBox(i) = Prop_ISN_CLS.KeyValue
            ParameterNameBox(i) = "Prop_ISN_CLS"
        End If
        i = i + 1
        If Prop_Period.Text Then
            ParameterValueBox(i) = CLng(Prop_Period.SelectedValue)
            ParameterNameBox(i) = "Prop_Period"
        End If
        i = i + 1
        If Prop_Kind2.Text Then
            ParameterValueBox(i) = CLng(Prop_Kind2.SelectedValue)
            ParameterNameBox(i) = "Prop_Kind2"
        End If
        i = i + 1
        If Prop_Children.Checked Then
            ParameterValueBox(i) = 1
            ParameterNameBox(i) = "Prop_Children"
        End If
        i = i + 1
        If Prop_Children_All.Checked Then
            ParameterValueBox(i) = 1
            ParameterNameBox(i) = "Prop_Children_All"
        End If
        i = i + 1
        Debug.Print("Parameters")
        For i = 0 To 24
            Debug.Print(ParameterNameBox(i) + "  " + CStr(ParameterValueBox(i)))
        Next
    End Sub

    Public Property ParametersBox_(ByVal sParameterName As String) As Long
        Get

        End Get
        Set(ByVal value As Long)


        End Set
    End Property
      
    Private Sub Prop_Kind2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Prop_Kind2.SelectedIndexChanged
        'If Len(Prop_Year.Text) > 1 Then
        '    sYear = Prop_Year.Text
        'End If
        If Prop_Kind2.SelectedValue = 1 Then
            Panel_Year.Visible = False
        Else
          Panel_Year.Visible = True
        End If
    End Sub

    Public Sub SetSecurityFlags()
        'проверим права пользователя и заблокируем флажки уровней секретности
        Dim sSELECT As String

        sSELECT = "SELECT Parameter " & _
              " FROM eqRolesListCondSpecial RLCS " & _
              " JOIN eqRoles R ON R.ID=RLCS.DocID " & _
              " JOIN eqGroupsRoles GR ON GR.RoleID=R.ID " & _
              " JOIN eqGroups G ON G.ID=GR.DocID " & _
              " JOIN eqGroupMembers GM ON GM.DocID=G.ID " & _
              " WHERE GM.UserOrGroupID=@UserID " & _
              " AND RLCS.FunctionID IN " & _
              "(select ID from eqDocTypeFunctions where FunctionName='Секретность' " & _
              " AND DocID IN (SELECT ID FROM eqDocTypes WHERE DocType ='Фонд') ) " & _
              " AND RLCS.Enabled=1"
        'добавить параметр юзер
        Dim ReportSelectCmd As New SqlClient.SqlCommand(sSELECT)
        ReportSelectCmd.Parameters.AddWithValue("@UserID", AppSettings.UserInfo.UserID)
        Dim tbl = AppSettings.GetDataTable(ReportSelectCmd)

        Panel_SecurityFlags.Visible = True
        Prop_SecurityFlags.Items(1).Selected = False
        Prop_SecurityFlags.Items(1).Enabled = False
        Prop_SecurityFlags.Items(2).Selected = False
        Prop_SecurityFlags.Items(2).Enabled = False 'чс
        Prop_SecurityFlags.Items(0).Selected = True 'Откр
        'если нет строк, то берем все
        'если есть - то только те, которые есть
        If tbl.Rows.Count() > 0 Then
            For Each row As DataRow In tbl.Rows
                If InStr(row(0).ToString(), "чс") > 0 Then
                    Prop_SecurityFlags.Items(2).Selected = True
                    Prop_SecurityFlags.Items(2).Enabled = True
                End If

            Next row
        Else
            Panel_SecurityFlags.Visible = True
            Prop_SecurityFlags.Items(1).Selected = True
            Prop_SecurityFlags.Items(1).Enabled = True
            Prop_SecurityFlags.Items(2).Selected = True
            Prop_SecurityFlags.Items(2).Enabled = True
            Prop_SecurityFlags.Items(0).Selected = True
        End If



    End Sub
End Class