﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:2.0.50727.4971
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On



Partial Public Class SEARCH_TOPO

    '''<summary>
    '''UpdatePanelLocation control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdatePanelLocation As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''ListDocLocation control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ListDocLocation As Global.WebApplication.ListDoc

    '''<summary>
    '''UpdatePanelRESULT control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdatePanelRESULT As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''MenuRESULT control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents MenuRESULT As Global.Equipage.WebUI.eqMenu

    '''<summary>
    '''ButtonDeattach control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ButtonDeattach As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''PlaceHolderRESULT control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PlaceHolderRESULT As Global.System.Web.UI.WebControls.PlaceHolder
End Class
