﻿Public Partial Class LOCATION
    Inherits BaseDoc

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Request.QueryString("ParentID") IsNot Nothing Then
                ISN_HIGH_LOCATION.DocID = New Guid(Request.QueryString("ParentID"))
            End If
            If tblARCHIVE_STORAGE.Rows.Count = 0 Then
                tblARCHIVE_STORAGE.InsertRow()
            End If
        End If
    End Sub

    Private Sub clearISN_HIGH_LOCATION_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles clearISN_HIGH_LOCATION.Click
        ISN_HIGH_LOCATION.DocID = Guid.Empty
    End Sub

End Class