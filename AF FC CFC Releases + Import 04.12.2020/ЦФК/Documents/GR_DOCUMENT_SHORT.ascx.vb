﻿Public Partial Class GR_DOCUMENT_SHORT
    Inherits BaseDoc
    Const STORAGE_GUID = "84E12153-532F-46AD-A0B7-0541450978E5"

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender

        HyperLink_DOC_IMAGE.Enabled = DocStateInfo.StateID <> Guid.Empty
        HyperLink_DOC_IMAGE.NavigateUrl = BasePage.GetDocURL("GR_DOC_IMAGE.ascx", DocID, False)

    End Sub

#Region "Doc Events"
    Public Overrides Function NewDoc() As Boolean
        NewDoc = MyBase.NewDoc()
        If NewDoc Then

            'надо установить архив
            Dim archTable = AppSettings.GetDataTable(New SqlClient.SqlCommand("SELECT ID, OKPO, FULL_SUBJ, ARCHIVE_LEVEL FROM GR_vARCHIVE_SUBJECT"))
            If archTable.Rows.Count = 1 Then
                Debug.Print(archTable.Rows(0).Item("ID").ToString)
                'а тут по умолчанию всегда одна строка
                Dim row1 = GR_tblSTORE_PLACE.InsertRow()
                For Each r In GR_tblSTORE_PLACE.Rows
                    DirectCast(r.FindControl("ID_ARCHIVE"), eqDocument).DocID = archTable.Rows(0).Item("ID")
                    DirectCast(r.FindControl("OKPO"), TextBox).Text = archTable.Rows(0).Item("OKPO")
                    DirectCast(r.FindControl("SUBJECT"), TextBox).Text = archTable.Rows(0).Item("FULL_SUBJ")
                    DirectCast(r.FindControl("ARCHIVE_LEVEL"), RadioButtonList).Text = archTable.Rows(0).Item("ARCHIVE_LEVEL")
                Next

            End If
            ID_OWNER.Text = "00000000-0200-0000-0000-000000000142"
        End If

        SYS_STATUS.Text = "Новый полностью"
        'видна только одна строка
        Dim row = GR_vDOC_DESCRIPT.InsertRow()
        '' '' ''GR_tblDOC_SIZE.InsertRow()
        '' '' ''GR_tblPRECIOUS_STONES.InsertRow()
        '' '' ''GR_tblRESTORATION.InsertRow()
        ' SetFilterDocDescript(-1)

        'GR_tblPRECIOUS_METAL.Enabled = False
        GR_tblPRECIOUS_STONES.Enabled = False
        GR_tblRESTORATION.Enabled = False
        GR_tblDOC_SIZE.Enabled = False


        [PUBLIC].Checked = True
        NUM.Enabled = False
        DOC_DATE.Enabled = False
    End Function
    Public Overrides Function SaveDoc() As Boolean

        If SYS_STATUS.Text = "Новый" Then
            SYS_STATUS.Text = "Новый сохранение"
        End If
        Try


            For Each r In GR_vDOC_DESCRIPT.Rows
                'тут только одна строка . Т.е. при сохранении не фильтруем, строка и так одна
                'тут происходит ошибка при первом сохранении листа описания
                If Len(DirectCast(r.FindControl("ID_VOLUME"), eqTextBox).Text) = 0 Then
                    DirectCast(r.FindControl("ID_VOLUME"), eqTextBox).Text = "00000000-0000-0000-0000-000000000001"
                End If
                DirectCast(r.FindControl("ART_DESIGN"), eqTextBox).Text = ART_DESIGN_Fict.Text
                DirectCast(r.FindControl("NUM"), eqTextBox).Text = NUM_Fict.Text
                DirectCast(r.FindControl("DESCRIPT_DATE"), eqTextBox).Text = DOC_DATE_Fict.Text
            Next

          
           
            'данные по архиву проставить, если изменялся
            'проставляем значения для Архива
            'а тут по умолчанию всегда одна строка
            For Each r In GR_tblSTORE_PLACE.Rows
                'нашли код архива и сделаем еще одну выборку
                Dim archTable1 = AppSettings.GetDataTable(New SqlClient.SqlCommand("SELECT ID, ISNULL(OKPO,'') AS OKPO, isnull(FULL_SUBJ,'') AS FULL_SUBJ, isnull(ARCHIVE_LEVEL,'a') AS ARCHIVE_LEVEL FROM GR_vARCHIVE_SUBJECT WHERE ID='" & DirectCast(r.FindControl("ID_ARCHIVE"), eqDocument).DocID.ToString & "'"))
                DirectCast(r.FindControl("OKPO"), TextBox).Text = archTable1.Rows(0).Item("OKPO")
                DirectCast(r.FindControl("SUBJECT"), TextBox).Text = archTable1.Rows(0).Item("FULL_SUBJ")
                DirectCast(r.FindControl("ARCHIVE_LEVEL"), RadioButtonList).Text = archTable1.Rows(0).Item("ARCHIVE_LEVEL")
            Next

            'проставим в спецификации значения из фиктивных полей


            MyBase.SaveDoc()
            'GR_tblPRECIOUS_METAL.Enabled = True
            GR_tblPRECIOUS_STONES.Enabled = True
            GR_tblRESTORATION.Enabled = True
            GR_tblDOC_SIZE.Enabled = True


            ' If Len(NUM.Text) = 0 Then
            'проставить дату и номер
            Dim descTable = AppSettings.GetDataTable(New SqlClient.SqlCommand("SELECT TOP 1 NUM  FROM GR_vDOC_DESCRIPT WHERE DocID='" & DocID.ToString & "' ORDER BY DESCRIPT_DATE"))
            If descTable.Rows.Count > 0 Then
                NUM.Text = descTable.Rows(0).Item("NUM")

                Dim descTable1 = AppSettings.GetDataTable(New SqlClient.SqlCommand("SELECT TOP 1 DESCRIPT_DATE  FROM GR_vDOC_DESCRIPT WHERE DocID='" & DocID.ToString & "' ORDER BY DESCRIPT_DATE"))
                DOC_DATE.Text = descTable1.Rows(0).Item("DESCRIPT_DATE")
                MyBase.SaveDoc()
            End If
            ' End If

            If SYS_STATUS.Text = "Новый сохранение" Then
                SetFilterDocDescript(0, GetLastIDDocDescript())
                ID_DOC_DESCRIPT_CURRENT.Text = GetLastIDDocDescript()
            End If

            If SYS_STATUS.Text = "Новый полностью" Then
                ' ''SetFilterDocDescript(0, GetLastIDDocDescript())
                ' ''ID_DOC_DESCRIPT_CURRENT.Text = GetLastIDDocDescript()
            End If

            For Each r In GR_tblDOC_SIZE.Rows
                DirectCast(r.FindControl("DocID"), eqTextBox).Text = DocID.ToString
                DirectCast(r.FindControl("ID_DOC_DESCRIPT"), eqTextBox).Text = ID_DOC_DESCRIPT_CURRENT.Text
                DirectCast(r.FindControl("RowID"), eqTextBox).Text = "1"
            Next

            For Each r In GR_tblRESTORATION.Rows
                DirectCast(r.FindControl("DocID"), eqTextBox).Text = DocID.ToString
                DirectCast(r.FindControl("ID_DOC_DESCRIPT"), eqTextBox).Text = ID_DOC_DESCRIPT_CURRENT.Text
                DirectCast(r.FindControl("RowID"), eqTextBox).Text = "1"
            Next

            For Each r In GR_tblPRECIOUS_STONES.Rows
                DirectCast(r.FindControl("DocID"), eqTextBox).Text = DocID.ToString
                DirectCast(r.FindControl("ID_DOC_DESCRIPT"), eqTextBox).Text = ID_DOC_DESCRIPT_CURRENT.Text
                DirectCast(r.FindControl("RowID"), eqTextBox).Text = "1"
            Next
            'проставим GUID для doc_description
            For Each r In GR_vDOC_DESCRIPT.Rows
                'тут только одна строка . Т.е. при сохранении не фильтруем, строка и так одна
                'тут происходит ошибка при первом сохранении листа описания
            Next
            SYS_STATUS.Text = "Сохранен"


            NUM.Enabled = False
            DOC_DATE.Enabled = False

        Catch ex As Exception
            If InStr(1, ex.Message, "FindControl") Then
                SYS_STATUS.Text = "Сохранен"

            Else
                AddMessage(ex.Message)
            End If
        End Try
        LoadDoc(DocID)
        ' Return MyBase.SaveDoc 'перенесла в начало
        Return True
    End Function


    Public Overrides Function LoadDoc(ByVal DocID As System.Guid) As Boolean
        LoadDoc = MyBase.LoadDoc(DocID)
        'сюда попадаем из списка или после сохранения документа
        Try
            'открыть заново

            Dim LastDescript As String

            If Len(SYS_STATUS.Text) = 0 Or SYS_STATUS.Text = "Сохранен" Then
                LastDescript = GetLastIDDocDescript()
                SetFilterDocDescript(0, LastDescript)
                ID_DOC_DESCRIPT_CURRENT.Text = LastDescript
            End If


            NUM.Enabled = False
            DOC_DATE.Enabled = False

            'проставляем значения для Архива
            'а тут по умолчанию всегда одна строка
            For Each r In GR_tblSTORE_PLACE.Rows
                'нашли код архива и сделаем еще одну выборку
                Dim archTable1 = AppSettings.GetDataTable(New SqlClient.SqlCommand("SELECT ID, ISNULL(OKPO,'') AS OKPO, isnull(FULL_SUBJ,'') AS FULL_SUBJ, isnull(ARCHIVE_LEVEL,'a') AS ARCHIVE_LEVEL FROM GR_vARCHIVE_SUBJECT WHERE ID='" & DirectCast(r.FindControl("ID_ARCHIVE"), eqDocument).DocID.ToString & "'"))
                DirectCast(r.FindControl("OKPO"), TextBox).Text = archTable1.Rows(0).Item("OKPO")
                DirectCast(r.FindControl("SUBJECT"), TextBox).Text = archTable1.Rows(0).Item("FULL_SUBJ")
                DirectCast(r.FindControl("ARCHIVE_LEVEL"), RadioButtonList).Text = archTable1.Rows(0).Item("ARCHIVE_LEVEL")
            Next

            For Each r In GR_vDOC_DESCRIPT.Rows
                NUM_Fict.Text = DirectCast(r.FindControl("NUM"), TextBox).Text
                DOC_DATE_Fict.Text = DirectCast(r.FindControl("DESCRIPT_DATE"), TextBox).Text
                ART_DESIGN_Fict.Text = DirectCast(r.FindControl("ART_DESIGN"), TextBox).Text
            Next


            If SYS_STATUS.Text = "Новый сохранение" Then
            Else
                SYS_STATUS.Text = "Загружен"
            End If


        Catch ex As Exception
            AddMessage(ex.Message)
        End Try
    End Function


    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        RegisterSpecificationAsInline(GR_tblSTORE_PLACE)
        RegisterSpecificationAsInline(GR_tblOFFICIAL_INFO)
        RegisterSpecificationAsInline(GR_vDOC_DESCRIPT)
        RegisterSpecificationAsInline(GR_tblSEAL)
        RegisterSpecificationAsInline(GR_tblPALEO_FEATURES)


        RegisterSpecificationAsInline(GR_tblPHYSICAL_STATE)


        ' RegisterSpecificationAsInline(GR_tblDOC_SIZE)
        GR_tblDOC_SIZE.AllowDelete = False
        GR_tblDOC_SIZE.AllowInsert = False
        GR_tblDOC_SIZE.AllowReorder = False
        GR_tblDOC_SIZE.ShowNumering = False
        GR_tblDOC_SIZE.ShowHeader = False
        GR_tblDOC_SIZE.ShowFooter = False

        'RegisterSpecificationAsInline(GR_tblPRECIOUS_STONES)
        GR_tblPRECIOUS_STONES.AllowDelete = False
        GR_tblPRECIOUS_STONES.AllowInsert = False
        GR_tblPRECIOUS_STONES.AllowReorder = False
        GR_tblPRECIOUS_STONES.ShowNumering = False
        GR_tblPRECIOUS_STONES.ShowHeader = False
        GR_tblPRECIOUS_STONES.ShowFooter = False

        ' RegisterSpecificationAsInline(GR_tblRESTORATION)
        GR_tblRESTORATION.AllowDelete = False
        GR_tblRESTORATION.AllowInsert = False
        GR_tblRESTORATION.AllowReorder = False
        GR_tblRESTORATION.ShowNumering = False
        GR_tblRESTORATION.ShowHeader = False
        GR_tblRESTORATION.ShowFooter = False

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        NUM.Enabled = False
        DOC_DATE.Enabled = False
    End Sub


    

    Public Sub AddDocDescription()
        'надо оставить только одну строку
        ' GR_tblPRECIOUS_METAL.Enabled = False
        GR_tblPRECIOUS_STONES.Enabled = False
        GR_tblRESTORATION.Enabled = False
        GR_tblDOC_SIZE.Enabled = False

        SetFilterDocDescript(0)

        Dim row = GR_vDOC_DESCRIPT.InsertRow()

        DirectCast(row.FindControl("DocID"), TextBox).Text = DocID.ToString
        DirectCast(row.FindControl("RowID"), TextBox).Text = GetLastRowIDDocDescript()

        ID_DOC_DESCRIPT_CURRENT.Text = Nothing
        SYS_STATUS.Text = "Новый"

    End Sub

    Public Sub OpenDocDescription()
        '  BasePage.OpenModalDialogWindow(Me, BasePage.GetListDocURL("GR_DOC_DESCRIPT_LIST.ascx", False, "DocID=" & DocID.ToString), "SelectDoc")
        BasePage.OpenModalDialogWindow(Me, BasePage.GetListDocURL("GR_DOC_DESCRIPT_LIST.ascx", False, "GR_DOCUMENT=" & DocID.ToString), "SelectDoc")
    End Sub

    Public Sub DeleteDocDescription()
        Dim FirstDescript As String
        Dim CurrentDescript As String
        'проверяем, если первый лист, то нельзя удалять 
        'если последующие, то открываем первый и удаляем выбранный
        FirstDescript = GetFirstIDDocDescript()
        CurrentDescript = ID_DOC_DESCRIPT_CURRENT.Text

        If FirstDescript = ID_DOC_DESCRIPT_CURRENT.Text Then
            'нельзя удалять
            AddMessage("Открытый лист описания является первым, его удалить нельзя. Можно только редактировать.")
        Else
            ''переоткрываем на первый документ и удаляем
            SetFilterDocDescript(0, FirstDescript)

            'удаление
            DeleteDocDescript(CurrentDescript)
            AddMessage("Удалено.")
        End If


    End Sub

    Private Sub Page_ModalDialogClosed(ByVal Argument As Equipage.WebUI.eqModalDialogArgument) Handles Me.ModalDialogClosed

        SetFilterDocDescript(0, Argument.ArgumentObject.ToString)
        ID_DOC_DESCRIPT_CURRENT.Text = Argument.ArgumentObject.ToString
        SYS_STATUS.Text = "Загружен"

    End Sub



    Public Sub ISN_VOLUME_SelectDoc(ByVal sender As Object, ByVal e As Equipage.WebUI.eqBaseContainer.SelectDocEventArgs)
        Dim a = e.DocID
    End Sub



    'Private Sub GR_tblPRECIOUS_METAL_RowInserted(ByVal SpecificationRow As Equipage.WebUI.eqSpecificationRow) Handles GR_tblPRECIOUS_METAL.RowInserted
    '    DirectCast(SpecificationRow.FindControl("DocID"), TextBox).Text = DocID.ToString
    '    DirectCast(SpecificationRow.FindControl("ID_DOC_DESCRIPT"), TextBox).Text = ID_DOC_DESCRIPT_CURRENT.Text
    '    DirectCast(SpecificationRow.FindControl("RowID"), TextBox).Text = "1"
    'End Sub


    '' ''Private Sub GR_tblPRECIOUS_STONES_RowInserted(ByVal SpecificationRow As Equipage.WebUI.eqSpecificationRow) Handles GR_tblPRECIOUS_STONES.RowInserted
    '' ''    DirectCast(SpecificationRow.FindControl("DocID"), eqTextBox).Text = DocID.ToString
    '' ''    DirectCast(SpecificationRow.FindControl("ID_DOC_DESCRIPT"), eqTextBox).Text = ID_DOC_DESCRIPT_CURRENT.Text
    '' ''    DirectCast(SpecificationRow.FindControl("RowID"), eqTextBox).Text = "1"
    '' ''End Sub

    '' ''Private Sub GR_tblRESTORATION_RowInserted(ByVal SpecificationRow As Equipage.WebUI.eqSpecificationRow) Handles GR_tblRESTORATION.RowInserted
    '' ''    DirectCast(SpecificationRow.FindControl("DocID"), eqTextBox).Text = DocID.ToString
    '' ''    DirectCast(SpecificationRow.FindControl("ID_DOC_DESCRIPT"), eqTextBox).Text = ID_DOC_DESCRIPT_CURRENT.Text
    '' ''    DirectCast(SpecificationRow.FindControl("RowID"), eqTextBox).Text = "1"
    '' ''End Sub


    '' ''Private Sub GR_tblDOC_SIZE_RowInserted(ByVal SpecificationRow As Equipage.WebUI.eqSpecificationRow) Handles GR_tblDOC_SIZE.RowInserted

    '' ''    DirectCast(SpecificationRow.FindControl("DocID"), eqTextBox).Text = DocID.ToString
    '' ''    DirectCast(SpecificationRow.FindControl("ID_DOC_DESCRIPT"), eqTextBox).Text = ID_DOC_DESCRIPT_CURRENT.Text
    '' ''    DirectCast(SpecificationRow.FindControl("RowID"), eqTextBox).Text = "1"

    '' ''End Sub


    Public Function GetFileImageName(ByVal ID As Guid) As String

        'в поле Name храниться весь путь
        Dim cmd As New SqlClient.SqlCommand("select NAME from GR_tblDOC_IMAGE where ID = @ID")
        cmd.Parameters.AddWithValue("@ID", ID)
        Dim DataTable = AppSettings.GetDataTable(cmd)
        If DataTable.Rows.Count = 0 Then
            GetFileImageName = Nothing
        Else
            GetFileImageName = GetStorgePath() + "\" + DataTable.Rows(0)("NAME")
        End If

    End Function

    Public Sub SetFilterDocDescript(ByVal CodeDocDescript As Integer, Optional ByVal ID As String = "")
        Dim IDDocDescript As String
        If Len(ID) > 0 Then
            IDDocDescript = ID
        Else
            IDDocDescript = "00000000-0000-0000-0000-000000000000"
        End If

        If CodeDocDescript > -1 Then
            Dim a = DataSetSpecificationDataTable("GR_vDOC_DESCRIPT")
            a.DefaultView.RowFilter = "ID='" & IDDocDescript & "'"
            MyBase.PutValuesFromDataSetToSpecificationControls("GR_vDOC_DESCRIPT")

            '' ''If a.DefaultView.Count = 0 Then
            '' ''    GR_vDOC_DESCRIPT.InsertRow()
            '' ''End If

            '' ''все остальные связанные таблицы
            ' ''Dim b = DataSetSpecificationDataTable("GR_tblPRECIOUS_METAL")
            ' ''b.DefaultView.RowFilter = "ID_DOC_DESCRIPT ='" & IDDocDescript & "'"
            ' ''MyBase.PutValuesFromDataSetToSpecificationControls("GR_tblPRECIOUS_METAL")

            'драг. камни
            Dim c = DataSetSpecificationDataTable("GR_tblPRECIOUS_STONES")
            c.DefaultView.RowFilter = "ID_DOC_DESCRIPT ='" & IDDocDescript & "'"
            MyBase.PutValuesFromDataSetToSpecificationControls("GR_tblPRECIOUS_STONES")
            If c.DefaultView.Count = 0 And IDDocDescript <> "00000000-0000-0000-0000-000000000000" Then
                GR_tblPRECIOUS_STONES.InsertRow()
                For Each r In GR_tblPRECIOUS_STONES.Rows
                    DirectCast(r.FindControl("DocID"), eqTextBox).Text = DocID.ToString
                    DirectCast(r.FindControl("ID_DOC_DESCRIPT"), eqTextBox).Text = IDDocDescript
                    DirectCast(r.FindControl("RowID"), eqTextBox).Text = "1"
                Next
            End If

            'реставрация
            Dim d = DataSetSpecificationDataTable("GR_tblRESTORATION")
            d.DefaultView.RowFilter = "ID_DOC_DESCRIPT ='" & IDDocDescript & "'"
            MyBase.PutValuesFromDataSetToSpecificationControls("GR_tblRESTORATION")
            If d.DefaultView.Count = 0 And IDDocDescript <> "00000000-0000-0000-0000-000000000000" Then
                GR_tblRESTORATION.InsertRow()
                For Each r In GR_tblRESTORATION.Rows
                    DirectCast(r.FindControl("DocID"), eqTextBox).Text = DocID.ToString
                    DirectCast(r.FindControl("ID_DOC_DESCRIPT"), eqTextBox).Text = IDDocDescript
                    DirectCast(r.FindControl("RowID"), eqTextBox).Text = "1"
                Next
            End If
            'размеры
            Dim f = DataSetSpecificationDataTable("GR_tblDOC_SIZE")
            f.DefaultView.RowFilter = "ID_DOC_DESCRIPT ='" & IDDocDescript & "'"
            MyBase.PutValuesFromDataSetToSpecificationControls("GR_tblDOC_SIZE")
            If f.DefaultView.Count = 0 And IDDocDescript <> "00000000-0000-0000-0000-000000000000" Then
                GR_tblDOC_SIZE.InsertRow()
                For Each r In GR_tblDOC_SIZE.Rows
                    DirectCast(r.FindControl("DocID"), eqTextBox).Text = DocID.ToString
                    DirectCast(r.FindControl("ID_DOC_DESCRIPT"), eqTextBox).Text = IDDocDescript
                    DirectCast(r.FindControl("RowID"), eqTextBox).Text = "1"
                Next
            End If
        End If
    End Sub
    Private Function GetLastRowIDDocDescript() As Integer
        Dim cmd As New SqlClient.SqlCommand("select MAX(RowID)+1 AS RowID from GR_vDOC_DESCRIPT where DocID = @ID")
        cmd.Parameters.AddWithValue("@ID", DocID)
        Dim DataTable = AppSettings.GetDataTable(cmd)
        If DataTable.Rows.Count = 0 Then
            GetLastRowIDDocDescript = 0
        Else
            GetLastRowIDDocDescript = DataTable.Rows(0)("RowID")
        End If
        Return GetLastRowIDDocDescript
    End Function

    Private Function GetFirstIDDocDescript() As String

        Dim cmd As New SqlClient.SqlCommand("SELECT TOP 1 ID AS FIRST_ID  FROM GR_vDOC_DESCRIPT WHERE DocID=@ID ORDER BY DESCRIPT_DATE")
        cmd.Parameters.AddWithValue("@ID", DocID)
        Dim DataTable = AppSettings.GetDataTable(cmd)
        If DataTable.Rows.Count = 0 Then
            GetFirstIDDocDescript = "00000000-0000-0000-0000-000000000000"
        Else
            GetFirstIDDocDescript = DataTable.Rows(0)("FIRST_ID").ToString
        End If
        Return GetFirstIDDocDescript
    End Function
    Private Function GetLastIDDocDescript() As String

        Dim cmd As New SqlClient.SqlCommand("SELECT TOP 1 ID AS FIRST_ID  FROM GR_vDOC_DESCRIPT WHERE DocID=@ID ORDER BY DESCRIPT_DATE DESC")
        cmd.Parameters.AddWithValue("@ID", DocID)
        Dim DataTable = AppSettings.GetDataTable(cmd)
        If DataTable.Rows.Count = 0 Then
            GetLastIDDocDescript = "00000000-0000-0000-0000-000000000000"
        Else
            GetLastIDDocDescript = DataTable.Rows(0)("FIRST_ID").ToString
        End If
        Return GetLastIDDocDescript

    End Function

    Private Sub DeleteDocDescript(ByVal ID As String)

        Dim cmd As New SqlClient.SqlCommand("DELETE FROM GR_tblPRECIOUS_METAL WHERE ID_DOC_DESCRIPT=@ID")
        'cmd.Parameters.Add(New SqlClient.SqlParameter("@ISN_DOC_DESCRIPT", SqlDbType.BigInt) With {.Value = FirstDescript})
        cmd.Parameters.AddWithValue("@ID", ID)
        AppSettings.ExecCommand(cmd)

        Dim cmd1 As New SqlClient.SqlCommand("DELETE FROM GR_tblPRECIOUS_STONES WHERE ID_DOC_DESCRIPT=@ID")
        cmd1.Parameters.AddWithValue("@ID", ID)
        AppSettings.ExecCommand(cmd1)

        Dim cmd2 As New SqlClient.SqlCommand("DELETE FROM GR_tblPRECIOUS_STONES WHERE ID_DOC_DESCRIPT=@ID")
        cmd2.Parameters.AddWithValue("@ID", ID)
        AppSettings.ExecCommand(cmd2)

        Dim cmd3 As New SqlClient.SqlCommand("DELETE FROM GR_tblPRECIOUS_STONES WHERE ID_DOC_DESCRIPT=@ID")
        cmd3.Parameters.AddWithValue("@ID", ID)
        AppSettings.ExecCommand(cmd3)

        Dim cmd4 As New SqlClient.SqlCommand("DELETE FROM GR_tblDOC_DESCRIPT WHERE ID=@ID")
        cmd4.Parameters.AddWithValue("@ID", ID)
        AppSettings.ExecCommand(cmd4)
    End Sub

    Private Function GetStorgePath() As String
        Dim cmd As New SqlClient.SqlCommand("select Text from tblConstantsSpec where ID = @ID")
        cmd.Parameters.AddWithValue("@ID", New Guid(STORAGE_GUID))
        Dim DataTable = AppSettings.GetDataTable(cmd)
        If DataTable.Rows.Count = 0 Then
            GetStorgePath = "C:\Госреестр-Файлы"
        Else
            GetStorgePath = DataTable.Rows(0)("Text")
        End If
        Return GetStorgePath
    End Function
#End Region

    Private Sub NUM_Fict_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles NUM_Fict.TextChanged
        NUM.Text = NUM_Fict.Text
    End Sub
End Class