﻿Imports System.Data
Imports System.Data.SqlClient

Partial Public Class FUND
    Inherits BaseDoc

#Region "События"

    Private Sub Page_AddMessageAction(ByVal sender As Object, ByVal e As Equipage.WebUI.eqBasePage.AddMessageResult) Handles Me.AddMessageAction
        If e = eqBasePage.AddMessageResult.Yes Then
            InternalSaveDoc()
        End If
    End Sub

    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        RegisterSpecificationAsInline(tblFUND_CHECK)
    End Sub

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If DocStateInfo.StateID = Guid.Empty Then
                ISN_DOC_TYPE.SelectedIndex = 1
            End If

            UpdateSpec(tblFUND_CREATOR)
            UpdateSpec(tblFUND_INCLUSION)
            UpdateREASONS()
        End If
    End Sub

    Private Sub Page_PreRender1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        HyperLinkDEPOSITs.Enabled = DocStateInfo.StateID <> Guid.Empty
        HyperLinkDEPOSITs.NavigateUrl = BasePage.GetListDocURL("DEPOSIT.ascx", False, "FUND=" & DocID.ToString)
        HyperLinkINVENTORYs.Enabled = DocStateInfo.StateID <> Guid.Empty
        HyperLinkINVENTORYs.NavigateUrl = BasePage.GetListDocURL("INVENTORY.ascx", False, "FUND=" & DocID.ToString)
        HyperLinkUNITs.Enabled = DocStateInfo.StateID <> Guid.Empty
        HyperLinkUNITs.NavigateUrl = BasePage.GetListDocURL("UNIT.ascx", False, "FUND=" & DocID.ToString)
        HyperLinkUNIT2s.Enabled = DocStateInfo.StateID <> Guid.Empty
        HyperLinkUNIT2s.NavigateUrl = BasePage.GetListDocURL("UNIT2.ascx", False, "FUND=" & DocID.ToString)
        HyperLinkSTRs.Enabled = DocStateInfo.StateID <> Guid.Empty
        HyperLinkSTRs.NavigateUrl = BasePage.GetListDocURL("INVENTORYSTRUCTURE.ascx", False, "FUND=" & DocID.ToString)

        MultiViewTabs.ActiveViewIndex = CInt(MenuTabs.SelectedValue)

        SECURITY_CHAR.Enabled = GetLock(LockID) AndAlso ISN_SECURLEVEL.SelectedValue <> "" AndAlso "1" = ISN_SECURLEVEL.SelectedValue Mod 10000000000
        If Not (ISN_SECURLEVEL.SelectedValue <> "" AndAlso ISN_SECURLEVEL.SelectedValue Mod 10000000000 = "1") Then
            SECURITY_CHAR.SelectedValue = ""
        End If
        ISN_SECURITY_REASON.Enabled = GetLock(LockID) AndAlso (ISN_SECURLEVEL.SelectedValue <> "" AndAlso ISN_SECURLEVEL.SelectedValue Mod 10000000000 = "1") And SECURITY_CHAR.SelectedValue = "p"
        If Not ((ISN_SECURLEVEL.SelectedValue <> "" AndAlso ISN_SECURLEVEL.SelectedValue Mod 10000000000 = "1") And SECURITY_CHAR.SelectedValue = "p") Then
            ISN_SECURITY_REASON.SelectedValue = ""
        End If

        vFUND_RENAME.Enabled = GetLock(LockID) And WAS_RENAMED.Checked And (FUND_KIND.SelectedValue = "a" Or FUND_KIND.SelectedValue = "b")
        vFUND_OAF.Enabled = GetLock(LockID) And FUND_KIND.SelectedValue = "b"
        tblFUND_OAF_REASON.Enabled = GetLock(LockID) And (FUND_KIND.SelectedValue = "b" Or FUND_KIND.SelectedValue = "c")
        JOIN_REASON.Enabled = GetLock(LockID) And FUND_KIND.SelectedValue = "b"
        OAF_NOTE.Enabled = GetLock(LockID) And FUND_KIND.SelectedValue = "b"
        tblUNDOCUMENTED_PERIOD.Enabled = GetLock(LockID) And HAS_UNDOCUMENTED_PERIODS.Checked
        tblFUND_CREATOR.Enabled = GetLock(LockID) And (FUND_KIND.SelectedValue = "a" Or FUND_KIND.SelectedValue = "d")
        tblFUND_INCLUSION.Enabled = GetLock(LockID) And HAS_INCLUSIONS.Checked And (FUND_KIND.SelectedValue = "a" Or FUND_KIND.SelectedValue = "d")
        Panel_E.Enabled = GetLock(LockID) And Not (CARRIER_TYPE.SelectedValue = "T" And Not HAS_ELECTRONIC_DOCS.Checked)
        Panel_T.Enabled = GetLock(LockID) And Not (CARRIER_TYPE.SelectedValue = "E" And Not HAS_TRADITIONAL_DOCS.Checked)

        StatsDisable(vFUND_DOCUMENT_STATS_P, _
                     vFUND_DOCUMENT_STATS_A, _
                     vFUND_DOCUMENT_STATS_E, _
                     vFUND_DOCUMENT_STATS_M, _
                     vFUND_DOCUMENT_STATS, _
                     tblFUND_DOC_TYPE.Rows.Select(Function(F) DirectCast(F.FindControl("ISN_DOC_TYPE"), DropDownList).SelectedItem.Text).Where(Function(F) Not String.IsNullOrEmpty(F)).ToArray)

        ScriptManager.RegisterStartupScript(Page, Me.GetType, "UI", "_updateUI();", True)

        If vFUND_RENAME.Enabled = True Then
            UpdateFUND_RENAME()
        End If
    End Sub

    Private Sub tblFUND_DOC_TYPE_RowInserted(ByVal SpecificationRow As Equipage.WebUI.eqSpecificationRow) Handles tblFUND_DOC_TYPE.RowInserted
        If DocID = Guid.Empty Then
            DirectCast(SpecificationRow.FindControl("ISN_DOC_TYPE"), DropDownList).SelectedIndex = ISN_DOC_TYPE.SelectedValue Mod 10000000000
        End If
    End Sub

    Private Sub FUND_KIND_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FUND_KIND.SelectedIndexChanged
        UpdateSpec(tblFUND_CREATOR)
        UpdateSpec(tblFUND_INCLUSION)
        UpdateREASONS()
    End Sub

    Private Sub tblFUND_CREATOR_RowInserted(ByVal SpecificationRow As Equipage.WebUI.eqSpecificationRow) Handles tblFUND_CREATOR.RowInserted
        UpdateSpec(tblFUND_CREATOR)
    End Sub

    Private Sub tblFUND_INCLUSION_RowInserted(ByVal SpecificationRow As Equipage.WebUI.eqSpecificationRow) Handles tblFUND_INCLUSION.RowInserted
        UpdateSpec(tblFUND_INCLUSION)
    End Sub

    Public Sub ISN_CHILD_FUND_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ISN_CHILD_FUND = DirectCast(sender, eqDocument)
        Dim SpecificationRow = DirectCast(ISN_CHILD_FUND.NamingContainer, eqSpecificationRow)
        Dim HasChildFund = ISN_CHILD_FUND.DocID <> Guid.Empty

        For Each cell As Control In SpecificationRow.Controls
            For Each ctl As Control In cell.Controls
                If TypeOf ctl Is eqTextBox Then
                    DirectCast(ctl, eqTextBox).Visible = Not HasChildFund
                ElseIf TypeOf ctl Is DropDownList Then
                    DirectCast(ctl, DropDownList).Visible = Not HasChildFund
                ElseIf TypeOf ctl Is Label Then
                    If ctl.ID = "WARNING" Then
                        If IsDBNull(DataSetSpecificationDataRow(SpecificationRow.Specification.ID, SpecificationRow.RowGuid)("ISN_CHILD_FUND")) Then
                            DirectCast(ctl, Label).Text = String.Format("Внимание! Все описи, россыпи и акты добавленного фонда № {0} будут добавлены к объединенному архивному фонду. Отмена операции будет невозможна.", ISN_CHILD_FUND.DataSetHeaderDataRow("FUND_NUM"))
                            DirectCast(ctl, Label).Visible = HasChildFund
                        End If
                    Else
                        DirectCast(ctl, Label).Visible = HasChildFund
                    End If
                End If
            Next
        Next
    End Sub

    Private Sub ISN_DOC_TYPE_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ISN_DOC_TYPE.TextChanged
        Select Case ISN_DOC_TYPE.Text
            Case 1, 2, 3
                CARRIER_TYPE.SelectedValue = "T"
        End Select
    End Sub

#End Region

#Region "Методы"

    Private Function InternalSaveDoc() As Boolean
        If Not CheckYear(DOC_START_YEAR, DOC_END_YEAR) Then
            Return False
        End If

        For Each Row In tblUNDOCUMENTED_PERIOD.Rows
            If Not CheckYear(Row.FindControl("PERIOD_START_YEAR"), Row.FindControl("PERIOD_END_YEAR")) Then
                Return False
            End If
        Next

        For Each Row In tblFUND_INCLUSION.Rows
            If Not CheckYear(Row.FindControl("START_YEAR"), Row.FindControl("END_YEAR")) Then
                Return False
            End If
        Next

        Dim RenameCount = 0
        Dim GuidOfSelected = Guid.Empty
        For Each Row In vFUND_RENAME.Rows
            Dim EnableDataRow = (Function(F) F("ID") = Row.RowGuid)
            Dim EnableControl = DirectCast(Row.FindControl("NAME_SAVED"), CheckBox)
            If EnableControl.Checked = True Then
                RenameCount = RenameCount + 1
                GuidOfSelected = Row.RowGuid
            End If
        Next
        If RenameCount > 1 Then
            AddMessage("В поле 'Название сохранено' выбрано более двух записей. Для корректного сохранение необходимо выбрать не более одной записи.")
            Return False
        ElseIf RenameCount = 1 And GuidOfSelected <> Guid.Empty Then
            FUND_NAME_FULL.Text = DirectCast(vFUND_RENAME.Row(GuidOfSelected).FindControl("FUND_NAME_FULL"), eqTextBox).Text
            FUND_NAME_SHORT.Text = DirectCast(vFUND_RENAME.Row(GuidOfSelected).FindControl("FUND_NAME_SHORT"), eqTextBox).Text
            If DirectCast(vFUND_RENAME.Row(GuidOfSelected).FindControl("DELETE_DATE_FMT"), TextBox).Text <> "" Then
                DirectCast(vFUND_RENAME.Row(GuidOfSelected).FindControl("NAME_SAVED"), CheckBox).Checked = False
            End If
        End If

        If tblFUND_CHECK.Rows.Count > 0 And vFUND_DOCUMENT_STATS.Rows.Count > 0 Then
            Dim valCARDBOARDED = DirectCast(tblFUND_CHECK.Rows(0).FindControl("CARDBOARDED"), eqTextBox).Text
            Dim valUNIT_COUNT = DirectCast(vFUND_DOCUMENT_STATS.Rows(0).FindControl("UNIT_COUNT"), eqTextBox).Text

            If Val(valCARDBOARDED) > Val(valUNIT_COUNT) Then
                AddMessage("Количество единиц хранения, указываемых в полях вкладки Физическое состояние, не должно превышать общее количество единиц хранения в фонде")
                Return False
            End If
        End If

        If KEEP_PERIOD.SelectedValue = "b" Then
            Dim Flag = False

            If [PROPERTY].SelectedValue = "d" Or [PROPERTY].SelectedValue = "e" Then
                Flag = True
            End If
            If [PROPERTY].SelectedValue = "c" And ISN_DOC_TYPE.SelectedValue = "2" Then
                Flag = True
            End If
            If Not Flag Then
                AddMessage("Срок хранения может принимать значение 'временно', только если поле Собственность принимает значение 'физического лица' или 'юридического лица' а также 'муниципальная' (фонд имеет тип документов 'документы по личному составу')")
                Return False
            End If
        End If

        If ISN_PERIOD.SelectedValue = "1" Then
            If FUND_NUM_1.Text <> "" Then
                AddMessage("Первая часть номера фонда не заполняется для исторического периода 'дореволюционный'")
                Return False
            End If
        End If

        If ISN_DOC_TYPE.SelectedValue = "2" Then
            If ISN_SECURITY_REASON.SelectedValue = "1" Then
                AddMessage("Причина ограничения доступа не может быть 'Тайна л/ж', если Тип фонда 'Документы по личному составу'")
                Return False
            End If
        End If

        If ISN_SECURLEVEL.SelectedValue = "1" Then
            If SECURITY_CHAR.SelectedValue = "" Then
                SetControlAlertStyle(DocType.GetFieldInfo(DocType.GetHeaderTableName, SECURITY_CHAR.ID), True)
                AddMessage("Поле: Доступ -> Обязательно для заполнения")
                Return False
            End If
        End If

        Dim NumCheckSQL = <sql>
                  Select FUND_NUM_1,FUND_NUM_2,FUND_NUM_3,Presence_Flag,Absence_Reason
                    From tblFund
                    Where ID != @ID
                    And ((FUND_NUM_1 = @NUM1) or ((FUND_NUM_1 IS NULL) AND @NUM1=''))
                    And ((FUND_NUM_2 = @NUM2) or (((FUND_NUM_2 IS NULL) OR FUND_NUM_2='' OR FUND_NUM_2='0') AND (@NUM2='' OR @NUM2='0')))
                    And ((FUND_NUM_3 = @NUM3) or((FUND_NUM_3 IS NULL) AND @NUM3=''))
                    And (Presence_Flag != 'b' Or ABSENCE_REASON != 'd')
                    And Deleted = 0
              </sql>
        Dim NumCheckCommand As New SqlClient.SqlCommand(NumCheckSQL.Value)
        NumCheckCommand.Parameters.AddWithValue("@ID", DocID)
        NumCheckCommand.Parameters.AddWithValue("@NUM1", FUND_NUM_1.Text)
        NumCheckCommand.Parameters.AddWithValue("@NUM2", FUND_NUM_2.Text)
        NumCheckCommand.Parameters.AddWithValue("@NUM3", FUND_NUM_3.Text)
        Dim NumCheck = AppSettings.GetDataTable(NumCheckCommand)
        If NumCheck.Rows.Count <> 0 Then
            If PRESENCE_FLAG.Text <> "b" Or ABSENCE_REASON.Text <> "d" Then
                AddMessage("Фонд с таким номером уже существует")
                Return False
            End If
        End If

        If PRESENCE_FLAG.Text = "b" Then
            Dim cmd As New SqlCommand(<sql>
UPDATE tblUNIT
SET IS_LOST = 'Y'
WHERE 1=1 --and Deleted = 0
 AND ID IN (
	SELECT ID
	FROM tblUNIT
	WHERE ISN_INVENTORY IN (
		SELECT ISN_INVENTORY
		FROM tblINVENTORY
		WHERE ISN_FUND = (
			SELECT ISN_FUND
			FROM tblFUND
			WHERE ID = @ID
		)
	)
)
                                      </sql>)
            cmd.Parameters.AddWithValue("@ID", DocID)
            AppSettings.ExecCommand(cmd)

        End If



        If FUND_KIND.Text = "b" Then
            UpdateOAF()
        End If

        Return MyBase.SaveDoc()
    End Function

    Private Function CheckYear(ByVal Start As eqTextBox, ByVal Finish As eqTextBox) As Boolean
        Dim StartYear = Nothing
        Dim FinishYear = Nothing

        If Not Int16.TryParse(Start.Text, StartYear) Then
            AddMessage(String.Format("Начальная дата '{0}' указана неверно", Start.Text))
            Return False
        End If

        If Not Int16.TryParse(Finish.Text, FinishYear) Then
            AddMessage(String.Format("Конечная дата '{0}' указана неверно", Finish.Text))
            Return False
        End If

        If StartYear > FinishYear Then
            AddMessage(String.Format("Начальная дата '{0}' должна быть не больше конечной даты '{1}'", Start.Text, Finish.Text))
            Return False
        End If

        Return True
    End Function

    Private Sub UpdateSpec(ByVal specification As eqSpecification)
        Dim Index = -1

        Select Case FUND_KIND.SelectedValue
            Case "a"
                Index = 1
            Case "d"
                Index = 0
            Case Else
                Exit Sub
        End Select

        For Each row In specification.Rows
            DirectCast(row.FindControl("FundOrganiz"), MultiView).ActiveViewIndex = Index
        Next
    End Sub

    Private Sub UpdateREASONS()
        Select Case FUND_KIND.SelectedValue
            Case "b"
                Reasons.ActiveViewIndex = 0
            Case "c"
                Reasons.ActiveViewIndex = 1
            Case Else
                Reasons.ActiveViewIndex = -1
        End Select
    End Sub

    Private Sub UpdateFUND_RENAME()
        Dim SQL = <sql>
                        select *
                        from vFUND_RENAME
                        where DocID = @ID
                  </sql>
        Dim EnableCommand As New SqlCommand(SQL.Value)
        EnableCommand.Parameters.AddWithValue("@ID", DocID)
        Dim EnableTable = AppSettings.GetDataTable(EnableCommand)
        If EnableTable.Rows.Count > 0 Then
            For Each row In vFUND_RENAME.Rows
                Dim EnableDataRow = EnableTable.AsEnumerable.SingleOrDefault(Function(F) F("ID") = row.RowGuid)
                Dim EnableControl = DirectCast(row.FindControl("NAME_SAVED"), CheckBox)
                If DirectCast(row.FindControl("DELETE_DATE_FMT"), TextBox).Text = "" Then
                    EnableControl.Enabled = True
                Else
                    EnableControl.Enabled = False
                End If
            Next
        End If
    End Sub

    Private Sub UpdateOAF()
        For Each row In vFUND_OAF.Rows
            Dim ISN_CHILD_FUND = DirectCast(row.FindControl("ISN_CHILD_FUND"), WebApplication.WebUI.eqDocument)

            Dim SelectSQL = <sql>
                                    select F.ID
                                    from tblFUND F  
                                    where F.ID = @ID AND F.Deleted = 0 and (F.PRESENCE_FLAG != 'b' or F.ABSENCE_REASON != 'b' or F.ISN_OAF != @ISN_FUND)  
                            </sql>
            Dim SelectCommand As New SqlCommand(SelectSQL.Value)
            SelectCommand.Parameters.AddWithValue("@ID", ISN_CHILD_FUND.DocID)
            SelectCommand.Parameters.AddWithValue("@ISN_FUND", DataSetHeaderDataRow("ISN_FUND"))

            Dim SelectDataTable = AppSettings.GetDataTable(SelectCommand)
            If SelectDataTable.Rows.Count > 0 Then
                Dim UpdateSQL = <sql>
                                    update tblFUND
                                    set PRESENCE_FLAG = 'b', ABSENCE_REASON = 'b', ISN_OAF = @ISN_FUND
                                    where ID = @ID

                                    update tblINVENTORY
                                    set ISN_FUND = @ISN_FUND
                                    where ISN_FUND = @ISN_OAF and Deleted = 0

                                    update tblACT
                                    set ISN_FUND = @ISN_FUND
                                    where ISN_FUND = @ISN_OAF and Deleted = 0

                                    update tblDEPOSIT
                                    set ISN_FUND = @ISN_FUND
                                    where ISN_FUND = @ISN_OAF and Deleted = 0
                                </sql>
                Dim UpdateComand As New SqlCommand(UpdateSQL.Value)
                UpdateComand.Parameters.AddWithValue("@ID", ISN_CHILD_FUND.DocID)
                UpdateComand.Parameters.AddWithValue("@ISN_OAF", ISN_CHILD_FUND.KeyValue)
                UpdateComand.Parameters.AddWithValue("@ISN_FUND", DataSetHeaderDataRow("ISN_FUND"))

                Dim MyTransaction = Not AppSettings.HasTransaction
                Try
                    AppSettings.BeginTransaction()

                    UpdateComand.Connection = AppSettings.Cnn
                    UpdateComand.Transaction = AppSettings.Trn
                    UpdateComand.ExecuteNonQuery()

                    If MyTransaction Then
                        AppSettings.CommitTransaction()
                    End If
                Catch ex As Exception
                    If MyTransaction Then
                        AppSettings.RollbackTransaction()
                    End If
                    Throw ex
                End Try
            End If
        Next
    End Sub

    Public Sub CalcOneFund()
        If GetLock(LockID) AndAlso Not LockCommand("Читатели") AndAlso Not ForbidRecalc.Checked Then
            BasePage.OpenModalDialogWindow(Me, "RecalcFund.aspx?DocTypeURL=" & DocTypeURL, "One", DataSetHeaderDataRow("ID"))
        End If
    End Sub

    Public Sub AddAct()
        If DocID <> Guid.Empty Then
            BasePage.OpenModalDialogWindow(Me, BasePage.GetNewDocURL("ACT.ascx", True), "FUND.ascx", DataSetHeaderDataRow("ISN_FUND"))
        End If
    End Sub

#End Region

#Region "Документ"

    Public Overrides Function NewDoc() As Boolean
        NewDoc = MyBase.NewDoc()
        If NewDoc Then
            If Request.QueryString("ARCHIVE") IsNot Nothing Then
                Crumbs.ISN_ARCHIVE.DocID = New Guid(Request.QueryString("ARCHIVE"))
            Else
                Dim dt = AppSettings.GetDataTable(New SqlCommand("SELECT ID FROM tblARCHIVE WHERE Deleted=0"))
                If dt.Rows.Count = 1 Then
                    Crumbs.ISN_ARCHIVE.DocID = DirectCast(dt.Rows(0).Item("ID"), Guid)
                End If
            End If
        End If
    End Function

    Public Overrides Function SaveDoc() As Boolean
        Dim ISN_CHILD_FUND As WebApplication.WebUI.eqDocument = Nothing
        Dim FUND_OAF_Messages As New List(Of String)

        For Each Row In vFUND_OAF.Rows
            ISN_CHILD_FUND = DirectCast(Row.FindControl("ISN_CHILD_FUND"), WebApplication.WebUI.eqDocument)
            If ISN_CHILD_FUND IsNot Nothing AndAlso ISN_CHILD_FUND.DocID <> Guid.Empty Then
                FUND_OAF_Messages.Add(String.Format("Внимание! Все описи, россыпи и акты добавленного фонда № {0} будут добавлены к объединенному архивному фонду. Отмена операции будет невозможна.", ISN_CHILD_FUND.DataSetHeaderDataRow("FUND_NUM")))
            End If
        Next

        If FUND_OAF_Messages.Count > 0 Then
            AddMessages(FUND_OAF_Messages, "Вы уверены в необходимости операции?", eqBasePage.AddMessageButtons.YesNo)
            Return False
        Else
            Return InternalSaveDoc()
        End If
    End Function

    Public Overrides Function DeleteDoc() As Boolean
        Dim DeleteRecursivelyCommand As New SqlClient.SqlCommand("dbo.DeleteRecursively") With {.CommandType = CommandType.StoredProcedure}
        DeleteRecursivelyCommand.Parameters.AddWithValue("@ISN", DataSetHeaderDataRow("ISN_FUND"))
        DeleteRecursivelyCommand.Parameters.AddWithValue("@OBJ", "fund")
        AppSettings.ExecCommand(DeleteRecursivelyCommand)

        setDocStatesLogDeletedRecursively("Удален")

        Return MyBase.DeleteDoc()
    End Function

#End Region

#Region "Контролы"

    Public Overrides Sub PutValueToControl(ByVal Value As Object, ByVal TargetControl As System.Web.UI.Control, Optional ByVal FieldFormat As String = "")
        If TypeOf TargetControl Is CheckBox Then
            DirectCast(TargetControl, CheckBox).Checked = (Value Is Nothing OrElse Value.ToString.Trim.ToUpper = "Y")
        Else
            MyBase.PutValueToControl(Value, TargetControl, FieldFormat)
        End If
    End Sub

    Public Overrides Function GetValueFromControl(ByVal SourceControl As System.Web.UI.Control) As Object
        If TypeOf SourceControl Is CheckBox Then
            Return IIf(DirectCast(SourceControl, CheckBox).Checked, "Y", "N")
        Else
            Return MyBase.GetValueFromControl(SourceControl)
        End If
    End Function

    Public Overrides Function ReadValuesFromSpecificationControlsToDataSet(ByVal SpecificationTableName As String) As Boolean
        Return MyBase.ReadValuesFromSpecificationControlsToDataSet(SpecificationTableName)
    End Function

    Public Overrides Function ReadValueFromSpecificationRowControlToDataSet(ByVal SpecificationRow As Equipage.WebUI.eqSpecificationRow, ByVal FieldName As String) As Boolean
        Dim f As eqTextBox, d As Date, v As String
        If SpecificationRow.Specification.ID = "vFUND_RENAME" And FieldName = "CREATE_DATE_FMT" Then
            f = DirectCast(SpecificationRow.FindControl("CREATE_DATE_FMT"), eqTextBox)
            If DateTime.TryParse(f.Text, d) Then
                v = d.ToString("yyyyMMdd")
            Else
                v = f.Text
            End If
            DataSetSpecificationDataRow(SpecificationRow.Specification.ID, SpecificationRow.RowGuid)("CREATE_DATE") = v
        ElseIf SpecificationRow.Specification.ID = "vFUND_RENAME" And FieldName = "DELETE_DATE_FMT" Then
            f = DirectCast(SpecificationRow.FindControl("DELETE_DATE_FMT"), eqTextBox)
            If DateTime.TryParse(f.Text, d) Then
                v = d.ToString("yyyyMMdd")
            Else
                v = f.Text
            End If
            DataSetSpecificationDataRow(SpecificationRow.Specification.ID, SpecificationRow.RowGuid)("DELETE_DATE") = v
        End If
        Return MyBase.ReadValueFromSpecificationRowControlToDataSet(SpecificationRow, FieldName)
    End Function

#End Region

#Region "Файлы"

    Private Sub RefFileUploadFile_Uploaded(ByVal sender As Object, ByVal e As RefFileArgs) Handles RefFileUploadFile.Uploaded
        Dim row = vREF_FILE_FUND.InsertRow(e.ID)
        DataSetSpecificationDataRow(vREF_FILE_FUND.ID, e.ID)("CreationDateTime") = e.TransferTime
        DirectCast(row.FindControl("NAME"), TextBox).Text = System.IO.Path.GetFileName(e.FileName)
    End Sub

    Private Sub RefFileUploadFile_Uploading(ByVal sender As Object, ByVal e As RefFileArgs) Handles RefFileUploadFile.Uploading
        e.FileName = GetFileSystemAttachFileName(e.ID, e.TransferTime, 701, DBNull.Value, e.FileName)
    End Sub

    Public Sub RefFile_Requesting(ByVal sender As Object, ByVal e As RefFileArgs)
        e.TransferTime = DataSetSpecificationDataRow(vREF_FILE_FUND.ID, e.ID)("CreationDateTime")
        e.FileName = GetFileSystemAttachFileName(e.ID, e.TransferTime, 701, DBNull.Value)
    End Sub

    Private Sub RefFileUploadFileS_Uploaded(ByVal sender As Object, ByVal e As RefFileArgs) Handles RefFileUploadFileS.Uploaded
        Dim row = vREF_FILE_FUND_S.InsertRow(e.ID)
        DataSetSpecificationDataRow(vREF_FILE_FUND_S.ID, e.ID)("CreationDateTime") = e.TransferTime
        DirectCast(row.FindControl("NAME"), TextBox).Text = System.IO.Path.GetFileName(e.FileName)
    End Sub

    Private Sub RefFileUploadFileS_Uploading(ByVal sender As Object, ByVal e As RefFileArgs) Handles RefFileUploadFileS.Uploading
        e.FileName = GetFileSystemAttachFileName(e.ID, e.TransferTime, 701, 100, e.FileName)
    End Sub

    Public Sub RefFileS_Requesting(ByVal sender As Object, ByVal e As RefFileArgs)
        e.TransferTime = DataSetSpecificationDataRow(vREF_FILE_FUND_S.ID, e.ID)("CreationDateTime")
        e.FileName = GetFileSystemAttachFileName(e.ID, e.TransferTime, 701, 100)
    End Sub

#End Region

End Class