﻿Public Partial Class GR_DOCUMENT_LIGHT
    Inherits BaseDoc
    Const STORAGE_GUID = "84E12153-532F-46AD-A0B7-0541450978E5"
    
    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
       
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender

        HyperLink_DOC_IMAGE.Enabled = DocStateInfo.StateID <> Guid.Empty
        HyperLink_DOC_IMAGE.NavigateUrl = BasePage.GetDocURL("GR_DOC_IMAGE.ascx", DocID, False)

    End Sub

#Region "Doc Events"
    Public Overrides Function NewDoc() As Boolean
        NewDoc = MyBase.NewDoc()
        If NewDoc Then

            'надо установить архив - если в БД только один архив (но это уже не так)
            'Dim archTable = AppSettings.GetDataTable(New SqlClient.SqlCommand("SELECT ID, OKPO, FULL_SUBJ, ARCHIVE_LEVEL FROM GR_vARCHIVE_SUBJECT"))
            'If archTable.Rows.Count = 1 Then
            'еще сделать выборку адреса архива (брать первый попавшийся)
            '    ADDRESS.Text = archTable.Rows(0).Item("ADDRESS")
            '    ID_ARCHIVE.DocID = archTable.Rows(0).Item("ID")
            'End If

        End If

        'только одна строка
        Dim row = GR_vDOC_DESCRIPT.InsertRow()
        SYS_STATUS.Text = "Новый"

        [PUBLIC].Checked = True

    End Function

    Public Overrides Function SaveDoc() As Boolean


        Try
            'проставим в спецификации значения из фиктивных полей
            SetFromFict()
            'проверки перед сохранением
            Dim ChkMsg = vbNullString
            'что бы одна из даты была заполнена
            If Len(APPROX_DATE.Text) = 0 And Len(CREATION_DATE.Text) = 0 Then
                ChkMsg = "Поля 'Дата (время создания) документа' и 'Ориентировочная дата': одно из полей должно быть обязательно заполнено."
                AddMessage(ChkMsg)
            End If

            If Len(DESCRIPT_NUM_Fict.Text) = 0 Then
                DESCRIPT_NUM_Fict.BackColor = Drawing.Color.Pink
            End If
            If Len(DESCRIPT_DATE_Fict.Text) = 0 Then
                DESCRIPT_DATE_Fict.BackColor = Drawing.Color.Pink
            End If

            If Len(DESCRIPT_NUM_Fict.Text) > 0 Then
                Dim cmd As New SqlClient.SqlCommand("SELECT ID  FROM GR_tblDOC_DESCRIPT WHERE DESCRIPT_NUM=@DESCRIPT_NUM And DocID<>@DocID")
                cmd.Parameters.AddWithValue("@DESCRIPT_NUM", DESCRIPT_NUM_Fict.Text)
                cmd.Parameters.AddWithValue("@DocID", DocID.ToString)
                Dim DataTable = AppSettings.GetDataTable(cmd)
                If DataTable.Rows.Count > 0 Then
                    ChkMsg = "Документ с регистрационным номером " & DESCRIPT_NUM_Fict.Text & " уже есть в БД."
                    AddMessage(ChkMsg)
                End If
            End If
            If Len(PROTOCOL_ORGANIZATION_Fict.Text) = 0 Then
                ChkMsg = "Поле 'Кем представлен документ' обязательно для заполнения."
                AddMessage(ChkMsg)
            End If
            '  If Len(DESCRIPT_NUM_Fict.Text) > 0 Then

            If Len(PROTOCOL_DATE_Fict.Text) = 0 Then
                'ChkMsg = "Поле 'Протокол ЭПК от' обязательно для заполнения."
                'AddMessage(ChkMsg)
                PROTOCOL_DATE_Fict.BackColor = Drawing.Color.Pink
            End If
            If Len(PROTOCOL_NUM_Fict.Text) = 0 Then
                'ChkMsg = "Поле 'Протокол ЭПК №' обязательно для заполнения."
                'AddMessage(ChkMsg)
                PROTOCOL_NUM_Fict.BackColor = Drawing.Color.Pink
            End If
            If Len(PROTOCOL_ROSARCH_DATE_Fict.Text) = 0 Then
                'ChkMsg = "Поле 'Протокол ЦЭПК Росархива от' обязательно для заполнения."
                'AddMessage(ChkMsg)
                PROTOCOL_ROSARCH_DATE_Fict.BackColor = Drawing.Color.Pink
            End If
            If Len(PROTOCOL_ROSARCH_NUM_Fict.Text) = 0 Then
                'ChkMsg = "Поле 'Протокол ЦЭПК Росархива №' обязательно для заполнения."
                'AddMessage(ChkMsg)
                PROTOCOL_ROSARCH_NUM_Fict.BackColor = Drawing.Color.Pink
            End If
            '  End If

            If GR_tblDOC_LANGUAGE.Rows.Count = 0 Then
                ChkMsg = "Поле 'Язык документа' обязательно для заполнения."
                AddMessage(ChkMsg)
            End If

            If GR_tblDOC_CARRIER.Rows.Count = 0 Then
                ChkMsg = "Поле 'Материальный носитель' обязательно для заполнения."
                AddMessage(ChkMsg)
            End If
            If Len(ID_ARCHIVE.Text) = 0 Then
                ChkMsg = "Поле 'Архив' обязательно для заполнения."
                AddMessage(ChkMsg)
            End If

            If Len(INVENTORY_NUM_1.Text) = 0 And Len(INVENTORY_NUM_ALTERNATIVE.Text) = 0 Then

                ChkMsg = "Заполните поле 'оп.№' либо 'оп.№ (альтернативный)'"
                AddMessage(ChkMsg)
            End If


            If Len(INVENTORY_NUM_1.Text) > 0 And Len(INVENTORY_NUM_ALTERNATIVE.Text) > 0 Then

                ChkMsg = "Заполните только одно поле 'оп.№' либо 'оп.№ (альтернативный)'"
                AddMessage(ChkMsg)
            End If

            If Len(ID_ARCHIVE_ADDRESS.Text) = 0 Then
                ChkMsg = "Поле 'Адрес архива' обязательно для заполнения."
                AddMessage(ChkMsg)
            End If

            If Len(ChkMsg) > 0 Then
                AddMessage("Проверка данных закончилась неудачно")
                Return False
            End If


            'нашли код архива и сделаем еще одну выборку
            'If Len(ADDRESS.Text) < 1 Then
            '    Dim archTable1 = AppSettings.GetDataTable(New SqlClient.SqlCommand("SELECT ID,ISNULL(ADDRESS,'') as ADDRESS, ISNULL(OKPO,'') AS OKPO, isnull(FULL_SUBJ,'') AS FULL_SUBJ, isnull(ARCHIVE_LEVEL,'a') AS ARCHIVE_LEVEL FROM GR_vARCHIVE_SUBJECT WHERE ID='" & ID_ARCHIVE.DocID.ToString & "'"))
            '    If archTable1.Rows.Count > 0 Then
            '        ADDRESS.Text = archTable1.Rows(0).Item("ADDRESS")
            '    End If
            'End If

            Dim SaveStatus As Boolean
            SaveStatus = MyBase.SaveDoc()
            If SaveStatus = False Then
                Return False
            End If

            Dim descTable = AppSettings.GetDataTable(New SqlClient.SqlCommand("SELECT TOP 1 isnull(DESCRIPT_NUM,' ') as DESCRIPT_NUM FROM GR_tblDOC_DESCRIPT WHERE DocID='" & DocID.ToString & "' ORDER BY DESCRIPT_DATE DESC"))
            If descTable.Rows.Count > 0 Then
                NUM.Text = descTable.Rows(0).Item("DESCRIPT_NUM")

                Dim descTable1 = AppSettings.GetDataTable(New SqlClient.SqlCommand("SELECT TOP 1 isnull(DESCRIPT_DATE, ' ') as DESCRIPT_DATE  FROM GR_tblDOC_DESCRIPT WHERE DocID='" & DocID.ToString & "' ORDER BY DESCRIPT_DATE  DESC"))
                DOC_DATE.Text = descTable1.Rows(0).Item("DESCRIPT_DATE")
                ' MyBase.SaveDoc()
            End If

            MyBase.SaveDoc()
            ' LoadDoc(DocID)
            SYS_STATUS.Text = "Сохранен"

        Catch ex As Exception
            If InStr(1, ex.Message, "FindControl") Then
                SYS_STATUS.Text = "Сохранен"
                ' LoadDoc(DocID)
            Else
                AddMessage(ex.Message)
            End If
        End Try


        Return True
    End Function

    Public Overrides Function LoadDoc(ByVal DocID As System.Guid) As Boolean
        LoadDoc = MyBase.LoadDoc(DocID)
        'сюда попадаем из списка или после сохранения документа
        Try
            'открыть заново

            Dim LastDescript As String

            If Len(SYS_STATUS.Text) = 0 Or SYS_STATUS.Text = "Загружен" Then
                LastDescript = GetLastIDDocDescript()
                ID_DOC_DESCRIPT_CURRENT.Text = LastDescript
                ''''SetFilterDocDescript(LastDescript)
            End If


            SetFict()

        Catch ex As Exception
            AddMessage(ex.Message)
        End Try
    End Function

    'Public Overrides Sub PutValueToControl(ByVal Value As Object, ByVal TargetControl As System.Web.UI.Control, Optional ByVal FieldFormat As String = "")

    '    'If TargetControl.ID = "ID_ARCHIVE_ADDRESS" Then
    '    '    Debug.Print(Value)
    '    'End If
    '    'Debug.Print(TargetControl.ID)
    '    MyBase.PutValueToControl(Value, TargetControl, FieldFormat)

    'End Sub
    
    'Public Overrides Sub PutDictionaryToControl(ByVal Value As Object, ByVal TargetControl As Web.UI.Control, ByVal DictonaryTable As DataTable, Optional ByVal FieldFormat As String = "")
    '    Select Case TargetControl.ID
    '        Case "ID_ARCHIVE_ADDRESS"
    '            Debug.Print(TargetControl.ID)
    '        Case "ID_AUTHOR"
    '            Debug.Print(TargetControl.ID)
    '        Case Else

    '    End Select
    '    MyBase.PutDictionaryToControl(Value, TargetControl, DictonaryTable, FieldFormat)
    'End Sub

#End Region

    Private Function GetLastIDDocDescript() As String

        Dim cmd As New SqlClient.SqlCommand("SELECT TOP 1 ID AS FIRST_ID  FROM GR_tblDOC_DESCRIPT WHERE DocID=@ID ORDER BY DESCRIPT_DATE DESC")
        cmd.Parameters.AddWithValue("@ID", DocID)
        Dim DataTable = AppSettings.GetDataTable(cmd)
        If DataTable.Rows.Count = 0 Then
            GetLastIDDocDescript = "00000000-0000-0000-0000-000000000000"
        Else
            GetLastIDDocDescript = DataTable.Rows(0)("FIRST_ID").ToString
        End If
        Return GetLastIDDocDescript

    End Function

   

    Public Sub AddDocDescription()



        Dim row = GR_vDOC_DESCRIPT.InsertRow()
        ID_DOC_DESCRIPT_CURRENT.Text = vbNullString
       
        SetFict()
        DESCRIPT_NUM_Fict.Text = vbNullString
        DESCRIPT_DATE_Fict.Text = vbNullString
    End Sub

    Private Function GetLastRowIDDocDescript() As Integer
        Dim cmd As New SqlClient.SqlCommand("select isnull(MAX(RowID),0)+1 AS RowID from GR_tblDOC_DESCRIPT where DocID = @ID")
        cmd.Parameters.AddWithValue("@ID", DocID)
        Dim DataTable = AppSettings.GetDataTable(cmd)
        If DataTable.Rows.Count = 0 Then
            GetLastRowIDDocDescript = 0
        Else
            GetLastRowIDDocDescript = DataTable.Rows(0)("RowID")
        End If
        Return GetLastRowIDDocDescript
    End Function

    Private Sub DeleteDocDescript(ByVal ID As String)
        Dim cmd4 As New SqlClient.SqlCommand("DELETE FROM GR_tblDOC_DESCRIPT WHERE ID=@ID")
        cmd4.Parameters.AddWithValue("@ID", ID)
        AppSettings.ExecCommand(cmd4)
    End Sub
    Public Sub DeleteDocDescription()
        Dim FirstDescript As String
        Dim CurrentDescript As String
        'проверяем, если первый лист, то нельзя удалять 
        'если последующие, то открываем первый и удаляем выбранный
        FirstDescript = GetFirstIDDocDescript()
        CurrentDescript = ID_DOC_DESCRIPT_CURRENT.Text

        If FirstDescript = ID_DOC_DESCRIPT_CURRENT.Text Then
            'нельзя удалять
            AddMessage("Открытый лист описания является первым, его удалить нельзя. Можно только редактировать.")
        Else

            'удаление
            DeleteDocDescript(CurrentDescript)
            'надо проставить номер документа последнего
            'UPDATE GR_tblDOCUMENT SET NUM=A.DESCRIPT_NUM, DOC_DATE=A.DESCRIPT_DATE FROM (select TOP 1 DESCRIPT_NUM, DESCRIPT_DATE from GR_tblDOC_DESCRIPT WHERE DocID=@ID ORDER BY DESCRIPT_DATE) A WHERE ID=@ID
            Dim cmd As New SqlClient.SqlCommand("UPDATE GR_tblDOCUMENT SET NUM=A.DESCRIPT_NUM, DOC_DATE=A.DESCRIPT_DATE FROM (select TOP 1 DESCRIPT_NUM, DESCRIPT_DATE from GR_tblDOC_DESCRIPT WHERE DocID=@ID ORDER BY DESCRIPT_DATE) A WHERE ID=@ID")
            cmd.Parameters.AddWithValue("@ID", DocID)
            AppSettings.ExecCommand(cmd)

            'последний документ
            Dim LastDescript = GetLastIDDocDescript()


            Me.LoadDoc(DocID)

            '''''''''''''' ID_DOC_DESCRIPT_CURRENT.Text = LastDescript

            ' SaveDoc()
            AddMessage("Удалено.")
        End If

    End Sub

    Private Function GetFirstIDDocDescript() As String

        Dim cmd As New SqlClient.SqlCommand("SELECT TOP 1 ID AS FIRST_ID  FROM GR_tblDOC_DESCRIPT WHERE DocID=@ID ORDER BY DESCRIPT_DATE")
        cmd.Parameters.AddWithValue("@ID", DocID)
        Dim DataTable = AppSettings.GetDataTable(cmd)
        If DataTable.Rows.Count = 0 Then
            GetFirstIDDocDescript = "00000000-0000-0000-0000-000000000000"
        Else
            GetFirstIDDocDescript = DataTable.Rows(0)("FIRST_ID").ToString
        End If
        Return GetFirstIDDocDescript
    End Function

    Public Sub OpenDocDescription()
        '  BasePage.OpenModalDialogWindow(Me, BasePage.GetListDocURL("GR_DOC_DESCRIPT_LIST.ascx", False, "DocID=" & DocID.ToString), "SelectDoc")
        BasePage.OpenModalDialogWindow(Me, BasePage.GetListDocURL("GR_DOC_DESCRIPT_LIST.ascx", False, "GR_DOCUMENT=" & DocID.ToString), "SelectDoc")
    End Sub
    Private Sub Page_ModalDialogClosed(ByVal Argument As Equipage.WebUI.eqModalDialogArgument) Handles Me.ModalDialogClosed


        ID_DOC_DESCRIPT_CURRENT.Text = Argument.ArgumentObject.ToString
        SYS_STATUS.Text = "Загружен"

        SetFict()
    End Sub

    Public Sub SetFromFict()
        For Each r In GR_vDOC_DESCRIPT.Rows
            If DirectCast(r.FindControl("ID_ForFind"), eqTextBox).Text = ID_DOC_DESCRIPT_CURRENT.Text Then
                DirectCast(r.FindControl("ART_DESIGN"), eqTextBox).Text = ART_DESIGN_Fict.Text
                DirectCast(r.FindControl("DESCRIPT_NUM"), eqTextBox).Text = DESCRIPT_NUM_Fict.Text
                DirectCast(r.FindControl("DESCRIPT_DATE"), eqTextBox).Text = DESCRIPT_DATE_Fict.Text
                DirectCast(r.FindControl("ANNOTATION"), eqTextBox).Text = ANNOTATION_Fict.Text
                DirectCast(r.FindControl("PRECIOUS"), eqTextBox).Text = PRECIOUS_Fict.Text
                DirectCast(r.FindControl("SIZES"), eqTextBox).Text = SIZES_Fict.Text
                DirectCast(r.FindControl("VOLUME"), eqTextBox).Text = VOLUME_Fict.Text
                DirectCast(r.FindControl("PHYSICAL_STATE"), eqTextBox).Text = PHYSICAL_STATE_Fict.Text
                DirectCast(r.FindControl("RESTORATION"), eqTextBox).Text = RESTORATION_Fict.Text
                DirectCast(r.FindControl("PROTOCOL_ORGANIZATION"), eqTextBox).Text = PROTOCOL_ORGANIZATION_Fict.Text
                DirectCast(r.FindControl("PROTOCOL_DATE"), eqTextBox).Text = PROTOCOL_DATE_Fict.Text
                DirectCast(r.FindControl("PROTOCOL_ROSARCH_DATE"), eqTextBox).Text = PROTOCOL_ROSARCH_DATE_Fict.Text
                DirectCast(r.FindControl("PROTOCOL_NUM"), eqTextBox).Text = PROTOCOL_NUM_Fict.Text
                DirectCast(r.FindControl("PROTOCOL_ROSARCH_NUM"), eqTextBox).Text = PROTOCOL_ROSARCH_NUM_Fict.Text
            End If
            '' ''DirectCast(r.FindControl("DocID"), TextBox).Text = DocID.ToString
            '' ''DirectCast(r.FindControl("RowID"), TextBox).Text = GetLastRowIDDocDescript()
        Next
    End Sub
    Public Sub SetFict()
        For Each r In GR_vDOC_DESCRIPT.Rows
            If DirectCast(r.FindControl("ID_ForFind"), eqTextBox).Text = ID_DOC_DESCRIPT_CURRENT.Text Then
                ART_DESIGN_Fict.Text = DirectCast(r.FindControl("ART_DESIGN"), eqTextBox).Text
                DESCRIPT_NUM_Fict.Text = DirectCast(r.FindControl("DESCRIPT_NUM"), eqTextBox).Text
                DESCRIPT_DATE_Fict.Text = DirectCast(r.FindControl("DESCRIPT_DATE"), eqTextBox).Text
                ANNOTATION_Fict.Text = DirectCast(r.FindControl("ANNOTATION"), eqTextBox).Text
                PRECIOUS_Fict.Text = DirectCast(r.FindControl("PRECIOUS"), eqTextBox).Text
                SIZES_Fict.Text = DirectCast(r.FindControl("SIZES"), eqTextBox).Text
                VOLUME_Fict.Text = DirectCast(r.FindControl("VOLUME"), eqTextBox).Text
                PHYSICAL_STATE_Fict.Text = DirectCast(r.FindControl("PHYSICAL_STATE"), eqTextBox).Text
                RESTORATION_Fict.Text = DirectCast(r.FindControl("RESTORATION"), eqTextBox).Text
                PROTOCOL_ORGANIZATION_Fict.Text = DirectCast(r.FindControl("PROTOCOL_ORGANIZATION"), eqTextBox).Text
                PROTOCOL_DATE_Fict.Text = DirectCast(r.FindControl("PROTOCOL_DATE"), eqTextBox).Text
                PROTOCOL_ROSARCH_DATE_Fict.Text = DirectCast(r.FindControl("PROTOCOL_ROSARCH_DATE"), eqTextBox).Text
                PROTOCOL_NUM_Fict.Text = DirectCast(r.FindControl("PROTOCOL_NUM"), eqTextBox).Text
                PROTOCOL_ROSARCH_NUM_Fict.Text = DirectCast(r.FindControl("PROTOCOL_ROSARCH_NUM"), eqTextBox).Text
            End If
        Next
    End Sub

#Region "Файлы"

    Private Sub RefFileUploadFile_Uploaded(ByVal sender As Object, ByVal e As RefFileArgs) Handles RefFileUploadFile.Uploaded
        'Dim row = GR_tblDOC_DESCRIPT_IMAGE.InsertRow(e.ID)
        'DataSetSpecificationDataRow(GR_tblDOC_DESCRIPT_IMAGE.ID, e.ID)("CreationDateTime") = e.TransferTime
        'DirectCast(row.FindControl("NAME"), TextBox).Text = System.IO.Path.GetFileName(e.FileName)
        'добавление строки в спецификацию
        Dim DOC_ATTR As String
        DOC_ATTR = CreateStorgePath()
        Dim RowID As Integer
        RowID = CreateRowID(e.FileName)
        If DOC_ATTR IsNot Nothing Then
            Dim row = GR_tblDOC_DESCRIPT_IMAGE.InsertRow(e.ID)
            DataSetSpecificationDataRow(GR_tblDOC_DESCRIPT_IMAGE.ID, e.ID)("CreationDateTime") = e.TransferTime
            DirectCast(row.FindControl("NAME"), TextBox).Text = DOC_ATTR + "\" + "Лист учета и описания" + "\" + System.IO.Path.GetFileName(e.FileName)
            '  DirectCast(row.FindControl("RowID"), TextBox).Text = RowID.ToString
        Else
            AddMessage("Заполните поля Архив, Номер фонда, Опись и Ед.хр. на вкладке Место хранения")
            e.Cancel = True
        End If
    End Sub

    Private Sub RefFileUploadFile_Uploading(ByVal sender As Object, ByVal e As RefFileArgs) Handles RefFileUploadFile.Uploading
        ' e.FileName = GetFileSystemAttachFileName(e.ID, e.TransferTime, 700, DBNull.Value, e.FileName)
        Dim DOC_ATTR As String
        DOC_ATTR = CreateStorgePath()
        If DOC_ATTR IsNot Nothing Then

            e.FileName = GetStorgePath() + "\" + DOC_ATTR + "\" + "Лист учета и описания" + "\" + System.IO.Path.GetFileName(e.FileName)

        Else
            AddMessage("Заполните поля Архив, Номер фонда, Опись и Ед.хр. на вкладке Место хранения")
            e.Cancel = True
        End If
    End Sub

    Public Sub RefFile_Requesting(ByVal sender As Object, ByVal e As RefFileArgs)
        'e.TransferTime = DataSetSpecificationDataRow(GR_tblDOC_DESCRIPT_IMAGE.ID, e.ID)("CreationDateTime")
        'e.FileName = GetFileSystemAttachFileName(e.ID, e.TransferTime, 700, DBNull.Value)

        'открываем файл на просмотр
        Try
            Dim FileName As String
            FileName = GetFileImageName(e.ID)
            If FileName IsNot Nothing Then
                e.FileName = FileName
            Else
                AddMessage("Сохраните документ")
            End If

        Catch ex As Exception
            AddMessage(ex.Message)
        End Try
    End Sub


    Public Function GetFileImageName(ByVal ID As Guid) As String

        'в поле Name храниться весь путь
        Dim cmd As New SqlClient.SqlCommand("select NAME from GR_tblDOC_DESCRIPT_IMAGE where ID = @ID")
        cmd.Parameters.AddWithValue("@ID", ID)
        Dim DataTable = AppSettings.GetDataTable(cmd)
        If DataTable.Rows.Count = 0 Then
            GetFileImageName = Nothing
        Else
            GetFileImageName = GetStorgePath() + "\" + DataTable.Rows(0)("NAME")
        End If

    End Function


    Private Function GetStorgePath() As String

        'Dirs()

        Dim cmd As New SqlClient.SqlCommand("select Text from tblConstantsSpec where ID = @ID")
        cmd.Parameters.AddWithValue("@ID", New Guid(STORAGE_GUID))
        Dim DataTable = AppSettings.GetDataTable(cmd)
        If DataTable.Rows.Count = 0 Then
            GetStorgePath = "C:\Госреестр-Файлы"
        Else
            GetStorgePath = DataTable.Rows(0)("Text")
        End If
        Return GetStorgePath
    End Function
    Private Function CreateStorgePath() As String
        'опеределяем путь к файлу и сохранем его в БД
        ' Dim ID_ARCHIVE As Guid = Nothing
        ' Dim ID_ARCHIVE As String = Nothing
        Dim ARCHIVE As String = Nothing
        Dim SUBJECT As String = Nothing
        Dim FUND_NUM As String = Nothing
        Dim INVENTORY_NUM As String = Nothing
        Dim UNIT_NUM As String = Nothing
        Dim INVENTORY_ALTERNATIVE As String = Nothing
        'проверка на заполненность обязательных полей номер фонда, номер описи и номер единицы хранения

        FUND_NUM = FUND_NUM_1.Text + FUND_NUM_2.Text + FUND_NUM_3.Text
        INVENTORY_NUM = INVENTORY_NUM_1.Text + INVENTORY_NUM_2.Text
        INVENTORY_ALTERNATIVE = INVENTORY_NUM_ALTERNATIVE.Text

        If Len(INVENTORY_ALTERNATIVE) > 0 Then
            INVENTORY_NUM = INVENTORY_ALTERNATIVE
        Else
            If Len(INVENTORY_NUM_3.Text) > 0 Then
                INVENTORY_NUM = INVENTORY_NUM + "_" + INVENTORY_NUM_3.Text
            End If

        End If
        

        UNIT_NUM = UNIT_NUM_1.Text + UNIT_NUM_2.Text
        If Len(UNIT_VOL_NUM.Text) > 0 Then
            UNIT_NUM = UNIT_NUM + "_" + UNIT_VOL_NUM.Text
        End If
        If Len(SHEETS.Text) > 0 Then
            UNIT_NUM = UNIT_NUM + "_" + SHEETS.Text
        End If

        '    'надо брать короткое имя архива
        ARCHIVE = GetArchiveNameShort(ID_ARCHIVE.DocID)
        
        If ID_ARCHIVE.ToString IsNot Nothing And Len(FUND_NUM.ToString) > 0 And Len(INVENTORY_NUM.ToString) > 0 And Len(UNIT_NUM.ToString) > 0 Then
            
            CreateStorgePath = ARCHIVE + "\" + FUND_NUM + "\" + INVENTORY_NUM + "\" + UNIT_NUM

        Else
            CreateStorgePath = Nothing
        End If

        Return CreateStorgePath
    End Function

    Private Function CreateRowID(ByVal FileName As String) As Integer
        Dim RowID As Integer = 0
        Return RowID
    End Function

    Private Function GetArchiveNameShort(ByVal ID_ARCHIVE As Guid) As String
        'Dim cmd As New SqlClient.SqlCommand("select NAME_SHORT from tblARCHIVE where ID = @ID")
        'cmd.Parameters.AddWithValue("@ID", ID_ARCHIVE)
        'Dim DataTable = AppSettings.GetDataTable(cmd)
        'If DataTable.Rows.Count = 0 Then
        '    GetArchiveNameShort = "Архив не найден"
        'Else
        '    GetArchiveNameShort = DataTable.Rows(0)("NAME_SHORT")
        'End If
        'Return GetArchiveNameShort

        Dim cmd As New SqlClient.SqlCommand("select A.NAME_SHORT, isnull(S.NAME, '') as ARCH_SUBJECT from  dbo.tblARCHIVE AS A LEFT OUTER JOIN dbo.tblSUBJECT_CL AS S ON A.ISN_SUBJECT = S.ISN_SUBJECT where A.ID = @ID")
        cmd.Parameters.AddWithValue("@ID", ID_ARCHIVE)
        Dim DataTable = AppSettings.GetDataTable(cmd)
        Dim sbj = DataTable.Rows(0)("ARCH_SUBJECT")
        If Len(sbj) = 0 Then

        Else
            sbj = sbj + "\"
        End If

        If DataTable.Rows.Count = 0 Then
            GetArchiveNameShort = "Архив не найден"
        Else
            GetArchiveNameShort = sbj + DataTable.Rows(0)("NAME_SHORT")
        End If
        Return GetArchiveNameShort
    End Function
#End Region

#Region "Выгрузка-загрузка Эксель"
    Public Sub MakeReport()

        If Me.SaveDoc() = True Then
            Dim cmd As New SqlClient.SqlCommand("select 1")
            AppSettings.ExecCommand(cmd)

            Dim ExcelReportGeneratorWork As New ExcelReportGenerator.ExcelReportGenerator
            Dim ewMemoryStream As New IO.MemoryStream
            Dim sReportFileName As String
            Dim sReportFileNameSave As String
            Dim IsHTML As Boolean

            sReportFileName = "GR_tplDocument_ExcelExchange.xls"
            If DESCRIPT_NUM_Fict.Text = "0" Or Len(DESCRIPT_NUM_Fict.Text) = 0 Then
                sReportFileNameSave = "ЛистОписанияУникальногоДокумента_" + Left(NAME.Text, 15)
            Else
                sReportFileNameSave = "ЛистОписанияУникальногоДокумента_№" + DESCRIPT_NUM_Fict.Text + "_" + Left(DESCRIPT_DATE_Fict.Text, 10)
            End If


            IsHTML = False

            Dim arrParameterValueBox(24) As String
            Dim arrParameterWebBox(24) As String
            SetParameterValueBox(arrParameterValueBox, arrParameterWebBox)

            ExcelReportGeneratorWork.ExcelReportGeneration(AppSettings, ewMemoryStream, _
                                                                    sReportFileName, _
                                                                    arrParameterValueBox, arrParameterWebBox)
            If IsHTML = True Then
                BasePage.AttachFile(sReportFileNameSave & ".doc", ewMemoryStream.ToArray())
            Else
                BasePage.AttachFile(sReportFileNameSave & ".xls", ewMemoryStream.ToArray())
            End If
            ewMemoryStream.Close()
        End If

    End Sub
    Public Sub SetParameterValueBox(ByVal ParameterValueBox() As String, ByVal ParameterNameBox() As String)
        Dim i As Integer
        For i = 0 To 24
            ParameterValueBox(i) = -1
        Next
        For i = 0 To 24
            ParameterNameBox(i) = ""
        Next

        i = 0

        'тут только по Id документа будем ориентироваться
        'If Len(ID_DOC_DESCRIPT_CURRENT.Text) > 0 Then
        '    ParameterValueBox(i) = ID_DOC_DESCRIPT_CURRENT.Text
        '    ParameterNameBox(i) = "Prop_ID_DOCUMENT"
        'End If

        If Len(DocID.ToString) > 0 Then
            ParameterValueBox(i) = DocID.ToString
            ParameterNameBox(i) = "Prop_ID_DOCUMENT"
        End If
        i = i + 1

        For i = 0 To 24
            Debug.Print(ParameterNameBox(i) + "  " + CStr(ParameterValueBox(i)))
        Next
    End Sub
#End Region
  
   


    
    Private Sub ID_ARCHIVE_SelectDocCompleted(ByVal sender As Object, ByVal e As Equipage.WebUI.eqBaseContainer.SelectDocEventArgs) Handles ID_ARCHIVE.SelectDocCompleted
        Dim ArchID = ID_ARCHIVE.KeyValue
        Dim cmdGet As New SqlClient.SqlCommand("SELECT ID, ADDRESS FROM tblARCHIVE_ADDRESS WHERE DocID=@ID_ARCHIVE")
        cmdGet.Parameters.AddWithValue("@ID_ARCHIVE", ArchID.ToString)
        Dim archTable = AppSettings.GetDataTable(cmdGet)
        MyBase.PutDictionaryToControl(vbNull, ID_ARCHIVE_ADDRESS, archTable)

       
        MyBase.FindControl("ID_ARCHIVE")
        Dim cmdGet_2 As New SqlClient.SqlCommand("SELECT NAME_SHORT FROM tblARCHIVE WHERE ID=@ID_ARCHIVE")
        cmdGet_2.Parameters.AddWithValue("@ID_ARCHIVE", ArchID.ToString)
        Dim archTable_2 = AppSettings.GetDataTable(cmdGet_2)

        If archTable_2.Rows.Count = 1 Then
            NAME_SHORT.Text = archTable_2.Rows(0).Item("NAME_SHORT")
        End If

    End Sub
End Class