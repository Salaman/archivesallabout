﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="GR_ExcelReports.ascx.vb"
    Inherits="WebApplication.GR_ExcelReports" %>
<%@ Register TagPrefix="qq" Namespace="WebApplication.WebUI" Assembly="WebApplication" %>
<div class="LogicBlockCaption">
    Государственный реестр уникальных документов Архивного фонда Российской Федерации
</div>
<table width="100%" class="LogicBlockTable">
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_GR_DOCUMENT" Text="ЛИСТ УЧЕТА И ОПИСАНИЯ УНИКАЛЬНОГО ДОКУМЕНТА"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" Checked="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_GR_ListRegisrationDate" Text="Перечень по времени регистрации"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_GR_ListFederationArchive" Text="Перечень по федеральному архиву"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_GR_ListRFSubject" Text="Перечень по субъекту РФ"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_GR_ListFederationSubject" Text="Перечень по федеральному округу"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_GR_ListKindAndDate" Text="Перечень по виду и датировке"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_GR_ListAutor" Text="Перечень по автору" GroupName="R"
                OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_GR_ListDate" Text="Перечень по датировке"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_GR_ListPreciousMetalStones" Text="Перечень документов с драг. камнями и драг. металлами"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_GR_ListSeal" Text="Перечень документов с печатями"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_GR_ListRestoration" Text="Перечень документов, прошедших реставрацию"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
</table>
<asp:Panel ID="Panel_Properties" runat="server" Width="350" BorderWidth="1" BackColor="White">
    <div class="LogicBlockCaption">
        Параметры отчета
    </div>
    <asp:Panel ID="Panel_SecurityFlags" runat="server">
        <table width="100%" cellspacing="4">
            <colgroup>
                <col width="110px" />
            </colgroup>
            <tr>
                <%--<td>
                    Уровень секретности:
                </td>
                <td>
                    <asp:CheckBoxList ID="Prop_SecurityFlags" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Value="1" Text="откр" Selected="True" />
                        <asp:ListItem Value="2" Text="с" Selected="True" />
                        <asp:ListItem Value="3" Text="чс" Selected="True" />
                    </asp:CheckBoxList>
                </td>--%>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="Panel_ISN_DOCUMENT" runat="server">
        <table width="100%" cellspacing="4">
            <colgroup>
                <col width="110px" />
            </colgroup>
            <tr>
                <td>
                    Лист описания
                </td>
                <td>
                   
                    <eq3:eqDocument ID="Prop_ID_DOCUMENT" runat="server" DocTypeURL="GR_DOC_DESCRIPT_LIST.ascx"
                        ListDocMode="NewWindowAndSelect" LoadDocMode="ListDocAndSelect" ShowingFields="DOC_NUM, DESCRIPT_NUM, NAME"
                        ShowingFieldsFormat="№ документа {0}, № листа описания {1}, наименование документа {2}"
                        KeyName="ID" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="Panel_ISN_ARCHIVE" runat="server">
        <table width="100%" cellspacing="4">
            <colgroup>
                <col width="110px" />
            </colgroup>
            <tr>
                <td>
                    Архив
                </td>
                <td>
                    <eq3:eqDocument ID="Prop_ID_ARCHIVE" runat="server" DocTypeURL="GR_ARCHIVE.ascx"
                        ListDocMode="NewWindowAndSelect" LoadDocMode="ListDocAndSelect" ShowingFields="FULL_SUBJ, NAME"
                        ShowingFieldsFormat="{0} {1}" KeyName="ID" Width="500px" OptionalQueryString="ARCHIVE_LEVEL=a" />
                    <%-- <asp:HyperLink ID="HyperLinkARCHIVEs" runat="server" Text="Выбрать" Target="_blank"
                        NavigateUrl="~/Default.aspx?DocTypeURL=GR_ARCHIVE.ascx&ListDoc=True" Style="font-weight: bold;
                        padding: 2px;" />--%>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="Panel_Year" runat="server">
        <table width="100%" cellspacing="4">
            <colgroup>
                <col width="110px" />
            </colgroup>
            <tr>
                <td>
                    Год
                </td>
                <td>
                    <eq:eqTextBox ID="Prop_Year" runat="server" Width="60" Text="2000" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="Panel_Dating_RB" runat="server">
        <table width="100%" cellspacing="4">
            <colgroup>
                <col width="110px" />
            </colgroup>
            <tr>
                <td>
                    <fieldset>
                        <legend>Датировка по</legend>
                        <asp:RadioButtonList ID="Prop_Dating_type" runat="server" RepeatDirection="vertical"
                            AutoPostBack="true" RepeatLayout="Flow" Width="40px">
                            <asp:ListItem Text="год" Value="1" Selected="true"></asp:ListItem>
                            <asp:ListItem Text="век" Value="2"></asp:ListItem>
                        </asp:RadioButtonList>
                    </fieldset>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="Panel_Year2" runat="server">
        <table width="100%" cellspacing="4">
            <colgroup>
                <col width="110px" />
            </colgroup>
            <tr>
                <td>
                    Год с
                </td>
                <td>
                    <eq:eqTextBox ID="Prop_Year1" runat="server" Width="60" Text="" />
                </td>
                <td>
                    по
                </td>
                <td>
                    <eq:eqTextBox ID="Prop_Year2" runat="server" Width="60" Text="" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="Panel_Dates" runat="server">
        <table width="100%" cellspacing="4">
            <colgroup>
                <col width="110px" />
            </colgroup>
            <tr>
                <td>
                    Дата с
                </td>
                <td>
                    <eq:eqTextBox ID="Prop_DOC_DATE1" runat="server" Width="100px" />
                    <aj:CalendarExtender runat="server" TargetControlID="Prop_DOC_DATE1" />
                </td>
                <td>
                    по
                </td>
                <td>
                    <eq:eqTextBox ID="Prop_DOC_DATE2" runat="server" Width="100px" />
                    <aj:CalendarExtender runat="server" TargetControlID="Prop_DOC_DATE2" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="Panel_CENTURY" runat="server">
        <table width="100%" cellspacing="4">
            <colgroup>
                <col width="110px" />
            </colgroup>
            <tr>
                <td>
                    Век с
                </td>
                <td>
                    <asp:DropDownList ID="ID_CENTURY_FROM" runat="server">
                    </asp:DropDownList>
                </td>
                <td>
                    по
                </td>
                <td>
                    <asp:DropDownList ID="ID_CENTURY_TO" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="Panel_DOC_KIND" runat="server">
        <table width="100%" cellspacing="4">
            <colgroup>
                <col width="110px" />
            </colgroup>
            <tr>
                <td>
                    Вид документа
                </td>
                <td>
                    <eq3:eqDocument ID="Prop_ID_DOC_KIND" runat="server" DocTypeURL="GR_DOC_KIND_CL.ascx"
                        ListDocMode="NewWindowAndSelect" LoadDocMode="ListDocAndSelect" ShowingFields="NAME"
                        ShowingFieldsFormat="{0}" KeyName="ID" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="Panel_ISN_AUTHOR" runat="server">
        <table width="100%" cellspacing="4">
            <colgroup>
                <col width="110px" />
            </colgroup>
            <tr>
                <td>
                    Автор документа
                </td>
                <td>
                    <eq3:eqDocument ID="Prop_ID_AUTHOR" runat="server" DocTypeURL="GR_AUTHOR.ascx" ListDocMode="NewWindowAndSelect"
                        LoadDocMode="ListDocAndSelect" ShowingFields="NAME" ShowingFieldsFormat="{0}"
                        KeyName="ID" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="Panel_ISN_SUBJECT" runat="server">
        <table width="100%" cellspacing="4">
            <colgroup>
                <col width="110px" />
            </colgroup>
            <tr>
                <td>
                    Федеральный округ / Субъект
                </td>
                <td>
                    <eq2:eqDocument ID="Prop_ISN_SUBJECT" runat="server" DocTypeURL="SUBJECT_CL.ascx"
                        ListDocMode="NewWindowAndSelect" LoadDocMode="ListDocAndSelect" ShowingFields="NAME"
                        ShowingFieldsFormat="{0}" KeyName="ISN_SUBJECT" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="Panel_REPORT_ID" runat="server">
        <table width="100%" cellspacing="4">
            <colgroup>
                <col width="110px" />
            </colgroup>
            <tr>
                <td>
                    Код отчета
                </td>
                <td>
                    <eq:eqTextBox ID="Prop_REPORT_ID" runat="server" Width="60" Text="1" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Panel>
<aj:AlwaysVisibleControlExtender ID="Panel_Properties_Ex" runat="server" TargetControlID="Panel_Properties"
    HorizontalSide="Right" VerticalSide="Top" HorizontalOffset="120" VerticalOffset="240" />
<span id="report_tag"></span>