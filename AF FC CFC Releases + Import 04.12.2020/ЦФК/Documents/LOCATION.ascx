﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="LOCATION.ascx.vb" Inherits="WebApplication.LOCATION" %>
<div style="border: solid 1px black; padding: 5px;">
    <table style="width: 700px;">
        <colgroup>
            <col style="width: 150px;" />
            <col />
        </colgroup>
        <tr>
            <td>
                Архив
            </td>
            <td>
                <eq2:eqDocument ID="ISN_ARCHIVE" runat="server" KeyName="ISN_ARCHIVE" DocTypeURL="ARCHIVE.ascx"
                    LoadDocMode="NewWindowAndSelect" ListDocMode="NewWindowAndSelect" ShowingFields="NAME_SHORT,ISN_ARCHIVE"
                    ShowingFieldsFormat="{0}" />
            </td>
        </tr>
        <tr>
            <td>
                Верхний уровень
            </td>
            <td> 
                <eq2:eqDocument ID="ISN_HIGH_LOCATION" KeyName="ISN_LOCATION" runat="server" DocTypeURL="LOCATION.ascx"
                    LoadDocMode="NewWindowAndSelect" ListDocMode="NewWindowAndSelect" ShowingFields="NAME"
                    ShowingFieldsFormat="{0}" ValidateHierarchy="true" />
                <asp:LinkButton ID="clearISN_HIGH_LOCATION" runat="server" Text="Удалить" ToolTip="Сделать корневым элементом" />
            </td>
        </tr>
        <tr>
            <td>
                Код
            </td>
            <td>
                <eq:eqTextBox ID="CODE" runat="server" />
            </td>
        </tr>
        <tr>
            <td valign="top">
                Сокращенное наименование*
            </td>
            <td>
                <eq:eqTextBox ID="NAME" runat="server" TextMode="MultiLine" Rows="4" />
            </td>
        </tr>
        <tr>
            <td valign="top">
                Полное наименование
            </td>
            <td>
                <eq:eqTextBox ID="NOTE" runat="server" TextMode="MultiLine" Rows="4" />
            </td>
        </tr>
    </table>
    <%--Начиная отсюда сохраняется в dbo.ARCHIVE_STORAGE--%>
    <eq:eqSpecification ID="tblARCHIVE_STORAGE" runat="server" AllowInsert="false" AllowDelete="false"
        AllowReorder="false" ShowNumering="false">
        <eq:eqSpecificationColumn>
            <ItemTemplate>
                <table>
                    <colgroup>
                        <col valign="top" style="width: 33%;" />
                        <col valign="top" style="width: 33%;" />
                        <col valign="top" style="width: 33%;" />
                    </colgroup>
                    <tr>
                        <td>
                            <fieldset>
                                <legend>Помещения архивохранилища </legend>
                                <table>
                                    <colgroup>
                                        <col />
                                        <col />
                                    </colgroup>
                                    <tr>
                                        <td>
                                            Всего
                                        </td>
                                        <td>
                                            <eq:eqTextBox ID="ROOM_CNT" runat="server" Width="40px" />
                                            <aj:FilteredTextBoxExtender runat="server" TargetControlID="ROOM_CNT" FilterType="Numbers" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Специальных
                                        </td>
                                        <td>
                                            <eq:eqTextBox ID="SPECIAL_ROOM_CNT" runat="server" Width="40px" />
                                            <aj:FilteredTextBoxExtender runat="server" TargetControlID="SPECIAL_ROOM_CNT" FilterType="Numbers" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Приспособленных
                                        </td>
                                        <td>
                                            <eq:eqTextBox ID="ADAPTED_ROOM_CNT" runat="server" Width="40px" />
                                            <aj:FilteredTextBoxExtender runat="server" TargetControlID="ADAPTED_ROOM_CNT" FilterType="Numbers" />
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                        <td>
                            <fieldset>
                                <legend>Площадь помещений архивохранилища </legend>
                                <table>
                                    <colgroup>
                                        <col />
                                        <col />
                                    </colgroup>
                                    <tr>
                                        <td>
                                            Общая
                                        </td>
                                        <td>
                                            <eq:eqTextBox ID="TOTAL_SPACE" runat="server" Width="40px" />
                                            <aj:FilteredTextBoxExtender runat="server" TargetControlID="TOTAL_SPACE" FilterType="Custom, Numbers"
                                                ValidChars=",." />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Не оснащенная охранной сигнализацией
                                        </td>
                                        <td>
                                            <eq:eqTextBox ID="SPACE_WITH_ALARM" runat="server" Width="40px" />
                                            <aj:FilteredTextBoxExtender runat="server" TargetControlID="SPACE_WITH_ALARM" FilterType="Custom, Numbers"
                                                ValidChars=",." />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Не оснащенная пожарной сигнализацией
                                        </td>
                                        <td>
                                            <eq:eqTextBox ID="SPACE_WITHOUT_ALARM" runat="server" Width="40px" />
                                            <aj:FilteredTextBoxExtender runat="server" TargetControlID="SPACE_WITHOUT_ALARM"
                                                FilterType="Custom, Numbers" ValidChars=",." />
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                        <td>
                            <fieldset>
                                <legend>Протяженность стелажжных полок (пог. м) </legend>
                                <table>
                                    <colgroup>
                                        <col />
                                        <col />
                                    </colgroup>
                                    <tr>
                                        <td>
                                            Всего
                                        </td>
                                        <td>
                                            <eq:eqTextBox ID="SHELF_LENGTH" runat="server" Width="40px" />
                                            <aj:FilteredTextBoxExtender runat="server" TargetControlID="SHELF_LENGTH" FilterType="Custom, Numbers"
                                                ValidChars=",." />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Металлических
                                        </td>
                                        <td>
                                            <eq:eqTextBox ID="METAL_SHELF_LENGTH" runat="server" Width="40px" />
                                            <aj:FilteredTextBoxExtender runat="server" TargetControlID="METAL_SHELF_LENGTH" FilterType="Custom, Numbers"
                                                ValidChars=",." />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Свободных
                                        </td>
                                        <td>
                                            <eq:eqTextBox ID="FREE_SHELF_LENGTH" runat="server" Width="40px" />
                                            <aj:FilteredTextBoxExtender runat="server" TargetControlID="FREE_SHELF_LENGTH" FilterType="Custom, Numbers"
                                                ValidChars=",." />
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                    </tr>
                </table>
                <table style="width: 700px;">
                    <colgroup>
                        <col style="width: 150px;" />
                        <col />
                    </colgroup>
                    <tr>
                        <td>
                            Корпус
                        </td>
                        <td>
                            <eq:eqTextBox ID="STORAGE" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Этаж
                        </td>
                        <td>
                            <eq:eqTextBox ID="FLOOR" runat="server" />
                        </td>
                    </tr>
                </table>
            </ItemTemplate>
        </eq:eqSpecificationColumn>
    </eq:eqSpecification>
</div>
