﻿Public Partial Class GR_Combining_CL
    Inherits BaseDoc

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Private Sub ButtonGetCL_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonGetCL.Click
        Try
            Panel_Language.Visible = False
            Panel_Author.Visible = False
            Panel_Carrier.Visible = False
            Panel_Kind.Visible = False
            Panel_CombiningOK.Visible = False
            'проверяем выбор типа справочника
            Select Case DocType.SelectedItem.Text
                Case "Язык документа"
                    Panel_Language.Visible = True
                Case "Автор документа"
                    Panel_Author.Visible = True
                Case "Вид материального носителя"
                    Panel_Carrier.Visible = True
                Case "Вид документа"
                    Panel_Kind.Visible = True
            End Select

            DocType.Enabled = False
            Button_Combining.Visible = True
            ButtonGetCL.Visible = False
            Button_NewCombining.Visible = True
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button_CombiningOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button_CombiningOK.Click
        Try

            'удаляем строки из спецификации напрямую
            'Dim cmdDel As New SqlClient.SqlCommand("DELETE FROM GR_tblDOC_IMAGE WHERE DocID=@DocID")
            'cmdDel.Parameters.AddWithValue("@DocID", DocID.ToString)
            'AppSettings.ExecCommand(cmdDel)
            Button_CombiningOK.Visible = False

            Select Case DocType.SelectedItem.Text
                Case "Автор документа"
                    'берем ID объединяемого значения
                    Dim CombiningID = Author_Name.SelectedValue 'это должно быть ID

                    For Each r In GR_vClassificator_Author.Rows
                        'пробегаем по всем строкам
                        'Dim Name = DirectCast(r.FindControl("NAME"), TextBox).Text
                        Dim AuthorID = DirectCast(r.FindControl("ID_VALUE"), DropDownList).Text
                        Dim CreationDateTime = Now
                        'обновляем ID значения в соотвествующей таблице
                        Dim cmdUpdate As New SqlClient.SqlCommand("UPDATE GR_tblDOC_AUTHOR SET ID_AUTHOR=@ID_AUTHOR_NEW WHERE ID_AUTHOR=@ID_AUTHOR")
                        cmdUpdate.Parameters.AddWithValue("@ID_AUTHOR_NEW", CombiningID.ToString)
                        cmdUpdate.Parameters.AddWithValue("@ID_AUTHOR", AuthorID.ToString)
                        AppSettings.ExecCommand(cmdUpdate)
                        'удаляем предыдущие значения из БД
                        Dim cmdDelete As New SqlClient.SqlCommand("UPDATE GR_tblAUTHOR SET DELETED=1 WHERE ID=@ID_AUTHOR")
                        cmdDelete.Parameters.AddWithValue("@ID_AUTHOR", AuthorID.ToString)
                        AppSettings.ExecCommand(cmdDelete)

                    Next
                Case "Язык документа"
                    'берем ID объединяемого значения
                    Dim CombiningID = Language_Name_cl.SelectedValue 'это должно быть ID

                    For Each r In GR_vClassificator_Language.Rows
                        'пробегаем по всем строкам
                        'Dim Name = DirectCast(r.FindControl("NAME"), TextBox).Text
                        Dim LanguageID = DirectCast(r.FindControl("ID_VALUE"), DropDownList).Text
                        Dim CreationDateTime = Now
                        'обновляем ID значения в соотвествующей таблице
                        Dim cmdUpdate As New SqlClient.SqlCommand("UPDATE GR_tblDOC_LANGUAGE SET ID_LANGUAGE=@ID_LANGUAGE_NEW WHERE ID_LANGUAGE=@ID_LANGUAGE")
                        cmdUpdate.Parameters.AddWithValue("@ID_LANGUAGE_NEW", CombiningID.ToString)
                        cmdUpdate.Parameters.AddWithValue("@ID_LANGUAGE", LanguageID.ToString)
                        AppSettings.ExecCommand(cmdUpdate)
                        'удаляем предыдущие значения из БД
                        Dim cmdDelete As New SqlClient.SqlCommand("UPDATE GR_tblLANGUAGE_CL SET DELETED=1 WHERE ID=@ID_LANGUAGE")
                        cmdDelete.Parameters.AddWithValue("@ID_LANGUAGE", LanguageID.ToString)
                        AppSettings.ExecCommand(cmdDelete)

                    Next
                Case "Вид материального носителя"
                    'берем ID объединяемого значения
                    Dim CombiningID = Carrier_Name.SelectedValue 'это должно быть ID

                    For Each r In GR_vClassificator_Carrier.Rows
                        'пробегаем по всем строкам
                        'Dim Name = DirectCast(r.FindControl("NAME"), TextBox).Text
                        Dim CarrierID = DirectCast(r.FindControl("ID_VALUE"), DropDownList).Text
                        Dim CreationDateTime = Now
                        'обновляем ID значения в соотвествующей таблице
                        Dim cmdUpdate As New SqlClient.SqlCommand("UPDATE GR_tblDOC_CARRIER SET ID_CARRIER=@ID_CARRIER_NEW WHERE ID_CARRIER=@ID_CARRIER")
                        cmdUpdate.Parameters.AddWithValue("@ID_CARRIER_NEW", CombiningID.ToString)
                        cmdUpdate.Parameters.AddWithValue("@ID_CARRIER", CarrierID.ToString)
                        AppSettings.ExecCommand(cmdUpdate)
                        'удаляем предыдущие значения из БД
                        Dim cmdDelete As New SqlClient.SqlCommand("UPDATE GR_tblCARRIER_CL SET DELETED=1 WHERE ID=@ID_CARRIER")
                        cmdDelete.Parameters.AddWithValue("@ID_CARRIER", CarrierID.ToString)
                        AppSettings.ExecCommand(cmdDelete)

                    Next
                Case "Вид документа"
                    'берем ID объединяемого значения
                    Dim CombiningID = Kind_Name.SelectedValue 'это должно быть ID

                    For Each r In GR_vClassificator_Kind.Rows
                        'пробегаем по всем строкам
                        'Dim Name = DirectCast(r.FindControl("NAME"), TextBox).Text
                        Dim KindID = DirectCast(r.FindControl("ID_VALUE"), DropDownList).Text
                        Dim CreationDateTime = Now
                        'обновляем ID значения в соотвествующей таблице
                        Dim cmdUpdate As New SqlClient.SqlCommand("UPDATE GR_tblDOCUMENT SET ID_DOC_KIND=@ID_DOC_KIND_NEW WHERE ID_DOC_KIND=@ID_DOC_KIND")
                        cmdUpdate.Parameters.AddWithValue("@ID_DOC_KIND_NEW", CombiningID.ToString)
                        cmdUpdate.Parameters.AddWithValue("@ID_DOC_KIND", KindID.ToString)
                        AppSettings.ExecCommand(cmdUpdate)
                        'удаляем предыдущие значения из БД
                        Dim cmdDelete As New SqlClient.SqlCommand("UPDATE GR_tblDOC_KIND_CL SET DELETED=1 WHERE ID=@ID_DOC_KIND")
                        cmdDelete.Parameters.AddWithValue("@ID_DOC_KIND", KindID.ToString)
                        AppSettings.ExecCommand(cmdDelete)

                    Next

                Case Else
            End Select

            '   Response.Redirect(Request.RawUrl)
            Label_Message.Text = "Объединение успешно выполнено!!!"
            AddMessage("Объединение успешно выполнено")
        Catch ex As Exception

            AddMessage(ex.Message)

        End Try
    End Sub

    Private Sub Button_NewCombining_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button_NewCombining.Click
        Try
            'Panel_Language.Visible = False
            'Panel_Author.Visible = False
            'Panel_Carrier.Visible = False
            'Panel_Kind.Visible = False


            'DocType.Enabled = True
            'Button_Combining.Visible = False
            'ButtonGetCL.Visible = True
            'Button_NewCombining.Visible = False

            Response.Redirect(Request.RawUrl)

        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button_Combining_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button_Combining.Click
        Try
           
            'проверяем выбор типа справочника
            Select Case DocType.SelectedItem.Text
                Case "Язык документа"
                    Panel_Language.Enabled = False
                Case "Автор документа"
                    Panel_Author.Enabled = False
                Case "Вид материального носителя"
                    Panel_Carrier.Enabled = False
                Case "Вид документа"
                    Panel_Kind.Enabled = False
            End Select

            Panel_CombiningOK.Visible = True
            DocType.Enabled = False
            Button_Combining.Visible = False
            ButtonGetCL.Visible = False
            Button_NewCombining.Visible = True
            Button_CombiningOK.Visible = True
        Catch ex As Exception

        End Try
    End Sub
End Class