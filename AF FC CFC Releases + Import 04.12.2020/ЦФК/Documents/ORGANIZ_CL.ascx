﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ORGANIZ_CL.ascx.vb"
    Inherits="WebApplication.ORGANIZ_CL" %>
<table style="width: 700px;">
    <colgroup>
        <col style="width: 150px;" />
        <col style="width: 100px;" />
        <col />
    </colgroup>
    <tr>
        <td>
            Номер фонда
        </td>
        <td>
            <asp:TextBox ID="FUND_NUMS" runat="server" ReadOnly="true" />
        </td>
    </tr>  
    <tr>
        <td style="width: 150px;">
            Название
        </td>
        <td colspan="2">
            <eq:eqTextBox ID="NAME" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            Код
        </td>
        <td colspan="2">
            <eq:eqTextBox ID="CODE" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            Год создания
        </td>
        <td>
            <eq:eqTextBox ID="CREATE_YEAR" runat="server" />
        </td>
        <td>
            <asp:CheckBox ID="CREATE_YEAR_INEXACT" runat="server" Text="Неточный:" TextAlign="Left" />
        </td>
    </tr>
    <tr>
        <td>
            Год упразднения
        </td>
        <td>
            <eq:eqTextBox ID="DELETE_YEAR" runat="server" />
        </td>
        <td>
            <asp:CheckBox ID="DELETE_YEAR_INEXACT" runat="server" Text="Неточный:" TextAlign="Left" />
        </td>
    </tr>
    <tr>
        <td valign="top">
            Адрес
        </td>
        <td colspan="2">
            <eq:eqTextBox ID="ADDRESS" runat="server" TextMode="MultiLine" Rows="4" />
        </td>
    </tr>
    <tr>
        <td valign="top">
            Примечание
        </td>
        <td colspan="2">
            <eq:eqTextBox ID="NOTE" runat="server" TextMode="MultiLine" Rows="4" />
        </td>
    </tr>
</table>
<fieldset>
    <legend>Переименования:</legend>
    <eq:eqSpecification ID="tblORGANIZ_RENAME" runat="server" Width="100%">
        <eq:eqSpecificationColumn HeaderText="Код">
            <ItemTemplate>
                <asp:TextBox ID="CODE" runat="server" />
            </ItemTemplate>
        </eq:eqSpecificationColumn>
        <eq:eqSpecificationColumn HeaderText="Название">
            <ItemTemplate>
                <asp:TextBox ID="NAME" runat="server" />
            </ItemTemplate>
        </eq:eqSpecificationColumn>
        <eq:eqSpecificationColumn HeaderText="Год начала периода">
            <ItemTemplate>
                <asp:TextBox ID="CREATE_DATE" runat="server" />
            </ItemTemplate>
        </eq:eqSpecificationColumn>
        <eq:eqSpecificationColumn HeaderText="Год окончания периода">
            <ItemTemplate>
                <asp:TextBox ID="DELETE_DATE" runat="server" />
            </ItemTemplate>
        </eq:eqSpecificationColumn>
        <eq:eqSpecificationColumn HeaderText="Примечание">
            <ItemTemplate>
                <asp:TextBox ID="NOTE" runat="server" />
            </ItemTemplate>
        </eq:eqSpecificationColumn>
    </eq:eqSpecification>
</fieldset>
