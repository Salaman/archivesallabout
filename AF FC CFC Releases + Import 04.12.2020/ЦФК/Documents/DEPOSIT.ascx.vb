﻿Imports System.Data
Imports System.Data.SqlClient

Partial Public Class DEPOSIT
    Inherits BaseDoc

#Region "События"

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        For Each r In vREF_ACT_FOR_DEPOSIT.Rows
            DirectCast(r.FindControl("ISN_ACT"), eqDocument).OptionalQueryString = "&DEPOSIT=" & DocID.ToString & "&ACT_OBJ=707"
        Next
    End Sub

    Private Sub Page_PreRender1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Crumbs.ISN_FUND.DocID <> Guid.Empty AndAlso Not IsDBNull(Crumbs.ISN_FUND.DataSetHeaderDataRow("ISN_ARCHIVE")) Then
            Crumbs.ISN_ARCHIVE.KeyValue = Crumbs.ISN_FUND.DataSetHeaderDataRow("ISN_ARCHIVE")
        End If

        vREF_ACT_FOR_DEPOSIT.Enabled = DocStateInfo.StateID <> Guid.Empty
    End Sub

#End Region

#Region "Документ"

    Public Overrides Function NewDoc() As Boolean
        NewDoc = MyBase.NewDoc()
        If NewDoc Then
            If Request.QueryString("FUND") IsNot Nothing Then
                Crumbs.ISN_FUND.DocID = New Guid(Request.QueryString("FUND"))
            End If
        End If
    End Function

#End Region

#Region "Контролы"

    Public Overrides Sub PutValueToControl(ByVal Value As Object, ByVal TargetControl As System.Web.UI.Control, Optional ByVal FieldFormat As String = "")
        If TypeOf TargetControl Is CheckBox Then
            DirectCast(TargetControl, CheckBox).Checked = (Value Is Nothing OrElse Value.ToString.Trim.ToUpper = "Y")
        Else
            MyBase.PutValueToControl(Value, TargetControl, FieldFormat)
        End If
    End Sub

    Public Overrides Function GetValueFromControl(ByVal SourceControl As System.Web.UI.Control) As Object
        If TypeOf SourceControl Is CheckBox Then
            Return IIf(DirectCast(SourceControl, CheckBox).Checked, "Y", "N")
        Else
            Return MyBase.GetValueFromControl(SourceControl)
        End If
    End Function

#End Region

End Class