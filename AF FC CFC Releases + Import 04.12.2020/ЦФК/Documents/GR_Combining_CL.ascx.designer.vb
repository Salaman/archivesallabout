﻿'------------------------------------------------------------------------------
' <auto-generated>
'     Этот код создан программой.
'     Исполняемая версия:2.0.50727.5485
'
'     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
'     повторной генерации кода.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On



Partial Public Class GR_Combining_CL

    '''<summary>
    '''DocType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents DocType As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ButtonGetCL control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ButtonGetCL As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Panel_Language control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Panel_Language As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Language_Name_cl control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Language_Name_cl As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''GR_vClassificator_Language control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents GR_vClassificator_Language As Global.Equipage.WebUI.eqSpecification

    '''<summary>
    '''Panel_Author control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Panel_Author As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Author_Name control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Author_Name As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''GR_vClassificator_Author control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents GR_vClassificator_Author As Global.Equipage.WebUI.eqSpecification

    '''<summary>
    '''Panel_Kind control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Panel_Kind As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Kind_Name control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Kind_Name As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''GR_vClassificator_Kind control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents GR_vClassificator_Kind As Global.Equipage.WebUI.eqSpecification

    '''<summary>
    '''Panel_Carrier control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Panel_Carrier As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Carrier_Name control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Carrier_Name As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''GR_vClassificator_Carrier control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents GR_vClassificator_Carrier As Global.Equipage.WebUI.eqSpecification

    '''<summary>
    '''Button_Combining control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Button_Combining As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Panel_CombiningOK control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Panel_CombiningOK As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Label_Message control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Label_Message As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Button_NewCombining control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Button_NewCombining As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Button_CombiningOK control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Button_CombiningOK As Global.System.Web.UI.WebControls.Button
End Class
