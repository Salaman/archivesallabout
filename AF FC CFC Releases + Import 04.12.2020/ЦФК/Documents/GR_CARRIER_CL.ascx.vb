﻿Public Partial Class GR_CARRIER_CL
    Inherits BaseDoc

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Request.QueryString("ParentID") IsNot Nothing Then
                ID_HIGH_CARRIER.DocID = New Guid(Request.QueryString("ParentID"))
            End If
        End If
    End Sub
    Private Sub clearID_HIGH_CARRIER_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles clearID_HIGH_CARRIER.Click
        ID_HIGH_CARRIER.DocID = Guid.Empty
    End Sub
End Class