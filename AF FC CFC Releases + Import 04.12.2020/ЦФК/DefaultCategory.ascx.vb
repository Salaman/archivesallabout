﻿Partial Public Class DefaultCategory
    Inherits BaseUserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        ' CategoryInfo
        Dim c = AppSettings.GetCategory(BasePage.CategoryID)
        CategoryName.Text = c.CategoryName
        CategoryDescription.Text = c.ToolTip

        CategoryDocuments.Controls.Add(New LiteralControl("<table>"))

        ' DocTypes
        Dim DocTypes = From DT As eqDocType In AppSettings.DocTypes.Values _
                       Where AppSettings.UserSecurities.HasDocumentsAccessMenu(DT.DocTypeID) _
                       Order By DT.SortOrder _
                       Where DT.Category.ID = BasePage.CategoryID
        For Each DocType In DocTypes
            ' Создаем кнопку
            Dim DocTypeButton As New HyperLink With {.ID = "DocTypeButton" & DocType.URL}
            If DocType.IsManaged Then
                If _Default.NavigateMode = _Default.NavigateModes.Redirect Then
                    If DocType.OpenMode = "L" Then
                        ' List mode
                        DocTypeButton.NavigateUrl = BasePage.GetListDocURL(DocType.URL, False)
                    Else
                        ' Document mode
                        DocTypeButton.NavigateUrl = BasePage.GetNewDocURL(DocType.URL, False)
                    End If
                Else
                    DocTypeButton.NavigateUrl = Page.ClientScript.GetPostBackClientHyperlink(Me, "DocType|" & DocType.URL & "|OpenMode|" & DocType.OpenMode)
                End If
            Else
                DocTypeButton.NavigateUrl = DocType.URL
            End If
            DocTypeButton.Text = DocType.DocTypeName
            DocTypeButton.ToolTip = DocType.ToolTip
            ' Создаем открывашку
            Dim OpenDocByNumControl As OpenDocByNum = Nothing
            If DocType.GetFieldInfo(DocType.GetHeaderTableName, "DocNum") IsNot Nothing Then
                OpenDocByNumControl = LoadControl("~/OpenDocByNum.ascx")
                OpenDocByNumControl.DocTypeURL = DocType.URL
            End If
            ' Добавляем
            CategoryDocuments.Controls.Add(New LiteralControl("<tr><td>"))
            CategoryDocuments.Controls.Add(DocTypeButton)
            CategoryDocuments.Controls.Add(New LiteralControl("</td><td>"))
            If OpenDocByNumControl IsNot Nothing Then CategoryDocuments.Controls.Add(OpenDocByNumControl)
            CategoryDocuments.Controls.Add(New LiteralControl("</td></tr>"))
        Next

        CategoryDocuments.Controls.Add(New LiteralControl("</table>"))

        PHChilds.Visible = DocTypes.Count > 0

    End Sub

    Public Overrides Sub RaisePostBackEvent(ByVal eventArgument As String)
        Dim Values = eventArgument.Split(New Char() {"|"})
        Select Case Values(0)
            Case "DocType"
                BasePage.IsListDoc = Values(3) = "L"
                BasePage.DocTypeURL = Values(1)
            Case "Category"
                BasePage.CategoryID = New Guid(Values(1))
        End Select
    End Sub

End Class