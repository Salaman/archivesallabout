﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="MainPageTextGR.ascx.vb" Inherits="WebApplication.MainPageTextGR" %>
<style type="text/css">
    p.MsoNormal
    {
        margin: 3.0pt 0cm;
        text-align: justify;
        text-indent: 36.0pt;
        font-size: 12.0pt;
        font-family: "Arial" , "sans-serif";
    }
    .style1
    {
        font-size: medium;
    }
    .style2
    {
        font-style: italic;
    }
</style>
<p class="style1" style="line-height: 150%">
    Госреестр уникальных документов Архивного фонда Российской Федерации 2.0
    <br />
</p>
<p class="style2" style="line-height: 150%">
    Государственный реестр уникальных документов Архивного фонда Российской Федерации
</p>