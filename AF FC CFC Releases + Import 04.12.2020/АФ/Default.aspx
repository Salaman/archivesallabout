﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Default.aspx.vb"
    Inherits="WebApplication._Default" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ContentPlaceHolderID="Header" runat="server">
    <asp:Panel ID="PanelSessionMonitor" runat="server">
        <div id="sessionMonitorDiv" style="color: #f00;"></div>
    </asp:Panel>
    <asp:Panel ID="PanelLoggedIn" runat="server" DefaultButton="LogOut">
        <table cellpadding="0" cellspacing="1">
            <tr>
                <td colspan="2" style="font-size: 10px; text-align: left;">
                    <asp:Localize ID="LocalizeUser" runat="server" Text="<%$ Resources:eqResources, CAP_User %>" />
                </td>
            </tr>
            <tr>
                <td style="border: solid 1px gray; background-color: white; padding-left: 5px; padding-right: 5px;">
                    <asp:HyperLink ID="HyperLinkUser" runat="server" SkinID="User" />
                </td>
                <td>
                    <asp:Button ID="LogOut" runat="server" Text="<%$ Resources:eqResources, CAP_Out %>" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="PanelLoggedOut" runat="server" DefaultButton="LogIn">
        <table cellpadding="0" cellspacing="1">
            <tr>
                <td style="font-size: 10px; text-align: left;">
                    <asp:Localize ID="LocalizeLogin" runat="server" Text="<%$ Resources:eqResources, CAP_Login %>" />
                </td>
                <td colspan="2" style="font-size: 10px; text-align: left;">
                    <asp:Localize ID="LocalizePassword" runat="server" Text="<%$ Resources:eqResources, CAP_Password %>" />
                </td>
            </tr>
            <tr>
                <td style="border: solid 1px gray; background-color: white;">
                    <asp:TextBox ID="tbLogin" runat="server" Style="border: none; border-width: 0px;" />
                </td>
                <td style="border: solid 1px gray; background-color: white;">
                    <asp:TextBox ID="tbPassword" runat="server" TextMode="Password" Style="border: none;
                        border-width: 0px;" />
                </td>
                <td>
                    <asp:Button ID="LogIn" runat="server" Text="<%$ Resources:eqResources, CAP_In %>"
                        Height="100%" />
                </td>
            </tr>
            <tr>
                <td style="text-align: left;">
                    <a href="Default.aspx?DocTypeURL=Registration.ascx">
                        <asp:Localize ID="LocalizeRegistration" runat="server" Text="<%$ Resources:eqResources, CAP_Registration %>" /></a>
                </td>
                <td colspan="2" style="text-align: left;">
                    <a id="LinkRemember" runat="server">
                        <asp:Localize ID="LocalizeRemember" runat="server" Text="<%$ Resources:eqResources, CAP_Remember %>" /></a>
                    <aj:PopupControlExtender ID="PopupControlExtenderRemember" runat="server" TargetControlID="LinkRemember"
                        PopupControlID="PanelRemember" Position="Bottom" />
                    <asp:Panel ID="PanelRemember" runat="server" Style="display: none;" Width="150" CssClass="PanelPlaceHolder">
                        <div class="LogicBlockCaption">
                            <asp:Localize ID="LocalizeEnterYourEmail" runat="server" Text="<%$ Resources:eqResources, CAP_EnterYourEmail %>" />
                        </div>
                        <eq:eqTextBox ID="EmailRemember" runat="server" />
                        <asp:Button ID="SendRemember" runat="server" Text="<%$ Resources:eqResources, CAP_SendRemember %>"
                            Width="100%" Style="margin-top: 2px;" />
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="PanelFakeUser" runat="server">
        <table cellpadding="0" cellspacing="1">
            <tr>
                <td style="font-size: 10px; text-align: left;">
                    <asp:Label ID="FakeUserLabel" runat="server" />
                </td>
                <td style="text-align: left;">
                    <eq:eqDocument ID="FakeUserID" ListDocMode="NewWindowAndSelect" runat="server" ShowingFields="DisplayName"
                        ShowingFieldsFormat="{0}" DocTypeURL="Users.ascx" LoadDocMode="NewWindowAndSelect" />
                </td>
                <td colspan="1">
                    <asp:LinkButton ID="FakeUserLogOut" runat="server" Text="Вернуться" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="PanelCheckBackup" runat="server" Visible="false">
        <asp:Label ID="LabelCheckBackup" runat="server" Font-Bold="true" ForeColor="Red" />
        <asp:HyperLink ID="LinkCheckButton" runat="server" Text="Резервное копирование/восстановление" />
    </asp:Panel>
</asp:Content>
<asp:Content ContentPlaceHolderID="Body" runat="server">
    <asp:Panel ID="PanelContentPlaceHolder" runat="server" DefaultButton="ButtonContentPlaceHolder">
        <asp:Button ID="ButtonContentPlaceHolder" runat="server" Style="display: none;" />
        <asp:Table ID="TableSystemMenuDocument" runat="server" Width="100%" BorderWidth="0px">
            <asp:TableRow>
                <asp:TableCell ID="TableCellSystemMenu" runat="server" VerticalAlign="Top" Width="10px">
                    <asp:UpdatePanel ID="UpdatePanelMenuSystem" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:ImageButton ID="ShowHideButton" runat="server" Style="margin: 3px 0px 0px 3px;" />
                            <eq:eqMenu ID="MenuSystem" runat="server" Width="170px" Style="margin: 0px;" Orientation="Vertical"
                                StaticDisplayLevels="1" MaximumDynamicDisplayLevels="20">
                                <StaticSelectedStyle CssClass="MenuItemStyleSelected" />
                                <DynamicSelectedStyle CssClass="MenuItemStyleSelected" />
                            </eq:eqMenu>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:TableCell>
                <asp:TableCell ID="TableCellDocument" runat="server" VerticalAlign="Top">
                    <div id="UpdatePanelPlaceHolderContentContainer" style="width: 100%;">
                        <asp:UpdatePanel ID="UpdatePanelPlaceHolderContainer" runat="server" RenderMode="Inline"
                            UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:PlaceHolder ID="PlaceHolderContainer" runat="server" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
        <asp:UpdatePanel ID="UpdatePanelModalMessages" runat="server" RenderMode="Inline"
            UpdateMode="Conditional">
            <ContentTemplate>
                <asp:PlaceHolder ID="PlaceHolderModalMessages" runat="server" Visible="false">
                    <asp:Panel ID="PanelMessages" runat="server" Style="display: none;" Width="600">
                        <asp:Panel ID="PanelMessagesTitle" runat="server" CssClass="PanelInfo">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="cursor: default;">
                                        <asp:Label ID="LabelMessagesTitle" runat="server" Text="<%$ Resources:eqResources, CAP_Messages %>" />
                                    </td>
                                    <td align="right">
                                        <asp:ImageButton ImageAlign="Right" ID="ImageButtonMessagesOk" runat="server" ImageUrl="App_Themes/DefaultTheme/Images/Delete.gif"
                                            ToolTip="<%$ Resources:eqResources, CAP_Close %>" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <asp:Panel ID="PanelMessagesContent" runat="server" Height="400" ScrollBars="Auto" CssClass="PanelPlaceHolder">
                            <asp:PlaceHolder ID="PlaceHolderMessages" runat="server" />
                        </asp:Panel>
                        <asp:Panel ID="PanelMessagesButton" runat="server" Height="50" CssClass="PanelPlaceHolder" style="text-align: center; padding: 10px;" Visible="false">
                            <asp:PlaceHolder ID="PlaceHolderButtons" runat="server" />
                        </asp:Panel>
                    </asp:Panel>
                    <asp:Button ID="ButtonShowMessages" Style="display: none;" runat="server" />
                    <aj:ModalPopupExtender ID="ModalPopupExtenderMessages" runat="server" PopupDragHandleControlID="PanelMessagesTitle"
                        TargetControlID="ButtonShowMessages" BackgroundCssClass="ModalBackground" DropShadow="true"
                        OkControlID="ImageButtonMessagesOk" PopupControlID="PanelMessages">
                    </aj:ModalPopupExtender>
                </asp:PlaceHolder>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="UpdatePanelModalDialog" runat="server" RenderMode="Inline" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:PlaceHolder ID="PlaceHolderModalDialog" runat="server">
                    <asp:Panel ID="PanelDialog" runat="server" Style="display: none;" Width="600">
                        <asp:Panel ID="PanelDialogTitle" runat="server" CssClass="PanelInfo">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="cursor: default;">
                                        <asp:Label ID="LabelDialogTitle" runat="server" Text="Модальный диалог" />
                                    </td>
                                    <td align="right">
                                        <asp:ImageButton ImageAlign="Right" ID="ImageButtonDialogOk" runat="server" ImageUrl="App_Themes/DefaultTheme/Images/Delete.gif"
                                            ToolTip="<%$ Resources:eqResources, CAP_Close %>" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <asp:Panel ID="PanelDialogContent" runat="server" Height="400" ScrollBars="Auto"
                            CssClass="PanelPlaceHolder" Style="padding: 0px;">
                        </asp:Panel>
                    </asp:Panel>
                    <asp:Button ID="ButtonShowDialog" Style="display: none;" runat="server" />
                    <aj:ModalPopupExtender ID="ModalPopupExtenderDialog" runat="server" PopupDragHandleControlID="PanelDialogTitle"
                        TargetControlID="ButtonShowDialog" BackgroundCssClass="ModalBackground" DropShadow="true"
                        OkControlID="ImageButtonDialogOk" PopupControlID="PanelDialog">
                    </aj:ModalPopupExtender>
                </asp:PlaceHolder>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
