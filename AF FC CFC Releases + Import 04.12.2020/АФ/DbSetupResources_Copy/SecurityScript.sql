﻿-- Добавляеи разрешения на IIS

-- ASPNET security

CREATE LOGIN [#ServerName#\ASPNET] FROM WINDOWS WITH DEFAULT_DATABASE = [master]

GO

USE [#DataBaseName#]

GO

CREATE USER [#ServerName#\ASPNET] FOR LOGIN [#ServerName#\ASPNET]
EXEC sp_addrolemember 'db_owner','#ServerName#\ASPNET'

GO

USE [master]

GO

GRANT CREATE DATABASE TO [#ServerName#\ASPNET]

GO

USE [master]

GO

EXEC sp_addsrvrolemember '#ServerName#\ASPNET', 'dbcreator'

GO

-- IIS APPPOOL security

CREATE LOGIN [IIS APPPOOL\DefaultAppPool] FROM WINDOWS WITH DEFAULT_DATABASE = [master]

GO

USE [#DataBaseName#]

GO

CREATE USER [IIS APPPOOL\DefaultAppPool] FOR LOGIN [IIS APPPOOL\DefaultAppPool]
EXEC sp_addrolemember 'db_owner','IIS APPPOOL\DefaultAppPool'

GO

USE [master]

GO

GRANT CREATE DATABASE TO [IIS APPPOOL\DefaultAppPool]

GO

USE [master]

GO

EXEC sp_addsrvrolemember 'IIS APPPOOL\DefaultAppPool', 'dbcreator'
