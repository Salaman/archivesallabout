﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InventoryStructureChoose.aspx.vb" Inherits="WebApplication.InventoryStructureChoose" %>
<%@ Register Src="ListDoc.ascx" TagName="ListDoc" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Выбор раздела описи</title>
</head>
<body style="background-color: White; padding: 5px;">
    <form id="form1" runat="server">
    <aj:ToolkitScriptManager ID="ToolkitScriptManagerDefault" runat="server" EnableScriptGlobalization="true"
        ScriptMode="Release" EnablePartialRendering="True" EnableHistory="true" />
    <table style="width: 100%;">
        <tr>
            <td>
                <div class="LogicBlockCaption">
                    <asp:Label id="LableTitle" Text="Выберите раздел описи, к которому хотели бы прикрепить перемещаемые объекты учета." runat="server"/>
                </div>
            </td>

            <td style="text-align: right;">
                <asp:Button ID="ButtonRefuse" runat="server" Text="Не прикреплять" />
            </td>

        </tr>
    </table>

    <asp:UpdatePanel ID="UpdatePanelInventoryStructure" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <uc1:ListDoc ID="ListDocInventoryStructure" runat="server" DocTypeURL= "INVENTORYSTRUCTURE.ascx" SelectDocMode="Single" />
        </ContentTemplate>
    </asp:UpdatePanel> 
    <table style="width: 100%;">
    <tr><td>   <asp:Button ID="ButtonClear" runat="server" Text="Открепить от всех разделов" /> </td>    </tr>
    </table>
    
    </form>
</body>
</html>
