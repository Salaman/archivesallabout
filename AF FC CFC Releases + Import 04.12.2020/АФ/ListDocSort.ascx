﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ListDocSort.ascx.vb"
    Inherits="WebApplication.ListDocSort" %>
<table width="100%" cellpadding="0" cellspacing="1" style="background-color: #e0e2dc;
    border: solid 1px gray;">
    <tr>
        <td>
            <asp:ImageButton ID="SortAsc" runat="server" AlternateText=">" ToolTip="Сортировка по возрастанию" />
        </td>
        <td>
            <asp:ImageButton ID="SortDesc" runat="server" AlternateText="<" ToolTip="Сортировка по убыванию" />
        </td>
    </tr>
</table>
