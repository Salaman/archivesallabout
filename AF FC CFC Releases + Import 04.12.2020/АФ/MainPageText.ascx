﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="MainPageText.ascx.vb"
    Inherits="WebApplication.MainPageText" %>
<style type="text/css">
    p.MsoNormal
    {
        margin: 3.0pt 0cm;
        text-align: justify;
        text-indent: 36.0pt;
        font-size: 12.0pt;
        font-family: "Arial" , "sans-serif";
    }
    .style1
    {
        font-size: medium;
    }
    .style2
    {
        font-style: italic;
    }
</style>
<p class="style1" style="line-height: 150%">
    Архивный фонд 5.0
    <br />
</p>
<p class="style2" style="line-height: 150%">
    Система, обеспечивающая поддержку централизованного государственного учета, научно-справочного
    аппарата и контроля за обеспечением сохранности документов в уполномоченных органах
    исполнительной власти субъектов Российской Федерации в области архивного дела.
</p>
