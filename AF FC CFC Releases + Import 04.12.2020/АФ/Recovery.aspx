﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Recovery.aspx.vb" Inherits="WebApplication.Recovery" %>
<%@ Register Src="ListDoc.ascx" TagName="ListDoc" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">

    <script type="text/javascript" src="jquery-1.6.min.js"></script>

    <script type="text/javascript" src="jquery-ui-1.8.12.min.js"></script>

    <title>Восстановление</title>
</head>
<body style="background-color: White; padding: 5px;">
    <form id="form1" runat="server">
    <aj:ToolkitScriptManager ID="ToolkitScriptManagerDefault" runat="server" EnableScriptGlobalization="true"
        ScriptMode="Release" EnablePartialRendering="True" EnableHistory="true" />
    <table style="width: 100%;">
        <tr>
            <td>
                <div class="LogicBlockCaption">
                    Выберите объекты, которые требуется восстановить.
                </div>
            </td>

            
            <td style="text-align: right;">
                <asp:Button ID="ButtonRecovery" runat="server" Text="Восстановить" />
            </td>
            
        </tr>
    </table>
    <table id="InvStructOptions" style="width: 100%;" runat=server>
            <tr>
            <td>
            <div class="LogicBlockCaption">
                    Нажмите, чтобы выбрать все дочерние элементы:
                </div>
            </td>
             <td style="text-align: right;">
                 <asp:Button ID="ButtonSelectChildren" runat="server" Text="Выбрать дочерние элементы" />
             </td>
                
        </tr>
    </table>
    <asp:UpdatePanel ID="UpdatePanelRecovery" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <uc1:ListDoc ID="ListDocRecovery" runat="server" DocTypeURL= "INVENTORY.ascx" SelectDocMode="Multi" />
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>

