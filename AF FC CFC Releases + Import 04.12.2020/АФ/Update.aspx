﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"
    CodeBehind="Update.aspx.vb" Inherits="WebApplication.Update" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ContentPlaceHolderID="Header" runat="server">
</asp:Content>
<asp:Content ContentPlaceHolderID="Body" runat="server">
    <asp:PlaceHolder ID="phMessages" runat="server"/>
    <asp:PlaceHolder ID="phRestrictedContent" runat="server">
        <table width="100%">
            <tr>
                <td>
                    <div class="LogicBlockCaption">
                        Apply update from script
                    </div>
                    <table width="100%" class="LogicBlockTable">
                        <colgroup>
                            <col style="width: 150px;" />
                            <col />
                        </colgroup>
                        <tr>
                            <td>
                                Update from script file
                            </td>
                            <td>
                                <asp:FileUpload ID="UpdateFromScriptFile" runat="server" />
                                <asp:Button ID="ApplyUpdateFromScriptFile" runat="server" Text="Apply" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Update options
                            </td>
                            <td>
                                <asp:CheckBox ID="DontDelete" runat="server" Text="Don't delete" Checked="true" />
                                <asp:CheckBox ID="NoSqlUpdates" runat="server" Text="Don't exec sql updates" />
                            </td>
                        </tr>
                    </table>
                    <div class="LogicBlockCaption">
                        Generate update script
                    </div>
                    <table width="100%" class="LogicBlockTable">
                        <colgroup>
                            <col style="width: 150px;" />
                            <col />
                        </colgroup>
                        <tr>
                            <td>
                                Update from script
                            </td>
                            <td>
                                <asp:Button ID="GenerateUpdateFromScript" runat="server" Text="Generate" />
                                <asp:CheckBox ID="Compressed" runat="server" Text="Compressed" Checked="true" />
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                Included Update Tables
                            </td>
                            <td>
                                <asp:TreeView ID="IncludedUpdateTables" runat="server" ShowCheckBoxes="Leaf" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:PlaceHolder>
</asp:Content>
