﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ContainerMini.ascx.vb"
    Inherits="WebApplication.ContainerMini" %>
<asp:LinkButton ID="Action" runat="server" Text="<%$ Resources:eqResources, CAP_Action %>" />
<asp:Panel ID="PanelPlaceHolder" runat="server" style="padding-top: 2px;" Visible="false">
    <asp:PlaceHolder ID="PlaceHolderContent" runat="server" />
</asp:Panel>
