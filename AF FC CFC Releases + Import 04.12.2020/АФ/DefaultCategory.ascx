﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="DefaultCategory.ascx.vb"
    Inherits="WebApplication.DefaultCategory" %>
<div class="PanelInfo">
    <asp:Localize ID="LocalizeUser" runat="server" Text="<%$ Resources:eqResources, CAP_CurrentCategory %>" />
    <asp:Label ID="CategoryName" runat="server" />
</div>
<div class="PanelPlaceHolder">
    <div class="LogicBlockCaption">
        <asp:Localize ID="LocalizeDescription" runat="server" Text="<%$ Resources:eqResources, CAP_Description %>" />
    </div>
    <div class="LogicBlock" style="padding-left: 10px;">
        <p>
            <asp:Label ID="CategoryDescription" runat="server" />
        </p>
    </div>
    <asp:PlaceHolder ID="PHChilds" runat="server">
        <div class="LogicBlockCaption">
            <asp:Localize ID="LocalizeDocuments" runat="server" Text="<%$ Resources:eqResources, CAP_Documents %>" />
        </div>
        <div class="LogicBlock" style="padding-left: 10px;">
            <asp:PlaceHolder ID="CategoryDocuments" runat="server" />
        </div>
    </asp:PlaceHolder>
</div>
