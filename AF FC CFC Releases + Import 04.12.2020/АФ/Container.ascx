﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Container.ascx.vb"
    Inherits="WebApplication.Container" %>
<asp:Panel ID="PanelInfo" runat="server" CssClass="PanelInfo">
    <asp:Label ID="LabelInfo" runat="server" />
</asp:Panel>
<asp:Panel ID="PanelMenu" runat="server" CssClass="PanelMenu">
    <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <eq:eqMenu ID="MenuActions" runat="server" StaticMenuItemStyle-ItemSpacing="2px"
                    Orientation="Horizontal" StaticDisplayLevels="1" ItemWrap="false" HorizontalMaxCount="8">
                    <StaticSelectedStyle CssClass="MenuItemStyleSelected" />
                    <DynamicSelectedStyle CssClass="MenuItemStyleSelected" />
                </eq:eqMenu>
            </td>
            <td style="text-align: right; padding-right: 10px; ">
                <asp:ImageButton ID="ImageButtonFirst" runat="server" ImageUrl="~/App_Themes/VTBTheme/Images/first.png"></asp:ImageButton>
                <asp:ImageButton ID="ImageButtonPrevious" runat="server" ImageUrl="~/App_Themes/VTBTheme/Images/previous.png"></asp:ImageButton>
                <asp:ImageButton ID="ImageButtonNext" runat="server" ImageUrl="~/App_Themes/VTBTheme/Images/next.png"></asp:ImageButton>
                <asp:ImageButton ID="ImageButtonLast" runat="server" ImageUrl="~/App_Themes/VTBTheme/Images/last.png"></asp:ImageButton>
            </td>
        </tr>
    </table>
</asp:Panel>
<div id="divDoc" style="overflow: auto; min-width: 830px;">
    <asp:Panel ID="PanelPlaceHolder" runat="server" CssClass="PanelPlaceHolder">
        <div id="divLevel" runat="server">
            <eq2:ArchiveCrumbs ID="Crumbs" runat="server" />
        </div>
        <asp:PlaceHolder ID="PlaceHolderContent" runat="server" />
    </asp:Panel>
</div>

<script type="text/javascript">
    function SetDocHeight() { document.getElementById('divDoc').style.height = (GetWindowHeight() - <%=IIF(Request.Querystring("Modal") is Nothing,"200","91") %>) + 'px'; };
</script>

<%
    ScriptManager.RegisterStartupScript(Page, Page.GetType, "SetDocHeight", "SetDocHeight(); $addHandler(window,'resize',SetDocHeight);", True)
%>
<asp:PlaceHolder ID="PlaceHolderShowConfirm" runat="server" Visible="false">
    <asp:Panel ID="PanelConfirm" runat="server" Style="display: none;" Width="300">
        <asp:Panel ID="PanelConfirmTitle" runat="server" CssClass="PanelInfo">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="cursor: default;">
                        <asp:Label ID="LabelConfirmTitle" runat="server" Text="<%$ Resources:eqResources, CAP_Confirm %>" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel ID="PanelConfirmContent" runat="server" ScrollBars="Auto" CssClass="PanelPlaceHolder">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td colspan="2">
                        <asp:PlaceHolder ID="PlaceHolderConfirm" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td style="padding: 2px 2px 2px 2px; width: 50%;">
                        <asp:Button ID="ButtonConfirmOk" runat="server" Text="<%$ Resources:eqResources, CAP_ConfirmYes %>"
                            ToolTip="<%$ Resources:eqResources, TIP_ConfirmYes %>" Width="100%" />
                    </td>
                    <td style="padding: 2px 2px 2px 2px; widows: 50%;">
                        <asp:Button ID="ButtonConfirmCancel" runat="server" Text="<%$ Resources:eqResources, CAP_ConfirmNo %>"
                            ToolTip="<%$ Resources:eqResources, TIP_ConfirmNo %>" Width="100%" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </asp:Panel>
    <asp:Button ID="ButtonShowConfirm" Style="display: none;" runat="server" />
    <aj:ModalPopupExtender ID="ModalPopupExtenderConfirm" runat="server" PopupDragHandleControlID="PanelConfirmTitle"
        TargetControlID="ButtonShowConfirm" BackgroundCssClass="ModalBackground" DropShadow="true"
        OkControlID="ButtonConfirmOk" CancelControlID="ButtonConfirmCancel" PopupControlID="PanelConfirm">
    </aj:ModalPopupExtender>
</asp:PlaceHolder>

<asp:Button ID="SaveDocHistoryTrigger" runat="server" style="display: none;" />
<asp:HiddenField ID="SaveDocHistoryFlag" runat="server" Value="0" />