﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="RecalcInventory.aspx.vb"
    Inherits="WebApplication.RecalcInventory" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
                div.popupConteiner {
                    position: fixed;
                    width: 100%;
                    height: 100%;
                    background-color: rgba(0,0,0,0.5);
                    z-index: 999;
                    display: table;
                    left: 0;
                    top: 0;
                }
                div.popupCenter {
                    display: table-cell;
                    vertical-align: middle;
                }
                div.popup {
                    display: table;
                    border: 1px solid black;
                    background-color: #fff;
                    color: black;
                    margin: 0 auto;
                    padding: 5px;
                    text-align: center;
                }
                div.popup p {color: black; text-align : center;}
    </style>
    <script type="text/javascript" src="jquery-1.6.min.js"></script>

</head>
<body style="background-color: White; padding: 5px;">

    <script type="text/javascript">
        sel = function(obj) {
            var scope = $(obj).parents('table').first();
            $(':input', scope).prop('checked', true);
            inv();
        }
        des = function(obj) {
            var scope = $(obj).parents('table').first();
            $(':input', scope).removeProp('checked');
        }
        inv = function(obj) {
            var c1 = $(':input[id*=c_E_ALL]').eq(0);
            var c2 = $(':input[id*=c_E_ALL]').eq(1);
            if (c1.prop('checked') && c2.prop('checked')) {
                if (obj != undefined && $(obj).prop('id') == c1.prop('id')) {
                    c2.removeProp('checked');
                    c1.prop('checked', true);
                } else {
                    c1.removeProp('checked');
                    c2.prop('checked', true);
                }
            }
        }
    </script>

    <form id="form1" runat="server">
    <asp:Panel ID = "worningPanel" runat = "server">
         <div class="popupConteiner">
            <div class="popupCenter">
                <div class="popup">
                    <p>Создавать ли резервную копию перед запуском пересчета?</p>
                    <p>
                        <asp:Button ID="worningAnsYes" runat="server" Text="Создать резервную копию" />
                        <asp:Button ID="worningAnsNo" runat="server" Text="Пересчитывать без резервирования" />
                        <asp:Button ID="worningAnsCancel" runat="server" Text="Отказаться от пересчета" />
                    </p>
                    <asp:Label ID="worningLabel" Visible="false" runat="server"></asp:Label>
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:ScriptManager runat="server">
    </asp:ScriptManager>
    <eq:eqMenu ID="MenuTabs" runat="server" Orientation="Horizontal" StaticEnableDefaultPopOutImage="False"
        ItemWrap="false" HorizontalMaxCount="3">
        <Items>
            <asp:MenuItem Text="Общие сведения" Value="0" Selected="true" />
            <asp:MenuItem Text="Количественные характеристики" Value="1" />
            <asp:MenuItem Text="Количественные характеристики для каждого типа документов" Value="2" />
            <asp:MenuItem Text="Характеристики физического состояния" Value="3" />
            <asp:MenuItem Text="Топография" Value="4" />
        </Items>
        <StaticSelectedStyle CssClass="MenuItemStyleSelected" />
    </eq:eqMenu>
    <asp:MultiView ID="MultiViewTabs" runat="server" ActiveViewIndex="0">
        <asp:View runat="server">
            <table>
                <tr>
                    <td colspan="2">
                        <a href="#" onclick="sel(this);">Выделить все</a>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <a href="#" onclick="des(this);">Снять выделение</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="c_A_NACH_DATA" Text="Начальная дата документов" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="c_A_MUSEUM" Text="Наличие музейных предметов" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="c_A_KON_DATA" Text="Конечная дата документов" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="c_A_TREASURE" Text="Кол.-во ед. хр. с драг. камнями и металлами в оформлении" />
                    </td>
                </tr>
            </table>
        </asp:View>
        <asp:View runat="server">
            <table>
                <tr>
                    <td colspan="2">
                        <a href="#" onclick="sel(this);">Выделить все</a>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <a href="#" onclick="des(this);">Снять выделение</a>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:CheckBox runat="server" ID="c_SUM_AA" Text="Всех видов документов на всех носителях" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:CheckBox runat="server" ID="c_SUM_BO" Text="Документов на бумажной основе" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:CheckBox runat="server" ID="c_SUM_TN" Text="Аудиовизуальных документов на традиционных носителях" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:CheckBox runat="server" ID="c_SUM_EN" Text="Документов на электронных носителях" />
                    </td>
                </tr>
            </table>
        </asp:View>
        <asp:View runat="server">
            <table>
                <tr>
                    <td colspan="2">
                        <a href="#" onclick="sel(this);">Выделить все</a>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <a href="#" onclick="des(this);">Снять выделение</a>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:CheckBox runat="server" ID="c_E_ALL_DV" Text="Всего единиц хранения в соответствии с разделом 'Движение документов'" onclick="inv(this);" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="c_E_ALL" Text="Всего единиц хранения" onclick="inv(this);" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="c_U_ALL" Text="Всего единиц учета" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="c_E_REG" Text="Количество зарегистрированных единиц хранения" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="c_U_REG" Text="Количество зарегистрированных единиц учета" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="c_E_OC" Text="Количество особо ценных единиц хранения" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="c_U_OC" Text="Количество особо ценных единиц учета" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="c_E_UN" Text="Количество уникальных единиц хранения" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="c_U_UN" Text="Количество уникальных единиц учета" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="c_E_SF" Text="Количество единиц хранения, имеющих СФ" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="c_U_SF" Text="Количество единиц учета, имеющих СФ" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="c_E_FP" Text="Количество единиц хранения, имеющих ФП" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="c_U_FP" Text="Количество единиц учета, имеющих ФП" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="c_E_NEO" Text="Количество необнаруженных единиц хранения" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="c_U_NEO" Text="Количество необнаруженных единиц учета" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="c_E_SEC" Text="Количество секретных единиц хранения" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="c_U_SEC" Text="Количество секретных единиц учета" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="c_E_CAT" Text="Количество закаталогизированных единиц хранения" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="c_U_CAT" Text="Количество закаталогизированных единиц учета" />
                    </td>
                </tr>
            </table>
        </asp:View>
        <asp:View runat="server">
            <table>
                <tr>
                    <td colspan="2">
                        <a href="#" onclick="sel(this);">Выделить все</a>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <a href="#" onclick="des(this);">Снять выделение</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="c_E_KARTED" Text="Количество закартонированных единиц хранения" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="c_E_SHIFR" Text="Количество единиц хранения, требующих шифровки" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="c_E_KART" Text="Количество единиц хранения, требующих картонизации" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="c_E_ZAM_OBL" Text="Количество единиц хранения, требующих замены обложки" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="c_E_POVR" Text="Количество неисправимо поврежденных единиц хранения" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="c_E_ZAT" Text="Количество листов с затухающими текстами" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="c_E_RESTAVR" Text="Количество единиц хранения, требующих реставрации" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="c_E_GOR" Text="Количество единиц хранения на горючей основе" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="c_E_PODSH" Text="Количество единиц хранения, требующих подшивки и переплета" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="c_E_KPO" Text="Количество единиц хранения, требующих КПО" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="c_E_DEZINF" Text="Количество единиц хранения, требующих дезинфекции" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="c_E_RAB" Text="Работы, проводимые с документами описи" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:CheckBox runat="server" ID="c_E_DEZINS" Text="Количество единиц хранения, требующих дезинсекции" />
                    </td>
                </tr>
            </table>
        </asp:View>
        <asp:View runat="server">
            <table>
                <tr>
                    <td colspan="2">
                        <asp:CheckBox runat="server" ID="c_E_TOPO" Text="Топография и номера единиц хранения" />
                    </td>
                </tr>
            </table>
        </asp:View>
    </asp:MultiView>
    <table style="padding: 10px;">
        <tr>
            <td>
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <asp:Button runat="server" ID="bGo" Text="Пересчитать" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>
                <asp:Button runat="server" ID="bClose" Text="Закрыть" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:UpdateProgress runat="server">
                    <ProgressTemplate>
                        <asp:Image runat="server" SkinID="UpdateProgress" />
                        <br />
                        <asp:Label runat="server" Text="Идет расчет..." />
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
