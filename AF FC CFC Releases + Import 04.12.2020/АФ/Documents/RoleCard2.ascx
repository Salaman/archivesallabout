﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="RoleCard2.ascx.vb"
    Inherits="WebApplication.RoleCard2" %>
<div class="LogicBlockCaption">
    Роль
    <asp:CheckBox ID="BUILD_IN_ROLE" runat="server" Text="Встороенная" Enabled="false"
        Visible="false" />
</div>
<table width="100%" class="LogicBlockTable">
    <colgroup>
        <col style="width: 150px;" />
        <col />
    </colgroup>
    <tr>
        <td>
            Название
        </td>
        <td>
            <eq:eqTextBox ID="RoleName" runat="server" Width="100%" />
        </td>
    </tr>
    <tr>
        <td>
            Описание
        </td>
        <td>
            <eq:eqTextBox ID="Description" runat="server" Width="100%" TextMode="MultiLine" Rows="3" />
        </td>
    </tr>
    <tr>
        <td>
            Стартовая страница
        </td>
        <td>
            <eq:eqTextBox ID="StartPage" runat="server" Width="100%" />
        </td>
    </tr>
</table>
<div class="LogicBlockCaption">
    Предоставление доступа к документам системы
</div>
<table width="100%" class="LogicBlockTable">
    <colgroup>
        <col style="width: 150px;" />
        <col />
    </colgroup>
    <tr>
        <td>
            Выберите документ
        </td>
        <td>
            <eq:eqDropDownList ID="RolesDocTypeID" runat="server" AutoPostBack="True" />
        </td>
    </tr>
    <tr>
        <td>
            Доступ
        </td>
        <td>
            <asp:CheckBox ID="Menu" Text="Меню" runat="server" />
            <asp:CheckBox ID="List" Text="Список" runat="server" />
            <asp:CheckBox ID="Open" Text="Открыть" runat="server" />
            <asp:CheckBox ID="New" Text="Создать" runat="server" />
            <asp:CheckBox ID="Priority" runat="server" />
            <asp:Label runat="server" AssociatedControlID="Priority" ForeColor="Red">
            <%=IIf(AppSettings.RolesAllowPriority, "Запретить", "Разрешить")%>
            </asp:Label>
        </td>
    </tr>
</table>
<div class="LogicBlockCaption">
    Настройка
</div>
<aj:TabContainer runat="server">
    <aj:TabPanel runat="server" HeaderText="Кнопки документа">
        <ContentTemplate>
        </ContentTemplate>
    </aj:TabPanel>
</aj:TabContainer>