﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="DOCUMENT.ascx.vb" Inherits="WebApplication.DOCUMENT" %>

<script type="text/javascript">
    function _updateUI() {
        $(":input[name*=PAGE_FROM]").change(function() {
            $(":input[name*=NOTE]").val($(":input[name*=PAGE_FROM]").val() + ' - ' + $(":input[name*=PAGE_TO]").val());
        });
        $(":input[name*=PAGE_TO]").change(function() {
            $(":input[name*=NOTE]").val($(":input[name*=PAGE_FROM]").val() + ' - ' + $(":input[name*=PAGE_TO]").val());
        });
    }
</script>

<table style="width: 97%; margin-bottom: 4px;">
    <tr>
        <td>
            <eq2:ArchiveCrumbs ID="Crumbs" runat="server" Level="UNIT" />
        </td>
    </tr>
</table>
<eq:eqMenu ID="MenuTabs" runat="server" Orientation="Horizontal" StaticEnableDefaultPopOutImage="False"
    ItemWrap="false" HorizontalMaxCount="5">
    <Items>
        <asp:MenuItem Text="Общие сведения" Value="0" Selected="true" />
        <asp:MenuItem Text="НСА" Value="1" />
    </Items>
    <StaticSelectedStyle CssClass="MenuItemStyleSelected" />
</eq:eqMenu>
<div style="border: solid 1px black; padding: 5px;">
    <asp:MultiView ID="MultiViewTabs" runat="server" ActiveViewIndex="0">
        <%--! Общие сведения--%>
        <asp:View runat="server">
            <table style="width: 700px;">
                <colgroup>
                    <col style="width: 200px;" />
                    <col />
                </colgroup>
                <tr>
                    <td>
                        Номер*:
                    </td>
                    <td>
                        <eq:eqTextBox ID="DOC_NUM" runat="server" Width="200px" />
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        Заголовок*:
                    </td>
                    <td>
                        <eq:eqTextBox ID="NAME" runat="server" TextMode="MultiLine" Rows="4" />
                    </td>
                </tr>
            </table>
            <table style="width: 700px;">
                <colgroup>
                    <col style="width: 200px;" />
                    <col style="width: 100px;" />
                    <col align="left" />
                </colgroup>
                <tr>
                    <td>
                        Дата документа*:
                    </td>
                    <td>
                        <eq:eqTextBox ID="DOCUM_DATE_FMT" runat="server" Width="100px" />
                        <aj:MaskedEditExtender ID = "DOCUM_DATE_FMT_MASK" TargetControlID = "DOCUM_DATE_FMT" runat="server" MaskType="Date" Mask="99/99/9999" ClearTextOnInvalid="true"  />
                    </td>
                    <td>
                        <asp:CheckBox ID="INEXACT_DOCUM_DATE" runat="server" Text="неточная:" TextAlign="Left" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Кол-во листов:
                    </td>
                    <td>
                        <eq:eqTextBox ID="PAGE_COUNT" runat="server" Width="100px" />
                    </td>
                    <td>
                        <fieldset>
                            <legend>Номера листов в интервале</legend>
                            <table>
                                <tr>
                                    <td>
                                        с*:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="PAGE_FROM" runat="server" Width="100px" />
                                        <aj:FilteredTextBoxExtender runat="server" TargetControlID="PAGE_FROM" FilterType="Numbers" />
                                    </td>
                                    <td>
                                        по*:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="PAGE_TO" runat="server" Width="100px" />
                                        <aj:FilteredTextBoxExtender runat="server" TargetControlID="PAGE_TO" FilterType="Numbers" />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
            </table>
            <table style="width: 700px;">
                <colgroup>
                    <col style="width: 200px;" />
                    <col />
                    <col />
                    <col />
                </colgroup>
                <tr>
                    <td>
                        Хар-ка секретности*:
                    </td>
                    <td>
                        <asp:DropDownList ID="ISN_SECURLEVEL" runat="server" />
                    </td>
                    <td>
                        Вид документа*:
                    </td>
                    <td>
                        <%--СПРАВОЧНИК--%>
                        <eq2:eqDocument ID="ISN_DOC_KIND" runat="server" DocTypeURL="DOC_KIND_CL.ascx" ListDocMode="NewWindowAndSelect"
                            LoadDocMode="ListDocAndSelect" ShowingFields="NAME" ShowingFieldsFormat="{0}"
                            KeyName="ISN_DOC_KIND" AdditionalListDocWhereString="SELECT V.ID FROM vDOC_KIND_CL_FOR_DOCUMENT V WHERE V.Deleted = 0" />
                    </td>
                </tr>
            </table>
            <table style="width: 700px;">
                <colgroup>
                    <col style="width: 200px;" />
                    <col />
                </colgroup>
                <tr>
                    <td valign="top">
                        Место события:
                    </td>
                    <td>
                        <eq:eqTextBox ID="EVENT_PLACE" runat="server" TextMode="MultiLine" Rows="4" />
                    </td>
                </tr>
            </table>
            <table style="width: 700px;">
                <colgroup>
                    <col style="width: 200px;" />
                    <col style="width: 100px;" />
                    <col align="left" />
                </colgroup>
                <tr>
                    <td>
                        Дата события:
                    </td>
                    <td>
                        <eq:eqTextBox ID="EVENT_DATE_FMT" runat="server" Width="100px" />
                    </td>
                    <td>
                        <asp:CheckBox ID="INEXACT_EVENT_DATE" runat="server" Text="неточная:" TextAlign="Left" />
                    </td>
                </tr>
            </table>
            <table>
                <colgroup>
                    <col style="width: 200px;" />
                    <col />
                    <col />
                    <col />
                </colgroup>
                <tr>
                    <td>
                        Подлинность:
                    </td>
                    <td>
                        <asp:DropDownList ID="IS_ORIGINAL" runat="server">
                            <asp:ListItem Text="подлинник" Value="a" />
                            <asp:ListItem Text="автограф" Value="b" />
                            <asp:ListItem Text="копия на правах подл." Value="c" />
                            <asp:ListItem Text="копия документов" Value="d" />
                            <asp:ListItem Text="копия с копии" Value="e" />
                            <asp:ListItem Text="факсимальная копия" Value="f" />
                            <asp:ListItem Text="заверенная копия" Value="j" />
                            <%--j - это правдо!--%>
                            <asp:ListItem Text="машинописная копия" Value="h" />
                            <asp:ListItem Text="ксерокопия" Value="i" />
                        </asp:DropDownList>
                    </td>
                    <td>
                        Способ воспроизведения:
                    </td>
                    <td>
                        <%--СПРАВОЧНИК--%>
                        <eq2:eqDocument ID="ISN_REPRODUCTION_METHOD" runat="server" DocTypeURL="REPRODUCTION_METHOD_CL.ascx"
                            ListDocMode="NewWindowAndSelect" LoadDocMode="NewWindowAndSelect" ShowingFields="NAME"
                            ShowingFieldsFormat="{0}" KeyName="ISN_REPRODUCTION_METHOD" />
                    </td>
                </tr>
            </table>
            <table style="width: 700px;">
                <colgroup>
                    <col style="width: 200px;" />
                    <col />
                </colgroup>
                <tr>
                    <td valign="top">
                        Приложения:
                    </td>
                    <td>
                        <eq:eqTextBox ID="ENCLOSURES" runat="server" TextMode="MultiLine" Rows="4" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Язык:
                    </td>
                    <td>
                        <%--СПЕЦИФИКАЦИЯ dbo.REF_LANGUAGE--%>
                        <eq:eqSpecification ID="vREF_LANGUAGE_DOCUMENT" runat="server">
                            <eq:eqSpecificationColumn>
                                <ItemTemplate>
                                    <eq:eqDropDownList ID="ISN_LANGUAGE" runat="server" Width="100%" UniqueGroupName="REF_LANGUAGE" />
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                        </eq:eqSpecification>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <fieldset>
                            <legend>Особенности </legend>
                            <%--СПЕЦИФИКАЦИЯ dbo.REF_FEATURE--%>
                            <eq:eqSpecification ID="vREF_FEATURE_DOCUMENT" runat="server">
                                <eq:eqSpecificationColumn>
                                    <ItemTemplate>
                                        <eq:eqDropDownList ID="ISN_FEATURE" runat="server" Width="100%" UniqueGroupName="REF_FEATURE" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        Номера листов:
                    </td>
                    <td>
                        <eq:eqTextBox ID="NOTE" runat="server" TextMode="MultiLine" Rows="4" />
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        Авторы документа:
                    </td>
                    <td>
                        <eq:eqTextBox ID="AUTHORS" runat="server" TextMode="MultiLine" Rows="4" />
                    </td>
                </tr>
            </table>
            <table style="width: 700px;">
                <tr>
                    <td>
                        <fieldset>
                            <legend>Файлы электронного образа</legend>
                            <eq:eqSpecification ID="vREF_FILE_DOCUMENT_S" runat="server" Width="100%" AllowInsert="false">
                                <eq:eqSpecificationColumn HeaderText="Описание">
                                    <ItemTemplate>
                                        <asp:TextBox ID="NAME" runat="server" Width="99%" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Файл">
                                    <ItemTemplate>
                                        <eq2:RefFile ID="ID" runat="server" Mode="FileSystem" OnRequesting="RefFileS_Requesting" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                            <table>
                                <tr>
                                    <td>
                                        Добавить файл:
                                    </td>
                                    <td>
                                        <eq2:RefFileUpload ID="RefFileUploadFileS" runat="server" Mode="FileSystem" />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
            </table>
        </asp:View>
        <%--НСА--%>
        <asp:View runat="server">
            <table style="width: 700px;">
                <colgroup>
                    <col style="width: 200px;" />
                    <col />
                </colgroup>
                <tr>
                    <td valign="top">
                        Аннотация:
                    </td>
                    <td>
                        <eq:eqTextBox ID="ANNOTATE" runat="server" TextMode="MultiLine" Rows="4" />
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        Вспомогательный НСА:
                    </td>
                    <td>
                        <eq:eqTextBox ID="ADDITIONAL_CLS" runat="server" TextMode="MultiLine" Rows="4" />
                    </td>
                </tr>
            </table>
            <table style="width: 700px;">
                <tr>
                    <td>
                        <fieldset>
                            <legend>Файлы</legend>
                            <eq:eqSpecification ID="vREF_FILE_DOCUMENT" runat="server" Width="100%" AllowInsert="false">
                                <eq:eqSpecificationColumn HeaderText="Описание">
                                    <ItemTemplate>
                                        <asp:TextBox ID="NAME" runat="server" Width="99%" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Файл">
                                    <ItemTemplate>
                                        <eq2:RefFile ID="ID" runat="server" Mode="FileSystem" OnRequesting="RefFile_Requesting" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                            <table>
                                <tr>
                                    <td>
                                        Добавить файл:
                                    </td>
                                    <td>
                                        <eq2:RefFileUpload ID="RefFileUploadFile" runat="server" Mode="FileSystem" />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <fieldset>
                            <legend>Межфондовый АНСА</legend>
                            <%--СПЕЦИФИКАЦИЯ dbo.REF_CLS ; OBJ_KIND=701--%>
                            <eq:eqSpecification ID="vREF_CLS701_DOCUMENT" runat="server">
                                <eq:eqSpecificationColumn>
                                    <ItemTemplate>
                                        <eq2:eqDocument ID="ISN_CLS" runat="server" DocTypeURL="CLS701.ascx" ListDocMode="NewWindowAndSelect"
                                            LoadDocMode="NewWindowAndSelect" ShowingFields="NAME" ShowingFieldsFormat="{0}" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                            <%----%>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <fieldset>
                            <legend>Внутрифондовый АНСА</legend>
                            <%--СПЕЦИФИКАЦИЯ dbo.REF_CLS ; OBJ_KIND=702--%>
                            <eq:eqSpecification ID="vREF_CLS702_DOCUMENT" runat="server">
                                <eq:eqSpecificationColumn>
                                    <ItemTemplate>
                                        <eq2:eqDocument ID="ISN_CLS" runat="server" DocTypeURL="CLS702.ascx" ListDocMode="NewWindowAndSelect"
                                            LoadDocMode="NewWindowAndSelect" ShowingFields="NAME" ShowingFieldsFormat="{0}" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <fieldset>
                            <legend>АНСА в рамках описи</legend>
                            <%--СПЕЦИФИКАЦИЯ dbo.REF_CLS ; OBJ_KIND=703--%>
                            <eq:eqSpecification ID="vREF_CLS703_DOCUMENT" runat="server">
                                <eq:eqSpecificationColumn>
                                    <ItemTemplate>
                                        <eq2:eqDocument ID="ISN_CLS" runat="server" DocTypeURL="CLS703.ascx" ListDocMode="NewWindowAndSelect"
                                            LoadDocMode="NewWindowAndSelect" ShowingFields="NAME" ShowingFieldsFormat="{0}" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
            </table>
        </asp:View>
    </asp:MultiView>
</div>
