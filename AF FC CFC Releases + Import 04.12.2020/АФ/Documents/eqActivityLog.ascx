﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="eqActivityLog.ascx.vb" Inherits="WebApplication.eqActivityLog" %>


<style type="text/css">
    .style1
    {
        width: 100%;
    }
    .style2
    {
        width: 133px;
    }
</style>
<table class="style1">
    <tr>
        <td class="style2">
            №</td>
        <td>
            <eq:eqTextBox ID="iID" runat="server"></eq:eqTextBox>
        </td>
    </tr>
    <tr>
        <td class="style2">
            Дата-Время</td>
        <td>
            <eq:eqTextBox ID="ActivityDate" runat="server"></eq:eqTextBox>
        </td>
    </tr>
    <tr>
        <td class="style2">
            Введенный Логин</td>
        <td>
            <eq:eqTextBox ID="Login" runat="server"></eq:eqTextBox>
        </td>
    </tr>
    <tr>
        <td class="style2">
            Введенный пароль</td>
        <td>
            <eq:eqTextBox ID="Pass" runat="server"></eq:eqTextBox>
        </td>
    </tr>
    <tr>
        <td class="style2">
            Результат аудентификации</td>
        <td>
            <asp:CheckBox ID="LogonRezult" runat="server" />
            <eq:eqTextBox ID="UserAgent" runat="server"></eq:eqTextBox>
        </td>
    </tr>
    <tr>
        <td class="style2">
            Информация о рабочем месте клиента</td>
        <td>
            <eq:eqTextBox ID="UserHostAddress" runat="server"></eq:eqTextBox>
        </td>
    </tr>
    <tr>
        <td class="style2">
            IP Пользователя</td>
        <td>
            <eq:eqTextBox ID="Req" runat="server"></eq:eqTextBox>
        </td>
    </tr>
</table>
