﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="FUND.ascx.vb" Inherits="WebApplication.FUND" %>

<script type="text/javascript">
    function _updateUI() {
        $(":input[name*=PRESENCE_FLAG]").bind('change', function() {
            $(":input[name*=ABSENCE_REASON]").prop("disabled", $(":input[name*=PRESENCE_FLAG]").prop("selectedIndex") == 0);
            $(":input[name*=MOVEMENT_NOTE]").prop("disabled", $(":input[name*=ABSENCE_REASON]").prop("selectedIndex") == 0);
        });
        $(":input[name*=ABSENCE_REASON]").bind('change', function() {
            $(":input[name*=MOVEMENT_NOTE]").prop("disabled", $(":input[name*=ABSENCE_REASON]").prop("selectedIndex") == 0);
        });
        $(":input[name*=ABSENCE_REASON]").prop("disabled", $(":input[name*=PRESENCE_FLAG]").prop("selectedIndex") == 0);
        $(":input[name*=MOVEMENT_NOTE]").prop("disabled", $(":input[name*=ABSENCE_REASON]").prop("selectedIndex") == 0);
        $(":input[name*=IS_LOST]").bind('change', function() {
            $(":input[name*=IS_IN_SEARCH]").prop("disabled", $(":input[name*=IS_LOST]").prop("checked"));
            if ($(":input[name*=IS_LOST]").prop("checked")) $(":input[name*=IS_IN_SEARCH]").removeProp("checked");
        });
        $(":input[name*=IS_IN_SEARCH]").prop("disabled", $(":input[name*=IS_LOST]").prop("checked"));
        if ($(":input[name*=IS_LOST]").prop("checked")) $(":input[name*=IS_IN_SEARCH]").removeProp("checked");

        var sr = $('table[id*=vFUND_DOCUMENT_STATS_A] tr[innerHTML*=Фотодокументы] td');
        $(':input:odd', sr).parent().hide();
        $('span[innerHTML*=уч]', sr).hide();

        $(":input[name*=CARRIER_TYPE]").bind('change', function() {
            var v = $(":input[name*=CARRIER_TYPE]:radio:checked").val();
            $(":input[name*=HAS_TRADITIONAL_DOCS]").prop("disabled", v == "T");
            $(":input[name*=HAS_ELECTRONIC_DOCS]").prop("disabled", v == "E");
            if (v == "T") $(":input[name*=HAS_TRADITIONAL_DOCS]").prop("checked", false);
            else if (v == "E") $(":input[name*=HAS_ELECTRONIC_DOCS]").prop("checked", false);
        }).change();
    }
</script>

<table style="width: 97%; margin-bottom: 4px;">
    <tr>
        <td>
            <eq2:ArchiveCrumbs ID="Crumbs" runat="server" Level="ARCHIVE" />
        </td>
        <td style="text-align: right;">
            Содержимое:
            <asp:HyperLink ID="HyperLinkINVENTORYs" runat="server" Text="Описи" Target="_blank"
                NavigateUrl="~/Default.aspx?DocTypeURL=INVENTORY.ascx&ListDoc=True" Style="font-weight: bold;
                padding: 2px;" />
            <asp:HyperLink ID="HyperLinkDEPOSITs" runat="server" Text="Россыпи" Target="_blank"
                NavigateUrl="~/Default.aspx?DocTypeURL=DEPOSIT.ascx&ListDoc=True" Style="font-weight: bold;
                padding: 2px;" />
            <asp:HyperLink ID="HyperLinkUNITs" runat="server" Text="Ед.хр./Ед.уч." Target="_blank"
                NavigateUrl="~/Default.aspx?DocTypeURL=UNIT.ascx&ListDoc=True" Style="font-weight: bold;
                padding: 2px;" />
            <asp:HyperLink ID="HyperLinkUNIT2s" runat="server" Text="Ед.хр./Ед.уч. (КФД)" Target="_blank"
                NavigateUrl="~/Default.aspx?DocTypeURL=UNIT2.ascx&ListDoc=True" Style="font-weight: bold;
                padding: 2px;" />
            <asp:HyperLink ID="HyperLinkSTRs" runat="server" Text="Разделы описи" Target="_blank"
                NavigateUrl="~/Default.aspx?DocTypeURL=INVENTORYSTRUCTURE.ascx&ListDoc=True"
                Style="font-weight: bold; padding: 2px;" />
        </td>
    </tr>
</table>
<eq:eqMenu ID="MenuTabs" runat="server" Orientation="Horizontal" StaticEnableDefaultPopOutImage="False"
    ItemWrap="false" HorizontalMaxCount="5">
    <Items>
        <asp:MenuItem Text="Общие сведения" Value="0" Selected="true" />
        <asp:MenuItem Text="Объем: трад. носители" Value="1" />
        <asp:MenuItem Text="Объем: эл. носители" Value="2" />
        <asp:MenuItem Text="НСА 1" Value="3" />
        <asp:MenuItem Text="НСА 2" Value="4" />
        <asp:MenuItem Text="АНСА" Value="5" />
        <asp:MenuItem Text="Доп. Инф." Value="6" />
        <asp:MenuItem Text="ОАФ" Value="7" />
        <asp:MenuItem Text="Физ. Состояние" Value="8" />
        <asp:MenuItem Text="Особенности" Value="9" />
    </Items>
    <StaticSelectedStyle CssClass="MenuItemStyleSelected" />
</eq:eqMenu>
<div style="border: solid 1px black; padding: 5px;">
    <asp:MultiView ID="MultiViewTabs" runat="server" ActiveViewIndex="0">
        <%--! Общие сведения--%>
        <asp:View runat="server">
            <table style="width: 700px;">
                <col style="width: 200px;" />
                <col />
                <tr>
                    <td>
                        <asp:CheckBox ID="ForbidRecalc" runat="server" Text="запретить пересчет"  />
                    </td>
                    <td>
                        
                    </td>
                </tr>
                <tr>
                    <td>
                        Номер*:
                    </td>
                    <td>
                        <eq:eqTextBox ID="FUND_NUM_1" Width="40px" runat="server" MaxLength="1" />
                        <eq:eqTextBox ID="FUND_NUM_2" Width="70px" runat="server" MaxLength="5" />
                        <eq:eqTextBox ID="FUND_NUM_3" Width="40px" runat="server" MaxLength="1" />
                        <aj:FilteredTextBoxExtender runat="server" TargetControlID="FUND_NUM_1" FilterType="Custom"
                            ValidChars="АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЬЫЪЭЮЯ" />
                        <aj:FilteredTextBoxExtender runat="server" TargetControlID="FUND_NUM_2" FilterType="Numbers" />
                        <aj:FilteredTextBoxExtender runat="server" TargetControlID="FUND_NUM_3" FilterType="Custom"
                            ValidChars="АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЬЫЪЭЮЯ" />
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        Название фонда*:
                    </td>
                    <td>
                        <eq:eqTextBox ID="FUND_NAME_FULL" runat="server" TextMode="MultiLine" Rows="4" />
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        Сокращенное название фонда*:
                    </td>
                    <td>
                        <eq:eqTextBox ID="FUND_NAME_SHORT" runat="server" TextMode="MultiLine" Rows="4" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Вид*:
                    </td>
                    <td>
                        <asp:DropDownList ID="FUND_KIND" runat="server" AutoPostBack="true" />
                        <%--Table: DIC_OBJ_PRESENTATION Column: VALUE--%>
                    </td>
                </tr>
            </table>
            <table style="width: 700px;">
                <colgroup>
                    <col style="width: 200px;" />
                    <col />
                    <col />
                    <col />
                </colgroup>
                <tr>
                    <td>
                        Хар-ка секретности*:
                    </td>
                    <td>
                        <asp:DropDownList ID="ISN_SECURLEVEL" runat="server" AutoPostBack="true" />
                        <%--Table: SECURLEVEL Column: NAME--%>
                    </td>
                    <td>
                        Доступ:*
                    </td>
                    <td>
                        <asp:DropDownList ID="SECURITY_CHAR" runat="server" AutoPostBack="true" />
                        <%--Table: SECURLEVEL Column: NAME--%>
                    </td>
                </tr>
                <tr>
                    <td>
                        Причина ограничения доступа:
                    </td>
                    <td>
                        <asp:DropDownList ID="ISN_SECURITY_REASON" runat="server" />
                    </td>
                    <td>
                        Исторический период:
                    </td>
                    <td>
                        <asp:DropDownList ID="ISN_PERIOD" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Фондообразователь:
                    </td>
                    <td colspan="3">
                        <%--СПЕЦИФИЕАЦИЯ dbo.FUND_CREATOR--%>
                        <eq:eqSpecification runat="server" ID="tblFUND_CREATOR">
                            <eq:eqSpecificationColumn HeaderText="Фондообразователь">
                                <ItemTemplate>
                                    <asp:MultiView ID="FundOrganiz" runat="server">
                                        <asp:View runat="server">
                                            <eq2:eqDocument ID="ISN_CITIZEN" runat="server" ShowingFields="NAME, BIRTH_DATE, DEATH_DATE"
                                                DocTypeURL="CITIZEN_CL.ascx" ListDocMode="NewWindowAndSelect" LoadDocMode="NewWindowAndSelect"
                                                ShowingFieldsFormat="{0}" />
                                        </asp:View>
                                        <asp:View runat="server">
                                            <eq2:eqDocument ID="ISN_ORGANIZ" runat="server" ShowingFields="NAME, CREATE_YEAR, DELETE_YEAR, ADDRESS"
                                                DocTypeURL="ORGANIZ_CL.ascx" ListDocMode="NewWindowAndSelect" LoadDocMode="NewWindowAndSelect"
                                                ShowingFieldsFormat="{0}" />
                                        </asp:View>
                                    </asp:MultiView>
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                            <eq:eqSpecificationColumn HeaderText="Основной">
                                <ItemTemplate>
                                    <asp:CheckBox ID="IS_PRIMARY" runat="server" />
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                        </eq:eqSpecification>
                    </td>
                </tr>
                <tr>
                    <td>
                        Категория*:
                    </td>
                    <td>
                        <asp:DropDownList ID="FUND_CATEGORY" runat="server" />
                    </td>
                    <td>
                        Тип фонда*:
                    </td>
                    <td>
                        <asp:DropDownList ID="ISN_DOC_TYPE" runat="server" AutoPostBack="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Типы документов*:
                    </td>
                    <td colspan="3">
                        <%--СПЕЦИФИКАЦИЯ dbo.FUND_DOC_TYPE--%>
                        <eq:eqSpecification runat="server" ID="tblFUND_DOC_TYPE">
                            <eq:eqSpecificationColumn>
                                <ItemTemplate>
                                    <asp:DropDownList ID="ISN_DOC_TYPE" runat="server" Width="300px" />
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                        </eq:eqSpecification>
                    </td>
                </tr>
                <tr>
                    <td>
                        Источник поступления:
                    </td>
                    <td colspan="3">
                        <%--СПЕЦИФИКАЦИЯ dbo.FUND_RECEIPT_SOURCE--%>
                        <eq:eqSpecification runat="server" ID="tblFUND_RECEIPT_SOURCE">
                            <eq:eqSpecificationColumn>
                                <ItemTemplate>
                                    <asp:DropDownList ID="ISN_RECEIPT_SOURCE" runat="server" Width="300px" />
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                        </eq:eqSpecification>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <fieldset>
                            <legend>Тип носителей документов*</legend>
                            <asp:RadioButtonList ID="CARRIER_TYPE" runat="server">
                                <asp:ListItem Text="традиционный" Value="T"></asp:ListItem>
                                <asp:ListItem Text="электронный" Value="E"></asp:ListItem>
                            </asp:RadioButtonList>
                            <br />
                            <asp:CheckBox ID="HAS_TRADITIONAL_DOCS" runat="server" Text="Содержит док. на традиционных носителях" />
                            <asp:CheckBox ID="HAS_ELECTRONIC_DOCS" runat="server" Text="Содержит док. на электронных носителях" />
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        Собственность:
                    </td>
                    <td>
                        <asp:DropDownList ID="PROPERTY" runat="server">
                            <asp:ListItem Text="" Value="" />
                            <asp:ListItem Text="федеральная" Value="a" />
                            <asp:ListItem Text="субъекта федерации" Value="b" />
                            <asp:ListItem Text="муниципальная" Value="c" />
                            <asp:ListItem Text="физического лица" Value="d" />
                            <asp:ListItem Text="юридического лица" Value="e" />
                            <asp:ListItem Text="совместная" Value="f" />
                        </asp:DropDownList>
                    </td>
                    <td>
                        Срок хранения*:
                    </td>
                    <td>
                        <asp:DropDownList ID="KEEP_PERIOD" runat="server">
                            <asp:ListItem Text="постоянно" Value="a" />
                            <asp:ListItem Text="временно" Value="b" />
                            <asp:ListItem Text="частично временно" Value="c" />
                            <asp:ListItem Text="депозит" Value="d" />
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        Основание поступления:
                    </td>
                    <td colspan="3">
                        <%--СПЕЦИФИКАЦИЯ dbo.FUND_RECEIPT_REASON--%>
                        <eq:eqSpecification runat="server" ID="tblFUND_RECEIPT_REASON">
                            <eq:eqSpecificationColumn>
                                <ItemTemplate>
                                    <asp:DropDownList ID="ISN_RECEIPT_REASON" runat="server" Width="300px" />
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                        </eq:eqSpecification>
                    </td>
                </tr>
                <tr>
                    <td>
                        Движение*:
                    </td>
                    <td>
                        <asp:DropDownList ID="PRESENCE_FLAG" runat="server">
                            <asp:ListItem Text="в наличии" Value="a" />
                            <asp:ListItem Text="выбыл" Value="b" />
                        </asp:DropDownList>
                    </td>
                    <td colspan="2">
                        <asp:DropDownList ID="ABSENCE_REASON" runat="server">
                            <asp:ListItem Text="" Value="" />
                            <asp:ListItem Text="переданный" Value="a" />
                            <asp:ListItem Text="присоединенный" Value="b" />
                            <asp:ListItem Text="утраченный" Value="c" />
                            <asp:ListItem Text="выделенный к уничтожению" Value="d" />
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        Вошел в ОАФ:
                    </td>
                    <td colspan="3">
                        <%--ПОЛЕ ДЛЯ ЧТЕНИЯ--%>
                        <%--dbo.FUND_OAF содержит ISN_CHILD_FUND--%>
                        <eq:eqTextBox ID="ISN_OAF" runat="server" ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        Справка по движению:
                    </td>
                    <td colspan="3">
                        <eq:eqTextBox ID="MOVEMENT_NOTE" runat="server" TextMode="MultiLine" Rows="4" />
                    </td>
                </tr>
            </table>
            <table style="width: 700px;">
                <tr>
                    <td>
                        <fieldset>
                            <legend>Описи </legend>
                            <table>
                                <tr>
                                    <td>
                                        Количество описей*:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="INVENTORY_COUNT" runat="server" Width="70px" />
                                    </td>
                                    <td>
                                        Введено описей:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="AUTO_INVENTORY_COUNT" runat="server" Width="70px" />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
            </table>
            <table style="width: 700px;">
                <tr>
                    <td>
                        <fieldset>
                            <legend>Даты документов</legend>
                            <table>
                                <tr>
                                    <td>
                                        Начальная дата*:
                                    </td>
                                    <td style="width: 80px;">
                                        <eq:eqTextBox ID="DOC_START_YEAR" runat="server" Width="100px" />
                                        <aj:FilteredTextBoxExtender runat="server" TargetControlID="DOC_START_YEAR" FilterType="Numbers" />
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="DOC_START_YEAR_INEXACT" runat="server" Text="неточная" TextAlign="Left" />
                                    </td>
                                    <td>
                                        Конечная дата*:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="DOC_END_YEAR" runat="server" Width="100px" />
                                        <aj:FilteredTextBoxExtender runat="server" TargetControlID="DOC_END_YEAR" FilterType="Numbers" />
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="DOC_END_YEAR_INEXACT" runat="server" Text="неточная" TextAlign="Left" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="right">
                                        Первое поступление документов*:
                                    </td>
                                    <td colspan="2">
                                        <eq:eqTextBox ID="DOC_RECEIPT_YEAR" runat="server" Width="100px" />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
            </table>
            <table style="width: 700px;">
                <colgroup>
                    <col style="width: 200px;" />
                    <col />
                    <col />
                    <col />
                </colgroup>
                <tr>
                    <td>
                        Последняя выверка учетных документов:
                    </td>
                    <td>
                        <eq:eqTextBox ID="LAST_DOC_CHECK_YEAR" runat="server" Width="70px" />
                    </td>
                    <td align="right">
                        Последняя проверка наличия:
                    </td>
                    <td>
                        <eq:eqTextBox ID="LAST_CHECKED_YEAR" runat="server" Width="70px" />
                    </td>
                </tr>
            </table>
            <table style="width: 700px;">
                <tr>
                    <td>
                        <fieldset>
                            <legend>Файлы электронного образа</legend>
                            <eq:eqSpecification ID="vREF_FILE_FUND_S" runat="server" Width="100%" AllowInsert="false">
                                <eq:eqSpecificationColumn HeaderText="Описание">
                                    <ItemTemplate>
                                        <asp:TextBox ID="NAME" runat="server" Width="99%" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Файл">
                                    <ItemTemplate>
                                        <eq2:RefFile ID="ID" runat="server" Mode="FileSystem" OnRequesting="RefFileS_Requesting" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                            <table>
                                <tr>
                                    <td>
                                        Добавить файл:
                                    </td>
                                    <td>
                                        <eq2:RefFileUpload ID="RefFileUploadFileS" runat="server" Mode="FileSystem" />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
            </table>
        </asp:View>
        <%--! Объем: трад. носители--%>
        <asp:View runat="server">
            <asp:Panel ID="Panel_T" runat="server">
                <table style="width: 700px;">
                    <tr>
                        <td>
                            <%--СПЕЦИФИКАЦИЯ "Всего"--%>
                            <eq:eqSpecification ID="vFUND_DOCUMENT_STATS" runat="server" ShowNumering="false"
                                AllowDelete="false" AllowInsert="false" AllowReorder="false">
                                <eq:eqSpecificationColumn HeaderText="">
                                    <ItemTemplate>
                                        <asp:Label ID="ISN_DOC_TYPE" runat="server" Width="120px" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="">
                                    <ItemTemplate>
                                        <nobr>ед. хр.</nobr>
                                        <br />
                                        <nobr>ед. уч.</nobr>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Всего ед. хранения">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_COUNT" runat="server" SkinID="Numeric" />
                                        <eq:eqTextBox ID="REG_UNIT" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Всего по описям">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_INVENTORY" runat="server" SkinID="Numeric" />
                                        <eq:eqTextBox ID="REG_UNIT_INVENTORY" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Введено">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_REGISTERED" runat="server" SkinID="Numeric" />
                                        <eq:eqTextBox ID="REG_UNIT_REGISTERED" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Неописанные">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_UNDESCRIBED" runat="server" SkinID="Numeric" />
                                        <eq:eqTextBox ID="REG_UNIT_UNDESCRIBED" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="ОЦД">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_OC_COUNT" runat="server" SkinID="Numeric" />
                                        <eq:eqTextBox ID="REG_UNIT_OC" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Уникальные">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNITS_UNIQUE" runat="server" SkinID="Numeric" />
                                        <eq:eqTextBox ID="REG_UNITS_UNIQUE" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Имеют СФ">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_HAS_SF" runat="server" SkinID="Numeric" />
                                        <eq:eqTextBox ID="REG_UNIT_HAS_SF" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Имеют ФП">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_HAS_FP" runat="server" SkinID="Numeric" />
                                        <eq:eqTextBox ID="REG_UNIT_HAS_FP" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Необнаруж.">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNITS_NOT_FOUND" runat="server" SkinID="Numeric" />
                                        <eq:eqTextBox ID="REG_UNITS_NOT_FOUND" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Секретные">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="SECRET_UNITS" runat="server" SkinID="Numeric" />
                                        <eq:eqTextBox ID="SECRET_REG_UNITS" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Закаталог.">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNITS_CATALOGUED" runat="server" SkinID="Numeric" />
                                        <eq:eqTextBox ID="REG_UNITS_CTALOGUE" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </td>
                    </tr>
                </table>
                <table style="width: 700px;">
                    <tr>
                        <td>
                            <fieldset>
                                <legend>Документы на бумажной основе </legend>
                                <%--СПЕЦИФИКАЦИЯ "Документы на бумажной основе"--%>
                                <eq:eqSpecification ID="vFUND_DOCUMENT_STATS_P" runat="server" ShowNumering="false"
                                    AllowDelete="false" AllowInsert="false" AllowReorder="false">
                                    <eq:eqSpecificationColumn HeaderText="">
                                        <ItemTemplate>
                                            <asp:Label ID="ISN_DOC_TYPE" runat="server" Width="150" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Всего ед. хранения">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNIT_COUNT" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Всего по описям">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNIT_INVENTORY" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Введено">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNIT_REGISTERED" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Неописанные">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNIT_UNDESCRIBED" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="ОЦД">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNIT_OC_COUNT" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Уникальные">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNITS_UNIQUE" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Имеют СФ">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNIT_HAS_SF" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Имеют ФП">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNIT_HAS_FP" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Необнаруж.">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNITS_NOT_FOUND" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Секретные">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="SECRET_UNITS" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Закаталог.">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNITS_CATALOGUED" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                </eq:eqSpecification>
                            </fieldset>
                        </td>
                    </tr>
                </table>
                <table style="width: 700px;">
                    <colgroup>
                        <col />
                        <col />
                        <col align="right" />
                        <col />
                        <col align="right" />
                        <col />
                    </colgroup>
                    <tr>
                        <td>
                            Неописанные документы (количество документов)
                        </td>
                        <td>
                            <eq:eqTextBox ID="UNDESCRIBED_DOC_COUNT" runat="server" SkinID="Numeric" />
                        </td>
                        <td>
                            Из них личного происхождения
                        </td>
                        <td>
                            <eq:eqTextBox ID="PERSONAL_UNDESCRIBED_DOC_COUNT" runat="server" SkinID="Numeric" />
                        </td>
                        <td>
                            Неописанные листы (кол-во листов)
                        </td>
                        <td>
                            <eq:eqTextBox ID="UNDECSRIBED_PAGE_COUNT" runat="server" SkinID="Numeric" />
                        </td>
                    </tr>
                </table>
                <table style="width: 700px;">
                    <tr>
                        <td>
                            <fieldset>
                                <legend>Аудиовизуальные документы на традиционных носителях </legend>
                                <%--СПЕЦИФИКАЦИЯ "Аудиовизуальные документы на традиционных носителях"--%>
                                <eq:eqSpecification ID="vFUND_DOCUMENT_STATS_A" runat="server" ShowNumering="false"
                                    AllowDelete="false" AllowInsert="false" AllowReorder="false">
                                    <eq:eqSpecificationColumn HeaderText="">
                                        <ItemTemplate>
                                            <asp:Label ID="ISN_DOC_TYPE" runat="server" Width="120" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="">
                                        <ItemTemplate>
                                            <span>
                                                <nobr>ед. хр.</nobr>
                                            </span>
                                            <br />
                                            <span>
                                                <nobr>ед. уч.</nobr>
                                            </span>
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Всего ед. хранения">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNIT_COUNT" runat="server" SkinID="Numeric" />
                                            <eq:eqTextBox ID="REG_UNIT" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Всего по описям">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNIT_INVENTORY" runat="server" SkinID="Numeric" />
                                            <eq:eqTextBox ID="REG_UNIT_INVENTORY" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Введено">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNIT_REGISTERED" runat="server" SkinID="Numeric" />
                                            <eq:eqTextBox ID="REG_UNIT_REGISTERED" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Неописанные">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNIT_UNDESCRIBED" runat="server" SkinID="Numeric" />
                                            <eq:eqTextBox ID="REG_UNIT_UNDESCRIBED" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="ОЦД">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNIT_OC_COUNT" runat="server" SkinID="Numeric" />
                                            <eq:eqTextBox ID="REG_UNIT_OC" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Уникальные">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNITS_UNIQUE" runat="server" SkinID="Numeric" />
                                            <eq:eqTextBox ID="REG_UNITS_UNIQUE" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Имеют СФ">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNIT_HAS_SF" runat="server" SkinID="Numeric" />
                                            <eq:eqTextBox ID="REG_UNIT_HAS_SF" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Имеют ФП">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNIT_HAS_FP" runat="server" SkinID="Numeric" />
                                            <eq:eqTextBox ID="REG_UNIT_HAS_FP" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Необнаруж.">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNITS_NOT_FOUND" runat="server" SkinID="Numeric" />
                                            <eq:eqTextBox ID="REG_UNITS_NOT_FOUND" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Секретные">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="SECRET_UNITS" runat="server" SkinID="Numeric" />
                                            <eq:eqTextBox ID="SECRET_REG_UNITS" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Закаталог.">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNITS_CATALOGUED" runat="server" SkinID="Numeric" />
                                            <eq:eqTextBox ID="REG_UNITS_CTALOGUE" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                </eq:eqSpecification>
                            </fieldset>
                        </td>
                    </tr>
                </table>
                <table style="width: 700px;">
                    <tr>
                        <td>
                            <fieldset>
                                <legend>Микроформы</legend>
                                <%--СПЕЦИФИКАЦИЯ "Микроформы"--%>
                                <eq:eqSpecification ID="vFUND_DOCUMENT_STATS_M" runat="server" ShowNumering="false"
                                    AllowDelete="false" AllowInsert="false" AllowReorder="false">
                                    <eq:eqSpecificationColumn HeaderText="">
                                        <ItemTemplate>
                                            <asp:Label ID="ISN_DOC_TYPE" runat="server" Width="120" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="">
                                        <ItemTemplate>
                                            <nobr>ед. хр.</nobr>
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Всего ед. хранения">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNIT_COUNT" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Всего по описям">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNIT_INVENTORY" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Введено">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNIT_REGISTERED" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Неописанные">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNIT_UNDESCRIBED" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="ОЦД">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNIT_OC_COUNT" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Уникальные">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNITS_UNIQUE" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Имеют СФ">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNIT_HAS_SF" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Имеют ФП">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNIT_HAS_FP" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Необнаруж.">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNITS_NOT_FOUND" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Секретные">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="SECRET_UNITS" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Закаталог.">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNITS_CATALOGUED" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                </eq:eqSpecification>
                            </fieldset>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </asp:View>
        <%--! Объем: эл. носители--%>
        <asp:View runat="server">
            <asp:Panel ID="Panel_E" runat="server">
                <table style="width: 700px;">
                    <tr>
                        <td>
                            <fieldset>
                                <legend>Документы на электронных носителях</legend>
                                <%--СПЕЦИФИКАЦИЯ "Документы на электронных носителях"--%>
                                <eq:eqSpecification ID="vFUND_DOCUMENT_STATS_E" runat="server" ShowNumering="false"
                                    AllowDelete="false" AllowInsert="false" AllowReorder="false">
                                    <eq:eqSpecificationColumn HeaderText="">
                                        <ItemTemplate>
                                            <asp:Label ID="ISN_DOC_TYPE" runat="server" Width="120" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="">
                                        <ItemTemplate>
                                            <nobr>ед. хр.</nobr>
                                            <br />
                                            <nobr>ед. уч.</nobr>
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Всего ед. хранения">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNIT_COUNT" runat="server" SkinID="Numeric" />
                                            <eq:eqTextBox ID="REG_UNIT" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Всего по описям">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNIT_INVENTORY" runat="server" SkinID="Numeric" />
                                            <eq:eqTextBox ID="REG_UNIT_INVENTORY" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Введено">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNIT_REGISTERED" runat="server" SkinID="Numeric" />
                                            <eq:eqTextBox ID="REG_UNIT_REGISTERED" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Неописанные">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNIT_UNDESCRIBED" runat="server" SkinID="Numeric" />
                                            <eq:eqTextBox ID="REG_UNIT_UNDESCRIBED" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="ОЦД">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNIT_OC_COUNT" runat="server" SkinID="Numeric" />
                                            <eq:eqTextBox ID="REG_UNIT_OC" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Уникальные">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNITS_UNIQUE" runat="server" SkinID="Numeric" />
                                            <eq:eqTextBox ID="REG_UNITS_UNIQUE" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Имеют СФ">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNIT_HAS_SF" runat="server" SkinID="Numeric" />
                                            <eq:eqTextBox ID="REG_UNIT_HAS_SF" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Имеют ФП">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNIT_HAS_FP" runat="server" SkinID="Numeric" />
                                            <eq:eqTextBox ID="REG_UNIT_HAS_FP" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Необнаруж.">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNITS_NOT_FOUND" runat="server" SkinID="Numeric" />
                                            <eq:eqTextBox ID="REG_UNITS_NOT_FOUND" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Секретные">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="SECRET_UNITS" runat="server" SkinID="Numeric" />
                                            <eq:eqTextBox ID="SECRET_REG_UNITS" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Закаталог.">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNITS_CATALOGUED" runat="server" SkinID="Numeric" />
                                            <eq:eqTextBox ID="REG_UNITS_CTALOGUE" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                </eq:eqSpecification>
                            </fieldset>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </asp:View>
        <%--! НСА 1--%>
        <asp:View runat="server">
            <table style="width: 900px;">
                <tr>
                    <td>
                        <fieldset>
                            <legend>Аннотация</legend>
                            <eq:eqTextBox ID="ANNOTATE" runat="server" TextMode="MultiLine" Rows="4" />
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <fieldset>
                            <legend>Историческая справка</legend>
                            <eq:eqTextBox ID="FUND_HISTORY" runat="server" TextMode="MultiLine" Rows="4" />
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <fieldset>
                            <legend>Вспомогательный НСА</legend>
                            <eq:eqTextBox ID="ADDITIONAL_NSA" runat="server" TextMode="MultiLine" Rows="4" />
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <fieldset>
                            <legend>Файлы</legend>
                            <eq:eqSpecification ID="vREF_FILE_FUND" runat="server" Width="100%" AllowInsert="false">
                                <eq:eqSpecificationColumn HeaderText="Описание">
                                    <ItemTemplate>
                                        <asp:TextBox ID="NAME" runat="server" Width="99%" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Файл">
                                    <ItemTemplate>
                                        <eq2:RefFile ID="ID" runat="server" Mode="FileSystem" OnRequesting="RefFile_Requesting" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                            <table>
                                <tr>
                                    <td>
                                        Добавить файл:
                                    </td>
                                    <td>
                                        <eq2:RefFileUpload ID="RefFileUploadFile" runat="server" Mode="FileSystem" />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
            </table>
        </asp:View>
        <%--НСА 2--%>
        <asp:View runat="server">
            <table style="width: 700px;">
                <tr>
                    <td>
                        <fieldset>
                            <legend>Межфондовые указатели к документам</legend>
                            <%--СПЕЦИФИКАЦИЯ dbo.FUND_PAPER_CLS--%>
                            <eq:eqSpecification ID="vFUND_PAPER_CLS704ba" runat="server">
                                <eq:eqSpecificationColumn>
                                    <ItemTemplate>
                                        <eq2:eqDocument ID="ISN_PAPER_CLS" runat="server" DocTypeURL="PAPER_CLS704ba.ascx"
                                            ListDocMode="NewWindowAndSelect" LoadDocMode="ListDocAndSelect" ShowingFields="NAME"
                                            ShowingFieldsFormat="{0}" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <fieldset>
                            <legend>Внутрифондовые указатели к описям</legend>
                            <%--СПЕЦИФИКАЦИЯ dbo.FUND_PAPER_CLS--%>
                            <eq:eqSpecification ID="vFUND_PAPER_CLS702bb" runat="server">
                                <eq:eqSpecificationColumn>
                                    <ItemTemplate>
                                        <eq2:eqDocument ID="ISN_PAPER_CLS" runat="server" DocTypeURL="PAPER_CLS702bb.ascx"
                                            ListDocMode="NewWindowAndSelect" LoadDocMode="ListDocAndSelect" ShowingFields="NAME"
                                            ShowingFieldsFormat="{0}" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <fieldset>
                            <legend>Внутрифондовые указатели к документам</legend>
                            <%--СПЕЦИФИКАЦИЯ dbo.FUND_PAPER_CLS--%>
                            <eq:eqSpecification ID="vFUND_PAPER_CLS704bb" runat="server">
                                <eq:eqSpecificationColumn>
                                    <ItemTemplate>
                                        <eq2:eqDocument ID="ISN_PAPER_CLS" runat="server" DocTypeURL="PAPER_CLS704bb.ascx"
                                            ListDocMode="NewWindowAndSelect" LoadDocMode="ListDocAndSelect" ShowingFields="NAME"
                                            ShowingFieldsFormat="{0}" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <fieldset>
                            <legend>Каталоги</legend>
                            <%--СПЕЦИФИКАЦИЯ dbo.FUND_PAPER_CLS--%>
                            <eq:eqSpecification ID="vFUND_PAPER_CLS700aa" runat="server">
                                <eq:eqSpecificationColumn>
                                    <ItemTemplate>
                                        <eq2:eqDocument ID="ISN_PAPER_CLS" runat="server" DocTypeURL="PAPER_CLS700aa.ascx"
                                            ListDocMode="NewWindowAndSelect" LoadDocMode="ListDocAndSelect" ShowingFields="NAME"
                                            ShowingFieldsFormat="{0}" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <fieldset>
                            <legend>Межфондовые указатели к описям</legend>
                            <%--СПЕЦИФИКАЦИЯ dbo.FUND_PAPER_CLS--%>
                            <eq:eqSpecification ID="vFUND_PAPER_CLS702ba" runat="server">
                                <eq:eqSpecificationColumn>
                                    <ItemTemplate>
                                        <eq2:eqDocument ID="ISN_PAPER_CLS" runat="server" DocTypeURL="PAPER_CLS702ba.ascx"
                                            ListDocMode="NewWindowAndSelect" LoadDocMode="ListDocAndSelect" ShowingFields="NAME"
                                            ShowingFieldsFormat="{0}" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <fieldset>
                            <legend>Обзоры</legend>
                            <%--СПЕЦИФИКАЦИЯ dbo.FUND_PAPER_CLS--%>
                            <eq:eqSpecification ID="vFUND_PAPER_CLS700ca" runat="server">
                                <eq:eqSpecificationColumn>
                                    <ItemTemplate>
                                        <eq2:eqDocument ID="ISN_PAPER_CLS" runat="server" DocTypeURL="PAPER_CLS700ca.ascx"
                                            ListDocMode="NewWindowAndSelect" LoadDocMode="ListDocAndSelect" ShowingFields="NAME"
                                            ShowingFieldsFormat="{0}" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <fieldset>
                            <legend>Перечни</legend>
                            <table width="100%">
                                <colgroup>
                                    <col style="width: 200px;" />
                                    <col />
                                </colgroup>
                                <tr>
                                    <td>
                                        Число перечней
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="LIST_COUNT" runat="server" Width="50px" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <%--СПЕЦИФИКАЦИЯ--%>
                                        <eq:eqSpecification ID="vFUND_PAPER_CLS700da" runat="server">
                                            <eq:eqSpecificationColumn>
                                                <ItemTemplate>
                                                    <eq2:eqDocument ID="ISN_PAPER_CLS" runat="server" DocTypeURL="PAPER_CLS700da.ascx"
                                                        ListDocMode="NewWindowAndSelect" LoadDocMode="ListDocAndSelect" ShowingFields="NAME"
                                                        ShowingFieldsFormat="{0}" />
                                                </ItemTemplate>
                                            </eq:eqSpecificationColumn>
                                        </eq:eqSpecification>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
            </table>
        </asp:View>
        <%--АНСА--%>
        <asp:View runat="server">
            <table style="width: 700px;">
                <tr>
                    <td>
                        <fieldset>
                            <legend>Межфондовый АНСА</legend>
                            <%--СПЕЦИФИКАЦИЯ dbo.REF_CLS ; OBJ_KIND=701--%>
                            <eq:eqSpecification ID="vREF_CLS701_FUND" runat="server">
                                <eq:eqSpecificationColumn>
                                    <ItemTemplate>
                                        <eq2:eqDocument ID="ISN_CLS" runat="server" DocTypeURL="CLS701.ascx" ListDocMode="NewWindowAndSelect"
                                            LoadDocMode="ListDocAndSelect" ShowingFields="NAME" ShowingFieldsFormat="{0}" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <fieldset>
                            <legend>Внутрифондовый АНСА</legend>
                            <%--СПЕЦИФИКАЦИЯ dbo.REF_CLS ; OBJ_KIND=702--%>
                            <eq:eqSpecification ID="vREF_CLS702_FUND" runat="server">
                                <eq:eqSpecificationColumn>
                                    <ItemTemplate>
                                        <eq2:eqDocument ID="ISN_CLS" runat="server" DocTypeURL="CLS702.ascx" ListDocMode="NewWindowAndSelect"
                                            LoadDocMode="ListDocAndSelect" ShowingFields="NAME" ShowingFieldsFormat="{0}" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
            </table>
            <table style="width: 700px;">
                <colgroup>
                    <col style="width: 300px;" />
                    <col />
                </colgroup>
                <tr>
                    <td>
                        Делопроизводственные ведомственные картотеки
                    </td>
                    <td>
                        <eq:eqTextBox ID="CARD_COUNT" runat="server" Width="50px" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <fieldset>
                            <legend>БД о составе и содержании документа </legend>
                            <table>
                                <colgroup>
                                    <col />
                                    <col />
                                    <col />
                                    <col />
                                    <col />
                                    <col />
                                </colgroup>
                                <tr>
                                    <td>
                                        Внутрифондовые
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="INNER_DB_COUNT" runat="server" Width="50px" />
                                    </td>
                                    <td>
                                        Межфондовые
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="FUND_DB_COUNT" runat="server" Width="50px" />
                                    </td>
                                    <td>
                                        Межархивные
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="ARCHIVE_DB_COUNT" runat="server" Width="50px" />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
            </table>
        </asp:View>
        <%--! Доп. Инф.--%>
        <asp:View runat="server">
            <table style="width: 99%;">
                <tr>
                    <td>
                        <fieldset>
                            <legend>Переименования фонда</legend>
                            <%--СПЕЦИФИКАЦИЯ dbo.FUND_RENAME--%>
                            <eq:eqSpecification ID="vFUND_RENAME" runat="server" ShowNumering="false">
                                <eq:eqSpecificationColumn HeaderText="Полное название">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="FUND_NAME_FULL" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Сокращенное название" Width="200px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="FUND_NAME_SHORT" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Дата начала периода" Width="80px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="CREATE_DATE_FMT" runat="server" />
                                        <aj:MaskedEditExtender ID = "CREATE_DATE_FMT_MASK" TargetControlID = "CREATE_DATE_FMT" runat="server" MaskType="Date" Mask="99/99/9999" ClearTextOnInvalid="true"  />
                                        <aj:CalendarExtender runat="server" TargetControlID="CREATE_DATE_FMT" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                
                                <eq:eqSpecificationColumn HeaderText="Неточная дата" Width="20px">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CREATE_DATE_INEXACT" runat="server" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                
                                <eq:eqSpecificationColumn HeaderText="Дата окончания периода" Width="80px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="DELETE_DATE_FMT" runat="server"  />
                                        <aj:MaskedEditExtender ID = "DELETE_DATE_FMT_MASK" TargetControlID = "DELETE_DATE_FMT" runat="server" MaskType="Date" Mask="99/99/9999" ClearTextOnInvalid="true"  />
                                        <aj:CalendarExtender runat="server" TargetControlID="DELETE_DATE_FMT" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                
                                <eq:eqSpecificationColumn HeaderText="Неточная дата" Width="20px">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="DELETE_DATE_INEXACT" runat="server" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                
                                <eq:eqSpecificationColumn HeaderText="Примечание" Width="150px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="NOTE" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Название сохранено" Width="50px"> 
                                    <ItemTemplate>
                                        <asp:CheckBox ID="NAME_SAVED" runat="server"  />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <fieldset>
                            <legend>Незадокументированные периоды </legend>
                            <%--СПЕЦИФИКАЦИЯ dbo.UNDOCUMENTED_PERIOD--%>
                            <eq:eqSpecification ID="tblUNDOCUMENTED_PERIOD" runat="server" ShowNumering="false">
                                <eq:eqSpecificationColumn HeaderText="Год начала периода">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="PERIOD_START_YEAR" runat="server" Width="80" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Год окончания периода">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="PERIOD_END_YEAR" runat="server" Width="80" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Причана отсутствия информации">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ISN_ABSENCE_REASON" runat="server" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Место нахождения документа">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="INFO_PLACE" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Примечание">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="NOTE" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <fieldset>
                            <legend>Фондовые включения </legend>
                            <%--СПЕЦИФИКАЦИЯ dbo.FUND_INCLUSION--%>
                            <eq:eqSpecification ID="tblFUND_INCLUSION" runat="server" ShowNumering="false">
                                <eq:eqSpecificationColumn HeaderText="Фондообразователь">
                                    <ItemTemplate>
                                        <asp:MultiView ID="FundOrganiz" runat="server">
                                            <asp:View runat="server">
                                                <asp:DropDownList ID="ISN_CITIZEN" runat="server" />
                                                <%--dbo.CITIZEN_CL--%>
                                            </asp:View>
                                            <asp:View runat="server">
                                                <asp:DropDownList ID="ISN_ORGANIZ" runat="server" />
                                                <%--dbo.ORGANIZ_CL--%>
                                            </asp:View>
                                        </asp:MultiView>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Виды документов">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="DOC_TYPES" runat="server" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Начальная дата" Width="100">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="START_YEAR" runat="server" Width="80" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Конечная дата" Width="100">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="END_YEAR" runat="server" Width="80" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Примечание" Width="150px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="NOTE" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <fieldset>
                            <legend>Библиография </legend>
                            <%-- СПЕЦИФИКАЦИЯ vFUND_PUBLICATION--%>
                            <eq:eqSpecification ID="vFUND_PUBLICATION" runat="server">
                                <eq:eqSpecificationColumn>
                                    <ItemTemplate>
                                        <eq2:eqDocument ID="ISN_PUBLICATION" runat="server" DocTypeURL="PUBLICATION.ascx"
                                            ListDocMode="NewWindowAndSelect" LoadDocMode="ListDocAndSelect" ShowingFields="PUBLICATION_NAME"
                                            KeyName="ISN_PUBLICATION" ShowingFieldsFormat="{0}" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Номер">
                                    <ItemTemplate>
                                        <asp:Label ID="PUBLICATION_NUM" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Авторы">
                                    <ItemTemplate>
                                        <asp:Label ID="AUTHORS" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Место">
                                    <ItemTemplate>
                                        <asp:Label ID="PUBLICATION_PLACE" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Год">
                                    <ItemTemplate>
                                        <asp:Label ID="PUBLICATION_YEAR" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Листов">
                                    <ItemTemplate>
                                        <asp:Label ID="SHEET_COUNT" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Публикатор">
                                    <ItemTemplate>
                                        <asp:Label ID="PUBLISHER" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Примечание">
                                    <ItemTemplate>
                                        <asp:Label ID="NOTE" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
            </table>
        </asp:View>
        <%--ОАФ--%>
        <asp:View runat="server">
            <table style="width: 900px;">
                <tr>
                    <td>
                        <fieldset>
                            <legend>Фонды</legend>
                            <%--СПЕЦИФИКАЦИЯ dbo.FUND_OAF--%>
                            <eq:eqSpecification ID="vFUND_OAF" runat="server" ShowNumering="false">
                                <eq:eqSpecificationColumn HeaderText="Фонд">
                                    <ItemTemplate>
                                        <eq2:eqDocument ID="ISN_CHILD_FUND" runat="server" DocTypeURL="FUND.ascx" ListDocMode="NewWindowAndSelect"
                                            LoadDocMode="NewWindowAndSelect" ShowingFields="FUND_NUM, ISN_ARCHIVE" ShowingFieldsFormat="{0}"
                                            KeyName="ISN_FUND" OnPreRender="ISN_CHILD_FUND_PreRender" AdditionalListDocWhereString="PRESENCE_FLAG <> 'b'" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="№">
                                    <ItemTemplate>
                                        <div style="width: 170px;">
                                            <eq:eqTextBox ID="FUND_NUM_1" Width="40px" runat="server" MaxLength="1" />
                                            <eq:eqTextBox ID="FUND_NUM_2" Width="70px" runat="server" MaxLength="5" />
                                            <eq:eqTextBox ID="FUND_NUM_3" Width="40px" runat="server" MaxLength="1" />
                                            <asp:Label ID="CHILD_FUND_NUM" runat="server" />
                                        </div>
                                        <aj:FilteredTextBoxExtender runat="server" TargetControlID="FUND_NUM_1" FilterType="Custom"
                                            ValidChars="АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЬЫЪЭЮЯ" />
                                        <aj:FilteredTextBoxExtender runat="server" TargetControlID="FUND_NUM_2" FilterType="Numbers" />
                                        <aj:FilteredTextBoxExtender runat="server" TargetControlID="FUND_NUM_3" FilterType="Custom"
                                            ValidChars="АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЬЫЪЭЮЯ" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Сокращенное название">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="FUND_NAME_SHORT" runat="server" Width="200" />
                                        <asp:Label ID="CHILD_FUND_NAME_SHORT" runat="server" Width="200" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Полное название">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="FUND_NAME_FULL" runat="server" Width="200" />
                                        <asp:Label ID="CHILD_FUND_NAME_FULL" runat="server" Width="200" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Категория">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="FUND_CATEGORY" runat="server" />
                                        <asp:Label ID="CHILD_FUND_CATEGORY" runat="server" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Начальная дата">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="DOC_START_YEAR" runat="server" />
                                        <asp:Label ID="CHILD_DOC_START_YEAR" runat="server" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Конечная дата">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="DOC_END_YEAR" runat="server" />
                                        <asp:Label ID="CHILD_DOC_END_YEAR" runat="server" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Примечание">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="NOTE" runat="server" />
                                        <asp:Label ID="CHILD_NOTE" runat="server" />
                                        <asp:Label ID="WARNING" runat="server" ForeColor="Red" Font-Bold="true" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:MultiView ID="Reasons" runat="server">
                            <asp:View runat="server">
                                <table width="100%">
                                    <colgroup>
                                        <col style="width: 200px;" />
                                        <col />
                                    </colgroup>
                                    <tr>
                                        <td>
                                            <%--Признаки объединения ОАФ:--%>
                                            Признаки объединения ОАФ:
                                        </td>
                                        <td>
                                            <%--СПЕЦИФИКАЦИЯ dbo.FUND_OAF_REASON--%>
                                            <eq:eqSpecification ID="tblFUND_OAF_REASON" runat="server" ShowNumering="false">
                                                <eq:eqSpecificationColumn>
                                                    <ItemTemplate>
                                                       <eq2:eqDocument ID="ISN_OAF_REASON" runat="server" DocTypeURL="OAF_REASON_CL.ascx"
                                                            ListDocMode="NewWindowAndSelect" LoadDocMode="NewWindowAndSelect" ShowingFields="NAME"
                                                            ShowingFieldsFormat="{0}" />
                                                    </ItemTemplate>
                                                </eq:eqSpecificationColumn>
                                            </eq:eqSpecification>
                                        </td>
                                    </tr>
                                </table>
                            </asp:View>
                            <asp:View runat="server">
                                <table width="100%">
                                    <colgroup>
                                        <col style="width: 200px;" />
                                        <col />
                                    </colgroup>
                                    <tr>
                                        <td>
                                            <%--Признаки объединения архивной коллекции:--%>
                                            Признаки объединения архивной коллекции:
                                        </td>
                                        <td>
                                            <%--СПЕЦИФИКАЦИЯ dbo.tblFUND_COLLECTION_REASONS--%>
                                            <eq:eqSpecification ID="tblFUND_COLLECTION_REASONS" runat="server" ShowNumering="false">
                                                <eq:eqSpecificationColumn>
                                                    <ItemTemplate>
                                                       <eq2:eqDocument ID="ISN_COLLECTION_REASON" runat="server" DocTypeURL="COLLECTION_REASON_CL.ascx"
                                                            ListDocMode="NewWindowAndSelect" LoadDocMode="NewWindowAndSelect" ShowingFields="NAME"
                                                            ShowingFieldsFormat="{0}" />
                                                    </ItemTemplate>
                                                </eq:eqSpecificationColumn>
                                            </eq:eqSpecification>
                                        </td>
                                    </tr>
                                </table>
                            </asp:View>                            
                        </asp:MultiView>
                    </td>
                </tr>
                <tr>
                    <td>
                        <fieldset>
                            <legend>Основание оъединения </legend>
                            <eq:eqTextBox ID="JOIN_REASON" runat="server" TextMode="MultiLine" Rows="4" />
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <fieldset>
                            <legend>Примечание </legend>
                            <eq:eqTextBox ID="OAF_NOTE" runat="server" TextMode="MultiLine" Rows="4" />
                        </fieldset>
                    </td>
                </tr>
            </table>
        </asp:View>
        <%--Физ. Состояние--%>
        <asp:View runat="server">
            <table style="width: 900px;">
                <tr>
                    <td>
                        <fieldset>
                            <legend>Акты проверки наличия и сотояния единиц хранения </legend>
                            <%--СПЕЦИФИКАЦИЯ vREF_ACT_FOR_FUND_UNIT--%>
                            <eq:eqSpecification ID="vREF_ACT_FOR_FUND_UNIT" runat="server" ShowNumering="false"
                                AllowDelete="false" AllowInsert="false">
                                <eq:eqSpecificationColumn HeaderText="Акт">
                                    <ItemTemplate>
                                        <eq2:eqDocument ID="ISN_ACT" runat="server" DocTypeURL="ACT.ascx" ListDocMode="NewWindow"
                                            LoadDocMode="NewWindow" ShowingFields="ACT_NUM" ShowingFieldsFormat="{0}" KeyName="ISN_ACT" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Дата">
                                    <ItemTemplate>
                                        <asp:Label ID="ACT_DATE" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Название">
                                    <ItemTemplate>
                                        <asp:Label ID="ACT_NAME" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Тип акта">
                                    <ItemTemplate>
                                        <asp:Label ID="ACTTYPENAME" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Движение">
                                    <ItemTemplate>
                                        <asp:Label ID="MOVEMENT_FLAG" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Проведённая работа">
                                    <ItemTemplate>
                                        <asp:Label ID="ACT_WORK" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Примечание">
                                    <ItemTemplate>
                                        <asp:Label ID="NOTE" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                            <%--dbo.ACT--%>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <%--СПЕЦИФИКАЦИЯ dbo.FUND_CHECK--%>
                        <eq:eqSpecification ID="tblFUND_CHECK" runat="server">
                            <eq:eqSpecificationColumn>
                                <ItemTemplate>
                                    <table style="width: 600px;">
                                        <colgroup>
                                            <col align="right" style="width: 30%;" />
                                            <col align="left" style="width: 20%;" />
                                            <col align="right" style="width: 30%;" />
                                            <col align="left" />
                                        </colgroup>
                                        <tr>
                                            <td>
                                                Закартонировано
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="CARDBOARDED" runat="server" Width="50px" />
                                            </td>
                                            <td>
                                                Требуют дезинсекции
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="UNITS_NEED_DISINSECTION" runat="server" Width="50px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Требуют картонирования
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="UNITS_NEED_CARDBOARDED" runat="server" Width="50px" />
                                            </td>
                                            <td>
                                                Требуют шифровки
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="UNITS_NEED_ENCIPHERING" runat="server" Width="50px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Неисправимо повреждены
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="UNITS_DBR" runat="server" Width="50px" />
                                            </td>
                                            <td>
                                                Требуют замены обложки
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="UNITS_NEED_COVER_CHANGE" runat="server" Width="50px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Требуют реставрации
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="UNITS_NEED_RESTORATION" runat="server" Width="50px" />
                                            </td>
                                            <td>
                                                Листов с затухающими текстами
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="FADING_PAGES" runat="server" Width="50px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Требуют подшивки или переплета
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="UNITS_NEED_BINDING" runat="server" Width="50px" />
                                            </td>
                                            <td>
                                                На горючей основе
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="UNITS_INFLAMMABLE" runat="server" Width="50px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Требуют дезинфекции
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="UNITS_NEED_DISINFECTION" runat="server" Width="50px" />
                                            </td>
                                            <td>
                                                Требуют КПО
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="UNITS_NEED_KPO" runat="server" Width="50px" />
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                        </eq:eqSpecification>
                    </td>
                </tr>
            </table>
            <table style="width: 400px;">
                <colgroup>
                    <col align="right" />
                    <col align="right" style="width: 250px; padding-right: 100px;" />
                </colgroup>
                <tr>
                    <td>
                        Состояние описей:
                    </td>
                    <td>
                        <asp:DropDownList ID="INVENTORY_STATE" runat="server">
                            <asp:ListItem Text="" Value="" />
                            <asp:ListItem Text="удовлетворительное" Value="a" />
                            <asp:ListItem Text="неудовлетворительное" Value="b" />
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
        </asp:View>
        <%--Особенности--%>
        <asp:View runat="server">
            <table style="width: 700px;">
                <colgroup>
                    <col />
                    <col />
                </colgroup>
                <tr>
                    <td colspan="2">
                        <fieldset>
                            <legend>Особенности</legend>
                            <%--СПЕЦИФИКАЦИЯ dbo.REF_FEATURE--%>
                            <eq:eqSpecification ID="vREF_FEATURE_FUND" runat="server">
                                <eq:eqSpecificationColumn>
                                    <ItemTemplate>
                                        <eq:eqDropDownList ID="ISN_FEATURE" runat="server" Width="100%" UniqueGroupName="REF_FEATURE" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        Язык:
                    </td>
                    <td>
                        <%--СПЕЦИФИКАЦИЯ dbo.REF_LANGUAGE--%>
                        <eq:eqSpecification ID="vREF_LANGUAGE_FUND" runat="server">
                            <eq:eqSpecificationColumn>
                                <ItemTemplate>
                                    <eq:eqDropDownList ID="ISN_LANGUAGE" runat="server" Width="100%" UniqueGroupName="REF_LANGUAGE" />
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                        </eq:eqSpecification>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table style="margin-left: 100px;">
                            <colgroup>
                                <col align="right" />
                                <col align="right" />
                            </colgroup>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="HAS_MUSEUM_ITEMS" runat="server" Text="Наличие музейных предметов"
                                        TextAlign="Left" />
                                </td>
                                <td>
                                    <asp:CheckBox ID="HAS_UNDOCUMENTED_PERIODS" runat="server" Text="Наличие незадокументированных периодов"
                                        TextAlign="Left" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="WAS_RENAMED" runat="server" Text="Наличие переименований" TextAlign="Left" />
                                </td>
                                <td>
                                    <asp:CheckBox ID="IS_IN_SEARCH" runat="server" Text="Розыск" TextAlign="Left" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="HAS_INCLUSIONS" runat="server" Text="Наличие фондовых включений"
                                        TextAlign="Left" />
                                </td>
                                <td>
                                    <asp:CheckBox ID="IS_LOST" runat="server" Text="Пути розыска исчерпаны" TextAlign="Left" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table style="margin-left: 100px;">
                            <colgroup>
                                <col />
                                <col />
                            </colgroup>
                            <tr>
                                <td>
                                    Кол-во ед. хр. с драгоценными камнями и металлами в оформлении
                                </td>
                                <td>
                                    <eq:eqTextBox ID="TREASURE_UNITS_COUNT" runat="server" Width="50px" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <fieldset>
                            <legend>Примечание </legend>
                            <eq:eqTextBox ID="NOTE" runat="server" TextMode="MultiLine" Rows="4" />
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <fieldset>
                            <legend>Топография: </legend>
                            <%--СПЕЦИФИКАЦИЯ dbo.REF_LOCATION--%>
                            <eq:eqSpecification ID="vREF_LOCATION_FUND" runat="server" ShowNumering="false">
                                <eq:eqSpecificationColumn HeaderText="Рубрика топографического указателя">
                                    <ItemTemplate>
                                        <eq2:eqDocument ID="ISN_LOCATION" runat="server" DocTypeURL="LOCATION.ascx" ListDocMode="NewWindowAndSelect"
                                            LoadDocMode="NewWindowAndSelect" ShowingFields="PATH_STR" ShowingFieldsFormat="{0}"
                                            KeyName="ISN_LOCATION" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Примечание">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="NOTE" runat="server"></eq:eqTextBox>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
            </table>
        </asp:View>
    </asp:MultiView>
</div>
