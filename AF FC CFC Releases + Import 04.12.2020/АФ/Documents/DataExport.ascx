﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="DataExport.ascx.vb" Inherits="WebApplication.DataExport" %>

<table style="width: 700px;">
  <tr>
    <th>
      Экспорт в Фондовый Каталог
    </th>
    <th></th>
  </tr>
  <tr>
    <td>Имя файла экспорта </td>
    <td style="width: 500px;"><asp:TextBox ID="fcDbName" runat="server" Text="Export_to_FC" ReadOnly="false"  Width="100%"></asp:TextBox></td>
  </tr>
  <tr>
    <td>Путь выгрузки </td>
    <td><asp:TextBox ID="fcDbBacPath" runat="server" Text="D:\" ReadOnly="true"  Width="100%"></asp:TextBox></td>
  </tr>
  
  <tr>
    <td>Использовать сжатие данных</td>
    <td>
      <asp:CheckBox runat="server" ID="isZiped"  AutoPostBack="True" />
    </td>
  </tr>
  <tr>
    <td>
        <asp:Button ID="just_do_it" runat="server" Text="Выполнить экспорт" />
    </td>
    <td>
        Загрузить: <asp:LinkButton ID="refBacFile" runat="server" Text="link" OnClick="refBacFile_Click"  />
        
    </td>
  </tr>
</table>