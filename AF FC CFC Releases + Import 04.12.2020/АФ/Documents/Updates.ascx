﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Updates.ascx.vb" Inherits="WebApplication.Updates" %>
<div class="LogicBlockCaption">
    Update at
    <asp:Label ID="CreationDateTime" runat="server" />
</div>
<table width="100%" class="LogicBlockTable">
    <colgroup>
        <col style="width: 150px;" />
        <col />
    </colgroup>
    <tr>
        <td>
            UpdateScript
        </td>
        <td>
            <eq:eqTextBox ID="UpdateScript" runat="server" TextMode="MultiLine" Rows="10" Width="100%" />
        </td>
    </tr>
    <tr>
        <td>
            UndoScript
        </td>
        <td>
            <eq:eqTextBox ID="UndoScript" runat="server" TextMode="MultiLine" Rows="10" Width="100%" />
        </td>
    </tr>
</table>
