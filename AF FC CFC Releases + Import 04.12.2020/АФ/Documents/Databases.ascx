﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Databases.ascx.vb" Inherits="WebApplication.Databases" %>

<div class="LogicBlockCaption">
    Базы данных
</div>
<table width="100%" class="LogicBlockTable">
    <colgroup>
        <col style="width: 120px;" />
        <col />
    </colgroup>
    <tr>
        <td>
            Название
        </td>
        <td>
            <eq:eqTextBox ID="Name" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            Строка подключения
        </td>
        <td>
            <eq:eqTextBox ID="Connection" runat="server" TextMode="MultiLine" Rows="2" />
        </td>
    </tr>
</table>