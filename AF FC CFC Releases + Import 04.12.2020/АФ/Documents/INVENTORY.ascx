﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="INVENTORY.ascx.vb"
    Inherits="WebApplication.INVENTORY" %>

<script type="text/javascript">
    function _updateUI() {
        $(":input[name*=PRESENCE_FLAG]").bind('change', function() {
            $(":input[name*=ABSENCE_REASON]").prop("disabled", $(":input[name*=PRESENCE_FLAG]").prop("selectedIndex") == 0);
            $(":input[name*=MOVEMENT_NOTE]").prop("disabled", $(":input[name*=ABSENCE_REASON]").prop("selectedIndex") == 0);
        });
        $(":input[name*=ABSENCE_REASON]").bind('change', function() {
            $(":input[name*=MOVEMENT_NOTE]").prop("disabled", $(":input[name*=ABSENCE_REASON]").prop("selectedIndex") == 0);
        });
        $(":input[name*=ABSENCE_REASON]").prop("disabled", $(":input[name*=PRESENCE_FLAG]").prop("selectedIndex") == 0);
        $(":input[name*=MOVEMENT_NOTE]").prop("disabled", $(":input[name*=ABSENCE_REASON]").prop("selectedIndex") == 0);
        var __inv_type = function() {
            var v = $(":input[name*=ISN_INVENTORY_TYPE]").prop("selectedIndex");
            $("#div_V_enabled").css("display", ((v >= 4 && v <= 8) ? '' : 'none'));
            $("#div_V_disabled").css("display", !(v >= 4 && v <= 8) ? '' : 'none');
        }
        $(":input[name*=ISN_INVENTORY_TYPE]").bind('change', __inv_type);
        __inv_type();

        var sr = $('table[id*=vINVENTORY_DOCUMENT_STATS_A] tr[innerHTML*=Фотодокументы] td');
        $(':input:odd', sr).parent().hide();
        $('span[innerHTML*=уч]', sr).hide();

        $(":input[name*=CARRIER_TYPE]").bind('change', function() {
            var v = $(":input[name*=CARRIER_TYPE]:radio:checked").val();
            $(":input[name*=HAS_TRADITIONAL_DOCS]").prop("disabled", v == "T");
            $(":input[name*=HAS_ELECTRONIC_DOCS]").prop("disabled", v == "E");
            if (v == "T") $(":input[name*=HAS_TRADITIONAL_DOCS]").prop("checked", false);
            else if (v == "E") $(":input[name*=HAS_ELECTRONIC_DOCS]").prop("checked", false);
        }).change();
    }    
</script>

<table style="width: 97%; margin-bottom: 4px;">
    <tr>
        <td>
            <eq2:ArchiveCrumbs ID="Crumbs" runat="server" Level="FUND" />
        </td>
        <td style="text-align: right;">
            Содержимое:
            <asp:HyperLink ID="HyperLinkUNITs" runat="server" Text="Единицы хранения / Единицы учета"
                Target="_blank" NavigateUrl="~/Default.aspx?DocTypeURL=UNIT.ascx&ListDoc=True"
                Style="font-weight: bold; padding: 2px;" />
            <asp:HyperLink ID="HyperLinkUNIT2s" runat="server" Text="Единицы хранения / Единицы учета (КФД)"
                Target="_blank" NavigateUrl="~/Default.aspx?DocTypeURL=UNIT2.ascx&ListDoc=True"
                Style="font-weight: bold; padding: 2px;" />
            <asp:HyperLink ID="HyperLinkSTRs" runat="server" Text="Разделы описи" Target="_blank"
                NavigateUrl="~/Default.aspx?DocTypeURL=INVENTORYSTRUCTURE.ascx&ListDoc=True"
                Style="font-weight: bold; padding: 2px;" />
        </td>
    </tr>
</table>
<eq:eqMenu ID="MenuTabs" runat="server" Orientation="Horizontal" StaticEnableDefaultPopOutImage="False"
    ItemWrap="false" HorizontalMaxCount="5">
    <Items>
        <asp:MenuItem Text="Общие сведения" Value="0" Selected="true" />
        <asp:MenuItem Text="Объем: трад. носители" Value="1" />
        <asp:MenuItem Text="Объем: эл. носители" Value="2" />
        <asp:MenuItem Text="НСА 1" Value="3" />
        <asp:MenuItem Text="НСА 2" Value="4" />
        <asp:MenuItem Text="Физ состояние" Value="5" />
        <asp:MenuItem Text="Акты и топографирование" Value="6" />
    </Items>
    <StaticSelectedStyle CssClass="MenuItemStyleSelected" />
</eq:eqMenu>
<div style="border: solid 1px black; padding: 5px;">
    <asp:MultiView ID="MultiViewTabs" runat="server" ActiveViewIndex="0">
        <%--! Общие сведения--%>
        <asp:View runat="server">
            <table style="width: 700px;">
                <colgroup>
                    <col align="left" />
                    <col />
                    <col align="right" />
                    <col />
                </colgroup>
                <tr>
                    <td>
                        <asp:CheckBox ID="ForbidRecalc" runat="server" Text="запретить пересчет"  />
                    </td>
                    <td>
                        
                    </td>
                </tr>
                <tr>
                    <td>
                        Номер:*
                    </td>
                    <td>
                        <eq:eqTextBox ID="INVENTORY_NUM_1" Width="60px" runat="server" MaxLength="7" />
                        <eq:eqTextBox ID="INVENTORY_NUM_2" Width="50px" runat="server" MaxLength="7" />
                        <aj:FilteredTextBoxExtender runat="server" TargetControlID="INVENTORY_NUM_1" FilterType="Numbers" />
                        <aj:FilteredTextBoxExtender runat="server" TargetControlID="INVENTORY_NUM_2" FilterType="Custom"
                            ValidChars="АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЬЫЪЭЮЯ1234567890.,-" />
                    </td>
                    <td>
                        Том:
                    </td>
                    <td>
                        <eq:eqTextBox ID="INVENTORY_NUM_3" Width="60px" runat="server" MaxLength="7" />
                        <aj:FilteredTextBoxExtender runat="server" TargetControlID="INVENTORY_NUM_3" FilterType="Custom, Numbers"
                            ValidChars="АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЬЫЪЭЮЯ1234567890.,-" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Количество экземпляров описи:*
                    </td>
                    <td>
                        <eq:eqTextBox ID="COPY_COUNT" Width="40px" runat="server" />
                    </td>
                    <td>
                        Срок хранения:*
                    </td>
                    <td>
                        <asp:DropDownList ID="INVENTORY_KEEP_PERIOD" runat="server">
                            <asp:ListItem Text="постоянно" Value="a" />
                            <asp:ListItem Text="временно" Value="b" />
                            <asp:ListItem Text="депозит" Value="c" />
                            <asp:ListItem Text="частично временно" Value="d" />
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        Название описи:
                    </td>
                    <td colspan="3">
                        <eq:eqTextBox ID="INVENTORY_NAME" runat="server" TextMode="MultiLine" Rows="4" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Категория:*
                    </td>
                    <td>
                        <asp:DropDownList ID="INVENTORY_KIND" runat="server">
                            <asp:ListItem Text="1" Value="a" />
                            <asp:ListItem Text="2" Value="b" />
                            <asp:ListItem Text="3" Value="c" />
                            <asp:ListItem Text="без категории" Value="d" />
                        </asp:DropDownList>
                    </td>
                    <td>
                        Тип описи:*
                    </td>
                    <td>
                        <asp:DropDownList ID="ISN_INVENTORY_TYPE" runat="server" Width="100%" AutoPostBack="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Видовой состав:
                    </td>
                    <td colspan="3">
                        <%--СПРАВОЧНИК--%>
                        <%-- ! надо фильтровать ! --%>
                        <div id="div_V_enabled" style="display: block;">
                            <eq2:eqDocument ID="ISN_DOC_KIND" runat="server" DocTypeURL="DOC_KIND_CL.ascx" ListDocMode="NewWindowAndSelect"
                                LoadDocMode="ListDocAndSelect" ShowingFields="NAME" ShowingFieldsFormat="{0}" />
                        </div>
                        <div id="div_V_disabled" disabled="true" style="display: none;">
                            Выбрать
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        Тип документов:*
                    </td>
                    <td colspan="3">
                        <%--СПЕЦИФИКАЦИЯ INVENTORY_DOC_TYPE--%>
                        <eq:eqSpecification ID="tblINVENTORY_DOC_TYPE" runat="server">
                            <eq:eqSpecificationColumn>
                                <ItemTemplate>
                                    <eq:eqDropDownList ID="ISN_DOC_TYPE" runat="server" Width="100%" UniqueGroupName="INVENTORY_DOC_TYPE" />
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                        </eq:eqSpecification>
                    </td>
                </tr>
                <tr>
                    <td>
                        Носитель документов:
                    </td>
                    <td colspan="3">
                        <%--СПЕЦИФИКАЦИЯ dbo.INVENTORY_DOC_STORAGE--%>
                        <eq:eqSpecification ID="tblINVENTORY_DOC_STORAGE" runat="server">
                            <eq:eqSpecificationColumn>
                                <ItemTemplate>
                                    <eq:eqDropDownList ID="ISN_STORAGE_MEDIUM" runat="server" Width="100%" UniqueGroupName="INVENTORY_DOC_STORAGE" />
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                        </eq:eqSpecification>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <fieldset>
                            <legend>Тип носителей документов*</legend>
                            <asp:RadioButtonList ID="CARRIER_TYPE" runat="server">
                                <asp:ListItem Text="традиционный" Value="T"></asp:ListItem>
                                <asp:ListItem Text="электронный" Value="E"></asp:ListItem>
                            </asp:RadioButtonList>
                            <br />
                            <asp:CheckBox ID="HAS_TRADITIONAL_DOCS" runat="server" Text="Содержит док. на традиционных носителях" />
                            <asp:CheckBox ID="HAS_ELECTRONIC_DOCS" runat="server" Text="Содержит док. на электронных носителях" />
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        Способ воспроизведения*:
                    </td>
                    <td colspan="3">
                        <%--СПРАВОЧНИК--%>
                        <eq2:eqDocument ID="ISN_REPRODUCTION_METHOD" runat="server" DocTypeURL="REPRODUCTION_METHOD_CL.ascx"
                            ListDocMode="NewWindowAndSelect" LoadDocMode="ListDocAndSelect" ShowingFields="NAME"
                            ShowingFieldsFormat="{0}" KeyName="ISN_REPRODUCTION_METHOD" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Источник поступления*:
                    </td>
                    <td>
                        <%--СПРАВОЧНИК--%>
                        <eq2:eqDocument ID="ISN_RECEIPT_SOURCE" runat="server" DocTypeURL="RECEIPT_SOURCE_CL.ascx"
                            ListDocMode="NewWindowAndSelect" LoadDocMode="ListDocAndSelect" ShowingFields="NAME"
                            ShowingFieldsFormat="{0}" />
                    </td>
                    <td>
                        Основание поступления*:
                    </td>
                    <td>
                        <%--СПРАВОЧНИК--%>
                        <eq2:eqDocument ID="ISN_RECEIPT_REASON" runat="server" DocTypeURL="RECEIPT_REASON_CL.ascx"
                            ListDocMode="NewWindowAndSelect" LoadDocMode="ListDocAndSelect" ShowingFields="NAME"
                            ShowingFieldsFormat="{0}" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Движение*:
                    </td>
                    <td>
                        <asp:Panel runat="server" ID="MovePanel">
                            <asp:DropDownList ID="PRESENCE_FLAG" runat="server">
                                <asp:ListItem Text="в наличии" Value="a" />
                                <asp:ListItem Text="выбыл" Value="b" />
                            </asp:DropDownList>
                            <asp:DropDownList ID="ABSENCE_REASON" runat="server">
                                <asp:ListItem Text="" Value="" />
                                <asp:ListItem Text="переданный" Value="a" />
                                <asp:ListItem Text="присоединенный" Value="b" />
                                <asp:ListItem Text="утраченный" Value="c" />
                                <asp:ListItem Text="выделенный к уничтожению" Value="d" />
                            </asp:DropDownList>
                        </asp:Panel>
                    </td>
                    <td>
                        Доступ:*
                    </td>
                    <td>
                        <asp:DropDownList ID="SECURITY_CHAR" runat="server" AutoPostBack="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Хар-ка секретности:*
                    </td>
                    <td>
                        <asp:DropDownList ID="ISN_SECURLEVEL" runat="server" AutoPostBack="true" />
                    </td>
                    <td>
                        Причина ограничения доступа:
                    </td>
                    <td>
                        <asp:DropDownList ID="ISN_SECURITY_REASON" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        Справка по движению:
                    </td>
                    <td colspan="3">
                        <eq:eqTextBox ID="MOVEMENT_NOTE" runat="server" TextMode="MultiLine" Rows="4" />
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <fieldset>
                            <legend>Даты документов</legend>
                            <table>
                                <tr>
                                    <td>
                                        Начальная дата:
                                    </td>
                                    <td style="width: 80px;">
                                        <eq:eqTextBox ID="DOC_START_YEAR" runat="server" Width="100px" />
                                        <aj:FilteredTextBoxExtender runat="server" TargetControlID="DOC_START_YEAR" FilterType="Numbers" />
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="DOC_START_YEAR_INEXACT" runat="server" Text="неточная" TextAlign="Left" />
                                    </td>
                                    <td>
                                        Конечная дата:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="DOC_END_YEAR" runat="server" Width="100px" />
                                        <aj:FilteredTextBoxExtender runat="server" TargetControlID="DOC_END_YEAR" FilterType="Numbers" />
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="DOC_END_YEAR_INEXACT" runat="server" Text="неточная" TextAlign="Left" />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        Признаки группировки ед.хр.:
                    </td>
                    <td colspan="3">
                        <%--СПЕЦИФИКАЦИЯ dbo.INVENTORY_GROUPING_ATTRIBUTE--%>
                        <eq:eqSpecification ID="tblINVENTORY_GROUPING_ATTRIBUTE" runat="server">
                            <eq:eqSpecificationColumn>
                                <ItemTemplate>
                                    <eq:eqDropDownList ID="ISN_GROUPING_ATTRIBUTE" runat="server" Width="100%" UniqueGroupName="INVENTORY_GROUPING_ATTRIBUTE" />
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                        </eq:eqSpecification>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        Примечание:
                    </td>
                    <td colspan="3">
                        <eq:eqTextBox ID="NOTE" runat="server" TextMode="MultiLine" Rows="4" />
                    </td>
                </tr>
            </table>
            <table style="width: 700px;">
                <tr>
                    <td>
                        <fieldset>
                            <legend>Файлы электронного образа</legend>
                            <eq:eqSpecification ID="vREF_FILE_INVENTORY_S" runat="server" Width="100%" AllowInsert="false">
                                <eq:eqSpecificationColumn HeaderText="Описание">
                                    <ItemTemplate>
                                        <asp:TextBox ID="NAME" runat="server" Width="99%" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Файл">
                                    <ItemTemplate>
                                        <eq2:RefFile ID="ID" runat="server" Mode="FileSystem" OnRequesting="RefFileS_Requesting" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                            <table>
                                <tr>
                                    <td>
                                        Добавить файл:
                                    </td>
                                    <td>
                                        <eq2:RefFileUpload ID="RefFileUploadFileS" runat="server" Mode="FileSystem" />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
            </table>
        </asp:View>
        <%--! Объем: трад. носители--%>
        <asp:View runat="server">
            <asp:Panel ID="Panel_T" runat="server">
                <table style="width: 700px;">
                    <tr>
                        <td>
                            <%--СПЕЦИФИКАЦИЯ "Всего"--%>
                            <eq:eqSpecification ID="vINVENTORY_DOCUMENT_STATS" runat="server" ShowNumering="false"
                                AllowDelete="false" AllowInsert="false" AllowReorder="false">
                                <eq:eqSpecificationColumn HeaderText="">
                                    <ItemTemplate>
                                        <asp:Label ID="ISN_DOC_TYPE" runat="server" Width="120" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="">
                                    <ItemTemplate>
                                        <nobr>ед. хр.</nobr>
                                        <br />
                                        <nobr>ед. уч.</nobr>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Всего ед. хранения">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_COUNT" runat="server" SkinID="Numeric" />
                                        <eq:eqTextBox ID="REG_UNIT" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <%--<eq:eqSpecificationColumn HeaderText="Всего по описям">
                                <ItemTemplate>
                                    <eq:eqTextBox ID="UNIT_INVENTORY" runat="server" SkinID="Numeric" />
                                    <eq:eqTextBox ID="REG_UNIT_INVENTORY" runat="server" SkinID="Numeric" />
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>--%>
                                <eq:eqSpecificationColumn HeaderText="Введено">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_REGISTERED" runat="server" SkinID="Numeric" />
                                        <eq:eqTextBox ID="REG_UNIT_REGISTERED" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <%--<eq:eqSpecificationColumn HeaderText="Неописанные">
                                <ItemTemplate>
                                    <eq:eqTextBox ID="UNIT_UNDESCRIBED" runat="server" SkinID="Numeric" />
                                    <eq:eqTextBox ID="REG_UNIT_UNDESCRIBED" runat="server" SkinID="Numeric" />
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>--%>
                                <eq:eqSpecificationColumn HeaderText="ОЦД">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_OC_COUNT" runat="server" SkinID="Numeric" />
                                        <eq:eqTextBox ID="REG_UNIT_OC" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Уникальные">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNITS_UNIQUE" runat="server" SkinID="Numeric" />
                                        <eq:eqTextBox ID="REG_UNITS_UNIQUE" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Имеют СФ">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_HAS_SF" runat="server" SkinID="Numeric" />
                                        <eq:eqTextBox ID="REG_UNIT_HAS_SF" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Имеют ФП">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_HAS_FP" runat="server" SkinID="Numeric" />
                                        <eq:eqTextBox ID="REG_UNIT_HAS_FP" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Необнаруж.">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNITS_NOT_FOUND" runat="server" SkinID="Numeric" />
                                        <eq:eqTextBox ID="REG_UNITS_NOT_FOUND" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Секретные">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="SECRET_UNITS" runat="server" SkinID="Numeric" />
                                        <eq:eqTextBox ID="SECRET_REG_UNITS" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Закаталог.">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNITS_CATALOGUED" runat="server" SkinID="Numeric" />
                                        <eq:eqTextBox ID="REG_UNITS_CTALOGUE" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </td>
                    </tr>
                </table>
                <table style="width: 700px;">
                    <tr>
                        <td>
                            <fieldset>
                                <legend>Документы на бумажной основе </legend>
                                <%--СПЕЦИФИКАЦИЯ "Документы на бумажной основе"--%>
                                <eq:eqSpecification ID="vINVENTORY_DOCUMENT_STATS_P" runat="server" ShowNumering="false"
                                    AllowDelete="false" AllowInsert="false" AllowReorder="false">
                                    <eq:eqSpecificationColumn HeaderText="">
                                        <ItemTemplate>
                                            <asp:Label ID="ISN_DOC_TYPE" runat="server" Width="150" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Всего ед. хранения">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNIT_COUNT" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <%--<eq:eqSpecificationColumn HeaderText="Всего по описям">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_INVENTORY" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>--%>
                                    <eq:eqSpecificationColumn HeaderText="Введено">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNIT_REGISTERED" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <%--<eq:eqSpecificationColumn HeaderText="Неописанные">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_UNDESCRIBED" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>--%>
                                    <eq:eqSpecificationColumn HeaderText="ОЦД">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNIT_OC_COUNT" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Уникальные">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNITS_UNIQUE" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Имеют СФ">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNIT_HAS_SF" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Имеют ФП">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNIT_HAS_FP" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Необнаруж.">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNITS_NOT_FOUND" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Секретные">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="SECRET_UNITS" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Закаталог.">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNITS_CATALOGUED" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                </eq:eqSpecification>
                            </fieldset>
                        </td>
                    </tr>
                </table>
                <table style="width: 700px;">
                    <colgroup>
                        <col />
                        <col />
                        <col align="right" />
                        <col />
                        <col align="right" />
                        <col />
                    </colgroup>
                    <tr>
                        <td>
                            Неописанные документы (количество документов)
                        </td>
                        <td>
                            <eq:eqTextBox ID="UNDESCRIBED_DOC_COUNT" runat="server" SkinID="Numeric" />
                        </td>
                        <td>
                            Из них личного происхождения
                        </td>
                        <td>
                            <eq:eqTextBox ID="PERSONAL_UNDESCRIBED_DOC_COUNT" runat="server" SkinID="Numeric" />
                        </td>
                        <td>
                            Неописанные листы (кол-во листов)
                        </td>
                        <td>
                            <eq:eqTextBox ID="UNDECSRIBED_PAGE_COUNT" runat="server" SkinID="Numeric" />
                        </td>
                    </tr>
                </table>
                <table style="width: 700px;">
                    <tr>
                        <td>
                            <fieldset>
                                <legend>Аудиовизуальные документы на традиционных носителях </legend>
                                <%--СПЕЦИФИКАЦИЯ "Аудиовизуальные документы на традиционных носителях"--%>
                                <eq:eqSpecification ID="vINVENTORY_DOCUMENT_STATS_A" runat="server" ShowNumering="false"
                                    AllowDelete="false" AllowInsert="false" AllowReorder="false">
                                    <eq:eqSpecificationColumn HeaderText="">
                                        <ItemTemplate>
                                            <asp:Label ID="ISN_DOC_TYPE" runat="server" Width="120" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="">
                                        <ItemTemplate>
                                            <span>
                                                <nobr>ед. хр.</nobr>
                                            </span>
                                            <br />
                                            <span>
                                                <nobr>ед. уч.</nobr>
                                            </span>
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Всего ед. хранения">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNIT_COUNT" runat="server" SkinID="Numeric" />
                                            <eq:eqTextBox ID="REG_UNIT" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <%--<eq:eqSpecificationColumn HeaderText="Всего по описям">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_INVENTORY" runat="server" />
                                        <eq:eqTextBox ID="REG_UNIT_INVENTORY" runat="server" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>--%>
                                    <eq:eqSpecificationColumn HeaderText="Введено">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNIT_REGISTERED" runat="server" SkinID="Numeric" />
                                            <eq:eqTextBox ID="REG_UNIT_REGISTERED" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <%--<eq:eqSpecificationColumn HeaderText="Неописанные">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_UNDESCRIBED" runat="server" SkinID="Numeric" />
                                        <eq:eqTextBox ID="REG_UNIT_UNDESCRIBED" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>--%>
                                    <eq:eqSpecificationColumn HeaderText="ОЦД">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNIT_OC_COUNT" runat="server" SkinID="Numeric" />
                                            <eq:eqTextBox ID="REG_UNIT_OC" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Уникальные">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNITS_UNIQUE" runat="server" SkinID="Numeric" />
                                            <eq:eqTextBox ID="REG_UNITS_UNIQUE" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Имеют СФ">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNIT_HAS_SF" runat="server" SkinID="Numeric" />
                                            <eq:eqTextBox ID="REG_UNIT_HAS_SF" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Имеют ФП">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNIT_HAS_FP" runat="server" SkinID="Numeric" />
                                            <eq:eqTextBox ID="REG_UNIT_HAS_FP" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Необнаруж.">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNITS_NOT_FOUND" runat="server" SkinID="Numeric" />
                                            <eq:eqTextBox ID="REG_UNITS_NOT_FOUND" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Секретные">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="SECRET_UNITS" runat="server" SkinID="Numeric" />
                                            <eq:eqTextBox ID="SECRET_REG_UNITS" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Закаталог.">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNITS_CATALOGUED" runat="server" SkinID="Numeric" />
                                            <eq:eqTextBox ID="REG_UNITS_CTALOGUE" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                </eq:eqSpecification>
                            </fieldset>
                        </td>
                    </tr>
                </table>
                <table style="width: 700px;">
                    <tr>
                        <td>
                            <fieldset>
                                <legend>Микроформы</legend>
                                <%--СПЕЦИФИКАЦИЯ "Микроформы"--%>
                                <eq:eqSpecification ID="vINVENTORY_DOCUMENT_STATS_M" runat="server" ShowNumering="false"
                                    AllowDelete="false" AllowInsert="false" AllowReorder="false">
                                    <eq:eqSpecificationColumn HeaderText="">
                                        <ItemTemplate>
                                            <asp:Label ID="ISN_DOC_TYPE" runat="server" Width="120" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="">
                                        <ItemTemplate>
                                            <nobr>ед. хр.</nobr>
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Всего ед. хранения">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNIT_COUNT" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <%--<eq:eqSpecificationColumn HeaderText="Всего по описям">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_INVENTORY" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>--%>
                                    <eq:eqSpecificationColumn HeaderText="Введено">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNIT_REGISTERED" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <%--<eq:eqSpecificationColumn HeaderText="Неописанные">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_UNDESCRIBED" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>--%>
                                    <eq:eqSpecificationColumn HeaderText="ОЦД">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNIT_OC_COUNT" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Уникальные">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNITS_UNIQUE" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Имеют СФ">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNIT_HAS_SF" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Имеют ФП">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNIT_HAS_FP" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Необнаруж.">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNITS_NOT_FOUND" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Секретные">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="SECRET_UNITS" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Закаталог.">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNITS_CATALOGUED" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                </eq:eqSpecification>
                            </fieldset>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </asp:View>
        <%--! Объем: эл. носители--%>
        <asp:View runat="server">
            <asp:Panel ID="Panel_E" runat="server">
                <table style="width: 700px;">
                    <tr>
                        <td>
                            <fieldset>
                                <legend>Документы на электронных носителях</legend>
                                <%--СПЕЦИФИКАЦИЯ "Документы на электронных носителях"--%>
                                <eq:eqSpecification ID="vINVENTORY_DOCUMENT_STATS_E" runat="server" ShowNumering="false"
                                    AllowDelete="false" AllowInsert="false" AllowReorder="false">
                                    <eq:eqSpecificationColumn HeaderText="">
                                        <ItemTemplate>
                                            <asp:Label ID="ISN_DOC_TYPE" runat="server" Width="120" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="">
                                        <ItemTemplate>
                                            <nobr>ед. хр.</nobr>
                                            <br />
                                            <nobr>ед. уч.</nobr>
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Всего ед. хранения">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNIT_COUNT" runat="server" SkinID="Numeric" />
                                            <eq:eqTextBox ID="REG_UNIT" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <%--<eq:eqSpecificationColumn HeaderText="Всего по описям">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_INVENTORY" runat="server" SkinID="Numeric" />
                                        <eq:eqTextBox ID="REG_UNIT_INVENTORY" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>--%>
                                    <eq:eqSpecificationColumn HeaderText="Введено">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNIT_REGISTERED" runat="server" SkinID="Numeric" />
                                            <eq:eqTextBox ID="REG_UNIT_REGISTERED" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <%--<eq:eqSpecificationColumn HeaderText="Неописанные">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_UNDESCRIBED" runat="server" SkinID="Numeric" />
                                        <eq:eqTextBox ID="REG_UNIT_UNDESCRIBED" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>--%>
                                    <eq:eqSpecificationColumn HeaderText="ОЦД">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNIT_OC_COUNT" runat="server" SkinID="Numeric" />
                                            <eq:eqTextBox ID="REG_UNIT_OC" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Уникальные">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNITS_UNIQUE" runat="server" SkinID="Numeric" />
                                            <eq:eqTextBox ID="REG_UNITS_UNIQUE" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Имеют СФ">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNIT_HAS_SF" runat="server" SkinID="Numeric" />
                                            <eq:eqTextBox ID="REG_UNIT_HAS_SF" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Имеют ФП">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNIT_HAS_FP" runat="server" SkinID="Numeric" />
                                            <eq:eqTextBox ID="REG_UNIT_HAS_FP" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Необнаруж.">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNITS_NOT_FOUND" runat="server" SkinID="Numeric" />
                                            <eq:eqTextBox ID="REG_UNITS_NOT_FOUND" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Секретные">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="SECRET_UNITS" runat="server" SkinID="Numeric" />
                                            <eq:eqTextBox ID="SECRET_REG_UNITS" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                    <eq:eqSpecificationColumn HeaderText="Закаталог.">
                                        <ItemTemplate>
                                            <eq:eqTextBox ID="UNITS_CATALOGUED" runat="server" SkinID="Numeric" />
                                            <eq:eqTextBox ID="REG_UNITS_CTALOGUE" runat="server" SkinID="Numeric" />
                                        </ItemTemplate>
                                    </eq:eqSpecificationColumn>
                                </eq:eqSpecification>
                            </fieldset>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </asp:View>
        <%--НСА 1--%>
        <asp:View runat="server">
            <table style="width: 900px;">
                <tr>
                    <td colspan="2">
                        <fieldset>
                            <legend>Аннотация</legend>
                            <eq:eqTextBox ID="ANNOTATE" runat="server" TextMode="MultiLine" Rows="4" />
                        </fieldset>
                    </td>
                </tr>
                <%--<tr>
                    <td colspan="2">
                        <fieldset>
                            <legend>*Историческая справка</legend>
                            <eq:eqTextBox ID="INVENTORY_HISTORY" runat="server" TextMode="MultiLine" Rows="4" />
                        </fieldset>
                    </td>
                </tr>
                --%>
                <tr>
                    <td colspan="2">
                        <fieldset>
                            <legend>Вспомогательный НСА</legend>
                            <eq:eqTextBox ID="ADDITIONAL_NSA" runat="server" TextMode="MultiLine" Rows="4" />
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <fieldset>
                            <legend>Файлы</legend>
                            <eq:eqSpecification ID="vREF_FILE_INVENTORY" runat="server" Width="100%" AllowInsert="false">
                                <eq:eqSpecificationColumn HeaderText="Описание">
                                    <ItemTemplate>
                                        <asp:TextBox ID="NAME" runat="server" Width="99%" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Файл">
                                    <ItemTemplate>
                                        <eq2:RefFile ID="ID" runat="server" Mode="FileSystem" OnRequesting="RefFile_Requesting" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                            <table>
                                <tr>
                                    <td>
                                        Добавить файл:
                                    </td>
                                    <td>
                                        <eq2:RefFileUpload ID="RefFileUploadFile" runat="server" Mode="FileSystem" />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <fieldset>
                            <legend>Особенности</legend>
                            <%--СПЕЦИФИКАЦИЯ dbo.REF_FEATURE--%>
                            <eq:eqSpecification ID="vREF_FEATURE_INVENTORY" runat="server">
                                <eq:eqSpecificationColumn>
                                    <ItemTemplate>
                                        <eq:eqDropDownList ID="ISN_FEATURE" runat="server" Width="100%" UniqueGroupName="REF_FEATURE" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <fieldset>
                            <legend>Язык</legend>
                            <%--СПЕЦИФИКАЦИЯ dbo.REF_LANGUAGE--%>
                            <eq:eqSpecification ID="vREF_LANGUAGE_INVENTORY" runat="server">
                                <eq:eqSpecificationColumn>
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ISN_LANGUAGE" runat="server" Width="100%" UniqueGroupName="REF_LANGUAGE" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:CheckBox ID="MUSEUM_UNITS_COUNT" runat="server" Text="Наличие музейных предметов"
                            TextAlign="Left" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Кол-во ед. хр. с драгоценными камнями и металлами в оформлении:
                    </td>
                    <td>
                        <eq:eqTextBox ID="UNITS_WITH_TREASURES_COUNT" runat="server" />
                    </td>
                </tr>
            </table>
        </asp:View>
        <%--НСА 2--%>
        <asp:View runat="server">
            <table style="width: 700px;">
                <tr>
                    <td>
                        <fieldset>
                            <legend>Межфондовый АНСА</legend>
                            <%--СПЕЦИФИКАЦИЯ dbo.REF_CLS ; OBJ_KIND=701--%>
                            <eq:eqSpecification ID="vREF_CLS701_INVENTORY" runat="server">
                                <eq:eqSpecificationColumn>
                                    <ItemTemplate>
                                        <eq2:eqDocument ID="ISN_CLS" runat="server" DocTypeURL="CLS701.ascx" ListDocMode="NewWindowAndSelect"
                                            LoadDocMode="ListDocAndSelect" ShowingFields="NAME" ShowingFieldsFormat="{0}" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <fieldset>
                            <legend>Внутрифондовый АНСА</legend>
                            <%--СПЕЦИФИКАЦИЯ dbo.REF_CLS ; OBJ_KIND=702--%>
                            <eq:eqSpecification ID="vREF_CLS702_INVENTORY" runat="server">
                                <eq:eqSpecificationColumn>
                                    <ItemTemplate>
                                        <eq2:eqDocument ID="ISN_CLS" runat="server" DocTypeURL="CLS702.ascx" ListDocMode="NewWindowAndSelect"
                                            LoadDocMode="ListDocAndSelect" ShowingFields="NAME" ShowingFieldsFormat="{0}" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <fieldset>
                            <legend>АНСА в рамках описи</legend>
                            <%--СПЕЦИФИКАЦИЯ dbo.REF_CLS ; OBJ_KIND=703--%>
                            <eq:eqSpecification ID="vREF_CLS703_INVENTORY" runat="server">
                                <eq:eqSpecificationColumn>
                                    <ItemTemplate>
                                        <eq2:eqDocument ID="ISN_CLS" runat="server" DocTypeURL="CLS703.ascx" ListDocMode="NewWindowAndSelect"
                                            LoadDocMode="ListDocAndSelect" ShowingFields="NAME" ShowingFieldsFormat="{0}" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <fieldset>
                            <legend>Бумажный НСА описи</legend>
                            <%--СПЕЦИФИКАЦИЯ dbo.INVENTORY_PAPER_CLS--%>
                            <eq:eqSpecification ID="tblINVENTORY_PAPER_CLS" runat="server">
                                <eq:eqSpecificationColumn>
                                    <ItemTemplate>
                                        <eq2:eqDocument ID="ISN_PAPER_CLS_INV" runat="server" DocTypeURL="PAPER_CLS_INV.ascx"
                                            ListDocMode="NewWindowAndSelect" LoadDocMode="ListDocAndSelect" ShowingFields="NAME"
                                            ShowingFieldsFormat="{0}" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
            </table>
        </asp:View>
        <%--Физ состояние--%>
        <asp:View runat="server">
            <table style="width: 900px;">
                <colgroup>
                    <col style="width: 35%;" />
                    <col />
                </colgroup>
                <tr>
                    <td colspan="2">
                        <fieldset>
                            <legend>Акты проверки наличия и состояния единиц хранения </legend>
                            <%--СПЕЦИФИКАЦИЯ vREF_ACT_FOR_INVENTORY_UNIT--%>
                            <eq:eqSpecification ID="vREF_ACT_FOR_INVENTORY_UNIT" runat="server" ShowNumering="false"
                                AllowDelete="false" AllowInsert="false">
                                <eq:eqSpecificationColumn HeaderText="Акт">
                                    <ItemTemplate>
                                        <eq2:eqDocument ID="ISN_ACT" runat="server" DocTypeURL="ACT.ascx" ListDocMode="NewWindowAndSelect"
                                            LoadDocMode="ListDocAndSelect" ShowingFields="ACT_NUM" ShowingFieldsFormat="{0}" KeyName="ISN_ACT" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Дата">
                                    <ItemTemplate>
                                        <asp:Label ID="ACT_DATE" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Название">
                                    <ItemTemplate>
                                        <asp:Label ID="ACT_NAME" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Тип акта">
                                    <ItemTemplate>
                                        <asp:Label ID="ACTTYPENAME" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Движение">
                                    <ItemTemplate>
                                        <asp:Label ID="MOVEMENT_FLAG" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Проведённая работа">
                                    <ItemTemplate>
                                        <asp:Label ID="ACT_WORK" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Примечание">
                                    <ItemTemplate>
                                        <asp:Label ID="NOTE" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                            <%--dbo.ACT--%>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td valign="top" style="width: 250px;">
                        <fieldset style="width: 250px;">
                            <legend>Требуемые виды работ с описью </legend>
                            <asp:CheckBox ID="CATALOGUING" runat="server" Text="Каталогизация" />
                            <%--СПЕЦИФИКАЦИЯ INVENTORY_REQUIRED_WORK--%>
                            <eq:eqSpecification ID="tblINVENTORY_REQUIRED_WORK" runat="server">
                                <eq:eqSpecificationColumn HeaderText="">
                                    <ItemTemplate>
                                        <eq2:eqDocument ID="ISN_REQUIRED_WORK" runat="server" DocTypeURL="INV_REQUIRED_WORK_CL.ascx"
                                            ListDocMode="NewWindowAndSelect" LoadDocMode="ListDocAndSelect" ShowingFields="NAME"
                                            ShowingFieldsFormat="{0}" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                    <td valign="top">
                        <fieldset>
                            <legend>Состояние единиц хранения описи </legend>
                            <%--СПЕЦИФИКАЦИЯ dbo.INVENTORY_CHECK--%>
                            <eq:eqSpecification ID="tblINVENTORY_CHECK" runat="server">
                                <eq:eqSpecificationColumn>
                                    <ItemTemplate>
                                        <table style="width: 500px;">
                                            <colgroup>
                                                <col align="right" style="width: 30%;" />
                                                <col align="left" style="width: 30%;" />
                                                <col align="right" style="width: 30%;" />
                                                <col align="left" />
                                            </colgroup>
                                            <tr>
                                                <td>
                                                    Закартонировано
                                                </td>
                                                <td>
                                                    <eq:eqTextBox ID="CARDBOARDED" runat="server" Width="50px" />
                                                </td>
                                                <td>
                                                    Требуют дезинсекции
                                                </td>
                                                <td>
                                                    <eq:eqTextBox ID="UNITS_NEED_DISINSECTION" runat="server" Width="50px" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Требуют картонирования
                                                </td>
                                                <td>
                                                    <eq:eqTextBox ID="UNITS_NEED_CARDBOARDED" runat="server" Width="50px" />
                                                </td>
                                                <td>
                                                    Требуют шифровки
                                                </td>
                                                <td>
                                                    <eq:eqTextBox ID="UNITS_NEED_ENCIPHERING" runat="server" Width="50px" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Неисправимо повреждены
                                                </td>
                                                <td>
                                                    <eq:eqTextBox ID="UNITS_DBR" runat="server" Width="50px" />
                                                </td>
                                                <td>
                                                    Требуют замены обложки
                                                </td>
                                                <td>
                                                    <eq:eqTextBox ID="UNITS_NEED_COVER_CHANGE" runat="server" Width="50px" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Требуют реставрации
                                                </td>
                                                <td>
                                                    <eq:eqTextBox ID="UNITS_NEED_RESTORATION" runat="server" Width="50px" />
                                                </td>
                                                <td>
                                                    Листов с затухающими текстами
                                                </td>
                                                <td>
                                                    <eq:eqTextBox ID="FADING_PAGES" runat="server" Width="50px" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Требуют подшивки или переплета
                                                </td>
                                                <td>
                                                    <eq:eqTextBox ID="UNITS_NEED_BINDING" runat="server" Width="50px" />
                                                </td>
                                                <td>
                                                    На горючей основе
                                                </td>
                                                <td>
                                                    <eq:eqTextBox ID="UNITS_INFLAMMABLE" runat="server" Width="50px" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Требуют дезинфекции
                                                </td>
                                                <td>
                                                    <eq:eqTextBox ID="UNITS_NEED_DISINFECTION" runat="server" Width="50px" />
                                                </td>
                                                <td>
                                                    Требуют КПО
                                                </td>
                                                <td>
                                                    <eq:eqTextBox ID="UNITS_NEED_KPO" runat="server" Width="50px" />
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <fieldset>
                            <legend>Работы, проводимые с документами описи</legend>
                            <eq:eqTextBox ID="INVENTORY_DOC_WORK" runat="server" TextMode="MultiLine" Rows="4" />
                        </fieldset>
                    </td>
                </tr>
            </table>
        </asp:View>
        <%--! Акты и топографирование--%>
        <asp:View runat="server">
            <table style="width: 900px;">
                <colgroup>
                    <col />
                </colgroup>
                <tr>
                    <td>
                        <fieldset>
                            <legend>Акты: </legend>
                            <%--СПЕЦИФИКАЦИЯ vREF_ACT--%>
                            <eq:eqSpecification ID="vREF_ACT_FOR_INVENTORY" runat="server" ShowNumering="false">
                                <eq:eqSpecificationColumn HeaderText="Акт">
                                    <ItemTemplate>
                                        <eq2:eqDocument ID="ISN_ACT" runat="server" DocTypeURL="ACT.ascx" ListDocMode="NewWindowAndSelect"
                                            LoadDocMode="NewWindowAndSelect" ShowingFields="ACT_NUM" ShowingFieldsFormat="{0}"
                                            KeyName="ISN_ACT" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Дата">
                                    <ItemTemplate>
                                        <asp:Label ID="ACT_DATE" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Название">
                                    <ItemTemplate>
                                        <asp:Label ID="ACT_NAME" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Тип акта">
                                    <ItemTemplate>
                                        <asp:Label ID="ACTTYPENAME" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Движение">
                                    <ItemTemplate>
                                        <asp:Label ID="MOVEMENT_FLAG" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Проведённая работа">
                                    <ItemTemplate>
                                        <asp:Label ID="ACT_WORK" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Примечание">
                                    <ItemTemplate>
                                        <asp:Label ID="NOTE" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                            <%--dbo.ACT--%>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <fieldset>
                            <legend>Топография: </legend>
                            <%--СПЕЦИФИКАЦИЯ dbo.REF_LOCATION--%>
                            <eq:eqSpecification ID="vREF_LOCATION_INVENTORY" runat="server" ShowNumering="false">
                                <eq:eqSpecificationColumn HeaderText="Рубрика топографического указателя">
                                    <ItemTemplate>
                                        <eq2:eqDocument ID="ISN_LOCATION" runat="server" DocTypeURL="LOCATION.ascx" ListDocMode="NewWindowAndSelect"
                                            LoadDocMode="NewWindowAndSelect" ShowingFields="PATH_STR" ShowingFieldsFormat="{0}"
                                            KeyName="ISN_LOCATION" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Номер ед.хр. с">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_NUM_FROM" runat="server"></eq:eqTextBox>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                
                                <eq:eqSpecificationColumn HeaderText="Номер ед.хр. с (стар.)">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_NUM_OLD_FROM" runat="server"></eq:eqTextBox>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                
                                <eq:eqSpecificationColumn HeaderText="Номер ед.хр. по">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_NUM_TO" runat="server"></eq:eqTextBox>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                
                                <eq:eqSpecificationColumn HeaderText="Номер ед.хр. по (стар.)">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_NUM_OLD_TO" runat="server"></eq:eqTextBox>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn> 
                                
                                <eq:eqSpecificationColumn HeaderText="Примечание">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="NOTE" runat="server"></eq:eqTextBox>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
            </table>
        </asp:View>
    </asp:MultiView>
</div>
