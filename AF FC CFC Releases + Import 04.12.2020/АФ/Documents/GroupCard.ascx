﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="GroupCard.ascx.vb"
    Inherits="WebApplication.GroupCard" %>
<div class="LogicBlockCaption">
    Название группы
</div>
<table width="100%" class="LogicBlockTable">
    <colgroup>
        <col style="width: 110px;" />
        <col />
    </colgroup>
    <tr>
        <td>
            Название
        </td>
        <td>
            <eq:eqTextBox ID="GroupName" runat="server" Width="100%" />
        </td>
    </tr>
</table>
<div class="LogicBlockCaption">
    Состав группы
</div>
<eq:eqSpecification ID="eqGroupMembers" runat="server" Width="100%">
    <eq:eqSpecificationColumn Width="120px" HeaderText="Тип">
        <ItemTemplate>
            <asp:DropDownList ID="Type" runat="server" Width="100%" OnSelectedIndexChanged="RefreshSpecificationRow"
                AutoPostBack="True">
                <asp:ListItem />
                <asp:ListItem Value="U" Text="Учетная запись" />
                <asp:ListItem Value="G" Text="Группа" />
            </asp:DropDownList>
        </ItemTemplate>
    </eq:eqSpecificationColumn>
    <eq:eqSpecificationColumn HeaderText="Группа или пользователь">
        <ItemTemplate>
            <eq:eqDropDownList ID="UserOrGroupID" UniqueGroupName="UserOrGroupID" runat="server" Width="100%" />
        </ItemTemplate>
    </eq:eqSpecificationColumn>
</eq:eqSpecification>
<div class="LogicBlockCaption">
    Роли группы
</div>
<eq:eqSpecification ID="eqGroupsRoles" runat="server" Width="100%">
    <eq:eqSpecificationColumn HeaderText="Роль">
        <ItemTemplate>
            <eq:eqDropDownList ID="RoleID" UniqueGroupName="RoleID" runat="server" Width="100%" />
        </ItemTemplate>
    </eq:eqSpecificationColumn>
</eq:eqSpecification>
