﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="STORAGE_MEDIUM_CL.ascx.vb"
    Inherits="WebApplication.STORAGE_MEDIUM_CL" %>
<table style="width: 700px;">
    <colgroup>
        <col width="150px" />
        <col />
    </colgroup>
    <tr>
        <td>
            Верхний уровень
        </td>
        <td>
            <eq2:eqDocument ID="ISN_HIGH_STORAGE_MEDIUM" KeyName="ISN_STORAGE_MEDIUM" runat="server"
                DocTypeURL="STORAGE_MEDIUM_CL.ascx" ListDocMode="NewWindowAndSelect" LoadDocMode="NewWindowAndSelect"
                ShowingFields="NAME" ShowingFieldsFormat="{0}" ValidateHierarchy="true" />
            <asp:LinkButton ID="clearISN_HIGH_STORAGE_MEDIUM" runat="server" Text="Удалить" ToolTip="Сделать корневым элементом" />
        </td>
    </tr>
    <tr>
        <td>
            Наименование
        </td>
        <td>
            <eq:eqTextBox ID="NAME" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            Код:
        </td>
        <td>
            <eq:eqTextBox ID="CODE" runat="server" />
        </td>
    </tr>
    <tr>
        <td valign="top">
            Примечание:
        </td>
        <td>
            <eq:eqTextBox ID="NOTE" runat="server" TextMode="MultiLine" Rows="4" />
        </td>
    </tr>
</table>
