﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="DocStructure.ascx.vb"
    Inherits="WebApplication.DocStructure" %>
<%@ Register Src="../ContainerMini.ascx" TagName="ContainerMini" TagPrefix="cm" %>
<div class="LogicBlockCaption">
    Поле документа
</div>
<table width="100%" class="LogicBlockTable">
    <colgroup>
        <col style="width: 120px;" />
        <col />
    </colgroup>
    <tr>
        <td>
            Документ
        </td>
        <td>
            <cm:ContainerMini ID="DocTypeID" runat="server" DocTypeURL="DocType.ascx" LoadDocMode="NewWindow"
                ShowingFields="DocType" ShowingFieldsFormat="{0}" />
        </td>
    </tr>
    <tr>
        <td>
            Название
        </td>
        <td>
            <eq:eqTextBox ID="FieldNameDisplay" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            Описание
        </td>
        <td>
            <eq:eqTextBox ID="FieldDescription" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            Таблица
        </td>
        <td>
            <eq:eqTextBox ID="TableName" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            Поле таблицы
        </td>
        <td>
            <eq:eqTextBox ID="FieldName" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            RangeForm
        </td>
        <td>
            <asp:RadioButtonList ID="RangeForm" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                <asp:ListItem Text="Header" Value="HEADER" />
                <asp:ListItem Text="Specification" Value="SPECIFICATION" />
            </asp:RadioButtonList>
        </td>
    </tr>
    <tr>
        <td>
            Required
        </td>
        <td>
            <asp:CheckBox ID="Required" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            ReadOnly
        </td>
        <td>
            <asp:CheckBox ID="ReadOnly" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            AuditEnabled
        </td>
        <td>
            <asp:CheckBox ID="AuditEnabled" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            ValuesScript
        </td>
        <td>
            <small>SELECT Key, Text [, Text, ...] ...</small>
            <eq:eqTextBox ID="ValuesScript" runat="server" TextMode="MultiLine" Rows="5" />
        </td>
    </tr>
    <tr>
        <td>
            SaveScript
        </td>
        <td>
            <small>Use parameters like this: @ID, @OwnerID, @FieldName...</small>
            <eq:eqTextBox ID="SaveScript" runat="server" TextMode="MultiLine" Rows="5" />
        </td>
    </tr>
    <tr>
        <td>
            FieldFormat
        </td>
        <td>
            <eq:eqTextBox ID="FieldFormat" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            FieldFormat Help
        </td>
        <td>
            <a href="http://msdn.microsoft.com/en-us/library/97x6twsz.aspx" target="_blank">Date
                and Time Format Strings</a> <a href="http://msdn.microsoft.com/en-us/library/427bttx3.aspx"
                    target="_blank">Numeric Format Strings</a>
        </td>
    </tr>
</table>
