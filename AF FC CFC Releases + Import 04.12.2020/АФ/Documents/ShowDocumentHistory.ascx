﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ShowDocumentHistory.ascx.vb"
    Inherits="WebApplication.ShowDocumentHistory" %>
<asp:Panel ID="HeaderPanel" runat="server">
</asp:Panel>
<asp:Panel ID="DocTypeIDPanel" runat="server" Visible="false">
    <table>
        <tr>
            <td>
                Выберите объект:
            </td>
            <td>
                <asp:DropDownList ID="ddlDocTypeID" runat="server">
                </asp:DropDownList>
            </td>
            <td>
                <asp:Label ID="LabelDoc" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Panel ID="ParameretsPanel" runat="server">
    <table>
        <tr>
            <td>
                <asp:Label ID="lblStartDate" runat="server" Text="<%$ Resources:eqResources, CAP_DateFrom %>" />
            </td>
            <td>
                <asp:Label ID="lblFinishDate" runat="server" Text="<%$ Resources:eqResources, CAP_DateTo %>" />
            </td>
        </tr>
        <tr>
            <td>
                <eq:eqTextBox ID="StartDate" runat="server" Width="77px" />
                <aj:CalendarExtender ID="CExtSpStartDate" runat="server" TargetControlID="StartDate" />
            </td>
            <td>
                <eq:eqTextBox ID="FinishDate" runat="server" Width="77px" />
                <aj:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="FinishDate" />
            </td>
        </tr>
    </table>
    <asp:Panel ID="ChooseUserAndFieldsPanel" runat="server" Visible="false">
        <table>
            <tr>
                <td>
                    <asp:Label ID="lblAuthor" runat="server" Text="<%$ Resources:eqResources, CAP_SelectAuthorOfChanges %>" />
                </td>
                <td>
                    <asp:DropDownList ID="ddlAuthor" runat="server" />
                </td>
                <td>
                    <asp:Label ID="lblField" runat="server" Text="<%$ Resources:eqResources, CAP_Field %>" />
                </td>
                <td>
                    <asp:DropDownList ID="ddlField" runat="server" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Button runat="server" ID="btnGenerateReport" Text="<%$ Resources:eqResources, CAP_GenerateReport %>" />
</asp:Panel>
<aj:TabContainer ID="Tab" runat="server">
    <aj:TabPanel ID="StatusHistoryTab" runat="server" HeaderText="<%$ Resources:eqResources, CAP_HistoryByDocStatuses %>">
    <ContentTemplate>
        <asp:Panel ID="StatusHistoryPanel" runat="server">
            <p>
                <asp:Localize ID="LocalizeHistotyByDocStatuses" runat="server" Text="<%$ Resources:eqResources, CAP_HistoryByDocStatuses %>" />
            </p>
            <asp:GridView ID="GridViewStatusHistory" runat="server" AutoGenerateColumns="False"
                AllowPaging="true" AllowSorting="true" PageSize="20">
                <Columns>
                    <asp:BoundField DataField="CreationDateTime" HeaderText="<%$ Resources:eqResources, CAP_DateOfChanges %>" />
                    <asp:BoundField DataField="Author" HeaderText="<%$ Resources:eqResources, CAP_AuthorOfChanges %>" />
                    <asp:BoundField DataField="StatusName" HeaderText="<%$ Resources:eqResources, CAP_Status %>" />
                </Columns>
            </asp:GridView>
        </asp:Panel>
        </ContentTemplate>
    </aj:TabPanel>
    <aj:TabPanel ID="FieldsHistoryTab" runat="server" HeaderText="<%$ Resources:eqResources, CAP_HistoryByDocFields %>">
    <ContentTemplate>
        <asp:Panel ID="FieldsHistoryPanel" runat="server">
            <p>
                <asp:Localize ID="LocalizeHistoryByDocFields" runat="server" Text="<%$ Resources:eqResources, CAP_HistoryByDocFields %>" />
            </p>
            <asp:GridView ID="GridViewFieldHistory" runat="server" AutoGenerateColumns="false"
                AllowPaging="true" AllowSorting="true" PageSize="20">
                <Columns>
                    <asp:BoundField DataField="CreationDateTime" HeaderText="<%$ Resources:eqResources, CAP_DateOfChanges %>" />
                    <asp:BoundField DataField="Author" HeaderText="<%$ Resources:eqResources, CAP_AuthorOfChanges %>" />
                    <asp:BoundField DataField="FieldNameDisplay" HeaderText="<%$ Resources:eqResources, CAP_Field %>" />
                    <asp:BoundField DataField="PrevValue" HeaderText="<%$ Resources:eqResources, CAP_PreviousValue %>" />
                    <asp:BoundField DataField="Value" HeaderText="<%$ Resources:eqResources, CAP_CurrentValue %>" />
                </Columns>
            </asp:GridView>
        </asp:Panel>
        </ContentTemplate>
    </aj:TabPanel>
</aj:TabContainer>
<%--<asp:Panel ID="ActivityHistoryPanel" runat="server">
<p>История активности работы с документом</p>


</asp:Panel>--%>
