﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="AuthorizedDep.ascx.vb"
    Inherits="WebApplication.AuthorizedDep" %>
<eq:eqPanel ID="CapPanel" runat="server">
    Информация об уполномоченном органе исполнительной власти субъекта РФ в области архивного дела:
</eq:eqPanel>
<table width =800px>
<col width =150px />
    <tr>
        <td>
            Сокращенное наименование
        </td>
        <td>
            <eq:eqTextBox runat="server" ID="ShortName" Width="100%" />
        </td>
    </tr>
        <tr>
        <td>
            Полное наименование
        </td>
        <td>
            <eq:eqTextBox runat="server" ID="FullName" Width="100%" Wrap=true TextMode=MultiLine Height=60px />
        </td>
    </tr>
        <tr>
        <td>
            Адрес и контактная информация
        </td>
        <td>
            <eq:eqTextBox runat="server" ID="Address" Width="100%" Wrap=true TextMode=MultiLine Height=60px />
        </td>
    </tr>
         <tr>
        <td>
            Федеральный округ и субъект Российской Федерации
        </td>
        <td>
            <eq:eqTextBox runat="server" ID="District" Width="100%" Wrap=true TextMode=MultiLine Height=40px />
        </td>
    </tr>   
</table>
