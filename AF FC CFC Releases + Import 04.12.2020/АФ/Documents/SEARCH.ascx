﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="SEARCH.ascx.vb" Inherits="WebApplication.SEARCH" %>
<asp:Panel runat="server" DefaultButton="ButtonSearch">
    <table style="width: 700px;">
        <tr>
            <td style="width: 50px;">
                Поиск:
            </td>
            <td style="width: 100%;">
                <eq:eqTextBox ID="TextBoxSearchPattern" runat="server" />
            </td>
            <td>
                <asp:Button ID="ButtonSearch" runat="server" Text="Поиск" />
            </td>
        </tr>
    </table>
    <table style="width: 700px;">
        <tr>
            <td>
                <asp:Label ID="LabelMessage" runat="server" />
            </td>
        </tr>
    </table>
    <asp:SqlDataSource ID="DataSourceSearch" runat="server" SelectCommand="SELECT * FROM fn_Search(@SearchPattern, null) ORDER BY Kind, Code">
        <SelectParameters>
            <asp:Parameter Name="SearchPattern" Type="String" DefaultValue="@@@@@@@@@@@@@@" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:ListView ID="ListViewResults" runat="server" DataSourceID="DataSourceSearch">
        <LayoutTemplate>
            <table style="width: 700px;">
                <tr>
                    <td>
                        <asp:DataPager runat="server" PageSize="20">
                            <Fields>
                                <asp:NextPreviousPagerField ShowFirstPageButton="true" ShowNextPageButton="false" FirstPageText="<<" PreviousPageText="<" />
                                <asp:NumericPagerField ButtonCount="10" />
                                <asp:NextPreviousPagerField ShowLastPageButton="true" ShowPreviousPageButton="false" LastPageText=">>" NextPageText=">" />
                            </Fields>
                        </asp:DataPager>
                    </td>
                </tr>            
                <tr>
                    <td>
                        <div id="itemPlaceholder" runat="server"></div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:DataPager runat="server" PageSize="20">
                            <Fields>
                                <asp:NextPreviousPagerField ShowFirstPageButton="true" ShowNextPageButton="false" FirstPageText="<<" PreviousPageText="<" />
                                <asp:NumericPagerField ButtonCount="10" />
                                <asp:NextPreviousPagerField ShowLastPageButton="true" ShowPreviousPageButton="false" LastPageText=">>" NextPageText=">" />
                            </Fields>
                        </asp:DataPager>
                    </td>
                </tr>
            </table>
        </LayoutTemplate>
        <ItemTemplate>
            <div>
                <p>
                    <h4>
                        <a href='<%#evalHref(Container) %>'><%#evalTitle(Container)%></a>
                    </h4>
                </p>
                <p>
                    <%#evalText(Container)%>
                </p>
                <p style="color: Gray;">
                    <font color="gray">
                        <small>
                            <a style="text-decoration: none;" href='<%#evalUpperHref(Container) %>' target="_blank"><%#Eval("Map")%></a>
                        </small>                       
                    </font>
                </p>
                <p style="color: Gray;">
                    <font color="gray">
                        <small>
                            <%#Eval("SubjectName")%>
                        </small>
                    </font>
                </p>
            </div>
        </ItemTemplate>
    </asp:ListView>
</asp:Panel>
