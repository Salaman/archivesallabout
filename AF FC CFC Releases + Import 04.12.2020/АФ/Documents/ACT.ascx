﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ACT.ascx.vb" Inherits="WebApplication.ACT" %>
<table style="width: 97%; margin-bottom: 4px;">
    <tr>
        <td>
            <eq2:ArchiveCrumbs ID="Crumbs" runat="server" Level="ACT" />
        </td>
    </tr>
</table>
<div style="border: solid 1px black; padding: 5px;">
    <table style="width: 700px;">
        <colgroup>
            <col align="right" style="width: 150px;" />
            <col align="left" />
        </colgroup>
        <tr>
            <td>
                Номер*:
            </td>
            <td>
                <table>
                    <colgroup>
                        <col />
                        <col />
                        <col />
                    </colgroup>
                    <tr>
                        <td>
                            <eq:eqTextBox ID="ACT_NUM" runat="server" Width="200px" />
                        </td>
                        <td>
                            Дата*:
                        </td>
                        <td>
                            <eq:eqTextBox ID="ACT_DATE" runat="server" Width="100px" />
                            <aj:MaskedEditExtender ID = "ACT_DATE_MASK" TargetControlID = "ACT_DATE" runat="server" MaskType="Date" Mask="99/99/9999" ClearTextOnInvalid="true"  />
                            <aj:CalendarExtender runat="server" TargetControlID="ACT_DATE" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top">
                Название:
            </td>
            <td>
                <eq:eqTextBox ID="ACT_NAME" runat="server" TextMode="MultiLine" Rows="4" />
            </td>
        </tr>
        <tr>
            <td>
                Тип акта*:
            </td>
            <td>
                <%--СПРАВОЧНИК--%>
                <eq2:eqDocument ID="ISN_ACT_TYPE" runat="server" DocTypeURL="ACT_TYPE_CL.ascx" ListDocMode="NewWindowAndSelect"
                    LoadDocMode="ListDocAndSelect" ShowingFields="NAME" ShowingFieldsFormat="{0}" />
            </td>
        </tr>
        <tr>
            <td>
                Движение*:
            </td>
            <td>
                <table width="100%">
                    <colgroup>
                        <col style="width: 20%;" />
                        <col align="right" style="width: 20%;" />
                        <col style="width: 20%;" />
                        <col align="right" style="width: 20%;" />
                        <col style="width: 20%;" />
                    </colgroup>
                    <tr>
                        <td>
                            <asp:DropDownList ID="MOVEMENT_FLAG" runat="server" Width="100%">
                                <asp:ListItem Text="" Value="" />
                                <asp:ListItem Text="поступление" Value="0" />
                                <asp:ListItem Text="без движения" Value="1" />
                                <asp:ListItem Text="выбытие" Value="2" />
                            </asp:DropDownList>
                        </td>
                        <td>
                            Объект*:
                        </td>
                        <td>
                            <asp:DropDownList ID="ACT_OBJ" runat="server" Width="100%">
                                <asp:ListItem Text="" Value="" />
                                <asp:ListItem Text="фонд" Value="701" />
                                <asp:ListItem Text="опись" Value="702" />
                                <asp:ListItem Text="ед. хранения" Value="703" />
                                <asp:ListItem Text="ед. учета" Value="704" />
                                <asp:ListItem Text="россыпь" Value="707" />
                            </asp:DropDownList>
                            <%--значения из dbo.DIC_OBJECT--%>
                        </td>
                        <td>
                            Объем ед. хр.*:
                        </td>
                        <td>
                            <eq:eqTextBox ID="UNIT_COUNT" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top">
                Работы выполняли:
            </td>
            <td>
                <eq:eqTextBox ID="ACT_PERSONS" runat="server" TextMode="MultiLine" Rows="4" />
            </td>
        </tr>
        <tr>
            <td valign="top">
                Проведенная работа:
            </td>
            <td>
                <eq:eqTextBox ID="ACT_WORK" runat="server" TextMode="MultiLine" Rows="4" />
            </td>
        </tr>
        <tr>
            <td valign="top">
                Примечание:
            </td>
            <td>
                <eq:eqTextBox ID="NOTE" runat="server" TextMode="MultiLine" Rows="6" />
            </td>
        </tr>
    </table>
    <table style="width: 700px;">
        <tr>
            <td>
                <fieldset>
                    <legend>Файлы</legend>
                    <eq:eqSpecification ID="vREF_FILE_ACT" runat="server" Width="100%" AllowInsert="false">
                        <eq:eqSpecificationColumn HeaderText="Описание">
                            <ItemTemplate>
                                <asp:TextBox ID="NAME" runat="server" Width="99%" />
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                        <eq:eqSpecificationColumn HeaderText="Файл">
                            <ItemTemplate>
                                <eq2:RefFile ID="ID" runat="server" Mode="FileSystem" OnRequesting="RefFile_Requesting" />
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                    </eq:eqSpecification>
                    <table>
                        <tr>
                            <td>
                                Добавить файл:
                            </td>
                            <td>
                                <eq2:RefFileUpload ID="RefFileUploadFile" runat="server" Mode="FileSystem" />
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
        <tr runat="server" id="HolderSummary" visible="false">
            <td>
                <fieldset>
                    <legend>Количественные характеристики </legend>
                    <div style="margin: 5px 0;">
                        Общее количество единиц хранения:
                        <asp:Label runat="server" ID="LabelSummary" />
                    </div>
                    <eq:eqSpecification ID="vREF_ACT_FOR_ACT" runat="server" Width="100%" AllowInsert="false" AllowDelete="false" AllowReorder="false" ShowNumering="false">
                        <eq:eqSpecificationColumn HeaderText="Объект учета">
                            <ItemTemplate>
                                <eq2:eqDocument ID="Summary" runat="server" LoadDocMode="NewWindow" />
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                        <eq:eqSpecificationColumn HeaderText="Количество ед. хранения">
                            <ItemTemplate>
                                <asp:TextBox ID="UNIT_COUNT" runat="server" Width="120" />
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                    </eq:eqSpecification>
                </fieldset>
            </td>
        </tr>
    </table>
</div>
