﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="DocListDoc.ascx.vb"
    Inherits="WebApplication.DocListDoc" %>
<%@ Register Src="../ContainerMini.ascx" TagName="ContainerMini" TagPrefix="cm" %>
<div class="LogicBlockCaption">
    Поле списка
</div>
<table width="100%" class="LogicBlockTable">
    <colgroup>
        <col style="width: 120px;" />
        <col />
    </colgroup>
    <tr>
        <td>
            Документ
        </td>
        <td>
            <cm:ContainerMini ID="DocTypeID" runat="server" DocTypeURL="DocType.ascx" LoadDocMode="NewWindow"
                ShowingFields="DocType" ShowingFieldsFormat="{0}" />
        </td>
    </tr>
    <tr>
        <td>
            Поле документа
        </td>
        <td>
            <cm:ContainerMini ID="FieldID" runat="server" DocTypeURL="DocStructure.ascx" LoadDocMode="NewWindow"
                ShowingFields="FieldName" ShowingFieldsFormat="{0}" />
        </td>
    </tr>
    <tr>
        <td>
            Описание
        </td>
        <td>
            <eq:eqTextBox ID="FieldDesc" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            FieldFormat
        </td>
        <td>
            <eq:eqTextBox ID="FieldFormat" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            FieldFormat Help
        </td>
        <td>
            <a href="http://msdn.microsoft.com/en-us/library/97x6twsz.aspx" target="_blank">Date
                and Time Format Strings</a> <a href="http://msdn.microsoft.com/en-us/library/427bttx3.aspx"
                    target="_blank">Numeric Format Strings</a>
        </td>
    </tr>
    <tr>
        <td>
            SortOrder
        </td>
        <td>
            <eq:eqTextBox ID="SortOrder" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            Editable
        </td>
        <td>
            <asp:CheckBox ID="Edit" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            ValuesScript
        </td>
        <td>
            <eq:eqTextBox ID="ValuesScript" runat="server" TextMode="MultiLine" Rows="5" />
        </td>
    </tr>
</table>
<div class="LogicBlockCaption">
    Ссылка на документ
</div>
<table width="100%" class="LogicBlockTable">
    <colgroup>
        <col style="width: 120px;" />
        <col />
    </colgroup>
    <tr>
        <td>
            DocTypeURL
        </td>
        <td>
            <eq:eqTextBox ID="DocTypeURLControl" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            ShowingFields
        </td>
        <td>
            <eq:eqTextBox ID="ShowingFields" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            ShowingFieldsFormat
        </td>
        <td>
            <eq:eqTextBox ID="ShowingFieldsFormat" runat="server" />
        </td>
    </tr>
</table>
<div class="LogicBlockCaption">
    Таблица подстановки
</div>
<table width="100%" class="LogicBlockTable">
    <colgroup>
        <col style="width: 120px;" />
        <col />
    </colgroup>
    <tr>
        <td>
            SourceTable
        </td>
        <td>
            <small>Type "ValuesScript" to use SELECT from ValuesScript field.</small>
            <eq:eqTextBox ID="SourceTable" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            KeyField
        </td>
        <td>
            <eq:eqTextBox ID="KeyField" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            DisplayField
        </td>
        <td>
            <eq:eqTextBox ID="DisplayField" runat="server" />
        </td>
    </tr>
</table>
