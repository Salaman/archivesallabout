﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Users.ascx.vb" Inherits="WebApplication.Users" %>
<div class="LogicBlockCaption">
    Учетная запись
    <asp:Label ID="DisplayName" runat="server" Font-Bold="True" />
    <asp:CheckBox ID="BUILD_IN_ACCOUNT" runat="server" Text="Встороенная" Enabled="false"
        Visible="false" />
</div>
<asp:Panel ID="LoginPassWordPanel" runat ="server" >
<div class="LogicBlockCaption">
    Логин и пароль
</div>
<table width="100%" class="LogicBlockTable">
    <colgroup>
        <col style="width: 110px;" />
        <col />
    </colgroup>
    <tr>
        <td>
            Доступ разрешен
        </td>
        <td>
            <asp:CheckBox ID="AccessGranted" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            Логин
        </td>
        <td>
            <eq:eqTextBox ID="Login" runat="server" MaxLength="100" Width="100%" />
        </td>
    </tr>
    <tr>
        <td>
            Пароль
        </td>
        <td>
            <eq:eqTextBox ID="Pass" runat="server" MaxLength="50" Width="100%" />
        </td>
    </tr>
</table>
</asp:Panel>
<div class="LogicBlockCaption">
    Персональная информация
</div>
<table width="100%" class="LogicBlockTable">
    <colgroup>
        <col style="width: 110px;" />
        <col />
        <col style="width: 110px;" />
        <col />
    </colgroup>
    <tr>
        <td>
            Фамилия
        </td>
        <td>
            <eq:eqTextBox ID="SurName" runat="server" MaxLength="255" Width="100%" />
        </td>
        <td>
            Имя
        </td>
        <td>
            <eq:eqTextBox ID="FirstName" runat="server" MaxLength="255" Width="100%" />
        </td>
    </tr>
    <tr>
        <td>
            Отчество
        </td>
        <td colspan="3">
            <eq:eqTextBox ID="Patronymic" runat="server" MaxLength="255" Width="100%" />
        </td>
    </tr>
    <tr>
        <td>
            Телефон
        </td>
        <td>
            <eq:eqTextBox ID="Phone" runat="server" MaxLength="50" Width="100%" />
        </td>
        <td>
            E-mail
        </td>
        <td>
            <eq:eqTextBox ID="Email" runat="server" Width="100%" />
        </td>
    </tr>
    <tr>
        <td>
            Примечания
        </td>
        <td colspan="3">
            <eq:eqTextBox ID="Description" runat="server" Width="100%" Rows="3" TextMode="MultiLine" />
        </td>
    </tr>
    <tr>
        <td>
            Подразделение
        </td>
        <td>
            <eq:eqTextBox ID="Department" runat="server" MaxLength="255" Width="100%" />
        </td>
        <td>
            Должность
        </td>
        <td>
            <eq:eqTextBox ID="Position" runat="server" MaxLength="500" Width="100%" />
        </td>
    </tr>
    <tr>
        <td>
            Руководитель
        </td>
        <td>
            <eq:eqTextBox ID="Supervisor" runat="server" MaxLength="255" Width="100%" />
        </td>
        <td>
            Номер комнаты
        </td>
        <td>
            <eq:eqTextBox ID="Room_Number" runat="server" MaxLength="255" Width="100%" />
        </td>
    </tr>
</table>
<div class="LogicBlockCaption">
    Внешний вид
</div>
<table width="100%" class="LogicBlockTable">
    <colgroup>
        <col style="width: 110px;" />
        <col />
    </colgroup>
    <tr>
        <td>
            Тема
        </td>
        <td>
            <asp:DropDownList ID="UserTheme" runat="server" />
        </td>
    </tr>
</table>
<asp:Panel ID="eqUsersGroupSecPanel" runat="server">
    <p>
        Группы в которые входит пользователь</p>
    <eq:eqSpecification ID="eqUsersGroupSec" runat="server" Width="100%" AllowReorder="false">
        <eq:eqSpecificationColumn HeaderText="Группа">
            <ItemTemplate>
                <asp:DropDownList ID="GroupID" runat="server" Width="100%" CssClass="txtfld2">
                </asp:DropDownList>
            </ItemTemplate>
        </eq:eqSpecificationColumn>
    </eq:eqSpecification>
</asp:Panel>
<asp:Panel ID="eqUsersRolwsSecPanel" runat="server" Enabled="true">
    <p>
        Права (роли) пользователя</p>
    <eq:eqSpecification ID="eqUsersRolwsSec" runat="server" Width="100%" AllowReorder="false"
        AllowDelete="false" AllowInsert="false">
<%--        <eq:eqSpecificationColumn HeaderText="Роль">
            <ItemTemplate>
                <eq:eqtextbox ID="RoleName" runat="server" Width="100%" CssClass="txtfld2" />
            </ItemTemplate>
        </eq:eqSpecificationColumn>--%>
                <eq:eqSpecificationColumn HeaderText="Карточка роли">
            <ItemTemplate>
               <eq:eqDocument ID="RoleID" runat ="server" DocTypeURL ="RoleCard.ascx"
               ShowingFields="RoleName" ShowingFieldsFormat="{0}" ListDocMode="None" LoadDocMode="NewWindow" CallbackMode="false" />
            </ItemTemplate>
        </eq:eqSpecificationColumn>
                        <eq:eqSpecificationColumn HeaderText="Карточка роли (области)">
            <ItemTemplate>
               <eq:eqDocument ID="RoleID1" runat ="server" DocTypeURL ="eqRolesAreasList.ascx"
               ShowingFields="RoleName" ShowingFieldsFormat="{0}" ListDocMode="None" LoadDocMode="NewWindow" CallbackMode="false" />
            </ItemTemplate>
        </eq:eqSpecificationColumn>
    </eq:eqSpecification>
</asp:Panel>