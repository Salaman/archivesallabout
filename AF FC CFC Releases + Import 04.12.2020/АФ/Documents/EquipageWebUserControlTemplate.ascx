﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="EquipageWebUserControlTemplate.ascx.vb"
    Inherits="WebApplication.EquipageWebUserControlTemplate" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode="Inline">
    <ContentTemplate>
        <eq:eqCollapsiblePanel ID="HeaderPanel" runat="server" Name="Header">
            <HeaderPanel>
                Шапка шапки
            </HeaderPanel>
            <ContentPanel>
                Шапка
            </ContentPanel>
        </eq:eqCollapsiblePanel>
        <table>
            <tr>
                <td>
                    Номер документа
                </td>
                <td>
                    <eq:eqTextBox ID="DocNum" runat="server"></eq:eqTextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Пометка
                </td>
                <td>
                    <asp:CheckBox ID="Flag" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    Комментарий
                </td>
                <td>
                    <eq:eqTextBox ID="Comment" runat="server" Width="250px" />
                </td>
            </tr>
            <tr>
                <td>
                    Время
                </td>
                <td>
                    <eq:eqTimeEdit ID="MyTime" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    Дата
                </td>
                <td>
                    <eq:eqTextBox ID="MyDate" runat="server" />
                    <aj:CalendarExtender ID="CExt" runat="server" TargetControlID="MyDate" />
                </td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>
<asp:UpdatePanel ID="UpdatePanel2" runat="server">
    <ContentTemplate>
        <eq:eqSpecification ID="vEquipageWebUserControlTemplateSpec" runat="server" ShowHeader="true"
            ShowNumering="true" AllowDelete="true" AllowInsert="true" AllowReorder="true">
            <eq:eqSpecificationColumn Name="Flag" HeaderText="Флажек">
                <ItemTemplate>
                    <asp:Label ID="qwe" runat="server">asdsajjd</asp:Label>
                    <asp:CheckBox ID="Flag" runat="server" />
                </ItemTemplate>
            </eq:eqSpecificationColumn>
            <eq:eqSpecificationColumn Name="Comment" HeaderText="Комментарий">
                <ItemTemplate>
                    <eq:eqTextBox ID="Comment" runat="server" Width="250px" />
                </ItemTemplate>
            </eq:eqSpecificationColumn>
            <eq:eqSpecificationColumn Name="MyTime" HeaderText="Время">
                <ItemTemplate>
                    <eq:eqTimeEdit ID="MyTime" runat="server" Width="250px" />
                </ItemTemplate>
            </eq:eqSpecificationColumn>
            <eq:eqSpecificationColumn Name="MyDate" HeaderText="Дата">
                <ItemTemplate>
                    <eq:eqTextBox ID="MyDate" runat="server" Width="250px" />
                    <aj:CalendarExtender ID="CExtSp" runat="server" TargetControlID="MyDate" />
                </ItemTemplate>
            </eq:eqSpecificationColumn>
        </eq:eqSpecification>
    </ContentTemplate>
</asp:UpdatePanel>
<table>
</table>
