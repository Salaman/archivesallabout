﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="DOC_KIND_CL.ascx.vb"
    Inherits="WebApplication.DOC_KIND_CL" %>
<table style="width: 700px;">
    <colgroup>
        <col width="150px" />
        <col />
    </colgroup>
    <tr>
        <td>
            Верхний уровень
        </td>
        <td> 
            <eq2:eqDocument ID="ISN_HIGH_DOC_KIND" KeyName="ISN_DOC_KIND" runat="server" DocTypeURL="DOC_KIND_CL.ascx"
                ListDocMode="NewWindowAndSelect" LoadDocMode="NewWindowAndSelect" ShowingFields="NAME"
                ShowingFieldsFormat="{0}" ValidateHierarchy="true" />
            <asp:LinkButton ID="clearISN_HIGH_DOC_KIND" runat="server" Text="Удалить" ToolTip="Сделать корневым элементом" />
        </td>
    </tr>
    <tr>
        <td>
            Наименование
        </td>
        <td>
            <eq:eqTextBox ID="NAME" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            Код:
        </td>
        <td>
            <eq:eqTextBox ID="CODE" runat="server" />
        </td>
    </tr>
    <tr>
        <td valign="top">
            Примечание:
        </td>
        <td>
            <eq:eqTextBox ID="NOTE" runat="server" TextMode="MultiLine" Rows="4" />
        </td>
    </tr>
</table>
