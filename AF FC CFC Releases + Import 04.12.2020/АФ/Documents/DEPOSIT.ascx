﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="DEPOSIT.ascx.vb" Inherits="WebApplication.DEPOSIT" %>
<table style="width: 97%; margin-bottom: 4px;">
    <tr>
        <td>
            <eq2:ArchiveCrumbs ID="Crumbs" runat="server" Level="FUND" />
        </td>
        <td style="text-align: right;">
        </td>
    </tr>
</table>
<div style="border: solid 1px black; padding: 5px;">
    <table style="width: 700px;">
        <colgroup>
            <col style="width: 100px;" />
            <col />
        </colgroup>
        <tr>
            <td>
                Номер*:
            </td>
            <td>
                <eq:eqTextBox ID="DEPOSIT_NUM" runat="server" Width="200px" />
            </td>
        </tr>
        <tr>
            <td>
                Название:
            </td>
            <td>
                <eq:eqTextBox ID="DEPOSIT_NAME" runat="server" TextMode="MultiLine" Rows="4" />
            </td>
        </tr>
    </table>
    <table style="width: 700px;">
        <colgroup>
            <col style="width: 100px;" />
            <col />
            <col style="width: 100px;" />
            <col />
        </colgroup>
        <tr>
            <td>
                Состав:
            </td>
            <td>
                <eq:eqTextBox ID="CONSISTS" runat="server" TextMode="MultiLine" Rows="4" />
            </td>
            <td>
                Примечание:
            </td>
            <td>
                <eq:eqTextBox ID="NOTE" runat="server" TextMode="MultiLine" Rows="4" />
            </td>
        </tr>
    </table>
    <fieldset>
        <table>
            <colgroup>
                <col align="right" style="width: 14%;" />
                <col align="right" style="width: 11%;" />
                <col align="right" style="width: 14%;" />
                <col align="right" style="width: 11%;" />
                <col align="right" style="width: 14%;" />
                <col align="right" style="width: 11%;" />
                <col align="right" style="width: 14%;" />
                <col align="right" />
            </colgroup>
            <tr>
                <td>
                    Кол-во ед. хр.:
                </td>
                <td>
                    <eq:eqTextBox ID="UNIT_COUNT" runat="server" />
                </td>
                <td>
                    Кол-во документов:
                </td>
                <td>
                    <eq:eqTextBox ID="DOC_COUNT" runat="server" />
                </td>
                <td>
                    Кол-во документов личного происхождения:
                </td>
                <td>
                    <eq:eqTextBox ID="PERSONAL_DOC_COUNT" runat="server" />
                </td>
                <td>
                    Кол-во листов:
                </td>
                <td>
                    <eq:eqTextBox ID="PAGE_COUNT" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    Обработано:
                </td>
                <td>
                    <eq:eqTextBox ID="PROCESSED" runat="server" />
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <eq:eqTextBox ID="PROCESSED_DOC" runat="server" />
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <eq:eqTextBox ID="PROCESSED_PERSONAL_DOC" runat="server" />
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <eq:eqTextBox ID="PROCESSED_PAGE" runat="server" />
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td>
                    <fieldset>
                        <legend>Документы на бумажной основе </legend>
                        <eq:eqSpecification ID="vDEPOSIT_DOC_TYPE_P" runat="server" ShowNumering="false"
                            AllowDelete="false" AllowInsert="false" AllowReorder="false">
                            <eq:eqSpecificationColumn HeaderText="">
                                <ItemTemplate>
                                    <asp:Label ID="ISN_DOC_TYPE" runat="server" />
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                            <eq:eqSpecificationColumn HeaderText="Всего ед. хранения">
                                <ItemTemplate>
                                    <eq:eqTextBox ID="UNIT_COUNT" runat="server" />
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                            <eq:eqSpecificationColumn HeaderText="Обработано">
                                <ItemTemplate>
                                    <eq:eqTextBox ID="PROCESSED_COUNT" runat="server" />
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                        </eq:eqSpecification>
                    </fieldset>
                </td>
                <td>
                    <fieldset>
                        <legend>Аудиовизуальные документы на трад. носителях </legend>
                        <eq:eqSpecification ID="vDEPOSIT_DOC_TYPE_A" runat="server" ShowNumering="false"
                            AllowDelete="false" AllowInsert="false" AllowReorder="false">
                            <eq:eqSpecificationColumn HeaderText="">
                                <ItemTemplate>
                                    <asp:Label ID="ISN_DOC_TYPE" runat="server" />
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                            <eq:eqSpecificationColumn HeaderText="Всего ед. хранения">
                                <ItemTemplate>
                                    <eq:eqTextBox ID="UNIT_COUNT" runat="server" />
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                            <eq:eqSpecificationColumn HeaderText="Обработано">
                                <ItemTemplate>
                                    <eq:eqTextBox ID="PROCESSED_COUNT" runat="server" />
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                        </eq:eqSpecification>
                    </fieldset>
                </td>
                <td>
                    <fieldset>
                        <legend>Документы на электронных носителях </legend>
                        <eq:eqSpecification ID="vDEPOSIT_DOC_TYPE_E" runat="server" ShowNumering="false"
                            AllowDelete="false" AllowInsert="false" AllowReorder="false">
                            <eq:eqSpecificationColumn HeaderText="">
                                <ItemTemplate>
                                    <asp:Label ID="ISN_DOC_TYPE" runat="server" />
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                            <eq:eqSpecificationColumn HeaderText="Всего ед. хранения">
                                <ItemTemplate>
                                    <eq:eqTextBox ID="UNIT_COUNT" runat="server" />
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                            <eq:eqSpecificationColumn HeaderText="Обработано">
                                <ItemTemplate>
                                    <eq:eqTextBox ID="PROCESSED_COUNT" runat="server" />
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                        </eq:eqSpecification>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <td>
                    <fieldset>
                        <legend>Микроформы</legend>
                        <eq:eqSpecification ID="vDEPOSIT_DOC_TYPE_M" runat="server" ShowNumering="false"
                            AllowDelete="false" AllowInsert="false" AllowReorder="false">
                            <eq:eqSpecificationColumn HeaderText="">
                                <ItemTemplate>
                                    <asp:Label ID="ISN_DOC_TYPE" runat="server" />
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                            <eq:eqSpecificationColumn HeaderText="Всего ед. хранения">
                                <ItemTemplate>
                                    <eq:eqTextBox ID="UNIT_COUNT" runat="server" />
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                            <eq:eqSpecificationColumn HeaderText="Обработано">
                                <ItemTemplate>
                                    <eq:eqTextBox ID="PROCESSED_COUNT" runat="server" />
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                        </eq:eqSpecification>
                    </fieldset>
                </td>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
        </table>
    </fieldset>
    <table style="width: 700px;">
        <tr>
            <td>
                <fieldset>
                    <legend>Акты</legend>
                    <%--СПЕЦИФИКАЦИЯ vREF_ACT--%>
                    <%--физически лежит в dbo.ACT--%>
                    <eq:eqSpecification ID="vREF_ACT_FOR_DEPOSIT" runat="server" ShowNumering="false">
                        <eq:eqSpecificationColumn HeaderText="Акт">
                            <ItemTemplate>
                                <eq2:eqDocument ID="ISN_ACT" runat="server" DocTypeURL="ACT.ascx" ListDocMode="NewWindowAndSelect"
                                    LoadDocMode="NewWindowAndSelect" ShowingFields="ACT_NUM" ShowingFieldsFormat="{0}"
                                    KeyName="ISN_ACT" />
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                        <eq:eqSpecificationColumn HeaderText="Дата">
                            <ItemTemplate>
                                <asp:Label ID="ACT_DATE" runat="server"></asp:Label>
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                        <eq:eqSpecificationColumn HeaderText="Название">
                            <ItemTemplate>
                                <asp:Label ID="ACT_NAME" runat="server"></asp:Label>
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                        <eq:eqSpecificationColumn HeaderText="Тип акта">
                            <ItemTemplate>
                                <asp:Label ID="ACTTYPENAME" runat="server"></asp:Label>
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                        <eq:eqSpecificationColumn HeaderText="Движение">
                            <ItemTemplate>
                                <asp:Label ID="MOVEMENT_FLAG" runat="server"></asp:Label>
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                        <eq:eqSpecificationColumn HeaderText="Проведённая работа">
                            <ItemTemplate>
                                <asp:Label ID="ACT_WORK" runat="server"></asp:Label>
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                        <eq:eqSpecificationColumn HeaderText="Примечание">
                            <ItemTemplate>
                                <asp:Label ID="NOTE" runat="server"></asp:Label>
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                    </eq:eqSpecification>
                </fieldset>
            </td>
        </tr>
    </table>
</div>
