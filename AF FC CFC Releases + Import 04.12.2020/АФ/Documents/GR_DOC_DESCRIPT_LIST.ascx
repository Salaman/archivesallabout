﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="GR_DOC_DESCRIPT_LIST.ascx.vb" Inherits="WebApplication.GR_DOC_DESCRIPT_LIST" %>
<table style="width: 700px;">
    <colgroup>
        <col width="150px" />
        <col />
    </colgroup>
    <tr>
        <td>
            Лист описания
        </td>
        <td>
            <eq:eqTextBox ID="NAME" runat="server" Height="30px" Width="185px"></eq:eqTextBox>
        </td>
        <td>
            Дата документа
        </td>
        <td>
            <eq:eqTextBox ID="DESCRIPT_DATE" runat="server"  Width="100px" />
             <aj:MaskedEditExtender ID = "DESCRIPT_DATE_MASK" TargetControlID = "DESCRIPT_DATE" runat="server" MaskType="Date" Mask="99/99/9999"   />
            <aj:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="DESCRIPT_DATE" />
        </td>
    </tr>
</table>