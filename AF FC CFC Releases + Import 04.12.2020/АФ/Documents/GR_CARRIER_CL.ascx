﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="GR_CARRIER_CL.ascx.vb"
    Inherits="WebApplication.GR_CARRIER_CL" %>
<table style="width: 700px;">
    <colgroup>
        <col width="150px" />
        <col />
    </colgroup>
    <tr>
        <td>
            Верхний уровень
        </td>
        <td>
            <eq3:eqDocument ID="ID_HIGH_CARRIER" KeyName="ID" runat="server" DocTypeURL="GR_CARRIER_CL.ascx"
                ListDocMode="NewWindowAndSelect" LoadDocMode="NewWindowAndSelect" ShowingFields="NAME"
                ShowingFieldsFormat="{0}" ValidateHierarchy="true" />
            <asp:LinkButton ID="clearID_HIGH_CARRIER" runat="server" Text="Удалить" ToolTip="Сделать корневым элементом" />
        </td>
    </tr>
    <tr>
        <td>
            Наименование
        </td>
        <td>
            <eq:eqTextBox ID="NAME" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            Век:
        </td>
        <td>
            <asp:DropDownList ID="ID_CENTURY" runat="server" Width="150px">
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td valign="top">
            Оригинальная запись:
        </td>
        <td>
            <eq:eqTextBox ID="ORIGINAL_RECORD" runat="server" TextMode="MultiLine" Rows="4" />
        </td>
    </tr>
</table>
