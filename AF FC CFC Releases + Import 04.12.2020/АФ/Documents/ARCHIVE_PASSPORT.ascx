﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ARCHIVE_PASSPORT.ascx.vb"
    Inherits="WebApplication.ARCHIVE_PASSPORT" %>
<table style="width: 97%; margin-bottom: 4px;">
    <tr>
        <td>
            <eq2:ArchiveCrumbs ID="Crumbs" runat="server" Level="ARCHIVE" />
        </td>
        <td style="text-align: right;">
        </td>
    </tr>
</table>
<eq:eqMenu ID="MenuTabs" runat="server" Orientation="Horizontal" StaticEnableDefaultPopOutImage="False"
    ItemWrap="false" HorizontalMaxCount="5">
    <Items>
        <asp:MenuItem Text="Состав арх. док." Value="0" Selected="true" />
        <asp:MenuItem Text="Состав СФ и ФП" Value="1" />
        <asp:MenuItem Text="Состав НСА" Value="2" />
        <asp:MenuItem Text="Доп. инф." Value="3" />
    </Items>
    <StaticSelectedStyle CssClass="MenuItemStyleSelected" />
</eq:eqMenu>
<div style="border: solid 1px black; padding: 5px;">
    <asp:MultiView ID="MultiViewTabs" runat="server" ActiveViewIndex="0">
        <%--Состав арх. док.--%>
        <asp:View ID="View2" runat="server">
            <table style="width: 700px;">
                <tr>
                    <td>
                        Состав и объем архивных документов
                    </td>
                </tr>
                <tr>
                    <td>
                        <fieldset>
                            <legend>Документы на бумажных носителях</legend>
                            <%--СПЕЦИФИКАЦИЯ "Документы на бумажных носителях"--%>
                            <eq:eqSpecification ID="vARCHIVE_STATS_P1" runat="server" ShowNumering="false" AllowDelete="false"
                                AllowInsert="false" AllowReorder="false">
                                <eq:eqSpecificationColumn HeaderText="" Width="150">
                                    <ItemTemplate>
                                        <asp:Label ID="ISN_DOC_TYPE" runat="server" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Кол-во фондов">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="FUND_COUNT" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Всего ед. хр.">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_COUNT" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Внесенных в описи">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_INVENTORY" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Секретных">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_SECRET" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Уникальных">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_UNIQUE" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Особо ценных">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_OC" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Принятых по договору на временное хр.">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_DEP" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Всего ед. уч.">
                                    <ItemTemplate>
                                        <asp:Panel runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Внесенных в описи">
                                    <ItemTemplate>
                                        <asp:Panel runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <fieldset>
                            <legend>Аудиовизуальные документы на традиционных носителях</legend>
                            <%--СПЕЦИФИКАЦИЯ "Аудиовизуальные документы на традиционных носителях"--%>
                            <eq:eqSpecification ID="vARCHIVE_STATS_A1" runat="server" ShowNumering="false" AllowDelete="false"
                                AllowInsert="false" AllowReorder="false">
                                <eq:eqSpecificationColumn HeaderText="" Width="150">
                                    <ItemTemplate>
                                        <asp:Label ID="ISN_DOC_TYPE" runat="server" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Кол-во фондов">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="FUND_COUNT" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Всего ед. хр.">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_COUNT" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Внесенных в описи">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_INVENTORY" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Секретных">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_SECRET" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Уникальных">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_UNIQUE" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Особо ценных">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_OC" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Принятых по договору на временное хр.">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_DEP" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Всего ед. уч.">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="REG_UNIT_COUNT" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Внесенных в описи">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="REG_UNIT_INVENTORY" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <fieldset>
                            <legend>Документы на электронных носителях</legend>
                            <%--СПЕЦИФИКАЦИЯ "Документы на электронных носителях"--%>
                            <eq:eqSpecification ID="vARCHIVE_STATS_E1" runat="server" ShowNumering="false" AllowDelete="false"
                                AllowInsert="false" AllowReorder="false">
                                <eq:eqSpecificationColumn HeaderText="" Width="150">
                                    <ItemTemplate>
                                        <asp:Label ID="ISN_DOC_TYPE" runat="server" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Кол-во фондов">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="FUND_COUNT" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Всего ед. хр.">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_COUNT" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Внесенных в описи">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_INVENTORY" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Секретных">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_SECRET" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Уникальных">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_UNIQUE" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Особо ценных">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_OC" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Принятых по договору на временное хр.">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_DEP" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Всего ед. уч.">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="REG_UNIT_COUNT" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Внесенных в описи">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="REG_UNIT_INVENTORY" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <fieldset>
                            <legend>Микроформы на правах подлинников</legend>
                            <%--СПЕЦИФИКАЦИЯ "Микроформы на правах подлинников"--%>
                            <eq:eqSpecification ID="vARCHIVE_STATS_M1" runat="server" ShowNumering="false" AllowDelete="false"
                                AllowInsert="false" AllowReorder="false">
                                <eq:eqSpecificationColumn HeaderText="" Width="150">
                                    <ItemTemplate>
                                        <asp:Label ID="ISN_DOC_TYPE" runat="server" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Кол-во фондов">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="FUND_COUNT" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Всего ед. хр.">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_COUNT" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Внесенных в описи">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_INVENTORY" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Секретных">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_SECRET" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Уникальных">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_UNIQUE" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Особо ценных">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_OC" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Принятых по договору на временное хр.">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_DEP" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Всего ед. уч.">
                                    <ItemTemplate>
                                        <asp:Panel runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Внесенных в описи">
                                    <ItemTemplate>
                                        <asp:Panel runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <%--СПЕЦИФИКАЦИЯ "Итого"--%>
                        <eq:eqSpecification ID="vARCHIVE_STATS1" runat="server" ShowNumering="false" AllowDelete="false"
                            AllowInsert="false" AllowReorder="false">
                            <eq:eqSpecificationColumn HeaderText="" Width="150">
                                <ItemTemplate>
                                    <asp:Label ID="ISN_DOC_TYPE" runat="server" />
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                            <eq:eqSpecificationColumn HeaderText="Кол-во фондов">
                                <ItemTemplate>
                                    <eq:eqTextBox ID="FUND_COUNT" runat="server" SkinID="Numeric" />
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                            <eq:eqSpecificationColumn HeaderText="Всего ед. хр.">
                                <ItemTemplate>
                                    <eq:eqTextBox ID="UNIT_COUNT" runat="server" SkinID="Numeric" />
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                            <eq:eqSpecificationColumn HeaderText="Внесенных в описи">
                                <ItemTemplate>
                                    <eq:eqTextBox ID="UNIT_INVENTORY" runat="server" SkinID="Numeric" />
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                            <eq:eqSpecificationColumn HeaderText="Секретных">
                                <ItemTemplate>
                                    <eq:eqTextBox ID="UNIT_SECRET" runat="server" SkinID="Numeric" />
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                            <eq:eqSpecificationColumn HeaderText="Уникальных">
                                <ItemTemplate>
                                    <eq:eqTextBox ID="UNIT_UNIQUE" runat="server" SkinID="Numeric" />
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                            <eq:eqSpecificationColumn HeaderText="Особо ценных">
                                <ItemTemplate>
                                    <eq:eqTextBox ID="UNIT_OC" runat="server" SkinID="Numeric" />
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                            <eq:eqSpecificationColumn HeaderText="Принятых по договору на временное хр.">
                                <ItemTemplate>
                                    <eq:eqTextBox ID="UNIT_DEP" runat="server" SkinID="Numeric" />
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                            <eq:eqSpecificationColumn HeaderText="Всего ед. уч.">
                                <ItemTemplate>
                                    <asp:Panel runat="server" SkinID="Numeric" />
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                            <eq:eqSpecificationColumn HeaderText="Внесенных в описи">
                                <ItemTemplate>
                                    <asp:Panel runat="server" SkinID="Numeric" />
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                        </eq:eqSpecification>
                    </td>
                </tr>
            </table>
        </asp:View>
        <%--Состав СФ и ФП--%>
        <asp:View ID="View3" runat="server">
            <table style="width: 700px;">
                <tr>
                    <td>
                        Состав и объем страхового фонда и фонда пользования
                    </td>
                </tr>
                <tr>
                    <td>
                        <fieldset>
                            <legend>Документы на бумажных носителях</legend>
                            <%--СПЕЦИФИКАЦИЯ "Документы на бумажных носителях"--%>
                            <eq:eqSpecification ID="vARCHIVE_STATS_P2" runat="server" ShowNumering="false" AllowDelete="false"
                                AllowInsert="false" AllowReorder="false">
                                <eq:eqSpecificationColumn HeaderText="" Width="150">
                                    <ItemTemplate>
                                        <asp:Label ID="ISN_DOC_TYPE" runat="server" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Кол-во ед. хр. скопированных для страхового фонда">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_HAS_SF" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Кол-во ед. хр. имеющих фонд пользования">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_HAS_FP" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Объем страхового фонда, кол-во кадров негатива">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="NEGATIVE_COUNT" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Объем страхового фонда, кол-во ед. хр. страхового фонда">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="SF_COUNT" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <fieldset>
                            <legend>Аудиовизуальные документы на традиционных носителях</legend>
                            <%--СПЕЦИФИКАЦИЯ "Аудиовизуальные документы на традиционных носителях"--%>
                            <eq:eqSpecification ID="vARCHIVE_STATS_A2" runat="server" ShowNumering="false" AllowDelete="false"
                                AllowInsert="false" AllowReorder="false">
                                <eq:eqSpecificationColumn HeaderText="" Width="150">
                                    <ItemTemplate>
                                        <asp:Label ID="ISN_DOC_TYPE" runat="server" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Кол-во ед. хр. скопированных для страхового фонда">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_HAS_SF" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Кол-во ед. хр. имеющих фонд пользования">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_HAS_FP" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Объем страхового фонда, кол-во кадров негатива">
                                    <ItemTemplate>
                                        <asp:Panel runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Объем страхового фонда, кол-во ед. хр. страхового фонда">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="SF_COUNT" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <fieldset>
                            <legend>Документы на электронных носителях</legend>
                            <%--СПЕЦИФИКАЦИЯ "Документы на электронных носителях"--%>
                            <eq:eqSpecification ID="vARCHIVE_STATS_E2" runat="server" ShowNumering="false" AllowDelete="false"
                                AllowInsert="false" AllowReorder="false">
                                <eq:eqSpecificationColumn HeaderText="" Width="150">
                                    <ItemTemplate>
                                        <asp:Label ID="ISN_DOC_TYPE" runat="server" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Кол-во ед. хр. скопированных для страхового фонда">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_HAS_SF" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Кол-во ед. хр. имеющих фонд пользования">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_HAS_FP" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Объем страхового фонда, кол-во кадров негатива">
                                    <ItemTemplate>
                                        <asp:Panel runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Объем страхового фонда, кол-во ед. хр. страхового фонда">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="SF_COUNT" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <fieldset>
                            <legend>Микроформы на правах подлинников</legend>
                            <%--СПЕЦИФИКАЦИЯ "Микроформы на правах подлинников"--%>
                            <eq:eqSpecification ID="vARCHIVE_STATS_M2" runat="server" ShowNumering="false" AllowDelete="false"
                                AllowInsert="false" AllowReorder="false">
                                <eq:eqSpecificationColumn HeaderText="" Width="150">
                                    <ItemTemplate>
                                        <asp:Label ID="ISN_DOC_TYPE" runat="server" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Кол-во ед. хр. скопированных для страхового фонда">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_HAS_SF" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Кол-во ед. хр. имеющих фонд пользования">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_HAS_FP" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Объем страхового фонда, кол-во кадров негатива">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="NEGATIVE_COUNT" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Объем страхового фонда, кол-во ед. хр. страхового фонда">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="SF_COUNT" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <%--СПЕЦИФИКАЦИЯ "Итого"--%>
                        <eq:eqSpecification ID="vARCHIVE_STATS2" runat="server" ShowNumering="false" AllowDelete="false"
                            AllowInsert="false" AllowReorder="false">
                            <eq:eqSpecificationColumn HeaderText="" Width="150">
                                <ItemTemplate>
                                    Всего
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                            <eq:eqSpecificationColumn HeaderText="Кол-во ед. хр. скопированных для страхового фонда">
                                <ItemTemplate>
                                    <eq:eqTextBox ID="UNIT_HAS_SF" runat="server" SkinID="Numeric" />
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                            <eq:eqSpecificationColumn HeaderText="Кол-во ед. хр. имеющих фонд пользования">
                                <ItemTemplate>
                                    <eq:eqTextBox ID="UNIT_HAS_FP" runat="server" SkinID="Numeric" />
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                            <eq:eqSpecificationColumn HeaderText="Объем страхового фонда, кол-во кадров негатива">
                                <ItemTemplate>
                                    <eq:eqTextBox ID="NEGATIVE_COUNT" runat="server" SkinID="Numeric" />
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                            <eq:eqSpecificationColumn HeaderText="Объем страхового фонда, кол-во ед. хр. страхового фонда">
                                <ItemTemplate>
                                    <eq:eqTextBox ID="SF_COUNT" runat="server" SkinID="Numeric" />
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                        </eq:eqSpecification>
                    </td>
                </tr>
            </table>
        </asp:View>
        <%--Состав НСА--%>
        <asp:View ID="View4" runat="server">
            <table style="width: 700px;">
                <tr>
                    <td>
                        Описи, каталоги, базы данных
                    </td>
                </tr>
                <tr>
                    <td>
                        <fieldset>
                            <legend>Документы на бумажных носителях</legend>
                            <%--СПЕЦИФИКАЦИЯ "Документы на бумажных носителях"--%>
                            <eq:eqSpecification ID="vARCHIVE_STATS_P3" runat="server" ShowNumering="false" AllowDelete="false"
                                AllowInsert="false" AllowReorder="false">
                                <eq:eqSpecificationColumn HeaderText="" Width="150">
                                    <ItemTemplate>
                                        <asp:Label ID="ISN_DOC_TYPE" runat="server" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Всего традиционных описей">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="INVENTORY_COUNT" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="В полн. комп. трад. описей">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="INVENTORY_COUNT_FULL" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Электронных описей">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="EL_INVENTORY_COUNT" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Кол-во ед. хр. / ед. уч.">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_CATALOGUED" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Трад. каталоги: Кол-во карт.">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="PAPER_CARD_COUNT" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Электронные каталоги: Кол-во записей">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="EL_RECORD_COUNT" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Элнктронные каталоги: Объем в МБ">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="EL_CATALOGUE_VOLUME" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="БД: Кол-во БД">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="DB_COUNT" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="БД: Кол-во записей">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="DB_RECORD_COUNT" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="БД: Объем в МБ">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="DB_VOLUME" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <fieldset>
                            <legend>Аудиовизуальные документы на традиционных носителях</legend>
                            <%--СПЕЦИФИКАЦИЯ "Аудиовизуальные документы на традиционных носителях"--%>
                            <eq:eqSpecification ID="vARCHIVE_STATS_A3" runat="server" ShowNumering="false" AllowDelete="false"
                                AllowInsert="false" AllowReorder="false">
                                <eq:eqSpecificationColumn HeaderText="" Width="150">
                                    <ItemTemplate>
                                        <asp:Label ID="ISN_DOC_TYPE" runat="server" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Всего традиционных описей">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="INVENTORY_COUNT" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="В полн. комп. трад. описей">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="INVENTORY_COUNT_FULL" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Электронных описей">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="EL_INVENTORY_COUNT" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Кол-во ед. хр. / ед. уч.">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_CATALOGUED" runat="server" SkinID="Numeric" />
                                        <eq:eqTextBox ID="REG_UNIT_CATALOGUED" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Трад. каталоги: Кол-во карт.">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="PAPER_CARD_COUNT" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Электронные каталоги: Кол-во записей">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="EL_RECORD_COUNT" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Электронные каталоги: Объем в МБ">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="EL_CATALOGUE_VOLUME" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="БД: Кол-во БД">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="DB_COUNT" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="БД: Кол-во записей">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="DB_RECORD_COUNT" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="БД: Объем в МБ">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="DB_VOLUME" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <fieldset>
                            <legend>Документы на электронных носителях</legend>
                            <%--СПЕЦИФИКАЦИЯ "Документы на электронных носителях"--%>
                            <eq:eqSpecification ID="vARCHIVE_STATS_E3" runat="server" ShowNumering="false" AllowDelete="false"
                                AllowInsert="false" AllowReorder="false">
                                <eq:eqSpecificationColumn HeaderText="" Width="150">
                                    <ItemTemplate>
                                        <asp:Label ID="ISN_DOC_TYPE" runat="server" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Всего традиционных описей">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="INVENTORY_COUNT" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="В полн. комп. трад. описей">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="INVENTORY_COUNT_FULL" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Электронных описей">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="EL_INVENTORY_COUNT" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Кол-во ед. хр. / ед. уч.">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_CATALOGUED" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Трад. каталоги: Кол-во карт.">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="PAPER_CARD_COUNT" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Электронные каталоги: Кол-во записей">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="EL_RECORD_COUNT" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Элнктронные каталоги: Объем в МБ">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="EL_CATALOGUE_VOLUME" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="БД: Кол-во БД">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="DB_COUNT" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="БД: Кол-во записей">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="DB_RECORD_COUNT" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="БД: Объем в МБ">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="DB_VOLUME" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <fieldset>
                            <legend>Микроформы на правах подлинников</legend>
                            <%--СПЕЦИФИКАЦИЯ "Микроформы на правах подлинников"--%>
                            <eq:eqSpecification ID="vARCHIVE_STATS_M3" runat="server" ShowNumering="false" AllowDelete="false"
                                AllowInsert="false" AllowReorder="false">
                                <eq:eqSpecificationColumn HeaderText="" Width="150">
                                    <ItemTemplate>
                                        <asp:Label ID="ISN_DOC_TYPE" runat="server" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Всего традиционных описей">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="INVENTORY_COUNT" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="В полн. комп. трад. описей">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="INVENTORY_COUNT_FULL" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Электронных описей">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="EL_INVENTORY_COUNT" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Кол-во ед. хр. / ед. уч.">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="UNIT_CATALOGUED" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Трад. каталоги: Кол-во карт.">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="PAPER_CARD_COUNT" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Электронные каталоги: Кол-во записей">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="EL_RECORD_COUNT" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Элнктронные каталоги: Объем в МБ">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="EL_CATALOGUE_VOLUME" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="БД: Кол-во БД">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="DB_COUNT" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="БД: Кол-во записей">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="DB_RECORD_COUNT" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="БД: Объем в МБ">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="DB_VOLUME" runat="server" SkinID="Numeric" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <%--СПЕЦИФИКАЦИЯ "Итого"--%>
                        <eq:eqSpecification ID="vARCHIVE_STATS3" runat="server" ShowNumering="false" AllowDelete="false"
                            AllowInsert="false" AllowReorder="false">
                            <eq:eqSpecificationColumn HeaderText="" Width="150">
                                <ItemTemplate>
                                    Всего
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                            <eq:eqSpecificationColumn HeaderText="Всего традиционных описей">
                                <ItemTemplate>
                                    <eq:eqTextBox ID="INVENTORY_COUNT" runat="server" SkinID="Numeric" />
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                            <eq:eqSpecificationColumn HeaderText="В полн. комп. трад. описей">
                                <ItemTemplate>
                                    <eq:eqTextBox ID="INVENTORY_COUNT_FULL" runat="server" SkinID="Numeric" />
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                            <eq:eqSpecificationColumn HeaderText="Электронных описей">
                                <ItemTemplate>
                                    <eq:eqTextBox ID="EL_INVENTORY_COUNT" runat="server" SkinID="Numeric" />
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                            <eq:eqSpecificationColumn HeaderText="Кол-во ед. хр. / ед. уч.">
                                <ItemTemplate>
                                    <eq:eqTextBox ID="UNIT_CATALOGUED" runat="server" SkinID="Numeric" />
                                    <eq:eqTextBox ID="REG_UNIT_CATALOGUED" runat="server" SkinID="Numeric" />
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                            <eq:eqSpecificationColumn HeaderText="Трад. каталоги: Кол-во карт.">
                                <ItemTemplate>
                                    <eq:eqTextBox ID="PAPER_CARD_COUNT" runat="server" SkinID="Numeric" />
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                            <eq:eqSpecificationColumn HeaderText="Электронные каталоги: Кол-во записей">
                                <ItemTemplate>
                                    <eq:eqTextBox ID="EL_RECORD_COUNT" runat="server" SkinID="Numeric" />
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                            <eq:eqSpecificationColumn HeaderText="Элнктронные каталоги: Объем в МБ">
                                <ItemTemplate>
                                    <eq:eqTextBox ID="EL_CATALOGUE_VOLUME" runat="server" SkinID="Numeric" />
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                            <eq:eqSpecificationColumn HeaderText="БД: Кол-во БД">
                                <ItemTemplate>
                                    <eq:eqTextBox ID="DB_COUNT" runat="server" SkinID="Numeric" />
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                            <eq:eqSpecificationColumn HeaderText="БД: Кол-во записей">
                                <ItemTemplate>
                                    <eq:eqTextBox ID="DB_RECORD_COUNT" runat="server" SkinID="Numeric" />
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                            <eq:eqSpecificationColumn HeaderText="БД: Объем в МБ">
                                <ItemTemplate>
                                    <eq:eqTextBox ID="DB_VOLUME" runat="server" SkinID="Numeric" />
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                        </eq:eqSpecification>
                    </td>
                </tr>
            </table>
        </asp:View>
        <%--Доп. инф.--%>
        <asp:View ID="View5" runat="server">
            <table style="width: 700px;">
                <colgroup>
                    <col style="width: 50%;" />
                    <col />
                </colgroup>
                <tr>
                    <td>
                        <fieldset>
                            <legend>Система справочно-информационных изданий </legend>
                            <table width="100%">
                                <colgroup>
                                    <col width="90%" />
                                    <col />
                                </colgroup>
                                <tr>
                                    <td>
                                        Всего
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="ALL_SII_COUNT" runat="server" SkinID="Numeric" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Путеводители, краткие справочники по фондам
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="GUIDE_COUNT" runat="server" SkinID="Numeric" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        из них по Л/С
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="GUIDE_LS" runat="server" SkinID="Numeric" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Описи
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="INVENTORY_COUNT" runat="server" SkinID="Numeric" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Каталоги
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="CATALOUGUE_COUNT" runat="server" SkinID="Numeric" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Указатели
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="INDEX_COUNT" runat="server" SkinID="Numeric" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Обзоры
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="REVIEW_COUNT" runat="server" SkinID="Numeric" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        По истории организаций
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="HISTORY_COUNT" runat="server" SkinID="Numeric" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        По административно-территориальному делению
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="ATD_COUNT" runat="server" SkinID="Numeric" />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <fieldset>
                            <legend>Состав и объем научно справочной библиотеки </legend>
                            <table width="100%">
                                <colgroup>
                                    <col width="90%" />
                                    <col />
                                </colgroup>
                                <tr>
                                    <td>
                                        Книги и брошюры
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="BOOK_COUNT" runat="server" SkinID="Numeric" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Газеты
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="NEWSPAPER_COUNT" runat="server" SkinID="Numeric" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Журналы
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="MAGAZINE_COUNT" runat="server" SkinID="Numeric" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Др. виды печатной продукции
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="OTHER_PRINT_COUNT" runat="server" SkinID="Numeric" />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                    <td>
                        <fieldset>
                            <legend>Условия хранения документов </legend>
                            <table width="100%">
                                <colgroup>
                                    <col width="90%" />
                                    <col />
                                </colgroup>
                                <tr>
                                    <td>
                                        Зданий (помещений) архива. Всего:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="ARH_BUILDING_COUNT" runat="server" SkinID="Numeric" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        в т. ч. специальных
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="SPEC_BUILDING_COUNT" runat="server" SkinID="Numeric" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        приспособленных
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="PRISP_BUILDING_COUNT" runat="server" SkinID="Numeric" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Общая площадь зданий (помещений) (кв.м.)
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="TOTAL_SPACE" runat="server" SkinID="Numeric" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        в т. ч. хранилищ
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="STORAGE_SPACE" runat="server" SkinID="Numeric" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Площадь зданий (помещений), не оснащенных охранной сигнализацией, кв. м.
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="SPACE_WITHOUT_SECUR" runat="server" SkinID="Numeric" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Площадь зданий (помещений), не оснащенных охранной сигнализацией, %
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="SPACE_WITHOU_SECURITY_PROC" runat="server" SkinID="Numeric" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Площадь зданий (помещений), не оснащенных пожарной сигнализацией, кв. м.
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="SPACE_WITHOUT_ALARM" runat="server" SkinID="Numeric" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Площадь зданий (помещений), не оснащенных пожарной сигнализацией, %
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="SPACE_WITHOUT_ALARM_PROC" runat="server" SkinID="Numeric" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Протяженность стелажных полок (пог. м.). Всего:
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="SHELF_LENGTH" runat="server" SkinID="Numeric" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        в т. ч. металлических
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="METAL_SHELF_LENGTH" runat="server" SkinID="Numeric" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Протяженность свободных стелажных полок (пог. м.)
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="FREE_SHELF_LENGTH" runat="server" SkinID="Numeric" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Степень загруженности в %
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="LOAD_LEVEL" runat="server" SkinID="Numeric" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Закартонировано ед. хр.
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="UNITS_CARDBOARDED" runat="server" SkinID="Numeric" />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table width="50%">
                            <colgroup>
                                <col width="90%" />
                                <col />
                            </colgroup>
                            <tr>
                                <td>
                                    Год паспорта
                                </td>
                                <td>
                                    <eq:eqTextBox ID="PASS_YEAR" runat="server" SkinID="Numeric" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Микроформы: Всего кадров
                                </td>
                                <td>
                                    <eq:eqTextBox ID="MICROFORM_FRAME_COUNT" runat="server" SkinID="Numeric" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Документы Л/П, не внесенные в описи
                                </td>
                                <td>
                                    <eq:eqTextBox ID="LP_DOC_COUNT" runat="server" SkinID="Numeric" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </asp:View>
    </asp:MultiView>
</div>
