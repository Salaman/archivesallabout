﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="RecalcFund.aspx.vb" Inherits="WebApplication.RecalcFund"  %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>

    <script type="text/javascript" src="jquery-1.6.min.js"></script>
    <style type="text/css">
                div.popupConteiner {
                    position: fixed;
                    width: 100%;
                    height: 100%;
                    background-color: rgba(0,0,0,0.5);
                    z-index: 999;
                    display: table;
                    left: 0;
                    top: 0;
                }
                div.popupCenter {
                    display: table-cell;
                    vertical-align: middle;
                }
                div.popup {
                    display: table;
                    border: 1px solid black;
                    background-color: #fff;
                    color: black;
                    margin: 0 auto;
                    padding: 5px;
                    text-align: center;
                }
                div.popup p {color: black; text-align : center;}
    </style>
</head>
<body style="background-color: White; padding: 5px;">

    <script type="text/javascript">
        sel = function(obj) {
            var scope = $(obj).parents('table').first();
            $(':input', scope).prop('checked', true);
        }
        des = function(obj) {
            var scope = $(obj).parents('table').first();
            $(':input', scope).removeProp('checked');
        }
    </script>



    <form id="form1" runat="server">
    <asp:Panel ID = "worningPanel" runat = "server">
         <div class="popupConteiner">
            <div class="popupCenter">
                <div class="popup">
                    <p>Создавать ли резервную копию перед запуском пересчета?</p>
                    <p>
                        <asp:Button ID="worningAnsYes" runat="server" Text="Создать резервную копию" />
                        <asp:Button ID="worningAnsNo" runat="server" Text="Пересчитывать без резервирования" />
                        <asp:Button ID="worningAnsCancel" runat="server" Text="Отказаться от пересчета" />
                    </p>
                    <asp:Label ID="worningLabel" Visible="false" runat="server"></asp:Label>
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID = "contentPanel" runat = "server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <eq:eqMenu ID="MenuTabs" runat="server" Orientation="Horizontal" StaticEnableDefaultPopOutImage="False"
        ItemWrap="false" HorizontalMaxCount="3">
        <Items>
            <asp:MenuItem Text="Основные сведения" Value="0" Selected="true" />
            <asp:MenuItem Text="Количественные характеристики" Value="1" />
            <asp:MenuItem Text="Количественные характеристики для каждого типа документов" Value="2" />
            <asp:MenuItem Text="Характеристики физического состояния" Value="3" />
            <asp:MenuItem Text="Топография" Value="4" />
        </Items>
        <StaticSelectedStyle CssClass="MenuItemStyleSelected" />
    </eq:eqMenu>
    <asp:MultiView ID="MultiViewTabs" runat="server" ActiveViewIndex="0">
        <asp:View ID="View1" runat="server">
            <table>
                <tr>
                    <td colspan="2">
                        <a href="#" onclick="sel(this);">Выделить все</a>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <a href="#" onclick="des(this);">Снять выделение</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="c_SROK_HRAN" Text="Срок хранения" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="c_HAS_MUSEUM" Text="Наличие музейных предметов" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="c_VVEDENO_O" Text="Введено описей" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="c_TREASURE_COUNT" Text="Кол.-во ед. хр. с драг. камнями и металлами в оформлении" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="c_NACH_DATA" Text="Начальная дата документов" />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="c_KON_DATA" Text="Конечная дата документов" />
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
        </asp:View>
        <asp:View ID="View2" runat="server">
            <table>
                <tr>
                    <td colspan="2">
                        <a href="#" onclick="sel(this);">Выделить все</a>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <a href="#" onclick="des(this);">Снять выделение</a>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:CheckBox runat="server" ID="c_SUM_AA" Text="Всех видов документов на всех носителях" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:CheckBox runat="server" ID="c_SUM_BO" Text="Документов на бумажной основе" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:CheckBox runat="server" ID="c_SUM_TN" Text="Аудиовизуальных документов на традиционных носителях" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:CheckBox runat="server" ID="c_SUM_EN" Text="Документов на электронных носителях" />
                    </td>
                </tr>
            </table>
        </asp:View>
        <asp:View ID="View3" runat="server">
            <table>
                <tr>
                    <td colspan="2">
                        <a href="#" onclick="sel(this);">Выделить все</a>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <a href="#" onclick="des(this);">Снять выделение</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="c_ERH_ALL" Text="Всего единиц хранения" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="c_EU_ALL" Text="Всего единиц учета" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="c_ERHO_ALL" Text="Всего единиц хранения по описям" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="c_EUO_ALL" Text="Всего единиц учета по описям" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:CheckBox runat="server" ID="c_NEOP_EHR" Text="Количество неописанных единиц хранения, документов, листов" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="c_EHR_REG" Text="Количество зарегистрированных единиц хранения" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="c_EU_REG" Text="Количество зарегистрированных единиц учета" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="c_ERH_OC" Text="Количество особо ценных единиц хранения" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="c_EU_OC" Text="Количество особо ценных единиц учета" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="c_ERH_U" Text="Количество уникальных единиц хранения" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="c_EU_U" Text="Количество уникальных единиц учета" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="c_EHR_SF" Text="Количество единиц хранения, имеющих СФ" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="c_EU_SF" Text="Количество единиц учета, имеющих СФ" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="c_EHR_FP" Text="Количество единиц хранения, имеющих ФП" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="c_EU_FP" Text="Количество единиц учета, имеющих ФП" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="c_EHR_NEOB" Text="Количество необнаруженных единиц хранения" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="c_EU_NEOB" Text="Количество необнаруженных единиц учета" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="c_EHR_SEC" Text="Количество секретных единиц хранения" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="c_EU_SEC" Text="Количество секретных единиц учета" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="c_EHR_CAT" Text="Количество закаталогизированных единиц хранения" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="c_EU_CAT" Text="Количество закаталогизированных единиц учета" />
                    </td>
                </tr>
            </table>
        </asp:View>
        <asp:View ID="View4" runat="server">
            <table>
                <tr>
                    <td colspan="2">
                        <a href="#" onclick="sel(this);">Выделить все</a>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <a href="#" onclick="des(this);">Снять выделение</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="c_E_KARTED" Text="Количество закартонированных единиц хранения" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="c_E_SHIFR" Text="Количество единиц хранения, требующих шифровки" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="c_E_KART" Text="Количество единиц хранения, требующих картонизации" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="c_E_ZAM_OBL" Text="Количество единиц хранения, требующих замены обложки" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="c_E_POVR" Text="Количество неисправимо поврежденных единиц хранения" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="c_E_ZAT" Text="Количество листов с затухающими текстами" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="c_E_RESTAVR" Text="Количество единиц хранения, требующих реставрации" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="c_E_GOR" Text="Количество на горючей основе" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="c_E_PODSH" Text="Количество единиц хранения, требующих подшивки и переплета" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="c_E_KPO" Text="Количество единиц хранения, требующих КПО" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:CheckBox runat="server" ID="c_E_DEZINF" Text="Количество единиц хранения, требующих дезинфекции" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:CheckBox runat="server" ID="c_E_DEZINS" Text="Количество единиц хранения, требующих дезинсекции" />
                    </td>
                </tr>
            </table>
        </asp:View>
        <asp:View ID="View5" runat="server">
            <table>
                <tr>
                    <td colspan="2">
                        <asp:CheckBox runat="server" ID="c_HRAN_FOND" Text="Хранилища фонда" />
                    </td>
                </tr>
            </table>
        </asp:View>
    </asp:MultiView>
    <table style="padding: 10px;">
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:Button runat="server" ID="bGo" Text="Пересчитать" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>
                <asp:Button runat="server" ID="bClose" Text="Закрыть" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                    <ProgressTemplate>
                        <asp:Image ID="Image1" runat="server" SkinID="UpdateProgress" />
                        <br />
                        <asp:Label ID="Label1" runat="server" Text="Идет расчет..." />
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </td>
        </tr>
    </table>
    </asp:Panel>
    </form>

</body>
</html>
