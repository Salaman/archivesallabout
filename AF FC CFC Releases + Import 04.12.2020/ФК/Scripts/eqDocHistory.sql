﻿CREATE TABLE [dbo].[eqDocHistory](
	[ID] [uniqueidentifier] NOT NULL,
	[OwnerID] [uniqueidentifier] NULL,
	[CreationDateTime] [smalldatetime] NULL,
	[Deleted] [bit] NOT NULL,
	[DocTypeID] [uniqueidentifier] NOT NULL,
	[DocContent] [xml] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[eqDocHistory] ADD  CONSTRAINT [DF_eqDocHistory_Deleted]  DEFAULT ((0)) FOR [Deleted]
GO
