﻿-- Specification
CREATE TABLE [dbo].[tblREF_CLS](
	ID uniqueidentifier NOT NULL,
	OwnerID uniqueidentifier NOT NULL,
	CreationDateTime datetime NOT NULL,
	DocID uniqueidentifier NOT NULL,
	RowID int NOT NULL,
	[ISN_REF_CLS] [int] NOT NULL,
	[ISN_CLS] [int] NULL,
	[ISN_TREE] [int] NULL,
	[ISN_OBJ] [int] NULL,
	[ORDER_NUM] [int] NULL,
	[KIND] [smallint] NULL,
	[NOTE] [varchar](300) NULL)
GO
