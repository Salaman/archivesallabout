﻿-- Specification
CREATE TABLE [dbo].[tblREF_ACT](
	ID uniqueidentifier NOT NULL,
	OwnerID uniqueidentifier NOT NULL,
	CreationDateTime datetime NOT NULL,
	DocID uniqueidentifier NOT NULL,
	RowID int NOT NULL,
	[ISN_REF_ACT] [int] NOT NULL,
	[ISN_ACT] [int] NOT NULL,
	[ISN_OBJ] [int] NOT NULL,
	[KIND] [smallint] NOT NULL,
	[UNIT_COUNT] [int] NULL)
GO
