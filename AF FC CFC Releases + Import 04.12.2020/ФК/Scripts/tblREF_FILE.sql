﻿-- Specification
CREATE TABLE [dbo].[tblREF_FILE](
	ID uniqueidentifier NOT NULL,
	OwnerID uniqueidentifier NOT NULL,
	CreationDateTime datetime NOT NULL,
	DocID uniqueidentifier NOT NULL,
	RowID int NOT NULL,
	[ISN_REF_FILE] [int] NOT NULL,
	[ISN_OBJ] [int] NULL,
	[KIND] [smallint] NULL,
	[GR_STORAGE] [smallint] NULL,
	[NAME] [varchar](300) NULL,
	[CATEGORY] [varchar](20) NULL,
	[WEIGHT] [int] NULL)
GO
