﻿-- Spravochn
CREATE TABLE [dbo].[tblSECURLEVEL](
	ID uniqueidentifier NOT NULL,
	OwnerID uniqueidentifier NOT NULL,
	CreationDateTime datetime NOT NULL,
	StatusID uniqueidentifier NOT NULL,
	Deleted bit NOT NULL,
	[ISN_SECURLEVEL] [int] NOT NULL,
	[CODE] [varchar](20) NULL,
	[NAME] [varchar](300) NULL,
	[NOTE] [varchar](max) NULL,
	[PROTECTED] [varchar](1) NULL,
	[WEIGHT] [int] NULL)
GO
