﻿-- Spravochn
CREATE TABLE [dbo].[tblACT](
	ID uniqueidentifier NOT NULL,
	OwnerID uniqueidentifier NOT NULL,
	CreationDateTime datetime NOT NULL,
	StatusID uniqueidentifier NOT NULL,
	Deleted bit NOT NULL,
	[ISN_ACT] [int] NOT NULL,
	[ISN_ACT_TYPE] [int] NULL,
	[ISN_COMMISSION] [int] NULL,
	[ISN_ESTIMATE_REASON] [int] NULL,
	[ISN_FUND] [int] NULL,
	[ACT_NUM] [varchar](30) NOT NULL,
	[ACT_DATE] [datetime] NOT NULL,
	[ACT_NAME] [varchar](300) NULL,
	[MOVEMENT_FLAG] [varchar](1) NOT NULL,
	[ACT_OBJ] [smallint] NOT NULL,
	[ACT_PERSONS] [varchar](max) NULL,
	[NOTE] [varchar](max) NULL,
	[ACT_WORK] [varchar](max) NULL,
	[UNIT_COUNT] [int] NULL,
	[PAGE_NUMBERS] [varchar](300) NULL,
	[DOC_DATES] [varchar](300) NULL)
GO
