﻿-- Spravochn
CREATE TABLE [dbo].[tblSECURITY_REASON](
	ID uniqueidentifier NOT NULL,
	OwnerID uniqueidentifier NOT NULL,
	CreationDateTime datetime NOT NULL,
	StatusID uniqueidentifier NOT NULL,
	Deleted bit NOT NULL,
	[Isn_security_reason] [int] NOT NULL,
	[Code] [varchar](20) NULL,
	[Name] [varchar](300) NULL,
	[Note] [varchar](max) NULL,
	[Protected] [varchar](1) NULL,
	[Weight] [int] NULL)
GO
