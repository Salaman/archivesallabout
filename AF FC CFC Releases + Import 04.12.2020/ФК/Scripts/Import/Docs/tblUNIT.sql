﻿-- Delete Specifications
DELETE FROM [eqTest].[dbo].[tblUNIT_ELECTRONIC]
DELETE FROM [eqTest].[dbo].[tblUNIT_FOTO]
DELETE FROM [eqTest].[dbo].[tblUNIT_MICROFORM]
DELETE FROM [eqTest].[dbo].[tblUNIT_MOVIE]
DELETE FROM [eqTest].[dbo].[tblUNIT_NTD]
DELETE FROM [eqTest].[dbo].[tblUNIT_PHONO]
DELETE FROM [eqTest].[dbo].[tblUNIT_REQUIRED_WORK]
DELETE FROM [eqTest].[dbo].[tblUNIT_STATE]
DELETE FROM [eqTest].[dbo].[tblUNIT_VIDEO]
DELETE FROM [eqTest].[dbo].[tblUNIT_WORK]
-- Delete Header
DELETE FROM [eqTest].[dbo].[tblUNIT]
-- Header
SET IDENTITY_INSERT [eqTest].[dbo].[tblUNIT] ON
GO
DECLARE @OwnerID uniqueidentifier
DECLARE @StatusID uniqueidentifier
SELECT TOP 1 @OwnerID=ID FROM eqUsers WHERE Login='sa'
SELECT @StatusID=ID FROM eqDocStates WHERE StateName='Сохранен' AND DocTypeID=(SELECT ID FROM eqDocTypes WHERE DocType='Единица хранения / Единица учета')
INSERT INTO [eqTest].[dbo].[tblUNIT]
           ([ID]
           ,[OwnerID]
           ,[CreationDateTime]
           ,[StatusID]
           ,[Deleted]
           ,[ISN_UNIT]
           ,[ISN_HIGH_UNIT]
           ,[ISN_INVENTORY]
           ,[ISN_DOC_TYPE]
           ,[ISN_LOCATION]
           ,[ISN_SECURLEVEL]
           ,[SECURITY_CHAR]
           ,[SECURITY_REASON]
           ,[ISN_INVENTORY_CLS]
           ,[ISN_STORAGE_MEDIUM]
           ,[ISN_DOC_KIND]
           ,[UNIT_KIND]
           ,[UNIT_NUM_1]
           ,[UNIT_NUM_2]
           ,[VOL_NUM]
           ,[NAME]
           ,[ANNOTATE]
           ,[DELO_INDEX]
           ,[PRODUCTION_NUM]
           ,[UNIT_CATEGORY]
           ,[NOTE]
           ,[IS_IN_SEARCH]
           ,[IS_LOST]
           ,[HAS_SF]
           ,[HAS_FP]
           ,[HAS_DEFECTS]
           ,[ARCHIVE_CODE]
           ,[CATALOGUED]
           ,[WEIGHT]
           ,[UNIT_CNT]
           ,[START_YEAR]
           ,[START_YEAR_INEXACT]
           ,[END_YEAR]
           ,[END_YEAR_INEXACT]
           ,[MEDIUM_TYPE]
           ,[BACKUP_COPY_CNT]
           ,[HAS_TREASURES]
           ,[IS_MUSEUM_ITEM]
           ,[PAGE_COUNT]
           ,[CARDBOARDED]
           ,[ADDITIONAL_CLS]
           ,[ALL_DATE]
           ,[ISN_SECURITY_REASON])
     SELECT
           NEWID()
           ,@OwnerID
           ,GETDATE()
           ,@StatusID
           ,0
           ,[ISN_UNIT]
           ,[ISN_HIGH_UNIT]
           ,[ISN_INVENTORY]
           ,[ISN_DOC_TYPE]
           ,[ISN_LOCATION]
           ,[ISN_SECURLEVEL]
           ,[SECURITY_CHAR]
           ,[SECURITY_REASON]
           ,[ISN_INVENTORY_CLS]
           ,[ISN_STORAGE_MEDIUM]
           ,[ISN_DOC_KIND]
           ,[UNIT_KIND]
           ,[UNIT_NUM_1]
           ,[UNIT_NUM_2]
           ,[VOL_NUM]
           ,[NAME]
           ,[ANNOTATE]
           ,[DELO_INDEX]
           ,[PRODUCTION_NUM]
           ,[UNIT_CATEGORY]
           ,[NOTE]
           ,[IS_IN_SEARCH]
           ,[IS_LOST]
           ,[HAS_SF]
           ,[HAS_FP]
           ,[HAS_DEFECTS]
           ,[ARCHIVE_CODE]
           ,[CATALOGUED]
           ,[WEIGHT]
           ,[UNIT_CNT]
           ,[START_YEAR]
           ,[START_YEAR_INEXACT]
           ,[END_YEAR]
           ,[END_YEAR_INEXACT]
           ,[MEDIUM_TYPE]
           ,[BACKUP_COPY_CNT]
           ,[HAS_TREASURES]
           ,[IS_MUSEUM_ITEM]
           ,[PAGE_COUNT]
           ,[CARDBOARDED]
           ,[ADDITIONAL_CLS]
           ,[ALL_DATE]
           ,[ISN_SECURITY_REASON]
     FROM [ROS_DB].[dbo].[UNIT]
GO
SET IDENTITY_INSERT [eqTest].[dbo].[tblUNIT] OFF
GO
-- Specification
DECLARE @OwnerID uniqueidentifier
SELECT TOP 1 @OwnerID=ID FROM eqUsers WHERE Login='sa'
INSERT INTO [eqTest].[dbo].[tblUNIT_ELECTRONIC]
           ([ID]
           ,[OwnerID]
           ,[CreationDateTime]
           ,[DocID]
           ,[RowID]
           ,[ISN_UNIT]
           ,[DATA_FORMAT]
           ,[SIZE]
           ,[CARRIER_DESC]
           ,[ACCOMP_DOC]
           ,[UNIT_CNT])
     SELECT
           NEWID()
           ,@OwnerID
           ,GETDATE()
           ,(SELECT TOP 1 ID FROM tblUNIT WHERE tblUNIT.ISN_UNIT = ISN_UNIT)
           ,0
           ,[ISN_UNIT]
           ,[DATA_FORMAT]
           ,[SIZE]
           ,[CARRIER_DESC]
           ,[ACCOMP_DOC]
           ,[UNIT_CNT]
     FROM [ROS_DB].[dbo].[UNIT_ELECTRONIC]
GO
-- Specification
DECLARE @OwnerID uniqueidentifier
SELECT TOP 1 @OwnerID=ID FROM eqUsers WHERE Login='sa'
INSERT INTO [eqTest].[dbo].[tblUNIT_FOTO]
           ([ID]
           ,[OwnerID]
           ,[CreationDateTime]
           ,[DocID]
           ,[RowID]
           ,[ISN_UNIT]
           ,[ISN_DOC_KIND]
           ,[PLACE]
           ,[AUTHOR]
           ,[BASE_KIND]
           ,[COLOR]
           ,[SIZE]
           ,[STORAGE_INFO]
           ,[NEGATIVE_COUNT]
           ,[POSITIVE_COUNT]
           ,[PRINT_COUNT]
           ,[DUP_NEGATIVE_COUNT]
           ,[SLIDE_COUNT]
           ,[BACKUP_COUNT]
           ,[SHOT_COUNT]
           ,[ORIGINAL]
           ,[ACCOMP_DOC]
           ,[FOTO_DATE])
     SELECT
           NEWID()
           ,@OwnerID
           ,GETDATE()
           ,(SELECT TOP 1 ID FROM tblUNIT WHERE tblUNIT.ISN_UNIT = ISN_UNIT)
           ,0
           ,[ISN_UNIT]
           ,[ISN_DOC_KIND]
           ,[PLACE]
           ,[AUTHOR]
           ,[BASE_KIND]
           ,[COLOR]
           ,[SIZE]
           ,[STORAGE_INFO]
           ,[NEGATIVE_COUNT]
           ,[POSITIVE_COUNT]
           ,[PRINT_COUNT]
           ,[DUP_NEGATIVE_COUNT]
           ,[SLIDE_COUNT]
           ,[BACKUP_COUNT]
           ,[SHOT_COUNT]
           ,[ORIGINAL]
           ,[ACCOMP_DOC]
           ,[FOTO_DATE]
     FROM [ROS_DB].[dbo].[UNIT_FOTO]
GO
-- Specification
DECLARE @OwnerID uniqueidentifier
SELECT TOP 1 @OwnerID=ID FROM eqUsers WHERE Login='sa'
INSERT INTO [eqTest].[dbo].[tblUNIT_MICROFORM]
           ([ID]
           ,[OwnerID]
           ,[CreationDateTime]
           ,[DocID]
           ,[RowID]
           ,[ISN_UNIT]
           ,[FRAME_COUNT]
           ,[BACKUP_COUNT]
           ,[CARRIER_DESC]
           ,[ACCOMP_DOC])
     SELECT
           NEWID()
           ,@OwnerID
           ,GETDATE()
           ,(SELECT TOP 1 ID FROM tblUNIT WHERE tblUNIT.ISN_UNIT = ISN_UNIT)
           ,0
           ,[ISN_UNIT]
           ,[FRAME_COUNT]
           ,[BACKUP_COUNT]
           ,[CARRIER_DESC]
           ,[ACCOMP_DOC]
     FROM [ROS_DB].[dbo].[UNIT_MICROFORM]
GO
-- Specification
DECLARE @OwnerID uniqueidentifier
SELECT TOP 1 @OwnerID=ID FROM eqUsers WHERE Login='sa'
INSERT INTO [eqTest].[dbo].[tblUNIT_MOVIE]
           ([ID]
           ,[OwnerID]
           ,[CreationDateTime]
           ,[DocID]
           ,[RowID]
           ,[ISN_UNIT]
           ,[ISN_DOC_KIND]
           ,[UNIT_TYPE]
           ,[BASE]
           ,[FILM_TYPE]
           ,[FILM_FORMAT]
           ,[FOOTAGE]
           ,[START_DATE]
           ,[END_DATE]
           ,[PART_COUNT]
           ,[PLAYBACK_TIME]
           ,[SOUND]
           ,[FORMAT]
           ,[COLOR]
           ,[AUTHOR]
           ,[SHOOTING_PLACE]
           ,[PRODUCTION_PLACE]
           ,[NEGATIVE_COUNT]
           ,[NEGATIVE_LENGTH]
           ,[DUP_NEGATIVE_COUNT]
           ,[DUP_NEGATIVE_LENGTH]
           ,[PHONO_NEGATIVE_COUNT]
           ,[PHONO_NEGATIVE_LENGTH]
           ,[PHONO_MAG_COUNT]
           ,[PHONO_MAG_LENGTH]
           ,[PHONO_COMBINED_COUNT]
           ,[PHONO_COMBINED_LENGTH]
           ,[INTERPOSITIVE_COUNT]
           ,[INTERPOSITIVE_LENGTH]
           ,[POSITIVE_COUNT]
           ,[POSITIVE_LENGTH]
           ,[DUP_BACKUP_COUNT]
           ,[DUP_BACKUP_LENGTH]
           ,[INTERPOSITIVE_BACKUP_COUNT]
           ,[INTERPOSITIVE_BACKUP_LENGTH]
           ,[CINEX_COUNT]
           ,[COLOR_PASS_COUNT]
           ,[CARRIER_DESC]
           ,[ACCOMP_DOC]
           ,[UNIT_CNT])
     SELECT
           NEWID()
           ,@OwnerID
           ,GETDATE()
           ,(SELECT TOP 1 ID FROM tblUNIT WHERE tblUNIT.ISN_UNIT = ISN_UNIT)
           ,0
           ,[ISN_UNIT]
           ,[ISN_DOC_KIND]
           ,[UNIT_TYPE]
           ,[BASE]
           ,[FILM_TYPE]
           ,[FILM_FORMAT]
           ,[FOOTAGE]
           ,[START_DATE]
           ,[END_DATE]
           ,[PART_COUNT]
           ,[PLAYBACK_TIME]
           ,[SOUND]
           ,[FORMAT]
           ,[COLOR]
           ,[AUTHOR]
           ,[SHOOTING_PLACE]
           ,[PRODUCTION_PLACE]
           ,[NEGATIVE_COUNT]
           ,[NEGATIVE_LENGTH]
           ,[DUP_NEGATIVE_COUNT]
           ,[DUP_NEGATIVE_LENGTH]
           ,[PHONO_NEGATIVE_COUNT]
           ,[PHONO_NEGATIVE_LENGTH]
           ,[PHONO_MAG_COUNT]
           ,[PHONO_MAG_LENGTH]
           ,[PHONO_COMBINED_COUNT]
           ,[PHONO_COMBINED_LENGTH]
           ,[INTERPOSITIVE_COUNT]
           ,[INTERPOSITIVE_LENGTH]
           ,[POSITIVE_COUNT]
           ,[POSITIVE_LENGTH]
           ,[DUP_BACKUP_COUNT]
           ,[DUP_BACKUP_LENGTH]
           ,[INTERPOSITIVE_BACKUP_COUNT]
           ,[INTERPOSITIVE_BACKUP_LENGTH]
           ,[CINEX_COUNT]
           ,[COLOR_PASS_COUNT]
           ,[CARRIER_DESC]
           ,[ACCOMP_DOC]
           ,[UNIT_CNT]
     FROM [ROS_DB].[dbo].[UNIT_MOVIE]
GO
-- Specification
DECLARE @OwnerID uniqueidentifier
SELECT TOP 1 @OwnerID=ID FROM eqUsers WHERE Login='sa'
INSERT INTO [eqTest].[dbo].[tblUNIT_NTD]
           ([ID]
           ,[OwnerID]
           ,[CreationDateTime]
           ,[DocID]
           ,[RowID]
           ,[ISN_UNIT]
           ,[ISN_NTD_KIND]
           ,[COMPLEX_NUM_1]
           ,[COMPLEX_NUM_2]
           ,[DEV_STAGE]
           ,[AUTHOR]
           ,[ORGANIZ]
           ,[REQUEST_DATE]
           ,[REQUEST_NUM]
           ,[PATENT_NUM]
           ,[COPYRIGHT_NUM])
     SELECT
           NEWID()
           ,@OwnerID
           ,GETDATE()
           ,(SELECT TOP 1 ID FROM tblUNIT WHERE tblUNIT.ISN_UNIT = ISN_UNIT)
           ,0
           ,[ISN_UNIT]
           ,[ISN_NTD_KIND]
           ,[COMPLEX_NUM_1]
           ,[COMPLEX_NUM_2]
           ,[DEV_STAGE]
           ,[AUTHOR]
           ,[ORGANIZ]
           ,[REQUEST_DATE]
           ,[REQUEST_NUM]
           ,[PATENT_NUM]
           ,[COPYRIGHT_NUM]
     FROM [ROS_DB].[dbo].[UNIT_NTD]
GO
-- Specification
DECLARE @OwnerID uniqueidentifier
SELECT TOP 1 @OwnerID=ID FROM eqUsers WHERE Login='sa'
INSERT INTO [eqTest].[dbo].[tblUNIT_PHONO]
           ([ID]
           ,[OwnerID]
           ,[CreationDateTime]
           ,[DocID]
           ,[RowID]
           ,[ISN_UNIT]
           ,[ISN_DOC_KIND]
           ,[BASE]
           ,[REC_SPEED]
           ,[REC_DATE]
           ,[PHONO_TYPE]
           ,[LENGTH]
           ,[REC_PLACE]
           ,[PLAYBACK_TIME]
           ,[PRODUCTION_PLACE]
           ,[AUTHOR]
           ,[PERFORMER]
           ,[ORIGINAL_COUNT]
           ,[DISK_COUNT]
           ,[NEGATIVE_COUNT]
           ,[COPY_COUNT]
           ,[BACKUP_COUNT]
           ,[NEGATIVE_LENGTH]
           ,[COPY_LENGTH]
           ,[BACKUP_LENGTH]
           ,[UNIT_CNT]
           ,[CARRIER_DESC]
           ,[ACCOMP_DOC])
     SELECT
           NEWID()
           ,@OwnerID
           ,GETDATE()
           ,(SELECT TOP 1 ID FROM tblUNIT WHERE tblUNIT.ISN_UNIT = ISN_UNIT)
           ,0
           ,[ISN_UNIT]
           ,[ISN_DOC_KIND]
           ,[BASE]
           ,[REC_SPEED]
           ,[REC_DATE]
           ,[PHONO_TYPE]
           ,[LENGTH]
           ,[REC_PLACE]
           ,[PLAYBACK_TIME]
           ,[PRODUCTION_PLACE]
           ,[AUTHOR]
           ,[PERFORMER]
           ,[ORIGINAL_COUNT]
           ,[DISK_COUNT]
           ,[NEGATIVE_COUNT]
           ,[COPY_COUNT]
           ,[BACKUP_COUNT]
           ,[NEGATIVE_LENGTH]
           ,[COPY_LENGTH]
           ,[BACKUP_LENGTH]
           ,[UNIT_CNT]
           ,[CARRIER_DESC]
           ,[ACCOMP_DOC]
     FROM [ROS_DB].[dbo].[UNIT_PHONO]
GO
-- Specification
DECLARE @OwnerID uniqueidentifier
SELECT TOP 1 @OwnerID=ID FROM eqUsers WHERE Login='sa'
INSERT INTO [eqTest].[dbo].[tblUNIT_REQUIRED_WORK]
           ([ID]
           ,[OwnerID]
           ,[CreationDateTime]
           ,[DocID]
           ,[RowID]
           ,[ISN_WORK]
           ,[ISN_UNIT]
           ,[ISN_REF_ACT]
           ,[NOTE]
           ,[CHECK_DATE]
           ,[IS_ACTUAL])
     SELECT
           NEWID()
           ,@OwnerID
           ,GETDATE()
           ,(SELECT TOP 1 ID FROM tblUNIT WHERE tblUNIT.ISN_UNIT = ISN_UNIT)
           ,0
           ,[ISN_WORK]
           ,[ISN_UNIT]
           ,[ISN_REF_ACT]
           ,[NOTE]
           ,[CHECK_DATE]
           ,[IS_ACTUAL]
     FROM [ROS_DB].[dbo].[UNIT_REQUIRED_WORK]
GO
-- Specification
DECLARE @OwnerID uniqueidentifier
SELECT TOP 1 @OwnerID=ID FROM eqUsers WHERE Login='sa'
INSERT INTO [eqTest].[dbo].[tblUNIT_STATE]
           ([ID]
           ,[OwnerID]
           ,[CreationDateTime]
           ,[DocID]
           ,[RowID]
           ,[ISN_UNIT]
           ,[ISN_STATE]
           ,[ISN_REF_ACT]
           ,[PAGE_NUMS]
           ,[PAGE_COUNT]
           ,[STATE_DATE]
           ,[NOTE]
           ,[IS_ACTUAL])
     SELECT
           NEWID()
           ,@OwnerID
           ,GETDATE()
           ,(SELECT TOP 1 ID FROM tblUNIT WHERE tblUNIT.ISN_UNIT = ISN_UNIT)
           ,0
           ,[ISN_UNIT]
           ,[ISN_STATE]
           ,[ISN_REF_ACT]
           ,[PAGE_NUMS]
           ,[PAGE_COUNT]
           ,[STATE_DATE]
           ,[NOTE]
           ,[IS_ACTUAL]
     FROM [ROS_DB].[dbo].[UNIT_STATE]
GO
-- Specification
DECLARE @OwnerID uniqueidentifier
SELECT TOP 1 @OwnerID=ID FROM eqUsers WHERE Login='sa'
INSERT INTO [eqTest].[dbo].[tblUNIT_VIDEO]
           ([ID]
           ,[OwnerID]
           ,[CreationDateTime]
           ,[DocID]
           ,[RowID]
           ,[ISN_UNIT]
           ,[ISN_DOC_KIND]
           ,[REC_DATE]
           ,[REC_TYPE]
           ,[REC_FORMAT]
           ,[BASE]
           ,[ORIGINAL]
           ,[START_DATE]
           ,[END_DATE]
           ,[PART_COUNT]
           ,[PLAYBACK_TIME]
           ,[SOUND]
           ,[COLOR]
           ,[AUTHOR]
           ,[SHOOTING_PLACE]
           ,[PRODUCTION_PLACE]
           ,[ORIGINAL_COUNT]
           ,[COPY_COUNT]
           ,[BACKUP_COUNT]
           ,[UNIT_CNT]
           ,[CARRIER_DESC]
           ,[ACCOMP_DOC]
           ,[PERFORMER])
     SELECT
           NEWID()
           ,@OwnerID
           ,GETDATE()
           ,(SELECT TOP 1 ID FROM tblUNIT WHERE tblUNIT.ISN_UNIT = ISN_UNIT)
           ,0
           ,[ISN_UNIT]
           ,[ISN_DOC_KIND]
           ,[REC_DATE]
           ,[REC_TYPE]
           ,[REC_FORMAT]
           ,[BASE]
           ,[ORIGINAL]
           ,[START_DATE]
           ,[END_DATE]
           ,[PART_COUNT]
           ,[PLAYBACK_TIME]
           ,[SOUND]
           ,[COLOR]
           ,[AUTHOR]
           ,[SHOOTING_PLACE]
           ,[PRODUCTION_PLACE]
           ,[ORIGINAL_COUNT]
           ,[COPY_COUNT]
           ,[BACKUP_COUNT]
           ,[UNIT_CNT]
           ,[CARRIER_DESC]
           ,[ACCOMP_DOC]
           ,[PERFORMER]
     FROM [ROS_DB].[dbo].[UNIT_VIDEO]
GO
-- Specification
DECLARE @OwnerID uniqueidentifier
SELECT TOP 1 @OwnerID=ID FROM eqUsers WHERE Login='sa'
INSERT INTO [eqTest].[dbo].[tblUNIT_WORK]
           ([ID]
           ,[OwnerID]
           ,[CreationDateTime]
           ,[DocID]
           ,[RowID]
           ,[ISN_UNIT]
           ,[ISN_WORK]
           ,[WORK_DATE]
           ,[NOTE])
     SELECT
           NEWID()
           ,@OwnerID
           ,GETDATE()
           ,(SELECT TOP 1 ID FROM tblUNIT WHERE tblUNIT.ISN_UNIT = ISN_UNIT)
           ,0
           ,[ISN_UNIT]
           ,[ISN_WORK]
           ,[WORK_DATE]
           ,[NOTE]
     FROM [ROS_DB].[dbo].[UNIT_WORK]
GO
