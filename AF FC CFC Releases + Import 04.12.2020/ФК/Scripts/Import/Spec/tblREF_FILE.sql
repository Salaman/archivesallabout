﻿-- Delete Specification
DELETE FROM [eqTest].[dbo].[tblREF_FILE]
-- Specification
SET IDENTITY_INSERT [eqTest].[dbo].[tblREF_FILE] ON
GO
DECLARE @OwnerID uniqueidentifier
SELECT TOP 1 @OwnerID=ID FROM eqUsers WHERE Login='sa'
INSERT INTO [eqTest].[dbo].[tblREF_FILE]
           ([ID]
           ,[OwnerID]
           ,[CreationDateTime]
           ,[DocID]
           ,[RowID]
           ,[ISN_REF_FILE]
           ,[ISN_OBJ]
           ,[KIND]
           ,[GR_STORAGE]
           ,[NAME]
           ,[CATEGORY]
           ,[WEIGHT])
     SELECT
           NEWID()
           ,@OwnerID
           ,GETDATE()
           ,CASE KIND
              WHEN 703 THEN (SELECT TOP 1 ID FROM tblUNIT WHERE tblUNIT.ISN_UNIT = ISN_OBJ)
              ELSE NULL
            END
           ,0
           ,[ISN_REF_FILE]
           ,[ISN_OBJ]
           ,[KIND]
           ,[GR_STORAGE]
           ,[NAME]
           ,[CATEGORY]
           ,[WEIGHT]
     FROM [ROS_DB].[dbo].[REF_FILE]
GO
SET IDENTITY_INSERT [eqTest].[dbo].[tblREF_FILE] OFF
GO
