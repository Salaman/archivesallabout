﻿SET IDENTITY_INSERT [eqTest].[dbo].[tblSECURITY_REASON] ON
GO
DECLARE @OwnerID uniqueidentifier
DECLARE @StatusID uniqueidentifier
SELECT TOP 1 @OwnerID=ID FROM eqUsers WHERE Login='sa'
SELECT @StatusID=ID FROM eqDocStates WHERE StateName='Сохранен' AND DocTypeID=(SELECT ID FROM eqDocTypes WHERE DocType='Причины ограничения доступа')
INSERT INTO [eqTest].[dbo].[tblSECURITY_REASON]
           ([ID]
           ,[OwnerID]
           ,[CreationDateTime]
           ,[StatusID]
           ,[Deleted]
           ,[Isn_security_reason]
           ,[Code]
           ,[Name]
           ,[Note]
           ,[Protected]
           ,[Weight])
     SELECT
           NEWID()
           ,@OwnerID
           ,GETDATE()
           ,@StatusID
           ,CASE [Deleted] WHEN 'Y' THEN 1 ELSE 0 END
           ,[Isn_security_reason]
           ,[Code]
           ,[Name]
           ,[Note]
           ,[Protected]
           ,[Weight]
     FROM [ROS_DB].[dbo].[SECURITY_REASON]
GO
SET IDENTITY_INSERT [eqTest].[dbo].[tblSECURITY_REASON] OFF
GO
