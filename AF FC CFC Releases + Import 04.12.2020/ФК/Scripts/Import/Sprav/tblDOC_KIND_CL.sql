﻿SET IDENTITY_INSERT [eqTest].[dbo].[tblDOC_KIND_CL] ON
GO
DECLARE @OwnerID uniqueidentifier
DECLARE @StatusID uniqueidentifier
SELECT TOP 1 @OwnerID=ID FROM eqUsers WHERE Login='sa'
SELECT @StatusID=ID FROM eqDocStates WHERE StateName='Сохранен' AND DocTypeID=(SELECT ID FROM eqDocTypes WHERE DocType='Видовой состав')
INSERT INTO [eqTest].[dbo].[tblDOC_KIND_CL]
           ([ID]
           ,[OwnerID]
           ,[CreationDateTime]
           ,[StatusID]
           ,[Deleted]
           ,[ISN_DOC_KIND]
           ,[ISN_HIGH_DOC_KIND]
           ,[CODE]
           ,[NAME]
           ,[FOREST_ELEM]
           ,[NOTE]
           ,[PROTECTED]
           ,[WEIGHT])
     SELECT
           NEWID()
           ,@OwnerID
           ,GETDATE()
           ,@StatusID
           ,CASE [Deleted] WHEN 'Y' THEN 1 ELSE 0 END
           ,[ISN_DOC_KIND]
           ,[ISN_HIGH_DOC_KIND]
           ,[CODE]
           ,[NAME]
           ,[FOREST_ELEM]
           ,[NOTE]
           ,[PROTECTED]
           ,[WEIGHT]
     FROM [ROS_DB].[dbo].[DOC_KIND_CL]
GO
SET IDENTITY_INSERT [eqTest].[dbo].[tblDOC_KIND_CL] OFF
GO
