﻿SET IDENTITY_INSERT [eqTest].[dbo].[tblCLS] ON
GO
/*
701 Межфондовые классификаторы
702 Внутрифондовые классификаторы
703 Классификаторы внутри описи
*/
DECLARE @OwnerID uniqueidentifier
DECLARE @StatusID1 uniqueidentifier
DECLARE @StatusID2 uniqueidentifier
DECLARE @StatusID3 uniqueidentifier
SELECT TOP 1 @OwnerID=ID FROM eqUsers WHERE Login='sa'
SELECT @StatusID1=ID FROM eqDocStates WHERE StateName='Сохранен' AND DocTypeID=(SELECT ID FROM eqDocTypes WHERE DocType='Межфондовые классификаторы')
SELECT @StatusID2=ID FROM eqDocStates WHERE StateName='Сохранен' AND DocTypeID=(SELECT ID FROM eqDocTypes WHERE DocType='Внутрифондовые классификаторы')
SELECT @StatusID3=ID FROM eqDocStates WHERE StateName='Сохранен' AND DocTypeID=(SELECT ID FROM eqDocTypes WHERE DocType='Классификаторы внутри описи')
INSERT INTO [eqTest].[dbo].[tblCLS]
           ([ID]
           ,[OwnerID]
           ,[CreationDateTime]
           ,[StatusID]
           ,[Deleted]
           ,[ISN_CLS]
           ,[ISN_HIGH_CLS]
           ,[CODE]
           ,[WEIGHT]
           ,[NAME]
           ,[OBJ_KIND]
           ,[MULTISELECT]
           ,[NOTE]
           ,[FOREST_ELEM]
           ,[PROTECTED])
     SELECT
           NEWID()
           ,@OwnerID
           ,GETDATE()
           ,CASE [OBJ_KIND] WHEN 701 THEN @StatusID1 WHEN 702 THEN @StatusID2 ELSE @StatusID3 END
           ,CASE [Deleted] WHEN 'Y' THEN 1 ELSE 0 END
		   ,[ISN_CLS]
		   ,[ISN_HIGH_CLS]
		   ,[CODE]
		   ,[WEIGHT]
		   ,[NAME]
		   ,[OBJ_KIND]
		   ,[MULTISELECT]
		   ,[NOTE]
		   ,[FOREST_ELEM]
		   ,[PROTECTED]
     FROM [ROS_DB].[dbo].[CLS]
GO
SET IDENTITY_INSERT [eqTest].[dbo].[tblCLS] OFF
GO
