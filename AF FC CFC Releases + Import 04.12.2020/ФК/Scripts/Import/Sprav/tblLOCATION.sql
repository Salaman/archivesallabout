﻿SET IDENTITY_INSERT [eqTest].[dbo].[tblLOCATION] ON
GO
DECLARE @OwnerID uniqueidentifier
DECLARE @StatusID uniqueidentifier
SELECT TOP 1 @OwnerID=ID FROM eqUsers WHERE Login='sa'
SELECT @StatusID=ID FROM eqDocStates WHERE StateName='Сохранен' AND DocTypeID=(SELECT ID FROM eqDocTypes WHERE DocType='Архивохранилище')
INSERT INTO [eqTest].[dbo].[tblLOCATION]
           ([ID]
           ,[OwnerID]
           ,[CreationDateTime]
           ,[StatusID]
           ,[Deleted]
		   ,[ISN_LOCATION]
		   ,[ISN_HIGH_LOCATION]
		   ,[ISN_ARCHIVE]
		   ,[CODE]
		   ,[NAME]
		   ,[NOTE]
		   ,[FOREST_ELEM]
		   ,[PROTECTED]
		   ,[WEIGHT])
     SELECT
           NEWID()
           ,@OwnerID
           ,GETDATE()
           ,@StatusID
           ,CASE [Deleted] WHEN 'Y' THEN 1 ELSE 0 END
		   ,[ISN_LOCATION]
		   ,[ISN_HIGH_LOCATION]
		   ,[ISN_ARCHIVE]
		   ,[CODE]
		   ,[NAME]
		   ,[NOTE]
		   ,[FOREST_ELEM]
		   ,[PROTECTED]
		   ,[WEIGHT]
     FROM [ROS_DB].[dbo].[LOCATION]
GO
SET IDENTITY_INSERT [eqTest].[dbo].[tblLOCATION] OFF
GO
