﻿-- Spravochn
CREATE TABLE [dbo].[tblCLS](
	ID uniqueidentifier NOT NULL,
	OwnerID uniqueidentifier NOT NULL,
	CreationDateTime datetime NOT NULL,
	StatusID uniqueidentifier NOT NULL,
	Deleted bit NOT NULL,
	[ISN_CLS] [int] NOT NULL,
	[ISN_HIGH_CLS] [int] NULL,
	[CODE] [varchar](20) NULL,
	[WEIGHT] [int] NULL,
	[NAME] [varchar](300) NULL,
	[OBJ_KIND] [smallint] NULL,
	[MULTISELECT] [varchar](1) NULL,
	[NOTE] [varchar](max) NULL,
	[FOREST_ELEM] [varchar](1) NULL,
	[PROTECTED] [varchar](1) NULL)
GO
