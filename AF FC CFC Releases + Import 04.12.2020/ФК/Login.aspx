﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Login.aspx.vb"
    Inherits="WebApplication.Login" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ContentPlaceHolderID="Body" runat="server">
    <asp:Panel runat="server" DefaultButton="ButtonLogin">
        <table width="100%">
            <tr>
                <td align="center">
                    <asp:Panel ID="PanelLoginPadding" runat="server" />
                    <table style="width: 300px;" class="LogicBlockTable">
                        <colgroup>
                            <col style="width: 150px;" />
                            <col style="width: 150px;" />
                        </colgroup>
                        <tr>
                            <td>
                                <asp:Localize ID="LocalizeUser" runat="server" Text="<%$ Resources:eqResources, CAP_User %>" />
                            </td>
                            <td>
                                <eq:eqTextBox ID="TextBoxLogin" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Localize ID="LocalizePassword" runat="server" Text="<%$ Resources:eqResources, CAP_Password %>" />
                            </td>
                            <td>
                                <eq:eqTextBox ID="TextBoxPassword" runat="server" TextMode="Password" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:Button ID="ButtonLogin" runat="server" Width="100%" Text="<%$ Resources:eqResources, CAP_In %>" />
                            </td>
                        </tr>
                    </table>
                    <asp:Panel ID="PanelMessagesPadding" runat="server" Height="10px" />
                    <asp:Panel ID="PanelMessagesContent" runat="server" Style="width: 90%;">
                        <asp:PlaceHolder ID="PlaceHolderMessages" runat="server" />
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
