﻿
var ADAPT = {};

ADAPT.DOC_P = '_DOCUMENT_STATS_P';
ADAPT.DOC_A = '_DOCUMENT_STATS_A';
ADAPT.DOC_E = '_DOCUMENT_STATS_E';
ADAPT.DOC_M = '_DOCUMENT_STATS_M';
ADAPT.DOC_TOTAL = '_DOCUMENT_STATS';

ADAPT.TYPE_PHOTO = 'Фотодокументы';
ADAPT.TYPE_PHONO = 'Фонодокументы';
ADAPT.TYPE_MOVIE = 'Кинодокументы';
ADAPT.TYPE_VIDEO = 'Видеодокументы';
ADAPT.TYPE_UD = 'Управленческая документация';
ADAPT.TYPE_LS = 'личному составу';
ADAPT.TYPE_LP = 'личного происхождения';
ADAPT.TYPE_NTD = 'НТД';
ADAPT.TYPE_MICRO = 'Микроформы';
ADAPT.TYPE_TOTAL = 'Всего';

ADAPT.UpdateStats = function(args) {

    var field = function(table, row, rowIndex, col) {
        var container = $('table[id$=' + table + '] tr[innerHTML*=' + row + '] td');
        var control = (rowIndex == 0 ? $(':input:even', container)[col] : (rowIndex == 1 ? $(':input:odd', container)[col] : $(':input', container)[col]));
        return $(control);
    };

    var disable = function(index) {

        //UD
        field(ADAPT.DOC_P, ADAPT.TYPE_UD, null, 0).prop('disabled', index != 1);
        //field(ADAPT.DOC_P, ADAPT.TYPE_UD, null, 1).prop('disabled', index != 1);
        //field(ADAPT.DOC_P, ADAPT.TYPE_UD, null, 2).prop('disabled', index != 1);
        field(ADAPT.DOC_P, ADAPT.TYPE_UD, null, 3).prop('disabled', index != 1);
        field(ADAPT.DOC_P, ADAPT.TYPE_UD, null, 4).prop('disabled', index != 1);
        field(ADAPT.DOC_P, ADAPT.TYPE_UD, null, 5).prop('disabled', index != 1);
        field(ADAPT.DOC_P, ADAPT.TYPE_UD, null, 6).prop('disabled', index != 1);
        field(ADAPT.DOC_P, ADAPT.TYPE_UD, null, 7).prop('disabled', index != 1);
        field(ADAPT.DOC_P, ADAPT.TYPE_UD, null, 8).prop('disabled', index != 1);
        field(ADAPT.DOC_P, ADAPT.TYPE_UD, null, 9).prop('disabled', index != 1);
        field(ADAPT.DOC_P, ADAPT.TYPE_UD, null, 10).prop('disabled', index != 1);

        //LS
        field(ADAPT.DOC_P, ADAPT.TYPE_LS, null, 0).prop('disabled', index != 2);
        //field(ADAPT.DOC_P, ADAPT.TYPE_LS, null, 1).prop('disabled', index != 2);
        //field(ADAPT.DOC_P, ADAPT.TYPE_LS, null, 2).prop('disabled', index != 2);
        field(ADAPT.DOC_P, ADAPT.TYPE_LS, null, 3).prop('disabled', index != 2);
        //field(ADAPT.DOC_P, ADAPT.TYPE_LS, null, 4).prop('disabled', index != 2);
        //field(ADAPT.DOC_P, ADAPT.TYPE_LS, null, 5).prop('disabled', index != 2);
        //field(ADAPT.DOC_P, ADAPT.TYPE_LS, null, 6).prop('disabled', index != 2);
        field(ADAPT.DOC_P, ADAPT.TYPE_LS, null, 7).prop('disabled', index != 2);
        field(ADAPT.DOC_P, ADAPT.TYPE_LS, null, 8).prop('disabled', index != 2);
        field(ADAPT.DOC_P, ADAPT.TYPE_LS, null, 9).prop('disabled', index != 2);
        field(ADAPT.DOC_P, ADAPT.TYPE_LS, null, 10).prop('disabled', index != 2);

        //LP
        field(ADAPT.DOC_P, ADAPT.TYPE_LP, null, 0).prop('disabled', index != 3);
        //field(ADAPT.DOC_P, ADAPT.TYPE_LP, null, 1).prop('disabled', index != 3);
        //field(ADAPT.DOC_P, ADAPT.TYPE_LP, null, 2).prop('disabled', index != 3);
        field(ADAPT.DOC_P, ADAPT.TYPE_LP, null, 3).prop('disabled', index != 3);
        field(ADAPT.DOC_P, ADAPT.TYPE_LP, null, 4).prop('disabled', index != 3);
        field(ADAPT.DOC_P, ADAPT.TYPE_LP, null, 5).prop('disabled', index != 3);
        field(ADAPT.DOC_P, ADAPT.TYPE_LP, null, 6).prop('disabled', index != 3);
        field(ADAPT.DOC_P, ADAPT.TYPE_LP, null, 7).prop('disabled', index != 3);
        field(ADAPT.DOC_P, ADAPT.TYPE_LP, null, 8).prop('disabled', index != 3);
        //field(ADAPT.DOC_P, ADAPT.TYPE_LP, null, 9).prop('disabled', index != 3);
        field(ADAPT.DOC_P, ADAPT.TYPE_LP, null, 10).prop('disabled', index != 3);

        //NTD
        field(ADAPT.DOC_P, ADAPT.TYPE_NTD, null, 0).prop('disabled', index != 4);
        //field(ADAPT.DOC_P, ADAPT.TYPE_NTD, null, 1).prop('disabled', index != 4);
        //field(ADAPT.DOC_P, ADAPT.TYPE_NTD, null, 2).prop('disabled', index != 4);
        field(ADAPT.DOC_P, ADAPT.TYPE_NTD, null, 3).prop('disabled', index != 4);
        field(ADAPT.DOC_P, ADAPT.TYPE_NTD, null, 4).prop('disabled', index != 4);
        field(ADAPT.DOC_P, ADAPT.TYPE_NTD, null, 5).prop('disabled', index != 4);
        field(ADAPT.DOC_P, ADAPT.TYPE_NTD, null, 6).prop('disabled', index != 4);
        field(ADAPT.DOC_P, ADAPT.TYPE_NTD, null, 7).prop('disabled', index != 4);
        field(ADAPT.DOC_P, ADAPT.TYPE_NTD, null, 8).prop('disabled', index != 4);
        field(ADAPT.DOC_P, ADAPT.TYPE_NTD, null, 9).prop('disabled', index != 4);
        field(ADAPT.DOC_P, ADAPT.TYPE_NTD, null, 10).prop('disabled', index != 4);

        //PHOTO
        field(ADAPT.DOC_A, ADAPT.TYPE_PHOTO, 0, 0).prop('disabled', index != 5);
        //field(ADAPT.DOC_A, ADAPT.TYPE_PHOTO, 0, 1).prop('disabled', index != 5);
        //field(ADAPT.DOC_A, ADAPT.TYPE_PHOTO, 0, 2).prop('disabled', index != 5);
        field(ADAPT.DOC_A, ADAPT.TYPE_PHOTO, 0, 3).prop('disabled', index != 5);
        field(ADAPT.DOC_A, ADAPT.TYPE_PHOTO, 0, 4).prop('disabled', index != 5);
        field(ADAPT.DOC_A, ADAPT.TYPE_PHOTO, 0, 5).prop('disabled', index != 5);
        field(ADAPT.DOC_A, ADAPT.TYPE_PHOTO, 0, 6).prop('disabled', index != 5);
        field(ADAPT.DOC_A, ADAPT.TYPE_PHOTO, 0, 7).prop('disabled', index != 5);
        field(ADAPT.DOC_A, ADAPT.TYPE_PHOTO, 0, 8).prop('disabled', index != 5);
        field(ADAPT.DOC_A, ADAPT.TYPE_PHOTO, 0, 9).prop('disabled', index != 5);
        field(ADAPT.DOC_A, ADAPT.TYPE_PHOTO, 0, 10).prop('disabled', index != 5);

        //PHONO
        field(ADAPT.DOC_A, ADAPT.TYPE_PHONO, 0, 0).prop('disabled', index != 6);
        //field(ADAPT.DOC_A, ADAPT.TYPE_PHONO, 0, 1).prop('disabled', index != 6);
        //field(ADAPT.DOC_A, ADAPT.TYPE_PHONO, 0, 2).prop('disabled', index != 6);
        field(ADAPT.DOC_A, ADAPT.TYPE_PHONO, 0, 3).prop('disabled', index != 6);
        field(ADAPT.DOC_A, ADAPT.TYPE_PHONO, 0, 4).prop('disabled', index != 6);
        field(ADAPT.DOC_A, ADAPT.TYPE_PHONO, 0, 5).prop('disabled', index != 6);
        field(ADAPT.DOC_A, ADAPT.TYPE_PHONO, 0, 6).prop('disabled', index != 6);
        field(ADAPT.DOC_A, ADAPT.TYPE_PHONO, 0, 7).prop('disabled', index != 6);
        field(ADAPT.DOC_A, ADAPT.TYPE_PHONO, 0, 8).prop('disabled', index != 6);
        field(ADAPT.DOC_A, ADAPT.TYPE_PHONO, 0, 9).prop('disabled', index != 6);
        //field(ADAPT.DOC_A, ADAPT.TYPE_PHONO, 0, 10).prop('disabled', index != 6);

        field(ADAPT.DOC_A, ADAPT.TYPE_PHONO, 1, 0).prop('disabled', index != 6);
        //field(ADAPT.DOC_A, ADAPT.TYPE_PHONO, 1, 1).prop('disabled', index != 6);
        //field(ADAPT.DOC_A, ADAPT.TYPE_PHONO, 1, 2).prop('disabled', index != 6);
        field(ADAPT.DOC_A, ADAPT.TYPE_PHONO, 1, 3).prop('disabled', index != 6);
        field(ADAPT.DOC_A, ADAPT.TYPE_PHONO, 1, 4).prop('disabled', index != 6);
        field(ADAPT.DOC_A, ADAPT.TYPE_PHONO, 1, 5).prop('disabled', index != 6);
        field(ADAPT.DOC_A, ADAPT.TYPE_PHONO, 1, 6).prop('disabled', index != 6);
        field(ADAPT.DOC_A, ADAPT.TYPE_PHONO, 1, 7).prop('disabled', index != 6);
        field(ADAPT.DOC_A, ADAPT.TYPE_PHONO, 1, 8).prop('disabled', index != 6);
        field(ADAPT.DOC_A, ADAPT.TYPE_PHONO, 1, 9).prop('disabled', index != 6);
        field(ADAPT.DOC_A, ADAPT.TYPE_PHONO, 1, 10).prop('disabled', index != 6);

        //MOVIE
        field(ADAPT.DOC_A, ADAPT.TYPE_MOVIE, 0, 0).prop('disabled', index != 7);
        //field(ADAPT.DOC_A, ADAPT.TYPE_MOVIE, 0, 1).prop('disabled', index != 7);
        //field(ADAPT.DOC_A, ADAPT.TYPE_MOVIE, 0, 2).prop('disabled', index != 7);
        field(ADAPT.DOC_A, ADAPT.TYPE_MOVIE, 0, 3).prop('disabled', index != 7);
        field(ADAPT.DOC_A, ADAPT.TYPE_MOVIE, 0, 4).prop('disabled', index != 7);
        field(ADAPT.DOC_A, ADAPT.TYPE_MOVIE, 0, 5).prop('disabled', index != 7);
        field(ADAPT.DOC_A, ADAPT.TYPE_MOVIE, 0, 6).prop('disabled', index != 7);
        field(ADAPT.DOC_A, ADAPT.TYPE_MOVIE, 0, 7).prop('disabled', index != 7);
        field(ADAPT.DOC_A, ADAPT.TYPE_MOVIE, 0, 8).prop('disabled', index != 7);
        field(ADAPT.DOC_A, ADAPT.TYPE_MOVIE, 0, 9).prop('disabled', index != 7);
        //field(ADAPT.DOC_A, ADAPT.TYPE_MOVIE, 0, 10).prop('disabled', index != 7);

        field(ADAPT.DOC_A, ADAPT.TYPE_MOVIE, 1, 0).prop('disabled', index != 7);
        //field(ADAPT.DOC_A, ADAPT.TYPE_MOVIE, 1, 1).prop('disabled', index != 7);
        //field(ADAPT.DOC_A, ADAPT.TYPE_MOVIE, 1, 2).prop('disabled', index != 7);
        field(ADAPT.DOC_A, ADAPT.TYPE_MOVIE, 1, 3).prop('disabled', index != 7);
        field(ADAPT.DOC_A, ADAPT.TYPE_MOVIE, 1, 4).prop('disabled', index != 7);
        field(ADAPT.DOC_A, ADAPT.TYPE_MOVIE, 1, 5).prop('disabled', index != 7);
        field(ADAPT.DOC_A, ADAPT.TYPE_MOVIE, 1, 6).prop('disabled', index != 7);
        field(ADAPT.DOC_A, ADAPT.TYPE_MOVIE, 1, 7).prop('disabled', index != 7);
        field(ADAPT.DOC_A, ADAPT.TYPE_MOVIE, 1, 8).prop('disabled', index != 7);
        field(ADAPT.DOC_A, ADAPT.TYPE_MOVIE, 1, 9).prop('disabled', index != 7);
        field(ADAPT.DOC_A, ADAPT.TYPE_MOVIE, 1, 10).prop('disabled', index != 7);

        //VIDEO
        field(ADAPT.DOC_A, ADAPT.TYPE_VIDEO, 0, 0).prop('disabled', index != 8);
        //field(ADAPT.DOC_A, ADAPT.TYPE_VIDEO, 0, 1).prop('disabled', index != 8);
        //field(ADAPT.DOC_A, ADAPT.TYPE_VIDEO, 0, 2).prop('disabled', index != 8);
        field(ADAPT.DOC_A, ADAPT.TYPE_VIDEO, 0, 3).prop('disabled', index != 8);
        field(ADAPT.DOC_A, ADAPT.TYPE_VIDEO, 0, 4).prop('disabled', index != 8);
        field(ADAPT.DOC_A, ADAPT.TYPE_VIDEO, 0, 5).prop('disabled', index != 8);
        field(ADAPT.DOC_A, ADAPT.TYPE_VIDEO, 0, 6).prop('disabled', index != 8);
        field(ADAPT.DOC_A, ADAPT.TYPE_VIDEO, 0, 7).prop('disabled', index != 8);
        field(ADAPT.DOC_A, ADAPT.TYPE_VIDEO, 0, 8).prop('disabled', index != 8);
        field(ADAPT.DOC_A, ADAPT.TYPE_VIDEO, 0, 9).prop('disabled', index != 8);
        //field(ADAPT.DOC_A, ADAPT.TYPE_VIDEO, 0, 10).prop('disabled', index != 8);

        field(ADAPT.DOC_A, ADAPT.TYPE_VIDEO, 1, 0).prop('disabled', index != 8);
        //field(ADAPT.DOC_A, ADAPT.TYPE_VIDEO, 1, 1).prop('disabled', index != 8);
        //field(ADAPT.DOC_A, ADAPT.TYPE_VIDEO, 1, 2).prop('disabled', index != 8);
        field(ADAPT.DOC_A, ADAPT.TYPE_VIDEO, 1, 3).prop('disabled', index != 8);
        field(ADAPT.DOC_A, ADAPT.TYPE_VIDEO, 1, 4).prop('disabled', index != 8);
        field(ADAPT.DOC_A, ADAPT.TYPE_VIDEO, 1, 5).prop('disabled', index != 8);
        field(ADAPT.DOC_A, ADAPT.TYPE_VIDEO, 1, 6).prop('disabled', index != 8);
        field(ADAPT.DOC_A, ADAPT.TYPE_VIDEO, 1, 7).prop('disabled', index != 8);
        field(ADAPT.DOC_A, ADAPT.TYPE_VIDEO, 1, 8).prop('disabled', index != 8);
        field(ADAPT.DOC_A, ADAPT.TYPE_VIDEO, 1, 9).prop('disabled', index != 8);
        field(ADAPT.DOC_A, ADAPT.TYPE_VIDEO, 1, 10).prop('disabled', index != 8);

        //MICRO
        field(ADAPT.DOC_A, ADAPT.TYPE_MICRO, null, 0).prop('disabled', index != 9);
        //field(ADAPT.DOC_A, ADAPT.TYPE_MICRO, null, 1).prop('disabled', index != 9);
        //field(ADAPT.DOC_A, ADAPT.TYPE_MICRO, null, 2).prop('disabled', index != 9);
        field(ADAPT.DOC_A, ADAPT.TYPE_MICRO, null, 3).prop('disabled', index != 9);
        field(ADAPT.DOC_A, ADAPT.TYPE_MICRO, null, 4).prop('disabled', index != 9);
        field(ADAPT.DOC_A, ADAPT.TYPE_MICRO, null, 5).prop('disabled', index != 9);
        field(ADAPT.DOC_A, ADAPT.TYPE_MICRO, null, 6).prop('disabled', index != 9);
        field(ADAPT.DOC_A, ADAPT.TYPE_MICRO, null, 7).prop('disabled', index != 9);
        field(ADAPT.DOC_A, ADAPT.TYPE_MICRO, null, 8).prop('disabled', index != 9);
        //field(ADAPT.DOC_A, ADAPT.TYPE_MICRO, null, 9).prop('disabled', index != 9);
        field(ADAPT.DOC_A, ADAPT.TYPE_MICRO, null, 10).prop('disabled', index != 9);

        //NTD
        field(ADAPT.DOC_E, ADAPT.TYPE_NTD, 0, 0).prop('disabled', index != 4);
        //field(ADAPT.DOC_E, ADAPT.TYPE_NTD, 0, 1).prop('disabled', index != 4);
        //field(ADAPT.DOC_E, ADAPT.TYPE_NTD, 0, 2).prop('disabled', index != 4);
        field(ADAPT.DOC_E, ADAPT.TYPE_NTD, 0, 3).prop('disabled', index != 4);
        field(ADAPT.DOC_E, ADAPT.TYPE_NTD, 0, 4).prop('disabled', index != 4);
        field(ADAPT.DOC_E, ADAPT.TYPE_NTD, 0, 5).prop('disabled', index != 4);
        field(ADAPT.DOC_E, ADAPT.TYPE_NTD, 0, 6).prop('disabled', index != 4);
        field(ADAPT.DOC_E, ADAPT.TYPE_NTD, 0, 7).prop('disabled', index != 4);
        field(ADAPT.DOC_E, ADAPT.TYPE_NTD, 0, 8).prop('disabled', index != 4);
        field(ADAPT.DOC_E, ADAPT.TYPE_NTD, 0, 9).prop('disabled', index != 4);
        field(ADAPT.DOC_E, ADAPT.TYPE_NTD, 0, 10).prop('disabled', index != 4);

        field(ADAPT.DOC_E, ADAPT.TYPE_NTD, 1, 0).prop('disabled', index != 4);
        //field(ADAPT.DOC_E, ADAPT.TYPE_NTD, 1, 1).prop('disabled', index != 4);
        //field(ADAPT.DOC_E, ADAPT.TYPE_NTD, 1, 2).prop('disabled', index != 4);
        field(ADAPT.DOC_E, ADAPT.TYPE_NTD, 1, 3).prop('disabled', index != 4);
        field(ADAPT.DOC_E, ADAPT.TYPE_NTD, 1, 4).prop('disabled', index != 4);
        field(ADAPT.DOC_E, ADAPT.TYPE_NTD, 1, 5).prop('disabled', index != 4);
        field(ADAPT.DOC_E, ADAPT.TYPE_NTD, 1, 6).prop('disabled', index != 4);
        field(ADAPT.DOC_E, ADAPT.TYPE_NTD, 1, 7).prop('disabled', index != 4);
        field(ADAPT.DOC_E, ADAPT.TYPE_NTD, 1, 8).prop('disabled', index != 4);
        field(ADAPT.DOC_E, ADAPT.TYPE_NTD, 1, 9).prop('disabled', index != 4);
        field(ADAPT.DOC_E, ADAPT.TYPE_NTD, 1, 10).prop('disabled', index != 4);

        //PHOTO
        field(ADAPT.DOC_E, ADAPT.TYPE_PHOTO, 0, 0).prop('disabled', index != 5);
        //field(ADAPT.DOC_E, ADAPT.TYPE_PHOTO, 0, 1).prop('disabled', index != 5);
        //field(ADAPT.DOC_E, ADAPT.TYPE_PHOTO, 0, 2).prop('disabled', index != 5);
        field(ADAPT.DOC_E, ADAPT.TYPE_PHOTO, 0, 3).prop('disabled', index != 5);
        field(ADAPT.DOC_E, ADAPT.TYPE_PHOTO, 0, 4).prop('disabled', index != 5);
        field(ADAPT.DOC_E, ADAPT.TYPE_PHOTO, 0, 5).prop('disabled', index != 5);
        field(ADAPT.DOC_E, ADAPT.TYPE_PHOTO, 0, 6).prop('disabled', index != 5);
        field(ADAPT.DOC_E, ADAPT.TYPE_PHOTO, 0, 7).prop('disabled', index != 5);
        field(ADAPT.DOC_E, ADAPT.TYPE_PHOTO, 0, 8).prop('disabled', index != 5);
        field(ADAPT.DOC_E, ADAPT.TYPE_PHOTO, 0, 9).prop('disabled', index != 5);
        //field(ADAPT.DOC_E, ADAPT.TYPE_PHOTO, 0, 10).prop('disabled', index != 5);        

        field(ADAPT.DOC_E, ADAPT.TYPE_PHOTO, 1, 0).prop('disabled', index != 5);
        //field(ADAPT.DOC_E, ADAPT.TYPE_PHOTO, 1, 1).prop('disabled', index != 5);
        //field(ADAPT.DOC_E, ADAPT.TYPE_PHOTO, 1, 2).prop('disabled', index != 5);
        field(ADAPT.DOC_E, ADAPT.TYPE_PHOTO, 1, 3).prop('disabled', index != 5);
        field(ADAPT.DOC_E, ADAPT.TYPE_PHOTO, 1, 4).prop('disabled', index != 5);
        field(ADAPT.DOC_E, ADAPT.TYPE_PHOTO, 1, 5).prop('disabled', index != 5);
        field(ADAPT.DOC_E, ADAPT.TYPE_PHOTO, 1, 6).prop('disabled', index != 5);
        field(ADAPT.DOC_E, ADAPT.TYPE_PHOTO, 1, 7).prop('disabled', index != 5);
        field(ADAPT.DOC_E, ADAPT.TYPE_PHOTO, 1, 8).prop('disabled', index != 5);
        field(ADAPT.DOC_E, ADAPT.TYPE_PHOTO, 1, 9).prop('disabled', index != 5);
        field(ADAPT.DOC_E, ADAPT.TYPE_PHOTO, 1, 10).prop('disabled', index != 5);

        //PHONO
        field(ADAPT.DOC_E, ADAPT.TYPE_PHONO, 0, 0).prop('disabled', index != 6);
        //field(ADAPT.DOC_E, ADAPT.TYPE_PHONO, 0, 1).prop('disabled', index != 6);
        //field(ADAPT.DOC_E, ADAPT.TYPE_PHONO, 0, 2).prop('disabled', index != 6);
        field(ADAPT.DOC_E, ADAPT.TYPE_PHONO, 0, 3).prop('disabled', index != 6);
        field(ADAPT.DOC_E, ADAPT.TYPE_PHONO, 0, 4).prop('disabled', index != 6);
        field(ADAPT.DOC_E, ADAPT.TYPE_PHONO, 0, 5).prop('disabled', index != 6);
        field(ADAPT.DOC_E, ADAPT.TYPE_PHONO, 0, 6).prop('disabled', index != 6);
        field(ADAPT.DOC_E, ADAPT.TYPE_PHONO, 0, 7).prop('disabled', index != 6);
        field(ADAPT.DOC_E, ADAPT.TYPE_PHONO, 0, 8).prop('disabled', index != 6);
        field(ADAPT.DOC_E, ADAPT.TYPE_PHONO, 0, 9).prop('disabled', index != 6);
        field(ADAPT.DOC_E, ADAPT.TYPE_PHONO, 0, 10).prop('disabled', index != 6);

        field(ADAPT.DOC_E, ADAPT.TYPE_PHONO, 1, 0).prop('disabled', index != 6);
        //field(ADAPT.DOC_E, ADAPT.TYPE_PHONO, 1, 1).prop('disabled', index != 6);
        //field(ADAPT.DOC_E, ADAPT.TYPE_PHONO, 1, 2).prop('disabled', index != 6);
        field(ADAPT.DOC_E, ADAPT.TYPE_PHONO, 1, 3).prop('disabled', index != 6);
        field(ADAPT.DOC_E, ADAPT.TYPE_PHONO, 1, 4).prop('disabled', index != 6);
        field(ADAPT.DOC_E, ADAPT.TYPE_PHONO, 1, 5).prop('disabled', index != 6);
        field(ADAPT.DOC_E, ADAPT.TYPE_PHONO, 1, 6).prop('disabled', index != 6);
        field(ADAPT.DOC_E, ADAPT.TYPE_PHONO, 1, 7).prop('disabled', index != 6);
        field(ADAPT.DOC_E, ADAPT.TYPE_PHONO, 1, 8).prop('disabled', index != 6);
        field(ADAPT.DOC_E, ADAPT.TYPE_PHONO, 1, 9).prop('disabled', index != 6);
        field(ADAPT.DOC_E, ADAPT.TYPE_PHONO, 1, 10).prop('disabled', index != 6);

        //MOVIE
        field(ADAPT.DOC_E, ADAPT.TYPE_MOVIE, 0, 0).prop('disabled', index != 7);
        //field(ADAPT.DOC_E, ADAPT.TYPE_MOVIE, 0, 1).prop('disabled', index != 7);
        //field(ADAPT.DOC_E, ADAPT.TYPE_MOVIE, 0, 2).prop('disabled', index != 7);
        field(ADAPT.DOC_E, ADAPT.TYPE_MOVIE, 0, 3).prop('disabled', index != 7);
        field(ADAPT.DOC_E, ADAPT.TYPE_MOVIE, 0, 4).prop('disabled', index != 7);
        field(ADAPT.DOC_E, ADAPT.TYPE_MOVIE, 0, 5).prop('disabled', index != 7);
        field(ADAPT.DOC_E, ADAPT.TYPE_MOVIE, 0, 6).prop('disabled', index != 7);
        field(ADAPT.DOC_E, ADAPT.TYPE_MOVIE, 0, 7).prop('disabled', index != 7);
        field(ADAPT.DOC_E, ADAPT.TYPE_MOVIE, 0, 8).prop('disabled', index != 7);
        field(ADAPT.DOC_E, ADAPT.TYPE_MOVIE, 0, 9).prop('disabled', index != 7);
        field(ADAPT.DOC_E, ADAPT.TYPE_MOVIE, 0, 10).prop('disabled', index != 7);

        field(ADAPT.DOC_E, ADAPT.TYPE_MOVIE, 1, 0).prop('disabled', index != 7);
        //field(ADAPT.DOC_E, ADAPT.TYPE_MOVIE, 1, 1).prop('disabled', index != 7);
        //field(ADAPT.DOC_E, ADAPT.TYPE_MOVIE, 1, 2).prop('disabled', index != 7);
        field(ADAPT.DOC_E, ADAPT.TYPE_MOVIE, 1, 3).prop('disabled', index != 7);
        field(ADAPT.DOC_E, ADAPT.TYPE_MOVIE, 1, 4).prop('disabled', index != 7);
        field(ADAPT.DOC_E, ADAPT.TYPE_MOVIE, 1, 5).prop('disabled', index != 7);
        field(ADAPT.DOC_E, ADAPT.TYPE_MOVIE, 1, 6).prop('disabled', index != 7);
        field(ADAPT.DOC_E, ADAPT.TYPE_MOVIE, 1, 7).prop('disabled', index != 7);
        field(ADAPT.DOC_E, ADAPT.TYPE_MOVIE, 1, 8).prop('disabled', index != 7);
        field(ADAPT.DOC_E, ADAPT.TYPE_MOVIE, 1, 9).prop('disabled', index != 7);
        field(ADAPT.DOC_E, ADAPT.TYPE_MOVIE, 1, 10).prop('disabled', index != 7);

        //VIDEO
        field(ADAPT.DOC_E, ADAPT.TYPE_VIDEO, 0, 0).prop('disabled', index != 8);
        //field(ADAPT.DOC_E, ADAPT.TYPE_VIDEO, 0, 1).prop('disabled', index != 8);
        //field(ADAPT.DOC_E, ADAPT.TYPE_VIDEO, 0, 2).prop('disabled', index != 8);
        field(ADAPT.DOC_E, ADAPT.TYPE_VIDEO, 0, 3).prop('disabled', index != 8);
        field(ADAPT.DOC_E, ADAPT.TYPE_VIDEO, 0, 4).prop('disabled', index != 8);
        field(ADAPT.DOC_E, ADAPT.TYPE_VIDEO, 0, 5).prop('disabled', index != 8);
        field(ADAPT.DOC_E, ADAPT.TYPE_VIDEO, 0, 6).prop('disabled', index != 8);
        field(ADAPT.DOC_E, ADAPT.TYPE_VIDEO, 0, 7).prop('disabled', index != 8);
        field(ADAPT.DOC_E, ADAPT.TYPE_VIDEO, 0, 8).prop('disabled', index != 8);
        field(ADAPT.DOC_E, ADAPT.TYPE_VIDEO, 0, 9).prop('disabled', index != 8);
        field(ADAPT.DOC_E, ADAPT.TYPE_VIDEO, 0, 10).prop('disabled', index != 8);

        field(ADAPT.DOC_E, ADAPT.TYPE_VIDEO, 1, 0).prop('disabled', index != 8);
        //field(ADAPT.DOC_E, ADAPT.TYPE_VIDEO, 1, 1).prop('disabled', index != 8);
        //field(ADAPT.DOC_E, ADAPT.TYPE_VIDEO, 1, 2).prop('disabled', index != 8);
        field(ADAPT.DOC_E, ADAPT.TYPE_VIDEO, 1, 3).prop('disabled', index != 8);
        field(ADAPT.DOC_E, ADAPT.TYPE_VIDEO, 1, 4).prop('disabled', index != 8);
        field(ADAPT.DOC_E, ADAPT.TYPE_VIDEO, 1, 5).prop('disabled', index != 8);
        field(ADAPT.DOC_E, ADAPT.TYPE_VIDEO, 1, 6).prop('disabled', index != 8);
        field(ADAPT.DOC_E, ADAPT.TYPE_VIDEO, 1, 7).prop('disabled', index != 8);
        field(ADAPT.DOC_E, ADAPT.TYPE_VIDEO, 1, 8).prop('disabled', index != 8);
        field(ADAPT.DOC_E, ADAPT.TYPE_VIDEO, 1, 9).prop('disabled', index != 8);
        field(ADAPT.DOC_E, ADAPT.TYPE_VIDEO, 1, 10).prop('disabled', index != 8);

    }

    var init = function() {

    }

    var start = function() {
        init();
        for (var i in args) {
            disable(args[i]);
        }
    }

    start(); alert('done!');
}

