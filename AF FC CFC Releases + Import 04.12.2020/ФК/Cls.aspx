﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Cls.aspx.vb" Inherits="WebApplication.Cls1" %>

<%@ Register Src="ListDoc.ascx" TagName="ListDoc" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Прикрепление к АНСА</title>
</head>
<body style="background-color: White; padding: 5px;">
    <form id="form1" runat="server">
    <aj:ToolkitScriptManager ID="ToolkitScriptManagerDefault" runat="server" EnableScriptGlobalization="true"
        ScriptMode="Release" EnablePartialRendering="True" EnableHistory="true" />
    <table style="width: 100%;">
        <tr>
            <td>
                <div class="LogicBlockCaption">
                    Выберите АНСА к которым Вы хотите прикрепить объекта учета и нажмите "Прикрепить"
                </div>
            </td>
            <td style="text-align: right;">
                <asp:Button ID="ButtonCls" runat="server" Text="Прикрепить" />
            </td>
        </tr>
    </table>
    <asp:UpdatePanel ID="UpdatePanelCLS" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eq:eqMenu ID="MenuTabs" runat="server" Orientation="Horizontal" StaticEnableDefaultPopOutImage="False"
                ItemWrap="false">
                <Items>
                    <asp:MenuItem Text="АНСА межфондовый" Value="0" Selected="true" />
                    <asp:MenuItem Text="АНСА внутрифондовый" Value="1" />
                    <asp:MenuItem Text="АНСА в рамках описи" Value="2" />
                </Items>
                <StaticSelectedStyle CssClass="MenuItemStyleSelected" />
            </eq:eqMenu>
                <asp:MultiView ID="MultiViewTabs" runat="server" ActiveViewIndex="0">
                    <asp:View runat="server">
                        <uc1:ListDoc ID="ListDocCLS701" runat="server" DocTypeURL="CLS701.ascx" SelectDocMode="Multi" />
                    </asp:View>
                    <asp:View runat="server">
                        <uc1:ListDoc ID="ListDocCLS702" runat="server" DocTypeURL="CLS702.ascx" SelectDocMode="Multi" />
                    </asp:View>
                    <asp:View runat="server">
                        <uc1:ListDoc ID="ListDocCLS703" runat="server" DocTypeURL="CLS703.ascx" SelectDocMode="Multi" />
                    </asp:View>
                </asp:MultiView>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
