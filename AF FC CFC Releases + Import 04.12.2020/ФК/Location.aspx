﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Location.aspx.vb" Inherits="WebApplication.Location1" %>

<%@ Register Src="ListDoc.ascx" TagName="ListDoc" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Топографирование</title>
</head>
<body style="background-color: White; padding: 5px;">
    <form id="form1" runat="server">
    <aj:ToolkitScriptManager ID="ToolkitScriptManagerDefault" runat="server" EnableScriptGlobalization="true"
        ScriptMode="Release" EnablePartialRendering="True" EnableHistory="true" />
    <table style="width: 100%;">
        <tr>
            <td>
                <div class="LogicBlockCaption">
                    Выберите рубрики топографического указателя к которой Вы хотите прикрепить объекты учета
                    и нажмите "Прикрепить"
                </div>
            </td>
            <td style="text-align: right;">
                <asp:Button ID="ButtonLocation" runat="server" Text="Прикрепить" />
            </td>
        </tr>
    </table>
    <asp:UpdatePanel ID="UpdatePanelLocation" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <uc1:ListDoc ID="ListDocLocation" runat="server" DocTypeURL="LOCATION.ascx" SelectDocMode="Multi" />
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
