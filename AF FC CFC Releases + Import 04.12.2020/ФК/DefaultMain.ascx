﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="DefaultMain.ascx.vb"
    Inherits="WebApplication.DefaultMain" %>

<div class="PanelInfo">
    <asp:Localize ID="LocalizeCurrentMain" runat="server" Text="<%$ Resources:eqResources, CAP_CurrentMain %>" />
</div>
<div class="PanelPlaceHolder">
    <asp:PlaceHolder ID="MainText" runat="server" />
    <asp:PlaceHolder ID="PHChilds" runat="server">
        <div class="LogicBlockCaption">
            <asp:Localize ID="LocalizeCategories" runat="server" Text="<%$ Resources:eqResources, CAP_Categories %>" />
        </div>
        <div class="LogicBlock" style="padding-left: 10px;">
            <asp:PlaceHolder ID="MainCategories" runat="server" />
        </div>
    </asp:PlaceHolder>
</div>
