﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="RecalcUnit.aspx.vb" Inherits="WebApplication.RecalcUnit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <script type="text/javascript" src="jquery-1.6.min.js"></script>

</head>
<body style="background-color: White; padding: 5px;">

    <script type="text/javascript">
        sel = function(obj) {
            var scope = $(obj).parents('table').first();
            $(':input', scope).prop('checked', true);
        }
        des = function(obj) {
            var scope = $(obj).parents('table').first();
            $(':input', scope).removeProp('checked');
        }
    </script>

    <form id="form1" runat="server">
    <asp:ScriptManager runat="server">
    </asp:ScriptManager>
    <eq:eqMenu ID="MenuTabs" runat="server" Orientation="Horizontal" StaticEnableDefaultPopOutImage="False"
        ItemWrap="false" HorizontalMaxCount="3">
        <Items>
            <asp:MenuItem Text="Общие сведения" Value="0" Selected="true" />
            <asp:MenuItem Text="Реквизиты кинодокументов" Value="1" />
            <asp:MenuItem Text="Реквизиты видеодокументов" Value="2" />
            <asp:MenuItem Text="Реквизиты фонодокументов" Value="3" />
        </Items>
        <StaticSelectedStyle CssClass="MenuItemStyleSelected" />
    </eq:eqMenu>
    <asp:MultiView ID="MultiViewTabs" runat="server" ActiveViewIndex="0">
        <asp:View runat="server">
            <table>
                <tr>
                    <td colspan="2">
                        <a href="#" onclick="sel(this);">Выделить все</a>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <a href="#" onclick="des(this);">Снять выделение</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="c_A_NACH_DATA" Text="Начальная дата" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="c_A_LISTS" Text="Листов" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="c_A_KON_DATA" Text="Конечная дата" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="c_A_EU" Text="Количество единиц хранения/учета" />
                    </td>
                </tr>
            </table>
        </asp:View>
        <asp:View runat="server">
            <table>
                <tr>
                    <td colspan="2">
                        <a href="#" onclick="sel(this);">Выделить все</a>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <a href="#" onclick="des(this);">Снять выделение</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="c_K_NEG" Text="Количество негативов" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="c_M_NEG" Text="Метраж негативов" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="c_K_DNEG" Text="Количество дубль-негативов (контратипов)" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="c_M_DNEG" Text="Метраж дубль-негативов (контратипов)" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="c_K_FON" Text="Количество фонограмм (негатив)" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="c_M_FON" Text="Метраж фонограмм (негатив)" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="c_K_MFON" Text="Количество магнитных фонограмм" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="c_M_MFON" Text="Метраж магнитных фонограмм" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="c_K_MFONS" Text="Количество магнитных фонограмм совмещен." />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="c_M_MFONS" Text="Метраж магнитных фонограмм совмещен." />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="c_K_PPOS" Text="Количество промежуточных позитивов" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="c_M_PPOS" Text="Метраж промежуточных позитивов" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="c_K_POS" Text="Количество позитивов" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="c_M_POS" Text="Метраж позитивов" />
                    </td>
                </tr>
            </table>
        </asp:View>
        <asp:View runat="server">
            <table>
                <tr>
                    <td colspan="2">
                        <a href="#" onclick="sel(this);">Выделить все</a>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <a href="#" onclick="des(this);">Снять выделение</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="c_K_ORIG" Text="Количество оригиналов" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="c_K_KOP" Text="Количество копий" />
                    </td>
                </tr>
            </table>
        </asp:View>
        <asp:View runat="server">
            <table>
                <tr>
                    <td colspan="2">
                        <a href="#" onclick="sel(this);">Выделить все</a>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <a href="#" onclick="des(this);">Снять выделение</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="c_K_ORIGN" Text="Количество оригиналов (негативов)" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="c_M_ORIGN" Text="Метраж оригиналов (негативов)" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="c_K_KOPP" Text="Количество копий (позитивов)" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="c_M_KOPP" Text="Метраж копий (позитивов)" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:CheckBox runat="server" ID="c_K_GORIG" Text="Количество граммофонных оригиналов" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:CheckBox runat="server" ID="c_K_GRAM" Text="Количество грампластинок" />
                    </td>
                </tr>
            </table>
        </asp:View>
    </asp:MultiView>
    <table style="padding: 10px;">
        <tr>
            <td>
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <asp:Button runat="server" ID="bGo" Text="Пересчитать" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>
                <asp:Button runat="server" ID="bClose" Text="Закрыть" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:UpdateProgress runat="server">
                    <ProgressTemplate>
                        <asp:Image runat="server" SkinID="UpdateProgress" />
                        <br />
                        <asp:Label runat="server" Text="Идет расчет..." />
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
