﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="DocType.ascx.vb" Inherits="WebApplication.DocType" %>
<div class="LogicBlockCaption">
    Документ
</div>
<table width="100%" class="LogicBlockTable">
    <colgroup>
        <col style="width: 150px;" />
        <col />
    </colgroup>
    <tr>
        <td>
            Название
        </td>
        <td>
            <eq:eqTextBox ID="dtDocType" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            Контрол
        </td>
        <td>
            <eq:eqTextBox ID="DocURL" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            Категория
        </td>
        <td>
            <asp:DropDownList ID="CATEGORY" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            Категория верхнего уровня
        </td>
        <td>
            <asp:TextBox ID="CATEGORY2" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            ImageURL
        </td>
        <td>
            <eq:eqTextBox ID="ImageURL" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            ToolTip
        </td>
        <td>
            <eq:eqTextBox ID="ToolTip" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            SortOrder
        </td>
        <td>
            <eq:eqTextBox ID="SortOrder" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            IsManaged
        </td>
        <td>
            <asp:CheckBox ID="IsManaged" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            URLTarget
        </td>
        <td>
            <eq:eqTextBox ID="URLTarget" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            OpenMode
        </td>
        <td>
            <asp:RadioButtonList ID="OpenMode" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal">
                <asp:ListItem Value="D" Text="Doc" />
                <asp:ListItem Value="L" Text="ListDoc" />
            </asp:RadioButtonList>
        </td>
    </tr>
    <tr>
        <td>
            PhsycalDelete
        </td>
        <td>
            <asp:CheckBox ID="PhysicalDelete" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            Base DocType
        </td>
        <td>
            <asp:DropDownList ID="BaseDocTypeID" runat="server" AutoPostBack="true" />
        </td>
    </tr>
    <tr>
        <td>
            Tree KeyFieldName
        </td>
        <td>
            <eq:eqTextBox ID="TreeKeyFieldName" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            Tree ParentKeyFieldName
        </td>
        <td>
            <eq:eqTextBox ID="TreeParentKeyFieldName" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            CanSelect Script
        </td>
        <td>
            <small>Must return bit</small>
            <br />
            <eq:eqTextBox ID="CanSelectScript" runat="server" />
        </td>
    </tr>
</table>
<table width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td align="left">
            <div class="LogicBlockCaption">
                Статусы
            </div>
        </td>
        <td align="right">
            <asp:LinkButton ID="DocStatesUpdatePanelAddStandart" runat="server" Text="Добавить стандартный набор" />
        </td>
    </tr>
</table>
<asp:UpdatePanel ID="eqvDocStatesUpdatePanel" runat="server" UpdateMode="Conditional"
    RenderMode="Inline">
    <ContentTemplate>
        <asp:GridView ID="BaseDocTypeStates" runat="server" Width="100%">
            <Columns>
                <asp:BoundField HeaderText="StateName" DataField="StateName" />
                <asp:BoundField HeaderText="StateDescription" DataField="StateDescription" />
            </Columns>
        </asp:GridView>
        <asp:PlaceHolder ID="PlaceHolderDocStates" runat="server">
            <eq:eqSpecification ID="eqvDocStates" runat="server" Width="100%">
                <eq:eqSpecificationColumn HeaderText="StateName">
                    <ItemTemplate>
                        <eq:eqTextBox ID="StateName" runat="server" AutoPostBack="true" OnTextChanged="StateName_TextChanged" />
                    </ItemTemplate>
                </eq:eqSpecificationColumn>
                <eq:eqSpecificationColumn HeaderText="StateDescription">
                    <ItemTemplate>
                        <eq:eqTextBox ID="StateDescription" runat="server" />
                    </ItemTemplate>
                </eq:eqSpecificationColumn>
            </eq:eqSpecification>
            <div class="LogicBlockCaptionMini">
                SavedState / DeletedState
            </div>
            <table width="100%" class="LogicBlockTable">
                <colgroup>
                    <col style="width: 150px;" />
                    <col />
                </colgroup>
                <tr>
                    <td>
                        SavedState
                    </td>
                    <td>
                        <asp:DropDownList ID="SavedState" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        DeletedState
                    </td>
                    <td>
                        <asp:DropDownList ID="DeletedState" runat="server" />
                    </td>
                </tr>
            </table>
        </asp:PlaceHolder>
    </ContentTemplate>
</asp:UpdatePanel>
<div class="LogicBlockCaption">
    Переходы
</div>
<asp:UpdatePanel ID="eqvDocRootsUpdatePanel" runat="server" UpdateMode="Conditional"
    RenderMode="Inline">
    <ContentTemplate>
        <eq:eqSpecification ID="eqvDocRoots" runat="server" Width="100%">
            <eq:eqSpecificationColumn HeaderText="SourceState">
                <ItemTemplate>
                    <asp:DropDownList ID="SourceStateID" runat="server" />
                </ItemTemplate>
            </eq:eqSpecificationColumn>
            <eq:eqSpecificationColumn HeaderText="DestState">
                <ItemTemplate>
                    <asp:DropDownList ID="DestStateID" runat="server" />
                </ItemTemplate>
            </eq:eqSpecificationColumn>
        </eq:eqSpecification>
    </ContentTemplate>
</asp:UpdatePanel>
<table width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td align="left">
            <div class="LogicBlockCaption">
                Кнопки
            </div>
        </td>
        <td align="right">
            <asp:LinkButton ID="DocToolBarUpdatePanelAddStandart" runat="server" Text="Добавить стандартный набор" />
        </td>
    </tr>
</table>
<asp:UpdatePanel ID="eqvDocToolBarUpdatePanel" runat="server" UpdateMode="Conditional"
    RenderMode="Inline">
    <ContentTemplate>
        <eq:eqSpecification ID="eqvDocToolBar" runat="server" Width="100%">
            <eq:eqSpecificationColumn HeaderText="Caption">
                <ItemTemplate>
                    <eq:eqTextBox ID="Caption" runat="server" />
                </ItemTemplate>
            </eq:eqSpecificationColumn>
            <eq:eqSpecificationColumn HeaderText="ImageURL">
                <ItemTemplate>
                    <eq:eqTextBox ID="ImageURL" runat="server" />
                </ItemTemplate>
            </eq:eqSpecificationColumn>
            <eq:eqSpecificationColumn HeaderText="CommandName">
                <ItemTemplate>
                    <eq:eqTextBox ID="CommandName" runat="server" />
                </ItemTemplate>
            </eq:eqSpecificationColumn>
            <eq:eqSpecificationColumn HeaderText="ToolTip">
                <ItemTemplate>
                    <eq:eqTextBox ID="ToolTip" runat="server" />
                </ItemTemplate>
            </eq:eqSpecificationColumn>
            <eq:eqSpecificationColumn HeaderText="JavaScriptHandler">
                <ItemTemplate>
                    <eq:eqTextBox ID="JavaScriptHandler" runat="server" />
                </ItemTemplate>
            </eq:eqSpecificationColumn>
            <eq:eqSpecificationColumn HeaderText="ShowInListOrDocument">
                <ItemTemplate>
                    <asp:RadioButtonList ID="ShowInListOrDocument" runat="server">
                        <asp:ListItem Text="List" Value="L" />
                        <asp:ListItem Text="Doc" Value="D" />
                    </asp:RadioButtonList>
                </ItemTemplate>
            </eq:eqSpecificationColumn>
        </eq:eqSpecification>
    </ContentTemplate>
</asp:UpdatePanel>
<table width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td align="left">
            <div class="LogicBlockCaption">
                Дополнительные условия (SELECT ... WHERE ... AND (FunctionText) AND (FunctionText)
                AND...)
            </div>
        </td>
        <td align="right">
            Common parameters: @UserID
        </td>
    </tr>
</table>
<asp:UpdatePanel ID="eqDocTypeFunctionsUpdatePanel" runat="server" UpdateMode="Conditional"
    RenderMode="Inline">
    <ContentTemplate>
        <eq:eqSpecification ID="eqDocTypeFunctions" runat="server" Width="100%">
            <eq:eqSpecificationColumn HeaderText="Дополнительное условие">
                <ItemTemplate>
                    <table width="100%">
                        <tr>
                            <td>
                                <div class="LogicBlockCaptionMini">
                                    FunctionName:
                                </div>
                                <eq:eqTextBox ID="FunctionName" runat="server" />
                            </td>
                            <td>
                                <div class="LogicBlockCaptionMini">
                                    ParameterName (Подстановка по тексту):
                                </div>
                                <eq:eqTextBox ID="ParameterName" runat="server" />
                            </td>
                            <td>
                                <div class="LogicBlockCaptionMini">
                                    ParameterType:
                                </div>
                                <asp:DropDownList ID="ParameterType" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <div class="LogicBlockCaptionMini">
                                    ParameterValuesScript:
                                </div>
                                <eq:eqTextBox ID="ParameterValuesScript" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <div class="LogicBlockCaptionMini">
                                    ParameterDefaultValueScript:
                                </div>
                                <eq:eqTextBox ID="ParameterDefaultValueScript" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <div class="LogicBlockCaptionMini">
                                    FunctionText:
                                </div>
                                <eq:eqTextBox ID="FunctionText" TextMode="MultiLine" Rows="5" runat="server" />
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
            </eq:eqSpecificationColumn>
        </eq:eqSpecification>
    </ContentTemplate>
</asp:UpdatePanel>
<div class="LogicBlockCaption">
    Tables
</div>
<asp:UpdatePanel ID="eqDocTablesUpdatePanel" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div class="LogicBlockCaptionMini">
            Table
        </div>
        <table width="100%" class="LogicBlockTable">
            <colgroup>
                <col style="width: 100px;" />
                <col />
                <col />
                <col />
            </colgroup>
            <tr>
                <td>
                    Name
                </td>
                <td>
                    <asp:DropDownList ID="TableNameList" runat="server" AutoPostBack="true" />
                </td>
                <td>
                    <eq:eqTextBox ID="NewTableName" runat="server" MaxLength="225" Width="200" />
                </td>
                <td>
                    <asp:Button ID="CreateRenameButton" runat="server" Width="100%" Text="Create/Rename" />
                </td>
            </tr>
        </table>
        <asp:Repeater ID="RepeaterColumns" runat="server">
            <HeaderTemplate>
                <div class="LogicBlockCaptionMini">
                    Columns
                </div>
                <table width="100%" class="LogicBlockTable">
                    <colgroup>
                        <col />
                        <col width="110px" />
                        <col width="90px" />
                        <col width="90px" />
                    </colgroup>
                    <tr class="GridHeader">
                        <th>
                            Name
                        </th>
                        <th>
                            Type
                        </th>
                        <th>
                            FieldInfo
                        </th>
                        <th>
                            ListDocFieldInfo
                        </th>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr class="GridItem">
                    <td>
                        <%#Container.DataItem("ColumnName")%>
                    </td>
                    <td>
                        <%#Container.DataItem("TypeName")%>
                    </td>
                    <td>
                        <%--<asp:LinkButton ID="OpenFieldInfo" runat="server" Text="Open" CommandName="OpenFieldInfo"
                                    CommandArgument='<%# Container.DataItem("FieldID") %>' Visible='<%# NOT IsDBNull(Container.DataItem("FieldID")) %>'
                                    OnCommand="ModifyButton_OnCommand" />--%>
                        <%--BasePage.GetDocURL("DocStructure.ascx", FieldInfoID, True)--%>
                        <%--<asp:HyperLink runat="server" Text="Open" NavigateUrl='<%# BasePage.GetDocURL("DocStructure.ascx", Container.DataItem("FieldID"), True) %>' Visible='<%# NOT IsDBNull(Container.DataItem("FieldID")) %>'></asp:HyperLink>--%>
                        <asp:HyperLink runat="server" Text="Open" Target="_blank" NavigateUrl='<%# GetOpenFieldInfoUrl(Container.DataItem) %>'
                            Visible='<%# NOT IsDBNull(Container.DataItem("FieldID")) %>'></asp:HyperLink>
                        <asp:LinkButton ID="CreateFieldInfo" runat="server" Text="Create" CommandName="CreateFieldInfo"
                            CommandArgument='<%# Container.DataItem("ColumnName") %>' Visible='<%# IsDBNull(Container.DataItem("FieldID")) %>'
                            OnCommand="ModifyButton_OnCommand" />
                        <asp:LinkButton ID="DeleteFieldInfo" runat="server" Text="Delete" CommandName="DeleteFieldInfo"
                            CommandArgument='<%# Container.DataItem("FieldID") %>' Visible='<%# NOT IsDBNull(Container.DataItem("FieldID")) %>'
                            OnCommand="ModifyButton_OnCommand" />
                    </td>
                    <td>
                        <%--<asp:LinkButton ID="OpenListDocFieldInfo" runat="server" Text="Open" CommandName="OpenListDocFieldInfo"
                                    CommandArgument='<%# Container.DataItem("ListDocFieldID") %>' Visible='<%# NOT IsDBNull(Container.DataItem("ListDocFieldID")) %>'
                                    OnCommand="ModifyButton_OnCommand" />--%>
                        <asp:HyperLink runat="server" Text="Open" Target="_blank" NavigateUrl='<%# GetOpenListDocFieldInfoUrl(Container.DataItem) %>'
                            Visible='<%# NOT IsDBNull(Container.DataItem("ListDocFieldID")) %>'></asp:HyperLink>
                        <asp:LinkButton ID="CreateListDocFieldInfo" runat="server" Text="Create" CommandName="CreateListDocFieldInfo"
                            CommandArgument='<%# Container.DataItem("ColumnName") & "," & Container.DataItem("FieldID").ToString %>'
                            Visible='<%# NOT IsDBNull(Container.DataItem("FieldID")) AND IsDBNull(Container.DataItem("ListDocFieldID")) %>'
                            OnCommand="ModifyButton_OnCommand" />
                        <asp:LinkButton ID="DeleteListDocFieldInfo" runat="server" Text="Delete" CommandName="DeleteListDocFieldInfo"
                            CommandArgument='<%# Container.DataItem("ListDocFieldID") %>' Visible='<%# NOT IsDBNull(Container.DataItem("ListDocFieldID")) %>'
                            OnCommand="ModifyButton_OnCommand" />
                    </td>
                </tr>
            </ItemTemplate>
            <AlternatingItemTemplate>
                <tr class="GridAlternatingItem">
                    <td>
                        <%#Container.DataItem("ColumnName")%>
                    </td>
                    <td>
                        <%#Container.DataItem("TypeName")%>
                    </td>
                    <td>
                        <%--<asp:LinkButton ID="OpenFieldInfo" runat="server" Text="Open" CommandName="OpenFieldInfo"
                                    CommandArgument='<%# Container.DataItem("FieldID") %>' Visible='<%# NOT IsDBNull(Container.DataItem("FieldID")) %>'
                                    OnCommand="ModifyButton_OnCommand" />--%>
                        <%--BasePage.GetDocURL("DocStructure.ascx", FieldInfoID, True)--%>
                        <%--<asp:HyperLink runat="server" Text="Open" NavigateUrl='<%# BasePage.GetDocURL("DocStructure.ascx", Container.DataItem("FieldID"), True) %>' Visible='<%# NOT IsDBNull(Container.DataItem("FieldID")) %>'></asp:HyperLink>--%>
                        <asp:HyperLink runat="server" Text="Open" Target="_blank" NavigateUrl='<%# GetOpenFieldInfoUrl(Container.DataItem) %>'
                            Visible='<%# NOT IsDBNull(Container.DataItem("FieldID")) %>'></asp:HyperLink>
                        <asp:LinkButton ID="CreateFieldInfo" runat="server" Text="Create" CommandName="CreateFieldInfo"
                            CommandArgument='<%# Container.DataItem("ColumnName") %>' Visible='<%# IsDBNull(Container.DataItem("FieldID")) %>'
                            OnCommand="ModifyButton_OnCommand" />
                        <asp:LinkButton ID="DeleteFieldInfo" runat="server" Text="Delete" CommandName="DeleteFieldInfo"
                            CommandArgument='<%# Container.DataItem("FieldID") %>' Visible='<%# NOT IsDBNull(Container.DataItem("FieldID")) %>'
                            OnCommand="ModifyButton_OnCommand" />
                    </td>
                    <td>
                        <%--<asp:LinkButton ID="OpenListDocFieldInfo" runat="server" Text="Open" CommandName="OpenListDocFieldInfo"
                                    CommandArgument='<%# Container.DataItem("ListDocFieldID") %>' Visible='<%# NOT IsDBNull(Container.DataItem("ListDocFieldID")) %>'
                                    OnCommand="ModifyButton_OnCommand" />--%>
                        <asp:HyperLink runat="server" Text="Open" Target="_blank" NavigateUrl='<%# GetOpenListDocFieldInfoUrl(Container.DataItem) %>'
                            Visible='<%# NOT IsDBNull(Container.DataItem("ListDocFieldID")) %>'></asp:HyperLink>
                        <asp:LinkButton ID="CreateListDocFieldInfo" runat="server" Text="Create" CommandName="CreateListDocFieldInfo"
                            CommandArgument='<%# Container.DataItem("ColumnName") & "," & Container.DataItem("FieldID").ToString %>'
                            Visible='<%# NOT IsDBNull(Container.DataItem("FieldID")) AND IsDBNull(Container.DataItem("ListDocFieldID")) %>'
                            OnCommand="ModifyButton_OnCommand" />
                        <asp:LinkButton ID="DeleteListDocFieldInfo" runat="server" Text="Delete" CommandName="DeleteListDocFieldInfo"
                            CommandArgument='<%# Container.DataItem("ListDocFieldID") %>' Visible='<%# NOT IsDBNull(Container.DataItem("ListDocFieldID")) %>'
                            OnCommand="ModifyButton_OnCommand" />
                    </td>
                </tr>
            </AlternatingItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </ContentTemplate>
</asp:UpdatePanel>
