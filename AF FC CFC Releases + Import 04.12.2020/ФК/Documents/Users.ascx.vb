﻿Public Partial Class Users
    Inherits BaseDoc

    Public Overrides Function PutValuesFromDataSetToHeaderControls() As Boolean
        UserTheme.Items.Clear()
        UserTheme.Items.Add("")
        For Each ThemeDirectory In My.Computer.FileSystem.GetDirectories(Server.MapPath("App_Themes"))
            Dim ThemeDirectoryInfo = My.Computer.FileSystem.GetDirectoryInfo(ThemeDirectory)
            Dim ThemeName = ThemeDirectoryInfo.Name
            UserTheme.Items.Add(ThemeName)
        Next
        Return MyBase.PutValuesFromDataSetToHeaderControls()
    End Function

    Public Overrides Function SaveDoc() As Boolean
        SaveDoc = MyBase.SaveDoc()
        If SaveDoc Then
            Session("UserTheme") = Nothing
            Response.Redirect(BasePage.GetDocURL(DocTypeURL, DocID, BasePage.IsModal))
        End If
    End Function

    Public Overrides Function DeleteDoc() As Boolean
        If BUILD_IN_ACCOUNT.Checked Then
            AddMessage("Данный пользователь не может быть удален т.к. пользователь встроенный")
        Else
            Return MyBase.DeleteDoc()
        End If
    End Function

End Class