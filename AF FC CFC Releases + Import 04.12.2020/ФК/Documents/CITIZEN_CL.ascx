﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CITIZEN_CL.ascx.vb"
    Inherits="WebApplication.CITIZEN_CL" %>
<table style="width: 700px;">
    <colgroup>
        <col style="width: 150px;" />
        <col />
    </colgroup>
    <tr>
        <td>
            Номер фонда
        </td>
        <td>
            <asp:TextBox ID="FUND_NUMS" runat="server" ReadOnly="true" />
        </td>
    </tr>    
    <tr>
        <td>
            Ф.И.О.
        </td>
        <td>
            <eq:eqTextBox ID="NAME" runat="server" />
        </td>
    </tr>
    <tr>
        <td valign="top">
            Родство
        </td>
        <td colspan="2">
            <eq:eqTextBox ID="RELATIONSHIP" runat="server" TextMode="MultiLine" Rows="2" />
        </td>
    </tr>
    <tr>
        <td>
            Предыдущая фамилия
        </td>
        <td>
            <eq:eqTextBox ID="LAST_FAMILY_NAME" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            Титул
        </td>
        <td>
            <eq:eqTextBox ID="TITLE" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            Псевдоним
        </td>
        <td>
            <eq:eqTextBox ID="NICKNAME" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            Дата рождения
        </td>
        <td>
            <eq:eqTextBox ID="BIRTH_DATE_FMT0" runat="server" Width="80" />
            <aj:CalendarExtender runat="server" TargetControlID="BIRTH_DATE_FMT0" />
        </td>
    </tr>
    <tr>
        <td>
            Дата смерти
        </td>
        <td>
            <eq:eqTextBox ID="DEATH_DATE_FMT0" runat="server" Width="80" />
            <aj:CalendarExtender runat="server" TargetControlID="DEATH_DATE_FMT0" />
        </td>
    </tr>
    <tr>
        <td>
            Профессия
        </td>
        <td>
            <eq:eqTextBox ID="PROFESSION" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            Должность
        </td>
        <td>
            <eq:eqTextBox ID="POST" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            Ученая степень
        </td>
        <td>
            <eq:eqTextBox ID="DEGREE" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            Воинское звание
        </td>
        <td>
            <eq:eqTextBox ID="MILITARY_RANK" runat="server" />
        </td>
    </tr>
    <tr>
        <td valign="top">
            Почетное звание
        </td>
        <td colspan="2">
            <eq:eqTextBox ID="HONORARY_RANK" runat="server" TextMode="MultiLine" Rows="2" />
        </td>
    </tr>
    <tr>
        <td valign="top">
            Примечание
        </td>
        <td colspan="2">
            <eq:eqTextBox ID="NOTE" runat="server" TextMode="MultiLine" Rows="4" />
        </td>
    </tr>
</table>
