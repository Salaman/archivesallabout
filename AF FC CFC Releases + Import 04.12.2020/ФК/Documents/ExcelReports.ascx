﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ExcelReports.ascx.vb" Inherits="WebApplication.ExcelReports" %>

<%@ Register TagPrefix="qq" Namespace="WebApplication.WebUI" Assembly="WebApplication" %>

<div class="LogicBlockCaption">
    Архив и архивохранилище
</div>
<table width="100%" class="LogicBlockTable">
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_PassportArchiveStore" Text="Паспорт архивохранилища"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" Checked="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_LocationStory" Text="Номерник архивохранилища"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_PassportArchive" Text="Паспорт архива" GroupName="R"
                OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_ArchStory" Text="Номерник архива" GroupName="R"
                OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_ArchiveDynamicsa_Abs" Text="Динамика паспорта архива (абсолютная)"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_ArchiveDynamics_Per" Text="Динамика  паспорта архива (в процентах)"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
</table>
<div class="LogicBlockCaption">
    Сводные сведения
</div>
<table width="100%" class="LogicBlockTable">
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_TraceableInfoArchiveList" Text="Сводные сведения по архиву"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_PhysicalStateDoc" Text="Физическое состояние документов фонда"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_PhysicalStateDocArchive" Text="Физическое состояние документов архива"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_PhysicalStateDocStorage" Text="Физическое состояние документов хранилища"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>    
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_UndetectedUnitStorage" Text="Необнаруженные единицы хранения"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_NsaContent" Text="Состав НСА к документам на бумажной основе"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
</table>
<div class="LogicBlockCaption">
    Состав фондов
</div>
<table width="100%" class="LogicBlockTable">
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_CompositionFund_1" Text="Состав фондов 1 категории"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_CompositionFund_2" Text="Состав фондов 2 категории"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_CompositionFund_3" Text="Состав фондов 3 категории"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_CompositionFund_None" Text="Состав некатегорированных фондов"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_CompositionFund_Any" Text="Состав фондов архива"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
</table>
<div class="LogicBlockCaption">
    Фонды
</div>
<table width="100%" class="LogicBlockTable">
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_FundList" Text="Список фондов" GroupName="R"
                OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_FundListImportantPlace" Text="Список фондов, содержащих ОЦД"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_CompositionFundList" Text="Состав фонда"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_InfoChangeFundsList" Text="Сведения об изменениях в составе и объеме фондов"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
</table>
<div class="LogicBlockCaption">
    Описи
</div>
<table width="100%" class="LogicBlockTable">
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_ArchList" Text="Архивная опись" GroupName="R"
                OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_InvVideoDok" Text="Опись видеодокументов"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_InvKinoDok" Text="Опись кинодокументов" GroupName="R"
                OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_InvFonoDokGramophoneRec" Text="Опись фонодокументов граммофонной записи (грампластинок)"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_InvFonoDokMagneticRec" Text="Опись фонодокументов магнитной записи"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_InvPhotographAlbum" Text="Опись фотоальбомов"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_InvPhotoDok" Text="Опись фотодокументов"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_InvElectronDok" Text="Опись электронных документов"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_ReestrOpisDel" Text="Реестр описей дел, документов"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_InvImportantPlace" Text="Опись ОЦД" GroupName="R"
                OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_ListImportantPlaceFund" Text="Перечень ОЦД в фонде (аналог описи ОЦД без заголовков)"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
</table>
<div class="LogicBlockCaption">
    Единицы хранения и учета
</div>
<table width="100%" class="LogicBlockTable">
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_DocumDeclassifyList" Text="Перечень рассекреченных документов"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_CardDeclassifyDoc" Text="Карточка рассекреченного документа"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_InnerList" Text="Внутренняя опись документов дела"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
</table>
<div class="LogicBlockCaption">
    Состав документов
</div>
<table width="100%" class="LogicBlockTable">
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_CompositionDoc_1" Text="Состав документов 1 категории"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_CompositionDoc_2" Text="Состав документов 2 категории"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_CompositionDoc_3" Text="Состав документов 3 категории"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_CompositionDoc_None" Text="Состав документов в некатегорированных фондах"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_CompositionDoc_Any" Text="Состав документов архива"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
</table>
<div class="LogicBlockCaption">
    Путеводители и указатели
</div>
<table width="100%" class="LogicBlockTable">
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_FundGuide" Text="Путеводитель по фондам архива"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_ShortGuide" Text="Краткий справочник по фондам архива"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_ShortGuideListFund" Text="Краткий справочник (список фондов) по фондам архива"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_Index" Text="Указатель" GroupName="R" OnCheckedChanged="Rep_Changed"
                AutoPostBack="true" />
        </td>
    </tr>
</table>
<div class="LogicBlockCaption">
    Топографический указатель
</div>
<table width="100%" class="LogicBlockTable">
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_TopIndex" Text="Постеллажный топографический указатель"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_FundTopIndex" Text="Пофондовый топографический указатель"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
</table>
<div class="LogicBlockCaption">
    Проверка
</div>
<table width="100%" class="LogicBlockTable">
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_Check" Text="Проверка правильности и полноты заполнения БД «Архивный фонд»"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" />
        </td>
    </tr>
</table>
<asp:Panel ID="Panel_Properties" runat="server" Width="350" BorderWidth="1" BackColor="White">
    <div class="LogicBlockCaption">
        Параметры отчета
    </div>
    <asp:Panel ID="Panel_SecurityFlags" runat="server">
        <table width="100%" cellspacing="4">
            <colgroup>
                <col width="110px" />
            </colgroup>
            <tr>
                <td>
                    Уровень секретности:
                </td>
                <td>
                    <asp:CheckBoxList ID="Prop_SecurityFlags" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Value="1" Text="откр" Selected="True" />
                        <asp:ListItem Value="2" Text="с" Selected="True" />
                        <asp:ListItem Value="3" Text="чс" Selected="True" />
                    </asp:CheckBoxList>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="Panel_ISN_LOCATION" runat="server">
        <table width="100%" cellspacing="4">
            <colgroup>
                <col width="110px" />
            </colgroup>
            <tr>
                <td>
                    Архивохранилище
                </td>
                <td>
                    <qq:eqDocument ID="Prop_ISN_LOCATION" runat="server" DocTypeURL="LOCATION.ascx" ListDocMode="NewWindowAndSelect"
                        LoadDocMode="ListDocAndSelect" ShowingFields="NAME" ShowingFieldsFormat="{0}"
                        KeyName="ISN_LOCATION" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="Panel_ISN_PASSPORT" runat="server">
        <table width="100%" cellspacing="4">
            <colgroup>
                <col width="110px" />
            </colgroup>
            <tr>
                <td>
                    Паспорт
                </td>
                <td>
                    <qq:eqDocument ID="Prop_ISN_PASSPORT" runat="server" DocTypeURL="ARCHIVE_PASSPORT.ascx"
                        ListDocMode="NewWindowAndSelect" LoadDocMode="ListDocAndSelect" ShowingFields="PASS_YEAR"
                        ShowingFieldsFormat="{0}" KeyName="ISN_PASSPORT" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="Panel_ISN_PASSPORT2" runat="server">
        <table width="100%" cellspacing="4">
            <colgroup>
                <col width="110px" />
            </colgroup>
            <tr>
                <td>
                    Паспорт 1
                </td>
                <td>
                    <qq:eqDocument ID="Prop_ISN_PASSPORT1" runat="server" DocTypeURL="ARCHIVE_PASSPORT.ascx"
                        ListDocMode="NewWindowAndSelect" LoadDocMode="ListDocAndSelect" ShowingFields="PASS_YEAR"
                        ShowingFieldsFormat="{0}" KeyName="ISN_PASSPORT" />
                </td>
                <td>
                    Паспорт 2
                </td>
                <td>
                    <qq:eqDocument ID="Prop_ISN_PASSPORT2" runat="server" DocTypeURL="ARCHIVE_PASSPORT.ascx"
                        ListDocMode="NewWindowAndSelect" LoadDocMode="ListDocAndSelect" ShowingFields="PASS_YEAR"
                        ShowingFieldsFormat="{0}" KeyName="ISN_PASSPORT" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="Panel_ISN_FUND" runat="server">
        <table width="100%" cellspacing="4">
            <colgroup>
                <col width="110px" />
            </colgroup>
            <tr>
                <td>
                    Фонд
                </td>
                <td>
                    <qq:eqDocument ID="Prop_ISN_FUND" runat="server" DocTypeURL="FUND.ascx" ListDocMode="NewWindowAndSelect"
                        LoadDocMode="ListDocAndSelect" ShowingFields="FUND_NUM" ShowingFieldsFormat="{0}"
                        KeyName="ISN_FUND" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="Panel_ISN_INVENTORY" runat="server">
        <table width="100%" cellspacing="4">
            <colgroup>
                <col width="110px" />
            </colgroup>
            <tr>
                <td>
                    Опись
                </td>
                <td>
                    <qq:eqDocument ID="Prop_ISN_INVENTORY" runat="server" DocTypeURL="INVENTORY.ascx"
                        ListDocMode="NewWindowAndSelect" LoadDocMode="ListDocAndSelect" ShowingFields="INVENTORY_NUM_RPT"
                        ShowingFieldsFormat="{0}" KeyName="ISN_INVENTORY" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="Panel_ISN_UNIT" runat="server">
        <table width="100%" cellspacing="4">
            <colgroup>
                <col width="110px" />
            </colgroup>
            <tr>
                <td>
                    Ед. хранения
                </td>
                <td>
                    <qq:eqDocument ID="Prop_ISN_UNIT" runat="server" DocTypeURL="UNIT.ascx" ListDocMode="NewWindowAndSelect"
                        LoadDocMode="ListDocAndSelect" ShowingFields="UNIT_NUM" ShowingFieldsFormat="{0}"
                        KeyName="ISN_UNIT" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="Panel_ISN_CLS" runat="server">
        <table width="100%" cellspacing="4">
            <colgroup>
                <col width="110px" />
            </colgroup>
            <tr>
                <td>
                    Классификатор
                </td>
                <td>
                    <qq:eqDocument ID="Prop_ISN_CLS" runat="server" DocTypeURL="CLS701.ascx" ListDocMode="NewWindowAndSelect"
                        LoadDocMode="ListDocAndSelect" ShowingFields="NAME" ShowingFieldsFormat="{0}"
                        KeyName="ISN_CLS" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="Panel_Year" runat="server">
        <table width="100%" cellspacing="4">
            <colgroup>
                <col width="110px" />
            </colgroup>
            <tr>
                <td>
                    Год
                </td>
                <td>
                    <eq:eqTextBox ID="Prop_Year" runat="server" Width="60" Text="2000" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="Panel_Year2" runat="server">
        <table width="100%" cellspacing="4">
            <colgroup>
                <col width="110px" />
            </colgroup>
            <tr>
                <td>
                    Год с
                </td>
                <td>
                    <eq:eqTextBox ID="Prop_Year1" runat="server" Width="60" Text="1800" />
                </td>
                <td>
                    по
                </td>
                <td>
                    <eq:eqTextBox ID="Prop_Year2" runat="server" Width="60" Text="2100" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="Panel_Period" runat="server">
        <table width="100%" cellspacing="4">
            <colgroup>
                <col width="110px" />
            </colgroup>
            <tr>
                <td>
                    Период
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="Prop_Period">
                        <asp:ListItem Text="" Value="0" />
                        <asp:ListItem Text="Дореволюционный" Value="1" />
                        <asp:ListItem Text="Послереволюционный" Value="2" />
                        <asp:ListItem Text="Без периода" Value="3" Selected="True" />
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="Panel_Kind" runat="server">
        <table width="100%" cellspacing="4">
            <colgroup>
                <col width="110px" />
            </colgroup>
            <tr>
                <td>
                    Объект учета
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="Prop_Kind">
                        <asp:ListItem Text="Фонд" Value="701" Selected="True" />
                        <asp:ListItem Text="Опись" Value="702" />
                        <asp:ListItem Text="Единица хранения" Value="703" />
                        <asp:ListItem Text="Единица учета" Value="704" />
                        <asp:ListItem Text="Документ" Value="705" />
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="Panel_Kind2" runat="server">
        <table width="100%" cellspacing="4">
            <colgroup>
                <col width="110px" />
            </colgroup>
            <tr>
                <td>
                    Поступление
                </td>
                <td>
                    <asp:DropDownList ID="Prop_Kind2" runat="server" AutoPostBack="true">
                        <asp:ListItem Value="1" Text="Все фонды" Selected="True" />
                        <asp:ListItem Value="2" Text="Фонды с первым поступлением" />
                        <asp:ListItem Value="3" Text="Фонды с новыми поступлениями" />
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="Panel_Children" runat="server">
        <table width="100%" cellspacing="4">
            <colgroup>
                <col width="110px" />
            </colgroup>
            <tr>
                <td>
                    Учитывать подчиненые рубрики
                </td>
                <td>
                    <asp:CheckBox ID="Prop_Children" runat="server" Checked="true" />
                   
                </td>
            </tr>
             <tr>
                <td>
                    Учитывать рубрики без фондов
                </td>
                <td>
                    <asp:CheckBox ID="Prop_Children_All" runat="server"  Checked="true"  />
                  
                </td>
            </tr>           
        </table>
    </asp:Panel>
    <asp:Panel ID="Panel_Check" runat="server">
        <table width="100%" cellspacing="4">
            <colgroup>
                <col width="110px" />
            </colgroup>
            <tr>
                <td>
                    Проверять
                </td>
                <td>
                    <asp:CheckBox ID="Prop_Fund" runat="server" Text="Фонды" Checked="true" />
                    <asp:CheckBox ID="Prop_FundPres" runat="server" Text="Только в наличии" />
                    <br />
                    <asp:CheckBox ID="Prop_Inventory" runat="server" Text="Описи" Checked="true" />
                    <asp:CheckBox ID="Prop_InvPres" runat="server" Text="Только в наличии" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Panel>
<aj:AlwaysVisibleControlExtender ID="Panel_Properties_Ex" runat="server" TargetControlID="Panel_Properties"
    HorizontalSide="Right" VerticalSide="Top" HorizontalOffset="120" VerticalOffset="240" />
<span id="report_tag"></span>