﻿'------------------------------------------------------------------------------
' <автоматически создаваемое>
'     Этот код создан программой.
'
'     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
'     повторной генерации кода. 
' </автоматически создаваемое>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class ShowDocumentHistory

    '''<summary>
    '''HeaderPanel элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents HeaderPanel As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''DocTypeIDPanel элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents DocTypeIDPanel As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''ddlDocTypeID элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents ddlDocTypeID As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''LabelDoc элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents LabelDoc As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ParameretsPanel элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents ParameretsPanel As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''lblStartDate элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents lblStartDate As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblFinishDate элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents lblFinishDate As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''StartDate элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents StartDate As Global.Equipage.WebUI.eqTextBox

    '''<summary>
    '''CExtSpStartDate элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents CExtSpStartDate As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''FinishDate элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents FinishDate As Global.Equipage.WebUI.eqTextBox

    '''<summary>
    '''CalendarExtender1 элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents CalendarExtender1 As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''ChooseUserAndFieldsPanel элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents ChooseUserAndFieldsPanel As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''lblAuthor элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents lblAuthor As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ddlAuthor элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents ddlAuthor As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''lblField элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents lblField As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ddlField элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents ddlField As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''btnGenerateReport элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents btnGenerateReport As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Tab элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents Tab As Global.AjaxControlToolkit.TabContainer

    '''<summary>
    '''StatusHistoryTab элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents StatusHistoryTab As Global.AjaxControlToolkit.TabPanel

    '''<summary>
    '''StatusHistoryPanel элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents StatusHistoryPanel As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''LocalizeHistotyByDocStatuses элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents LocalizeHistotyByDocStatuses As Global.System.Web.UI.WebControls.Localize

    '''<summary>
    '''GridViewStatusHistory элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents GridViewStatusHistory As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''FieldsHistoryTab элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents FieldsHistoryTab As Global.AjaxControlToolkit.TabPanel

    '''<summary>
    '''FieldsHistoryPanel элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents FieldsHistoryPanel As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''LocalizeHistoryByDocFields элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents LocalizeHistoryByDocFields As Global.System.Web.UI.WebControls.Localize

    '''<summary>
    '''GridViewFieldHistory элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents GridViewFieldHistory As Global.System.Web.UI.WebControls.GridView
End Class
