﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UNIT2.ascx.vb" Inherits="WebApplication.UNIT2" %>
<table style="width: 97%; margin-bottom: 4px;">
    <tr>
        <td>
            <eq2:ArchiveCrumbs ID="Crumbs" runat="server" Level="INVENTORY" />
        </td>
        <td style="text-align: right;">
            Раздел описи:
            <eq2:eqDocument ID="ISN_INVENTORY_CLS" runat="server" DocTypeURL="INVENTORYSTRUCTURE.ascx"
                ListDocMode="NewWindowAndSelect" LoadDocMode="NewWindowAndSelect" ShowingFields="NAME,ISN_INVENTORY"
                ShowingFieldsFormat="{0}" KeyName="ISN_INVENTORY_CLS" />
        </td>
        <td style="text-align: right;">
            Тип:
            <asp:DropDownList ID="UNIT_KIND" runat="server" Width="150" Enabled="false"/>
        </td>
        <td style="text-align: right;">
            Содержимое:
            <asp:HyperLink ID="HyperLinkDOCUMENTs" runat="server" Text="Документы" Target="_blank"
                NavigateUrl="~/Default.aspx?DocTypeURL=DOCUMENT.ascx&ListDoc=True" Style="font-weight: bold;
                padding: 2px;" />
        </td>
    </tr>
</table>
<div style="border: solid 1px black; padding: 5px;">
    <table style="width: 700px;">
        <tr id="Field_DocType">
            <td  style="width: 100px; text-align: right;">
                Тип документов*:
            </td>
            <td style="width: 250px;">
                <asp:DropDownList ID="ISN_DOC_TYPE" runat="server" Width="100%" AutoPostBack="true"/>
            </td>
        </tr>
    </table>
    <asp:FormView ID="FOTO" runat="server">
        <ItemTemplate>
            <table style="width: 700px;">
                <tr>
                    <td style="width: 150px; text-align: right;">
                        Номер единицы хранения*:
                    </td>
                    <td style="width: 30px;">
                        <eq:eqTextBox ID="UNIT_NUM_2" runat="server" MaxLength="2" />
                        <aj:FilteredTextBoxExtender ID="FOTO_UNIT_NUM_2" runat="server" TargetControlID="UNIT_NUM_2"
                            FilterType="Custom" ValidChars="АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЬЫЪЭЮЯ" />
                    </td>
                    <td style="width: 80px;">
                        <eq:eqTextBox ID="UNIT_NUM_1" runat="server" MaxLength="8" />
                        <aj:FilteredTextBoxExtender ID="FOTO_UNIT_NUM_1" runat="server" TargetControlID="UNIT_NUM_1"
                            FilterType="Numbers" />
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
            <table style="width: 700px;">
                <tr>
                    <td style="width: 150px; text-align: right;">
                        Производственный номер:
                    </td>
                    <td>
                        <eq:eqTextBox ID="PRODUCTION_NUM" runat="server" Width="100" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 150px; text-align: right;">
                        Темы, Персоны, Ключевые слова:
                    </td>
                    <td>
                        <eq:eqTextBox ID="ADDITIONAL_CLS" runat="server" TextMode="MultiLine" Rows="2" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 150px; text-align: right;">
                        Аннотация:
                    </td>
                    <td>
                        <eq:eqTextBox ID="ANNOTATE" runat="server" TextMode="MultiLine" Rows="2" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 150px; text-align: right;">
                        Примечание:
                    </td>
                    <td>
                        <eq:eqTextBox ID="NOTE" runat="server" TextMode="MultiLine" Rows="2" />
                    </td>
                </tr>
            </table>
            
            <table style="width: 700px;">
                <tr>
                    <td>
                        <fieldset>
                            <legend>Файлы электронного образа</legend>
                            <eq:eqSpecification ID="vREF_FILE_UNIT_S" runat="server" Width="100%" AllowInsert="false">
                                <eq:eqSpecificationColumn HeaderText="Описание">
                                    <ItemTemplate>
                                        <asp:TextBox ID="NAME" runat="server" Width="99%" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Файл">
                                    <ItemTemplate>
                                        <eq2:RefFile ID="ID" runat="server" Mode="FileSystem" OnRequesting="RefFileS_Requesting" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                            <table>
                                <tr>
                                    <td>
                                        Добавить файл:
                                    </td>
                                    <td>
                                        <eq2:RefFileUpload ID="RefFileUploadFileS" runat="server" Mode="FileSystem" OnUploaded="UploadedFoto" OnUploading="UploadingFoto" />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
            </table>
                   
        </ItemTemplate>
    </asp:FormView>
    <eq:eqSpecification ID="tblUNIT_FOTO" runat="server" SkinID="Empty">
        <eq:eqSpecificationColumn>
            <ItemTemplate>
                <table style="width: 700px;">
                    <tr>
                        <td style="width: 150px; text-align: right;">
                            Размер:
                        </td>
                        <td>
                            <eq:eqTextBox ID="SIZE" runat="server" Width="200px" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px; text-align: right;">
                            Цветность:
                        </td>
                        <td>
                            <asp:DropDownList ID="COLOR" runat="server" Width="200" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px; text-align: right;">
                            Вид фотодокумента:
                        </td>
                        <td>
                            <asp:DropDownList ID="ISN_DOC_KIND" runat="server" Width="200" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px; text-align: right;">
                            Оригинал/копия:
                        </td>
                        <td>
                            <eq:eqTextBox ID="ORIGINAL" runat="server" Width="150px" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px; text-align: right;">
                            Дата съемки:
                        </td>
                        <td>
                            <eq:eqTextBox ID="FOTO_DATE" runat="server" Width="100px" />
                            <aj:CalendarExtender runat="server" TargetControlID="FOTO_DATE" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px; text-align: right;">
                            Место съемки:
                        </td>
                        <td>
                            <eq:eqTextBox ID="PLACE" runat="server" TextMode="MultiLine" Rows="2" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px; text-align: right;">
                            Авторы съемки:
                        </td>
                        <td>
                            <eq:eqTextBox ID="AUTHOR" runat="server" TextMode="MultiLine" Rows="2" />
                        </td>
                    </tr>
                </table>   
            </ItemTemplate>
        </eq:eqSpecificationColumn>
    </eq:eqSpecification>
    <eq:eqSpecification ID="tblUNIT_FOTO_EX" runat="server" SkinID="Empty">
        <eq:eqSpecificationColumn>
            <ItemTemplate>
                <table style="width: 700px;">
                    <tr>
                        <td style="width: 150px; text-align: right;">
                            Шифр:
                        </td>
                        <td>
                            <eq:eqTextBox ID="CODE" runat="server" Width="100px" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px; text-align: right;">
                            Рубрики, Индексы:
                        </td>
                        <td>
                            <eq:eqTextBox ID="RUBRIC" runat="server" TextMode="MultiLine" Rows="2" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px; text-align: right;">
                            Источник поступления:
                        </td>
                        <td>
                            <eq:eqTextBox ID="SOURCE" runat="server" TextMode="MultiLine" Rows="2" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px; text-align: right;">
                            Точный год начала съемки:
                        </td>
                        <td>
                            <eq:eqTextBox ID="START_YEAR" runat="server" Width="100px" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px; text-align: right;">
                            Точный год окончания съемки:
                        </td>
                        <td>
                            <eq:eqTextBox ID="END_YEAR" runat="server" Width="100px" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px; text-align: right;">
                            Количество снимков в единице хранения:
                        </td>
                        <td>
                            <eq:eqTextBox ID="COUNT" runat="server" Width="100px" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px; text-align: right;">
                            Составитель:
                        </td>
                        <td>
                            <eq:eqTextBox ID="COMPILER" runat="server" Width="200px" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px; text-align: right;">
                            Дата составления карточки:
                        </td>
                        <td>
                            <eq:eqTextBox ID="FOTO_DATE" runat="server" />
                            <aj:CalendarExtender runat="server" TargetControlID="FOTO_DATE" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px; text-align: right;">
                            Публикации:
                        </td>
                        <td>
                            <eq:eqTextBox ID="PUBLICATION" runat="server" TextMode="MultiLine" Rows="2" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px; text-align: right;">
                            Изображение:
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
            </ItemTemplate>
        </eq:eqSpecificationColumn>
    </eq:eqSpecification>
    <asp:FormView ID="MOVIE" runat="server" Visible="true">
        <ItemTemplate>
            <table style="width: 700px;">
                <tr>
                    <td style="width: 150px; text-align: right;">
                        Номер учетный:
                    </td>
                    <td style="width: 80px; text-align: right;">
                        <eq:eqTextBox ID="UNIT_NUM_2" runat="server" MaxLength="2" Width="30px" />
                        <aj:FilteredTextBoxExtender ID="MOVIE_UNIT_NUM_2" runat="server" TargetControlID="UNIT_NUM_2"
                            FilterType="Custom" ValidChars="АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЬЫЪЭЮЯ" />
                    </td>
                    <td style="width: 80px;">
                        <eq:eqTextBox ID="UNIT_NUM_1" runat="server" MaxLength="8" />
                        <aj:FilteredTextBoxExtender ID="MOVIE_UNIT_NUM_1" runat="server" TargetControlID="UNIT_NUM_1"
                            FilterType="Numbers" />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td style="width: 150px; text-align: right;">
                        Дата выпуска:
                    </td>
                    <td style="width: 80px;">
                        <eq:eqTextBox ID="START_YEAR" runat="server" />
                        <aj:FilteredTextBoxExtender runat="server" TargetControlID="START_YEAR" FilterType="Numbers" />
                    </td>
                    <td style="width: 80px;">
                        <eq:eqTextBox ID="END_YEAR" runat="server" />
                        <aj:FilteredTextBoxExtender runat="server" TargetControlID="END_YEAR" FilterType="Numbers" />
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
            <table style="width: 700px;">
                <tr>
                    <td style="width: 150px; text-align: right;">
                        Производственный номер:
                    </td>
                    <td>
                        <eq:eqTextBox ID="PRODUCTION_NUM" runat="server" Width="100" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 150px; text-align: right;">
                        Название*:
                    </td>
                    <td>
                        <eq:eqTextBox ID="NAME" runat="server" TextMode="MultiLine" Rows="2" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 150px; text-align: right;">
                        Вид киноматериала:
                    </td>
                    <td>
                        к/л
                    </td>
                </tr>
                <tr>
                    <td style="width: 150px; text-align: right;">
                        Аннотация:
                    </td>
                    <td>
                        <eq:eqTextBox ID="ANNOTATE" runat="server" TextMode="MultiLine" Rows="2" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 150px; text-align: right;">
                        Ключевые слова:
                    </td>
                    <td>
                        <eq:eqTextBox ID="ADDITIONAL_CLS" runat="server" TextMode="MultiLine" Rows="2" />
                    </td>
                </tr>
            </table>
        </ItemTemplate>
    </asp:FormView>
    <eq:eqSpecification ID="tblUNIT_MOVIE" runat="server" SkinID="Empty">
        <eq:eqSpecificationColumn>
            <ItemTemplate>
                <table style="width: 700px;">
                    <tr>
                        <td style="width: 150px; text-align: right;">
                            Количество частей:
                        </td>
                        <td>
                            <eq:eqTextBox ID="PART_COUNT" runat="server" Width="100px" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px; text-align: right;">
                            Метраж:
                        </td>
                        <td>
                            <eq:eqTextBox ID="FOOTAGE" runat="server" Width="100px" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px; text-align: right;">
                            Звук:
                        </td>
                        <td>
                            <asp:DropDownList ID="SOUND" runat="server" Width="200" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px; text-align: right;">
                            Цвет:
                        </td>
                        <td>
                            <asp:DropDownList ID="COLOR" runat="server" Width="200" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px; text-align: right;">
                            Формат:
                        </td>
                        <td>
                            <asp:DropDownList ID="FILM_FORMAT" runat="server" Width="200" />
                        </td>
                    </tr>
                </table>
            </ItemTemplate>
        </eq:eqSpecificationColumn>
    </eq:eqSpecification>
    <eq:eqSpecification ID="tblUNIT_MOVIE_EX" runat="server" SkinID="Empty">
        <eq:eqSpecificationColumn>
            <ItemTemplate>
                <table style="width: 700px;">
                    <tr>
                        <td style="width: 150px; text-align: right;">
                            Номер сюжета:
                        </td>
                        <td>
                            <eq:eqTextBox ID="STORY_NUM" runat="server" Width="100px" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px; text-align: right;">
                            Студия:
                        </td>
                        <td>
                            <eq:eqTextBox ID="STUDIO" runat="server" TextMode="MultiLine" Rows="2" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px; text-align: right;">
                            Режиссеры:
                        </td>
                        <td>
                            <eq:eqTextBox ID="DIRECTOR" runat="server" TextMode="MultiLine" Rows="2" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px; text-align: right;">
                            Операторы:
                        </td>
                        <td>
                            <eq:eqTextBox ID="OPERATOR" runat="server" TextMode="MultiLine" Rows="2" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px; text-align: right;">
                            Прочие создатели:
                        </td>
                        <td>
                            <eq:eqTextBox ID="CREATOR" runat="server" TextMode="MultiLine" Rows="2" />
                        </td>
                    </tr>
                </table>
            </ItemTemplate>
        </eq:eqSpecificationColumn>
    </eq:eqSpecification>
    <asp:FormView ID="VIDEO" runat="server">
        <ItemTemplate>
            <table style="width: 700px;">
                <tr>
                    <td style="width: 150px; text-align: right;">
                        Номер учетный:
                    </td>
                    <td style="width: 80px; text-align: right;">
                        <eq:eqTextBox ID="UNIT_NUM_2" runat="server" MaxLength="2" Width="30px" />
                        <aj:FilteredTextBoxExtender ID="VIDEO_UNIT_NUM_2" runat="server" TargetControlID="UNIT_NUM_2"
                            FilterType="Custom" ValidChars="АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЬЫЪЭЮЯ" />
                    </td>
                    <td style="width: 80px;">
                        <eq:eqTextBox ID="UNIT_NUM_1" runat="server" MaxLength="8" />
                        <aj:FilteredTextBoxExtender ID="VIDEO_UNIT_NUM_1" runat="server" TargetControlID="UNIT_NUM_1"
                            FilterType="Numbers" />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td style="width: 150px; text-align: right;">
                        Дата выпуска:
                    </td>
                    <td style="width: 80px;">
                        <eq:eqTextBox ID="START_YEAR" runat="server" />
                        <aj:FilteredTextBoxExtender runat="server" TargetControlID="START_YEAR" FilterType="Numbers" />
                    </td>
                    <td style="width: 80px;">
                        <eq:eqTextBox ID="END_YEAR" runat="server" />
                        <aj:FilteredTextBoxExtender runat="server" TargetControlID="END_YEAR" FilterType="Numbers" />
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
            <table style="width: 700px;">
                <tr>
                    <td style="width: 150px; text-align: right;">
                        Производственный номер:
                    </td>
                    <td>
                        <eq:eqTextBox ID="PRODUCTION_NUM" runat="server" Width="100" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 150px; text-align: right;">
                        Название*:
                    </td>
                    <td>
                        <eq:eqTextBox ID="NAME" runat="server" TextMode="MultiLine" Rows="2" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 150px; text-align: right;">
                        Вид киноматериала:
                    </td>
                    <td>
                        в/ф
                    </td>
                </tr>
                <tr>
                    <td style="width: 150px; text-align: right;">
                        Аннотация:
                    </td>
                    <td>
                        <eq:eqTextBox ID="ANNOTATE" runat="server" TextMode="MultiLine" Rows="2" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 150px; text-align: right;">
                        Ключевые слова:
                    </td>
                    <td>
                        <eq:eqTextBox ID="ADDITIONAL_CLS" runat="server" TextMode="MultiLine" Rows="2" />
                    </td>
                </tr>
            </table>
        </ItemTemplate>
    </asp:FormView>
    <eq:eqSpecification ID="tblUNIT_VIDEO" runat="server" SkinID="Empty">
        <eq:eqSpecificationColumn>
            <ItemTemplate>
                <table style="width: 700px;">
                    <tr>
                        <td style="width: 150px; text-align: right;">
                            Количество частей:
                        </td>
                        <td>
                            <eq:eqTextBox ID="PART_COUNT" runat="server" Width="100px" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px; text-align: right;">
                            Звук:
                        </td>
                        <td>
                            <asp:DropDownList ID="SOUND" runat="server" Width="200" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px; text-align: right;">
                            Цвет:
                        </td>
                        <td>
                            <asp:DropDownList ID="COLOR" runat="server" Width="200" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px; text-align: right;">
                            Формат:
                        </td>
                        <td>
                            <eq:eqTextBox ID="REC_FORMAT" runat="server" />
                        </td>
                    </tr>
                </table>
            </ItemTemplate>
        </eq:eqSpecificationColumn>
    </eq:eqSpecification>
    <eq:eqSpecification ID="tblUNIT_VIDEO_EX" runat="server" SkinID="Empty">
        <eq:eqSpecificationColumn>
            <ItemTemplate>
                <table style="width: 700px;">
                    <tr>
                        <td style="width: 150px; text-align: right;">
                            Номер сюжета:
                        </td>
                        <td>
                            <eq:eqTextBox ID="STORY_NUM" runat="server" Width="100px" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px; text-align: right;">
                            Студия:
                        </td>
                        <td>
                            <eq:eqTextBox ID="STUDIO" runat="server" TextMode="MultiLine" Rows="2" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px; text-align: right;">
                            Режиссеры:
                        </td>
                        <td>
                            <eq:eqTextBox ID="DIRECTOR" runat="server" TextMode="MultiLine" Rows="2" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px; text-align: right;">
                            Операторы:
                        </td>
                        <td>
                            <eq:eqTextBox ID="OPERATOR" runat="server" TextMode="MultiLine" Rows="2" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px; text-align: right;">
                            Прочие создатели:
                        </td>
                        <td>
                            <eq:eqTextBox ID="CREATOR" runat="server" TextMode="MultiLine" Rows="2" />
                        </td>
                    </tr>
                </table>
            </ItemTemplate>
        </eq:eqSpecificationColumn>
    </eq:eqSpecification>
</div>
