﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="GR_ARCHIVE.ascx.vb"
    Inherits="WebApplication.GR_ARCHIVE" %>
    
    <table style="width: 97%; margin-bottom: 4px;">
    <tr>
        <td>
            <eq2:ArchiveCrumbs ID="Crumbs" runat="server" Level="DEP" />
            <eq2:ArchiveCrumbs ID="HiddenCrumbs" runat="server" Level="ARCHIVE" Visible="false" />
        </td>
    </tr>
</table>
<div style="border: solid 1px black; padding: 5px;">
    <table style="width: 700px;">
        <colgroup>
            <col style="width: 120px;" />
            <col />
            <col />
            <col />
        </colgroup>
        <tr>
            <td>
                Код
            </td>
            <td colspan="3">
                <eq:eqTextBox ID="CODE" runat="server" Width="200px" />
            </td>
        </tr>
        <tr>
            <td>
                Код ОКПО
            </td>
            <td colspan="3">
                <eq:eqTextBox ID="OKPO" runat="server" Width="200px" />
            </td>
        </tr>
        <tr>
            <td>
                Уровень архива*
            </td>
            <td>
                <asp:DropDownList ID="ARCHIVE_LEVEL" runat="server">
                    <asp:ListItem Text="федеральный" Value="a" />
                    <asp:ListItem Text="региональный" Value="b" />
                    <asp:ListItem Text="муниципальный" Value="c" />
                </asp:DropDownList>
            </td>
            <td>
                Тип архива*
            </td>
            <td>
                <asp:DropDownList ID="ARCHIVE_TYPE" runat="server">
                    <asp:ListItem Text="архив" Value="a" />
                    <asp:ListItem Text="архив л/с" Value="b" />
                </asp:DropDownList>
            </td>
        </tr>
    </table>
    <table style="width: 700px;">
        <colgroup>
            <col valign="top" style="width: 120px;" />
            <col />
        </colgroup>
        <tr>
            <td>
                Сокращенное название*
            </td>
            <td>
                <eq:eqTextBox ID="NAME_SHORT" runat="server" TextMode="MultiLine" Rows="2" />
            </td>
        </tr>
        <tr>
            <td>
                Полное название*
            </td>
            <td>
                <eq:eqTextBox ID="NAME" runat="server" TextMode="MultiLine" Rows="4" />
            </td>
        </tr>
         <tr>
            <td>
                Отображаемое название*
            </td>
            <td>
                <eq:eqTextBox ID="NAME_VISIBLE" runat="server" TextMode="MultiLine" Rows="4" />
            </td>
        </tr>
        <tr>
            <td>
                Федеральный округ / Субъект*
            </td>
            <td>
                <%--СПРАВОЧНИК--%>
                <eq2:eqDocument ID="ISN_SUBJECT" runat="server" DocTypeURL="SUBJECT_CL.ascx" ListDocMode="NewWindowAndSelect"
                    LoadDocMode="ListDocAndSelect" ShowingFields="NAME" ShowingFieldsFormat="{0}"
                    KeyName="ISN_SUBJECT" />
            </td>
        </tr>
          <tr>
            <td>
                Адрес*
            </td>
            <td>
                <eq:eqSpecification ID="tblARCHIVE_ADDRESS" runat="server" ShowNumering="false" Visible="true">
                    <eq:eqSpecificationColumn>
                        <ItemTemplate>
                            <table>
                                <tr>
                                    <td>
                                        <eq:eqTextBox ID="ADDRESS" runat="server" width=500px TextMode="MultiLine"
                                            Visible="true" />
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                    </eq:eqSpecificationColumn>
                </eq:eqSpecification>
            </td>
        </tr>
        <tr>
            <td>
                Название и адрес органа управления
            </td>
            <td>
                <eq:eqTextBox ID="AUTHORITY" runat="server" TextMode="MultiLine" Rows="4" />
            </td>
        </tr>
    </table>
</div>
