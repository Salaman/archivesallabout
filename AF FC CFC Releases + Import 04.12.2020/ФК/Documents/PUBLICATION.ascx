﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="!PUBLICATION.ascx.vb"
    Inherits="WebApplication.PUBLICATION" %>
<table style="width: 700px;">
    <colgroup>
        <col style="width: 150px;" />
        <col />
    </colgroup>
    <tr>
        <td>
            №
        </td>
        <td>
            <eq:eqTextBox ID="PUBLICATION_NUM" runat="server" Width="100" />
        </td>
    </tr>
    <tr>
        <td>
            Тип издания
        </td>
        <td>
            <asp:DropDownList ID="ISN_PUBLICATION_TYPE" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            Название
        </td>
        <td>
            <eq:eqTextBox ID="PUBLICATION_NAME" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            Авторы
        </td>
        <td>
            <eq:eqTextBox ID="AUTHORS" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            Место издания
        </td>
        <td>
            <eq:eqTextBox ID="PUBLICATION_PLACE" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            Издательство
        </td>
        <td>
            <eq:eqTextBox ID="PUBLISHER" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            Год издания
        </td>
        <td>
            <eq:eqTextBox ID="PUBLICATION_YEAR" runat="server" Width="100" />
        </td>
    </tr>
    <tr>
        <td>
            Листов
        </td>
        <td>
            <eq:eqTextBox ID="SHEET_COUNT" runat="server" Width="100" />
        </td>
    </tr>
    <tr>
        <td valign="top">
            Примечание
        </td>
        <td>
            <eq:eqTextBox ID="NOTE" runat="server" TextMode="MultiLine" Rows="4" />
        </td>
    </tr>
</table>
