﻿Public Partial Class GR_ExcelReports
    Inherits BaseDoc
    Public REPORT_ID As Integer
    
    Public Sub Rep_Changed(ByVal sender As Object, ByVal e As System.EventArgs)
        For Each c In Panel_Properties.Controls
            If TypeOf c Is Panel Then
                DirectCast(c, Panel).Visible = False
            End If
        Next
        Panel_SecurityFlags.Visible = True
        REPORT_ID = 0
        If Rep_GR_DOCUMENT.Checked Then ' 
            Panel_ISN_DOCUMENT.Visible = True
            Prop_REPORT_ID.Text = 0
        End If
        If Rep_GR_ListRegisrationDate.Checked Then '1 по времени регистрации
            Panel_Dates.Visible = True
            Prop_REPORT_ID.Text = 1
        End If
        If Rep_GR_ListFederationArchive.Checked Then '2 по федеральномк архиву
            Panel_ISN_ARCHIVE.Visible = True
            Prop_REPORT_ID.Text = 2
        End If
        If Rep_GR_ListRFSubject.Checked Then '3 по субъекту РФ
            Prop_REPORT_ID.Text = 3
            Panel_ISN_SUBJECT.Visible = True
        End If
        If Rep_GR_ListFederationSubject.Checked Then '4 по федеральному округу
            Panel_ISN_SUBJECT.Visible = True
            Prop_REPORT_ID.Text = 4
        End If
        If Rep_GR_ListKindAndDate.Checked Then '5 по виду и датировке
            Panel_Dating_RB.Visible = True
            If Prop_Dating_type.SelectedIndex = 0 Then
                Panel_Year2.Visible = True
                Panel_CENTURY.Visible = False
            End If
            If Prop_Dating_type.SelectedIndex = 1 Then
                Panel_Year2.Visible = False
                Panel_CENTURY.Visible = True
            End If
            Panel_DOC_KIND.Visible = True
            Prop_REPORT_ID.Text = 5
        End If
        If Rep_GR_ListAutor.Checked Then '6 по автору
            Panel_ISN_AUTHOR.Visible = True
            Prop_REPORT_ID.Text = 6
        End If
        If Rep_GR_ListDate.Checked Then '7 по датировке
            If Prop_Dating_type.SelectedIndex = 0 Then
                Panel_Year2.Visible = True
                Panel_CENTURY.Visible = False
            End If
            If Prop_Dating_type.SelectedIndex = 1 Then
                Panel_Year2.Visible = False
                Panel_CENTURY.Visible = True
            End If
            Panel_Dating_RB.Visible = True
            Prop_REPORT_ID.Text = 7
        End If
        If Rep_GR_ListPreciousMetalStones.Checked Then '8 с дрг. камнями и металлами
            Prop_REPORT_ID.Text = 8
        End If
        If Rep_GR_ListSeal.Checked Then '9 с печатями
            Prop_REPORT_ID.Text = 9
        End If
        If Rep_GR_ListRestoration.Checked Then '10 прошедшие реставрацию
            Prop_REPORT_ID.Text = 10
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Rep_Changed(Nothing, EventArgs.Empty)
        End If

    End Sub

    Public Sub GR_MakeReport()
        Dim cmd As New SqlClient.SqlCommand("select 1")
        AppSettings.ExecCommand(cmd)

        Dim ExcelReportGeneratorWork As New ExcelReportGenerator.ExcelReportGenerator
        Dim ewMemoryStream As New IO.MemoryStream
        Dim sReportFileName As String
        Dim sReportFileNameSave As String
        Dim IsHTML As Boolean

        sReportFileName = vbNullString
        sReportFileNameSave = vbNullString

        If Rep_GR_DOCUMENT.Checked Then 'ЛИСТ УЧЕТА И ОПИСАНИЯ УНИКАЛЬНОГО ДОКУМЕНТА
            sReportFileName = "GR_tplDocument.xls"
            sReportFileNameSave = "ЛистУчетаУникальногоДокумента"
            IsHTML = True
        End If

        If Rep_GR_ListRegisrationDate.Checked Then 'Перечень по времени регистрации
            sReportFileName = "GR_tplListRegisrationDate.xls"
            sReportFileNameSave = "ПереченьПоВремениРегистрации"
        End If
        If Rep_GR_ListFederationArchive.Checked Then 'Перечень по федеральному архиву
            sReportFileName = "GR_tplListFederationArchive.xls"
            sReportFileNameSave = "ПереченьПоФедеральномуАрхиву"
        End If
        If Rep_GR_ListRFSubject.Checked Then 'субъекту РФ
            sReportFileName = "GR_tplListRFSubject.xls"
            sReportFileNameSave = "ПереченьПоСубъектуРФ"
        End If
        If Rep_GR_ListFederationSubject.Checked Then 'Перечень по федеральному округу
            sReportFileName = "GR_tplListFederationSubject.xls"
            sReportFileNameSave = "ПереченьПоФедеральномуОкругу"
        End If
        If Rep_GR_ListKindAndDate.Checked Then 'Перечень по виду и датировке
            sReportFileName = "GR_tplListKindAndDate.xls"
            sReportFileNameSave = "ПереченьПоВидуИДдатировке"
        End If
        If Rep_GR_ListAutor.Checked Then 'Перечень по автору
            sReportFileName = "GR_tplListAutor.xls"
            sReportFileNameSave = "ПереченьПоАвтору"
        End If
        If Rep_GR_ListDate.Checked Then 'Перечень по датировке
            sReportFileName = "GR_tplListDate.xls"
            sReportFileNameSave = "ПереченьПоДатировке"
        End If
        If Rep_GR_ListPreciousMetalStones.Checked Then 'Перечень с драг.камнями и металлами
            sReportFileName = "GR_tplListPreciousMetalStones.xls"
            sReportFileNameSave = "ПереченьСДрагКамнямиИМеталлами"
        End If
        If Rep_GR_ListSeal.Checked Then 'Перечень с печатями
            sReportFileName = "GR_tplListSeal.xls"
            sReportFileNameSave = "ПереченьСПечатями"
        End If
        If Rep_GR_ListRestoration.Checked Then 'Перечень прошедшие реставрацию
            sReportFileName = "GR_tplListRestoration.xls"
            sReportFileNameSave = "ПереченьПрошедшиеРеставрацию"
        End If
        If Not IsDBNull(sReportFileName) Then
            If IsDBNull(sReportFileNameSave) Then
                sReportFileNameSave = "НазваниеНеОпределено"
            End If

            Dim arrParameterValueBox(24) As String
            Dim arrParameterWebBox(24) As String
            SetParameterValueBox(arrParameterValueBox, arrParameterWebBox)

            ExcelReportGeneratorWork.ExcelReportGeneration(AppSettings, ewMemoryStream, _
                                                                    sReportFileName, _
                                                                    arrParameterValueBox, arrParameterWebBox)
            If IsHTML = True Then
                BasePage.AttachFile(sReportFileNameSave & "_" & Now.ToString("yyyy.MM.dd_HH.mm.ss") & ".doc", ewMemoryStream.ToArray())
            Else
                BasePage.AttachFile(sReportFileNameSave & "_" & Now.ToString("yyyy.MM.dd_HH.mm.ss") & ".xls", ewMemoryStream.ToArray())
            End If
            ewMemoryStream.Close()
        End If

    End Sub

    Public Sub SetParameterValueBox(ByVal ParameterValueBox() As String, ByVal ParameterNameBox() As String)
        Dim i As Integer
        For i = 0 To 24
            ParameterValueBox(i) = -1
        Next
        For i = 0 To 24
            ParameterNameBox(i) = ""
        Next

        i = 0
        'If Prop_SecurityFlags.Items(0).Selected Then
        '    ParameterValueBox(i) = Prop_SecurityFlags.Items(0).Value
        '    ParameterNameBox(i) = "Prop_ISN_SECURLEVEL"
        'End If
        'i = i + 1
        'If Prop_SecurityFlags.Items(1).Selected Then
        '    ParameterValueBox(i) = Prop_SecurityFlags.Items(1).Value
        '    ParameterNameBox(i) = "Prop_ISN_SECURLEVEL1"
        'End If
        'i = i + 1
        'If Prop_SecurityFlags.Items(2).Selected Then
        '    ParameterValueBox(i) = Prop_SecurityFlags.Items(2).Value
        '    ParameterNameBox(i) = "Prop_ISN_SECURLEVEL2"
        'End If
        'i = i + 1

        If Len(Prop_ID_DOCUMENT.KeyValue.ToString) > 0 Then
            ParameterValueBox(i) = Prop_ID_DOCUMENT.KeyValue.ToString
            ParameterNameBox(i) = "Prop_ID_DOCUMENT"
        End If
        i = i + 1


        If Len(Prop_Year.Text) > 0 Then
            ParameterValueBox(i) = CLng(Prop_Year.Text)
            ParameterNameBox(i) = "Prop_Year"
        End If
        i = i + 1
        If Len(Prop_Year1.Text) > 0 Then
            ParameterValueBox(i) = CLng(Prop_Year1.Text)
            ParameterNameBox(i) = "Prop_Year1"
        End If
        i = i + 1
        If Len(Prop_Year2.Text) > 0 Then
            ParameterValueBox(i) = CLng(Prop_Year2.Text)
            ParameterNameBox(i) = "Prop_Year2"
        End If
        i = i + 1

        If Len(CStr(Prop_DOC_DATE1.Text)) > 0 Then
            ParameterValueBox(i) = CStr(Prop_DOC_DATE1.Text)
            ParameterNameBox(i) = "Prop_DOC_DATE1"
        End If
        i = i + 1
        If Len(CStr(Prop_DOC_DATE2.Text)) > 0 Then
            ParameterValueBox(i) = CStr(Prop_DOC_DATE2.Text)
            ParameterNameBox(i) = "Prop_DOC_DATE2"
        End If
        i = i + 1
        'If Prop_Dating_type.SelectedIndex.ToString Then
        ParameterValueBox(i) = Prop_Dating_type.SelectedIndex.ToString
        ParameterNameBox(i) = "Prop_Dating_type"
        ' End If
        i = i + 1
        If ID_CENTURY_FROM.SelectedIndex Then
            ParameterValueBox(i) = ID_CENTURY_FROM.SelectedValue
            ParameterNameBox(i) = "Prop_ID_CENTURY1"
        End If
        i = i + 1

        If ID_CENTURY_TO.SelectedIndex Then
            ParameterValueBox(i) = ID_CENTURY_TO.SelectedValue
            ParameterNameBox(i) = "Prop_ID_CENTURY2"
        End If
        i = i + 1

        If Len(Prop_ID_ARCHIVE.KeyValue.ToString) > 0 Then
            ParameterValueBox(i) = Prop_ID_ARCHIVE.KeyValue.ToString
            ParameterNameBox(i) = "Prop_ID_ARCHIVE"
        End If
        i = i + 1

        If Prop_ISN_SUBJECT.KeyValue > 0 Then
            ParameterValueBox(i) = Prop_ISN_SUBJECT.KeyValue
            ParameterNameBox(i) = "Prop_ISN_SUBJECT"
        End If
        i = i + 1

        If Len(Prop_ID_DOC_KIND.KeyValue.ToString) > 0 Then
            ParameterValueBox(i) = Prop_ID_DOC_KIND.KeyValue.ToString
            ParameterNameBox(i) = "Prop_ID_DOC_KIND"
        End If
        i = i + 1
        If Len(Prop_ID_AUTHOR.KeyValue.ToString) > 0 Then
            ParameterValueBox(i) = Prop_ID_AUTHOR.KeyValue.ToString
            ParameterNameBox(i) = "Prop_ID_AUTHOR"
        End If
        i = i + 1

        ParameterValueBox(i) = Prop_REPORT_ID.Text
        ParameterNameBox(i) = "Prop_REPORT_ID"
        i = i + 1

        Debug.Print("Parameters")
        For i = 0 To 24
            Debug.Print(ParameterNameBox(i) + "  " + CStr(ParameterValueBox(i)))
        Next
    End Sub

   
    Private Sub Prop_Dating_type_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Prop_Dating_type.SelectedIndexChanged
        If Prop_Dating_type.SelectedIndex = 0 Then
            Panel_Year2.Visible = True
            Panel_CENTURY.Visible = False
        End If
        If Prop_Dating_type.SelectedIndex = 1 Then
            Panel_Year2.Visible = False
            Panel_CENTURY.Visible = True
        End If
    End Sub
End Class
