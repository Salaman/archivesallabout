﻿Public Partial Class FC_ExcelReports
    Inherits BaseDoc

    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Rep_Changed(Nothing, EventArgs.Empty)
        End If
    End Sub

    Public Sub Rep_Changed(ByVal sender As Object, ByVal e As System.EventArgs)
        For Each c In Panel_Properties.Controls
            If TypeOf c Is Panel Then
                DirectCast(c, Panel).Visible = False
            End If
        Next
        If Rep_PassportArchive.Checked Then
            Panel_ISN_ARCHIVE.Visible = True
            Panel_Year.Visible = True
        ElseIf Rep_ArchiveDynamics_Abs.Checked Then
            Panel_ArchiveLevel.Visible = True
            Panel_Arch.Visible = True
            Panel_Year2.Visible = True
        ElseIf Rep_ArchiveDynamics_Pct.Checked Then
            Panel_ArchiveLevel.Visible = True
            Panel_Arch.Visible = True
            Panel_Year2.Visible = True
        ElseIf Rep_ConsolidatedPassportArchive.Checked Then
            Panel_ArchiveLevel.Visible = True
            Panel_Arch.Visible = True
            Panel_Year.Visible = True
        End If

    End Sub

    Public Sub MakeReport()
        Dim cmd As New SqlClient.SqlCommand("select 1")
        AppSettings.ExecCommand(cmd)

        Dim ExcelReportGeneratorWork As New ExcelReportGenerator.ExcelReportGenerator
        Dim ewMemoryStream As New IO.MemoryStream
        Dim sReportFileName As String
        Dim sReportFileNameSave As String
        Dim IsHTML As Boolean

        sReportFileName = vbNullString
        sReportFileNameSave = vbNullString

        If Rep_PassportArchive.Checked Then 'паспорт архива
            sReportFileName = "FC_tplArchivePassport.xls"
            sReportFileNameSave = "ПаспортАрхива"
        End If

        If Rep_ConsolidatedPassportArchive.Checked Then 'паспорт архива
            sReportFileName = "FC_tplConsolidatedPassportArchive.xls"
            sReportFileNameSave = "СводныйПаспортАрхива"
        End If


        If Rep_ArchiveDynamics_Abs.Checked Then 'динамика паспорта архива абсолютная
            sReportFileName = "FC_tplArchiveDynamicAbs.xls"
            sReportFileNameSave = "ДинамикаПаспортаАрхиваАбс"
        End If

        If Rep_ArchiveDynamics_Pct.Checked Then 'динамика паспорта архива проценты
            sReportFileName = "FC_tplArchiveDynamicPct.xls"
            sReportFileNameSave = "ДинамикаПаспортаАрхиваПрц"
        End If


        If Not IsDBNull(sReportFileName) Then
            If IsDBNull(sReportFileNameSave) Then
                sReportFileNameSave = "НазваниеНеОпределено"
            End If

            Dim arrParameterValueBox(24) As String
            Dim arrParameterWebBox(24) As String
            SetParameterValueBox(arrParameterValueBox, arrParameterWebBox)

            ExcelReportGeneratorWork.ExcelReportGeneration(AppSettings, ewMemoryStream, _
                                                                    sReportFileName, _
                                                                    arrParameterValueBox, arrParameterWebBox)
            If IsHTML = True Then
                BasePage.AttachFile(sReportFileNameSave & "_" & Now.ToString("yyyy.MM.dd_HH.mm.ss") & ".doc", ewMemoryStream.ToArray())
            Else
                BasePage.AttachFile(sReportFileNameSave & "_" & Now.ToString("yyyy.MM.dd_HH.mm.ss") & ".xls", ewMemoryStream.ToArray())
            End If
            ewMemoryStream.Close()
        End If
    End Sub

    Public Sub SetParameterValueBox(ByVal ParameterValueBox() As String, ByVal ParameterNameBox() As String)
        'надо переделать на списки
        Dim i As Integer
        For i = 0 To 24
            ParameterValueBox(i) = -1
        Next
        For i = 0 To 24
            ParameterNameBox(i) = ""
        Next

        i = 0
        If Len(Prop_ID_ARCHIVE.KeyValue.ToString) > 0 Then
            ParameterValueBox(i) = Prop_ID_ARCHIVE.KeyValue.ToString
            ParameterNameBox(i) = "Prop_ID_ARCHIVE"
        End If
        i = i + 1

        If Len(Prop_ID_ARCHIVE_MULTI.Text) > 0 Then
            ParameterValueBox(i) = Prop_ID_ARCHIVE_MULTI.Text
            ParameterNameBox(i) = "Prop_ID_ARCHIVE_MULTI"
        End If
        i = i + 1
        If Len(Prop_NAME_ARCHIVE.Text) > 0 Then
            ParameterValueBox(i) = Prop_NAME_ARCHIVE.Text
            ParameterNameBox(i) = "Prop_NAME_ARCHIVE"
        End If
        i = i + 1

        If Prop_Year.Text Then
            ParameterValueBox(i) = CLng(Prop_Year.Text)
            ParameterNameBox(i) = "Prop_Year"
        End If
        i = i + 1
        If Prop_Year1.Text Then
            ParameterValueBox(i) = CLng(Prop_Year1.Text)
            ParameterNameBox(i) = "Prop_Year1"
        End If
        i = i + 1
        If Prop_Year2.Text Then
            ParameterValueBox(i) = CLng(Prop_Year2.Text)
            ParameterNameBox(i) = "Prop_Year2"
        End If
        i = i + 1

        If Prop_ArchiveLevel.Text Then
            ParameterValueBox(i) = CLng(Prop_ArchiveLevel.SelectedValue)
            ParameterNameBox(i) = "Prop_Period"
        End If
        i = i + 1
        Debug.Print("Parameters")
        For i = 0 To 24
            Debug.Print(ParameterNameBox(i) + "  " + CStr(ParameterValueBox(i)))
        Next
    End Sub


    Private Sub GetArchives_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles GetArchives.Click
        Dim add As String
        add = "ARCHIVE_LEVEL_MUNICIPALITY=c"
        BasePage.OpenModalDialogWindow(Me, "ChooseArchives.aspx?DocTypeURL=" & DocTypeURL & add, "SelectDoc")

    End Sub

    'Private Sub GetArchives2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles GetArchives2.Click
    'кнопка выбора архивов напрямую из формы архива
    '    Dim add As String
    '    add = "ARCHIVE_LEVEL_MUNICIPALITY=c&SingleAndMulti=1"
    '    BasePage.OpenModalDialogWindow(Me, BasePage.GetListDocURL("GR_ARCHIVE.ascx", False, add), "SelectDoc")

    'End Sub

    Private Sub Page_ModalDialogClosed(ByVal Argument As Equipage.WebUI.eqModalDialogArgument) Handles Me.ModalDialogClosed

        Dim selectedDocs = DirectCast(Argument.ArgumentObject, List(Of Guid))

        If selectedDocs.Count > 0 Then

            'получился список ID выбранных архивов
            Dim SQLString As New StringBuilder

            SQLString.AppendLine("    SELECT     A.ISN_ARCHIVE, CASE isnull(SH.NAME,'null') WHEN 'Субъекты и ФО' THEN S.NAME WHEN 'null' THEN A.NAME ELSE SH.NAME  + '/' + S.NAME + '/' + A.NAME END AS FULL_SUBJ_NAME ")
            SQLString.AppendLine("FROM         dbo.tblSUBJECT_CL AS SH INNER JOIN ")
            SQLString.AppendLine("                     dbo.tblSUBJECT_CL AS S ON SH.ISN_SUBJECT = S.ISN_HIGH_SUBJECT RIGHT OUTER JOIN ")
            SQLString.AppendLine("                     dbo.tblARCHIVE AS A ON S.ISN_SUBJECT = A.ISN_SUBJECT ")
            SQLString.AppendLine("  WHERE(A.ID = @ID) ")

            Dim cmd As New SqlClient.SqlCommand(SQLString.ToString)

            Dim DataTable = AppSettings.GetDataTable("SELECT 0")

            DataTable.Clear()
            Dim arch As String
            Dim i As Integer
            i = 1
            Prop_NAME_ARCHIVE.Height = selectedDocs.Count * 50
            Prop_NAME_ARCHIVE.Text = Nothing
            Prop_ID_ARCHIVE_MULTI.Text = ""
            'выберем их названия и запишем в видимое поле
            For Each selectedDoc In selectedDocs
                cmd.Parameters.Clear()
                DataTable.Clear()
                cmd.Parameters.AddWithValue("@ID", selectedDoc)
                DataTable = AppSettings.GetDataTable(cmd)
                arch = DataTable.Rows(0)("FULL_SUBJ_NAME")
                Prop_NAME_ARCHIVE.Text = Prop_NAME_ARCHIVE.Text + i.ToString + "." + arch & vbCrLf
                'Prop_ID_ARCHIVE_MULTI.Text = Prop_ID_ARCHIVE_MULTI.Text + selectedDoc.ToString + "','"
                Prop_ID_ARCHIVE_MULTI.Text = Prop_ID_ARCHIVE_MULTI.Text + DataTable.Rows(0)("ISN_ARCHIVE").ToString + ","
                Debug.Print(selectedDoc.ToString)
                i = i + 1
            Next

            Prop_ID_ARCHIVE_MULTI.Text = Left(Prop_ID_ARCHIVE_MULTI.Text, Prop_ID_ARCHIVE_MULTI.Text.Length - 1)
        End If
    End Sub

    Private Sub Prop_ArchiveLevel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Prop_ArchiveLevel.SelectedIndexChanged
        If Prop_ArchiveLevel.SelectedIndex = 3 Then
            Prop_NAME_ARCHIVE.Visible = True
            GetArchives.Visible = True
            Prop_ID_ARCHIVE_MULTI.Text = Nothing
            Prop_NAME_ARCHIVE.Text = Nothing
        ElseIf Prop_ArchiveLevel.SelectedIndex = 0 Then
            GetArchives.Visible = False
            Prop_ID_ARCHIVE_MULTI.Text = Nothing
            Prop_NAME_ARCHIVE.Text = Prop_ArchiveLevel.SelectedItem.Text
            Prop_ID_ARCHIVE_MULTI.Text = "SELECT ISN_ARCHIVE FROM tblARCHIVE WHERE DELETED=0"
        ElseIf Prop_ArchiveLevel.SelectedIndex = 1 Then
            GetArchives.Visible = False
            Prop_ID_ARCHIVE_MULTI.Text = Nothing
            Prop_NAME_ARCHIVE.Text = Prop_ArchiveLevel.SelectedItem.Text
            Prop_ID_ARCHIVE_MULTI.Text = "SELECT ISN_ARCHIVE FROM tblARCHIVE WHERE DELETED=0 AND ARCHIVE_LEVEL='b'"
        ElseIf Prop_ArchiveLevel.SelectedIndex = 2 Then
            GetArchives.Visible = False
            Prop_ID_ARCHIVE_MULTI.Text = Nothing
            Prop_NAME_ARCHIVE.Text = Prop_ArchiveLevel.SelectedItem.Text
            Prop_ID_ARCHIVE_MULTI.Text = "SELECT ISN_ARCHIVE FROM tblARCHIVE WHERE DELETED=0 AND ARCHIVE_LEVEL='c'"
        End If
    End Sub

End Class