﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="GR_DOCUMENT.ascx.vb"
    Inherits="WebApplication.GR_DOCUMENT" Explicit="true" %>
<table style="width: 97%; margin-bottom: 4px;">
</table>
<table style="width: 700px;">
    <colgroup>
        <col width="150px" />
        <col />
    </colgroup>
    <tr>
        <td>
            ЛИСТ УЧЕТА И ОПИСАНИЯ УНИКАЛЬНОГО ДОКУМЕНТА
        </td>
    </tr>
</table>
<table style="width: 700px;">
    <colgroup>
        <col width="150px" />
        <col />
    </colgroup>
    <tr>
        <td>
            Номер документа
        </td>
        <td>
            <eq:eqTextBox ID="NUM" runat="server" Height="20px" Width="50px" Enable="False"></eq:eqTextBox>
        </td>
        <td>
            Дата включения документа в Государственный реестр
        </td>
        <td>
            <eq:eqTextBox ID="DOC_DATE" runat="server" Height="20px" Width="70px" Enable="False"></eq:eqTextBox>
            <aj:MaskedEditExtender ID="DOC_DATE_MASK" TargetControlID="DOC_DATE" runat="server"
                MaskType="Date" Mask="99/99/9999" />
            <%--<eq:eqTextBox ID="DOC_DATE" runat="server" Width="100px Enabled="false"" />--%>
            <%--<aj:CalendarExtender runat="server" TargetControlID="DOC_DATE" />--%>
        </td>
        <td>
            Доступен для интернет-поиска
        </td>
        <td>
            <asp:CheckBox ID="PUBLIC" runat="server" Checked="true" />
        </td>
    </tr>
    <tr>
        <td>
            Название документа*
        </td>
        <td>
            <eq:eqTextBox ID="NAME" runat="server" Height="60px" Width="500px" TextMode="MultiLine"></eq:eqTextBox>
        </td>
        <td>
            Собственность*
        </td>
        <td>
            <asp:DropDownList ID="ID_PROPERTY" runat="server">
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td>
            Самоназвание документа
        </td>
        <td>
            <eq:eqTextBox ID="SELF_NAME" runat="server" Height="60px" Width="500px" TextMode="MultiLine"></eq:eqTextBox>
        </td>
        <td>
            Тип владелеца*
        </td>
        <td>
            <eq3:eqDocument ID="ID_OWNER" runat="server" DocTypeURL="GR_OWNER.ascx" ListDocMode="NewWindowAndSelect"
                LoadDocMode="ListDocAndSelect" ShowingFields=" NAME, ABBREVIATION" ShowingFieldsFormat="{0}  {1} "
                KeyName="ID" />
        </td>
    </tr>
    <tr>
        <td>
            Вид документа*
        </td>
        <td>
            <asp:DropDownList ID="ID_DOC_KIND" runat="server">
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td>
            <eq:eqTextBox ID="ID_DOC_DESCRIPT_CURRENT" runat="server" Visible="false"></eq:eqTextBox>
        </td>
        <%--<td>
            <eq:eqTextBox ID="ISN_DOC_DESCRIPT_MAX" runat="server" Visible="false"></eq:eqTextBox>
        </td>--%>
        <td>
            <eq:eqTextBox ID="SYS_STATUS" runat="server" Visible="false" Enabled="false"></eq:eqTextBox>
        </td>
    </tr>
</table>
<eq:eqMenu ID="MenuTabs" runat="server" Orientation="Horizontal" StaticEnableDefaultPopOutImage="False"
    ItemWrap="false" HorizontalMaxCount="5">
    <Items>
        <asp:MenuItem Text="Рег.номер. Аннотация. Художественное оформление. Том" Value="0"
            Selected="true" />
        <asp:MenuItem Text="Язык. Автор" Value="1" />
        <asp:MenuItem Text="Датировка" Value="2" />
        <asp:MenuItem Text="Историческая справка" Value="3" />
        <asp:MenuItem Text="Материальный носитель. Драг.металлы и камни" Value="4" />
        <asp:MenuItem Text="Палеограф. Печать. Автограф" Value="5" />
        <asp:MenuItem Text="Публикации" Value="6" />
        <asp:MenuItem Text="Экспонирование. Страховая оценка" Value="7" />
        <asp:MenuItem Text="Место хранения, служебная информация" Value="8" />
        <asp:MenuItem Text="Размеры" Value="9" />
        <asp:MenuItem Text="Физ.состояние. Реставрация" Value="10" />
        <asp:MenuItem Text="Экспертиза" Value="11" />
        <asp:MenuItem Text="Изображения" Value="12" />
    </Items>
    <StaticSelectedStyle CssClass="MenuItemStyleSelected" />
</eq:eqMenu>
<div style="border: solid 1px black; padding: 5px;">
    <asp:MultiView ID="MultiViewTabs" runat="server" ActiveViewIndex="0">
        <%--Данные из таблицы doc_description--%>
        <asp:View runat="server">
            <table>
                <tr>
                    <td align="left">
                        <%--СПЕЦИФИКАЦИЯ dbo.GR_vDOC_DESCRIPT--%>
                        <eq:eqSpecification ID="GR_vDOC_DESCRIPT" runat="server">
                            <eq:eqSpecificationColumn>
                                <ItemTemplate>
                                    <table style="width: 400px;">
                                        <%--<colgroup>
                                            <col align="right" style="width: 30%;" />
                                            <col align="left" style="width: 20%;" />
                                            <col align="right" style="width: 30%;" />
                                            <col align="left" />
                                        </colgroup>--%>
                                        <tr>
                                            <td>
                                                Регистрационный номер*
                                            </td>
                                            <td align="left">
                                                <eq:eqTextBox ID="NUM" runat="server" Width="50px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Дело, том*
                                            </td>
                                            <td align="left">
                                                <eq3:eqDocument ID="ID_VOLUME" runat="server" DocTypeURL="GR_VOLUME_LIST.ascx" ListDocMode="NewWindowAndSelect"
                                                    LoadDocMode="ListDocAndSelect" ShowingFields="FILE_NUMBER,NAME,NUMBER" ShowingFieldsFormat="Дело № {0},Наименование дела '{1}', Том № {2}"
                                                    KeyName="ID" OnSelectDoc="ISN_VOLUME_SelectDoc" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Дата регистрации*
                                            </td>
                                            <td align="left">
                                                <eq:eqTextBox ID="DESCRIPT_DATE" runat="server" Width="70px" />
                                                <aj:MaskedEditExtender ID="DESCRIPT_DATE_MASK" TargetControlID="DESCRIPT_DATE" runat="server"
                                                    MaskType="Date" Mask="99/99/9999" />
                                                <aj:CalendarExtender runat="server" TargetControlID="DESCRIPT_DATE" />
                                            </td>
                                        </tr>
                                    </table>
                                    <table>
                                        <tr>
                                            <td>
                                                Аннотация
                                            </td>
                                            <td>
                                                Художественное оформление
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <eq:eqTextBox ID="ANNOTATION" runat="server" Width="500px" Height="500px" TextMode="MultiLine" />
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="ART_DESIGN" runat="server" Width="500px" Height="500px" TextMode="MultiLine" />
                                            </td>
                                        </tr>
                                    </table>
                                    <table>
                                        <tr>
                                            <td>
                                                <eq:eqTextBox ID="DocID" runat="server" Visible="false" />
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="RowID" runat="server" Visible="false" />
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                        </eq:eqSpecification>
                    </td>
                </tr>
            </table>
        </asp:View>
        <asp:View runat="server">
            <table style="width: 700px;">
                <col style="width: 200px;" />
                <col />
                <tr>
                    <td>
                        <fieldset>
                            <legend>Язык документа</legend>
                            <%--СПЕЦИФИКАЦИЯ dbo.GR_tblDOC_LANGUAGE--%>
                            <eq:eqSpecification ID="GR_tblDOC_LANGUAGE" runat="server" ShowNumering="false">
                                <eq:eqSpecificationColumn HeaderText="Название" Width="1000px">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ID_LANGUAGE" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Страницы" Width="50px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="PAGES" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                        <fieldset>
                            <legend>Автор документа</legend>
                            <%--СПЕЦИФИКАЦИЯ dbo.GR_tblDOC_AUTHOR--%>
                            <eq:eqSpecification ID="GR_tblDOC_AUTHOR" runat="server" ShowNumering="false">
                                <eq:eqSpecificationColumn HeaderText="Автор документа" Width="100px">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ID_AUTHOR" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
            </table>
        </asp:View>
        <asp:View runat="server">
            <table>
                <%--Это должна быть вкладка Датировка--%>
                <tr>
                    <td>
                        Дата (время создания) документа
                    </td>
                    <td>
                        <eq:eqTextBox ID="CREATION_DATE" runat="server" Width="550px" TextMode="MultiLine" />
                    </td>
                </tr>
            </table>
            <table style="width: 550px;">
                <tr>
                    <td>
                        <fieldset>
                            <legend>Даты в интервале</legend>
                            <table>
                                <tr>
                                    <td>
                                        Точная дата (дд.мм.гггг): от
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="EXACT_DATE" runat="server" Width="70px" />
                                        <aj:MaskedEditExtender ID="EXACT_DATE_MASK" TargetControlID="EXACT_DATE" runat="server"
                                            MaskType="Date" Mask="99/99/9999" ClearTextOnInvalid="true" 
                                            InputDirection="RightToLeft" /> 
                                        <aj:MaskedEditValidator ID="EXACT_DATE_VALIDATOR" runat="server"
                                                 ControlExtender="EXACT_DATE_MASK"
                                                 ControlToValidate="EXACT_DATE" 
                                                 IsValidEmpty="true"
                                                 InvalidValueMessage="Неверный формат"  
                                                 Display="Dynamic"
                                                 /> 
                                            
 <%-- <aj:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="EXACT_DATE" />--%>
                                    </td>
                                    <td>
                                        до
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="EXACT_DATE1" runat="server" Width="70px" />
                                        <aj:MaskedEditExtender ID="EXACT_DATE1_MASK" TargetControlID="EXACT_DATE1" runat="server"
                                            MaskType="Date" Mask="99/99/9999" ClearTextOnInvalid="true" />
                                        
                                        <aj:MaskedEditValidator ID="EXACT_DATE1_VALIDATOR" runat="server"
                                                 ControlExtender="EXACT_DATE1_MASK"
                                                 ControlToValidate="EXACT_DATE1" 
                                                 IsValidEmpty="true"
                                                 InvalidValueMessage="Неверный формат"  
                                                 Display="Dynamic"
                                                 /> 
                                        <%--<aj:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="EXACT_DATE1" />--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Год: от
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="YEAR" runat="server" Width="40px" />
                                    </td>
                                    <td>
                                        до
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="YEAR1" runat="server" Width="40px" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Десятилетие: от
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="DECADE" runat="server" Width="20px" />
                                    </td>
                                    <td>
                                        до
                                    </td>
                                    <td>
                                        <eq:eqTextBox ID="DECADE1" runat="server" Width="20px" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Часть века: от
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ID_CENTURY_PART" runat="server" Width="150px">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        до
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ID_CENTURY_PART1" runat="server" Width="150px">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Век: от
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ID_CENTURY" runat="server" Width="150px">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        до
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ID_CENTURY1" runat="server" Width="150px">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td>
                        Ориентировочная дата
                    </td>
                    <td>
                        <eq:eqTextBox ID="APPROX_DATE" runat="server" Width="100px" />
                    </td>
                    <td>
                        Дата от сотворения мира
                    </td>
                    <td>
                        <eq:eqTextBox ID="DATE_FROM_WC" runat="server" Width="100px" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Стиль
                    </td>
                    <td>
                        <asp:DropDownList ID="ID_STYLE" runat="server" Width="100px">
                        </asp:DropDownList>
                    </td>
                    <td>
                        Датировка копии точная дата
                    </td>
                    <td>
                        <eq:eqTextBox ID="COPY_EXACT_DATE" runat="server" Width="100px" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Датировка копии точная дата - часть века
                    </td>
                    <td>
                        <asp:DropDownList ID="ID_COPY_EXACT_CENT_PART" runat="server" Width="150px">
                        </asp:DropDownList>
                    </td>
                    <td>
                        Датировка копии точная дата - век
                    </td>
                    <td>
                        <asp:DropDownList ID="ID_COPY_CENTURY" runat="server" Width="150px">
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
        </asp:View>
        <asp:View runat="server">
            <table>
                <tr>
                    <td>
                        Историческая справка
                    </td>
                    <td>
                        <eq:eqTextBox ID="HISTORY" runat="server" Width="500px" Height="500px" TextMode="MultiLine" />
                    </td>
                </tr>
            </table>
        </asp:View>
        <%--Материальный носитель. Драг.металлы и камни--%>
        <asp:View runat="server">
            <table style="width: 700px;">
                <col style="width: 200px;" />
                <col />
                <tr>
                    <td>
                        <fieldset>
                            <legend>Материальный носитель</legend>
                            <%--СПЕЦИФИКАЦИЯ dbo.GR_tblDOC_CARRIER--%>
                            <eq:eqSpecification ID="GR_tblDOC_CARRIER" runat="server" ShowNumering="false">
                                <eq:eqSpecificationColumn HeaderText="Материальный носитель" Width="300px">
                                    <ItemTemplate>
                                        <%-- <asp:DropDownList ID="ID_CARRIER" runat="server">
                                        </asp:DropDownList>--%>
                                        <%--<eq3:eqDocument ID="ID_HIGH_CARRIER" KeyName="ID_CARRIER" runat="server" DocTypeURL="GR_CARRIER_CL.ascx"
                                            ListDocMode="NewWindowAndSelect" LoadDocMode="NewWindowAndSelect" ShowingFields="NAME"
                                            ShowingFieldsFormat="{0}" ValidateHierarchy="true" />--%>
                                        <eq3:eqDocument ID="ID_CARRIER" runat="server" DocTypeURL="GR_CARRIER_CL.ascx" ListDocMode="NewWindowAndSelect"
                                            LoadDocMode="NewWindowAndSelect" ShowingFields="NAME" ShowingFieldsFormat="{0}"
                                            KeyName="ID" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Век" Width="30px">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ID_CENTURY" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Оригинальная запись" Width="300px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="ORIGINAL_RECORD" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td>
                        <fieldset>
                            <legend>Драг.металлы</legend>
                            <%--СПЕЦИФИКАЦИЯ dbo.GR_tblPRECIOUS_METAL--%>
                            <eq:eqSpecification ID="GR_tblPRECIOUS_METAL" runat="server" ShowNumering="false">
                                <eq:eqSpecificationColumn HeaderText="Оригинальная запись" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="ORIGINAL_RECORD" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Драг.металл" Width="100px">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ID_METAL" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Проба" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="PROBE" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="В лигатуре" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="MASS_ALLOY" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="В чистоте" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="MASS_PURE" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Масса" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="MASS" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="DocID" Width="100px" Visible="false">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="DocID" runat="server" Visible="false" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="RowID" Width="100px" Visible="false">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="RowID" runat="server" Visible="false" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="ID_DOC_DESCRIPT" Width="100px" Visible="false">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="ID_DOC_DESCRIPT" runat="server" Visible="false" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td>
                        <fieldset>
                            <legend>Драг.камни</legend>
                            <%--СПЕЦИФИКАЦИЯ dbo.GR_tblPRECIOUS_STONES--%>
                            <eq:eqSpecification ID="GR_tblPRECIOUS_STONES" runat="server" ShowNumering="false">
                                <eq:eqSpecificationColumn HeaderText="Оригинальная запись" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="ORIGINAL_RECORD" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Драг.камень" Width="100px">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ID_STONE" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Количество" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="QUANTITY" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Вес в каратах" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="WHEIGHT" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="DocID" Width="100px" Visible="false">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="DocID" runat="server" Visible="false" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="RowID" Width="100px" Visible="false">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="RowID" runat="server" Visible="false" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="ID_DOC_DESCRIPT" Width="100px" Visible="false">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="ID_DOC_DESCRIPT" runat="server" Visible="false" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
            </table>
        </asp:View>
        <asp:View runat="server">
            <table style="width: 700px;">
                <col style="width: 200px;" />
                <col />
                <tr>
                    <td>
                        <fieldset>
                            <legend>Палеограф. особенности</legend>
                            <%--СПЕЦИФИКАЦИЯ dbo.GR_tblPALEO_FEATURES--%>
                            <eq:eqSpecification ID="GR_tblPALEO_FEATURES" runat="server" ShowNumering="false">
                                <eq:eqSpecificationColumn HeaderText="Палеограф.особенности" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="NAME" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Расположение" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="LOCATION" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                        <fieldset>
                            <legend>Автограф</legend>
                            <%--СПЕЦИФИКАЦИЯ dbo.GR_tblAUTOGRAPH--%>
                            <eq:eqSpecification ID="GR_tblAUTOGRAPH" runat="server" ShowNumering="false">
                                <eq:eqSpecificationColumn HeaderText="Автограф" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="NAME" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Расположение" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="LOCATION" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Содержание" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="CONTENTS" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                        <fieldset>
                            <legend>Печать</legend>
                            <%--СПЕЦИФИКАЦИЯ dbo.GR_tblSEAL--%>
                            <eq:eqSpecification ID="GR_tblSEAL" runat="server" ShowNumering="false">
                                <eq:eqSpecificationColumn HeaderText="Печать" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="NAME" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Описание" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="DESCRIPTION" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Расположение" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="LOCATION" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Наличие кустодии" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="HAS_KUSTODY" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Физ.состояние" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="PHYSICAL_STATE" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
            </table>
        </asp:View>
        <asp:View runat="server">
            <table>
                <tr>
                    <td>
                        <fieldset>
                            <legend>Публикации</legend>
                            <%--СПЕЦИФИКАЦИЯ dbo.GR_tblPUBLICATION--%>
                            <eq:eqSpecification ID="GR_tblPUBLICATION" runat="server" ShowNumering="false">
                                <eq:eqSpecificationColumn HeaderText="Автор" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="AUTHOR" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Публикатор" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="PUBLISHER" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Редактор" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="EDITOR" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Название публикации" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="NAME" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Сборник" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="COLLECTION" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Место издания" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="PUBLISH_PLACE" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Год издания" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="PUBLISH_YEAR" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Месяц издания" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="PUBLISH_MONTH" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Издательство" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="PUBLISH_HOUSE" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Часть" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="PART" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Том" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="VOLUME" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Выпуск" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="RELESE" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Страницы" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="PAGES" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Примечание" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="NOTE" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Документ, разрешающий публикации" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="PERMISSION_DOC_NAME" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="№ документа" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="PERMISSION_DOC_NUMBER" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Дата документа" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="PERMISSION_DOC_DATE" runat="server" Width="100px" />
                                        <aj:CalendarExtender runat="server" TargetControlID="PERMISSION_DOC_DATE" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
            </table>
        </asp:View>
        <asp:View runat="server">
            <table>
                <tr>
                    <td>
                        <fieldset>
                            <legend>Экспонирование</legend>
                            <%--СПЕЦИФИКАЦИЯ dbo.GR_tblEXPOSITION--%>
                            <eq:eqSpecification ID="GR_tblEXPOSITION" runat="server" ShowNumering="false">
                                <eq:eqSpecificationColumn HeaderText="Название выставки" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="NAME" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Страна проведения" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="COUNTRY" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Город проведения" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="CITY" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Место проведения" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="PLACE" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Время проведения" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="TIME" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td>
                        <fieldset>
                            <legend>Страховая оценка</legend>
                            <%--СПЕЦИФИКАЦИЯ dbo.GR_tblINSURANCE_VAL--%>
                            <eq:eqSpecification ID="GR_tblINSURANCE_VAL" runat="server" ShowNumering="false">
                                <eq:eqSpecificationColumn HeaderText="Год" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="YEAR" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Месяц" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="MONTH" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Число" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="DAY" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Название страховой компании" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="INSURANCE_COMPANY_NAME" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Сумма" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="SUM" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Валюта" Width="100px">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ID_CURRENCY" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Название фрагмента документа" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="DOC_FRAGMENT_NAME" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Объем" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="FRAGMENT_SIZE" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Методика оценки" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="ESTIMATION_PROC" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
            </table>
        </asp:View>
        <%--Место хранения. Служебная информация--%>
        <asp:View runat="server">
            <table>
                <tr>
                    <td>
                        Место хранения
                    </td>
                    <td align="center">
                        <%--СПЕЦИФИКАЦИЯ dbo.GR_tblSTORE_PLACE--%>
                        <eq:eqSpecification ID="GR_tblSTORE_PLACE" runat="server">
                            <eq:eqSpecificationColumn>
                                <ItemTemplate>
                                    <table style="width: 600px;">
                                        <colgroup>
                                            <col align="right" style="width: 30%;" />
                                            <col align="left" style="width: 20%;" />
                                            <col align="right" style="width: 30%;" />
                                            <col align="left" />
                                        </colgroup>
                                        <tr>
                                            <td>
                                                <fieldset>
                                                    <legend>Уровень архива</legend>
                                                    <asp:RadioButtonList ID="ARCHIVE_LEVEL" runat="server" RepeatDirection="vertical"
                                                        RepeatLayout="Flow" Width="500px">
                                                        <asp:ListItem Text="федеральный" Value="a"></asp:ListItem>
                                                        <asp:ListItem Text="региональный" Value="b"></asp:ListItem>
                                                        <asp:ListItem Text="муниципальный" Value="с"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </fieldset>
                                            </td>
                                        </tr>
                                    </table>
                                    <table>
                                        <tr>
                                            <td>
                                                Код ОКПО архива
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="OKPO" runat="server" Width="100px" Enabled="False" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Наименование архива
                                            </td>
                                            <td>
                                                <eq3:eqDocument ID="ID_ARCHIVE" runat="server" DocTypeURL="GR_ARCHIVE.ascx" ListDocMode="NewWindowAndSelect"
                                                    LoadDocMode="ListDocAndSelect" ShowingFields="NAME" ShowingFieldsFormat="{0}"
                                                    KeyName="ID" Width="500px" OnRequesting="Archive_Requesting" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Федеральный округ / Субъект
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="SUBJECT" runat="server" Width="500px" Enabled="False" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Муниципальное образование
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="MUNICIPALITY" runat="server" Width="500px" />
                                            </td>
                                        </tr>
                                    </table>
                                    <table style="width: 600px;">
                                        <colgroup>
                                            <col align="right" style="width: 30%;" />
                                            <col align="left" style="width: 20%;" />
                                            <col align="right" style="width: 30%;" />
                                            <col align="left" />
                                        </colgroup>
                                        <tr>
                                            <td>
                                                Фонд Название
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="FUND_NAME" runat="server" Width="70px" />
                                            </td>
                                            <td>
                                                №
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="FUND_NUM_1" runat="server" Width="70px" />
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="FUND_NUM_2" runat="server" Width="70px" />
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="FUND_NUM_3" runat="server" Width="70px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Опись
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="INVENTORY_NUM_1" runat="server" Width="70px" />
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="INVENTORY_NUM_2" runat="server" Width="70px" />
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="INVENTORY_NUM_3" runat="server" Width="70px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Том
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="INVENTORY_VOLUME" runat="server" Width="70px" />
                                            </td>
                                            <td>
                                                Отделение
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="DEPARTMENT" runat="server" Width="70px" />
                                            </td>
                                            <td>
                                                Рубрика
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="RUBRIC" runat="server" Width="70px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Часть
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="FUND_OLD_PART" runat="server" Width="70px" />
                                            </td>
                                            <td>
                                                Номер
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="FUND_OLD_NUMBER" runat="server" Width="70px" />
                                            </td>
                                            <td>
                                                Дело
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="FUND_OLD_FILE" runat="server" Width="70px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Ед.хр.
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="UNIT_NUM_1" runat="server" Width="70px" />
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="UNIT_NUM_2" runat="server" Width="70px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Ед.уч.
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="UNIT_REG_NUM_1" runat="server" Width="70px" />
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="UNIT_REG_NUM_2" runat="server" Width="70px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Листы
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="SHEETS" runat="server" Width="70px" />
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                        </eq:eqSpecification>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td>
                        Официальная информация
                    </td>
                    <td align="center">
                        <%--СПЕЦИФИКАЦИЯ dbo.GR_tblOFFICIAL_INFO--%>
                        <eq:eqSpecification ID="GR_tblOFFICIAL_INFO" runat="server">
                            <eq:eqSpecificationColumn>
                                <ItemTemplate>
                                    <table style="width: 600px;">
                                        <colgroup>
                                            <col align="right" style="width: 30%;" />
                                            <col align="left" style="width: 20%;" />
                                            <col align="right" style="width: 30%;" />
                                            <col align="left" />
                                        </colgroup>
                                        <tr>
                                            <td>
                                                Кем представлен документ
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="ORGANIZATION" runat="server" Width="500px" />
                                            </td>
                                        </tr>
                                    </table>
                                    <table>
                                        <tr>
                                            <td>
                                                Протокол ЭПК №
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="PROTOCOL_NUM" runat="server" Width="70px" />
                                            </td>
                                            <td>
                                                Протокол ЦЭПК Росархива №
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="PROTOCOL_ROSARCH_NUM" runat="server" Width="70px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Протокол ЭПК дата
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="PROTOCOL_DATE" runat="server" Width="70px" />
                                                <aj:CalendarExtender runat="server" TargetControlID="PROTOCOL_DATE" />
                                            </td>
                                            <td>
                                                Протокол ЦЭПК Росархива дата
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="PROTOCOL_ROSARCH_DATE" runat="server" Width="70px" />
                                                <aj:CalendarExtender runat="server" TargetControlID="PROTOCOL_ROSARCH_DATE" />
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                        </eq:eqSpecification>
                    </td>
                </tr>
            </table>
        </asp:View>
        <%--СПЕЦИФИКАЦИЯ Размеры dbo.GR_tblDOC_SIZE--%>
        <asp:View runat="server">
            <table>
                <tr>
                    <td>
                        Объем, листов
                    </td>
                    <td>
                        <eq:eqTextBox ID="SHEET_COUNT" runat="server" Width="70px" />
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td align="center">
                        <%--СПЕЦИФИКАЦИЯ dbo.GR_tblDOC_SIZE--%>
                        <eq:eqSpecification ID="GR_tblDOC_SIZE" runat="server">
                            <eq:eqSpecificationColumn>
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                Для листов
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="SHEET_NUMBERS" runat="server" Width="70px" />
                                            </td>
                                            <td>
                                                min/max/midi
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="MIN_MAX_MIDI" runat="server" Width="70px">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                    <table>
                                        <tr>
                                            <td>
                                                Латинское обозначение формата
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="LAT_NOTION1" runat="server" Width="70px" />
                                            </td>
                                            <td>
                                                .
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="LAT_NOTION2" runat="server" Width="70px" />
                                            </td>
                                            <td>
                                                /
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="LAT_NOTION3" runat="server" Width="70px" />
                                            </td>
                                        </tr>
                                    </table>
                                    <table>
                                        <tr>
                                            <td>
                                                Размер 1
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="DIM1" runat="server" Width="70px" />
                                            </td>
                                            <td>
                                                Диаметр
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="DIAMETER" runat="server" Width="70px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Размер 2
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="DIM2" runat="server" Width="70px" />
                                            </td>
                                            <td>
                                                Высота свитка
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="HEIGHT" runat="server" Width="70px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Размер 3
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="DIM3" runat="server" Width="70px" />
                                            </td>
                                            <td>
                                                Объем, листов
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="SHEET_VOL" runat="server" Width="70px" />
                                            </td>
                                        </tr>
                                    </table>
                                    <table>
                                        <td>
                                            Примечание
                                        </td>
                                        <td>
                                            <eq:eqTextBox ID="NOTE" runat="server" Width="300px" Height="100px" TextMode="MultiLine" />
                                        </td>
                                    </table>
                                    <table>
                                        <tr>
                                            <td>
                                                <eq:eqTextBox ID="DocID" runat="server" Visible="false" />
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="RowID" runat="server" Visible="false" />
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="ID_DOC_DESCRIPT" runat="server" Visible="false" />
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                        </eq:eqSpecification>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td>
                        Для кино/фоно документов
                    </td>
                </tr>
                <tr>
                    <td>
                        Ширина пленки
                    </td>
                    <td>
                        <eq:eqTextBox ID="FILM_WIDTH" runat="server" Width="70px" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Объем документа
                    </td>
                    <td>
                        <eq:eqTextBox ID="DOC_VOL" runat="server" Width="70px" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Время звучания
                    </td>
                    <td>
                        <eq:eqTextBox ID="SOUND_TIME" runat="server" Width="70px" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Время представления
                    </td>
                    <td>
                        <eq:eqTextBox ID="PRESENTATION_TIME" runat="server" Width="70px" />
                    </td>
                </tr>
                <tr>
            </table>
        </asp:View>
        <%--Физическое состояние. Реставрация--%>
        <asp:View runat="server">
            <table>
                <tr>
                    <td>
                        <fieldset>
                            <legend>Физическое состояние</legend>
                            <eq:eqSpecification ID="GR_tblPHYSICAL_STATE" runat="server" ShowNumering="false">
                                <eq:eqSpecificationColumn HeaderText="Физическое состояние">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="NAME" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Потребность в реставрации">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="RESTORATION_NEEDED" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Дата документа">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="DATE" runat="server" Width="100px" TextMode="MultiLine" />
                                        <aj:CalendarExtender runat="server" TargetControlID="DATE" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td>
                        <fieldset>
                            <legend>Реставрация</legend>
                            <eq:eqSpecification ID="GR_tblRESTORATION" runat="server" ShowNumering="false">
                                <eq:eqSpecificationColumn HeaderText="Сведения о реставрации" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="INFO" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Дата" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="DATE" runat="server" Width="100px" />
                                        <aj:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="DATE" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Реставраторы" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="RESTORATORS" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Реставрационный центр" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="RESTORE_CENTER" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Страницы?" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="PAGES" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="DocID" Width="100px" Visible="false">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="DocID" runat="server" Visible="false" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="RowID" Width="100px" Visible="false">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="RowID" runat="server" Visible="false" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="ID_DOC_DESCRIPT" Width="100px" Visible="false">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="ID_DOC_DESCRIPT" runat="server" Visible="false" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
            </table>
        </asp:View>
        <%--экспертиза--%>
        <asp:View runat="server">
            <table>
                <tr>
                    <td>
                        Экспертиза
                    </td>
                    <td align="center">
                        <eq:eqSpecification ID="GR_tblEXPERT_REPORT" runat="server">
                            <eq:eqSpecificationColumn>
                                <ItemTemplate>
                                    <table style="width: 600px;">
                                        <colgroup>
                                            <col align="right" style="width: 30%;" />
                                            <col align="left" style="width: 20%;" />
                                            <col align="right" style="width: 30%;" />
                                            <col align="left" />
                                        </colgroup>
                                        <tr>
                                            <td>
                                                Оригинальная запись
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="ORIGINAL_RECORD" runat="server" Width="500px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Автор
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="AUTHOR" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Должность, звание
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="POST_TITLE" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Экспертное заключение
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="REPORT" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                        </eq:eqSpecification>
                    </td>
                </tr>
            </table>
            <table style="width: 700px;">
                <tr>
                    <td>
                        <fieldset>
                            <legend>Файлы электронного образа</legend>
                            <eq:eqSpecification ID="GR_tblEXPERT_REPORT_IMAGE" runat="server" Width="100%" AllowInsert="false">
                                <eq:eqSpecificationColumn HeaderText="Описание">
                                    <ItemTemplate>
                                        <asp:TextBox ID="NAME" runat="server" Width="99%" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Файл">
                                    <ItemTemplate>
                                        <eq2:RefFile ID="ID" runat="server" Mode="FileSystem" OnRequesting="RefFileE_Requesting" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                            <table>
                                <tr>
                                    <td>
                                        Добавить файл:
                                    </td>
                                    <td>
                                        <eq2:RefFileUpload ID="RefFileUploadFileE" runat="server" Mode="FileSystem" />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
            </table>
        </asp:View>
        <%--Изображения--%>
        <asp:View runat="server">
         <table style="width: 97%; margin-bottom: 4px;">
    <tr>
        
        <td style="text-align: left;">
           
            <asp:HyperLink ID="HyperLink_DOC_IMAGE" runat="server" Text="Ссылки на файлы электронного образа" Target="_blank"
                NavigateUrl="~/Default.aspx?DocTypeURL=GR_DOC_IMAGE.ascx" Style="font-weight: bold;
                padding: 2px;" />
        </td>
    </tr>
 <%--</table>
            <table style="width: 700px;">
                <tr>
                    <td>
                        <fieldset>
                            <legend>Файлы электронного образа</legend>
                            <eq:eqSpecification ID="GR_tblDOC_IMAGE" runat="server" Width="100%" AllowInsert="false">
                                <eq:eqSpecificationColumn HeaderText="Описание">
                                    <ItemTemplate>
                                        <asp:TextBox ID="NAME" runat="server" Width="99%" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Файл">
                                    <ItemTemplate>
                                        <eq2:RefFile ID="ID" runat="server" Mode="FileSystem" OnRequesting="RefFileS_Requesting" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Доступен для интернет-поиска">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="PUBLIC" runat="server" Checked="true" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                            <table>
                                <tr>
                                    <td>
                                        Добавить файл:
                                    </td>
                                    <td>
                                        <eq2:RefFileUpload ID="RefFileUploadFileS" runat="server" Mode="FileSystem" />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
            </table> --%>
            
        </asp:View>
    </asp:MultiView>
</div>
