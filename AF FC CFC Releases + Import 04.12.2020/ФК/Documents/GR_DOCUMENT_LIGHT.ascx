﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="GR_DOCUMENT_LIGHT.ascx.vb"
    Inherits="WebApplication.GR_DOCUMENT_LIGHT" %>
<table style="width: 97%; margin-bottom: 4px;">
</table>
<table style="width: 700px;">
    <colgroup>
        <col width="150px" />
        <col />
    </colgroup>
    <tr>
        <td>
            ЛИСТ УЧЕТА И ОПИСАНИЯ УНИКАЛЬНОГО ДОКУМЕНТА
        </td>
    </tr>
</table>
<table style="width: 1000px;">
    <colgroup>
        <col width="150px" />
        <col />
    </colgroup>
    <tr>
        <td>
            Регистрационный номер
        </td>
        <td>
            <eq:eqTextBox ID="DESCRIPT_NUM_Fict" runat="server" Height="20px" Width="50px"></eq:eqTextBox>
        </td>
        <%-- <td>
            <asp:TextBox ID="TEST" runat="server" Height="20px" Width="50px"></asp:TextBox>
        </td>--%>
    </tr>
</table>
<table>
    <tr>
        <td>
            Дата включения документа в Государственный реестр
        </td>
        <td>
            <eq:eqTextBox ID="DESCRIPT_DATE_Fict" runat="server" Height="20px" Width="70px"></eq:eqTextBox>
            <aj:MaskedEditExtender ID="DOC_DATE_MASK" TargetControlID="DESCRIPT_DATE_Fict" runat="server"
                MaskType="Date" Mask="99/99/9999" />
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            1. Описание документа
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            Название (заголовок) документа*
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            <eq:eqTextBox ID="NAME" runat="server" Height="60px" Width="1000px" TextMode="MultiLine"></eq:eqTextBox>
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            Самоназвание документа
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            <eq:eqTextBox ID="SELF_NAME" runat="server" Height="60px" Width="1000px" TextMode="MultiLine"></eq:eqTextBox>
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            Вид документа*
        </td>
        <td>
            <asp:DropDownList ID="ID_DOC_KIND" runat="server">
            </asp:DropDownList>
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            Автор документа
        </td>
    </tr>
</table>
<table style="width: 1000px;">
    <tr>
        <td>
            <fieldset>
                <eq:eqSpecification ID="GR_tblDOC_AUTHOR" runat="server" ShowNumering="false">
                    <eq:eqSpecificationColumn HeaderText="" Width="800px">
                        <ItemTemplate>
                            <asp:DropDownList ID="ID_AUTHOR" runat="server" Width="800px">
                            </asp:DropDownList>
                        </ItemTemplate>
                    </eq:eqSpecificationColumn>
                </eq:eqSpecification>
            </fieldset>
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            Дата (время создания) документа
        </td>
        <td>
            <eq:eqTextBox ID="CREATION_DATE" runat="server" Width="550px" TextMode="MultiLine" />
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            Век*
        </td>
        <td>
            <asp:DropDownList ID="ID_CENTURY_FROM" runat="server" Width="150px">
            </asp:DropDownList>
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            Ориентировочная дата
        </td>
        <td>
            <eq:eqTextBox ID="APPROX_DATE" runat="server" Width="550px" />
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            Язык документа*
        </td>
    </tr>
</table>
<table style="width: 200px;">
    <tr>
        <td>
            <fieldset>
                <eq:eqSpecification ID="GR_tblDOC_LANGUAGE" runat="server" Width="200px" ShowNumering="false">
                    <eq:eqSpecificationColumn HeaderText="Название" Width="200px">
                        <ItemTemplate>
                            <asp:DropDownList ID="ID_LANGUAGE" runat="server" Width="200px">
                            </asp:DropDownList>
                        </ItemTemplate>
                    </eq:eqSpecificationColumn>
                </eq:eqSpecification>
            </fieldset>
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            Аннотация*
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            <eq:eqTextBox ID="ANNOTATION_Fict" runat="server" Width="1000px" Height="200px" TextMode="MultiLine" />
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            Историческая справка*
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            <eq:eqTextBox ID="HISTORY" runat="server" Width="1000px" Height="200px" TextMode="MultiLine" />
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            Наличие драгоценных металлов и камней
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            <eq:eqTextBox ID="PRECIOUS_Fict" runat="server" Width="1000px" Height="50px" TextMode="MultiLine" />
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            Палеографические особенности
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            <eq:eqTextBox ID="PALEO_FEATURES" runat="server" Width="1000px" Height="200px" TextMode="MultiLine" />
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            Печати
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            <eq:eqTextBox ID="SEALS" runat="server" Width="1000px" Height="200px" TextMode="MultiLine" />
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            Художественные особенности оформления
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            <eq:eqTextBox ID="ART_DESIGN_Fict" runat="server" Width="1000px" Height="200px" TextMode="MultiLine" />
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            Собственность*
        </td>
        <td>
            <asp:DropDownList ID="ID_PROPERTY" runat="server">
            </asp:DropDownList>
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            2. Физическое состояние документа
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            Материальный носитель*
        </td>
    </tr>
</table>
<table style="width: 1000px;">
    <tr>
        <td>
            <fieldset>
                <eq:eqSpecification ID="GR_tblDOC_CARRIER" runat="server" ShowNumering="false">
                    <eq:eqSpecificationColumn HeaderText="Наименование" Width="800px">
                        <ItemTemplate>
                            <eq3:eqDocument ID="ID_CARRIER" runat="server" DocTypeURL="GR_CARRIER_CL.ascx" ListDocMode="NewWindowAndSelect"
                                LoadDocMode="NewWindowAndSelect" ShowingFields="NAME" ShowingFieldsFormat="{0}"
                                KeyName="ID" />
                        </ItemTemplate>
                    </eq:eqSpecificationColumn>
                </eq:eqSpecification>
            </fieldset>
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            Размеры*
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            <eq:eqTextBox ID="SIZES_Fict" runat="server" Width="1000px" Height="50px" TextMode="MultiLine" />
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            Объем*
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            <eq:eqTextBox ID="VOLUME_Fict" runat="server" Width="1000px" Height="50px" TextMode="MultiLine" />
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            Физическое состояние*
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            <eq:eqTextBox ID="PHYSICAL_STATE_Fict" runat="server" Width="1000px" Height="50px"
                TextMode="MultiLine" />
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            Сведения о реставрации
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            <eq:eqTextBox ID="RESTORATION_Fict" runat="server" Width="1000px" Height="50px" TextMode="MultiLine" />
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            3. Место хранения документа
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            Место хранения*
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            <eq3:eqDocument ID="ID_ARCHIVE" runat="server" DocTypeURL="GR_ARCHIVE.ascx" ListDocMode="NewWindowAndSelect"
                LoadDocMode="ListDocAndSelect" ShowingFields="NAME" ShowingFieldsFormat="{0}"
                KeyName="ID" Width="500px" OnRequesting="Archive_Requesting" AutoPostBack="true" />
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            Адрес места хранения*
        </td>
    </tr>
</table>
<%-- <table>
        <tr>
            <td>
                <eq:eqTextBox ID="ADDRESS" runat="server" Width="1000px" Height="50px" TextMode="MultiLine" />
            </td>
        </tr>
    </table>--%>
<table>
    <tr>
        <td>
            <asp:DropDownList ID="ID_ARCHIVE_ADDRESS" runat="server" Width="1000px" Height="50px"
                TextMode="MultiLine">
            </asp:DropDownList>
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            Архивный шифр*: Архив:
        </td>
        <td>
            <eq:eqTextBox ID="NAME_SHORT" runat="server" Width="40px" Height="20px" Enabled="False" />
        </td>
         <td>
            , фонд №
        </td>
        <td>
            <eq:eqTextBox ID="FUND_NUM_1" runat="server" Width="8px" Height="20px" />
            <eq:eqTextBox ID="FUND_NUM_2" runat="server" Width="40px" Height="20px" />
            <eq:eqTextBox ID="FUND_NUM_3" runat="server" Width="8px" Height="20px" />
        </td>
        <td>
            , оп.№
        </td>
        <td>
            <eq:eqTextBox ID="INVENTORY_NUM_1" runat="server" Width="24px" Height="20px" />
            <eq:eqTextBox ID="INVENTORY_NUM_2" runat="server" Width="16px" Height="20px" />
        </td>
        <td>
            , оп.№ (альтернативный)
        </td>
        <td>
            <eq:eqTextBox ID="INVENTORY_NUM_ALTERNATIVE" runat="server" Width="200px" Height="20px" />
        </td>
        <td>
            том
        </td>
        <td>
            <eq:eqTextBox ID="INVENTORY_NUM_3" runat="server" Width="24px" Height="20px" />
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            ед.хр.№
        </td>
        <td>
            <eq:eqTextBox ID="UNIT_NUM_1" runat="server" Width="64px" Height="20px" />
            <eq:eqTextBox ID="UNIT_NUM_2" runat="server" Width="16px" Height="20px" />
        </td>
        <td>
            / ед.уч.№
        </td>
        <td>
            <eq:eqTextBox ID="UNIT_REG_NUM_1" runat="server" Width="64px" Height="20px" />
            <eq:eqTextBox ID="UNIT_REG_NUM_2" runat="server" Width="16px" Height="20px" />
        </td>
        <td>
            том
        </td>
        <td>
            <eq:eqTextBox ID="UNIT_VOL_NUM" runat="server" Width="40px" Height="20px" />
        </td>
        <td>
            , лл.
        </td>
        <td>
            <eq:eqTextBox ID="SHEETS" runat="server" Width="200px" Height="20px" />
        </td>
        <td>
            <eq:eqTextBox ID="QtyImage" runat="server" Width="200px" Height="20px" Visible="false" />
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            4. Служебная информация
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            Кем представлен документ*
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            <eq:eqTextBox ID="PROTOCOL_ORGANIZATION_Fict" runat="server" Width="900px" Height="50px"
                TextMode="MultiLine" />
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            Протокол ЭПК от
        </td>
        <td>
            <eq:eqTextBox ID="PROTOCOL_DATE_Fict" runat="server" Width="100px" Height="20px" />
            <aj:MaskedEditExtender ID="PROTOCOL_DATE_MASK" TargetControlID="PROTOCOL_DATE_Fict"
                runat="server" MaskType="Date" Mask="99/99/9999" />
        </td>
        <td>
            №
        </td>
        <td>
            <eq:eqTextBox ID="PROTOCOL_NUM_Fict" runat="server" Width="100px" Height="20px" />
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            Протокол ЦЭПК Росархива от
        </td>
        <td>
            <eq:eqTextBox ID="PROTOCOL_ROSARCH_DATE_Fict" runat="server" Width="100px" Height="20px" />
            <aj:MaskedEditExtender ID="PROTOCOL_ROSARCH_DATE_MASK" TargetControlID="PROTOCOL_ROSARCH_DATE_Fict"
                runat="server" MaskType="Date" Mask="99/99/9999" />
        </td>
        <td>
            №
        </td>
        <td>
            <eq:eqTextBox ID="PROTOCOL_ROSARCH_NUM_Fict" runat="server" Width="100px" Height="20px" />
        </td>
    </tr>
</table>
<table style="width: 97%; margin-bottom: 4px;">
    <tr>
        <td style="text-align: left;">
            <asp:HyperLink ID="HyperLink_DOC_IMAGE" runat="server" Text="Электронные образы уникального документа"
                Target="_blank" NavigateUrl="~/Default.aspx?DocTypeURL=GR_DOC_IMAGE.ascx" Style="font-weight: bold;
                padding: 2px;" />
        </td>
    </tr>
</table>
<table>
    <tr>
        <td colspan="2">
            <fieldset>
                <legend>Файлы электронного образа листа учета и описания уникального документа</legend>
                <eq:eqSpecification ID="GR_tblDOC_DESCRIPT_IMAGE" runat="server" Width="100%" AllowInsert="false">
                    <eq:eqSpecificationColumn HeaderText="Описание">
                        <ItemTemplate>
                            <asp:TextBox ID="NAME" runat="server" Width="99%" />
                        </ItemTemplate>
                    </eq:eqSpecificationColumn>
                    <eq:eqSpecificationColumn HeaderText="Файл">
                        <ItemTemplate>
                            <eq2:RefFile ID="ID" runat="server" Mode="FileSystem" OnRequesting="RefFile_Requesting" />
                        </ItemTemplate>
                    </eq:eqSpecificationColumn>
                </eq:eqSpecification>
                <table>
                    <tr>
                        <td>
                            Добавить файл:
                        </td>
                        <td>
                            <eq2:RefFileUpload ID="RefFileUploadFile" runat="server" Mode="FileSystem" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            Доступен для интернет-поиска
        </td>
        <td>
            <asp:CheckBox ID="PUBLIC" runat="server" Checked="true" />
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            <eq:eqSpecification ID="GR_vDOC_DESCRIPT" runat="server" ShowNumering="false" Visible="false">
                <eq:eqSpecificationColumn>
                    <ItemTemplate>
                        <table>
                            <tr>
                                <td>
                                    <eq:eqTextBox ID="DESCRIPT_NUM" runat="server" Width="30px" Height="10px" Visible="true" />
                                    <eq:eqTextBox ID="DESCRIPT_DATE" runat="server" Width="30px" Height="10px" Visible="true" />
                                    <eq:eqTextBox ID="ANNOTATION" runat="server" Width="30px" Height="10px" TextMode="MultiLine"
                                        Visible="true" />
                                    <eq:eqTextBox ID="ART_DESIGN" runat="server" Width="30px" Height="10px" TextMode="MultiLine"
                                        Visible="true" />
                                    <eq:eqTextBox ID="PRECIOUS" runat="server" Width="30px" Height="10px" TextMode="MultiLine"
                                        Visible="true" />
                                    <eq:eqTextBox ID="SIZES" runat="server" Width="30px" Height="10px" TextMode="MultiLine"
                                        Visible="true" />
                                    <eq:eqTextBox ID="VOLUME" runat="server" Width="30px" Height="10px" TextMode="MultiLine"
                                        Visible="true" />
                                    <eq:eqTextBox ID="PHYSICAL_STATE" runat="server" Width="30px" Height="10px" TextMode="MultiLine"
                                        Visible="true" />
                                    <eq:eqTextBox ID="RESTORATION" runat="server" Width="30px" Height="10px" TextMode="MultiLine"
                                        Visible="true" />
                                    <eq:eqTextBox ID="PROTOCOL_ORGANIZATION" runat="server" Width="30px" Height="10px"
                                        TextMode="MultiLine" Visible="true" />
                                    <eq:eqTextBox ID="PROTOCOL_NUM" runat="server" Width="30px" Height="10px" TextMode="MultiLine"
                                        Visible="true" />
                                    <eq:eqTextBox ID="PROTOCOL_ROSARCH_NUM" runat="server" Width="30px" Height="10px"
                                        TextMode="MultiLine" Visible="true" />
                                    <eq:eqTextBox ID="PROTOCOL_ROSARCH_DATE" runat="server" Width="30px" Height="10px"
                                        TextMode="MultiLine" Visible="true" />
                                    <eq:eqTextBox ID="PROTOCOL_DATE" runat="server" Width="30px" Height="10px" TextMode="MultiLine"
                                        Visible="true" />
                                    <eq:eqTextBox ID="ID_ForFind" runat="server" Width="30px" Height="10px" TextMode="MultiLine"
                                        Visible="true" />
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </eq:eqSpecificationColumn>
            </eq:eqSpecification>
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            <eq:eqTextBox ID="NUM" runat="server" Width="50px" Height="20px" TextMode="MultiLine"
                Visible="false" />
            <eq:eqTextBox ID="DOC_DATE" runat="server" Width="50px" Height="20px" TextMode="MultiLine"
                Visible="false" />
            <eq:eqTextBox ID="SYS_STATUS" runat="server" Width="50px" Height="20px" TextMode="MultiLine"
                Visible="false" />
            <eq:eqTextBox ID="ID_DOC_DESCRIPT_CURRENT" runat="server" Width="50px" Height="20px"
                TextMode="MultiLine" Visible="false" />
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            <eq:eqTextBox ID="YEAR_FROM" runat="server" Width="50px" Height="20px" TextMode="MultiLine"
                Visible="false" />
            <eq:eqTextBox ID="YEAR_TO" runat="server" Width="50px" Height="20px" TextMode="MultiLine"
                Visible="false" />
            <eq:eqTextBox ID="EXACT_DATE_FROM" runat="server" Width="50px" Height="20px" TextMode="MultiLine"
                Visible="false" />
            <eq:eqTextBox ID="EXACT_DATE_TO" runat="server" Width="50px" Height="20px" TextMode="MultiLine"
                Visible="false" />
            <eq:eqTextBox ID="ID_CENTURY_TO" runat="server" Width="50px" Height="20px" TextMode="MultiLine"
                Visible="false" />
        </td>
    </tr>
</table>
