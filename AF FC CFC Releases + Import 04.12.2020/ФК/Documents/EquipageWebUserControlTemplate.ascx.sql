--created by Anatoly Mel'kov on 2009-08-11
--������ ���������
create table tblEquipageWebUserControlTemplate
(
--<<��������� ����
ID uniqueidentifier primary key,
OwnerID uniqueidentifier,
CreationDateTime datetime,
StatusID uniqueidentifier,
Deleted int,
-->>��������� ����

DocNum int identity (1,1),
Flag bit,
Comment nvarchar(max),
MyTime datetime,
MyDate datetime
)
go
create view vEquipageWebUserControlTemplate as
	select  tblEquipageWebUserControlTemplate.* from tblEquipageWebUserControlTemplate
go
create table tblEquipageWebUserControlTemplateSpec
(
--<<��������� ����
ID uniqueidentifier primary key,
DocID uniqueidentifier,
OwnerID uniqueidentifier,
CreationDateTime datetime,
StatusID uniqueidentifier,
RowID int,
-->>��������� ����

Flag bit,
Comment nvarchar(255),
MyTime datetime,
MyDate datetime
)
go
create view vEquipageWebUserControlTemplateSpec as
select tblEquipageWebUserControlTemplateSpec.* from tblEquipageWebUserControlTemplateSpec
go

/*
drop view dbo.vEquipageWebUserControlTemplate
drop view dbo.vEquipageWebUserControlTemplateSpec
drop table dbo.tblEquipageWebUserControlTemplate
drop table dbo.tblEquipageWebUserControlTemplateSpec
*/