﻿Imports System.Data.SqlClient
Partial Public Class GR_SEARCH
    Inherits BaseDoc

    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        DataSourceSearch.ConnectionString = My.Settings.DatabaseConnectionString
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
        End If
    End Sub

    Private ReadOnly Property SearchPattern() As String()
        Get
            Dim S = TextBoxSearchPattern.Text
            Dim R As New List(Of String)

            If S.Contains("""") Then
                For Each p In S.Split(New String() {""""}, StringSplitOptions.RemoveEmptyEntries)
                    R.Add("""" & p & """")
                Next
            Else
                For Each p In S.Split(New String() {" "}, StringSplitOptions.RemoveEmptyEntries)
                    R.Add("""*" & p & "*""")
                Next
            End If
            If R.Count = 0 Then
                R.Add("*")
            End If
            Return R.ToArray
        End Get
    End Property

    Private ReadOnly Property FullSearchPattern() As String
        Get
            Dim S = TextBoxSearchPattern.Text
            Dim R As New List(Of String)

            If S.Contains("""") Then
                For Each p In S.Split(New String() {""""}, StringSplitOptions.RemoveEmptyEntries)
                    R.Add("""" & p & """")
                Next
            Else
                For Each p In S.Split(New String() {" "}, StringSplitOptions.RemoveEmptyEntries)
                    '  R.Add("""*" & p & "*""")
                    R.Add("FORMSOF(INFLECTIONAL, " & p & ") OR " & p)

                Next
            End If
            If R.Count = 0 Then
                R.Add("*")
            End If
            Return R.Item(0).ToString
        End Get
    End Property
    Private ReadOnly Property FilterPattern() As String()
        Get

            Dim R As New List(Of String)
            'составим строку относительно галочек
            If NAME.Checked = True Then
                R.Add("InName")
            End If
            If SELF_NAME.Checked = True Then
                R.Add("InSelfName")
            End If
            If HISTORY.Checked = True Then
                R.Add("InHistory")
            End If
            If APPROX_DATE.Checked = True Then
                R.Add("InApproxDate")
            End If
            If CREATION_DATE.Checked = True Then
                R.Add("InCreationDate")
            End If
            If ANNOTATION.Checked = True Then
                R.Add("InAnnotation")
            End If
            If ANNOTATION.Checked = True Then
                R.Add("InAnnotation")
            End If
            If ART_DESIGN.Checked = True Then
                R.Add("InArtDesign")
            End If
            If PRECIOUS.Checked = True Then
                R.Add("InPrecious")
            End If
            If PALEO_FEATURES.Checked = True Then
                R.Add("InPaleoFeatures")
            End If
            If RESTORATION.Checked = True Then
                R.Add("InRestoration")
            End If
            If SEALS.Checked = True Then
                R.Add("InSeals")
            End If
            If SIZES.Checked = True Then
                R.Add("InSizes")
            End If
            If VOLUME.Checked = True Then
                R.Add("InVolume")
            End If
            If PHYSICAL_STATE.Checked = True Then
                R.Add("InPhisicalState")
            End If
            If PROTOCOL_ORGANIZATION.Checked = True Then
                R.Add("InProtocolOrganization")
            End If
            If AUTHOR.Checked = True Then
                R.Add("InAuthor")
            End If
            If R.Count = 0 Then
                R.Add("NonFilter")
                If Len(TextBoxSearchPattern.Text) > 0 Then
                    AddMessage("Выберите поля для поиска")
                End If
                TextBoxSearchPattern.Text = vbNullString
                'если не заданы поля для поиска и задан текст , то поиск не осуществлять

            End If
            Return R.ToArray
        End Get
    End Property

    Private ReadOnly Property AdvancedSearchArchive() As String
        Get
            Dim S = ID_ARCHIVE.Text.ToString
            If Len(S) = 0 Then
                S = "0"
            End If
            Return S
        End Get
    End Property
    Private ReadOnly Property AdvancedSearchCarrier() As String
        Get
            Dim S = ID_CARRIER.DocID.ToString
            If S = "00000000-0000-0000-0000-000000000000" Then
                S = "0"
            End If
            Return S
        End Get
    End Property
    Private ReadOnly Property AdvancedSearchCenturyFrom() As String
        Get
            Dim S = ID_CENTURY_FROM.Text.ToString

            If Len(S) = 0 Then
                If Len(ID_CENTURY_TO.Text.ToString) > 0 Then
                    S = "00000000-0200-0000-0000-000000000144"
                Else
                    S = "0"
                End If

            End If
            Return S
        End Get
    End Property
    Private ReadOnly Property AdvancedSearchCenturyTo() As String
        Get
            Dim S = ID_CENTURY_TO.Text.ToString
            If Len(S) = 0 Then
                If Len(ID_CENTURY_FROM.Text.ToString) > 0 Then
                    S = "00000000-0200-0000-0000-000000000044"
                Else
                    S = "0"
                End If

            End If
            Return S
        End Get
    End Property
    Private ReadOnly Property AdvancedSearchDocKind() As String
        Get
            Dim S = ID_DOC_KIND.Text.ToString
            If Len(S) = 0 Then
                S = "0"
            End If
            Return S
        End Get
    End Property
    Private ReadOnly Property AdvancedSearchLanguage() As String
        Get
            Dim S = ID_LANGUAGE.Text.ToString
            If Len(S) = 0 Then
                S = "0"
            End If
            Return S
        End Get
    End Property
    Private ReadOnly Property AdvancedSearchProperty() As String
        Get
            Dim S = ID_PROPERTY.Text.ToString
            If Len(S) = 0 Then
                S = "0"
            End If
            Return S
        End Get
    End Property

    Private ReadOnly Property AdvancedSearchDateFrom() As String
        Get
            Dim S As String
            If Len(DATE_FROM.Text) = 0 Then
                ' S = "1000-01-01"
                S = "0"
            Else
                S = DATE_FROM.Text
            End If
            Return S
        End Get
    End Property

    Private ReadOnly Property AdvancedSearchDateTo() As String
        Get
            Dim S As String
            If Len(DATE_TO.Text) = 0 Then
                '  S = "2100-01-01"
                S = "0"
            Else
                S = DATE_TO.Text
            End If
            Return S
        End Get
    End Property
    Public Function ChkSearch() As String
        Dim msg As String
        msg = vbNullString

        If FilterPattern.Count = 0 And Len(TextBoxSearchPattern.Text) > 0 Then
            'не выбрано ни одно поле для поиска
            msg = "Выберите поля для поиска"
            AddMessage(msg)
        End If

        'если не заданы условия поиска и не задана строка поиска "Задан пустой поисковый запрос"
        If Len(TextBoxSearchPattern.Text) = 0 Then
            'надо проверить , что бы хотя бы одно условие было
        End If


        Return msg
    End Function

    Private Sub ButtonSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click
        '
        Try
            Dim ChkMsg = vbNullString
            ChkSearch()

            Dim s = ""
            For Each p In SearchPattern
                If s <> "" Then
                    s &= " AND "
                End If
                s &= p
            Next

            DataSourceSearch.SelectParameters("fulltext_pattern").DefaultValue = FullSearchPattern
            DataSourceSearch.DataBind()
            DataSourceSearch.SelectParameters("match_pattern").DefaultValue = s
            DataSourceSearch.DataBind()

            Dim f = ""

            For Each p In FilterPattern
                If f <> "" Then
                    f &= " AND "
                End If
                f &= p
            Next

            DataSourceSearch.SelectParameters("filter").DefaultValue = f
            DataSourceSearch.DataBind()

            DataSourceSearch.SelectParameters("archive").DefaultValue = AdvancedSearchArchive
            DataSourceSearch.DataBind()
            DataSourceSearch.SelectParameters("carrier").DefaultValue = AdvancedSearchCarrier
            DataSourceSearch.DataBind()
            DataSourceSearch.SelectParameters("kind").DefaultValue = AdvancedSearchDocKind
            DataSourceSearch.DataBind()
            DataSourceSearch.SelectParameters("property").DefaultValue = AdvancedSearchProperty
            DataSourceSearch.DataBind()
            DataSourceSearch.SelectParameters("century_from").DefaultValue = AdvancedSearchCenturyFrom
            DataSourceSearch.DataBind()
            DataSourceSearch.SelectParameters("century_to").DefaultValue = AdvancedSearchCenturyTo
            DataSourceSearch.DataBind()
            DataSourceSearch.SelectParameters("language").DefaultValue = AdvancedSearchLanguage
            DataSourceSearch.DataBind()
            DataSourceSearch.SelectParameters("date_from").DefaultValue = AdvancedSearchDateFrom
            DataSourceSearch.DataBind()
            DataSourceSearch.SelectParameters("date_to").DefaultValue = AdvancedSearchDateTo
            DataSourceSearch.DataBind()

           

        Catch ex As Exception

            AddMessage(ex.Message)

        End Try

    End Sub

    Private Sub DataSourceSearch_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles DataSourceSearch.Selected
        LabelMessage.Text = String.Format("Найдено: {0}", e.AffectedRows)
        'проверка на пустой поисковый запрос
        If Len(TextBoxSearchPattern.Text) = 0 And AdvancedSearchArchive = "0" And AdvancedSearchCarrier = "0" _
        And AdvancedSearchDocKind = "0" And AdvancedSearchProperty = "0" And AdvancedSearchCenturyFrom = "0" _
        And AdvancedSearchCenturyTo = "0" And AdvancedSearchLanguage = "0" And AdvancedSearchDateFrom = "0" _
        And AdvancedSearchDateTo = "0" Then
            'если не задан текст для поиска, то должены быть выбраны условия для расширенного поиска
            LabelMessage.Text = String.Format("Задан пустой поисковый запрос", e.AffectedRows)

        End If
    End Sub

    Public Function evalHref(ByVal Container As Object)
        Dim Data = DirectCast(DataBinder.GetDataItem(Container), DataRowView)
        Return BasePage.GetDocURL("GR_DOCUMENT_LIGHT.ascx", Data("ID"), False)

    End Function

    ''' <summary>
    ''' Возвращает ссылку на контейнер, который содержит контейнер, который содержит один из результатов поиска.
    ''' </summary>
    ''' <param name="Container"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function evalUpperHref(ByVal Container As Object) ' (Roman 300713)
        Dim Data = DirectCast(DataBinder.GetDataItem(Container), DataRowView)
        Dim cmdText As String
        Dim cmd As SqlCommand

        cmdText = <sql>SELECT TOP 1 ID FROM GR_tblDOCUMENT WHERE ID=@ID </sql>

        cmd = New SqlCommand(cmdText)
        cmd.Parameters.AddWithValue("@ID", Data("ID"))
        Return BasePage.GetDocURL("GR_DOCUMENT_LIGHT.ascx", AppSettings.ExecCommandScalar(cmd), False)
    End Function

    Public Function evalTitle(ByVal Container As Object)
        'надо проверить - если предыдущее название не изменилось, то заголовок не добавлять. Nata 16/09/13
        Dim Data = DirectCast(DataBinder.GetDataItem(Container), DataRowView)

        'If IsDBNull(Data("Code")) Then
        '    Return Data("Header") & ". " & Data("Name")
        'Elsef
        '    Return Data("Header") & ". №" & Data("Code") & " . " & Data("Name")
        'End If

        Return Data("row_num") & ". " & "№" & Data("NUM") & " " & Data("Name")
    End Function

    Public Function evalText(ByVal Container As Object)
        Dim Data = DirectCast(DataBinder.GetDataItem(Container), DataRowView)
        Dim S As New String(Data("Search").ToString)
        Dim F As New String(Data("SearchField").ToString)
        Dim R As New StringBuilder
        Dim Arr As List(Of Triplet)
        Dim Sorter As Comparison(Of Triplet) = Function(X, Y) IIf(X.Second = Y.Second, 0, IIf(X.Second > Y.Second, 1, -1))

        Const TAG_BEGIN = "<b style='color: #0000ff;'>"
        Const TAG_END = "</b>"
        Const DIST = 30

        Const TAG_BOLD_BEGIN = "<B>"
        Const TAG_BOLD_END = "</B>"

        Dim before1 As Integer, before2 As Integer, before As Integer = 0
        Dim after1 As Integer, after2 As Integer, after As Integer = 0
        Dim first1 As Integer = 0, first2 As Integer = 0
        Dim last1 As Integer = 0, last2 As Integer = 0
        Dim match As Integer = 0, lastmatch As Integer = 0

        Arr = New List(Of Triplet)
        For Each p In SearchPattern
            Dim pt = p.Trim(New Char() {""""}).Trim(New Char() {"*"})
            match = S.IndexOf(pt, lastmatch, StringComparison.InvariantCultureIgnoreCase)
            If match <> -1 Then
                Arr.Add(New Triplet With {.First = pt, .Second = match, .Third = pt.Length})
            End If
        Next
        If Arr.Count > 0 Then
            Arr.Sort(Sorter)
            lastmatch = Arr.Last.Second + Arr.Last.Third

            For Each El In Arr
                first1 = El.Second
                If first1 < last1 Then Continue For
                first2 = first1 - DIST
                If first2 < 0 Then first2 = 0

                before1 = S.LastIndexOf(".", first1)
                before2 = S.LastIndexOf(" ", first2)
                If before1 <> -1 And before2 <> -1 Then
                    before = Math.Max(before1 + 1, before2)
                ElseIf before1 <> -1 And before2 = -1 Then
                    before = before1 + 1
                ElseIf before1 = -1 And before2 <> -1 Then
                    before = before2
                Else
                    before = 0
                End If
                R.Append(TAG_BOLD_BEGIN)
                R.Append(F) 'nata
                R.Append(": ")
                R.Append(TAG_BOLD_END)
                If after >= before Then
                    R.Append(S.Substring(last1, first1 - last1))
                Else
                    R.Append(S.Substring(last1, after - last1))
                    If before <> (before1 + 1) Then R.Append(" ... ")
                    R.Append(S.Substring(before, first1 - before))
                End If
                R.Append(TAG_BEGIN)
                R.Append(El.First)
                R.Append(TAG_END)

                last1 = El.Second + El.Third
                last2 = last1 + DIST
                If last2 > S.Length Then last2 = S.Length

                after1 = S.IndexOf(".", last1)
                after2 = S.IndexOf(" ", last2)
                If after1 <> -1 And after2 <> -1 Then
                    after = Math.Min(after1 + 1, after2)
                ElseIf after1 <> -1 And after2 = -1 Then
                    after = after1 + 1
                ElseIf after1 = -1 And after2 <> -1 Then
                    after = after2
                Else
                    after = S.Length
                End If
            Next

            R.Append(S.Substring(last1, after - last1))
            If after <> (after1 + 1) Then R.Append(" ... ")
        End If

        Return R.ToString
    End Function

    Private Sub ADVANCED_SEARCH_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ADVANCED_SEARCH.CheckedChanged
        If ADVANCED_SEARCH.Checked = True Then
            Panel_ADVANCED_SEARCH.Visible = True
        Else
            Panel_ADVANCED_SEARCH.Visible = False
        End If
    End Sub

    Private Sub Prop_Dating_type_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Prop_Dating_type.SelectedIndexChanged
        If Prop_Dating_type.SelectedIndex = 0 Then
            Panel_DATE.Visible = True
            Panel_CENTURY.Visible = False
        End If
        If Prop_Dating_type.SelectedIndex = 1 Then
            Panel_DATE.Visible = False
            Panel_CENTURY.Visible = True
        End If
        ID_CENTURY_FROM.Text = vbNullString
        ID_CENTURY_TO.Text = vbNullString
        DATE_FROM.Text = vbNullString
        DATE_TO.Text = vbNullString
    End Sub

    Private Sub ALL_FIELD_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ALL_FIELD.CheckedChanged
        Dim flg = ALL_FIELD.Checked
        NAME.Checked = flg
        SELF_NAME.Checked = flg
        HISTORY.Checked = flg
        APPROX_DATE.Checked = flg
        CREATION_DATE.Checked = flg
        ANNOTATION.Checked = flg
        ANNOTATION.Checked = flg
        ART_DESIGN.Checked = flg
        PRECIOUS.Checked = flg
        PALEO_FEATURES.Checked = flg
        RESTORATION.Checked = flg
        SEALS.Checked = flg
        SIZES.Checked = flg
        VOLUME.Checked = flg
        PHYSICAL_STATE.Checked = flg
        AUTHOR.Checked = flg
        PROTOCOL_ORGANIZATION.Checked = flg
        ' TextBoxSearchPattern.Text = vbNullString
    End Sub
End Class