﻿'------------------------------------------------------------------------------
' <auto-generated>
'     Этот код создан программой.
'     Исполняемая версия:2.0.50727.5472
'
'     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
'     повторной генерации кода.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On



Partial Public Class SEARCH

    '''<summary>
    '''TextBoxSearchPattern элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents TextBoxSearchPattern As Global.Equipage.WebUI.eqTextBox

    '''<summary>
    '''ButtonSearch элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents ButtonSearch As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''LabelMessage элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents LabelMessage As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''DataSourceSearch элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents DataSourceSearch As Global.System.Web.UI.WebControls.SqlDataSource

    '''<summary>
    '''ListViewResults элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents ListViewResults As Global.System.Web.UI.WebControls.ListView
End Class
