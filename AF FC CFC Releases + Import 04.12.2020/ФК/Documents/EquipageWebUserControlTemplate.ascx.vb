﻿Imports Equipage.WebUI.eqMath
Imports Equipage.WebUI

''' <summary>
''' Шаблон контрола, при нереносе в свой проект изменить комментарии
''' В шаблоне реализован пример нескольких загаловочный полей и спецификации
''' </summary>
''' <remarks>created by Anatoly Melkov on 2009-09-11</remarks>
Partial Public Class EquipageWebUserControlTemplate
    Inherits BaseDoc

#Region "Константы, переменные"

#End Region '>>"Константы, переменные"

#Region "Математические функции и рассчет даты, времени"

#End Region '>>"Математические функции и рассчет времени"

#Region "Events, события страницы и контролов"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
#End Region '>>"Events, события страницы и контролов"

#Region "Переопределенные функции"

#End Region '>>"Переопределенные функции"

#Region "Функции переходов по статусам"

#End Region '>>"Функции переходов по статусам"

#Region "Работа с БД"

#End Region '>>"Работа с БД"

#Region "Права доступа, показать/скрыть коптрол, разрешить/запретить редактирование"

#End Region '>>"Права доступа, показать/скрыть коптрол, разрешить/запретить редактирование"

#Region "Специальные функции для Help-файла"

    Public Function IsAccountWorkClear() As Boolean

    End Function

    Public Function IsAccountWorkFilled() As Boolean

    End Function

#End Region '>>"Специальные функции для Help-файла"

#Region "Свойства"

#End Region '>>"Свойства"

#Region "Прочие"

#End Region '>>"Прочие"

End Class