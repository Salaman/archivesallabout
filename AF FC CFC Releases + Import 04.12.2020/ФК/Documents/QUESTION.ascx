﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="QUESTION.ascx.vb" Inherits="WebApplication.QUESTION" %>
<div style="border: solid 1px black; padding: 5px;">
    <table style="width: 700px;">
        <colgroup>
            <col style="width: 200px;" />
            <col />
        </colgroup>
        <tr>
            <td>
                Содержание вопроса:
            </td>
        </tr>
        <tr>
            <td>
                <eq:eqTextBox ID="NAME" runat="server" TextMode="MultiLine" Rows="4" />
            </td>
        </tr>
        <tr>
            <td>
                <div style="border: 0; padding: 2px;">
                    <eq:eqMenu ID="MenuTabs" runat="server" Orientation="Horizontal" StaticEnableDefaultPopOutImage="false" ItemWrap="false">
                        <Items>
                            <asp:MenuItem Text="Фонды" Value="0" Selected="true" />
                            <asp:MenuItem Text="Описи" Value="1" />
                            <asp:MenuItem Text="Единицы хранения" Value="2" />
                            <asp:MenuItem Text="Документы" Value="3" />
                        </Items>
                        <StaticSelectedStyle CssClass="MenuItemStyleSelected" />
                    </eq:eqMenu>
                </div>
            </td>
        </tr>
    </table>
    <asp:MultiView ID="MultiViewTabs" runat="server" ActiveViewIndex="0">
        <%--Межфондовый--%>
        <asp:View runat="server">
            <table style="width: 700px;">
                <tr>
                    <td>
                        <fieldset>
                            <legend>Фонды</legend>
                            <eq:eqSpecification ID="vREF_QUESTION_FUND" runat="server">
                                <eq:eqSpecificationColumn>
                                    <ItemTemplate>
                                        <eq2:eqDocument ID="ISN_OBJ" runat="server" DocTypeURL="FUND.ascx" ListDocMode="NewWindowAndSelect"
                                            KeyName="ISN_FUND" LoadDocMode="NewWindowAndSelect" ShowingFields="ARCHIVE_NAME_SHORT,FUND_NUM"
                                            ShowingFieldsFormat="{0} /Фонд {1}" CallbackMode="false" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
            </table>
        </asp:View>
        <%--Внутрифондовый--%>
        <asp:View runat="server">
            <table style="width: 700px;">
                <tr>
                    <td>
                        <fieldset>
                            <legend>Описи</legend>
                            <eq:eqSpecification ID="vREF_QUESTION_INVENTORY" runat="server">
                                <eq:eqSpecificationColumn>
                                    <ItemTemplate>
                                        <eq2:eqDocument ID="ISN_OBJ" runat="server" DocTypeURL="INVENTORY.ascx" ListDocMode="NewWindowAndSelect"
                                            KeyName="ISN_INVENTORY" LoadDocMode="NewWindowAndSelect" ShowingFields="ARCHIVE_NAME_SHORT,FUND_NUM,INVENTORY_NUM_DISPLAY"
                                            ShowingFieldsFormat="{0} /Фонд {1} /Опись {2}" CallbackMode="false" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
            </table>
        </asp:View>
        <%--В рамках описи--%>
        <asp:View runat="server">
            <table style="width: 700px;">
                <tr>
                    <td>
                        <fieldset>
                            <legend>Ед.хр./ед.уч.</legend>
                            <eq:eqSpecification ID="vREF_QUESTION_UNIT" runat="server">
                                <eq:eqSpecificationColumn>
                                    <ItemTemplate>
                                        <eq2:eqDocument ID="ISN_OBJ" runat="server" DocTypeURL="UNIT.ascx" ListDocMode="NewWindowAndSelect"
                                            KeyName="ISN_UNIT" LoadDocMode="NewWindowAndSelect" ShowingFields="ARCHIVE_NAME_SHORT,FUND_NUM,INVENTORY_NUM,UNIT_NUM"
                                            ShowingFieldsFormat="{0} /Фонд {1} /Опись {2} /Ед.хр. {3}" CallbackMode="false" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
            </table>
        </asp:View>
        <%--В рамках дела--%>
        <asp:View runat="server">
            <table style="width: 700px;">
                <tr>
                    <td>
                        <fieldset>
                            <legend>Документы</legend>
                            <eq:eqSpecification ID="vREF_QUESTION_DOCUMENT" runat="server">
                                <eq:eqSpecificationColumn HeaderText="Название документа">
                                    <ItemTemplate>
                                        <eq2:eqDocument ID="ISN_OBJ" runat="server" DocTypeURL="DOCUMENT.ascx" ListDocMode="NewWindowAndSelect"
                                            KeyName="ISN_DOCUM" LoadDocMode="NewWindowAndSelect" ShowingFields="ARCHIVE_NAME_SHORT,FUND_NUM,INVENTORY_NUM,UNIT_NUM,DOC_NUM"
                                            ShowingFieldsFormat="{0} /Фонд {1} /Опись {2} /Ед.хр. {3} /Документ {4}" CallbackMode="false" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Номера листов">
                                    <ItemTemplate>
                                        <asp:TextBox ID="Pages" runat="server" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
            </table>
        </asp:View>
    </asp:MultiView>
    <table style="width: 700px;">
        <tr>
            <td>
                <fieldset>
                    <legend>Рубрики</legend>
                    <eq:eqSpecification ID="vREF_CLS_QUESTION" runat="server">
                        <eq:eqSpecificationColumn>
                            <ItemTemplate>
                                <eq2:eqDocument ID="ISN_CLS" runat="server" DocTypeURL="CLS701.ascx" ListDocMode="NewWindowAndSelect"
                                    KeyName="ISN_CLS" LoadDocMode="NewWindowAndSelect" ShowingFields="NAME" ShowingFieldsFormat="{0}" CallbackMode="false" />
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                    </eq:eqSpecification>
                </fieldset>
            </td>
        </tr>
        <tr>
            <td>
                <fieldset>
                    <legend>Тематика к вопросу</legend>
                    <eq:eqSpecification ID="vREF_CLS_QUESTION_TEMATIK" runat="server">
                        <eq:eqSpecificationColumn>
                            <ItemTemplate>
                                <eq2:eqDocument ID="ISN_CLS" runat="server" DocTypeURL="CLS701.ascx" ListDocMode="NewWindowAndSelect"
                                    KeyName="ISN_CLS" LoadDocMode="NewWindowAndSelect" ShowingFields="NAME" ShowingFieldsFormat="{0}"
                                    AdditionalListDocWhereString="TEMATIK" CallbackMode="false" />
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                    </eq:eqSpecification>
                </fieldset>            
            </td>
        </tr>
        <tr>
            <td>
                <fieldset>
                    <legend>Персоналии к вопросу</legend>
                    <eq:eqSpecification ID="vREF_CLS_QUESTION_PERSONA" runat="server">
                        <eq:eqSpecificationColumn>
                            <ItemTemplate>
                                <eq2:eqDocument ID="ISN_CLS" runat="server" DocTypeURL="CLS701.ascx" ListDocMode="NewWindowAndSelect"
                                    KeyName="ISN_CLS" LoadDocMode="NewWindowAndSelect" ShowingFields="NAME" ShowingFieldsFormat="{0}"
                                    AdditionalListDocWhereString="PERSONA" CallbackMode="false" />
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                    </eq:eqSpecification>
                </fieldset>            
            </td>   
        </tr>
        <tr>       
            <td>
                <fieldset>
                    <legend>География к вопросу</legend>
                    <eq:eqSpecification ID="vREF_CLS_QUESTION_GEOGRAF" runat="server">
                        <eq:eqSpecificationColumn>
                            <ItemTemplate>
                                <eq2:eqDocument ID="ISN_CLS" runat="server" DocTypeURL="CLS701.ascx" ListDocMode="NewWindowAndSelect"
                                    KeyName="ISN_CLS" LoadDocMode="NewWindowAndSelect" ShowingFields="NAME" ShowingFieldsFormat="{0}"
                                    AdditionalListDocWhereString="GEOGRAF" CallbackMode="false" />
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                    </eq:eqSpecification>
                </fieldset>            
            </td>  
        </tr>
        <tr>    
            <td>
                <fieldset>
                    <legend>Ключевые слова к вопросу</legend>
                    <eq:eqSpecification ID="vREF_CLS_QUESTION_WORD" runat="server">
                        <eq:eqSpecificationColumn>
                            <ItemTemplate>
                                <eq2:eqDocument ID="ISN_CLS" runat="server" DocTypeURL="CLS701.ascx" ListDocMode="NewWindowAndSelect"
                                    KeyName="ISN_CLS" LoadDocMode="NewWindowAndSelect" ShowingFields="NAME" ShowingFieldsFormat="{0}"
                                    AdditionalListDocWhereString="WORD" CallbackMode="false" />
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                    </eq:eqSpecification>
                </fieldset>            
            </td>            
        </tr>
    </table>
</div>
