﻿Imports System.Web
Imports System.Web.Services

Public Class GR_DOC_IMAGE1
    Implements System.Web.IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        ' Dim originalPath = "C:\GR_Files\Penguins.jpeg"
        Dim originalPath = "http://unikdoc.rusarchives.ru/GRSearch/image.ashx?Preview=3&File=%D0%A0%D0%93%D0%90%D0%9B%D0%98\0276\001\00053\Preview\00000002_lod3.jpg"
        'context.Response.ContentType = "text/plain"
        'context.Response.Write("Hello World!")
        context.Response.ContentType = "image/jpeg"
        context.Response.WriteFile(originalPath)

        '     Response.ContentType = "image/jpeg";

        'Response.OutputStream.Write(rdr.GetSqlBinary(0).Value, 0, rdr.GetSqlBinary(0).Length);

    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class