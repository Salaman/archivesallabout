﻿Imports System.Data.SqlClient
Imports ImportLib
Imports System.Web.Configuration
Imports MiscLib.db

Imports System.IO


'Imports CopyImportUtil.UtilForm



''' <summary>
''' Очень много кода этого класса повзаимствовано из DataBaseSetup::CreateForm.vb
''' Ресурсы так же взаимствуются из этого проекта. Они копируются в папку DbSetupResources_Copy скриптом "События построения" (задается в настройках сборки проекта)
''' </summary>
''' <remarks></remarks>
Partial Public Class DataExport
    Inherits BaseDoc

    Dim connString As String = eqSQLResourceConfigHelper.GetConnectionString(WebConfigurationManager.OpenWebConfiguration("~"))
    Dim cnnTemp As SqlConnection = New SqlConnection(connString) 'AppSettings.Cnn
    Dim cnnReal As SqlConnection = New SqlConnection(connString)
    Dim connStrB As SqlConnectionStringBuilder = New SqlConnectionStringBuilder(connString)


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        AppSettings.ExecCommandScalar(New SqlCommand("DELETE FROM tblDataExport WHERE CreationDateTime < (SELECT MAX(CreationDateTime) FROM tblDataExport)"))

        Dim cmd As New SqlCommand("SELECT TOP 1 fcDbName FROM tblDataExport ORDER BY CreationDateTime DESC")
        fcDbName.Text = AppSettings.ExecCommandScalar(cmd)

        fcDbBacPath.Text = Path.Combine(Path.Combine(getTempPath(), HiddenImport.bacFolderName), connStrB.InitialCatalog)

        cmd = New SqlCommand("SELECT TOP 1 isZiped FROM tblDataExport ORDER BY CreationDateTime DESC")
        isZiped.Checked = AppSettings.ExecCommandScalar(cmd)

    End Sub

    Private Function getFileCriationTime(ByVal fileName As String) As DateTime
        Dim ret As DateTime?
        Try
            ret = File.GetCreationTime(fileName)
        Catch ex As Exception
            ret = Nothing
        End Try
        Return ret
    End Function

    Private Sub just_do_it_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles just_do_it.Click
        Try
            Dim tbBaseName As String = "tmpFCtbl"

            Dim oldFileCriationTime As DateTime
            Dim hi As HiddenImport
            Try
                If refBacFile.Enabled = False Then
                    oldFileCriationTime = Nothing
                Else
                    oldFileCriationTime = getFileCriationTime(Path.Combine(fcDbBacPath.Text, refBacFile.Text))
                End If
                hi = New HiddenImport(fcDbBacPath.Text, Server.MapPath("~\"), isZiped.Checked)
                hi.mkDir()
                hi.mkTmpDb(tbBaseName)
                cnnTemp.Open()
                cnnReal.Open()
            Catch ex As Exception
                AddMessage("Исключение на подготовительном этапе")
                Throw ex
            End Try

            Using cmd As New SqlCommand
                cmd.Connection = cnnTemp
                cmd.CommandTimeout = 600000

                ''<создание>
                Try
                    cmd.CommandText = "USE [" & connStrB.InitialCatalog & "]"
                    cmd.ExecuteNonQuery()
                    ''</создание>

                    ''<экспорт>
                    hi.import(tbBaseName)
                    ''</экспорт>

                    ''<бэкап>
                    Dim pathToBak As String = System.IO.Path.Combine(fcDbBacPath.Text, fcDbName.Text + ".bak")
                    Dim scsb As SqlConnectionStringBuilder = New SqlConnectionStringBuilder(connString)
                    scsb.InitialCatalog = tbBaseName
                    Dim o As dbObject = New dbObject(scsb)
                    o.takeBak(pathToBak)
                    'hi.makeBak(pathToBak)
                    'ServerInfo.BackupDataBase(cnnTemp, connStrB.InitialCatalog, pathToBak)
                    ''</бэкап>

                    ''<сжатие>
                    If isZiped.Checked Then
                        makeGZip(pathToBak)
                        If My.Computer.FileSystem.FileExists(pathToBak + ".gzip") Then
                            My.Computer.FileSystem.DeleteFile(pathToBak)
                        End If
                    End If
                    ''</сжатие>

                Catch ex As Exception
                    AddMessage("Исключение при работе с БД источника")
                    Throw ex
                Finally
                    'unlockDb(cmd, connStrB.InitialCatalog)
                    deleteDb(cmd, tbBaseName)
                End Try
            End Using

            refBacFileSetFile()

            Dim newFileCriationTime As DateTime
            If refBacFile.Enabled = False Then
                newFileCriationTime = Nothing
            Else
                newFileCriationTime = getFileCriationTime(Path.Combine(fcDbBacPath.Text, refBacFile.Text))
            End If

            Dim erFlag As Boolean = False
            If oldFileCriationTime = Nothing Then
                If newFileCriationTime = Nothing Then
                    erFlag = True
                End If
            ElseIf newFileCriationTime <= oldFileCriationTime Then
                erFlag = True
            End If

            If erFlag Then
                AddMessage("Некорректное завершение утилиты импорта БД.")
            End If
        Catch ex As Exception
            AddMessage("В процессе экспорта возникли ошибки. Не удалось создать bak-файл.")
        End Try
        

    End Sub

    Private Sub makeEmptyDb(ByRef cmd As SqlCommand, ByVal dbName As String)
        cmd.CommandText = <sql>
    USE master
    IF EXISTS(select * from sys.databases where name='%dbName%')
    BEGIN
        alter database %dbName%
        set offline with rollback immediate
        alter database %dbName%
        set online

        DROP DATABASE %dbName%
    END
                                    </sql>.FirstNode().ToString().Replace("%dbName%", dbName)
        cmd.ExecuteNonQuery()
        cmd.CommandText = "CREATE DATABASE " + dbName
        cmd.ExecuteNonQuery()
    End Sub


    Private Sub makeDbStruct(ByRef cmd As SqlCommand, ByVal dbName As String)
        Dim scriptsRes = My.Resources.CreateScript & vbCrLf & My.Resources.AddScript_FC
        Dim scripts = scriptsRes.Split(New String() {vbCrLf & "GO" & vbCrLf}, StringSplitOptions.RemoveEmptyEntries)
        Dim scriptsLen = scripts.Length

        cmd.CommandText = "USE [" & dbName & "]"
        cmd.ExecuteNonQuery()

        For i As Integer = 0 To scriptsLen - 1
            Dim script = scripts(i)

            cmd.CommandText = script.Replace("#DataBaseName#", dbName)
            Try
                Dim affectedRows = cmd.ExecuteNonQuery
            Catch ex As Exception
                Throw ex
            End Try
        Next

        Dim tryScriptRes = My.Resources.SecurityScript
        Dim tryScripts = tryScriptRes.Split(New String() {vbCrLf & "GO" & vbCrLf}, StringSplitOptions.RemoveEmptyEntries)
        Dim tryScriptsLen = tryScripts.Length

        For i As Integer = 0 To tryScriptsLen - 1
            Dim tryScript = tryScripts(i)

            Try
                cmd.CommandText = tryScript.Replace("#DataBaseName#", dbName).Replace("#ServerName#", Environment.MachineName)
                cmd.ExecuteNonQuery()
            Catch ex As Exception
            End Try
        Next
    End Sub

    Private Sub unlockDb(ByRef cmd As SqlCommand, ByVal dbName As String)
        cmd.CommandText = <sql>
                                        alter database %dbName%
                                        set offline with rollback immediate
                                        alter database %dbName%
                                        set online
                                    </sql>.FirstNode().ToString().Replace("%dbName%", dbName)
        cmd.ExecuteNonQuery()
    End Sub

    Private Sub deleteDb(ByRef cmd As SqlCommand, ByVal dbName As String)
        unlockDb(cmd, dbName)
        cmd.CommandText = "DROP DATABASE [" & dbName & "]"
        cmd.ExecuteNonQuery()
    End Sub


    Private Sub makeGZip(ByVal fileName As String)
        Dim fs As New IO.FileStream(fileName, IO.FileMode.Open)
        Dim fd As New IO.FileStream(fileName + ".gzip", IO.FileMode.Create)
        Dim csStrim As New System.IO.Compression.GZipStream(fd, IO.Compression.CompressionMode.Compress)
        Try
            Dim buffer(1024) As Byte
            Dim nRead As Integer

            nRead = fs.Read(buffer, 0, buffer.Length)
            While nRead > 0
                csStrim.Write(buffer, 0, nRead)
                nRead = fs.Read(buffer, 0, buffer.Length)
            End While
        Catch ex As Exception
            Throw ex
        Finally
            csStrim.Close()
            fs.Close()
            fd.Close()
        End Try
    End Sub

    Protected Sub refBacFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles refBacFile.Click
        Try
            Dim fArr As String() = Directory.GetFiles(Path.Combine(Path.Combine(getTempPath(), HiddenImport.bacFolderName), connStrB.InitialCatalog), "*.bak*")
            'Directory.GetFiles(Path.Combine(Path.Combine(Server.MapPath("~\"), HiddenImport.bacFolderName), connStrB.InitialCatalog), "*.bak*")
            If fArr.Length > 0 Then
                Dim file As System.IO.FileInfo = New System.IO.FileInfo(fArr(0))
                BasePage.AttachFile(file.Name, My.Computer.FileSystem.ReadAllBytes(file.FullName))
            End If
        Catch ex As Exception

        End Try
    End Sub


    Private Sub refBacFile_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles refBacFile.Load
        refBacFileSetFile()
    End Sub
    Private Sub refBacFileSetFile()
        Dim fArr As String() = Nothing
        Dim p As String = Path.Combine(Path.Combine(getTempPath(), HiddenImport.bacFolderName), connStrB.InitialCatalog)
        'Path.Combine(Path.Combine(Server.MapPath("~\"), HiddenImport.bacFolderName), connStrB.InitialCatalog)
        If Directory.Exists(p) Then
            fArr = Directory.GetFiles(p, "*.bak*")
        End If
        If fArr IsNot Nothing AndAlso fArr.Length > 0 Then
            refBacFile.Text = Path.GetFileName(fArr(0))
            refBacFile.Enabled = True
        Else
            refBacFile.Text = "(файл не найден)"
            refBacFile.Enabled = False
        End If
    End Sub

    Public Shared Function getTempPath() As String
        Dim TempPath As String = System.Configuration.ConfigurationManager.AppSettings.Item("TempPath")
        If TempPath Is Nothing Then
            Return Path.GetTempPath()
        Else
            Return TempPath
        End If
    End Function
End Class

Public Class HiddenImport
    Dim util As System.Diagnostics.Process

    Public Const bacFolderName As String = "ImportFcBak"

    'Dim dbName As String
    Dim bakName As String
    Dim serverDir As String
    Dim zip As Boolean

    Dim cpFiles = New String() {"CopyImportUtil.exe", "ImportLib.dll", "lock", "CopyImportUtil.recent" _
                                , "Microsoft.WindowsAPICodePack.dll", "Microsoft.WindowsAPICodePack.Shell.dll"}
    Dim fcDbBacPath

    Dim connString As String = eqSQLResourceConfigHelper.GetConnectionString(WebConfigurationManager.OpenWebConfiguration("~"))
    Dim connStrB As SqlConnectionStringBuilder = New SqlConnectionStringBuilder(connString)

    Public Sub New(ByVal _bakName As String, ByVal _serverDir As String, ByVal _zip As Boolean)
        'dbName = _dbName
        bakName = _bakName
        serverDir = _serverDir
        zip = _zip

        fcDbBacPath = Path.Combine(Path.Combine(DataExport.getTempPath(), bacFolderName), connStrB.InitialCatalog)
        'Path.Combine(Path.Combine(serverDir, bacFolderName), connStrB.InitialCatalog)
        _Recent_Path = Path.Combine(fcDbBacPath, "CopyImportUtil.recent")
    End Sub

    Public Sub mkDir()

        If Directory.Exists(fcDbBacPath) Then
            Directory.Delete(fcDbBacPath, True)
        End If
        Directory.CreateDirectory(fcDbBacPath)
        Dim binPath = Path.Combine(serverDir, "bin")

        For Each fName In cpFiles
            If File.Exists(Path.Combine(binPath, fName)) Then
                File.Copy(Path.Combine(binPath, fName), Path.Combine(fcDbBacPath, fName))
            End If
        Next
    End Sub

    Public Sub mkTmpDb(ByVal dbName As String)
        Using cnnTemp As New SqlConnection(connString)
            Dim cmd As New SqlCommand
            cmd.Connection = cnnTemp
            cmd.CommandTimeout = 600000
            cnnTemp.Open()

            ' makeEmptyDb
            cmd.CommandText = <sql>
    USE master
    IF EXISTS(select * from sys.databases where name='%dbName%')
    BEGIN
        alter database %dbName%
        set offline with rollback immediate
        alter database %dbName%
        set online

        DROP DATABASE %dbName%
    END
                                    </sql>.FirstNode().ToString().Replace("%dbName%", dbName)
            cmd.ExecuteNonQuery()
            cmd.CommandText = "CREATE DATABASE " + dbName
            cmd.ExecuteNonQuery()

            ' makeDbStruct
            Dim scriptsRes = My.Resources.CreateScript & vbCrLf & My.Resources.AddScript_FC
            Dim scripts = scriptsRes.Split(New String() {vbCrLf & "GO" & vbCrLf}, StringSplitOptions.RemoveEmptyEntries)
            Dim scriptsLen = scripts.Length

            cmd.CommandText = "USE [" & dbName & "]"
            cmd.ExecuteNonQuery()

            For i As Integer = 0 To scriptsLen - 1
                Dim script = scripts(i)

                cmd.CommandText = script.Replace("#DataBaseName#", dbName)
                Try
                    Dim affectedRows = cmd.ExecuteNonQuery
                Catch ex As Exception
                    Throw ex
                End Try
            Next

            Dim tryScriptRes = My.Resources.SecurityScript
            Dim tryScripts = tryScriptRes.Split(New String() {vbCrLf & "GO" & vbCrLf}, StringSplitOptions.RemoveEmptyEntries)
            Dim tryScriptsLen = tryScripts.Length

            For i As Integer = 0 To tryScriptsLen - 1
                Dim tryScript = tryScripts(i)

                Try
                    cmd.CommandText = tryScript.Replace("#DataBaseName#", dbName).Replace("#ServerName#", Environment.MachineName)
                    cmd.ExecuteNonQuery()
                Catch ex As Exception
                End Try
            Next
        End Using
    End Sub

    Private Sub unlockDb(ByRef cmd As SqlCommand, ByVal dbName As String)
        cmd.CommandText = <sql>
                                        alter database %dbName%
                                        set offline with rollback immediate
                                        alter database %dbName%
                                        set online
                                    </sql>.FirstNode().ToString().Replace("%dbName%", dbName)
        cmd.ExecuteNonQuery()
    End Sub

    Private Sub deleteDb(ByRef cmd As SqlCommand, ByVal dbName As String)
        unlockDb(cmd, dbName)
        cmd.CommandText = "DROP DATABASE [" & dbName & "]"
        cmd.ExecuteNonQuery()
    End Sub

    Private Sub makeGZip(ByVal fileName As String)
        Dim fs As New IO.FileStream(fileName, IO.FileMode.Open)
        Dim fd As New IO.FileStream(fileName + ".gzip", IO.FileMode.Create)
        Dim csStrim As New System.IO.Compression.GZipStream(fd, IO.Compression.CompressionMode.Compress)
        Try
            Dim buffer(1024) As Byte
            Dim nRead As Integer

            nRead = fs.Read(buffer, 0, buffer.Length)
            While nRead > 0
                csStrim.Write(buffer, 0, nRead)
                nRead = fs.Read(buffer, 0, buffer.Length)
            End While
        Catch ex As Exception
            Throw ex
        Finally
            csStrim.Close()
            fs.Close()
            fd.Close()
        End Try
    End Sub

    Private Sub mkRecFile(ByVal tbBaseName As String)
        SaveRecent(connStrB.DataSource, connStrB.DataSource, fcDbBacPath, connStrB.InitialCatalog, tbBaseName)
        makePFile(connStrB.Password, connStrB.Password)
    End Sub
    Private Sub makePFile(ByVal p1 As String, ByVal p2 As String)
        Dim xml = _
            <p>
                <p1></p1>
                <p2></p2>
            </p>
        xml.<p1>.Value = p1
        xml.<p2>.Value = p2
        xml.Save(Path.Combine(fcDbBacPath, "p.xml"))

    End Sub


    Public Function import(ByVal tbBaseName As String) As Boolean
        mkRecFile(tbBaseName)
        Dim util As System.Diagnostics.Process = Process.Start(Path.Combine(fcDbBacPath, "CopyImportUtil.exe"), "/silent")

        util.WaitForExit(60 * 60 * 1000) 'wait for 1h
        If Not util.HasExited Then
            util.Kill()
        End If
    End Function

    Public Sub makeBak(ByVal pathToBak As String)
        Using cnnTemp As New SqlConnection(connString)
            cnnTemp.Open()
            If True Then
                ServerInfo.BackupDataBase(cnnTemp, connStrB.InitialCatalog, pathToBak)
            Else

            End If

        End Using
    End Sub

#Region "Список последних значений в папке временных файлов"

    <Serializable()> _
    Public Class RecentItem
        Public ItemTime As Date
        Public ItemText As String

        Public Sub New()
        End Sub

        Public Sub New(ByVal Text As String)
            ItemTime = Now
            ItemText = Text.Trim
        End Sub
    End Class

    <Serializable()> _
    Public Class RecentStruct
        Public SourceServer As New List(Of RecentItem)
        Public DestServer As New List(Of RecentItem)
        Public SourceDB As New List(Of RecentItem)
        Public SourceDBFolder As New RecentItem
        Public DestDB As New List(Of RecentItem)
        Public BakFile As New List(Of RecentItem)
        Public BakFileOption As Boolean
        Public DestID As Integer
    End Class

    Private _Recent As New RecentStruct
    Private _Recent_Path As String

    Private Sub LoadRecent(ByRef cboSourceServer As String _
        , ByRef cboDestServer As String _
        , ByRef txtSourceDBFolder As String _
        , ByRef cboSourceDB As String _
        , ByRef cboDestDB As String _
)

        If Not File.Exists(_Recent_Path) Then
            Exit Sub
        End If

        Using reader As New StreamReader(_Recent_Path)
            Dim xs As New System.Xml.Serialization.XmlSerializer(GetType(RecentStruct))
            _Recent = xs.Deserialize(reader)
        End Using

        LoadRecentCombo(_Recent.SourceServer, cboSourceServer)
        LoadRecentCombo(_Recent.DestServer, cboDestServer)
        LoadRecentTxt(_Recent.SourceDBFolder, txtSourceDBFolder)
        LoadRecentCombo(_Recent.SourceDB, cboSourceDB)
        LoadRecentCombo(_Recent.DestDB, cboDestDB)
        'LoadRecentCombo(_Recent.BakFile, cboFile)

        'chkFile.Checked = _Recent.BakFileOption
        'Select Case _Recent.DestID
        '    Case 1
        '        radAF.Checked = True
        '    Case 2
        '        radCFC.Checked = True
        '    Case 3
        '        radFC.Checked = True
        'End Select
    End Sub

    Private Sub LoadRecentCombo(ByVal rList As List(Of RecentItem), ByRef rCombo As String)
        For Each i In rList.OrderByDescending(Function(F) F.ItemTime)
            rCombo = i.ItemText
            Exit For
        Next
    End Sub
    Private Sub LoadRecentTxt(ByVal rI As RecentItem, ByRef rTxt As String)
        rTxt = rI.ItemText
    End Sub

    Private Sub SaveRecent(ByRef cboSourceServer As String _
        , ByRef cboDestServer As String _
        , ByRef txtSourceDBFolder As String _
        , ByRef cboSourceDB As String _
        , ByRef cboDestDB As String _
        )
        SaveRecentCombo(_Recent.SourceServer, cboSourceServer)
        SaveRecentCombo(_Recent.DestServer, cboDestServer)
        SaveRecentTxt(_Recent.SourceDBFolder, txtSourceDBFolder)
        SaveRecentCombo(_Recent.SourceDB, cboSourceDB)
        SaveRecentCombo(_Recent.DestDB, cboDestDB)

        'If chkFile.Checked Then
        '    SaveRecentCombo(_Recent.BakFile, cboFile)
        'End If

        '_Recent.BakFileOption = chkFile.Checked
        'If radAF.Checked Then
        '    _Recent.DestID = 1
        'ElseIf radCFC.Checked Then
        '    _Recent.DestID = 2
        'ElseIf radFC.Checked Then
        '    _Recent.DestID = 3
        'End If

        _Recent.DestID = 3

        Using writer As New StreamWriter(_Recent_Path)
            Dim xs As New System.Xml.Serialization.XmlSerializer(GetType(RecentStruct))
            xs.Serialize(writer, _Recent)
        End Using
    End Sub

    Private Sub SaveRecentCombo(ByVal rList As List(Of RecentItem), ByVal rCombo As String)
        If rCombo.Trim <> "" Then
            Dim i = rList.SingleOrDefault(Function(F) F.ItemText = rCombo.Trim)
            If i Is Nothing Then
                rList.Add(New RecentItem(rCombo.Trim))
            Else
                i.ItemTime = Now
            End If
        End If
    End Sub

    Private Sub SaveRecentTxt(ByVal rI As RecentItem, ByVal rTxt As String)
        rI.ItemText = rTxt
    End Sub

#End Region

End Class


