﻿Imports System.Web
Imports System.Runtime.CompilerServices
Imports System.Text
Imports System.Xml
Imports System.IO

Partial Public Class GR_DOC_IMAGE
    Inherits BaseDoc
    Const STORAGE_GUID = "84E12153-532F-46AD-A0B7-0541450978E5"
    
    Public gg As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        For Each r In GR_vDOC_IMAGE.Rows
            Dim imgPath = GetStorgePath() + "\" + DirectCast(r.FindControl("NAME"), TextBox).Text
            Dim imgPath_1 = Mid(imgPath, InStrRev(imgPath, "\"))
            imgPath = Replace(imgPath, imgPath_1, "\preview" + imgPath_1)
            'только _lod3
            imgPath = Replace(imgPath, ".jpg", "_lod3.jpg")
            imgPath = Replace(imgPath, ".jpeg", "_lod3.jpeg")
            Dim img = DirectCast(r.FindControl("IMF"), Image)
            If File.Exists(imgPath) Then
                img.Attributes("src") = "ImageHandler.ashx?FilePath=" & imgPath
            End If
        Next

    End Sub

    Public Sub CalcNumbers()
        'пересчет номеров 
        Try
           
            Me.SaveDoc()
            'перезаписываем по одной (?)
            For Each r In GR_vDOC_IMAGE.Rows

                Dim Name = DirectCast(r.FindControl("NAME"), TextBox).Text

                Dim cmdIns As New SqlClient.SqlCommand("UPDATE [GR_tblDOC_IMAGE] SET [RowID]=@NUMBER,[NUMBER] = @NUMBER,[CHAR_NUMBER]=@CHAR_NUMBER WHERE DocID=@DocID AND NAME=@Name")

                cmdIns.Parameters.AddWithValue("@DocID", DocID.ToString)
                cmdIns.Parameters.AddWithValue("@ID", ID.ToString)

                Dim sNumber = GetFileName(Name)
                Dim RowID = GetFileNameNumber(sNumber)
                Dim CharNumber = GetFileNameCharNumber(sNumber)
                '  ParseFirstIntegerPartString(test)
                cmdIns.Parameters.AddWithValue("@NUMBER", RowID) 'надо вычислить в зависимости от имени файла
                cmdIns.Parameters.AddWithValue("@CHAR_NUMBER", CharNumber)
                cmdIns.Parameters.AddWithValue("@RowID", RowID) 'надо вычислить в зависимости от имени файла
                cmdIns.Parameters.AddWithValue("@Name", Name)

                AppSettings.ExecCommand(cmdIns)
            Next
            'обновить спецификацию

            Me.LoadDoc(DocID) 'загрузили с новой сортировкой

            'а теперь сделаем update -проставим номер равным RowID пересчитаем в зависимости от сортировки
            '' ''и перегрузим
            Dim i = 1
            For Each r In GR_vDOC_IMAGE.Rows

                Dim Name = DirectCast(r.FindControl("NAME"), TextBox).Text
                Dim cmdUpdate As New SqlClient.SqlCommand("UPDATE [GR_tblDOC_IMAGE] SET [NUMBER] =@i WHERE DocID=@DocID AND NAME=@Name")
                cmdUpdate.Parameters.AddWithValue("@DocID", DocID.ToString)
                cmdUpdate.Parameters.AddWithValue("@Name", Name)
                cmdUpdate.Parameters.AddWithValue("@i", i)
                AppSettings.ExecCommand(cmdUpdate)
                i = i + 1
            Next


            Me.LoadDoc(DocID)
        Catch ex As Exception

            AddMessage(ex.Message)

        End Try



    End Sub

    Public Overrides Function SaveDoc() As Boolean


        Try
            'проверять бы еще на наличие строк
            'удаляем строки из спецификации напрямую
            Dim cmdDel As New SqlClient.SqlCommand("DELETE FROM GR_tblDOC_IMAGE WHERE DocID=@DocID")
            cmdDel.Parameters.AddWithValue("@DocID", DocID.ToString)
            AppSettings.ExecCommand(cmdDel)

            'перезаписываем по одной (?)
            For Each r In GR_vDOC_IMAGE.Rows
                Dim imgPath = GetStorgePath() + "\" + DirectCast(r.FindControl("NAME"), TextBox).Text
                Dim imgPath_1 = Mid(imgPath, InStrRev(imgPath, "\"))
                imgPath = Replace(imgPath, imgPath_1, "\preview" + imgPath_1)
                'только _lod3
                imgPath = Replace(imgPath, ".jpg", "_lod3.jpg")
                imgPath = Replace(imgPath, ".jpeg", "_lod3.jpeg")
                Dim img = DirectCast(r.FindControl("IMF"), Image)
                img.Attributes("src") = "ImageHandler.ashx?FilePath=" & imgPath

                Dim Name = DirectCast(r.FindControl("NAME"), TextBox).Text

                'и заново записываем

                Dim IsPublic As Integer
                If DirectCast(r.FindControl("PUBLIC"), CheckBox).Checked = True Then
                    IsPublic = 1
                Else
                    IsPublic = 0

                End If
                Dim ID = Guid.NewGuid()
                Dim CreationDateTime = Now

                Dim cmdIns As New SqlClient.SqlCommand("INSERT INTO [GR_tblDOC_IMAGE]([ID],[OwnerID],[CreationDateTime],[DocID],[RowID],[NAME],[NUMBER],[CHAR_NUMBER],[PUBLIC]) VALUES(@ID, N'12345678-9012-3456-7890-123456789012',@CreationDateTime,@DocID,@RowID,@NAME,@NUMBER,@CHAR_NUMBER,@PUBLIC)")
                Dim Number = DirectCast(r.FindControl("NUMBER"), TextBox).Text
                Dim CharNumber = DirectCast(r.FindControl("CHAR_NUMBER"), TextBox).Text
                cmdIns.Parameters.AddWithValue("@DocID", DocID.ToString)
                cmdIns.Parameters.AddWithValue("@ID", ID.ToString)
                cmdIns.Parameters.AddWithValue("@CreationDateTime", CreationDateTime)

                'Dim sNumber = GetFileName(Name)
                'Dim RowID = GetFileNameNumber(sNumber)
                'Dim CharNumber = GetFileNameCharNumber(sNumber)
                '  ParseFirstIntegerPartString(test)
                cmdIns.Parameters.AddWithValue("@NUMBER", Number) 'надо вычислить в зависимости от имени файла
                cmdIns.Parameters.AddWithValue("@CHAR_NUMBER", CharNumber)
                cmdIns.Parameters.AddWithValue("@RowID", Number) 'надо вычислить в зависимости от имени файла
                cmdIns.Parameters.AddWithValue("@Name", Name)
                cmdIns.Parameters.AddWithValue("@PUBLIC", IsPublic)
                AppSettings.ExecCommand(cmdIns)
            Next

        Catch ex As Exception

            AddMessage(ex.Message)

        End Try


        Return True
    End Function


 
    Public Function GetFileName(ByVal sFilePath As String) As String

        Try
            Dim sFileName As String
            sFileName = ""
            sFileName = Left(sFilePath, InStrRev(sFilePath, ".") - 1)
            sFileName = Right(sFileName, Len(sFileName) - InStrRev(sFilePath, "\"))

            Return sFileName
        Catch ex As Exception
            ' AddMessage(ex.Message)
            Return ""
        End Try

    End Function

    Public Function GetFileNameNumber(ByVal sFileName As String) As Integer

        Try
            Dim iFileNumber As Integer
            iFileNumber = 9999999
            Dim sFileNumber As String
            sFileNumber = ""
            'берем по одному символу, пока не найдем букву
            If IsNumeric(sFileName) Then
                iFileNumber = CInt(sFileName)
            Else
                For i = 1 To Len(sFileName)
                    Dim symbol = Mid(sFileName, i, 1)
                    If IsNumeric(symbol) Then
                        'цифра
                        sFileNumber = sFileNumber + symbol
                    Else
                        ' ElseIf UCase(symbol) <> symbol Or LCase(symbol) <> symbol Then
                        'символ какой-нибудь
                        iFileNumber = CInt(sFileNumber)
                        If iFileNumber = 0 Then
                            iFileNumber = 9999999
                        End If
                        Return iFileNumber
                    End If
                Next i
            End If
            Return iFileNumber
        Catch ex As Exception
            ' AddMessage(ex.Message)
            Return 9999999
        End Try

    End Function
    Public Function GetFileNameCharNumber(ByVal sFileName As String) As String

        Try
            
            Dim sFileNumber As String
            sFileNumber = ""
            'берем по одному символу, пока не найдем букву
            If IsNumeric(sFileName) Then
                Return ""
            Else
                For i = 1 To Len(sFileName)
                    Dim symbol = Mid(sFileName, i, 1)
                    If IsNumeric(symbol) Then
                        'цифра

                    Else
                        ' ElseIf UCase(symbol) <> symbol Or LCase(symbol) <> symbol Then
                        'символ какой-нибудь
                        sFileNumber = Right(sFileName, Len(sFileName) - i + 1)
                        Return sFileNumber
                    End If
                Next i
            End If
            Return sFileNumber
        Catch ex As Exception
            ' AddMessage(ex.Message)
            Return ""
        End Try

    End Function

 
#Region "File Events"

    Private Sub RefFileUploadFileS_Uploaded(ByVal sender As Object, ByVal e As RefFileArgs) Handles RefFileUploadFileS.Uploaded
        'добавление строки в спецификацию
        Dim DOC_ATTR As String
        DOC_ATTR = CreateStorgePath()
        Dim RowID As Integer
        RowID = CreateRowID(e.FileName)
        If DOC_ATTR IsNot Nothing Then
            Dim row = GR_vDOC_IMAGE.InsertRow(e.ID)
            DataSetSpecificationDataRow(GR_vDOC_IMAGE.ID, e.ID)("CreationDateTime") = e.TransferTime
            DirectCast(row.FindControl("NAME"), TextBox).Text = DOC_ATTR + "\" + System.IO.Path.GetFileName(e.FileName)
            DirectCast(row.FindControl("PUBLIC"), CheckBox).Checked = True
            '  DirectCast(row.FindControl("RowID"), TextBox).Text = RowID.ToString
        Else
            AddMessage("Заполните поля Архив, Номер фонда, Опись и Ед.хр. на вкладке Место хранения")
            e.Cancel = True
        End If

    End Sub

    Private Sub RefFileUploadFileS_Uploading(ByVal sender As Object, ByVal e As RefFileArgs) Handles RefFileUploadFileS.Uploading
        'сохраняем файл 
        'надо блокировать вкладку, пока не будет сохранено место хранения
        Dim DOC_ATTR As String
        DOC_ATTR = CreateStorgePath()
        If DOC_ATTR IsNot Nothing Then

            e.FileName = GetStorgePath() + "\" + DOC_ATTR + "\" + System.IO.Path.GetFileName(e.FileName)

        Else
            AddMessage("Заполните поля Архив, Номер фонда, Опись и Ед.хр. на вкладке Место хранения")
            e.Cancel = True
        End If
    End Sub

    Public Sub RefFileS_Requesting(ByVal sender As Object, ByVal e As RefFileArgs)
        'открываем файл на просмотр
        Try
            Dim FileName As String
            FileName = GetFileImageName(e.ID)
            If FileName IsNot Nothing Then
                e.FileName = FileName
            Else
                AddMessage("Сохраните документ")
            End If

        Catch ex As Exception
            AddMessage(ex.Message)
        End Try
    End Sub
    ''' <summary>


    Private Function GetStorgePath() As String

        'Dirs()

        Dim cmd As New SqlClient.SqlCommand("select Text from tblConstantsSpec where ID = @ID")
        cmd.Parameters.AddWithValue("@ID", New Guid(STORAGE_GUID))
        Dim DataTable = AppSettings.GetDataTable(cmd)
        If DataTable.Rows.Count = 0 Then
            GetStorgePath = "C:\Госреестр-Файлы"
        Else
            GetStorgePath = DataTable.Rows(0)("Text")
        End If
        Return GetStorgePath
    End Function

    Private Function GetArchiveNameShort(ByVal ID_ARCHIVE As Guid) As String
        Dim cmd As New SqlClient.SqlCommand("select A.NAME_SHORT, isnull(S.NAME, '') as ARCH_SUBJECT from  dbo.tblARCHIVE AS A LEFT OUTER JOIN dbo.tblSUBJECT_CL AS S ON A.ISN_SUBJECT = S.ISN_SUBJECT where A.ID = @ID")
        cmd.Parameters.AddWithValue("@ID", ID_ARCHIVE)
        Dim DataTable = AppSettings.GetDataTable(cmd)
        Dim sbj = DataTable.Rows(0)("ARCH_SUBJECT")
        If Len(sbj) = 0 Then

        Else
            sbj = sbj + "\"
        End If

        If DataTable.Rows.Count = 0 Then
            GetArchiveNameShort = "Архив не найден"
        Else
            GetArchiveNameShort = sbj + DataTable.Rows(0)("NAME_SHORT")
        End If
        Return GetArchiveNameShort
    End Function

    Private Function CreateStorgePath() As String
        'опеределяем путь к файлу и сохранем его в БД
        ' Dim ID_ARCHIVE As Guid = Nothing
        ' Dim ID_ARCHIVE As String = Nothing
        Dim ARCHIVE As String = Nothing
        Dim SUBJECT As String = Nothing
        Dim FUND_NUM As String = Nothing
        Dim INVENTORY_NUM As String = Nothing
        Dim UNIT_NUM As String = Nothing
        Dim INVENTORY_ALTERNATIVE As String = Nothing
        'проверка на заполненность обязательных полей номер фонда, номер описи и номер единицы хранения

        FUND_NUM = FUND_NUM_1.Text + FUND_NUM_2.Text + FUND_NUM_3.Text
        INVENTORY_NUM = INVENTORY_NUM_1.Text + INVENTORY_NUM_2.Text
        INVENTORY_ALTERNATIVE = INVENTORY_NUM_ALTERNATIVE.Text

        If Len(INVENTORY_ALTERNATIVE) > 0 Then
            INVENTORY_NUM = INVENTORY_ALTERNATIVE
        Else
            If Len(INVENTORY_NUM_3.Text) > 0 Then
                INVENTORY_NUM = INVENTORY_NUM + "_" + INVENTORY_NUM_3.Text
            End If

        End If

        UNIT_NUM = UNIT_NUM_1.Text + UNIT_NUM_2.Text
        If Len(UNIT_VOL_NUM.Text) > 0 Then
            UNIT_NUM = UNIT_NUM + "_" + UNIT_VOL_NUM.Text
        End If
        If Len(SHEETS.Text) > 0 Then
            UNIT_NUM = UNIT_NUM + "_" + SHEETS.Text
        End If

        '    'надо брать короткое имя архива
        ARCHIVE = GetArchiveNameShort(ID_ARCHIVE.DocID)
        ''SUBJECT = DirectCast(r.FindControl("SUBJECT"), TextBox).Text


        If ID_ARCHIVE.ToString IsNot Nothing And Len(FUND_NUM.ToString) > 0 And Len(INVENTORY_NUM.ToString) > 0 And Len(UNIT_NUM.ToString) > 0 Then
         
            'формируем путь  (удалить все непотребные символы от 1 до 31)
            '  CreateStorgePath = GetStorgePath() + "\" + ARCHIVE + "\" + FUND_NUM + "\" + INVENTORY_NUM + "\" + UNIT_NUM
            CreateStorgePath = ARCHIVE + "\" + FUND_NUM + "\" + INVENTORY_NUM + "\" + UNIT_NUM

        Else
            CreateStorgePath = Nothing
        End If

        Return CreateStorgePath
    End Function

    Private Function CreateRowID(ByVal FileName As String) As Integer
        Dim RowID As Integer = 0
        Return RowID
    End Function
    Public Function GetFileImageName(ByVal ID As Guid) As String

        'в поле Name храниться весь путь
        Dim cmd As New SqlClient.SqlCommand("select NAME from GR_tblDOC_IMAGE where ID = @ID")
        cmd.Parameters.AddWithValue("@ID", ID)
        Dim DataTable = AppSettings.GetDataTable(cmd)
        If DataTable.Rows.Count = 0 Then
            GetFileImageName = Nothing
        Else
            GetFileImageName = GetStorgePath() + "\" + DataTable.Rows(0)("NAME")
        End If

    End Function
  
    Sub Dirs()
        Dim myName$, myPath$, N%, Folders$()

        myPath = InputBox("Введите директорию", , "c:\temp\")
        If Right(myPath, 1) <> "\" Then myPath = myPath & "\"
        If Dir(Left(myPath, Len(myPath) - 1), 16) = "" Then MsgBox("Папка не существует!") : Exit Sub
        myName = Dir(myPath, vbDirectory)
        Do While myName <> ""
            If GetAttr(myPath & myName) = vbDirectory And myName <> "." Then
                N = N + 1
                ReDim Preserve Folders$(0 To N)
                Folders(N) = myPath & myName
            End If
            myName = Dir()
        Loop
        MsgBox(Join(Folders, vbLf))
    End Sub
#End Region

    'Protected Overrides Sub Finalize()
    '    MyBase.Finalize()
    'End Sub
End Class