﻿'------------------------------------------------------------------------------
' <auto-generated>
'     Этот код создан программой.
'     Исполняемая версия:2.0.50727.5485
'
'     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
'     повторной генерации кода.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On



Partial Public Class ARCHIVE

    '''<summary>
    '''Crumbs элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents Crumbs As Global.WebApplication.WebUI.ArchiveCrumbs

    '''<summary>
    '''HiddenCrumbs элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents HiddenCrumbs As Global.WebApplication.WebUI.ArchiveCrumbs

    '''<summary>
    '''HyperLinkFUNDs элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents HyperLinkFUNDs As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''HyperLinkSTRs элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents HyperLinkSTRs As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''CODE элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents CODE As Global.Equipage.WebUI.eqTextBox

    '''<summary>
    '''ARCHIVE_LEVEL элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents ARCHIVE_LEVEL As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ARCHIVE_TYPE элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents ARCHIVE_TYPE As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''NAME_SHORT элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents NAME_SHORT As Global.Equipage.WebUI.eqTextBox

    '''<summary>
    '''NAME элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents NAME As Global.Equipage.WebUI.eqTextBox

    '''<summary>
    '''ISN_SUBJECT элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents ISN_SUBJECT As Global.WebApplication.WebUI.eqDocument

    '''<summary>
    '''tblARCHIVE_ADDRESS элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents tblARCHIVE_ADDRESS As Global.Equipage.WebUI.eqSpecification

    '''<summary>
    '''AUTHORITY элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents AUTHORITY As Global.Equipage.WebUI.eqTextBox

    '''<summary>
    '''vREF_FILE_ARCHIVE элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents vREF_FILE_ARCHIVE As Global.Equipage.WebUI.eqSpecification

    '''<summary>
    '''RefFileUploadFile элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents RefFileUploadFile As Global.WebApplication.RefFileUpload

    '''<summary>
    '''vARCHIVE_PASSPORT_Spec элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents vARCHIVE_PASSPORT_Spec As Global.Equipage.WebUI.eqSpecification

    '''<summary>
    '''HyperLinkPassportAdd элемент управления.
    '''</summary>
    '''<remarks>
    '''Автоматически создаваемое поле.
    '''Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
    '''</remarks>
    Protected WithEvents HyperLinkPassportAdd As Global.System.Web.UI.WebControls.HyperLink
End Class
