﻿Imports System.Data
Imports System.Data.SqlClient

Partial Public Class INVENTORY
    Inherits BaseDoc

#Region "События"

    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        RegisterSpecificationAsInline(tblINVENTORY_CHECK)
    End Sub

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        For Each r In vREF_ACT_FOR_INVENTORY.Rows
            DirectCast(r.FindControl("ISN_ACT"), eqDocument).OptionalQueryString = "&INVENTORY=" & DocID.ToString & "&ACT_OBJ=702"
        Next

        If Not IsPostBack Then
            If DocStateInfo.StateID = Guid.Empty Then
                Dim FundSQL = <sql>
                    select tblFUND.*
                        ,(select top 1 RS.ISN_RECEIPT_SOURCE from tblFUND_RECEIPT_SOURCE RS where RS.ISN_FUND = tblFUND.ISN_FUND and RS.RowID = 0) RS_ISN
                        ,(select top 1 RR.ISN_RECEIPT_REASON from tblFUND_RECEIPT_REASON RR where RR.ISN_FUND = tblFUND.ISN_FUND and RR.RowID = 0) RR_ISN
                    from tblFUND
                    where tblFUND.ID = @ID
                              </sql>
                Dim FundCommand As New SqlCommand(FundSQL.Value)
                FundCommand.Parameters.AddWithValue("@ID", Crumbs.ISN_FUND.DocID)
                Dim FundDataTable = AppSettings.GetDataTable(FundCommand)

                If FundDataTable.Rows.Count > 0 Then
                    ISN_INVENTORY_TYPE.SelectedValue = FundDataTable.Rows(0)("ISN_DOC_TYPE").ToString
                    PRESENCE_FLAG.SelectedValue = FundDataTable.Rows(0)("PRESENCE_FLAG").ToString
                    ABSENCE_REASON.SelectedValue = FundDataTable.Rows(0)("ABSENCE_REASON").ToString
                    INVENTORY_KIND.SelectedValue = FundDataTable.Rows(0)("FUND_CATEGORY").ToString

                    Select Case FundDataTable.Rows(0)("ISN_SECURLEVEL").ToString
                        Case "1", "2" ' откр, с
                            ISN_SECURLEVEL.SelectedValue = FundDataTable.Rows(0)("ISN_SECURLEVEL")
                    End Select

                    If FundDataTable.Rows(0)("PRESENCE_FLAG").ToString = "b" Then
                        MovePanel.Enabled = False
                    End If

                    If Not IsDBNull(FundDataTable.Rows(0)("RS_ISN")) Then
                        ISN_RECEIPT_SOURCE.KeyValue = FundDataTable.Rows(0)("RS_ISN")
                    End If

                    If Not IsDBNull(FundDataTable.Rows(0)("RR_ISN")) Then
                        ISN_RECEIPT_REASON.KeyValue = FundDataTable.Rows(0)("RR_ISN")
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        HyperLinkUNITs.Enabled = DocStateInfo.StateID <> Guid.Empty
        HyperLinkUNITs.NavigateUrl = BasePage.GetListDocURL("UNIT.ascx", False, "INVENTORY=" & DocID.ToString)
        HyperLinkUNIT2s.Enabled = DocStateInfo.StateID <> Guid.Empty
        HyperLinkUNIT2s.NavigateUrl = BasePage.GetListDocURL("UNIT2.ascx", False, "INVENTORY=" & DocID.ToString)
        HyperLinkSTRs.Enabled = DocStateInfo.StateID <> Guid.Empty
        HyperLinkSTRs.NavigateUrl = BasePage.GetListDocURL("INVENTORYSTRUCTURE.ascx", False, "INVENTORY=" & DocID.ToString)

        If Crumbs.ISN_FUND.DocID <> Guid.Empty AndAlso Not IsDBNull(Crumbs.ISN_FUND.DataSetHeaderDataRow("ISN_ARCHIVE")) Then
            Crumbs.ISN_ARCHIVE.KeyValue = Crumbs.ISN_FUND.DataSetHeaderDataRow("ISN_ARCHIVE")
        End If

        MultiViewTabs.ActiveViewIndex = CInt(MenuTabs.SelectedValue)

        SECURITY_CHAR.Enabled = GetLock(LockID) AndAlso ISN_SECURLEVEL.SelectedValue <> "" AndAlso ISN_SECURLEVEL.SelectedValue Mod 10000000000 = "1"
        If Not (ISN_SECURLEVEL.SelectedValue <> "" AndAlso ISN_SECURLEVEL.SelectedValue Mod 10000000000 = "1") Then
            SECURITY_CHAR.SelectedValue = ""
        End If
        ISN_SECURITY_REASON.Enabled = GetLock(LockID) AndAlso ISN_SECURLEVEL.SelectedValue <> "" AndAlso ISN_SECURLEVEL.SelectedValue Mod 10000000000 = "1" And SECURITY_CHAR.SelectedValue = "p"
        If Not ((ISN_SECURLEVEL.SelectedValue <> "" AndAlso ISN_SECURLEVEL.SelectedValue Mod 10000000000 = "1") And SECURITY_CHAR.SelectedValue = "p") Then
            ISN_SECURITY_REASON.SelectedValue = ""
        End If

        Panel_E.Enabled = GetLock(LockID) And Not (CARRIER_TYPE.SelectedValue = "T" And Not HAS_ELECTRONIC_DOCS.Checked)
        Panel_T.Enabled = GetLock(LockID) And Not (CARRIER_TYPE.SelectedValue = "E" And Not HAS_TRADITIONAL_DOCS.Checked)


        StatsDisable(vINVENTORY_DOCUMENT_STATS_P, _
             vINVENTORY_DOCUMENT_STATS_A, _
             vINVENTORY_DOCUMENT_STATS_E, _
             vINVENTORY_DOCUMENT_STATS_M, _
             vINVENTORY_DOCUMENT_STATS, _
             tblINVENTORY_DOC_TYPE.Rows.Select(Function(F) DirectCast(F.FindControl("ISN_DOC_TYPE"), DropDownList).SelectedItem.Text).Where(Function(F) Not String.IsNullOrEmpty(F)).ToArray)


        vREF_ACT_FOR_INVENTORY.Enabled = DocStateInfo.StateID <> Guid.Empty

        ScriptManager.RegisterStartupScript(Page, Me.GetType, "JS", "_updateUI();", True)
    End Sub

    Private Sub tblINVENTORY_DOC_TYPE_RowInserted(ByVal SpecificationRow As Equipage.WebUI.eqSpecificationRow) Handles tblINVENTORY_DOC_TYPE.RowInserted
        If DocID = Guid.Empty Then

            '<Roman> 26.08.2013 фильтрует список типов документов, оставляя только те, которые указаны в родительском фонде
            Dim lac As ListItemCollection = DirectCast(SpecificationRow.FindControl("ISN_DOC_TYPE"), DropDownList).Items
            Dim fundId As String = Request.QueryString("FUND")
            Dim cmdString As String = <sql>
                                        SELECT ISN_DOC_TYPE 
                                        FROM tblDOC_TYPE_CL 
                                        WHERE deleted=0
                                        AND ISN_DOC_TYPE NOT IN (
	                                        SELECT ISN_DOC_TYPE
	                                        FROM tblFUND_DOC_TYPE
	                                        WHERE 
	                                        ISN_FUND = (
		                                        SELECT ISN_FUND
		                                        FROM tblFUND
		                                        WHERE ID=@ID
	                                        )
                                        )
                                      </sql>
            Dim cmd As New SqlCommand(cmdString)
            cmd.Parameters.AddWithValue("@ID", fundId)
            For Each row As DataRow In AppSettings.GetDataTable(cmd).Rows
                lac.Remove(lac.FindByValue(row.Item(0).ToString))
            Next
            '<\Roman>


            DirectCast(SpecificationRow.FindControl("ISN_DOC_TYPE"), DropDownList).SelectedValue = ISN_INVENTORY_TYPE.SelectedValue
        End If
    End Sub

    Private Sub ISN_INVENTORY_TYPE_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ISN_INVENTORY_TYPE.TextChanged
        Select Case ISN_INVENTORY_TYPE.Text
            Case 1, 2, 3
                CARRIER_TYPE.SelectedValue = "T"
        End Select
    End Sub

#End Region

#Region "Методы"

    Public Sub CalcOneInventory()
        If GetLock(LockID) AndAlso Not LockCommand("Читатели") AndAlso Not ForbidRecalc.Checked Then
            BasePage.OpenModalDialogWindow(Me, "RecalcInventory.aspx?DocTypeURL=" & DocTypeURL, "One", DataSetHeaderDataRow("ID"))
        End If
    End Sub

    Public Sub AddAct()
        If DocID <> Guid.Empty Then
            BasePage.OpenModalDialogWindow(Me, BasePage.GetNewDocURL("ACT.ascx", True), "INVENTORY.ascx", DataSetHeaderDataRow("ISN_INVENTORY"))
        End If
    End Sub

#End Region

#Region "Документ"

    Public Overrides Function NewDoc() As Boolean
        NewDoc = MyBase.NewDoc()
        If NewDoc Then
            If Request.QueryString("FUND") IsNot Nothing Then
                Crumbs.ISN_FUND.DocID = New Guid(Request.QueryString("FUND"))
                DataSetHeaderDataRow("ISN_FUND") = Crumbs.ISN_FUND.KeyValue
                PutValueFromDataSetToHeaderControl(ISN_SECURLEVEL)
            End If
        End If
    End Function

    Public Overrides Function SaveDoc() As Boolean
        If tblINVENTORY_DOC_TYPE.Rows.Count = 0 Then
            AddMessage("Поле: Тип докуметов -> Обязательно для заполнения")
            Return False
        End If

        If ISN_INVENTORY_TYPE.SelectedValue = "1" Then
            If ISN_SECURITY_REASON.SelectedValue = "1" Then
                AddMessage("Причина ограничения доступа не может быть 'Тайна л/ж', если Тип описи 'Управленческая документация'")
                Return False
            End If
        End If

        If ISN_INVENTORY_TYPE.SelectedValue = "2" Then
            If ISN_SECURITY_REASON.SelectedValue = "1" Then
                AddMessage("Причина ограничения доступа не может быть 'Тайна л/ж', если Тип описи 'Документы по личному составу'")
                Return False
            End If
        End If

        If ISN_SECURLEVEL.SelectedValue <> "" AndAlso ISN_SECURLEVEL.SelectedValue Mod 10000000000 = "1" Then
            If SECURITY_CHAR.SelectedValue = "" Then
                SetControlAlertStyle(DocType.GetFieldInfo(DocType.GetHeaderTableName, SECURITY_CHAR.ID), True)
                AddMessage("Поле: Доступ -> Обязательно для заполнения")
                Return False
            End If
        End If

        Dim NumCheckSQL = <sql>
                  Select *
                    From tblINVENTORY I left join tblFUND F on I.ISN_FUND = F.ISN_FUND
                    Where I.ID != @ID
                    And (I.INVENTORY_NUM_1 = @NUM1 or ((I.INVENTORY_NUM_1 IS NULL or I.INVENTORY_NUM_1='0' or I.INVENTORY_NUM_1='') AND (@NUM1='' or @NUM1='0')))
                    And (I.INVENTORY_NUM_2 = @NUM2 or (I.INVENTORY_NUM_2 IS NULL AND @NUM2=''))
                    And (@NUM3 = isnull(cast(I.INVENTORY_NUM_3 as varchar(10)), '') or ((I.INVENTORY_NUM_3 IS NULL or I.INVENTORY_NUM_3='0' or I.INVENTORY_NUM_3='') AND (@NUM3='' or @NUM3='0')))
                    And F.ISN_FUND = @ISN_FUND
                    And I.Deleted = 0
              </sql>
        Dim NumCheckCommand As New SqlCommand(NumCheckSQL.Value)
        NumCheckCommand.Parameters.AddWithValue("@ID", DocID)
        NumCheckCommand.Parameters.AddWithValue("@NUM1", INVENTORY_NUM_1.Text)
        NumCheckCommand.Parameters.AddWithValue("@NUM2", INVENTORY_NUM_2.Text)
        NumCheckCommand.Parameters.AddWithValue("@NUM3", INVENTORY_NUM_3.Text)
        NumCheckCommand.Parameters.AddWithValue("@ISN_FUND", Crumbs.ISN_FUND.KeyValue)
        Dim NumCheck = AppSettings.GetDataTable(NumCheckCommand)
        If NumCheck.Rows.Count <> 0 Then
            AddMessage("Опись с таким номером уже существует")
            Return False
        End If

        SaveDoc = MyBase.SaveDoc()
        If SaveDoc Then
            If PRESENCE_FLAG.SelectedValue = "b" Then
                Dim IsLostSQL = <sql>
                                    update tblUNIT set IS_LOST = 'Y'
                                    where ISN_INVENTORY = @ISN_INVENTORY and Deleted = 0
                                </sql>
                Dim IsLostCommand As New SqlCommand(IsLostSQL.Value) With {.CommandTimeout = 3600}
                IsLostCommand.Parameters.AddWithValue("@ISN_INVENTORY", DataSetHeaderDataRow("ISN_INVENTORY"))
                Dim r = AppSettings.ExecCommand(IsLostCommand)
            End If
        End If
    End Function

    Public Overrides Function DeleteDoc() As Boolean
        Dim DeleteRecursivelyCommand As New SqlCommand("dbo.DeleteRecursively") With {.CommandType = CommandType.StoredProcedure}
        DeleteRecursivelyCommand.Parameters.AddWithValue("@ISN", DataSetHeaderDataRow("ISN_INVENTORY"))
        DeleteRecursivelyCommand.Parameters.AddWithValue("@OBJ", "inventory")
        AppSettings.ExecCommand(DeleteRecursivelyCommand)

        setDocStatesLogDeletedRecursively("Удален")

        Return MyBase.DeleteDoc()
    End Function

#End Region

#Region "Контролы"

    Public Overrides Sub PutValueToControl(ByVal Value As Object, ByVal TargetControl As System.Web.UI.Control, Optional ByVal FieldFormat As String = "")
        If TypeOf TargetControl Is CheckBox Then
            DirectCast(TargetControl, CheckBox).Checked = (Value Is Nothing OrElse Value.ToString.Trim.ToUpper = "Y")
        Else
            MyBase.PutValueToControl(Value, TargetControl, FieldFormat)
        End If
    End Sub

    Public Overrides Function GetValueFromControl(ByVal SourceControl As System.Web.UI.Control) As Object
        If TypeOf SourceControl Is CheckBox Then
            Return IIf(DirectCast(SourceControl, CheckBox).Checked, "Y", "N")
        Else
            Return MyBase.GetValueFromControl(SourceControl)
        End If
    End Function

#End Region

#Region "Файлы"

    Private Sub RefFileUploadFile_Uploaded(ByVal sender As Object, ByVal e As RefFileArgs) Handles RefFileUploadFile.Uploaded
        Dim row = vREF_FILE_INVENTORY.InsertRow(e.ID)
        DataSetSpecificationDataRow(vREF_FILE_INVENTORY.ID, e.ID)("CreationDateTime") = e.TransferTime
        DirectCast(row.FindControl("NAME"), TextBox).Text = System.IO.Path.GetFileName(e.FileName)
    End Sub

    Private Sub RefFileUploadFile_Uploading(ByVal sender As Object, ByVal e As RefFileArgs) Handles RefFileUploadFile.Uploading
        e.FileName = GetFileSystemAttachFileName(e.ID, e.TransferTime, 702, DBNull.Value, e.FileName)
    End Sub

    Public Sub RefFile_Requesting(ByVal sender As Object, ByVal e As RefFileArgs)
        e.TransferTime = DataSetSpecificationDataRow(vREF_FILE_INVENTORY.ID, e.ID)("CreationDateTime")
        e.FileName = GetFileSystemAttachFileName(e.ID, e.TransferTime, 702, DBNull.Value)
    End Sub

    Private Sub RefFileUploadFileS_Uploaded(ByVal sender As Object, ByVal e As RefFileArgs) Handles RefFileUploadFileS.Uploaded
        Dim row = vREF_FILE_INVENTORY_S.InsertRow(e.ID)
        DataSetSpecificationDataRow(vREF_FILE_INVENTORY_S.ID, e.ID)("CreationDateTime") = e.TransferTime
        DirectCast(row.FindControl("NAME"), TextBox).Text = System.IO.Path.GetFileName(e.FileName)
    End Sub

    Private Sub RefFileUploadFileS_Uploading(ByVal sender As Object, ByVal e As RefFileArgs) Handles RefFileUploadFileS.Uploading
        e.FileName = GetFileSystemAttachFileName(e.ID, e.TransferTime, 702, 100, e.FileName)
    End Sub

    Public Sub RefFileS_Requesting(ByVal sender As Object, ByVal e As RefFileArgs)
        e.TransferTime = DataSetSpecificationDataRow(vREF_FILE_INVENTORY_S.ID, e.ID)("CreationDateTime")
        e.FileName = GetFileSystemAttachFileName(e.ID, e.TransferTime, 702, 100)
    End Sub

#End Region

End Class