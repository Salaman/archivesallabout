﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="RoleCard.ascx.vb" Inherits="WebApplication.RoleCard" %>
<%@ Register Src="../ListDocFunction.ascx" TagName="ListDocFunction" TagPrefix="ldf" %>
<eq:eqCollapsiblePanel runat="server" Collapsed="true">
    <HeaderPanel>
        Название, Описание, Стартовая
        <asp:CheckBox ID="BUILD_IN_ROLE" runat="server" Text="Встроенная" Enabled="false"
            Visible="false" />
    </HeaderPanel>
    <ContentPanel>
        <table width="100%" class="LogicBlockTable">
            <colgroup>
                <col style="width: 120px;" />
                <col />
            </colgroup>
            <tr>
                <td>
                    Название
                </td>
                <td>
                    <eq:eqTextBox ID="RoleName" runat="server" Width="100%" />
                </td>
            </tr>
            <tr>
                <td>
                    Описание
                </td>
                <td>
                    <eq:eqTextBox ID="Description" runat="server" Width="100%" TextMode="MultiLine" Rows="5" />
                </td>
            </tr>
            <tr>
                <td>
                    Стартовая страница
                </td>
                <td>
                    <eq:eqTextBox ID="StartPage" runat="server" Width="100%" />
                </td>
            </tr>
        </table>
    </ContentPanel>
</eq:eqCollapsiblePanel>
<eq:eqCollapsiblePanel runat="server" Collapsed="true">
    <HeaderPanel>
        Предоставление доступа к документам системы
    </HeaderPanel>
    <ContentPanel>
        <asp:UpdatePanel ID="upeqRolesDocumentsAccess" runat="server" UpdateMode="Conditional"
            RenderMode="Inline">
            <ContentTemplate>
                <eq:eqSpecification ID="eqRolesDocumentsAccess" runat="server" Width="100%">
                    <eq:eqSpecificationColumn HeaderText="Тип документа">
                        <ItemTemplate>
                            <eq:eqDropDownList ID="DocTypeID" UniqueGroupName="RDADTID" runat="server" Width="100%" />
                        </ItemTemplate>
                    </eq:eqSpecificationColumn>
                    <eq:eqSpecificationColumn HeaderText="В меню" Width="50px">
                        <ItemTemplate>
                            <asp:CheckBox ID="Menu" runat="server" />
                        </ItemTemplate>
                    </eq:eqSpecificationColumn>
                    <eq:eqSpecificationColumn HeaderText="Список" Width="50px">
                        <ItemTemplate>
                            <asp:CheckBox ID="List" runat="server" />
                        </ItemTemplate>
                    </eq:eqSpecificationColumn>
                    <eq:eqSpecificationColumn HeaderText="Открыть" Width="50px">
                        <ItemTemplate>
                            <asp:CheckBox ID="Open" runat="server" />
                        </ItemTemplate>
                    </eq:eqSpecificationColumn>
                    <eq:eqSpecificationColumn HeaderText="Создать" Width="50px">
                        <ItemTemplate>
                            <asp:CheckBox ID="New" runat="server" />
                        </ItemTemplate>
                    </eq:eqSpecificationColumn>
                    <eq:eqSpecificationColumn Width="70px">
                        <HeaderTemplate>
                            <div style="color: Red;">
                                <%=IIf(AppSettings.RolesAllowPriority, "Запретить", "Разрешить")%></div>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="Priority" runat="server" />
                        </ItemTemplate>
                    </eq:eqSpecificationColumn>
                </eq:eqSpecification>
            </ContentTemplate>
        </asp:UpdatePanel>
    </ContentPanel>
</eq:eqCollapsiblePanel>
    <table class="LogicBlockCaption" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td align="right" style="width: 300px;">
                Ограничение доступа к документу:&nbsp;
            </td>
            <td>
                <div>
                    <eq:eqDropDownList ID="RolesDocTypeID" runat="server" AutoPostBack="True" Width="100%" />
                </div>
            </td>
        </tr>
    </table>
<aj:TabContainer runat="server" ID="TabContainerRestrictions" ToolTip="Панель ограничений">
    <aj:TabPanel runat="server" ID="DocButtons" HeaderText="Кнопки документа">
        <ContentTemplate>
            <div class="LogicBlockCaption">
                Ограничение доступа к кнопкам меню документа
            </div>
            <asp:UpdatePanel ID="upeqRolesToolBarDenied" runat="server" UpdateMode="Conditional"
                RenderMode="Inline">
                <ContentTemplate>
                    <eq:eqSpecification ID="eqRolesToolBarDenied" runat="server" Width="100%" OnRowInserted="FilterSpecification_RowInserted">
                        <eq:eqSpecificationColumn HeaderText="Тип документа">
                            <ItemTemplate>
                                <eq:eqDropDownList ID="DocTypeID" runat="server" Width="100%" OnSelectedIndexChanged="RefreshSpecificationRow"
                                    AutoPostBack="True" />
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                        <eq:eqSpecificationColumn HeaderText="Кнопка">
                            <ItemTemplate>
                                <eq:eqDropDownList ID="IDButton" runat="server" Width="100%" />
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                        <eq:eqSpecificationColumn HeaderText="Статус">
                            <ItemTemplate>
                                <eq:eqDropDownList ID="StatusID" runat="server" Width="100%" />
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                        <eq:eqSpecificationColumn Width="70px">
                            <HeaderTemplate>
                                <div style="color: Red;">
                                    <%=IIf(AppSettings.RolesAllowPriority, "Приоритет", "Разрешить")%></div>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="Priority" runat="server" />
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                    </eq:eqSpecification>
                </ContentTemplate>
            </asp:UpdatePanel>
        </ContentTemplate>
    </aj:TabPanel>
    <aj:TabPanel runat="server" HeaderText="Кнопки списка">
        <ContentTemplate>
            <div class="LogicBlockCaption">
                Ограничение доступа к кнопкам меню в списке документов
            </div>
            <asp:UpdatePanel ID="upeqRolesToolBarDeniedList" runat="server" UpdateMode="Conditional"
                RenderMode="Inline">
                <ContentTemplate>
                    <eq:eqSpecification ID="eqRolesToolBarDeniedList" runat="server" Width="100%" OnRowInserted="FilterSpecification_RowInserted">
                        <eq:eqSpecificationColumn HeaderText="Тип документа">
                            <ItemTemplate>
                                <eq:eqDropDownList ID="DocTypeID" runat="server" Width="100%" OnSelectedIndexChanged="RefreshSpecificationRow"
                                    AutoPostBack="True" />
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                        <eq:eqSpecificationColumn HeaderText="Кнопка">
                            <ItemTemplate>
                                <eq:eqDropDownList ID="IDButton" UniqueGroupName="RTBDLIDB" runat="server" Width="100%" />
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                        <eq:eqSpecificationColumn Width="70px">
                            <HeaderTemplate>
                                <div style="color: Red;">
                                    <%=IIf(AppSettings.RolesAllowPriority, "Приоритет", "Разрешить")%></div>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="Priority" runat="server" />
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                    </eq:eqSpecification>
                </ContentTemplate>
            </asp:UpdatePanel>
        </ContentTemplate>
    </aj:TabPanel>
    <aj:TabPanel runat="server" HeaderText="Поля">
        <ContentTemplate>
            <div class="LogicBlockCaption">
                Ограничение доступа к полям документа
            </div>
            <asp:UpdatePanel ID="upeqRolesControlDenied" runat="server" UpdateMode="Conditional"
                RenderMode="Inline">
                <ContentTemplate>
                    <eq:eqSpecification ID="eqRolesControlDenied" runat="server" Width="100%" OnRowInserted="FilterSpecification_RowInserted">
                        <eq:eqSpecificationColumn HeaderText="Тип документа">
                            <ItemTemplate>
                                <eq:eqDropDownList ID="IDDocType" runat="server" Width="100%" OnSelectedIndexChanged="RefreshSpecificationRow"
                                    AutoPostBack="True" />
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                        <eq:eqSpecificationColumn HeaderText="Поле документа">
                            <ItemTemplate>
                                <eq:eqDropDownList ID="IDControl" runat="server" Width="100%" OnSelectedIndexChanged="RefreshSpecificationRow"
                                    AutoPostBack="True" />
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                        <eq:eqSpecificationColumn HeaderText="Статус">
                            <ItemTemplate>
                                <eq:eqDropDownList ID="StateID" runat="server" Width="100%" />
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                        <eq:eqSpecificationColumn Width="50px" HeaderText="Скрыть">
                            <ItemTemplate>
                                <asp:CheckBox ID="Hidden" runat="server" />
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                        <eq:eqSpecificationColumn Width="90px" HeaderText="Только чтение">
                            <ItemTemplate>
                                <asp:CheckBox ID="Disable" runat="server" />
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                        <eq:eqSpecificationColumn Width="70px">
                            <HeaderTemplate>
                                <div style="color: Red;">
                                    <%=IIf(AppSettings.RolesAllowPriority, "Приоритет", "Разрешить")%></div>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="Priority" runat="server" />
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                    </eq:eqSpecification>
                </ContentTemplate>
            </asp:UpdatePanel>
        </ContentTemplate>
    </aj:TabPanel>
    <aj:TabPanel runat="server" HeaderText="Список">
        <ContentTemplate>
            <div class="LogicBlockCaption">
                Ораничение доступа к документам в списке документов, дополнительные условия
            </div>
            <asp:UpdatePanel ID="upeqRolesListCondSpecial" runat="server" UpdateMode="Conditional"
                RenderMode="Inline">
                <ContentTemplate>
                    <eq:eqSpecification ID="eqRolesListCondSpecial" runat="server" Width="100%" OnRowInserted="FilterSpecification_RowInserted">
                        <eq:eqSpecificationColumn HeaderText="Тип документа">
                            <ItemTemplate>
                                <eq:eqDropDownList ID="DocTypeID" runat="server" Width="100%" AutoPostBack="true"
                                    OnSelectedIndexChanged="RefreshSpecificationRow" />
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                        <eq:eqSpecificationColumn HeaderText="Выбор функции">
                            <ItemTemplate>
                                <eq:eqDropDownList ID="FunctionID" runat="server" AutoPostBack="true" OnSelectedIndexChanged="RefreshSpecificationRow" />
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                        <eq:eqSpecificationColumn HeaderText="Функция">
                            <ItemTemplate>
                                <asp:HiddenField ID="Parameter" runat="server" />
                                <asp:CheckBox ID="Enabled" runat="server" Visible="false" />
                                <ldf:ListDocFunction ID="Function" runat="server" OnFunctionChanged="Function_OnFunctionChanged"
                                    Visible="false" />
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                        <eq:eqSpecificationColumn Width="70px">
                            <HeaderTemplate>
                                <div style="color: Red;">
                                    <%=IIf(AppSettings.RolesAllowPriority, "Приоритет", "Разрешить")%></div>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="Priority" runat="server" />
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                    </eq:eqSpecification>
                </ContentTemplate>
            </asp:UpdatePanel>
        </ContentTemplate>
    </aj:TabPanel>
    <aj:TabPanel runat="server" HeaderText="Переходы">
        <ContentTemplate>
            <div class="LogicBlockCaption">
                Ограничение переходов по статусам для документов
            </div>
            <asp:UpdatePanel ID="upeqRolesStatus" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                <ContentTemplate>
                    <eq:eqSpecification ID="eqRolesStatus" runat="server" Width="100%" OnRowInserted="FilterSpecification_RowInserted">
                        <eq:eqSpecificationColumn HeaderText="Тип документа">
                            <ItemTemplate>
                                <eq:eqDropDownList ID="DocTypeID" runat="server" Width="100%" OnSelectedIndexChanged="RefreshSpecificationRow"
                                    AutoPostBack="True" />
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                        <eq:eqSpecificationColumn HeaderText="Из статуса">
                            <ItemTemplate>
                                <asp:DropDownList ID="SourceStateID" runat="server" Width="100%" />
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                        <eq:eqSpecificationColumn HeaderText="В статус">
                            <ItemTemplate>
                                <asp:DropDownList ID="DestStateID" runat="server" Width="100%" />
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                        <eq:eqSpecificationColumn Width="70px">
                            <HeaderTemplate>
                                <div style="color: Red;">
                                    <%=IIf(AppSettings.RolesAllowPriority, "Приоритет", "Разрешить")%></div>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="Priority" runat="server" />
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                    </eq:eqSpecification>
                </ContentTemplate>
            </asp:UpdatePanel>
        </ContentTemplate>
    </aj:TabPanel>
    <aj:TabPanel runat="server" HeaderText="Статусы">
        <ContentTemplate>
            <div class="LogicBlockCaption">
                Ограничение редактируемости документов в зависимости от статуса
            </div>
            <asp:UpdatePanel ID="upeqRolesDocumentDisableByStatus" runat="server" UpdateMode="Conditional"
                RenderMode="Inline">
                <ContentTemplate>
                    <eq:eqSpecification ID="eqRolesDocumentDisableByStatus" runat="server" Width="100%"
                        OnRowInserted="FilterSpecification_RowInserted">
                        <eq:eqSpecificationColumn HeaderText="Тип документа">
                            <ItemTemplate>
                                <eq:eqDropDownList ID="DocTypeID" runat="server" Width="100%" OnSelectedIndexChanged="RefreshSpecificationRow"
                                    AutoPostBack="True" />
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                        <eq:eqSpecificationColumn HeaderText="Статус">
                            <ItemTemplate>
                                <eq:eqDropDownList ID="StateID" UniqueGroupName="RDDBSSID" runat="server" Width="100%" />
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                        <eq:eqSpecificationColumn Width="70px">
                            <HeaderTemplate>
                                <div style="color: Red;">
                                    <%=IIf(AppSettings.RolesAllowPriority, "Приоритет", "Разрешить")%></div>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="Priority" runat="server" />
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                    </eq:eqSpecification>
                </ContentTemplate>
            </asp:UpdatePanel>
        </ContentTemplate>
    </aj:TabPanel>
    <aj:TabPanel runat="server" HeaderText="Обязательность">
        <ContentTemplate>
            <div class="LogicBlockCaption">
                Обязательность полей документа при переходе к статусу
            </div>
            <asp:UpdatePanel ID="upeqDocFieldStatusRequired" runat="server" UpdateMode="Conditional"
                RenderMode="Inline">
                <ContentTemplate>
                    <eq:eqSpecification ID="eqDocFieldStatusRequired" runat="server" Width="100%" OnRowInserted="FilterSpecification_RowInserted">
                        <eq:eqSpecificationColumn HeaderText="Тип документа">
                            <ItemTemplate>
                                <eq:eqDropDownList ID="DocTypeID" runat="server" Width="100%" OnSelectedIndexChanged="RefreshSpecificationRow"
                                    AutoPostBack="True" />
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                        <eq:eqSpecificationColumn HeaderText="Поле формы">
                            <ItemTemplate>
                                <eq:eqDropDownList ID="FieldID" runat="server" Width="100%" />
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                        <eq:eqSpecificationColumn HeaderText="При переходе к статусу">
                            <ItemTemplate>
                                <eq:eqDropDownList ID="DocStateID" runat="server" Width="100%" />
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                        <eq:eqSpecificationColumn Width="70px">
                            <HeaderTemplate>
                                <div style="color: Red;">
                                    <%=IIf(AppSettings.RolesAllowPriority, "Приоритет", "Разрешить")%></div>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="Priority" runat="server" />
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                    </eq:eqSpecification>
                </ContentTemplate>
            </asp:UpdatePanel>
        </ContentTemplate>
    </aj:TabPanel>
    <aj:TabPanel runat="server" HeaderText="Панели">
        <ContentTemplate>
            <div class="LogicBlockCaption">
                Ограничение доступа к панелям в документе
            </div>
            <asp:UpdatePanel ID="upeqRolesDocAreasList" runat="server" UpdateMode="Conditional"
                RenderMode="Inline">
                <ContentTemplate>
                    <eq:eqSpecification ID="eqRolesDocAreasList" runat="server" Width="100%" OnRowInserted="FilterSpecification_RowInserted">
                        <eq:eqSpecificationColumn HeaderText="Тип документа">
                            <ItemTemplate>
                                <eq:eqDropDownList ID="IDDocType" runat="server" Width="100%" OnSelectedIndexChanged="RefreshSpecificationRow"
                                    AutoPostBack="True" />
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                        <eq:eqSpecificationColumn HeaderText="Панель в документе">
                            <ItemTemplate>
                                <eq:eqDropDownList ID="IDArea" runat="server" Width="100%" />
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                        <eq:eqSpecificationColumn HeaderText="Статус">
                            <ItemTemplate>
                                <eq:eqDropDownList ID="IDDocStates" runat="server" Width="100%" />
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                        <eq:eqSpecificationColumn Width="50px" HeaderText="Скрыть">
                            <ItemTemplate>
                                <asp:CheckBox ID="Hidden" runat="server" />
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                        <eq:eqSpecificationColumn Width="90px" HeaderText="Только чтение">
                            <ItemTemplate>
                                <asp:CheckBox ID="Disable" runat="server" />
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                        <eq:eqSpecificationColumn Width="60px" HeaderText="Свернуто">
                            <ItemTemplate>
                                <asp:CheckBox ID="Collapsed" runat="server" />
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                        <eq:eqSpecificationColumn Width="70px">
                            <HeaderTemplate>
                                <div style="color: Red;">
                                    <%=IIf(AppSettings.RolesAllowPriority, "Приоритет", "Разрешить")%></div>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="Priority" runat="server" />
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                    </eq:eqSpecification>
                </ContentTemplate>
            </asp:UpdatePanel>
        </ContentTemplate>
    </aj:TabPanel>
</aj:TabContainer>
