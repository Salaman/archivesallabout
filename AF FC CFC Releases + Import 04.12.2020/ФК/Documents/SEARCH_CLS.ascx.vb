﻿Partial Public Class SEARCH_CLS
    Inherits BaseDoc

    Private ListDocRESULT As ListDoc

    Private Property SelectedCLS() As Guid
        Get
            If ViewState("SelectedCLS") Is Nothing Then
                Return Guid.Empty
            Else
                Return ViewState("SelectedCLS")
            End If
        End Get
        Set(ByVal value As Guid)
            ViewState("SelectedCLS") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        LoadListDocRESULT()
    End Sub

    Private Sub LoadListDocRESULT()
        PlaceHolderRESULT.Controls.Clear()
        ListDocRESULT = DirectCast(LoadControl("../ListDoc.ascx"), ListDoc)
        ListDocRESULT.ID = MenuRESULT.SelectedValue.Replace(".ascx", "")

        Dim sessionDocChange As Integer = 0

        If (ListDocRESULT.ClientID = "DOCUMENT") Then
            sessionDocChange = 1
            Session("StateListDoc") = sessionDocChange

        ElseIf (ListDocRESULT.ClientID = "FUND") Then
            sessionDocChange = 2
            Session("StateListDoc") = sessionDocChange

        ElseIf (ListDocRESULT.ClientID = "INVENTORY") Then
            sessionDocChange = 3
            Session("StateListDoc") = sessionDocChange

        ElseIf (ListDocRESULT.ClientID = "UNIT") Then
            sessionDocChange = 4
            Session("StateListDoc") = sessionDocChange

        End If

        ListDocRESULT.DocTypeURL = MenuRESULT.SelectedValue
        ListDocRESULT.LoadDocMode = eqBaseContainer.LoadDocModes.NewWindow
        ListDocRESULT.SelectDocMode = eqBaseContainer.SelectDocModes.Multi
        FormWhereStrings()
        PlaceHolderRESULT.Controls.Add(ListDocRESULT)
    End Sub

    Private Sub ListDocCLS_SelectDoc(ByVal DocID As System.Guid) Handles ListDocCLS.SelectDoc
        SelectedCLS = DocID
        FormWhereStrings()
        ListDocRESULT.Refresh()
    End Sub

    Private Sub FormWhereStrings()
        If SelectedCLS <> Guid.Empty Then
            Select Case ListDocRESULT.DocTypeURL
                Case "FUND.ascx"
                    ListDocRESULT.AdditionalFromString = "LEFT JOIN vREF_CLS701_FUND SR ON SR.ISN_OBJ=[" & ListDocRESULT.DocType.GetHeaderTableName & "].ISN_FUND" & vbCrLf & _
                                                         "LEFT JOIN vCLS701 SC ON SC.ISN_CLS=SR.ISN_CLS"
                    ListDocRESULT.AdditionalWhereString = "AND SR.KIND=701 AND SC.ID='" & SelectedCLS.ToString & "'"
                Case "INVENTORY.ascx"
                    ListDocRESULT.AdditionalFromString = "LEFT JOIN vREF_CLS701_INVENTORY SR ON SR.ISN_OBJ=[" & ListDocRESULT.DocType.GetHeaderTableName & "].ISN_INVENTORY" & vbCrLf & _
                                                         "LEFT JOIN vCLS701 SC ON SC.ISN_CLS=SR.ISN_CLS"
                    ListDocRESULT.AdditionalWhereString = "AND SR.KIND=702 AND SC.ID='" & SelectedCLS.ToString & "'"
                Case "UNIT.ascx"
                    ListDocRESULT.AdditionalFromString = "LEFT JOIN vREF_CLS701_UNIT SR ON SR.ISN_OBJ=[" & ListDocRESULT.DocType.GetHeaderTableName & "].ISN_UNIT" & vbCrLf & _
                                                         "LEFT JOIN vCLS701 SC ON SC.ISN_CLS=SR.ISN_CLS"
                    ListDocRESULT.AdditionalWhereString = "AND (SR.KIND=703 OR SR.KIND=704) AND SC.ID='" & SelectedCLS.ToString & "'"
                Case "DOCUMENT.ascx"
                    ListDocRESULT.AdditionalFromString = "LEFT JOIN vREF_CLS701_DOCUMENT SR ON SR.ISN_OBJ=[" & ListDocRESULT.DocType.GetHeaderTableName & "].ISN_DOCUM" & vbCrLf &
                                                         "LEFT JOIN vCLS701 SC ON SC.ISN_CLS=SR.ISN_CLS"
                    ListDocRESULT.AdditionalWhereString = "AND SR.KIND=705 AND SC.ID='" & SelectedCLS.ToString & "'"
                    'для подсчета документов в иерархии
                    Session("SelectedCLSforDocs") = SelectedCLS
            End Select
        Else
            ListDocRESULT.ListDocRefreshOnLoad = False
        End If
    End Sub

    Private Sub MenuRESULT_MenuItemClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MenuEventArgs) Handles MenuRESULT.MenuItemClick
        LoadListDocRESULT()
    End Sub

    Public Sub ButtonDeattach_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonDeattach.Click
        If SelectedCLS <> Guid.Empty Then
            Dim cmdSB As New Text.StringBuilder
            cmdSB.AppendLine("DECLARE @ISN_CLS bigint")
            cmdSB.AppendLine("SELECT @ISN_CLS=ISN_CLS FROM tblCLS WHERE ID='" & SelectedCLS.ToString & "'")
            For Each doc In ListDocRESULT.SelectedDocuments
                Select Case ListDocRESULT.DocTypeURL
                    Case "FUND.ascx"
                        cmdSB.AppendLine("DELETE FROM tblREF_CLS WHERE ISN_CLS=@ISN_CLS AND KIND=701 AND DocID='" & doc.ToString & "'")
                    Case "INVENTORY.ascx"
                        cmdSB.AppendLine("DELETE FROM tblREF_CLS WHERE ISN_CLS=@ISN_CLS AND KIND=702 AND DocID='" & doc.ToString & "'")
                    Case "UNIT.ascx"
                        cmdSB.AppendLine("DELETE FROM tblREF_CLS WHERE ISN_CLS=@ISN_CLS AND (KIND=703 OR KIND=704) AND DocID='" & doc.ToString & "'")
                End Select
            Next
            AppSettings.ExecCommand(New SqlClient.SqlCommand(cmdSB.ToString))
            FormWhereStrings()
            ListDocRESULT.Refresh()
        End If
    End Sub

End Class