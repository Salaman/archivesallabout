﻿Imports System.Data
Imports System.Data.SqlClient

Partial Public Class ARCHIVE
    Inherits BaseDoc

#Region "События"

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        HyperLinkFUNDs.Enabled = DocStateInfo.StateID <> Guid.Empty
        HyperLinkFUNDs.NavigateUrl = BasePage.GetListDocURL("FUND.ascx", False, "ARCHIVE=" & DocID.ToString)
        HyperLinkPassportAdd.Enabled = DocStateInfo.StateID <> Guid.Empty
        HyperLinkPassportAdd.NavigateUrl = BasePage.GetNewDocURL("ARCHIVE_PASSPORT.ascx", False, "ARCHIVE=" & DocID.ToString)
        HyperLinkSTRs.Enabled = DocStateInfo.StateID <> Guid.Empty
        HyperLinkSTRs.NavigateUrl = BasePage.GetListDocURL("INVENTORYSTRUCTURE.ascx", False, "ARCHIVE=" & DocID.ToString)

        If Crumbs.ISN_AuthorizedDep IsNot Nothing AndAlso Not IsDBNull(DataSetHeaderDataRow("ISN_AuthorizedDep")) Then
            Crumbs.ISN_AuthorizedDep.KeyValue = DataSetHeaderDataRow("ISN_AuthorizedDep")
        End If
    End Sub

#End Region

#Region "Документ"

    Public Overrides Function NewDoc() As Boolean
        Dim dt = AppSettings.GetDataTable(New SqlCommand("SELECT ID FROM tblARCHIVE WHERE Deleted=0"))
        If dt.Rows.Count = 1 Then
            Response.Redirect(BasePage.GetDocURL(DocTypeURL, DirectCast(dt.Rows(0).Item("ID"), Guid), BasePage.IsModal))
            Return True
        ElseIf dt.Rows.Count > 1 Then
            Response.Redirect(BasePage.GetListDocURL(DocTypeURL, BasePage.IsModal))
            Return True
        End If
        Return MyBase.NewDoc()
    End Function

    Public Overrides Function SaveDoc() As Boolean
        SaveDoc = MyBase.SaveDoc()
        If SaveDoc Then
            Dim InventoryStrucrureSQL = <sql>
                                        declare @ISN bigint
                                        select top 1 @ISN = ISN_ARCHIVE from tblARCHIVE
                                        if not exists(select ID from tblINVENTORY_STRUCTURE where NAME = 'Структура описи' and ISN_HIGH_INVENTORY_CLS is null)
                                        insert into tblINVENTORY_STRUCTURE values
                                        (newid(), '12345678-9012-3456-7890-123456789012', getdate(), '00000000-0000-0000-0000-000000000000', 0, 'A4366C7E-ACBA-4A71-B59A-15D51107DBFE', 0,
                                        @ISN, null, null, null, null, null, 'Структура описи', null, 'F', 'Y', 1)
                                    </sql>
            Dim InventoryStrucrureCommand As New SqlCommand(InventoryStrucrureSQL.Value)
            AppSettings.ExecCommand(InventoryStrucrureCommand)
        End If
    End Function

    Public Overrides Function DeleteDoc() As Boolean
        Dim DeleteRecursivelyCommand As New SqlCommand("dbo.DeleteRecursively") With {.CommandType = CommandType.StoredProcedure}
        DeleteRecursivelyCommand.Parameters.AddWithValue("@ISN", DataSetHeaderDataRow("ISN_ARCHIVE"))
        DeleteRecursivelyCommand.Parameters.AddWithValue("@OBJ", "archive")
        DeleteRecursivelyCommand.CommandTimeout = 3000 '(Roman20140115) было 30, но при удалении огромных многогигабайтных архивов этого времени нехватает
        AppSettings.ExecCommand(DeleteRecursivelyCommand)

        setDocStatesLogDeletedRecursively("Удален")

        Return MyBase.DeleteDoc()
    End Function

#End Region

#Region "Контролы"

    Public Overrides Sub PutValueToControl(ByVal Value As Object, ByVal TargetControl As System.Web.UI.Control, Optional ByVal FieldFormat As String = "")
        If TypeOf TargetControl Is CheckBox Then
            DirectCast(TargetControl, CheckBox).Checked = (Value Is Nothing OrElse Value.ToString.Trim.ToUpper = "Y")
        Else
            MyBase.PutValueToControl(Value, TargetControl, FieldFormat)
        End If
    End Sub

    Public Overrides Function GetValueFromControl(ByVal SourceControl As System.Web.UI.Control) As Object
        If TypeOf SourceControl Is CheckBox Then
            Return IIf(DirectCast(SourceControl, CheckBox).Checked, "Y", "N")
        Else
            Return MyBase.GetValueFromControl(SourceControl)
        End If
    End Function

#End Region

#Region "Файлы"

    Private Sub RefFileUploadFile_Uploaded(ByVal sender As Object, ByVal e As RefFileArgs) Handles RefFileUploadFile.Uploaded
        Dim row = vREF_FILE_ARCHIVE.InsertRow(e.ID)
        DataSetSpecificationDataRow(vREF_FILE_ARCHIVE.ID, e.ID)("CreationDateTime") = e.TransferTime
        DirectCast(row.FindControl("NAME"), TextBox).Text = System.IO.Path.GetFileName(e.FileName)
    End Sub

    Private Sub RefFileUploadFile_Uploading(ByVal sender As Object, ByVal e As RefFileArgs) Handles RefFileUploadFile.Uploading
        e.FileName = GetFileSystemAttachFileName(e.ID, e.TransferTime, 700, DBNull.Value, e.FileName)
    End Sub

    Public Sub RefFile_Requesting(ByVal sender As Object, ByVal e As RefFileArgs)
        e.TransferTime = DataSetSpecificationDataRow(vREF_FILE_ARCHIVE.ID, e.ID)("CreationDateTime")
        e.FileName = GetFileSystemAttachFileName(e.ID, e.TransferTime, 700, DBNull.Value)
    End Sub

#End Region

End Class