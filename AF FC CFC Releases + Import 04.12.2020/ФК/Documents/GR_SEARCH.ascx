﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="GR_SEARCH.ascx.vb" Inherits="WebApplication.GR_SEARCH" %>
<asp:Panel runat="server" DefaultButton="ButtonSearch">
<table style="width: 700px;">
        <tr>
            <td style="width: 50px;">
                Поиск:
            </td>
            <td style="width: 100%;">
                <eq:eqTextBox ID="TextBoxSearchPattern" runat="server" />
            </td>
            <td>
                <asp:Button ID="ButtonSearch" runat="server" Text="Поиск" />
            </td>
        </tr>
    </table>
    
   <table "width: 1000px;">
                <tr>
                    <td>
                    <fieldset>
                            <legend>Полнотекстовый поиск</legend>
                            
                            <table>
          <tr>
        <td>
            <asp:CheckBox ID="ALL_FIELD" runat="server" Text="Все поля" AutoPostBack="true" Checked="true" TextAlign="Right"/>
        </td>
        
        
    </tr>
    </table>
<table>
          <tr>
        <td>
            <asp:CheckBox ID="NAME" runat="server" Text="Название" Checked="true" TextAlign="Right"/>
        </td>
        <td>
            <asp:CheckBox ID="SELF_NAME" runat="server" Text="Самоназвание" Checked="true" TextAlign="Right"/>
        </td>
        <td>
            <asp:CheckBox ID="CREATION_DATE" runat="server" Text="Дата (время создания)" Checked="true" TextAlign="Right"/>
        </td>
        <td>
            <asp:CheckBox ID="APPROX_DATE" runat="server" Text="Ориентировочная дата" Checked="true" TextAlign="Right"/>
        </td>
        <td>
            <asp:CheckBox ID="HISTORY" runat="server" Text="Историческая справка" Checked="true" TextAlign="Right"/>
        </td>
        <td>
            <asp:CheckBox ID="ANNOTATION" runat="server" Text="Аннотация" Checked="true" TextAlign="Right"/>
        </td>
        <td>
            <asp:CheckBox ID="PRECIOUS" runat="server" Text="Наличие драгоценных металлов и камней" Checked="true" TextAlign="Right"/>
        </td>
        <td>
            <asp:CheckBox ID="AUTHOR" runat="server" Text="Автор" Checked="true" TextAlign="Right"/>
        </td>
    </tr>
    </table>
    
    <table>
          <tr>
          <td>
            <asp:CheckBox ID="PALEO_FEATURES" runat="server" Text="Палеографические особенности" Checked="true" TextAlign="Right"/>
        </td>
          <td>
            <asp:CheckBox ID="SEALS" runat="server" Text="Печати" Checked="true" TextAlign="Right"/>
        </td>
        <td>
            <asp:CheckBox ID="ART_DESIGN" runat="server" Text="Художественные особенности оформления" Checked="true" TextAlign="Right"/>
        </td>
        <td>
            <asp:CheckBox ID="SIZES" runat="server" Text="Размеры" Checked="true" TextAlign="Right"/>
        </td>
        <td>
            <asp:CheckBox ID="VOLUME" runat="server" Text="Объем" Checked="true" TextAlign="Right"/>
        </td>
        <td>
            <asp:CheckBox ID="PHYSICAL_STATE" runat="server" Text="Физическое состояние" Checked="true" TextAlign="Right"/>
        </td>
        <td>
            <asp:CheckBox ID="RESTORATION" runat="server" Text="Сведения о реставрации" Checked="true" TextAlign="Right"/>
        </td>
        <td>
            <asp:CheckBox ID="PROTOCOL_ORGANIZATION" runat="server" Text="Кем представлен документ" Checked="true" TextAlign="Right"/>
        </td>
    </tr>
    </table>
   </fieldset>
   
    <table>
   <tr>
   <td>
            <asp:CheckBox ID="ADVANCED_SEARCH" runat="server" Text="Расширенный поиск" AutoPostBack="true" Checked="true" TextAlign="Right"/>
        </td>
    </tr>
    </table>
<fieldset>                       
<asp:Panel ID="Panel_ADVANCED_SEARCH" runat="server" width="500" >

<table width="300" TextAlign="Left">
            <tr>
                <td>
                    Архив
                </td>
                <td>
                    <asp:DropDownList ID="ID_ARCHIVE" runat="server" Width="250px">
                    </asp:DropDownList>
                    
                </td>
                <td>
                   Вид документа
                </td>
                <td>
                    <asp:DropDownList ID="ID_DOC_KIND" runat="server" Width="150px">
                    </asp:DropDownList>
                    
                </td>
                <td>
                   Язык документа  
                </td>
                <td>
                    <asp:DropDownList ID="ID_LANGUAGE" runat="server" Width="150px">
                    </asp:DropDownList>
                    
                </td>
                <td>
                   Собственность  
                </td>
                <td>
                    <asp:DropDownList ID="ID_PROPERTY" runat="server" Width="150px">
                    </asp:DropDownList>
                    
                </td>
            </tr>
        </table>
          <table width="300" TextAlign="Left">   
            <tr>
                <td>
                    Материальный носитель
                </td>
                <td>
                    <eq3:eqDocument ID="ID_CARRIER" runat="server" DocTypeURL="GR_CARRIER_CL.ascx" ListDocMode="NewWindowAndSelect"
                                            LoadDocMode="NewWindowAndSelect" ShowingFields="NAME" ShowingFieldsFormat="{0}"
                                            KeyName="ID" />
                    
                </td>
                
                
            </tr>
        </table>
        
         <table >
            <tr>
                <td>
                    <fieldset>
                        <legend>Датировка</legend>
        <asp:Panel ID="Panel_Dating_RB" runat="server">
        <table width="100%" cellspacing="4">
            <colgroup>
                <col width="110px" />
            </colgroup>
                   
                        <asp:RadioButtonList ID="Prop_Dating_type" runat="server" RepeatDirection="vertical"
                            AutoPostBack="true" RepeatLayout="Flow" Width="100px">
                            <asp:ListItem Text="даты" Value="1" Selected="true""></asp:ListItem>
                            <asp:ListItem Text="века" Value="2""></asp:ListItem>
                        </asp:RadioButtonList>
                   
                </td>
            </tr>
        </table>
        </asp:Panel>
        <asp:Panel ID="Panel_DATE" runat="server">
         <table width="300" TextAlign="Left">
            <tr>
                <td>
                    Дата с
                </td>
                <td>
                    <eq:eqTextBox ID="DATE_FROM" runat="server" Width="70px" />
                    <aj:CalendarExtender runat="server" TargetControlID="DATE_FROM" />
                </td>
                <td>
                    по
                </td>
                <td>
                    <eq:eqTextBox ID="DATE_TO" runat="server" Width="70px" />
                    <aj:CalendarExtender runat="server" TargetControlID="DATE_TO" />
                </td>
            </tr>
        </table>
         </asp:Panel>
         <asp:Panel ID="Panel_CENTURY" runat="server" visible="false">
        <table width="300" TextAlign="Left">   
            <tr>
                <td>
                    Век с
                </td>
                <td>
                    <asp:DropDownList ID="ID_CENTURY_FROM" runat="server" Width="70px">
                    </asp:DropDownList>
                    
                </td>
                <td>
                    по
                </td>
                <td>
                    <asp:DropDownList ID="ID_CENTURY_TO" runat="server" Width="70px">
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
         </asp:Panel>
    </fieldset>
                </td>
            </tr>
        </table>
       
        
    </asp:Panel>
   </fieldset>
  </td>
  </tr>
            </table> 
               
    
    <asp:SqlDataSource ID="DataSourceSearch" runat="server" SelectCommand="SELECT * FROM GR_fn_Search(@fulltext_pattern, @match_pattern, @filter, @archive, @kind, @property, @language, @carrier, @century_from, @century_to, @date_from, @date_to) ORDER BY NUM ">
        <SelectParameters>
            <asp:Parameter Name="fulltext_pattern" Type="String" DefaultValue="@@@@@@@@@@@@@@" />
        </SelectParameters>
         <SelectParameters>
            <asp:Parameter Name="match_pattern" Type="String" DefaultValue="@@@@@@@@@@@@@@" />
        </SelectParameters>
        <SelectParameters>
            <asp:Parameter Name="filter" Type="String" DefaultValue="@@@@@@@@@@@@@@" />
        </SelectParameters>
       <SelectParameters>
            <asp:Parameter Name="archive" Type="String" DefaultValue="@@@@@@@@@@@@@@" />
        </SelectParameters>
        <SelectParameters>
            <asp:Parameter Name="kind" Type="String" DefaultValue="@@@@@@@@@@@@@@" />
        </SelectParameters>
        <SelectParameters>
            <asp:Parameter Name="property" Type="String" DefaultValue="@@@@@@@@@@@@@@" />
        </SelectParameters>
       <SelectParameters>
            <asp:Parameter Name="language" Type="String" DefaultValue="@@@@@@@@@@@@@@" />
        </SelectParameters>
        <SelectParameters>
            <asp:Parameter Name="carrier" Type="String" DefaultValue="@@@@@@@@@@@@@@" />
        </SelectParameters>
        <SelectParameters>
            <asp:Parameter Name="century_from" Type="String" DefaultValue="@@@@@@@@@@@@@@" />
        </SelectParameters>
        <SelectParameters>
            <asp:Parameter Name="century_to" Type="String" DefaultValue="@@@@@@@@@@@@@@" />
        </SelectParameters>
        <SelectParameters>
            <asp:Parameter Name="date_from" Type="String" DefaultValue="1000-01-01" />
        </SelectParameters>
        <SelectParameters>
            <asp:Parameter Name="date_to" Type="String" DefaultValue="2100-01-01" />
        </SelectParameters>
    </asp:SqlDataSource>
    <table style="width: 700px; height: 50px; ">
        <tr>
            <td>
               
            </td>
        </tr>
    </table>
    <table style="width: 700px;  ">
        <tr>
            <td>
                <asp:Label ID="LabelMessage" runat="server" />
            </td>
        </tr>
    </table>
    <asp:ListView ID="ListViewResults" runat="server" DataSourceID="DataSourceSearch">
        <LayoutTemplate>
        
            <table style="width: 700px;">
                <tr>
                    <td>
                        <asp:DataPager runat="server" PageSize="20">
                            <Fields>
                                <asp:NextPreviousPagerField ShowFirstPageButton="true" ShowNextPageButton="false" FirstPageText="<<" PreviousPageText="<" />
                                <asp:NumericPagerField ButtonCount="10" />
                                <asp:NextPreviousPagerField ShowLastPageButton="true" ShowPreviousPageButton="false" LastPageText=">>" NextPageText=">" />
                            </Fields>
                        </asp:DataPager>
                    </td>
                </tr>            
                <tr>
                    <td>
                        <div id="itemPlaceholder" runat="server"></div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:DataPager runat="server" PageSize="20">
                            <Fields>
                                <asp:NextPreviousPagerField ShowFirstPageButton="true" ShowNextPageButton="false" FirstPageText="<<" PreviousPageText="<" />
                                <asp:NumericPagerField ButtonCount="10" />
                                <asp:NextPreviousPagerField ShowLastPageButton="true" ShowPreviousPageButton="false" LastPageText=">>" NextPageText=">" />
                            </Fields>
                        </asp:DataPager>
                    </td>
                </tr>
            </table>
        </LayoutTemplate>
        <ItemTemplate>
            <div>
                <p>
                    <h4>
                        <a href='<%#evalHref(Container) %>'><%#evalTitle(Container)%></a>
                    </h4>
                </p>
                <p>
                 <%--   <%#evalText(Container)%>--%>
                </p>
                <p style="color: Gray;">
                    <font color="gray">
                        <small>
                           
                        </small>                       
                    </font>
                </p>
                <p style="color: Gray;">
                    <font color="gray">
                        <small>
                           
                        </small>
                    </font>
                </p>
            </div>
        </ItemTemplate>
    </asp:ListView>
</asp:Panel>