﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="GR_DOCUMENT_SHORT.ascx.vb" Inherits="WebApplication.GR_DOCUMENT_SHORT" %>
<table style="width: 97%; margin-bottom: 4px;">
</table>
<table style="width: 700px;">
    <colgroup>
        <col width="150px" />
        <col />
    </colgroup>
    <tr>
        <td>
            ЛИСТ УЧЕТА И ОПИСАНИЯ УНИКАЛЬНОГО ДОКУМЕНТА
        </td>
    </tr>
</table>
<table style="width: 700px;">
    <colgroup>
        <col width="150px" />
        <col />
    </colgroup>
    <tr>
        <td>
            Номер документа
        </td>
        <td>
            <eq:eqTextBox ID="NUM_Fict" runat="server" Height="20px" Width="50px" ></eq:eqTextBox>
            <eq:eqTextBox ID="NUM" runat="server" Height="20px" Width="50px" Enable="False"></eq:eqTextBox>
        </td>
         </tr>
         </table>
         <table>
          <tr>
        <td>
            Дата включения документа в Государственный реестр
        </td>
        <td>
            <eq:eqTextBox ID="DOC_DATE" runat="server" Height="20px" Width="70px" Enable="False"></eq:eqTextBox>
            <eq:eqTextBox ID="DOC_DATE_Fict" runat="server" Height="20px" Width="70px"></eq:eqTextBox>
            <aj:MaskedEditExtender ID="DOC_DATE_MASK" TargetControlID="DOC_DATE_Fict" runat="server"
                MaskType="Date" Mask="99/99/9999" />
        </td>
        
    </tr>
    </table>
    <table>
          <tr>
        <td>
            1. Описание документа
        </td>
    </tr>
    </table>
    
    <table>
          <tr>
        <td>
            Название (заголовок) документа   
        </td>
        <td>
            <eq:eqTextBox ID="NAME" runat="server" Height="60px" Width="500px" TextMode="MultiLine"></eq:eqTextBox>
        </td>
    </tr>
    </table>
     <table>
          <tr>
        <td>
            Самоназвание документа    
        </td>
        <td>
            <eq:eqTextBox ID="SELF_NAME" runat="server" Height="60px" Width="500px" TextMode="MultiLine"></eq:eqTextBox>
        </td>
    </tr>
    </table>
   <table> 
     <tr>
        <td>
            Вид документа*
        </td>
        <td>
            <asp:DropDownList ID="ID_DOC_KIND" runat="server">
            </asp:DropDownList>
        </td>
    </tr>
     </table>
   <table> 
    <tr>
        <td>
            <eq:eqTextBox ID="ID_DOC_DESCRIPT_CURRENT" runat="server" Visible="false"></eq:eqTextBox>
        </td>
        <td>
            <eq:eqTextBox ID="SYS_STATUS" runat="server" Visible="false" Enabled="false"></eq:eqTextBox>
        </td>
    </tr>
</table>

 <table>
   <tr>
   <td>
             Автор документа  
    </td>
    </tr>
    </table>
  <table> 
    <tr>
        <td>
             <fieldset>

                            <eq:eqSpecification ID="GR_tblDOC_AUTHOR" runat="server" ShowNumering="false">
                                <eq:eqSpecificationColumn HeaderText="" Width="100px">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ID_AUTHOR" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
        </td>
    </tr>
</table>
<table> 
     <tr>
        <td>
            Дата (время создания) документа
        </td>
        <td>
             <eq:eqTextBox ID="CREATION_DATE" runat="server" Width="550px" TextMode="MultiLine" />
        </td>
    </tr>
     </table>
     <table> 
     <tr>
        <td>
           Ориентировочная дата  
        </td>
        <td>
              <eq:eqTextBox ID="APPROX_DATE" runat="server" Width="100px" />
        </td>
    </tr>
     </table>
     <table> 
     <tr>
        <td>
            Век  
        </td>
        <td>
              <asp:DropDownList ID="ID_CENTURY" runat="server" Width="150px">
                                        </asp:DropDownList>
        </td>
    </tr>
     </table>
     
     
     <table>
   <tr>
   <td>
             Язык документа 
    </td>
    </tr>
    </table>
       <table> 
    <tr>
        <td>
            
                             <fieldset>
                            <eq:eqSpecification ID="GR_tblDOC_LANGUAGE" runat="server" ShowNumering="false">
                                <eq:eqSpecificationColumn HeaderText="Название" Width="500px">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ID_LANGUAGE" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                </eq:eqSpecification>
                        </fieldset>
        </td>
    </tr>
</table>
    
    <table>
                <tr>
                    <td>
                      
                        <eq:eqSpecification ID="GR_vDOC_DESCRIPT" runat="server" ShowNumering="false">
                            <eq:eqSpecificationColumn>
                                <ItemTemplate>
                                    <table style="width: 400px;">
  
                                        <tr>
                                            
                                            <td align="left">
                                                <eq:eqTextBox ID="NUM" runat="server" Width="50px" />
                                            </td>
                                        </tr>
                                         <tr>
                                            <td align="left">
                                                <eq:eqTextBox ID="DESCRIPT_DATE" runat="server" Width="70px"  />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                               
                                                    <eq:eqTextBox ID="ID_VOLUME" runat="server" Width="50px" visible="false"/>
                                            </td>
                                        </tr>
                                       
                                    </table>
                                    <table>
                                   <tr>
                                    <td>
                                             Аннотация
                                            </td>
                                    </tr>
                                   </table>
                                    <table>
                                        <tr>
                                           
                                             <td>
                                                <eq:eqTextBox ID="ANNOTATION" runat="server" Width="1000px" Height="200px" TextMode="MultiLine" />
                                            </td>
                                            
                                        </tr>
                                        <tr>
                                        
                                            <td>
                                                <eq:eqTextBox ID="ART_DESIGN" runat="server" Width="500px" Height="200px" TextMode="MultiLine" visible="False" />
                                            </td>
                                           
                                        </tr>
                                    </table>
                                    <table>
                                        <tr>
                                            <td>
                                                <eq:eqTextBox ID="DocID" runat="server"/>
                                            </td>
                                             
                                            <td>
                                                <eq:eqTextBox ID="RowID" runat="server" Visible="false" />
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                        </eq:eqSpecification>
                    </td>
                </tr>
            </table> 
             <table>
          <tr>
        <td>
             Историческая справка 
        </td>
    </tr>
    </table>
            <table>
                <tr>
                    
                    <td>
                        <eq:eqTextBox ID="HISTORY" runat="server" Width="1000px" Height="200px" TextMode="MultiLine" />
                    </td>
                </tr>
            </table>
           
   <table>
   <tr>
   <td>
             Наличие драгоценных металлов и камней  
    </td>
    </tr>
    </table>
    
     <table>
                <tr>
                    <td>
                            <eq:eqSpecification ID="GR_tblPRECIOUS_STONES" runat="server" ShowNumering="false">
                                <eq:eqSpecificationColumn>
                                    <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                        <eq:eqTextBox ID="ORIGINAL_RECORD" runat="server" TextMode="MultiLine"  Width="1000px"  Height="200px"/>
                                        <asp:DropDownList ID="ID_STONE" runat="server" Visible="false">
                                        </asp:DropDownList>
                                        <eq:eqTextBox ID="QUANTITY" runat="server" TextMode="MultiLine" Visible="false"/>
                                        <eq:eqTextBox ID="WHEIGHT" runat="server" TextMode="MultiLine" Visible="false"/>
                 
                                        <eq:eqTextBox ID="DocID" runat="server" Visible="false" />
                                        <eq:eqTextBox ID="RowID" runat="server" Visible="false" />
                                        <eq:eqTextBox ID="ID_DOC_DESCRIPT" runat="server"/>
                                        </td>
                                        </tr>
                                    </table>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                    </td>
                </tr>
            </table>
            
            
         <table>
          <tr>
        <td>
             Палеографические особенности 
        </td>
    </tr>
    </table>
    
             <table>
                <tr>
                     <td>
                            <eq:eqSpecification ID="GR_tblPALEO_FEATURES" runat="server" ShowNumering="false">
                                <eq:eqSpecificationColumn>
                                    <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                        <eq:eqTextBox ID="NAME" runat="server" TextMode="MultiLine" Width="1000px"  Height="200px"/>
                                        <eq:eqTextBox ID="LOCATION" runat="server" TextMode="MultiLine" Visible="false"/>
                                   </td>
                                        </tr>
                                    </table>
                                     </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                         </td>
                </tr>
            </table>
              <table>
          <tr>
        <td>
             Печати
        </td>
    </tr>
    </table>
          <table>
                <tr>
                     <td>
                            <eq:eqSpecification ID="GR_tblSEAL" runat="server" ShowNumering="false">
                                <eq:eqSpecificationColumn>
                                    <ItemTemplate>
                                        <table>
                                        <tr>
                                            <td>
                                        <eq:eqTextBox ID="NAME" runat="server" TextMode="MultiLine" Width="1000px"  Height="200px"/>
                                        <eq:eqTextBox ID="DESCRIPTION" runat="server" TextMode="MultiLine"  visible="false"/>
                                        <eq:eqTextBox ID="LOCATION" runat="server" TextMode="MultiLine"  visible="false"/>
                                        <eq:eqTextBox ID="HAS_KUSTODY" runat="server" TextMode="MultiLine"  visible="false"/>
                                        <eq:eqTextBox ID="PHYSICAL_STATE" runat="server" TextMode="MultiLine" visible="false" />
                                        </td>
                                        </tr>
                                    </table>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                       
                         </td>
                </tr>
            </table>
             <table>
                <tr>
                     <td>
                        Художественные особенности оформления  
                    </td>
                </tr>
            </table>
              <table>
                <tr>
                     
                    <td>
                        <eq:eqTextBox ID="ART_DESIGN_Fict" runat="server" Width="1000px" Height="200px" TextMode="MultiLine" />
                    </td>
                </tr>
            </table>
            
       <table>
          <tr>
        <td>
             Собственность*  
        </td>
        <td>
             <asp:DropDownList ID="ID_PROPERTY" runat="server">
            </asp:DropDownList>
        </td>
    </tr>
    </table>     
    
    <table>
          <tr>
        <td>
            2. Физическое состояние документа
        </td>
    </tr>
    </table> 
     <table>
          <tr>
        <td>
           Материальный носитель
        </td>
    </tr>
    </table> 
     <table > 
                <tr>
                    <td>
                        <fieldset>
                            <eq:eqSpecification ID="GR_tblDOC_CARRIER" runat="server" ShowNumering="false">
                                <eq:eqSpecificationColumn HeaderText="Материальный носитель" Width="300px">
                                    <ItemTemplate> 
                                        <eq3:eqDocument ID="ID_CARRIER" runat="server" DocTypeURL="GR_CARRIER_CL.ascx" ListDocMode="NewWindowAndSelect"
                                            LoadDocMode="NewWindowAndSelect" ShowingFields="NAME" ShowingFieldsFormat="{0}"
                                            KeyName="ID" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Век" Width="30px">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ID_CENTURY" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Оригинальная запись" Width="300px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="ORIGINAL_RECORD" runat="server" TextMode="MultiLine"/>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
            </table>
            
   
           <table>
          <tr>
        <td>
           Размеры
        </td>
    </tr>
    </table>   
          
                <table>
                <tr>
                    <td>
                        <eq:eqSpecification ID="GR_tblDOC_SIZE" runat="server">
                            <eq:eqSpecificationColumn>
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                Для листов
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="SHEET_NUMBERS" runat="server" Width="70px" />
                                            </td>
                                            <td>
                                                min/max/midi
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="MIN_MAX_MIDI" runat="server" Width="70px">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                    <table>
                                        <tr>
                                            <td>
                                                Латинское обозначение формата
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="LAT_NOTION1" runat="server" Width="70px" />
                                            </td>
                                            <td>
                                                .
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="LAT_NOTION2" runat="server" Width="70px" />
                                            </td>
                                            <td>
                                                /
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="LAT_NOTION3" runat="server" Width="70px" />
                                            </td>
                                        </tr>
                                    </table>
                                    <table>
                                        <tr>
                                            <td>
                                                Размер 1
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="DIM1" runat="server" Width="70px" />
                                            </td>
                                            <td>
                                                Диаметр
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="DIAMETER" runat="server" Width="70px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Размер 2
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="DIM2" runat="server" Width="70px" />
                                            </td>
                                            <td>
                                                Высота свитка
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="HEIGHT" runat="server" Width="70px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Размер 3
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="DIM3" runat="server" Width="70px" />
                                            </td>
                                            <td>
                                                Объем, листов
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="SHEET_VOL" runat="server" Width="70px" />
                                            </td>
                                        </tr>
                                    </table>
                                    <table>
                                        <td>
                                            Примечание
                                        </td>
                                        <td>
                                            <eq:eqTextBox ID="NOTE" runat="server" Width="300px" Height="100px" TextMode="MultiLine" />
                                        </td>
                                    </table>
                                    <table>
                                        <tr>
                                            <td>
                                                <eq:eqTextBox ID="DocID" runat="server"  />
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="RowID" runat="server" Visible="false"/>
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="ID_DOC_DESCRIPT" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                        </eq:eqSpecification>
                    </td>
                </tr>
            </table>
            
             <table>
          <tr>
        <td>
           Физическое состояние
        </td>
    </tr>
    </table>  
    
              <table>
                <tr>
                    <td>
                       
                            <eq:eqSpecification ID="GR_tblPHYSICAL_STATE" runat="server" ShowNumering="false">
                                <eq:eqSpecificationColumn>
                                    <ItemTemplate>
                                     <table>
                                        <tr>
                                            <td>
                                        <eq:eqTextBox ID="NAME" runat="server" TextMode="MultiLine" Width="1000px"  Height="200px"/>
                                        <eq:eqTextBox ID="RESTORATION_NEEDED" runat="server" TextMode="MultiLine" visible="false" />
                                        <eq:eqTextBox ID="DATE" runat="server" Width="100px" TextMode="MultiLine"  visible="false"  />
                                       </td>
                                        </tr>
                                    </table>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                       
                    </td>
                </tr>
            </table>
            
              <table>
          <tr>
        <td>
             Сведения о реставрации   
        </td>
    </tr>
    </table>
    
            <table>
                <tr>
                    <td>
                            <eq:eqSpecification ID="GR_tblRESTORATION" runat="server" ShowNumering="false">
                                <eq:eqSpecificationColumn>
                                    <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                        <eq:eqTextBox ID="INFO" runat="server" TextMode="MultiLine" />
                                
                                        <eq:eqTextBox ID="DATE" runat="server" Width="100px" Visible="false"/>
                                       
                                        <eq:eqTextBox ID="RESTORATORS" runat="server" TextMode="MultiLine" Visible="false"/>
                                   
                                        <eq:eqTextBox ID="RESTORE_CENTER" runat="server" TextMode="MultiLine" Visible="false"/>
                                   
                                        <eq:eqTextBox ID="PAGES" runat="server" TextMode="MultiLine" Visible="false"/>
                                    
                                        <eq:eqTextBox ID="DocID" runat="server" Visible="false" />
                                    
                                        <eq:eqTextBox ID="RowID" runat="server" Visible="false" />
                                   
                                        <eq:eqTextBox ID="ID_DOC_DESCRIPT" runat="server" />
                                        </td>
                                        </tr>
                                    </table>
                                    </ItemTemplate>
                                    
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        
                    </td>
                </tr>
            </table>
            
           <table>
          <tr>
        <td>
           3. Место хранения документа 
        </td>
    </tr> 
             </table>
             <table>
                <tr>
                   
                    <td align="center">
                        <%--СПЕЦИФИКАЦИЯ dbo.GR_tblSTORE_PLACE--%>
                        <eq:eqSpecification ID="GR_tblSTORE_PLACE" runat="server">
                            <eq:eqSpecificationColumn>
                                <ItemTemplate>
                                    <table style="width: 600px;">
                                        <colgroup>
                                            <col align="right" style="width: 30%;" />
                                            <col align="left" style="width: 20%;" />
                                            <col align="right" style="width: 30%;" />
                                            <col align="left" />
                                        </colgroup>
                                        <tr>
                                            <td>
                                                <fieldset>
                                                    <legend>Уровень архива</legend>
                                                    <asp:RadioButtonList ID="ARCHIVE_LEVEL" runat="server" RepeatDirection="vertical"
                                                        RepeatLayout="Flow" Width="500px">
                                                        <asp:ListItem Text="федеральный" Value="a"></asp:ListItem>
                                                        <asp:ListItem Text="региональный" Value="b"></asp:ListItem>
                                                        <asp:ListItem Text="муниципальный" Value="с"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </fieldset>
                                            </td>
                                        </tr>
                                    </table>
                                    <table>
                                        <tr>
                                            <td>
                                                Код ОКПО архива
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="OKPO" runat="server" Width="100px" Enabled="False" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Наименование архива
                                            </td>
                                            <td>
                                                <eq3:eqDocument ID="ID_ARCHIVE" runat="server" DocTypeURL="GR_ARCHIVE.ascx" ListDocMode="NewWindowAndSelect"
                                                    LoadDocMode="ListDocAndSelect" ShowingFields="NAME" ShowingFieldsFormat="{0}"
                                                    KeyName="ID" Width="500px" OnRequesting="Archive_Requesting" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Федеральный округ / Субъект
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="SUBJECT" runat="server" Width="500px" Enabled="False" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Муниципальное образование
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="MUNICIPALITY" runat="server" Width="500px" />
                                            </td>
                                        </tr>
                                    </table>
                                    <table style="width: 600px;">
                                        <colgroup>
                                            <col align="right" style="width: 30%;" />
                                            <col align="left" style="width: 20%;" />
                                            <col align="right" style="width: 30%;" />
                                            <col align="left" />
                                        </colgroup>
                                        <tr>
                                            <td>
                                                Фонд Название
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="FUND_NAME" runat="server" Width="70px" />
                                            </td>
                                            <td>
                                                №
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="FUND_NUM_1" runat="server" Width="70px" />
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="FUND_NUM_2" runat="server" Width="70px" />
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="FUND_NUM_3" runat="server" Width="70px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Опись
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="INVENTORY_NUM_1" runat="server" Width="70px" />
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="INVENTORY_NUM_2" runat="server" Width="70px" />
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="INVENTORY_NUM_3" runat="server" Width="70px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Том
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="INVENTORY_VOLUME" runat="server" Width="70px" />
                                            </td>
                                            <td>
                                                Отделение
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="DEPARTMENT" runat="server" Width="70px" />
                                            </td>
                                            <td>
                                                Рубрика
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="RUBRIC" runat="server" Width="70px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Часть
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="FUND_OLD_PART" runat="server" Width="70px" />
                                            </td>
                                            <td>
                                                Номер
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="FUND_OLD_NUMBER" runat="server" Width="70px" />
                                            </td>
                                            <td>
                                                Дело
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="FUND_OLD_FILE" runat="server" Width="70px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Ед.хр.
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="UNIT_NUM_1" runat="server" Width="70px" />
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="UNIT_NUM_2" runat="server" Width="70px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Ед.уч.
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="UNIT_REG_NUM_1" runat="server" Width="70px" />
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="UNIT_REG_NUM_2" runat="server" Width="70px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Листы
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="SHEETS" runat="server" Width="70px" />
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                        </eq:eqSpecification>
                    </td>
                </tr>
            </table>
           
            <table>
          <tr>
        <td>
           4. Служебная информация 
        </td>
    </tr> 
    </table>
             
           
            <table>
                <tr>
                    
                    <td>
                  
                        <eq:eqSpecification ID="GR_tblOFFICIAL_INFO" runat="server">
                            <eq:eqSpecificationColumn>
                                <ItemTemplate>
                                    <table>
                                       
                                        <tr>
                                            <td>
                                                Кем представлен документ
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="ORGANIZATION" runat="server" Width="500px" />
                                            </td>
                                        </tr>
                                    </table>
                                    <table>
                                        <tr>
                                            <td>
                                                Протокол ЭПК №
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="PROTOCOL_NUM" runat="server" Width="70px" />
                                            </td>
                                            <td>
                                                Протокол ЦЭПК Росархива №
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="PROTOCOL_ROSARCH_NUM" runat="server" Width="70px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Протокол ЭПК дата
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="PROTOCOL_DATE" runat="server" Width="70px" />
                                                <aj:CalendarExtender runat="server" TargetControlID="PROTOCOL_DATE" />
                                            </td>
                                            <td>
                                                Протокол ЦЭПК Росархива дата
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="PROTOCOL_ROSARCH_DATE" runat="server" Width="70px" />
                                                <aj:CalendarExtender runat="server" TargetControlID="PROTOCOL_ROSARCH_DATE" />
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                        </eq:eqSpecification>
                    </td>
                </tr>
            </table>
           
            
             <table>
          <tr>
         <td>
            Доступен для интернет-поиска
        </td>
        <td>
            <asp:CheckBox ID="PUBLIC" runat="server" Checked="true" />
        </td>
    </tr>
    </table>
    
     <table style="width: 97%; margin-bottom: 4px;">
    <tr>
        
        <td style="text-align: left;">
           
            <asp:HyperLink ID="HyperLink_DOC_IMAGE" runat="server" Text="Ссылки на файлы электронного образа" Target="_blank"
                NavigateUrl="~/Default.aspx?DocTypeURL=GR_DOC_IMAGE.ascx" Style="font-weight: bold;
                padding: 2px;" />
        </td>
    </tr>
     </table>
    
        
           <table visible="false">
          <tr>
        
    
        <td>
            
                <eq:eqTextBox ID="ID_OWNER" runat="server" Width="70px" visible="false"/>
        </td>
         </tr>
    </table>
    
    <table visible="false">
                
                                <tr>
                                  
                                    <td>
                                        <eq:eqTextBox ID="EXACT_DATE" runat="server" Width="70px" visible="false"/>
                                        
                                           
                                    </td>
                                    
                                    <td>
                                        <eq:eqTextBox ID="EXACT_DATE1" runat="server" Width="70px" visible="false"/>
                                       
                                    </td>
                                </tr>
                                <tr>
                                    
                                    <td>
                                        <eq:eqTextBox ID="YEAR" runat="server" Width="40px" visible="false"/>
                                    </td>
                                   
                                    <td>
                                        <eq:eqTextBox ID="YEAR1" runat="server" Width="40px" visible="false"/>
                                    </td>
                                </tr>
                                <tr>
                                   
                                    <td>
                                        <eq:eqTextBox ID="DECADE" runat="server" Width="20px" visible="false"/>
                                    </td>
                                   
                                    <td>
                                        <eq:eqTextBox ID="DECADE1" runat="server" Width="20px" visible="false"/>
                                    </td>
                                </tr>
                                <tr>
                                    
                                    
                                    <td>
                                        <asp:DropDownList ID="ID_CENTURY_PART1" runat="server" Width="150px" visible="false">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    
                                   
                                    <td>
                                        <asp:DropDownList ID="ID_CENTURY1" runat="server" Width="150px" visible="false">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                           
            </table>
            <table visible="false">
                <tr>
                   
                    <td>
                        <eq:eqTextBox ID="DATE_FROM_WC" runat="server" Width="100px" visible="false"/>
                    </td>
                </tr>
                <tr>
                    
                    <td>
                        <asp:DropDownList ID="ID_STYLE" runat="server" Width="100px" visible="false">
                        </asp:DropDownList>
                    </td>
                    
                    <td>
                        <eq:eqTextBox ID="COPY_EXACT_DATE" runat="server" Width="100px" visible="false"/>
                    </td>
                </tr>
                <tr>
                    
                    <td>
                        <asp:DropDownList ID="ID_COPY_EXACT_CENT_PART" runat="server" Width="150px" visible="false">
                        </asp:DropDownList>
                    </td>
                   
                    <td>
                        <asp:DropDownList ID="ID_COPY_CENTURY" runat="server" Width="150px" visible="false">
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>