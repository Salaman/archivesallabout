﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="FC_ExcelReports.ascx.vb"
    Inherits="WebApplication.FC_ExcelReports" %>
<%@ Register TagPrefix="qq" Namespace="WebApplication.WebUI" Assembly="WebApplication" %>
<div class="LogicBlockCaption">
    Паспорт
</div>
<table width="100%" class="LogicBlockTable">
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_ArchiveUnion" Text="Сеть архивов" GroupName="R"
                OnCheckedChanged="Rep_Changed" AutoPostBack="true" Checked="false" Enabled="false" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_PassportArchive" Text="Паспорт архива" GroupName="R"
                OnCheckedChanged="Rep_Changed" AutoPostBack="true" Checked="true" Enabled="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_ConsolidatedPassportArchive" Text="Сводный паспорт архива"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" Checked="false"
                Enabled="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_ArchiveDynamics_Abs" Text="Динамика паспорта архива (абсолютная)"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" Checked="false"
                Enabled="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_ArchiveDynamics_Pct" Text="Динамика  паспорта архива (в процентах)"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" Checked="false"
                Enabled="true" />
        </td>
    </tr>
</table>
<div class="LogicBlockCaption">
    Статистика по архивам
</div>
<table width="100%" class="LogicBlockTable">
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_Income" Text="Поступления" GroupName="R"
                OnCheckedChanged="Rep_Changed" AutoPostBack="true" Checked="false" Enabled="false" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_Property" Text="Собственность" GroupName="R"
                OnCheckedChanged="Rep_Changed" AutoPostBack="true" Checked="false" Enabled="false" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_StorageCondition" Text="Условия хранения"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" Checked="false"
                Enabled="false" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_SignalingEquipment" Text="Оснащенность сигнализацией"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" Checked="false"
                Enabled="false" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_UndetectedCases" Text="Необнаруженные дела"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" Checked="false"
                Enabled="false" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_PhysicalConditionPaperDoc" Text="Физическое состояние документов на бумажной основе"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" Checked="false"
                Enabled="false" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_PhysicalConditionSpecialDoc" Text="Физическое состояние спецдокументации"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" Checked="false"
                Enabled="false" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_IFUFPaperDoc" Text="СФ и ФП документов на бумажной основе"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" Checked="false"
                Enabled="false" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_IFUFSpecialDoc" Text="СФ и ФП спецдокументации"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" Checked="false"
                Enabled="false" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_SAArchiveFund" Text="НСА к фондам архива"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" Checked="false"
                Enabled="false" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_SAGroupArchiveFund" Text="НСА к фондам группы архивов"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" Checked="false"
                Enabled="false" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_CompletenessInventory" Text="Комплектность описей"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" Checked="false"
                Enabled="false" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_NonInventoryDoc" Text="Неописанные документы"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" Checked="false"
                Enabled="false" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_CatalogizationPaperDoc" Text="Каталогизация документов на бумажной основе"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" Checked="false"
                Enabled="false" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_CatalogizationSpecialDoc" Text="Каталогизация спецдокументации"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" Checked="false"
                Enabled="false" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_Database" Text="Базы данных" GroupName="R"
                OnCheckedChanged="Rep_Changed" AutoPostBack="true" Checked="false" Enabled="false" />
        </td>
    </tr>
</table>
<div class="LogicBlockCaption">
    Перечни фондов
</div>
<table width="100%" class="LogicBlockTable">
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_AvailableFund" Text="Фонды в наличии" GroupName="R"
                OnCheckedChanged="Rep_Changed" AutoPostBack="true" Checked="false" Enabled="false" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_FederalPropertyFund" Text="Фонды Федеральной собственности"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" Checked="false"
                Enabled="false" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_SubjectPropertyFund" Text="Фонды собственности субъектов Федерации"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" Checked="false"
                Enabled="false" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_MunicipalityPropertyFund" Text="Фонды муниципальной собственности"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" Checked="false"
                Enabled="false" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_IndividualsPropertyFund" Text="Фонды собственности физических лиц"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" Checked="false"
                Enabled="false" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_LegalPropertyFund" Text="Фонды собственности юридических лиц"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" Checked="false"
                Enabled="false" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_JointPropertyFund" Text="Фонды 'совместной' собственности"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" Checked="false"
                Enabled="false" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_IsWantedFund" Text="Фонды, находящиеся в розыске"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" Checked="false"
                Enabled="false" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_SecretFund" Text="Секретные фонды" GroupName="R"
                OnCheckedChanged="Rep_Changed" AutoPostBack="true" Checked="false" Enabled="false" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_PartiallySecretFund" Text="Частично секретные фонды"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" Checked="false"
                Enabled="false" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_IncomeDocFund" Text="Фонды, по которым поступили документы за указываемый период"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" Checked="false"
                Enabled="false" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_RetiredDocFund" Text="Фонды, по которым выбыли документы за указываемый период"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" Checked="false"
                Enabled="false" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="Rep_IncomeFund" Text="Поступившие за указываемый период фонды"
                GroupName="R" OnCheckedChanged="Rep_Changed" AutoPostBack="true" Checked="false"
                Enabled="false" />
        </td>
    </tr>
</table>
<asp:Panel ID="Panel_Properties" runat="server" Width="350" BorderWidth="1" BackColor="White">
    <div class="LogicBlockCaption">
        Параметры отчета
    </div>
    <asp:Panel ID="Panel_ArchiveLevel" runat="server">
        <table width="100%" cellspacing="4">
            <colgroup>
                <col width="510px" />
            </colgroup>
            <tr>
                <td>
                    по уровням архива
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="Prop_ArchiveLevel" AutoPostBack="true">
                        <asp:ListItem Text="все" Value="0" Selected="True" />
                        <asp:ListItem Text="все региональные" Value="1" />
                        <asp:ListItem Text="все муниципальные" Value="2" />
                        <asp:ListItem Text="произвольно муниципальные" Value="3" />
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="text-align: right;">
                    <asp:Button ID="GetArchives" runat="server" Text="Выбрать архив(ы)" Visible="false" />
                </td>
            </tr>
            <%--<tr>
                <td style="text-align: right;">
                    <asp:Button ID="GetArchives2" runat="server" Text="Выбрать архив(ы) 2" />
                </td>
            </tr>--%>
        </table>
    </asp:Panel>
    <asp:Panel ID="Panel_Arch" runat="server">
        <table width="100%" cellspacing="4">
            <colgroup>
                <col width="110px" />
            </colgroup>
            <tr>
                <td>
                    Выбранные архивы
                </td>
                <td>
                    <eq:eqTextBox ID="Prop_ID_ARCHIVE_MULTI" runat="server" Width="350" Text="SELECT ISN_ARCHIVE FROM tblARCHIVE WHERE DELETED=0" visible="false"/>
                </td>
            </tr>
            <tr>
                <td>
                    <eq:eqTextBox ID="Prop_NAME_ARCHIVE" runat="server" Width="320" Text="все"
                        TextMode="MultiLine" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="Panel_ISN_ARCHIVE" runat="server">
        <table width="100%" cellspacing="4">
            <colgroup>
                <col width="110px" />
            </colgroup>
            <tr>
                <td>
                    Архив
                </td>
                <td>
                    <qq:eqDocument ID="Prop_ID_ARCHIVE" runat="server" DocTypeURL="ARCHIVE.ascx" ListDocMode="NewWindowAndSelect"
                        LoadDocMode="ListDocAndSelect" ShowingFields="NAME" ShowingFieldsFormat="{0}"
                        KeyName="ISN_ARCHIVE" Width="500px" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="Panel_Year" runat="server">
        <table width="100%" cellspacing="4">
            <colgroup>
                <col width="110px" />
            </colgroup>
            <tr>
                <td>
                    Год
                </td>
                <td>
                    <eq:eqTextBox ID="Prop_Year" runat="server" Width="60" Text="2000" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="Panel_Year2" runat="server">
        <table width="100%" cellspacing="4">
            <colgroup>
                <col width="110px" />
            </colgroup>
            <tr>
                <td>
                    Год с
                </td>
                <td>
                    <eq:eqTextBox ID="Prop_Year1" runat="server" Width="60" Text="1800" />
                </td>
                <td>
                    по
                </td>
                <td>
                    <eq:eqTextBox ID="Prop_Year2" runat="server" Width="60" Text="2100" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Panel>
<aj:AlwaysVisibleControlExtender ID="Panel_Properties_Ex" runat="server" TargetControlID="Panel_Properties"
    HorizontalSide="Right" VerticalSide="Top" HorizontalOffset="120" VerticalOffset="240" />
<span id="report_tag"></span>