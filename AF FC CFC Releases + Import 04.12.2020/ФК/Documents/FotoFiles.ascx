﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="FotoFiles.ascx.vb" Inherits="WebApplication.FotoFiles" %>
<table style="width: 97%; margin-bottom: 4px;">
    <tr>
        <td>
            <eq2:ArchiveCrumbs ID="Crumbs" runat="server" Level="UNIT" />
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            <eq:eqTextBox ID= "File_DocID" runat="server" />
        </td>
        <td>
            <eq2:RefFile ID="ID" runat="server" Mode="FileSystem" OnRequesting="RequestFotoFile"/>
        </td>
    </tr>
</table>