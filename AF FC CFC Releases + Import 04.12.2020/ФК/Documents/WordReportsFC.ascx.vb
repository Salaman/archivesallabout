﻿Partial Public Class WordReportsFC
    Inherits BaseDoc

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            tb1.Text = DateTime.Now.Year
            tb8.Text = DateTime.Now.Year
            tb9.Text = DateTime.Now.Year
        End If
        If Me.ISN_ARCHIVE.Items.Count = 0 Then
            Dim tblA = AppSettings.GetDataTable("select ISN_ARCHIVE, coalesce( NAME_SHORT, name, CODE) NAME_SHORT from tblARCHIVE where Deleted=0 order by 2", "tblARCHIVE")
            Me.ISN_ARCHIVE.DataTextField = "NAME_SHORT"
            Me.ISN_ARCHIVE.DataValueField = "ISN_ARCHIVE"
            Me.ISN_ARCHIVE.DataSource = tblA
            Me.ISN_ARCHIVE.DataBind()
        End If
    End Sub

    Protected Sub btMakeReport_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btMakeReport.Click
        Try
            Dim ReportGenerator As New WordReportGenerator.WordReportGeneratorObsolete(AppSettings.Cnn)
            If rb1.Checked And IsNumeric(tb1.Text) Then
                ReportGenerator.YearNo = tb1.Text
                Dim stream = ReportGenerator.FillReport_SetArchivov()
                BasePage.AttachFile("Сеть_архивов_" & Now.ToString("yyyy.MM.dd_HH.mm.ss") & ".doc", stream.ToArray)
            ElseIf rb2.Checked Then
                Dim stream = ReportGenerator.FillReport_PassportArchive(ISN_ARCHIVE.SelectedValue)
                BasePage.AttachFile("Паспорт_архива_" & Now.ToString("yyyy.MM.dd_HH.mm.ss") & ".doc", stream.ToArray)
                'ElseIf rb3.Checked Then
                'ElseIf rb4.Checked Then
                'ElseIf rb5.Checked Then
            ElseIf rb6.Checked Then
                Dim stream = ReportGenerator.FillReport_Postuplenie()
                BasePage.AttachFile("Поступление_" & Now.ToString("yyyy.MM.dd_HH.mm.ss") & ".doc", stream.ToArray)
            ElseIf rb7.Checked Then
                Dim stream = ReportGenerator.FillReport_Sobsvennost()
                BasePage.AttachFile("Собственность_" & Now.ToString("yyyy.MM.dd_HH.mm.ss") & ".doc", stream.ToArray)
            ElseIf rb8.Checked And IsNumeric(tb8.Text) Then
                ReportGenerator.YearNo = tb8.Text
                Dim stream = ReportGenerator.FillReport_UsloviyaHraneniya()
                BasePage.AttachFile("Условия_хранения_" & Now.ToString("yyyy.MM.dd_HH.mm.ss") & ".doc", stream.ToArray)
            ElseIf rb9.Checked And IsNumeric(tb9.Text) Then
                ReportGenerator.YearNo = tb9.Text
                Dim stream = ReportGenerator.FillReport_OsnashennostSign()
                BasePage.AttachFile("Оснащенность_сигнализацией_" & Now.ToString("yyyy.MM.dd_HH.mm.ss") & ".doc", stream.ToArray)
            ElseIf rb10.Checked Then
                Dim stream = ReportGenerator.FillReport_NeobnaruzhDela()
                BasePage.AttachFile("Необнаруженные_дела_" & Now.ToString("yyyy.MM.dd_HH.mm.ss") & ".doc", stream.ToArray)
            ElseIf rb11.Checked Then
                Dim stream = ReportGenerator.FillReport_FizSostBo()
                BasePage.AttachFile("_Физическое_состояние_документов_на_бумажной_основе_" & Now.ToString("yyyy.MM.dd_HH.mm.ss") & ".doc", stream.ToArray)
            ElseIf rb12.Checked Then
                Dim stream = ReportGenerator.FillReport_FizSpec()
                BasePage.AttachFile("Физическое_состояние_спецдокументации_" & Now.ToString("yyyy.MM.dd_HH.mm.ss") & ".doc", stream.ToArray)
            ElseIf rb13.Checked Then
                Dim stream = ReportGenerator.FillReport_SfFpBo()
                BasePage.AttachFile("СФ_и_ФП_документов_на_бумажной_основе_" & Now.ToString("yyyy.MM.dd_HH.mm.ss") & ".doc", stream.ToArray)
            ElseIf rb14.Checked Then
                Dim stream = ReportGenerator.FillReport_SfFpSpec()
                BasePage.AttachFile("СФ_и_ФП_спецдокументации_" & Now.ToString("yyyy.MM.dd_HH.mm.ss") & ".doc", stream.ToArray)
                'ElseIf rb15.Checked Then
                'ElseIf rb16.Checked Then
            ElseIf rb17.Checked Then
                Dim stream = ReportGenerator.FillReport_KomplektnostOpisey()
                BasePage.AttachFile("Комплектность_описей_" & Now.ToString("yyyy.MM.dd_HH.mm.ss") & ".doc", stream.ToArray)
            ElseIf rb18.Checked Then
                Dim stream = ReportGenerator.FillReport_NeopisDok()
                BasePage.AttachFile("Неописанные_документы_" & Now.ToString("yyyy.MM.dd_HH.mm.ss") & ".doc", stream.ToArray)
            ElseIf rb19.Checked Then
                Dim stream = ReportGenerator.FillReport_KatalogBo()
                BasePage.AttachFile("Каталогизация_документов_на_бумажной_основе_" & Now.ToString("yyyy.MM.dd_HH.mm.ss") & ".doc", stream.ToArray)
            ElseIf rb20.Checked Then
                Dim stream = ReportGenerator.FillReport_KatalogSpec()
                BasePage.AttachFile("Каталогизация_спецдокументации_" & Now.ToString("yyyy.MM.dd_HH.mm.ss") & ".doc", stream.ToArray)
            ElseIf rb21.Checked Then
                Dim stream = ReportGenerator.FillReport_BazyDannyh()
                BasePage.AttachFile("Базы_данных_" & Now.ToString("yyyy.MM.dd_HH.mm.ss") & ".doc", stream.ToArray)
            ElseIf rb22.Checked Then
                Dim stream = ReportGenerator.FillReport_FondyVNalichii()
                BasePage.AttachFile("Фонды_в_наличии_" & Now.ToString("yyyy.MM.dd_HH.mm.ss") & ".doc", stream.ToArray)
            ElseIf rb23.Checked Then
                Dim stream = ReportGenerator.FillReport_FondyFed()
                BasePage.AttachFile("Фонды_Федеральной_собственности_" & Now.ToString("yyyy.MM.dd_HH.mm.ss") & ".doc", stream.ToArray)
            ElseIf rb24.Checked Then
                Dim stream = ReportGenerator.FillReport_FondySubFed()
                BasePage.AttachFile("Фонды_собственности_субъектов_Федерации_" & Now.ToString("yyyy.MM.dd_HH.mm.ss") & ".doc", stream.ToArray)
            ElseIf rb25.Checked Then
                Dim stream = ReportGenerator.FillReport_FondyMunic()
                BasePage.AttachFile("Фонды_муниципальной_собственности_" & Now.ToString("yyyy.MM.dd_HH.mm.ss") & ".doc", stream.ToArray)
            ElseIf rb26.Checked Then
                Dim stream = ReportGenerator.FillReport_FondyFl()
                BasePage.AttachFile("Фонды_собственности_физических_лиц_" & Now.ToString("yyyy.MM.dd_HH.mm.ss") & ".doc", stream.ToArray)
            ElseIf rb27.Checked Then
                Dim stream = ReportGenerator.FillReport_FondyUl()
                BasePage.AttachFile("Фонды_собственности_юридических_лиц_" & Now.ToString("yyyy.MM.dd_HH.mm.ss") & ".doc", stream.ToArray)
            ElseIf rb28.Checked Then
                Dim stream = ReportGenerator.FillReport_FondySovmestn()
                BasePage.AttachFile("Фонды_«совместной»_собственности_" & Now.ToString("yyyy.MM.dd_HH.mm.ss") & ".doc", stream.ToArray)
            ElseIf rb29.Checked Then
                Dim stream = ReportGenerator.FillReport_FondRozysk()
                BasePage.AttachFile("Фонды,_находящиеся_в_розыске_" & Now.ToString("yyyy.MM.dd_HH.mm.ss") & ".doc", stream.ToArray)
            ElseIf rb30.Checked Then
                Dim stream = ReportGenerator.FillReport_FondSecret()
                BasePage.AttachFile("Секретные_фонды_" & Now.ToString("yyyy.MM.dd_HH.mm.ss") & ".doc", stream.ToArray)
            ElseIf rb31.Checked Then
                Dim stream = ReportGenerator.FillReport_FondChastichnoSecret()
                BasePage.AttachFile("Частично_секретные_фонды_" & Now.ToString("yyyy.MM.dd_HH.mm.ss") & ".doc", stream.ToArray)
                'ElseIf rb32.Checked Then
                'ElseIf rb33.Checked Then
                'ElseIf rb34.Checked Then
                'ElseIf rb35.Checked Then
                'ElseIf rb36.Checked Then
                'ElseIf rb37.Checked Then
                'ElseIf rb38.Checked Then
                'ElseIf rb39.Checked Then
            End If
        Catch ex As Exception
            Me.AddMessage(ex.Message)
        End Try
    End Sub

End Class