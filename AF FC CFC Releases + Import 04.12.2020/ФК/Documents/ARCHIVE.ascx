﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ARCHIVE.ascx.vb" Inherits="WebApplication.ARCHIVE" %>
<table style="width: 97%; margin-bottom: 4px;">
    <tr>
        <td>
            <eq2:ArchiveCrumbs ID="Crumbs" runat="server" Level="DEP" />
            <eq2:ArchiveCrumbs ID="HiddenCrumbs" runat="server" Level="ARCHIVE" Visible="false" />
        </td>
        <td style="text-align: right;">
            Содержимое:
            <asp:HyperLink ID="HyperLinkFUNDs" runat="server" Text="Фонды" Target="_blank" NavigateUrl="~/Default.aspx?DocTypeURL=FUND.ascx&ListDoc=True"
                Style="font-weight: bold; padding: 2px;" />
            <asp:HyperLink ID="HyperLinkSTRs" runat="server" Text="Разделы описи" Target="_blank"
                NavigateUrl="~/Default.aspx?DocTypeURL=INVENTORYSTRUCTURE.ascx&ListDoc=True"
                Style="font-weight: bold; padding: 2px;" />
        </td>
    </tr>
</table>
<div style="border: solid 1px black; padding: 5px;">
    <table style="width: 700px;">
        <colgroup>
            <col style="width: 120px;" />
            <col />
            <col />
            <col />
        </colgroup>
        <tr>
            <td>
                Код
            </td>
            <td colspan="3">
                <eq:eqTextBox ID="CODE" runat="server" Width="200px" />
            </td>
        </tr>
        <tr>
            <td>
                Уровень архива*
            </td>
            <td>
                <asp:DropDownList ID="ARCHIVE_LEVEL" runat="server">
                    <asp:ListItem Text="федеральный" Value="a" />
                    <asp:ListItem Text="региональный" Value="b" />
                    <asp:ListItem Text="муниципальный" Value="c" />
                </asp:DropDownList>
            </td>
            <td>
                Тип архива*
            </td>
            <td>
                <asp:DropDownList ID="ARCHIVE_TYPE" runat="server">
                    <asp:ListItem Text="архив" Value="a" />
                    <asp:ListItem Text="архив л/с" Value="b" />
                </asp:DropDownList>
            </td>
        </tr>
    </table>
    <table style="width: 700px;">
        <colgroup>
            <col valign="top" style="width: 120px;" />
            <col />
        </colgroup>
        <tr>
            <td>
                Сокращенное название*
            </td>
            <td>
                <eq:eqTextBox ID="NAME_SHORT" runat="server" TextMode="MultiLine" Rows="2" />
            </td>
        </tr>
        <tr>
            <td>
                Полное название*
            </td>
            <td>
                <eq:eqTextBox ID="NAME" runat="server" TextMode="MultiLine" Rows="4" />
            </td>
        </tr>
        <tr>
            <td>
                Федеральный округ / Субъект
            </td>
            <td>
                <%--СПРАВОЧНИК--%>
                <eq2:eqDocument ID="ISN_SUBJECT" runat="server" DocTypeURL="SUBJECT_CL.ascx" ListDocMode="NewWindowAndSelect"
                    LoadDocMode="NewWindowAndSelect" ShowingFields="NAME" ShowingFieldsFormat="{0}" />
            </td>
        </tr>
        <tr>
            <td>
                Адрес
            </td>
            <td>
                <eq:eqTextBox ID="ADDRESS" runat="server" TextMode="MultiLine" Rows="4" />
            </td>
        </tr>
       <%-- <tr>
            <td>
                Адрес
            </td>
            <td>
                <eq:eqSpecification ID="tblARCHIVE_ADDRESS" runat="server" ShowNumering="false" Visible="true">
                    <eq:eqSpecificationColumn>
                        <ItemTemplate>
                            <table>
                                <tr>
                                    <td>
                                        <eq:eqTextBox ID="ADDRESS" runat="server" width=500px TextMode="MultiLine"
                                            Visible="true" />
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                    </eq:eqSpecificationColumn>
                </eq:eqSpecification>
            </td>
        </tr>--%>
        <tr>
            <td>
                Название и адрес органа управления
            </td>
            <td>
                <eq:eqTextBox ID="AUTHORITY" runat="server" TextMode="MultiLine" Rows="4" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <fieldset>
                    <legend>Файлы</legend>
                    <eq:eqSpecification ID="vREF_FILE_ARCHIVE" runat="server" Width="100%" AllowInsert="false">
                        <eq:eqSpecificationColumn HeaderText="Описание">
                            <ItemTemplate>
                                <asp:TextBox ID="NAME" runat="server" Width="99%" />
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                        <eq:eqSpecificationColumn HeaderText="Файл">
                            <ItemTemplate>
                                <eq2:RefFile ID="ID" runat="server" Mode="FileSystem" OnRequesting="RefFile_Requesting" />
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                    </eq:eqSpecification>
                    <table>
                        <tr>
                            <td>
                                Добавить файл:
                            </td>
                            <td>
                                <eq2:RefFileUpload ID="RefFileUploadFile" runat="server" Mode="FileSystem" />
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <fieldset>
                    <legend>Паспорт</legend>
                    <%--СПЕЦИФИКАЦИЯ dbo.ARCHIVE_PASSPORT--%>
                    <eq:eqSpecification ID="vARCHIVE_PASSPORT_Spec" runat="server" AllowInsert="false"
                        AllowDelete="false" AllowReorder="false" ShowNumering="false" Width="100%">
                        <eq:eqSpecificationColumn HeaderText="Год">
                            <ItemTemplate>
                                <eq:eqDocument ID="ID" runat="server" DocTypeURL="ARCHIVE_PASSPORT.ascx" ListDocMode="None"
                                    LoadDocMode="NewWindow" ShowingFields="PASS_YEAR" ShowingFieldsFormat="{0}" />
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                    </eq:eqSpecification>
                    <asp:HyperLink ID="HyperLinkPassportAdd" runat="server" Target="_blank">Добавить паспорт</asp:HyperLink>
                </fieldset>
            </td>
        </tr>
    </table>
</div>
