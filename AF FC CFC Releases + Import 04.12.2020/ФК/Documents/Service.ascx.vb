﻿Imports System
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient

Imports System.Web.Configuration
Imports MiscLib.db

Imports WebApplication.Update

Partial Public Class Service
    Inherits BaseDoc
    Friend Update

#Region "События"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BackupDatabaseNameFill()
        End If
    End Sub

    Private Sub vServiceLog_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs) Handles vServiceLog.RowCommand
        If e.CommandName = "Restore" Then
            Dim ctl = DirectCast(sender, eqSpecificationRow)
            Dim row = DataSetSpecificationDataRow(vServiceLog.ID, ctl.RowGuid)
            BackupRestore(row("BackupSetGUID"), row("BackupPath"))
        End If
    End Sub

#End Region

#Region "Вспомогательные методы"

    Private Function BackupDatabaseName() As String
        Dim cmd As New SqlCommand("SELECT db_name()")
        Return AppSettings.ExecCommandScalar(cmd)
    End Function

    Private Sub BackupDatabaseNameFill()
        DBName.Text = BackupDatabaseName()
    End Sub

    Private Function BackupDatabasePath() As String
        Dim cmd As New SqlCommand("SELECT physical_name FROM sys.database_files WHERE file_id = 1")
        Dim result = AppSettings.ExecCommandScalar(cmd)
        result = Path.Combine(Path.GetFullPath(Path.Combine(result, "..\..")), String.Format("Backup\{0}.bak", BackupDatabaseName))
        Return result
    End Function

    Private Sub BackupDatabasePathFill()
        If BackupPath.Text = "" Then
            BackupPath.Text = BackupDatabasePath()
        End If
    End Sub

    Private Function BackupDatabasePathValid() As Boolean
        Try
            Dim bakPath = Path.GetFullPath(BackupPath.Text)
            If Path.GetFileNameWithoutExtension(bakPath) = "" Or Path.GetExtension(bakPath) = "" Then
                Throw New Exception("Введите имя файла для резервной копии.")
            End If
            If Path.GetDirectoryName(bakPath).ToCharArray.Intersect(Path.GetInvalidPathChars).Count > 0 Then
                Throw New Exception("Неверный путь для файла резервной копии.")
            End If
            If Path.GetFileName(bakPath).ToCharArray.Intersect(Path.GetInvalidFileNameChars).Count > 0 Then
                Throw New Exception("Неверное имя файла резервной копии.")
            End If
        Catch ex As Exception
            BasePage.AddMessage(ex.Message)
            Return False
        End Try
        Return True
    End Function

    Private Function BackupDatabasePathCreate() As Boolean
        Try
            Dim bakPath = Path.GetDirectoryName(BackupPath.Text)
            If Not Directory.Exists(bakPath) Then
                Try
                    Directory.CreateDirectory(bakPath)
                Catch ex As Exception
                    Throw New Exception("Невозможно создать указанную директорию.")
                End Try
            End If
        Catch ex As Exception
            BasePage.AddMessage(ex.Message)
            Return False
        End Try
        Return True
    End Function

    Private Function BackupPosition(ByVal BackupSetID As Guid, ByVal BackupPath As String) As Integer
        Dim SQL As New StringBuilder
        SQL.AppendFormat("RESTORE HEADERONLY").AppendLine()
        SQL.AppendFormat("FROM DISK = N'{0}'", BackupPath).AppendLine()
        SQL.AppendFormat("WITH NOUNLOAD").AppendLine()

        Dim cmd As New SqlCommand With {.CommandTimeout = 3600}
        cmd.CommandText = SQL.ToString

        Dim dt = AppSettings.GetDataTable(cmd)
        Return dt.AsEnumerable.Single(Function(F) F("BackupSetGUID") = BackupSetID)("Position")
    End Function

    Private Function BackupDataWrite() As Boolean
        Dim SQL As New StringBuilder
        SQL.AppendFormat("RESTORE HEADERONLY").AppendLine()
        SQL.AppendFormat("FROM DISK = N'{0}'", BackupPath.Text).AppendLine()
        SQL.AppendFormat("WITH NOUNLOAD").AppendLine()

        Dim cmd As New SqlCommand With {.CommandTimeout = 3600}
        cmd.CommandText = SQL.ToString

        Dim dt = AppSettings.GetDataTable(cmd)
        Dim v = dt.DefaultView
        v.Sort = "Position DESC" ' Последний бэкап

        If Overwrite.Checked Then
            SQL.Remove(0, SQL.Length)
            SQL.AppendLine("DELETE tblServiceLog")

            cmd.CommandText = SQL.ToString
            AppSettings.ExecCommand(cmd)
        End If

        SQL.Remove(0, SQL.Length)
        SQL.AppendLine("INSERT tblServiceLog (ID, DocID, OwnerID, CreationDateTime, RowID, BackupSetGUID, BackupPath)")
        SQL.AppendLine("VALUES (newid(), @DocID, @OwnerID, @CreationDateTime, 0, @BackupSetGUID, @BackupPath)")

        cmd.CommandText = SQL.ToString
        cmd.Parameters.AddWithValue("@DocID", DocID)
        cmd.Parameters.AddWithValue("@OwnerID", AppSettings.UserInfo.UserID)
        cmd.Parameters.AddWithValue("@CreationDateTime", v(0)("BackupFinishDate"))
        cmd.Parameters.AddWithValue("@BackupSetGUID", v(0)("BackupSetGUID"))
        cmd.Parameters.AddWithValue("@BackupPath", BackupPath.Text)
        Dim result = AppSettings.ExecCommand(cmd)
        Return (result = 1)
    End Function

    Private Function BackupDataSave() As DataSet
        Dim result As New DataSet
        Using ad As New SqlDataAdapter("SELECT * FROM tblService", AppSettings.Cnn)
            Using cb As New SqlCommandBuilder(ad)
                ad.Fill(result, "tblService")
            End Using
        End Using
        Using ad As New SqlDataAdapter("SELECT * FROM tblServiceLog", AppSettings.Cnn)
            Using cb As New SqlCommandBuilder(ad)
                ad.Fill(result, "tblServiceLog")
            End Using
        End Using
        Return result
    End Function

    Private Function BackupDataOpen(ByVal Data As DataSet) As DataSet
        Dim result As New DataSet
        Using ad As New SqlDataAdapter("SELECT * FROM tblService", AppSettings.Cnn)
            Using cb As New SqlCommandBuilder(ad)
                Dim cmd As New SqlCommand("DELETE tblService")
                AppSettings.ExecCommand(cmd)

                cmd = cb.GetInsertCommand(True)
                For Each row As DataRow In Data.Tables("tblService").Rows
                    For Each p As SqlParameter In cmd.Parameters
                        p.Value = row(p.ParameterName.TrimStart("@"))
                    Next
                    AppSettings.ExecCommand(cmd)
                Next
            End Using
        End Using
        Using ad As New SqlDataAdapter("SELECT * FROM tblServiceLog", AppSettings.Cnn)
            Using cb As New SqlCommandBuilder(ad)
                Dim cmd As New SqlCommand("DELETE tblServiceLog")
                AppSettings.ExecCommand(cmd)

                cmd = cb.GetInsertCommand(True)
                For Each row As DataRow In Data.Tables("tblServiceLog").Rows
                    For Each p As SqlParameter In cmd.Parameters
                        p.Value = row(p.ParameterName.TrimStart("@"))
                    Next
                    AppSettings.ExecCommand(cmd)
                Next
            End Using
        End Using
        Return result
    End Function

    Private Sub Clear(ByVal cnn As SqlConnection)
        Dim SQL As New StringBuilder
        SQL.AppendLine("DECLARE @spid int")
        SQL.AppendLine("SELECT @spid = MIN(spid) FROM master.dbo.sysprocesses WHERE dbid = db_id(@dbname)")
        SQL.AppendLine("WHILE @spid IS NOT NULL")
        SQL.AppendLine("BEGIN")
        SQL.AppendLine("  EXECUTE ('KILL ' + @spid)")
        SQL.AppendLine("  SELECT @spid = MIN(spid) FROM master.dbo.sysprocesses WHERE dbid = db_id(@dbname) AND spid > @spid")
        SQL.AppendLine("END")

        Dim cmd As New SqlCommand With {.CommandTimeout = 3600}
        cmd.Connection = cnn
        cmd.CommandText = SQL.ToString
        cmd.Parameters.AddWithValue("@dbname", DBName.Text)
        cmd.ExecuteNonQuery()
    End Sub

    Private Sub BackupRestore(ByVal BackupSetID As Guid, ByVal BackupPath As String)
        If BackupDatabasePathValid() Then
            Dim UserName = AppSettings.UserInfo.UserLogin
            Dim Pos As Integer = BackupPosition(BackupSetID, BackupPath)
            Dim Data = BackupDataSave()

            Try
                AppSettings.Cnn.Close()
                SqlConnection.ClearAllPools()

                Dim cnn As New SqlConnection(My.Settings.DatabaseConnectionString)
                cnn.Open()

                Dim cmd As New SqlCommand
                cmd.Connection = cnn
                cmd.CommandTimeout = 3600

                cmd.CommandText = "USE [master]"
                cmd.ExecuteNonQuery()

                'cmd.CommandText = "ALTER DATABASE [" & DBName.Text & "] SET SINGLE_USER WITH NO_WAIT"
                'Dim a1 = cmd.ExecuteNonQuery()
                Clear(cnn)

                Dim SQL As New StringBuilder
                SQL.AppendFormat("RESTORE DATABASE [{0}]", DBName.Text).AppendLine()
                SQL.AppendFormat("FROM DISK = N'{0}'", BackupPath).AppendLine()
                SQL.AppendFormat("WITH FILE = {0}", Pos).AppendLine()
                SQL.AppendFormat(", REPLACE").AppendLine()

                cmd.CommandText = SQL.ToString
                cmd.ExecuteNonQuery()

                cmd.CommandText = String.Format("USE [{0}]", DBName.Text)
                cmd.ExecuteNonQuery()

                Session("AppSettings") = New eqAppSettings
                AppSettings.OpenSession(cnn, UserName)

                If BackupDataOpen(Data) IsNot Nothing Then
                    NewDoc()
                End If

                BasePage.AddMessage("Резервная копия успешно восстановлена.")
            Catch ex As Exception
                BasePage.AddMessage("Ошибка во время восстановления резервной копии: " & ex.Message)
            End Try
        End If
    End Sub

    Private Sub BackupMake()
        Dim SQL As New StringBuilder
        SQL.AppendFormat("BACKUP DATABASE [{0}]", DBName.Text).AppendLine()
        SQL.AppendFormat("TO DISK = N'{0}'", BackupPath.Text).AppendLine()
        SQL.AppendFormat("WITH NAME = N'{0} - Full Database Backup'", DBName.Text).AppendLine()
        If Overwrite.Checked Then
            SQL.AppendLine(", INIT")
        End If

        Dim cmd = New SqlCommand With {.CommandTimeout = 3600}
        cmd.CommandText = SQL.ToString

        Try
            AppSettings.ExecCommand(cmd)
            If BackupDataWrite() Then
                If SaveDoc() Then
                    Response.Redirect(BasePage.GetDocURL(DocTypeURL, DocID, BasePage.IsModal))
                End If
            End If
        Catch ex As Exception
            BasePage.AddMessage("Ошибка во время создания резервной копии: " & ex.Message)
        End Try
    End Sub

#End Region

#Region "Методы"

    Public Sub Restore()
        If vServiceLog.Rows.Count > 0 Then
            Dim row = DataSetSpecificationDataRow(vServiceLog.ID, vServiceLog.Rows(0).RowGuid)
            BackupRestore(row("BackupSetGUID"), row("BackupPath"))
        Else
            AddMessage("Резервных копий нет.")
        End If
    End Sub

    Public Sub Backup()
        If BackupDatabasePathValid() AndAlso BackupDatabasePathCreate() Then
            BackupMake()
        End If
    End Sub

#End Region

#Region "Документ"

    Public Overrides Function NewDoc() As Boolean
        Dim dt = AppSettings.GetDataTable(New SqlCommand("SELECT ID FROM tblService WHERE Deleted=0"))
        If dt.Rows.Count > 0 Then
            Response.Redirect(BasePage.GetDocURL(DocTypeURL, DirectCast(dt.Rows(0)("ID"), Guid), BasePage.IsModal))
            Return True
        End If
        NewDoc = MyBase.NewDoc()
        If NewDoc Then
            BackupDatabasePathFill()
        End If
    End Function

    Public Overrides Function SaveDoc() As Boolean
        BackupDatabasePathFill()
        If Not BackupDatabasePathValid() Then
            Return False
        End If
        SaveDoc = MyBase.SaveDoc()
    End Function

    Public Overrides Function LoadDoc(ByVal DocID As System.Guid) As Boolean
        LoadDoc = MyBase.LoadDoc(DocID)
        If LoadDoc Then
            BackupDatabasePathFill()
        End If
    End Function

#End Region

    Private Sub DownloadBak_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles DownloadBak.Click
        Dim connString As String = System.Configuration.ConfigurationManager.ConnectionStrings("WebApplication.My.MySettings.DatabaseConnectionString").ConnectionString 'AppSettings.Cnn.ConnectionString
        Dim fname As String = "AF_" + DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss") + ".bak"
        Dim path As String = getTempDir()

        Dim pathToBak As String = System.IO.Path.Combine(path, fname)

        If My.Computer.FileSystem.DirectoryExists(path) Then
            My.Computer.FileSystem.DeleteDirectory(path, FileIO.DeleteDirectoryOption.DeleteAllContents)
        End If
        My.Computer.FileSystem.CreateDirectory(path)

        Dim scsb As SqlConnectionStringBuilder = New SqlConnectionStringBuilder(connString)
        'scsb.InitialCatalog = tbBaseName
        Dim o As dbObject = New dbObject(scsb)
        o.takeBak(pathToBak)
        If ChbDownloadZipBak.Checked Then
            makeGZip(pathToBak)
            My.Computer.FileSystem.DeleteFile(pathToBak)
            sendFile(pathToBak + ".gzip")
        Else
            sendFile(pathToBak)
        End If

    End Sub

    Private Sub sendFile(ByVal fname As String)
        Try
            Dim file As System.IO.FileInfo = New System.IO.FileInfo(fname)
            BasePage.AttachFile(file.Name, My.Computer.FileSystem.ReadAllBytes(file.FullName))
        Catch ex As Exception

        End Try
    End Sub

    Private Function getTempDir() As String
        Dim TempPath As String = System.Configuration.ConfigurationManager.AppSettings.Item("TempPath")
        If TempPath Is Nothing Then
            TempPath = Path.GetTempPath()
        End If
        TempPath = System.IO.Path.Combine(TempPath, "tempBak")
        If Not Directory.Exists(TempPath) Then
            Directory.CreateDirectory(TempPath)
        End If
        Return TempPath
    End Function

    Private Sub makeGZip(ByVal fileName As String)
        Dim fs As New IO.FileStream(fileName, IO.FileMode.Open)
        Dim fd As New IO.FileStream(fileName + ".gzip", IO.FileMode.Create)
        Dim csStrim As New System.IO.Compression.GZipStream(fd, IO.Compression.CompressionMode.Compress)
        Try
            Dim buffer(1024) As Byte
            Dim nRead As Integer

            nRead = fs.Read(buffer, 0, buffer.Length)
            While nRead > 0
                csStrim.Write(buffer, 0, nRead)
                nRead = fs.Read(buffer, 0, buffer.Length)
            End While
        Catch ex As Exception
            Throw ex
        Finally
            csStrim.Close()
            fs.Close()
            fd.Close()
        End Try
    End Sub



    ''' Скопированно прямиком из Update.aspx
    ''' 
    Private Const fileName As String = "upd.gzip"

    Private Sub UpdateDb_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles UpdateDb.Click
        Dim filePath As String = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, fileName)
        Dim NoSqlUpdates_Checked As Boolean = False

        Dim upd As Update = New Update

        Dim fileStream As FileStream = Nothing

        If Dir(filePath) <> "" Then
            Try
                'File.SetAttributes(filePath, File.GetAttributes(filePath) And Not FileAttributes.ReadOnly)
                'File.Copy(filePath, filePath + ".gzip", True)
                File.Copy(filePath, Path.Combine(getTempDir(), Path.GetFileName(filePath)), True)

                'filePath = filePath + ".gzip"
                filePath = Path.Combine(getTempDir(), Path.GetFileName(filePath))
                File.SetAttributes(filePath, File.GetAttributes(filePath) And Not FileAttributes.ReadOnly)

                fileStream = New FileStream(filePath, FileMode.Open)
                AppSettings.BeginTransaction()
                Dim UpdateFromDataset As New DataSet
                Try
                    If fileName.EndsWith(".gzip") Then
                        Dim UpdateGZipStream As New IO.Compression.GZipStream(fileStream, IO.Compression.CompressionMode.Decompress)
                        UpdateFromDataset.ReadXml(UpdateGZipStream, XmlReadMode.ReadSchema)
                    ElseIf fileName.EndsWith(".xml") Then
                        UpdateFromDataset.ReadXml(fileStream, XmlReadMode.ReadSchema)
                    Else
                        Throw New Exception("Данное расширение не поддерживается")
                    End If
                Catch ex As Exception
                    Throw New Exception("Непрафильный формат файла -> " & ex.Message)
                End Try
                If UpdateFromDataset.DataSetName = "Update" Then
                    For Each eqTable In upd.eqTables
                        If eqTable = "eqUpdates" Then
                            ' eqUpdates
                            Dim GoSeparatorsList As New List(Of String)
                            For Each inString As String In New String() {" ", vbCr, vbLf, vbCrLf, vbTab}
                                For Each outString As String In New String() {" ", vbCr, vbLf, vbCrLf, vbTab}
                                    GoSeparatorsList.Add(inString & "GO" & outString)
                                    GoSeparatorsList.Add(inString & "go" & outString)
                                    GoSeparatorsList.Add(inString & "Go" & outString)
                                    GoSeparatorsList.Add(inString & "gO" & outString)
                                Next
                            Next
                            Dim GoSeparatorsArray = GoSeparatorsList.ToArray
                            If NoSqlUpdates_Checked = False Then
                                Dim eqUpdatesOriginalSqlCommand As New SqlClient.SqlCommand("SELECT ID FROM eqUpdates")
                                Dim eqUpdatesOriginalDataTable = AppSettings.GetDataTable(eqUpdatesOriginalSqlCommand)
                                Dim eqUpdatesOriginalID = From R As DataRow In eqUpdatesOriginalDataTable.Rows Select DirectCast(R("ID"), Guid)
                                For Each UpdateRow In From R In UpdateFromDataset.Tables("eqUpdates") Where Not eqUpdatesOriginalID.Contains(DirectCast(R("ID"), Guid)) AndAlso R("Deleted") = False Order By R("CreationDateTime") Ascending
                                    Dim UpdateScript = UpdateRow("UpdateScript").ToString & " "
                                    Dim UpdateScriptArray = UpdateScript.Split(GoSeparatorsArray, StringSplitOptions.RemoveEmptyEntries)
                                    For Each UpdateScriptBatch In UpdateScriptArray
                                        If UpdateScriptBatch.Trim <> "" Then
                                            Try
                                                Dim UpdateSqlCommand As New SqlClient.SqlCommand(UpdateScriptBatch)
                                                AppSettings.ExecCommand(UpdateSqlCommand)
                                            Catch ex As Exception
                                                Throw New Exception("Ошибка при исполнении скрипта: " & vbCrLf & "" & ex.Message & vbCrLf & UpdateScriptBatch)
                                            End Try
                                        End If
                                    Next
                                    AppSettings.WriteUpdate(UpdateRow)
                                Next
                            End If
                        Else
                            ' eqTables
                            If UpdateFromDataset.Tables.Contains(eqTable) Then
                                upd.UpdateTableFromDataset(UpdateFromDataset, eqTable)
                            End If
                        End If
                    Next
                    ' Tables
                    For Each Table As DataTable In UpdateFromDataset.Tables
                        If Not upd.eqTables.Contains(Table.TableName) Then
                            upd.UpdateTableFromDataset(UpdateFromDataset, Table.TableName)
                        End If
                    Next
                    ' SampleAppSettings
                    Try
                        Dim SampleAppSettings As New eqAppSettings
                        SampleAppSettings.OpenSession(AppSettings.Cnn, AppSettings.UserInfo.UserLogin, AppSettings.Trn)
                        SampleAppSettings.Manager = My.Resources.ResourceManager
                        Session("AppSettings") = SampleAppSettings
                    Catch ex As Exception
                        Throw New Exception("Не удалось загрузить систему с новой конфигурацией -> " & ex.Message)
                    End Try
                Else
                    Throw New Exception("Непрафильный формат файла -> Не содержит корневого узла Update")
                End If
                AppSettings.CommitTransaction()
                AddMessage("Обновление успешно применено")
            Catch ex As Exception
                AppSettings.RollbackTransaction()
                Throw ex
                'AddMessage("Ошибка при загрузке UpdateScript файла -> " & ex.Message)
            Finally
                If Not fileStream Is Nothing Then
                    fileStream.Close()
                End If

            End Try
        Else
            AddMessage("Приложите UpdateScript файл")
        End If
    End Sub
End Class