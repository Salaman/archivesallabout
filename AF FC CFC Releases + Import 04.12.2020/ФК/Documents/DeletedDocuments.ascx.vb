﻿Imports System.IO
Imports System.Xml
Imports System.Xml.Xsl

Partial Public Class DeletedDocuments
    Inherits BaseDoc

    Public Overrides Function LoadDoc(ByVal DocID As System.Guid) As Boolean
        If MyBase.LoadDoc(DocID) Then
            LoadTargetDocument()
        End If
    End Function

    Private Sub LoadTargetDocument()
        Dim targetDocTypeID As Guid = DataSetHeaderDataRow("DocTypeID")
        Dim targetDocument As String = DataSetHeaderDataRow("Document")

        If targetDocTypeID.Equals(Guid.Empty) OrElse String.IsNullOrEmpty(targetDocument) Then
            Exit Sub
        End If

        Dim DS As New DataSet
        Dim DT As eqDocType

        Using sreader As New StringReader(targetDocument)
            Using xreader As XmlReader = XmlReader.Create(sreader)
                DS.ReadXml(xreader)
            End Using
        End Using

        DT = AppSettings.DocTypes.Values.FirstOrDefault(Function(F) F.DocTypeID = targetDocTypeID)

        If DT Is Nothing Then
            Throw New Exception("В системе не зарегистрирован DocTypeID: " & targetDocTypeID.ToString)
        End If

        Using swriter As New StringWriter
            Using hwriter As New HtmlTextWriter(swriter)
                hwriter.AddAttribute(HtmlTextWriterAttribute.Class, "LogicBlockTable")
                hwriter.RenderBeginTag(HtmlTextWriterTag.Table)

                For Each col As DataColumn In DS.Tables(0).Columns
                    Dim FieldInfo As DataRow = DT.GetFieldInfo(col.Table.TableName, col.ColumnName)

                    hwriter.RenderBeginTag(HtmlTextWriterTag.Tr)

                    hwriter.AddStyleAttribute(HtmlTextWriterStyle.FontWeight, "bold")
                    hwriter.RenderBeginTag(HtmlTextWriterTag.Td)
                    hwriter.WriteEncodedText(FieldInfo("FieldNameDisplay"))
                    hwriter.RenderEndTag()

                    hwriter.RenderBeginTag(HtmlTextWriterTag.Td)

                    '====

                    Dim value As String = DS.Tables(0).Rows(0)(col.ColumnName).ToString

                    If String.Compare(value, Boolean.FalseString, True) = 0 Or String.Compare(value, Boolean.TrueString) = 0 Then
                        hwriter.WriteEncodedText(IIf(Boolean.Parse(value), "[v]", "[x]"))
                    Else
                        hwriter.WriteEncodedText(value.ToString)
                    End If

                    '====

                    hwriter.RenderEndTag()
                    hwriter.RenderEndTag()
                Next

                hwriter.RenderEndTag()
            End Using

            DocumentForDisplay.Text = swriter.ToString
        End Using
    End Sub

End Class