﻿Imports System.Data
Imports System.Data.SqlClient

Partial Public Class ACT
    Inherits BaseDoc

#Region "События"

    Private Sub Page_PreRender2(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Crumbs.ISN_FUND.DocID <> Guid.Empty AndAlso Not IsDBNull(Crumbs.ISN_FUND.DataSetHeaderDataRow("ISN_ARCHIVE")) Then
            Crumbs.ISN_ARCHIVE.KeyValue = Crumbs.ISN_FUND.DataSetHeaderDataRow("ISN_ARCHIVE")
        End If

        Dim SQL = <Text>
            select RA.ID, RA.DocID
                ,(select COUNT(*) from tblFUND F where F.ID=RA.DocID and RA.KIND=701) FCNT
                ,(select COUNT(*) from tblINVENTORY I where I.ID=RA.DocID and RA.KIND=702) ICNT
                ,(select COUNT(*) from tblDEPOSIT D where D.ID=RA.DocID and RA.KIND=707) DCNT
                ,(select COUNT(*) from tblUNIT U where U.ID=RA.DocID and U.UNIT_KIND = 704 and RA.KIND=704) UCNT
                ,(select COUNT(*) from tblUNIT U where U.ID=RA.DocID and U.UNIT_KIND = 703 and RA.KIND=703) HCNT
                ,isnull((select DS.UNIT_COUNT from tblDOCUMENT_STATS DS
                where DS.ISN_FUND = RA.ISN_OBJ and RA.KIND = 701 and DS.ISN_DOC_TYPE is null and DS.ISN_INVENTORY is null and DS.CARRIER_TYPE is null),0) FUCNT
                ,isnull((select DS.UNIT_COUNT from tblDOCUMENT_STATS DS
                where DS.ISN_FUND = A.ISN_FUND and RA.KIND = 702 and DS.ISN_DOC_TYPE is null and DS.ISN_INVENTORY = RA.ISN_OBJ and DS.CARRIER_TYPE is null),0) IUCNT
                ,isnull((select D.UNIT_COUNT from tblDEPOSIT D
                where D.ISN_DEPOSIT = RA.ISN_OBJ and RA.KIND = 707),0) DUCNT
                ,isnull((select U.UNIT_CNT from tblUNIT U
                where U.ISN_UNIT = RA.ISN_OBJ and RA.KIND = 704 and U.UNIT_KIND = 704),0) UUCNT
                ,isnull((select 1 from tblUNIT U
                where U.ISN_UNIT = RA.ISN_OBJ and RA.KIND = 703 and U.UNIT_KIND = 703),0) HUCNT
            from tblREF_ACT RA
            join tblACT A on RA.ISN_ACT = A.ISN_ACT
            where A.ISN_ACT = @ISN_ACT
        </Text>

        If DocStateInfo.StateID <> Guid.Empty Then
            Dim SummaryCommand As New SqlCommand(SQL.Value)
            SummaryCommand.Parameters.AddWithValue("@ISN_ACT", DataSetHeaderDataRow("ISN_ACT"))

            Dim SummaryDataTable = AppSettings.GetDataTable(SummaryCommand)
            Dim SummaryCountTotal As Integer = 0

            For Each Row In vREF_ACT_FOR_ACT.Rows
                Dim SummaryDataRow = SummaryDataTable.AsEnumerable.SingleOrDefault(Function(F) F("ID") = Row.RowGuid)
                Dim SummaryControl = DirectCast(Row.FindControl("Summary"), WebApplication.WebUI.eqDocument)

                If SummaryDataRow IsNot Nothing Then
                    If SummaryDataRow("FCNT") > 0 Then
                        SummaryControl.DocTypeURL = "FUND.ascx"
                        SummaryControl.ShowingFields = "FUND_NUM"
                        SummaryControl.ShowingFieldsFormat = "Фонд № {0}"
                    ElseIf SummaryDataRow("ICNT") > 0 Then
                        SummaryControl.DocTypeURL = "INVENTORY.ascx"
                        SummaryControl.ShowingFields = "INVENTORY_DISPLAY_INACT"
                        SummaryControl.ShowingFieldsFormat = "Опись № {0}"
                    ElseIf SummaryDataRow("DCNT") > 0 Then
                        SummaryControl.DocTypeURL = "DEPOSIT.ascx"
                        SummaryControl.ShowingFields = "DEPOSIT_NUM"
                        SummaryControl.ShowingFieldsFormat = "Россыпь № {0}"
                    ElseIf SummaryDataRow("UCNT") > 0 Then
                        SummaryControl.DocTypeURL = "UNIT.ascx"
                        SummaryControl.ShowingFields = "UNIT_NUM"
                        SummaryControl.ShowingFieldsFormat = "Ед.уч. № {0}"
                    ElseIf SummaryDataRow("HCNT") > 0 Then
                        SummaryControl.DocTypeURL = "UNIT.ascx"
                        SummaryControl.ShowingFields = "UNIT_NUM"
                        SummaryControl.ShowingFieldsFormat = "Ед.хр. № {0}"
                    End If
                    SummaryControl.DocID = SummaryDataRow("DocID")
                    If DataSetSpecificationDataRow("vREF_ACT_FOR_ACT", Row.RowGuid)("UNIT_COUNT") IsNot DBNull.Value Then
                        SummaryCountTotal += DataSetSpecificationDataRow("vREF_ACT_FOR_ACT", Row.RowGuid)("UNIT_COUNT")
                    Else
                        SummaryCountTotal += 0
                    End If
                End If
            Next

            LabelSummary.Text = SummaryCountTotal
            HolderSummary.Visible = True
        End If

        If BasePage.IsModalDialog Then
            Select Case BasePage.ModalDialogArgument.Argument
                Case "FUND.ascx"
                    BaseContainer.ClearMenuItems()
                    BaseContainer.AddMenuItem("Сохранить и закрыть", "SaveAndClose", "Images/Save.gif")
                Case "INVENTORY.ascx", "UNIT.ascx"
                    BaseContainer.ClearMenuItems()
                    BaseContainer.AddMenuItem("Сохранить и закрыть", "SaveAndClose", "Images/Save.gif")
                    ACT_OBJ.Enabled = False
            End Select
        End If
    End Sub

#End Region

#Region "Методы"

    Public Function AddToRef() As Boolean
        Try
            Select Case DataSetHeaderDataRow("ACT_OBJ")
                Case 701
                    Dim SQL = <sql>
                                            delete from tblREF_ACT where ISN_ACT = @ISN_ACT

                                            select @DocID = ID from tblFUND where ISN_FUND = @ISN_OBJ
                                            select @ISN_REF_ACT = isnull(max(ISN_REF_ACT)+1, 1) from tblREF_ACT
                                            insert into tblREF_ACT
                                            values (newid(), @OwnerID, getdate(), @DocID, 0, @ISN_REF_ACT, @ISN_ACT, @ISN_OBJ, @KIND, NULL)
                                        </sql>
                    Dim cmd As New SqlCommand(SQL.Value)
                    cmd.Parameters.Add(New SqlParameter("@ISN_ACT", SqlDbType.BigInt) With {.Value = DataSetHeaderDataRow("ISN_ACT")})
                    cmd.Parameters.Add(New SqlParameter("@OwnerID", SqlDbType.UniqueIdentifier) With {.Value = AppSettings.UserInfo.UserID})
                    cmd.Parameters.Add(New SqlParameter("@DocID", SqlDbType.UniqueIdentifier) With {.Value = DocID})
                    cmd.Parameters.Add(New SqlParameter("@ISN_REF_ACT", SqlDbType.BigInt) With {.Value = 0})
                    cmd.Parameters.Add(New SqlParameter("@ISN_OBJ", SqlDbType.BigInt) With {.Value = BasePage.ModalDialogArgument.ArgumentObject})
                    cmd.Parameters.Add(New SqlParameter("@KIND", SqlDbType.SmallInt) With {.Value = 701})
                    AppSettings.ExecCommand(cmd)
                Case 702
                    Dim SQL = <sql>
                                            delete from tblREF_ACT where ISN_ACT = @ISN_ACT

                                            select @DocID = ID from tblINVENTORY where ISN_INVENTORY = @ISN_OBJ
                                            select @ISN_REF_ACT = isnull(max(ISN_REF_ACT)+1, 1) from tblREF_ACT
                                            insert into tblREF_ACT
                                            values (newid(), @OwnerID, getdate(), @DocID, 0, @ISN_REF_ACT, @ISN_ACT, @ISN_OBJ, @KIND, NULL)
                                        </sql>
                    Dim cmd As New SqlCommand(SQL.Value)
                    cmd.Parameters.Add(New SqlParameter("@ISN_ACT", SqlDbType.BigInt) With {.Value = DataSetHeaderDataRow("ISN_ACT")})
                    cmd.Parameters.Add(New SqlParameter("@OwnerID", SqlDbType.UniqueIdentifier) With {.Value = AppSettings.UserInfo.UserID})
                    cmd.Parameters.Add(New SqlParameter("@DocID", SqlDbType.UniqueIdentifier) With {.Value = DocID})
                    cmd.Parameters.Add(New SqlParameter("@ISN_REF_ACT", SqlDbType.BigInt) With {.Value = 0})
                    cmd.Parameters.Add(New SqlParameter("@ISN_OBJ", SqlDbType.BigInt) With {.Value = BasePage.ModalDialogArgument.ArgumentObject})
                    cmd.Parameters.Add(New SqlParameter("@KIND", SqlDbType.SmallInt) With {.Value = 702})
                    AppSettings.ExecCommand(cmd)
                Case 703
                    Dim SQL = <sql>
                                            delete from tblREF_ACT where ISN_ACT = @ISN_ACT

                                            select @DocID = ID from tblUNIT where ISN_UNIT = @ISN_OBJ
                                            select @ISN_REF_ACT = isnull(max(ISN_REF_ACT)+1, 1) from tblREF_ACT
                                            insert into tblREF_ACT
                                            values (newid(), @OwnerID, getdate(), @DocID, 0, @ISN_REF_ACT, @ISN_ACT, @ISN_OBJ, @KIND, NULL)
                                        </sql>
                    Dim cmd As New SqlCommand(SQL.Value)
                    cmd.Parameters.Add(New SqlParameter("@ISN_ACT", SqlDbType.BigInt) With {.Value = DataSetHeaderDataRow("ISN_ACT")})
                    cmd.Parameters.Add(New SqlParameter("@OwnerID", SqlDbType.UniqueIdentifier) With {.Value = AppSettings.UserInfo.UserID})
                    cmd.Parameters.Add(New SqlParameter("@DocID", SqlDbType.UniqueIdentifier) With {.Value = DocID})
                    cmd.Parameters.Add(New SqlParameter("@ISN_REF_ACT", SqlDbType.BigInt) With {.Value = 0})
                    cmd.Parameters.Add(New SqlParameter("@ISN_OBJ", SqlDbType.BigInt) With {.Value = BasePage.ModalDialogArgument.ArgumentObject})
                    cmd.Parameters.Add(New SqlParameter("@KIND", SqlDbType.SmallInt) With {.Value = 703})
                    AppSettings.ExecCommand(cmd)
            End Select
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Sub SaveAndClose()
        If SaveDoc() Then
            BasePage.CloseModalDialogWindow("ACT.ascx", DocID)
        End If
    End Sub

#End Region

#Region "Документ"

    Public Overrides Function NewDoc() As Boolean
        NewDoc = MyBase.NewDoc()
        If BasePage.IsModalDialog Then
            Select Case BasePage.ModalDialogArgument.Argument
                Case "FUND.ascx"
                    DataSetHeaderDataRow("ISN_FUND") = BasePage.ModalDialogArgument.ArgumentObject
                    PutValueFromDataSetToHeaderControl("ISN_FUND")
                    ACT_OBJ.SelectedValue = ""
                Case "INVENTORY.ascx"
                    Dim SQL = <sql>
                                    select tblFUND.ISN_FUND from tblFUND
                                    join tblINVENTORY on tblFUND.ISN_FUND = tblINVENTORY.ISN_FUND
                                    where tblINVENTORY.ISN_INVENTORY = @ISN_INVENTORY
                            </sql>
                    Dim cmd = New SqlCommand(SQL.Value)
                    cmd.Parameters.AddWithValue("@ISN_INVENTORY", BasePage.ModalDialogArgument.ArgumentObject)
                    DataSetHeaderDataRow("ISN_FUND") = AppSettings.ExecCommandScalar(cmd)
                    PutValueFromDataSetToHeaderControl("ISN_FUND")
                    ACT_OBJ.SelectedValue = "702"
                Case "UNIT.ascx"
                    Dim SQL = <sql>
                                    select tblFUND.ISN_FUND from tblFUND 
                                    join tblINVENTORY on tblFUND.ISN_FUND = tblINVENTORY.ISN_FUND
                                    join tblUNIT on tblUNIT.ISN_INVENTORY = tblINVENTORY.ISN_INVENTORY
                                    where tblUNIT.ISN_UNIT = @ISN_UNIT
                            </sql>
                    Dim cmd = New SqlCommand(SQL.Value)
                    cmd.Parameters.AddWithValue("@ISN_UNIT", BasePage.ModalDialogArgument.ArgumentObject)
                    DataSetHeaderDataRow("ISN_FUND") = AppSettings.ExecCommandScalar(cmd)
                    PutValueFromDataSetToHeaderControl("ISN_FUND")
                    ACT_OBJ.SelectedValue = "703"
            End Select
        End If
    End Function

    Public Overrides Function SaveDoc() As Boolean
        Dim ActDate As Date
        If Date.TryParse(ACT_DATE.Text, ActDate) Then
            Dim CheckSQL = <sql>
                            select *
                            from tblACT
                            where ACT_NUM = @NUM 
                                and ACT_DATE = @DATE 
                                and Deleted = 0 
                                and ID != @ID
                                and MOVEMENT_FLAG = @MOVEMENT_FLAG
                                and ISN_FUND = @ISN_FUND 
                        </sql>
            Dim CheckCommand As New SqlCommand(CheckSQL.Value)
            CheckCommand.Parameters.AddWithValue("@NUM", ACT_NUM.Text)
            CheckCommand.Parameters.AddWithValue("@DATE", Date.Parse(ACT_DATE.Text))
            CheckCommand.Parameters.AddWithValue("@ID", DocID)
            CheckCommand.Parameters.AddWithValue("@MOVEMENT_FLAG", MOVEMENT_FLAG.SelectedValue)
            'проверка на фонд Natalia Lyalina 02/04/13
            CheckCommand.Parameters.AddWithValue("@ISN_FUND", DataSetHeaderDataRow("ISN_FUND").ToString())
            Dim CheckDataTable = AppSettings.GetDataTable(CheckCommand)
            If CheckDataTable.Rows.Count > 0 Then
                AddMessage("Акт с таким же номером, типом движения и датой в пределах данного фонда уже существует ")
                Return False
            End If
        Else
            AddMessage("Дата акта указана неверно")
            Return False
        End If

        SaveDoc = MyBase.SaveDoc
        If SaveDoc Then
            If BasePage.IsModalDialog Then
                Select Case BasePage.ModalDialogArgument.Argument
                    Case "FUND.ascx"
                    Case Else
                        Return AddToRef()
                End Select
            End If
        End If
    End Function

#End Region

#Region "Контролы"

    Public Overrides Sub PutValueToControl(ByVal Value As Object, ByVal TargetControl As System.Web.UI.Control, Optional ByVal FieldFormat As String = "")
        If TypeOf TargetControl Is CheckBox Then
            DirectCast(TargetControl, CheckBox).Checked = (Value Is Nothing OrElse Value.ToString.Trim.ToUpper = "Y")
        Else
            MyBase.PutValueToControl(Value, TargetControl, FieldFormat)
        End If
    End Sub

    Public Overrides Function GetValueFromControl(ByVal SourceControl As System.Web.UI.Control) As Object
        If TypeOf SourceControl Is CheckBox Then
            Return IIf(DirectCast(SourceControl, CheckBox).Checked, "Y", "N")
        Else
            Return MyBase.GetValueFromControl(SourceControl)
        End If
    End Function

#End Region

#Region "Файлы"

    Private Sub RefFileUploadFile_Uploaded(ByVal sender As Object, ByVal e As RefFileArgs) Handles RefFileUploadFile.Uploaded
        Dim row = vREF_FILE_ACT.InsertRow(e.ID)
        DataSetSpecificationDataRow(vREF_FILE_ACT.ID, e.ID)("CreationDateTime") = e.TransferTime
        DirectCast(row.FindControl("NAME"), TextBox).Text = System.IO.Path.GetFileName(e.FileName)
    End Sub

    Private Sub RefFileUploadFile_Uploading(ByVal sender As Object, ByVal e As RefFileArgs) Handles RefFileUploadFile.Uploading
        e.FileName = GetFileSystemAttachFileName(e.ID, e.TransferTime, 706, DBNull.Value, e.FileName)
    End Sub

    Public Sub RefFile_Requesting(ByVal sender As Object, ByVal e As RefFileArgs)
        e.TransferTime = DataSetSpecificationDataRow(vREF_FILE_ACT.ID, e.ID)("CreationDateTime")
        e.FileName = GetFileSystemAttachFileName(e.ID, e.TransferTime, 706, DBNull.Value)
    End Sub

#End Region

End Class