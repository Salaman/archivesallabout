﻿Partial Public Class SEARCH_TOPO
    Inherits BaseDoc

    Private ListDocRESULT As ListDoc

    Private Property SelectedLocation() As Guid
        Get
            If ViewState("SelectedLocation") Is Nothing Then
                Return Guid.Empty
            Else
                Return ViewState("SelectedLocation")
            End If
        End Get
        Set(ByVal value As Guid)
            ViewState("SelectedLocation") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        LoadListDocRESULT()
    End Sub

    Private Sub LoadListDocRESULT()
        PlaceHolderRESULT.Controls.Clear()
        ListDocRESULT = DirectCast(LoadControl("../ListDoc.ascx"), ListDoc)
        ListDocRESULT.ID = MenuRESULT.SelectedValue.Replace(".ascx", "")
        ListDocRESULT.DocTypeURL = MenuRESULT.SelectedValue
        ListDocRESULT.LoadDocMode = eqBaseContainer.LoadDocModes.NewWindow
        ListDocRESULT.SelectDocMode = eqBaseContainer.SelectDocModes.Multi
        FormWhereStrings()
        PlaceHolderRESULT.Controls.Add(ListDocRESULT)
    End Sub

    Private Sub ListDocCLS_SelectDoc(ByVal DocID As System.Guid) Handles ListDocLocation.SelectDoc
        SelectedLocation = DocID
        FormWhereStrings()
        ListDocRESULT.Refresh()
    End Sub

    Private Sub FormWhereStrings()
        If SelectedLocation <> Guid.Empty Then
            Select Case ListDocRESULT.DocTypeURL
                Case "FUND.ascx"
                    ListDocRESULT.AdditionalFromString = "LEFT JOIN tblREF_LOCATION SR ON SR.ISN_OBJ=[" & ListDocRESULT.DocType.GetHeaderTableName & "].ISN_FUND" & vbCrLf & _
                                                         "LEFT JOIN tblLocation SL ON SL.ISN_LOCATION=SR.ISN_LOCATION"
                    ListDocRESULT.AdditionalWhereString = "AND SR.KIND=701 AND SL.ID='" & SelectedLocation.ToString & "'"
                Case "INVENTORY.ascx"
                    ListDocRESULT.AdditionalFromString = "LEFT JOIN tblREF_LOCATION SR ON SR.ISN_OBJ=[" & ListDocRESULT.DocType.GetHeaderTableName & "].ISN_INVENTORY" & vbCrLf & _
                                                         "LEFT JOIN tblLocation SL ON SL.ISN_LOCATION=SR.ISN_LOCATION"
                    ListDocRESULT.AdditionalWhereString = "AND SR.KIND=702 AND SL.ID='" & SelectedLocation.ToString & "'"
                Case "UNIT.ascx"
                    ListDocRESULT.AdditionalFromString = "LEFT JOIN tblLocation SL ON SL.ISN_LOCATION=[" & ListDocRESULT.DocType.GetHeaderTableName & "].ISN_LOCATION"
                    ListDocRESULT.AdditionalWhereString = "AND SL.ID='" & SelectedLocation.ToString & "'"
            End Select
        Else
            ListDocRESULT.ListDocRefreshOnLoad = False
        End If
    End Sub

    Private Sub MenuRESULT_MenuItemClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MenuEventArgs) Handles MenuRESULT.MenuItemClick
        LoadListDocRESULT()
    End Sub

    Public Sub ButtonDeattach_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonDeattach.Click
        If SelectedLocation <> Guid.Empty Then
            Dim cmdSB As New Text.StringBuilder
            cmdSB.AppendLine("DECLARE @ISN_LOCATION bigint")
            cmdSB.AppendLine("SELECT @ISN_LOCATION=ISN_LOCATION FROM tblLOCATION WHERE ID='" & SelectedLocation.ToString & "'")
            For Each doc In ListDocRESULT.SelectedDocuments
                Select Case ListDocRESULT.DocTypeURL
                    Case "FUND.ascx"
                        cmdSB.AppendLine("DELETE FROM tblREF_LOCATION WHERE ISN_LOCATION=@ISN_LOCATION AND KIND=701 AND DocID='" & doc.ToString & "'")
                    Case "INVENTORY.ascx"
                        cmdSB.AppendLine("DELETE FROM tblREF_LOCATION WHERE ISN_LOCATION=@ISN_LOCATION AND KIND=702 AND DocID='" & doc.ToString & "'")
                    Case "UNIT.ascx"
                        cmdSB.AppendLine("UPDATE [" & ListDocRESULT.DocType.GetHeaderTableName & "] SET ISN_LOCATION=NULL WHERE ISN_LOCATION=@ISN_LOCATION AND ID='" & doc.ToString & "'")
                End Select
            Next
            AppSettings.ExecCommand(New SqlClient.SqlCommand(cmdSB.ToString))
            FormWhereStrings()
            ListDocRESULT.Refresh()
        End If
    End Sub

End Class