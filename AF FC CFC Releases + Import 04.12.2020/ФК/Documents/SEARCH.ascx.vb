﻿Imports System.Data.SqlClient

Partial Public Class SEARCH
    Inherits BaseDoc

    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        DataSourceSearch.ConnectionString = My.Settings.DatabaseConnectionString
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
        End If
    End Sub

    Private ReadOnly Property SearchPattern() As String()
        Get
            Dim S = TextBoxSearchPattern.Text
            Dim R As New List(Of String)

            If S.Contains("""") Then
                For Each p In S.Split(New String() {""""}, StringSplitOptions.RemoveEmptyEntries)
                    R.Add("""" & p & """")
                Next
            Else
                For Each p In S.Split(New String() {" "}, StringSplitOptions.RemoveEmptyEntries)
                    R.Add("""*" & p & "*""")
                Next
            End If

            Return R.ToArray
        End Get
    End Property

    Private Sub ButtonSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click
        Dim s = ""
        For Each p In SearchPattern
            If s <> "" Then
                s &= " AND "
            End If
            s &= p
        Next

        DataSourceSearch.SelectParameters("SearchPattern").DefaultValue = s
        DataSourceSearch.DataBind()
    End Sub

    Private Sub DataSourceSearch_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles DataSourceSearch.Selected
        LabelMessage.Text = String.Format("Найдено: {0}", e.AffectedRows)
        e.Command.CommandTimeout = 1800
    End Sub

    Public Function evalHref(ByVal Container As Object)
        Dim Data = DirectCast(DataBinder.GetDataItem(Container), DataRowView)

        Select Case Data("Kind")
            Case 0
                Return BasePage.GetDocURL("AuthorizedDep.ascx", Data("ID"), False)
            Case 700
                Return BasePage.GetDocURL("ARCHIVE.ascx", Data("ID"), False)
            Case 701
                Return BasePage.GetDocURL("FUND.ascx", Data("ID"), False)
            Case 702
                Return BasePage.GetDocURL("INVENTORY.ascx", Data("ID"), False)
            Case 703
                Return BasePage.GetDocURL("UNIT.ascx", Data("ID"), False)
            Case 707
                Return BasePage.GetDocURL("DEPOSIT.ascx", Data("ID"), False)
            Case 705
                Return BasePage.GetDocURL("DOCUMENT.ascx", Data("ID"), False)
            Case Else
                Return "#"
        End Select
    End Function

    ''' <summary>
    ''' Возвращает ссылку на контейнер, который содержит контейнер, который содержит один из результатов поиска.
    ''' </summary>
    ''' <param name="Container"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function evalUpperHref(ByVal Container As Object) ' (Roman 300713)
        Dim Data = DirectCast(DataBinder.GetDataItem(Container), DataRowView)
        Dim cmdText As String
        Dim cmd As SqlCommand
        'AppSettings.GetDataTable(cmd)
        Select Case Data("Kind")
            'Case 0
            '    Return BasePage.GetDocURL("AuthorizedDep.ascx", Data("ID"), False)
            'Case 700
            '    Return BasePage.GetDocURL("ARCHIVE.ascx", Data("ID"), False)
            Case 701
                'Return BasePage.GetDocURL("FUND.ascx", Data("ID"), False)
                cmdText = <sql>
                            SELECT TOP 1 ID FROM tblARCHIVE WHERE ISN_ARCHIVE= (
                                SELECT TOP 1 ISN_ARCHIVE FROM tblFUND WHERE ID=@FID
                            )
                        </sql>
                cmd = New SqlCommand(cmdText)
                cmd.Parameters.AddWithValue("@FID", Data("ID"))
                Return BasePage.GetDocURL("ARCHIVE.ascx", AppSettings.ExecCommandScalar(cmd), False)
            Case 702
                'Return BasePage.GetDocURL("INVENTORY.ascx", Data("ID"), False)
                cmdText = <sql>
                            SELECT TOP 1 ID FROM tblFUND WHERE ISN_FUND= (
                                SELECT TOP 1 ISN_FUND FROM tblINVENTORY WHERE ID=@IID
                            )
                        </sql>
                cmd = New SqlCommand(cmdText)
                cmd.Parameters.AddWithValue("@IID", Data("ID"))
                Return BasePage.GetDocURL("FUND.ascx", AppSettings.ExecCommandScalar(cmd), False)
            Case 703, 704
                'Return BasePage.GetDocURL("UNIT.ascx", Data("ID"), False)
                cmdText = <sql>
                            SELECT TOP 1 ID FROM tblINVENTORY WHERE ISN_INVENTORY= (
                                SELECT TOP 1 ISN_INVENTORY FROM tblUNIT WHERE ID=@UID
                            )
                        </sql>
                cmd = New SqlCommand(cmdText)
                cmd.Parameters.AddWithValue("@UID", Data("ID"))
                Return BasePage.GetDocURL("INVENTORY.ascx", AppSettings.ExecCommandScalar(cmd), False)
            Case 707
                'Return BasePage.GetDocURL("DEPOSIT.ascx", Data("ID"), False)
                cmdText = <sql>
                            SELECT TOP 1 ID FROM tblFUND WHERE ISN_FUND= (
                                SELECT TOP 1 ISN_FUND FROM tblDEPOSIT WHERE ID=@DID
                            )
                        </sql>
                cmd = New SqlCommand(cmdText)
                cmd.Parameters.AddWithValue("@DID", Data("ID"))
                Return BasePage.GetDocURL("FUND.ascx", AppSettings.ExecCommandScalar(cmd), False)
            Case 705
                'Return BasePage.GetDocURL("DOCUMENT.ascx", Data("ID"), False)
                cmdText = <sql>
                            SELECT TOP 1 ID FROM tblUNIT WHERE ISN_UNIT= (
                                SELECT TOP 1 ISN_UNIT FROM tblDOCUMENT WHERE ID=@DID
                            )
                        </sql>
                cmd = New SqlCommand(cmdText)
                cmd.Parameters.AddWithValue("@DID", Data("ID"))
                Return BasePage.GetDocURL("UNIT.ascx", AppSettings.ExecCommandScalar(cmd), False)
            Case Else
                Return "#"
        End Select
    End Function

    Public Function evalTitle(ByVal Container As Object)
        Dim Data = DirectCast(DataBinder.GetDataItem(Container), DataRowView)

        If IsDBNull(Data("Code")) Then
            Return Data("Header") & ". " & Data("Name")
        Else
            Return Data("Header") & ". №" & Data("Code") & " . " & Data("Name")
        End If
    End Function

    Public Function evalText(ByVal Container As Object)
        Dim Data = DirectCast(DataBinder.GetDataItem(Container), DataRowView)
        Dim S As New String(Data("Search").ToString)
        Dim R As New StringBuilder

        Dim Sorter As Comparison(Of Triplet) = Function(X, Y) IIf(X.Second = Y.Second, 0, IIf(X.Second > Y.Second, 1, -1))

        Const TAG_BEGIN = "<b style='color: #0000ff;'>"
        Const TAG_END = "</b>"
        Const DIST = 8

        

        '(Roman-2014-05-29)

        Dim SArrTemp As String() 'every array string contains text from one block of fn_Search() (block: sql-code piece in function fn_Search(), processing one table)
        SArrTemp = Split(S, " %%% ")
        Dim SArr As String()()
        SArr = New String(SArrTemp.Length - 1)() {}
        For i As Integer = 0 To SArrTemp.Length - 1
            SArr(i) = Split(SArrTemp(i), " ||| ") ' every sub-array string contains text from one cell of one table
        Next

        Dim ptArr As List(Of Triplet) = New List(Of Triplet) ' search patterns array (3-dimensional cortege as elements: pattern string, 1st match index, pattern string length)
        For Each p In SearchPattern
            Dim pt = p.Trim(New Char() {""""}).Trim(New Char() {"*"})
            Dim match As Integer = S.IndexOf(pt, 0, StringComparison.InvariantCultureIgnoreCase)
            If match <> -1 Then
                ptArr.Add(New Triplet With {.First = pt, .Second = match, .Third = pt.Length})
            End If
        Next


        R.Append("<p>") ' bigin trxt forming

        Dim resNumInString As Integer = 4 ' max number of results in one string
        Dim resStrNumInCell As Integer = 4 ' max strings number for one cell
        Dim iResStrNumInCell As Integer = 0

        For Each strBlock As String() In SArr

            For Each strCell As String In strBlock
                If strCell = "" Then
                    Continue For
                End If
                Dim cellResult As String = "" ' cell search-results in formate " ... testtest RES1 testtest ... testtest RES2 testtest ... "
                '''''''''''''''''''''''''''
                cellResult = ProcessSearchResultCell(strCell, ptArr, resNumInString, TAG_BEGIN, TAG_END, DIST)
                If cellResult <> "" Then
                    R.Append(cellResult)
                    R.Append("</p><p>")
                    iResStrNumInCell += 1
                End If
                '''''''''''''''''''''''''''
                If iResStrNumInCell >= resStrNumInCell Then
                    Exit For
                End If
            Next
            If iResStrNumInCell >= resStrNumInCell Then
                Exit For
            End If
        Next
        R.Append("</p>") ' end text froming
        '(/Roman-2014-05-29)

        Return R.ToString
    End Function

    ''' <summary>
    ''' (Roman-2014-05-29)
    ''' </summary>
    ''' <param name="cellStr">text from cell for processing</param>
    ''' <param name="ptArr">array of search patterns</param>
    ''' <param name="maxResNum">max number of results</param>
    ''' <param name="TAG_BEGIN">tag for pointing searching matches</param>
    ''' <param name="TAG_END">end-tag for pointing searching matches</param>
    ''' <param name="DIST">radius of surrounding text-pieces</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function ProcessSearchResultCell(ByVal cellStr As String, ByRef ptArr As List(Of Triplet), ByVal maxResNum As Integer, ByVal TAG_BEGIN As String, ByVal TAG_END As String, ByVal DIST As Integer) As String
        Dim ret As StringBuilder = New StringBuilder(" ... ")
        Dim resNum As Integer = 0 'how many results are already found
        Dim position As Integer = -1 'search position
        For Each pt As Triplet In ptArr
            For i As Integer = 0 To maxResNum - 1
                If resNum >= maxResNum Then
                    Exit For 'we've got maxResNum search results. We don't need any more
                End If
                If cellStr.IndexOf(pt.First, position + 1, StringComparison.InvariantCultureIgnoreCase) = -1 Then 'nothing new to find with this pattern
                    position = 0
                    Exit For 'go to the next pattern
                Else 'something was found
                    position = cellStr.IndexOf(pt.First, position + 1, StringComparison.InvariantCultureIgnoreCase)
                    ret.Append(cellStr.Substring(Math.Max(0, position - DIST), position - Math.Max(0, position - DIST))) 'left text-piece
                    ret.Append(TAG_BEGIN)
                    ret.Append(cellStr.Substring(position, pt.Third)) 'search result
                    ret.Append(TAG_END)
                    ret.Append(cellStr.Substring(position + pt.Third, Math.Min(cellStr.Length - (position + pt.Third), DIST))) 'right text-piece
                    ret.Append(" ... ")
                    resNum += 1
                End If
            Next

        Next
        If ret.ToString = " ... " Then 'if nothing was found
            Return "" 'return empty string
        Else
            Return ret.ToString
        End If
    End Function


End Class