﻿Public Partial Class DOC_KIND_CL
    Inherits BaseDoc

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Request.QueryString("ParentID") IsNot Nothing Then
                ISN_HIGH_DOC_KIND.DocID = New Guid(Request.QueryString("ParentID"))
            End If
        End If
    End Sub

    Private Sub clearISN_HIGH_DOC_KIND_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles clearISN_HIGH_DOC_KIND.Click
        ISN_HIGH_DOC_KIND.DocID = Guid.Empty
    End Sub

End Class