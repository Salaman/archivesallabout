﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="tblConstants.ascx.vb"
    Inherits="WebApplication.tblConstants" %>
<p>
    Константы
</p>
<table>
    <col width="160px" />
    <tr>
        <td>
            Наименование группы констант:
        </td>
        <td>
            <eq:eqTextBox ID="ConstantGroup" runat="server" Width="200px" />
        </td>
    </tr>
    <tr>
        <td>
            Описание:
        </td>
        <td>
            <eq:eqTextBox ID="Description" runat="server" Width="450px" />
        </td>
    </tr>
</table>
<p>
    Значения:</p>
<eq:eqSpecification ID="tblConstantsSpec" runat="server" Width=650>
    <eq:eqSpecificationColumn HeaderText="Значение">
        <ItemTemplate>
            <eq:eqTextBox ID="Value" runat="server" Width="100px" />
        </ItemTemplate>
    </eq:eqSpecificationColumn>
    <eq:eqSpecificationColumn HeaderText ="Текст">
    <ItemTemplate>
        <eq:eqTextBox ID="Text" runat="server" Width="100%" /></ItemTemplate>
    </eq:eqSpecificationColumn>
</eq:eqSpecification>
