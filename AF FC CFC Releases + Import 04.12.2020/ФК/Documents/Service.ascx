﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Service.ascx.vb" Inherits="WebApplication.Service" %>
<div class="LogicBlockCaption">
    Резервная копия БД
</div>
<table style="width: 700px;">
    <colgroup>
        <col style="width: 250px;" />
        <col />
    </colgroup>
    <tr>
        <td>
            При резервном копировании удалять все предыдущие резервные копии
        </td>
        <td>
            <asp:CheckBox ID="Overwrite" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            Вы используете БД
        </td>
        <td>
            <asp:Label ID="DBName" runat="server" />
        </td>
    </tr>
    <tr>
        <td><asp:Button ID="UpdateDb" runat="server" Text="Обновить БД" /></td>
        <td></td>
    </tr>
    <tr>
        <td>
            Путь для резервной копии БД
        </td>
        <td>
            <eq:eqTextBox ID="BackupPath" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            Период проверки актуальности резервной копии (дней)
        </td>
        <td>
            <eq:eqTextBox ID="BackupInterval" runat="server" Width="50" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:Button ID="DownloadBak" runat="server" Text="Выгрузить резервную копию" />
        </td>
        <td>
            <asp:CheckBox ID="ChbDownloadZipBak" runat="server" /> использовать сжатие
        </td>
    </tr>
</table>



<div class="LogicBlockCaption">
    Созданные резервные копии БД
</div>
<table style="width: 700px;">
    <tr>
        <td>
            <eq:eqSpecification ID="vServiceLog" runat="server" AllowDelete="false" AllowInsert="false" AllowReorder="false">
                <eq:eqSpecificationColumn HeaderText="Дата" Width="140">
                    <ItemTemplate>
                        <asp:Label ID="CreationDateTime" runat="server" />
                    </ItemTemplate>
                </eq:eqSpecificationColumn>
                <eq:eqSpecificationColumn HeaderText="Пользователь">
                    <ItemTemplate>
                        <asp:Label ID="OwnerID" runat="server" />
                    </ItemTemplate>
                </eq:eqSpecificationColumn>
                <eq:eqSpecificationColumn Width="100">
                    <ItemTemplate>
                        <asp:LinkButton ID="Restore" runat="server" Text="Восстановить" CommandName="Restore" />
                    </ItemTemplate>
                </eq:eqSpecificationColumn>
            </eq:eqSpecification>
        </td>
    </tr>
</table>
