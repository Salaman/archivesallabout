﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="View.ascx.vb" Inherits="WebApplication.View" %>
<%@ Register Src="../ListDocFilter.ascx" TagName="ListDocFilter" TagPrefix="ldf" %>
<%@ Register Src="../ListDocFunction.ascx" TagName="ListDocFunction" TagPrefix="ldf" %>
<asp:PlaceHolder ID="PlaceHolderView" runat="server">
    <eq:eqCollapsiblePanel runat="server" Collapsed="true" ID="ColPanHeader">
        <HeaderPanel>
            <%--Представление--%>
            <asp:Localize ID="LocalizeView" runat="server" Text="<%$ Resources:eqResources, CAP_View %>" />
        </HeaderPanel>
        <ContentPanel>
            <table width="100%" class="LogicBlockTable">
                <colgroup>
                    <col style="width: 130px;" />
                    <col />
                </colgroup>
                <tr>
                    <td>
                        <%--Название--%>
                        <asp:Localize ID="LocalizeViewName" runat="server" Text="<%$ Resources:eqResources, CAP_Caption %>" />
                    </td>
                    <td>
                        <eq:eqTextBox ID="ViewName" runat="server" Width="100%" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <%--Описание--%>
                        <asp:Localize ID="LocalizeDescription" runat="server" Text="<%$ Resources:eqResources, CAP_Description %>" />
                    </td>
                    <td>
                        <eq:eqTextBox ID="Description" runat="server" Width="100%" Rows="3" TextMode="MultiLine" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <%--Документ--%>
                        <asp:Localize ID="LocalizeParentID" runat="server" Text="<%$ Resources:eqResources, CAP_Document %>" />
                    </td>
                    <td>
                        <asp:DropDownList ID="ParentID" runat="server" Width="100%" AutoPostBack="True" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <%--Тип представления--%>
                        <asp:Localize ID="LocalizeViewAccess" runat="server" Text="<%$ Resources:eqResources, CAP_ViewAccess %>" />
                    </td>
                    <td>
                        <asp:DropDownList ID="Access" runat="server" Width="100%" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <%--По умолчанию--%>
                        <asp:Localize ID="LocalizeIsDefault" runat="server" Text="<%$ Resources:eqResources, CAP_Default %>" />
                    </td>
                    <td>
                        <asp:CheckBox ID="IsDefault" runat="server" />
                    </td>
                </tr>
                <tr id="trOwnerID" runat="server">
                    <td>
                        <%--Автор--%>
                        <asp:Localize ID="LocalizeOwnerID" runat="server" Text="<%$ Resources:eqResources, CAP_Author %>" />
                    </td>
                    <td>
                        <eq:eqDocument ID="OwnerID" runat="server" DocTypeURL="Users.ascx" ShowingFields="DisplayName"
                            ShowingFieldsFormat="{0}" ListDocMode="NewWindowAndSelect" LoadDocMode="NewWindowAndSelect" />
                    </td>
                </tr>
            </table>
        </ContentPanel>
    </eq:eqCollapsiblePanel>
</asp:PlaceHolder>
<eq:eqCollapsiblePanel runat="server" Collapsed="true" ID="ColPanSetup">
    <HeaderPanel>
        <%--Настройки--%>
        <asp:Localize ID="LocalizeSettings" runat="server" Text="<%$ Resources:eqResources, CAP_Settings %>" />
    </HeaderPanel>
    <ContentPanel>
        <table width="100%" class="LogicBlockTable">
            <colgroup>
                <col style="width: 130px;" />
                <col />
            </colgroup>
            <tr>
                <td>
                    <%--Показать удаленные--%>
                    <asp:Localize ID="LocalizeShowDeleted" runat="server" Text="<%$ Resources:eqResources, CAP_ShowDeleted %>" />
                </td>
                <td>
                    <asp:CheckBox ID="ShowDeleted" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <%--Записей на странице--%>
                    <asp:Localize ID="LocalizeRowsPerPage" runat="server" Text="<%$ Resources:eqResources, CAP_RowsPerPage %>" />
                </td>
                <td>
                    <asp:DropDownList ID="RowsPerPage" Width="70px" runat="server">
                        <asp:ListItem Value="1">1</asp:ListItem>
                        <asp:ListItem Value="10">10</asp:ListItem>
                        <asp:ListItem Value="30">30</asp:ListItem>
                        <asp:ListItem Value="50">50</asp:ListItem>
                        <asp:ListItem Value="70">70</asp:ListItem>
                        <asp:ListItem Value="100" Selected="True">100</asp:ListItem>
                        <asp:ListItem Value="200">200</asp:ListItem>
                        <asp:ListItem Value="500">500</asp:ListItem>
                        <asp:ListItem Value="1000">1000</asp:ListItem>
                        <asp:ListItem Value="2000">2000</asp:ListItem>
                        <asp:ListItem Value="5000">5000</asp:ListItem>
                        <asp:ListItem Value="10000">10000</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <%--Ограничение на количество записей--%>
                    <asp:Localize ID="LocalizeMaxRows" runat="server" Text="<%$ Resources:eqResources, CAP_MaxRowsRestriction %>" />
                </td>
                <td>
                    <asp:DropDownList ID="MaxRows" Width="70px" runat="server">
                        <asp:ListItem Value="" Selected="True">Нет</asp:ListItem>
                        <asp:ListItem Value="1">1</asp:ListItem>
                        <asp:ListItem Value="10">10</asp:ListItem>
                        <asp:ListItem Value="100">100</asp:ListItem>
                        <asp:ListItem Value="1000">1000</asp:ListItem>
                        <asp:ListItem Value="10000">10000</asp:ListItem>
                        <asp:ListItem Value="100000">100000</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <%--Открывать документ в новом окне--%>
                    <asp:Localize ID="LocalizeLoadDocInNewWindow" runat="server" Text="<%$ Resources:eqResources, CAP_LoadDocInNewWindow %>" />
                </td>
                <td>
                    <asp:CheckBox ID="LoadDocInNewWindow" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <%--Открывать документ кликом по всей строке--%>
                    <asp:Localize ID="LocalizeLoadDocOnRowClick" runat="server" Text="<%$ Resources:eqResources, CAP_LoadDocOnRowClick %>" />
                </td>
                <td>
                    <asp:CheckBox ID="LoadDocOnRowClick" runat="server" />
                </td>
            </tr>
        </table>
    </ContentPanel>
</eq:eqCollapsiblePanel>
<div class="LogicBlockCaption">
    <%--Поля--%>
    <asp:Localize ID="LocalizeFields" runat="server" Text="<%$ Resources:eqResources, CAP_Fields %>" />
</div>
<asp:UpdatePanel ID="upeqViewFields" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <eq:eqSpecification ID="eqViewFields" runat="server" Width="100%" AllowDelete="false"
            AllowInsert="false">
            <eq:eqSpecificationColumn HeaderText="<%$ Resources:eqResources, CAP_Field %>" Name="FieldsColumn">
                <ItemTemplate>
                    <asp:HiddenField ID="FieldID" runat="server" />
                    <eq:eqTextBox ID="Header" runat="server" />
                </ItemTemplate>
            </eq:eqSpecificationColumn>
            <eq:eqSpecificationColumn HeaderText="<%$ Resources:eqResources, CAP_Filter %>" Name="Filter">
                <ItemTemplate>
                    <div style="vertical-align: middle;">
                        <asp:HiddenField ID="Operator1" runat="server" Visible="false" />
                        <asp:HiddenField ID="Filter1" runat="server" Visible="false" />
                        <asp:HiddenField ID="Logic12" runat="server" Visible="false" />
                        <asp:HiddenField ID="Operator2" runat="server" Visible="false" />
                        <asp:HiddenField ID="Filter2" runat="server" Visible="false" />
                        <ldf:ListDocFilter ID="Filter" runat="server" OnFilterChanged="Filter_OnFilterChanged"
                            ShowFilterText="true" />
                    </div>
                </ItemTemplate>
            </eq:eqSpecificationColumn>
            <eq:eqSpecificationColumn HeaderText="<%$ Resources:eqResources, CAP_Aggregate %>" Width="90px" Name="AggregateColumn">
                <ItemTemplate>
                    <asp:DropDownList ID="AggregateType" runat="server">
                        <asp:ListItem Text="" Value="" />
                        <asp:ListItem Text="<%$ Resources:eqResources, CAP_Count %>" Value="COUNT" />
                        <asp:ListItem Text="<%$ Resources:eqResources, CAP_Sum %>" Value="SUM" />
                        <asp:ListItem Text="<%$ Resources:eqResources, CAP_Minimum %>" Value="MIN" />
                        <asp:ListItem Text="<%$ Resources:eqResources, CAP_Maximum %>" Value="MAX" />
                        <asp:ListItem Text="<%$ Resources:eqResources, CAP_Averange %>" Value="AVG" />
                    </asp:DropDownList>
                </ItemTemplate>
            </eq:eqSpecificationColumn>
            <eq:eqSpecificationColumn HeaderText="<%$ Resources:eqResources, CAP_ColumnWidth %>" Width="100px" Name="WidthColumn">
                <ItemTemplate>
                    <eq:eqTextBox ID="Width" runat="server" />
                </ItemTemplate>
            </eq:eqSpecificationColumn>
            <eq:eqSpecificationColumn Width="100px" Name="VisibleColumn">
                <HeaderTemplate>
                    <asp:CheckBox ID="HeaderEnabled" runat="server" AutoPostBack="true" OnCheckedChanged="HeaderEnabled_OnCheckedChanged" />
                    <%--Отображать--%>
                    <asp:Localize ID="LocalizeDisplay" runat="server" Text="<%$ Resources:eqResources, CAP_Display %>" />
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:CheckBox ID="Enabled" runat="server" />
                </ItemTemplate>
            </eq:eqSpecificationColumn>
        </eq:eqSpecification>
    </ContentTemplate>
</asp:UpdatePanel>
<div class="LogicBlockCaption" id="diveqViewConditions" runat="server">
    <%--Дополнительные условия--%>
    <asp:Localize ID="LocalizeAdditionalConditions" runat="server" Text="<%$ Resources:eqResources, CAP_AdditionalConditions %>" />
</div>
<asp:UpdatePanel ID="upeqViewConditions" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <eq:eqSpecification ID="eqViewConditions" runat="server" Width="100%">
            <eq:eqSpecificationColumn Width="60px">
                <ItemTemplate>
                    <eq:eqDropDownList ID="Logic" runat="server">
                        <asp:ListItem Text="ИЛИ" Value="OR" />
                        <asp:ListItem Text="И" Value="AND" />
                    </eq:eqDropDownList>
                </ItemTemplate>
            </eq:eqSpecificationColumn>
            <eq:eqSpecificationColumn HeaderText="<%$ Resources:eqResources, CAP_Field %>">
                <ItemTemplate>
                    <eq:eqDropDownList ID="FieldID" runat="server" AutoPostBack="true" />
                </ItemTemplate>
            </eq:eqSpecificationColumn>
            <eq:eqSpecificationColumn HeaderText="<%$ Resources:eqResources, CAP_Filter %>">
                <ItemTemplate>
                    <div style="vertical-align: middle;">
                        <asp:HiddenField ID="Operator1" runat="server" Visible="false" />
                        <asp:HiddenField ID="Filter1" runat="server" Visible="false" />
                        <asp:HiddenField ID="Logic12" runat="server" Visible="false" />
                        <asp:HiddenField ID="Operator2" runat="server" Visible="false" />
                        <asp:HiddenField ID="Filter2" runat="server" Visible="false" />
                        <ldf:ListDocFilter ID="Filter" runat="server" OnFilterChanged="Filter_OnFilterChanged"
                            ShowFilterText="true" />
                    </div>
                </ItemTemplate>
            </eq:eqSpecificationColumn>
        </eq:eqSpecification>
    </ContentTemplate>
</asp:UpdatePanel>
<div class="LogicBlockCaption" id="diveqViewFunctions" runat="server">
    <%--Функции--%>
    <asp:Localize ID="LocalizeFunctions" runat="server" Text="<%$ Resources:eqResources, CAP_Functions %>" />
</div>
<asp:UpdatePanel ID="upeqViewFunctions" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <eq:eqSpecification ID="eqViewFunctions" runat="server" Width="100%">
            <eq:eqSpecificationColumn HeaderText="<%$ Resources:eqResources, CAP_FunctionSelect %>">
                <ItemTemplate>
                    <eq:eqDropDownList ID="FunctionsID" runat="server" AutoPostBack="true" OnSelectedIndexChanged="FunctionsID_OnSelectedIndexChanged" />
                </ItemTemplate>
            </eq:eqSpecificationColumn>
            <eq:eqSpecificationColumn HeaderText="<%$ Resources:eqResources, CAP_Function %>">
                <ItemTemplate>
                    <asp:HiddenField ID="Parameter" runat="server" />
                    <asp:CheckBox ID="Enabled" runat="server" Visible="false" />
                    <ldf:ListDocFunction ID="Function" runat="server" OnFunctionChanged="Function_OnFunctionChanged"
                        Visible="false" />
                </ItemTemplate>
            </eq:eqSpecificationColumn>
        </eq:eqSpecification>
    </ContentTemplate>
</asp:UpdatePanel>
<div class="LogicBlockCaption" id="divSort" runat="server">
    <%--Сортировка--%>
    <asp:Localize ID="LocalizeSorting" runat="server" Text="<%$ Resources:eqResources, CAP_Sorting %>" />
</div>
<asp:UpdatePanel ID="upeqViewSorting" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <eq:eqSpecification ID="eqViewSorting" runat="server" Width="100%">
            <eq:eqSpecificationColumn HeaderText="<%$ Resources:eqResources, CAP_Field %>">
                <ItemTemplate>
                    <eq:eqDropDownList ID="FieldID" runat="server" UniqueGroupName="eqViewSorting" />
                </ItemTemplate>
            </eq:eqSpecificationColumn>
            <eq:eqSpecificationColumn HeaderText="<%$ Resources:eqResources, CAP_SortDirection %>" Width="180px">
                <ItemTemplate>
                    <asp:RadioButtonList ID="SortDirection" runat="server" RepeatDirection="Horizontal"
                        RepeatLayout="Flow">
                        <asp:ListItem Text="<%$ Resources:eqResources, CAP_SortDirectionAscending %>" Value="ASC" Selected="True" />
                        <asp:ListItem Text="<%$ Resources:eqResources, CAP_SortDirectionDescending %>" Value="DESC" />
                    </asp:RadioButtonList>
                </ItemTemplate>
            </eq:eqSpecificationColumn>
        </eq:eqSpecification>
    </ContentTemplate>
</asp:UpdatePanel>
<div class="LogicBlockCaption" id="divGroup" runat="server">
    <%--Группировка--%>
    <asp:Localize ID="LocalizeGroupping" runat="server" Text="<%$ Resources:eqResources, CAP_Groupping %>" />
</div>
<asp:UpdatePanel ID="upeqViewGroupping" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <eq:eqSpecification ID="eqViewGroupping" runat="server" Width="100%">
            <eq:eqSpecificationColumn HeaderText="<%$ Resources:eqResources, CAP_Field %>">
                <ItemTemplate>
                    <eq:eqDropDownList ID="FieldID" runat="server" UniqueGroupName="eqViewGroupping" />
                </ItemTemplate>
            </eq:eqSpecificationColumn>
            <eq:eqSpecificationColumn HeaderText="<%$ Resources:eqResources, CAP_GroupExpanded %>" Width="180px">
                <ItemTemplate>
                    <asp:CheckBox ID="GroupExpanded" runat="server" />
                </ItemTemplate>
            </eq:eqSpecificationColumn>
        </eq:eqSpecification>
    </ContentTemplate>
</asp:UpdatePanel>
<div class="LogicBlockCaption" id="divColorSelect" runat="server">
    <%--Выделение цветом--%>
    <asp:Localize ID="LocalizeViewPainting" runat="server" Text="<%$ Resources:eqResources, CAP_ViewPainting %>" />
</div>
<asp:UpdatePanel ID="upeqViewPainting" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <eq:eqSpecification ID="eqViewPainting" runat="server" Width="100%">
            <eq:eqSpecificationColumn HeaderText="<%$ Resources:eqResources, CAP_Field %>">
                <ItemTemplate>
                    <eq:eqDropDownList ID="FieldID" runat="server" AutoPostBack="true" />
                </ItemTemplate>
            </eq:eqSpecificationColumn>
            <eq:eqSpecificationColumn HeaderText="<%$ Resources:eqResources, CAP_Filter %>">
                <ItemTemplate>
                    <div style="vertical-align: middle;">
                        <asp:HiddenField ID="Operator1" runat="server" Visible="false" />
                        <asp:HiddenField ID="Filter1" runat="server" Visible="false" />
                        <asp:HiddenField ID="Logic12" runat="server" Visible="false" />
                        <asp:HiddenField ID="Operator2" runat="server" Visible="false" />
                        <asp:HiddenField ID="Filter2" runat="server" Visible="false" />
                        <ldf:ListDocFilter ID="Filter" runat="server" OnFilterChanged="Filter_OnFilterChanged"
                            ShowFilterText="true" />
                    </div>
                </ItemTemplate>
            </eq:eqSpecificationColumn>
            <eq:eqSpecificationColumn HeaderText="<%$ Resources:eqResources, CAP_FullRow %>" Width="70px">
                <ItemTemplate>
                    <asp:CheckBox ID="FullRow" runat="server" />
                </ItemTemplate>
            </eq:eqSpecificationColumn>
            <eq:eqSpecificationColumn HeaderText="<%$ Resources:eqResources, CAP_Color %>" Width="50px">
                <ItemTemplate>
                    <eq:eqColorPicker ID="Color" runat="server" Width="50px" Height="18px" BorderColor="Gray"
                        BorderStyle="Solid" BorderWidth="1px" />
                </ItemTemplate>
            </eq:eqSpecificationColumn>
        </eq:eqSpecification>
    </ContentTemplate>
</asp:UpdatePanel>
<%--<aj:TabContainer ID="TabContainerSetups" runat="server">
    <aj:TabPanel ID="TabPanelCommon" runat="server" HeaderText="Общие">
        <ContentTemplate>
        </ContentTemplate>
    </aj:TabPanel>
    <aj:TabPanel ID="TabPanelFields" runat="server" HeaderText="Столбцы">
        <ContentTemplate>
            
        </ContentTemplate>
    </aj:TabPanel>
    <aj:TabPanel ID="TabPanelFilters" runat="server" HeaderText="Фильтры">
        <ContentTemplate>
            
        </ContentTemplate>
    </aj:TabPanel>
    <aj:TabPanel ID="TabPanelGroupping" runat="server" HeaderText="Группировка">
        <ContentTemplate>
        </ContentTemplate>
    </aj:TabPanel>
    <aj:TabPanel ID="TabPanelSorting" runat="server" HeaderText="Сортировка">
        <ContentTemplate>
        </ContentTemplate>
    </aj:TabPanel>
    <aj:TabPanel ID="TabPanelAggregates" runat="server" HeaderText="Агрегаты">
        <ContentTemplate>
        </ContentTemplate>
    </aj:TabPanel>
    <aj:TabPanel ID="TabPanelPainting" runat="server" HeaderText="Раскраска">
        <ContentTemplate>
        </ContentTemplate>
    </aj:TabPanel>
</aj:TabContainer>--%>
