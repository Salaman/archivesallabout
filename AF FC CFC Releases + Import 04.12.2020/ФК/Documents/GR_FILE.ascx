﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="GR_FILE.ascx.vb" Inherits="WebApplication.GR_FILE"
    Explicit="true" %>
    <table style="width: 97%; margin-bottom: 4px;">
    <tr>
        
        <td style="text-align: right;">
           
            <asp:HyperLink ID="HyperLinkDOCDESCRIPTs" runat="server" Text="Листы описания" Target="_blank"
                NavigateUrl="~/Default.aspx?DocTypeURL=GR_DOC_DESCRIPT_LIST.ascx&ListDoc=True" Style="font-weight: bold;
                padding: 2px;" />
        </td>
    </tr>
</table>
<table style="width: 700px;">
    <colgroup>
        <col width="150px" />
        <col />
    </colgroup>
    <tr>
        <td>
            Дело №*
        </td>
        <td>
            <eq:eqTextBox ID="NUMBER" runat="server" Height="30px" Width="185px"></eq:eqTextBox>
        </td>
        <td>
            Заголовок дела*
        </td>
        <td>
            <eq:eqTextBox ID="NAME" runat="server" Height="30px" Width="185px"></eq:eqTextBox>
        </td>
    </tr>
</table>
<table style="width: 700px;">
    <col style="width: 200px;" />
    <col />
    <tr>
        <td>
          <fieldset>
                <legend>Том*</legend>
                <%--СПЕЦИФИКАЦИЯ dbo.GR_tblVOLUME--%>
                <eq:eqSpecification ID="GR_tblVOLUME" runat="server" ShowNumering="false">
                    <eq:eqSpecificationColumn HeaderText="№ тома" Width="100px">
                        <ItemTemplate>
                            <eq:eqTextBox ID="NUMBER" runat="server" TextMode="MultiLine" />
                        </ItemTemplate>
                    </eq:eqSpecificationColumn>
                    <eq:eqSpecificationColumn HeaderText="Год" Width="100px">
                        <ItemTemplate>
                            <eq:eqTextBox ID="YEAR" runat="server" TextMode="MultiLine" />
                        </ItemTemplate>
                    </eq:eqSpecificationColumn>
                    <eq:eqSpecificationColumn HeaderText="Номера листов описания" Width="100px">
                        <ItemTemplate>
                            <eq:eqTextBox ID="DOC_NUMBERS" runat="server" TextMode="MultiLine" />
                        </ItemTemplate>
                    </eq:eqSpecificationColumn>
                </eq:eqSpecification>
            </fieldset>
        </td>
    </tr>
</table>
