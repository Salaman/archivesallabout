﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="SEARCH_CLS.ascx.vb" Inherits="WebApplication.SEARCH_CLS" %>
<%@ Register Src="../ListDoc.ascx" TagName="ListDoc" TagPrefix="uc1" %>
<table>
    <tr>
        <td valign="top" style="width: 300px;">
            <table cellspacing="0">
                <tr>
                    <td class="MenuItemStyle MenuItemStyleSelected">
                        АНСА межфондовый
                    </td>
                </tr>
            </table>
            <asp:UpdatePanel ID="UpdatePanelCLS" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <uc1:ListDoc ID="ListDocCLS" runat="server" DocTypeURL="CLS701.ascx" SelectDocMode="Single" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>
        <td valign="top">
            <asp:UpdatePanel ID="UpdatePanelRESULT" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <table>
                        <tr>
                            <td>
                                <eq:eqMenu ID="MenuRESULT" runat="server" Orientation="Horizontal" StaticEnableDefaultPopOutImage="False"
                                    ItemWrap="false">
                                    <Items>
                                        <asp:MenuItem Text="Фонд" Value="FUND.ascx" Selected="true" />
                                        <asp:MenuItem Text="Опись" Value="INVENTORY.ascx" />
                                        <asp:MenuItem Text="Единица хранения / Единица учета" Value="UNIT.ascx" />
                                        <asp:MenuItem Text="Документ" Value="DOCUMENT.ascx" />
                                    </Items>
                                    <StaticSelectedStyle CssClass="MenuItemStyleSelected" />
                                </eq:eqMenu>
                            </td>
                            <td>
                                <asp:Button ID="ButtonDeattach" runat="server"  Text="Открепить выбранные" />
                            </td>
                        </tr>
                    </table>
                    <asp:PlaceHolder ID="PlaceHolderRESULT" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>
    </tr>
</table>
