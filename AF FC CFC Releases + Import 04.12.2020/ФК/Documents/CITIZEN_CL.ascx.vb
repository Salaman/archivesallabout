﻿Public Partial Class CITIZEN_CL
    Inherits BaseDoc

    Public Overrides Function ReadValueFromHeaderControlToDataSet(ByVal FieldName As String, Optional ByVal ToReadOnly As Boolean = False) As Boolean
        Dim d As Date, v As String
        If FieldName = "BIRTH_DATE_FMT" Then
            If DateTime.TryParse(BIRTH_DATE_FMT0.Text, d) Then
                v = d.ToString("yyyyMMdd")
            Else
                v = BIRTH_DATE_FMT0.Text
            End If
            DataSetHeaderDataRow("BIRTH_DATE") = v
            DataSetHeaderDataRow("BIRTH_DATE_FMT") = BIRTH_DATE_FMT0.Text 'Roman 260713
        ElseIf FieldName = "DEATH_DATE_FMT" Then
            If DateTime.TryParse(DEATH_DATE_FMT0.Text, d) Then
                v = d.ToString("yyyyMMdd")
            Else
                v = DEATH_DATE_FMT0.Text
            End If
            DataSetHeaderDataRow("DEATH_DATE") = v
            DataSetHeaderDataRow("DEATH_DATE_FMT") = DEATH_DATE_FMT0.Text 'Roman 260713
        End If
        Return MyBase.ReadValueFromHeaderControlToDataSet(FieldName, ToReadOnly)
    End Function

    Private Sub BIRTH_DATE_FMT0_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles BIRTH_DATE_FMT0.Load
        If IsDBNull(DataSetHeaderDataRow("BIRTH_DATE_FMT")) Then
            BIRTH_DATE_FMT0.Text = ""
        Else
            BIRTH_DATE_FMT0.Text = DataSetHeaderDataRow("BIRTH_DATE_FMT")
        End If
        If IsDBNull(DataSetHeaderDataRow("DEATH_DATE_FMT")) Then
            DEATH_DATE_FMT0.Text = ""
        Else
            DEATH_DATE_FMT0.Text = DataSetHeaderDataRow("DEATH_DATE_FMT")
        End If
        '                                                               Roman 260713 Была проблема: не сохранялись даты в этой карточке
        '                                                               Поле DEATH_DATE_FMT.Text возвращало "".
        '                                                             ' Через Администрирование в веб-интерфейсе проблема не решалась,
    End Sub '                                                         ' т.к. если сделать это поле не ReadOnly (в следствие чего и возвращалась пустая строка),
    '                                                                 ' то вылезала ошибка инсерта в vCITIZEN_CL "because it contains a derived or constant field. -> в Equipage.Core.eqDocType.SaveDoc(DataSet& DS) ".
    '                                                                 ' Возможно, есть способ решить эту проблему проще и изящнее, но я решил не заморачиваться.
End Class