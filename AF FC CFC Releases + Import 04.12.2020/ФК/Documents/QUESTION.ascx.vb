﻿Imports System.Data
Imports System.Data.SqlClient

Partial Public Class QUESTION
    Inherits BaseDoc

#Region "События"

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        MultiViewTabs.ActiveViewIndex = CInt(Val(MenuTabs.SelectedValue))
    End Sub

#End Region

#Region "Документ"

    Public Overrides Function SaveDoc() As Boolean
        Dim bRUBRIK = vREF_CLS_QUESTION.Rows.Length = 0
        Dim bOBJ = vREF_QUESTION_FUND.Rows.Length = 0 AndAlso vREF_QUESTION_INVENTORY.Rows.Length = 0 AndAlso vREF_QUESTION_UNIT.Rows.Length = 0 AndAlso vREF_QUESTION_DOCUMENT.Rows.Length = 0
        If bRUBRIK And bOBJ Then
            AddMessage("Необходимо выбрать не менее одной рубрики и не менее одного объекта учета")
            Return False
        ElseIf bRUBRIK Then
            AddMessage("Необходимо выбрать не менее одной рубрики")
            Return False
        ElseIf bOBJ Then
            AddMessage("Необходимо выбрать не менее одного объекта учета")
            Return False
        End If

        Return MyBase.SaveDoc()
    End Function

#End Region

End Class