﻿Imports WordReportGenerator
Imports WordReportGenerator.WordReportGenerator
Imports WordReportGenerator.WordReportTreeSupport

Partial Public Class WordReports
    Inherits BaseDoc

#Region "Property"

    Private ReadOnly Property Prop(ByVal p As Object) As Object
        Get
            Dim value As Object
            If TypeOf p Is Equipage.WebUI.eqTextBox Then
                value = DirectCast(p, Equipage.WebUI.eqTextBox).Text
                If IsNumeric(value) Then Return CInt(value)
            ElseIf TypeOf p Is WebApplication.WebUI.eqDocument Then
                value = DirectCast(p, WebApplication.WebUI.eqDocument).KeyValue
                If value IsNot Nothing Then Return CLng(value)
            ElseIf TypeOf p Is DropDownList Then
                value = DirectCast(p, DropDownList).SelectedValue
                If IsNumeric(value) Then Return CInt(value)
            ElseIf TypeOf p Is CheckBox Then
                value = DirectCast(p, CheckBox).Checked
                Return CBool(value)
            Else
                value = p.ToString()
                Return value
            End If
            Return 0
        End Get
    End Property

    Private ReadOnly Property Secur() As Integer
        Get
            Dim s = 0
            If Prop_SecurityFlags.Items(0).Selected Then s += 1
            If Prop_SecurityFlags.Items(1).Selected Then s += 2
            If Prop_SecurityFlags.Items(2).Selected Then s += 4
            Return s
        End Get
    End Property

#End Region

#Region "Events"

    Public Sub Rep_Changed(ByVal sender As Object, ByVal e As System.EventArgs)
        For Each c In Panel_Properties.Controls
            If TypeOf c Is Panel Then
                DirectCast(c, Panel).Visible = False
            End If
        Next
        Panel_SecurityFlags.Visible = True
        If Rep_PassportArchiveStore.Checked Then ' Паспорт архивохранилища
            Panel_ISN_LOCATION.Visible = True
        ElseIf Rep_LocationStory.Checked Then ' Номерник архивохранилища
            Panel_ISN_LOCATION.Visible = True
        ElseIf Rep_PassportArchive.Checked Then ' Паспорт архива
            Panel_ISN_PASSPORT.Visible = True
        ElseIf Rep_ArchStory.Checked Then ' Номерник архива
        ElseIf Rep_ArchiveDynamicsa_Abs.Checked Then ' Динамика паспорта архива (абсолютная)
            Panel_ISN_PASSPORT2.Visible = True
        ElseIf Rep_ArchiveDynamics_Per.Checked Then ' Динамика  паспорта архива (в процентах)
            Panel_ISN_PASSPORT2.Visible = True
        ElseIf Rep_TraceableInfoArchiveList.Checked Then ' Сводные сведения по архиву
        ElseIf Rep_PhysicalStateDoc.Checked Then ' Физическое состояние документов
        ElseIf Rep_UndetectedUnitStorage.Checked Then ' Необнаруженные единицы хранения
        ElseIf Rep_NsaContent.Checked Then ' Состав НСА к документам на бумажной основе
        ElseIf Rep_CompositionFund_1.Checked Then ' Состав фондов 1 категории
        ElseIf Rep_CompositionFund_2.Checked Then ' Состав фондов 2 категории
        ElseIf Rep_CompositionFund_3.Checked Then ' Состав фондов 3 категории
        ElseIf Rep_CompositionFund_None.Checked Then ' Состав некатегорированных фондов
        ElseIf Rep_CompositionFund_Any.Checked Then ' Состав фондов архива
        ElseIf Rep_FundList.Checked Then ' Список фондов
            Panel_Year2.Visible = True
        ElseIf Rep_FundListImportantPlace.Checked Then ' Список фондов, содержащих ОЦД
        ElseIf Rep_CompositionFundList.Checked Then ' Состав фонда
            Panel_ISN_FUND.Visible = True
        ElseIf Rep_InfoChangeFundsList.Checked Then ' Сведения об изменениях в составе и объеме фондов
            Panel_Period.Visible = True
            Panel_Year.Visible = True
        ElseIf Rep_ArchList.Checked Then ' Архивная опись
            Panel_ISN_INVENTORY.Visible = True
        ElseIf Rep_InvVideoDok.Checked Then ' Опись видеодокументов
            Panel_ISN_INVENTORY.Visible = True
        ElseIf Rep_InvKinoDok.Checked Then ' Опись кинодокументов
            Panel_ISN_INVENTORY.Visible = True
        ElseIf Rep_InvFonoDokGramophoneRec.Checked Then ' Опись фонодокументов граммофонной записи (грампластинок)
            Panel_ISN_INVENTORY.Visible = True
        ElseIf Rep_InvFonoDokMagneticRec.Checked Then ' Опись фонодокументов магнитной записи
            Panel_ISN_INVENTORY.Visible = True
        ElseIf Rep_InvPhotographAlbum.Checked Then ' Опись фотоальбомов
            Panel_ISN_INVENTORY.Visible = True
        ElseIf Rep_InvPhotoDok.Checked Then ' Опись фотодокументов
            Panel_ISN_INVENTORY.Visible = True
        ElseIf Rep_InvElectronDok.Checked Then ' Опись электронных документов
            Panel_ISN_INVENTORY.Visible = True
        ElseIf Rep_ReestrOpisDel.Checked Then ' Реестр описей дел, документов
            Panel_Year.Visible = True
        ElseIf Rep_InvImportantPlace.Checked Then ' Опись ОЦД
        ElseIf Rep_ListImportantPlaceFund.Checked Then ' Перечень ОЦД в фонде (аналог описи ОЦД без заголовков)
            Panel_ISN_FUND.Visible = True
        ElseIf Rep_DocumDeclassifyList.Checked Then ' Перечень рассекреченных документов
        ElseIf Rep_CardDeclassifyDoc.Checked Then ' Карточка рассекреченного документа
            Panel_ISN_UNIT.Visible = True
        ElseIf Rep_InnerList.Checked Then ' Внутренняя опись документов дела
            Panel_ISN_UNIT.Visible = True
        ElseIf Rep_CompositionDoc_1.Checked Then ' Состав документов 1 категории
        ElseIf Rep_CompositionDoc_2.Checked Then ' Состав документов 2 категории
        ElseIf Rep_CompositionDoc_3.Checked Then ' Состав документов 3 категории
        ElseIf Rep_CompositionDoc_None.Checked Then ' Состав документов в некатегорированных фондах
        ElseIf Rep_CompositionDoc_Any.Checked Then ' Состав документов архива
        ElseIf Rep_FundGuide.Checked Then ' Путеводитель по фондам архива
            Panel_ISN_CLS.Visible = True
            Panel_Children.Visible = True
            Panel_Kind2.Visible = True
            Panel_Year.Visible = True
        ElseIf Rep_ShortGuide.Checked Then ' Краткий справочник по фондам архива
            Panel_ISN_CLS.Visible = True
            Panel_Children.Visible = True
            Panel_Kind2.Visible = True
            Panel_Year.Visible = True
        ElseIf Rep_ShortGuideListFund.Checked Then ' Краткий справочник (список фондов) по фондам архива
            Panel_ISN_CLS.Visible = True
            Panel_Children.Visible = True
            Panel_Kind2.Visible = True
            Panel_Year.Visible = True
        ElseIf Rep_Index.Checked Then ' Указатель
            Panel_ISN_FUND.Visible = True
            Panel_ISN_CLS.Visible = True
            Panel_Children.Visible = True
            Panel_Kind2.Visible = True
        ElseIf Rep_TopIndex.Checked Then ' Постеллажный топографический указатель
            Panel_ISN_LOCATION.Visible = True
        ElseIf Rep_FundTopIndex.Checked Then ' Пофондовый топографический указатель
            Panel_ISN_FUND.Visible = True
        ElseIf Rep_Check.Checked Then ' Проверка правильности и полноты заполнения БД «Архивный фонд»
            Panel_Check.Visible = True
        End If
    End Sub

    Private Sub LoadProperties()
        Try
            If Request.QueryString("ISN_ARCHIVE") IsNot Nothing Then
            ElseIf Request.QueryString("ISN_FUND") IsNot Nothing Then
                Prop_ISN_FUND.KeyValue = Request.QueryString("ISN_FUND")
            ElseIf Request.QueryString("ISN_INVENTORY") IsNot Nothing Then
                Prop_ISN_INVENTORY.KeyValue = Request.QueryString("ISN_INVENTORY")
            ElseIf Request.QueryString("ISN_UNIT") IsNot Nothing Then
                Prop_ISN_UNIT.KeyValue = Request.QueryString("ISN_UNIT")
            ElseIf Request.QueryString("ISN_LOCATION") IsNot Nothing Then
                Prop_ISN_LOCATION.KeyValue = Request.QueryString("ISN_LOCATION")
            ElseIf Request.QueryString("ISN_PASSPORT") IsNot Nothing Then
                Prop_ISN_PASSPORT.KeyValue = Request.QueryString("ISN_PASSPORT")
            End If
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Rep_Changed(Nothing, EventArgs.Empty)
            LoadProperties()
        End If
    End Sub

#End Region

    Public Sub MakeReport()
        Dim Report As WordReport = Nothing

        Try
            If Rep_PassportArchiveStore.Checked Then ' Паспорт архивохранилища
                UpdateTreeSupport(AppSettings.Cnn, Kind.LOCATION, True)
                Report = FillReport_PassportArchiveStore(AppSettings.Cnn, Secur, Prop(Prop_ISN_LOCATION))
            ElseIf Rep_LocationStory.Checked Then ' Номерник архивохранилища
                UpdateTreeSupport(AppSettings.Cnn, Kind.LOCATION, True)
                Report = FillReport_ArchStory(AppSettings.Cnn, Secur, Prop(Prop_ISN_LOCATION), True)
            ElseIf Rep_PassportArchive.Checked Then ' Паспорт архива
                Report = FillReport_PassportArchive(AppSettings.Cnn, Secur, Prop(Prop_ISN_PASSPORT), True)
            ElseIf Rep_ArchStory.Checked Then ' Номерник архива
                Report = FillReport_ArchStory(AppSettings.Cnn, Secur, 0, True)
            ElseIf Rep_ArchiveDynamicsa_Abs.Checked Then ' Динамика паспорта архива (абсолютная)
                Report = FillReport_ArchiveDynamics(AppSettings.Cnn, Secur, Prop(Prop_ISN_PASSPORT1), Prop(Prop_ISN_PASSPORT2), False)
            ElseIf Rep_ArchiveDynamics_Per.Checked Then ' Динамика  паспорта архива (в процентах)
                Report = FillReport_ArchiveDynamics(AppSettings.Cnn, Secur, Prop(Prop_ISN_PASSPORT1), Prop(Prop_ISN_PASSPORT2), True)
            ElseIf Rep_TraceableInfoArchiveList.Checked Then ' Сводные сведения по архиву
                Report = FillReport_TraceableInfoArchiveList(AppSettings.Cnn, Secur)
            ElseIf Rep_PhysicalStateDoc.Checked Then ' Физическое состояние документов
                Report = FillReport_PhysicalStateDoc(AppSettings.Cnn, Secur)
            ElseIf Rep_UndetectedUnitStorage.Checked Then ' Необнаруженные единицы хранения
                Report = FillReport_UndetectedUnitStorage(AppSettings.Cnn, Secur)
            ElseIf Rep_NsaContent.Checked Then ' Состав НСА к документам на бумажной основе
                Report = FillReport_NsaContent(AppSettings.Cnn, Secur)
            ElseIf Rep_CompositionFund_1.Checked Then ' Состав фондов 1 категории
                Report = FillReport_CompositionFund(AppSettings.Cnn, Secur, WordReportEnum.Category.a)
            ElseIf Rep_CompositionFund_2.Checked Then ' Состав фондов 2 категории
                Report = FillReport_CompositionFund(AppSettings.Cnn, Secur, WordReportEnum.Category.b)
            ElseIf Rep_CompositionFund_3.Checked Then ' Состав фондов 3 категории
                Report = FillReport_CompositionFund(AppSettings.Cnn, Secur, WordReportEnum.Category.c)
            ElseIf Rep_CompositionFund_None.Checked Then ' Состав некатегорированных фондов
                Report = FillReport_CompositionFund(AppSettings.Cnn, Secur, WordReportEnum.Category.d)
            ElseIf Rep_CompositionFund_Any.Checked Then ' Состав фондов архива
                Report = FillReport_CompositionFund(AppSettings.Cnn, Secur, WordReportEnum.Category.any)
            ElseIf Rep_FundList.Checked Then ' Список фондов
                Report = FillReport_FundList(AppSettings.Cnn, Secur, Prop(Prop_Year1), Prop(Prop_Year2))
            ElseIf Rep_FundListImportantPlace.Checked Then ' Список фондов, содержащих ОЦД
                Report = FillReport_FundListImportantPlace(AppSettings.Cnn, Secur)
            ElseIf Rep_CompositionFundList.Checked Then ' Состав фонда
                Report = FillReport_CompositionFundList(AppSettings.Cnn, Secur, Prop(Prop_ISN_FUND))
            ElseIf Rep_InfoChangeFundsList.Checked Then ' Сведения об изменениях в составе и объеме фондов
                Report = FillReport_InfoChangeFundsList(AppSettings.Cnn, Secur, Prop(Prop_Period), Prop(Prop_Year))
            ElseIf Rep_ArchList.Checked Then ' Архивная опись
                Report = FillReport_ArchList(AppSettings.Cnn, Secur, Prop(Prop_ISN_INVENTORY), "")
            ElseIf Rep_InvVideoDok.Checked Then ' Опись видеодокументов
                Report = FillReport_InvVideoDok(AppSettings.Cnn, Secur, Prop(Prop_ISN_INVENTORY))
            ElseIf Rep_InvKinoDok.Checked Then ' Опись кинодокументов
                Report = FillReport_InvKinoDok(AppSettings.Cnn, Secur, Prop(Prop_ISN_INVENTORY))
            ElseIf Rep_InvFonoDokGramophoneRec.Checked Then ' Опись фонодокументов граммофонной записи (грампластинок)
                Report = FillReport_InvFonoDokGramophoneRec(AppSettings.Cnn, Secur, Prop(Prop_ISN_INVENTORY))
            ElseIf Rep_InvFonoDokMagneticRec.Checked Then ' Опись фонодокументов магнитной записи
                Report = FillReport_InvFonoDokMagneticRec(AppSettings.Cnn, Secur, Prop(Prop_ISN_INVENTORY))
            ElseIf Rep_InvPhotographAlbum.Checked Then ' Опись фотоальбомов
                Report = FillReport_InvPhotographAlbum(AppSettings.Cnn, Secur, Prop(Prop_ISN_INVENTORY))
            ElseIf Rep_InvPhotoDok.Checked Then ' Опись фотодокументов
                Report = FillReport_InvPhotoDok(AppSettings.Cnn, Secur, Prop(Prop_ISN_INVENTORY))
            ElseIf Rep_InvElectronDok.Checked Then ' Опись электронных документов
                Report = FillReport_InvElectronDok(AppSettings.Cnn, Secur, Prop(Prop_ISN_INVENTORY))
            ElseIf Rep_ReestrOpisDel.Checked Then ' Реестр описей дел, документов
                Report = FillReport_ReestrOpisDel(AppSettings.Cnn, Secur, Prop(Prop_Year))
            ElseIf Rep_InvImportantPlace.Checked Then ' Опись ОЦД
                Report = FillReport_InvImportantPlace(AppSettings.Cnn, Secur)
            ElseIf Rep_ListImportantPlaceFund.Checked Then ' Перечень ОЦД в фонде (аналог описи ОЦД без заголовков)
                Report = FillReport_ListImportantPlaceFund(AppSettings.Cnn, Secur, Prop(Prop_ISN_FUND))
            ElseIf Rep_DocumDeclassifyList.Checked Then ' Перечень рассекреченных документов
                Report = FillReport_DocumDeclassifyList(AppSettings.Cnn, Secur)
            ElseIf Rep_CardDeclassifyDoc.Checked Then ' Карточка рассекреченного документа
                Report = FillReport_CardDeclassifyDoc(AppSettings.Cnn, Secur, Prop(Prop_ISN_UNIT))
            ElseIf Rep_InnerList.Checked Then ' Внутренняя опись документов дела
                Report = FillReport_InnerList(AppSettings.Cnn, Secur, Prop(Prop_ISN_UNIT))
            ElseIf Rep_CompositionDoc_1.Checked Then ' Состав документов 1 категории
                Report = FillReport_CompositionDoc(AppSettings.Cnn, Secur, WordReportEnum.Category.a)
            ElseIf Rep_CompositionDoc_2.Checked Then ' Состав документов 2 категории
                Report = FillReport_CompositionDoc(AppSettings.Cnn, Secur, WordReportEnum.Category.b)
            ElseIf Rep_CompositionDoc_3.Checked Then ' Состав документов 3 категории
                Report = FillReport_CompositionDoc(AppSettings.Cnn, Secur, WordReportEnum.Category.c)
            ElseIf Rep_CompositionDoc_None.Checked Then ' Состав документов в некатегорированных фондах
                Report = FillReport_CompositionDoc(AppSettings.Cnn, Secur, WordReportEnum.Category.d)
            ElseIf Rep_CompositionDoc_Any.Checked Then ' Состав документов архива
                Report = FillReport_CompositionDoc(AppSettings.Cnn, Secur, WordReportEnum.Category.any)
            ElseIf Rep_FundGuide.Checked Then ' Путеводитель по фондам архива
                UpdateTreeSupport(AppSettings.Cnn, Kind.CLS, True)
                Report = FillReport_FundGuide(AppSettings.Cnn, Secur, False, Prop(Prop_ISN_CLS), Prop(Prop_Children), Prop(Prop_Kind2), Prop(Prop_Year))
            ElseIf Rep_ShortGuide.Checked Then ' Краткий справочник по фондам архива
                UpdateTreeSupport(AppSettings.Cnn, Kind.CLS, True)
                Report = FillReport_ShortGuide(AppSettings.Cnn, Secur, False, Prop(Prop_ISN_CLS), Prop(Prop_Children), Prop(Prop_Kind2), Prop(Prop_Year))
            ElseIf Rep_ShortGuideListFund.Checked Then ' Краткий справочник (список фондов) по фондам архива
                UpdateTreeSupport(AppSettings.Cnn, Kind.CLS, True)
                Report = FillReport_ShortGuideListFund(AppSettings.Cnn, Secur, False, Prop(Prop_ISN_CLS), Prop(Prop_Children), Prop(Prop_Kind2), Prop(Prop_Year))
            ElseIf Rep_Index.Checked Then ' Указатель
                UpdateTreeSupport(AppSettings.Cnn, Kind.CLS, True)
                Report = FillReport_Index(AppSettings.Cnn, Secur, Prop(Prop_ISN_FUND), Prop(Prop_ISN_CLS), Prop(Prop_Children), Prop(Prop_Kind))
            ElseIf Rep_TopIndex.Checked Then ' Постеллажный топографический указатель
                UpdateTreeSupport(AppSettings.Cnn, Kind.LOCATION, True)
                Report = FillReport_TopIndex(AppSettings.Cnn, Secur, False, Prop(Prop_ISN_LOCATION))
            ElseIf Rep_FundTopIndex.Checked Then ' Пофондовый топографический указатель
                UpdateTreeSupport(AppSettings.Cnn, Kind.LOCATION, True)
                Report = FillReport_FundTopIndex(AppSettings.Cnn, Secur, False, Prop(Prop_ISN_FUND))
            ElseIf Rep_Check.Checked Then ' Проверка правильности и полноты заполнения БД «Архивный фонд»
                Report = FillReport_Check(AppSettings.Cnn, Secur, Prop(Prop_Fund), Prop(Prop_FundPres), New Boolean() {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, Prop(Prop_Inventory), Prop(Prop_InvPres), New Boolean() {1, 1, 1, 1, 1, 1, 1, 1, 1})
            End If

            BasePage.AttachFile(Report.FileName, Report.Data)
        Catch ex As Exception
            AddMessage("Ошибка при построении отчета >> " & ex.Message)
        End Try
    End Sub

End Class