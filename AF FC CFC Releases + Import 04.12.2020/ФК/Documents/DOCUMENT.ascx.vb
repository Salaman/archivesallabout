﻿Imports System.Data
Imports System.Data.SqlClient

Partial Public Class DOCUMENT
    Inherits BaseDoc

#Region "События"

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If DocStateInfo.StateID = Guid.Empty Then
                Dim UnitSQL = <sql>
                    select tblUNIT.*
                    from tblUNIT
                    where tblUNIT.ID = @ID
                              </sql>
                Dim UnitCommand As New SqlCommand(UnitSQL.Value)
                UnitCommand.Parameters.AddWithValue("@ID", Crumbs.ISN_UNIT.DocID)
                Dim UnitDataTable = AppSettings.GetDataTable(UnitCommand)

                If UnitDataTable.Rows.Count > 0 Then
                    If UnitDataTable.Rows(0)("ISN_SECURLEVEL") < 3 Then ' откр, с
                        ISN_SECURLEVEL.SelectedValue = UnitDataTable.Rows(0)("ISN_SECURLEVEL")
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub Page_PreRender2(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Crumbs.ISN_UNIT.DocID <> Guid.Empty AndAlso Not IsDBNull(Crumbs.ISN_UNIT.DataSetHeaderDataRow("ISN_INVENTORY")) Then
            Crumbs.ISN_INVENTORY.KeyValue = Crumbs.ISN_UNIT.DataSetHeaderDataRow("ISN_INVENTORY")
            If Not IsDBNull(Crumbs.ISN_INVENTORY.DataSetHeaderDataRow("ISN_FUND")) Then
                Crumbs.ISN_FUND.KeyValue = Crumbs.ISN_INVENTORY.DataSetHeaderDataRow("ISN_FUND")
                If Not IsDBNull(Crumbs.ISN_FUND.DataSetHeaderDataRow("ISN_ARCHIVE")) Then
                    Crumbs.ISN_ARCHIVE.KeyValue = Crumbs.ISN_FUND.DataSetHeaderDataRow("ISN_ARCHIVE")
                End If
            End If
        End If
        MultiViewTabs.ActiveViewIndex = CInt(MenuTabs.SelectedValue)

        ' потому как ядро его блокирует, мы его какбы разблокируем
        DOCUM_DATE_FMT.ReadOnly = Not GetLock(LockID)
        EVENT_DATE_FMT.ReadOnly = Not GetLock(LockID)

        ScriptManager.RegisterStartupScript(Page, Me.GetType, "JS", "_updateUI();", True)
    End Sub

#End Region

#Region "Методы"

    Public Sub CalcOneDocument()
        If GetLock(LockID) Then
            Dim r As New Recalc(AppSettings)
            Dim page As Triplet = Nothing

            Try
                r.CalcDocument(NOTE.Text.Trim, page)
                PAGE_COUNT.Text = page.First.ToString
                PAGE_FROM.Text = page.Second.ToString
                PAGE_TO.Text = page.Third.ToString
            Catch ex As Exception
                AddMessage("Не удалось выполнить расчет: >> " & ex.Message)
            End Try
        End If
    End Sub

#End Region

#Region "Документ"

    Public Overrides Function NewDoc() As Boolean
        NewDoc = MyBase.NewDoc()
        If NewDoc Then
            If Request.QueryString("UNIT") IsNot Nothing Then
                Crumbs.ISN_UNIT.DocID = New Guid(Request.QueryString("UNIT"))
                DataSetHeaderDataRow("ISN_UNIT") = Crumbs.ISN_UNIT.KeyValue
                PutValueFromDataSetToHeaderControl(ISN_SECURLEVEL)
            End If
        End If
    End Function

#End Region

#Region "Контролы"

    Public Overrides Sub PutValueToControl(ByVal Value As Object, ByVal TargetControl As System.Web.UI.Control, Optional ByVal FieldFormat As String = "")
        If TypeOf TargetControl Is CheckBox Then
            DirectCast(TargetControl, CheckBox).Checked = (Value Is Nothing OrElse Value.ToString.Trim.ToUpper = "Y")
        Else
            MyBase.PutValueToControl(Value, TargetControl, FieldFormat)
        End If
    End Sub

    Public Overrides Function GetValueFromControl(ByVal SourceControl As System.Web.UI.Control) As Object
        If TypeOf SourceControl Is CheckBox Then
            Return IIf(DirectCast(SourceControl, CheckBox).Checked, "Y", "N")
        Else
            Return MyBase.GetValueFromControl(SourceControl)
        End If
    End Function

    Public Overrides Function ReadValueFromHeaderControlToDataSet(ByVal FieldName As String, Optional ByVal ToReadOnly As Boolean = False) As Boolean
        Dim f As eqTextBox, d As Date, v As String
        If FieldName = "DOCUM_DATE_FMT" Then
            f = DOCUM_DATE_FMT
            If Date.TryParse(f.Text, d) Then
                v = d.ToString("yyyyMMdd")
            Else
                v = f.Text
            End If
            DataSetHeaderDataRow("DOCUM_DATE") = v
        ElseIf FieldName = "EVENT_DATE_FMT" Then
            f = EVENT_DATE_FMT
            If Date.TryParse(f.Text, d) Then
                v = d.ToString("yyyyMMdd")
            Else
                v = f.Text
            End If
            DataSetHeaderDataRow("EVENT_DATE") = v
        End If
        Return MyBase.ReadValueFromHeaderControlToDataSet(FieldName, ToReadOnly)
    End Function

#End Region

#Region "Файлы"

    Private Sub RefFileUploadFile_Uploaded(ByVal sender As Object, ByVal e As RefFileArgs) Handles RefFileUploadFile.Uploaded
        Dim row = vREF_FILE_DOCUMENT.InsertRow(e.ID)
        DataSetSpecificationDataRow(vREF_FILE_DOCUMENT.ID, e.ID)("CreationDateTime") = e.TransferTime
        DirectCast(row.FindControl("NAME"), TextBox).Text = System.IO.Path.GetFileName(e.FileName)
    End Sub

    Private Sub RefFileUploadFile_Uploading(ByVal sender As Object, ByVal e As RefFileArgs) Handles RefFileUploadFile.Uploading
        e.FileName = GetFileSystemAttachFileName(e.ID, e.TransferTime, 705, DBNull.Value, e.FileName)
    End Sub

    Public Sub RefFile_Requesting(ByVal sender As Object, ByVal e As RefFileArgs)
        e.TransferTime = DataSetSpecificationDataRow(vREF_FILE_DOCUMENT.ID, e.ID)("CreationDateTime")
        e.FileName = GetFileSystemAttachFileName(e.ID, e.TransferTime, 705, DBNull.Value)
    End Sub

    Private Sub RefFileUploadFileS_Uploaded(ByVal sender As Object, ByVal e As RefFileArgs) Handles RefFileUploadFileS.Uploaded
        Dim row = vREF_FILE_DOCUMENT_S.InsertRow(e.ID)
        DataSetSpecificationDataRow(vREF_FILE_DOCUMENT_S.ID, e.ID)("CreationDateTime") = e.TransferTime
        DirectCast(row.FindControl("NAME"), TextBox).Text = System.IO.Path.GetFileName(e.FileName)
    End Sub

    Private Sub RefFileUploadFileS_Uploading(ByVal sender As Object, ByVal e As RefFileArgs) Handles RefFileUploadFileS.Uploading
        e.FileName = GetFileSystemAttachFileName(e.ID, e.TransferTime, 705, 100, e.FileName)
    End Sub

    Public Sub RefFileS_Requesting(ByVal sender As Object, ByVal e As RefFileArgs)
        'e.TransferTime = DataSetSpecificationDataRow(vREF_FILE_DOCUMENT_S.ID, e.ID)("CreationDateTime")
        e.FileName = GetFileSystemAttachFileName(e.ID, e.TransferTime, 705, 100)
    End Sub

#End Region

End Class