﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="GR_DOC_DESCRIPT.ascx.vb"
    Inherits="WebApplication.GR_DOC_DESCRIPT" %>
<%--<table style="width: 97%; margin-bottom: 4px;">
    <tr>
        <td>
            <eq2:ArchiveCrumbs ID="Crumbs" runat="server" Level="GR_DOCUMENT" />
        </td>
    </tr>
</table>--%>
<table style="width: 700px;">
    <colgroup>
        <col width="150px" />
        <col />
    </colgroup>
    <tr>
        <td>
            Лист описания
        </td>
        <td>
            <eq:eqTextBox ID="NAME" runat="server" Height="30px" Width="185px"></eq:eqTextBox>
        </td>
        <td>
            Дата документа
        </td>
        <td>
            <eq:eqTextBox ID="DESCRIPT_DATE" runat="server"  Width="100px" />
             <aj:MaskedEditExtender ID = "DESCRIPT_DATE_MASK" TargetControlID = "DESCRIPT_DATE" runat="server" MaskType="Date" Mask="99/99/9999"   />
            <aj:CalendarExtender runat="server" TargetControlID="DESCRIPT_DATE" />
        </td>
    </tr>
</table>
<eq:eqMenu ID="MenuTabs" runat="server" Orientation="Horizontal" StaticEnableDefaultPopOutImage="False"
    ItemWrap="false" HorizontalMaxCount="5">
    <Items>
        <asp:MenuItem Text="Аннотация" Value="0" Selected="true" />
        <asp:MenuItem Text="Том.Дело." Value="1" />
        <asp:MenuItem Text="Драг металлы и камни" Value="2" />
        <asp:MenuItem Text="Художественное оформление" Value="3" />
        <asp:MenuItem Text="Размеры" Value="4" />
        <asp:MenuItem Text="Реставрация" Value="5" />
    </Items>
    <StaticSelectedStyle CssClass="MenuItemStyleSelected" />
</eq:eqMenu>
<div style="border: solid 1px black; padding: 5px;">
    <asp:MultiView ID="MultiViewTabs" runat="server" ActiveViewIndex="0">
        <%-- АННОТАЦИЯ GR_tblSHEET_DESCRIPT--%>
        <asp:View runat="server">
            <table>
                <tr>
                    <td>
                       <eq:eqTextBox ID="ANNOTATION" runat="server" Rows="4"></eq:eqTextBox>
                    </td>
                </tr>
            </table>
        </asp:View>
        <%--Из справочника Том. Дело. --%>
        <asp:View runat="server">
            <table>
                <tr>
                    <td>
                        Дело, том
                    </td>
                    <td>
                        <%--СПРАВОЧНИК--%>
                        <eq2:eqDocument ID="ISN_VOLUME" runat="server" DocTypeURL="GR_FILE.ascx"
                            ListDocMode="NewWindowAndSelect" LoadDocMode="ListDocAndSelect" ShowingFields="NUMBER,NAME,VOLUME_NUMBER"
                            ShowingFieldsFormat="{0},{1},{2}" KeyName="ISN_VOLUME"  />
                    </td>
                </tr>
            </table>
        </asp:View>
        <%--СПЕЦИФИКАЦИЯ Драг.металлы и камни GR_tblPRECIOUS_METAL_CL--%>
        <asp:View runat="server">
            <table>
                <tr>
                    <td>
                        <fieldset>
                            <legend>Драг.металлы</legend>
                            <%--СПЕЦИФИКАЦИЯ dbo.GR_tblPRECIOUS_METAL_CL--%>
                            <eq:eqSpecification ID="GR_tblPRECIOUS_METAL_CL" runat="server" ShowNumering="false">
                                <eq:eqSpecificationColumn HeaderText="Оригинальная запись" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="ORIGINAL_RECORD" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Драг.металл" Width="100px">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ISN_METAL" runat="server" TextMode="MultiLine">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Проба" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="PROBE" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="В лигатуре" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="MASS_ALLOY" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="В чистоте" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="MASS_PURE" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Масса" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="MASS" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                        <fieldset>
                            <legend>Драг.камни</legend>
                            <%--СПЕЦИФИКАЦИЯ dbo.GR_tblPRECIOUS_STONES--%>
                            <eq:eqSpecification ID="GR_tblPRECIOUS_STONES" runat="server" ShowNumering="false">
                                <eq:eqSpecificationColumn HeaderText="Оригинальная запись" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="ORIGINAL_RECORD" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Драг.камень" Width="100px">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ISN_STONE" runat="server" TextMode="MultiLine">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Количество" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="QUANTITY" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Вес в каратах" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="WHEIGHT" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
            </table>
        </asp:View>
        <asp:View runat="server">
            <table>
                <tr>
                     <td>
                       <eq:eqTextBox ID="ART_DESIGN" runat="server" Rows="4"></eq:eqTextBox>
                    </td>
                </tr>
            </table>
        </asp:View>
        <%--СПЕЦИФИКАЦИЯ Размеры dbo.GR_tblDOC_SIZE--%>
        <asp:View runat="server">
            <table>
                <tr>
                    <td>
                        <%--СПЕЦИФИКАЦИЯ dbo.GR_tblDOC_SIZE--%>
                        <eq:eqSpecification ID="GR_tblDOC_SIZE" runat="server">
                            <eq:eqSpecificationColumn>
                                <ItemTemplate>
                                    <table style="width: 600px;">
                                        <colgroup>
                                            <col align="right" style="width: 30%;" />
                                            <col align="left" style="width: 20%;" />
                                            <col align="right" style="width: 30%;" />
                                            <col align="left" />
                                        </colgroup>
                                        <tr>
                                            <td>
                                                Для листов
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="SHEET_NUMBERS" runat="server" />
                                            </td>
                                            <td>
                                                min/мах/midi
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="MIN_MAX_MIDI" runat="server" TextMode="MultiLine">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Латинское обозначение формата
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="LAT_NOTION1" runat="server" />
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="LAT_NOTION2" runat="server" />
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="LAT_NOTION3" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Размер 1
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="DIM1" runat="server" />
                                            </td>
                                            <td>
                                                Размер 2
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="DIM2" runat="server" />
                                            </td>
                                            <td>
                                                Размер 3
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="DIM3" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Диаметр
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="DIAMETER" runat="server" />
                                            </td>
                                            <td>
                                                Высота свитка
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="HEIGHT" runat="server" />
                                            </td>
                                            <td>
                                                Объем - листов
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="SHEET_VOL" runat="server" />
                                            </td>
                                            <td>
                                                Примечание
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="NOTE" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Для кино/фото документов
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Ширина пленки
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="FILM_WIDTH" runat="server" />
                                            </td>
                                            <td>
                                                Объем документа
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="DOC_VOL" runat="server" />
                                            </td>
                                            <td>
                                                Время звучания
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="SOUND_TIME" runat="server" />
                                            </td>
                                            <td>
                                                Время представления
                                            </td>
                                            <td>
                                                <eq:eqTextBox ID="PRESENTATION_TIME" runat="server" />
                                            </td>
                                        </tr>
                                </ItemTemplate>
                            </eq:eqSpecificationColumn>
                        </eq:eqSpecification>
                    </td>
                </tr>
            </table>
        </asp:View>
        <%--СПЕЦИФИКАЦИЯ dbo.GR_tblRESTORATION--%>
        <asp:View runat="server">
            <table>
                <tr>
                    <td>
                        <fieldset>
                            <legend>Реставрация</legend>
                            <eq:eqSpecification ID="GR_tblRESTORATION" runat="server" ShowNumering="false">
                                <eq:eqSpecificationColumn HeaderText="Сведения о реставрации" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="INFO" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Дата" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="DATE" runat="server" Width="100px" />
                                        <aj:CalendarExtender runat="server" TargetControlID="DATE" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Реставраторы" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="RESTORATORS" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Реставрационный центр" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="RESTORE_CENTER" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                                <eq:eqSpecificationColumn HeaderText="Страницы?" Width="100px">
                                    <ItemTemplate>
                                        <eq:eqTextBox ID="PAGES" runat="server" TextMode="MultiLine" />
                                    </ItemTemplate>
                                </eq:eqSpecificationColumn>
                            </eq:eqSpecification>
                        </fieldset>
                    </td>
                </tr>
            </table>
        </asp:View>
    </asp:MultiView>
</div>
