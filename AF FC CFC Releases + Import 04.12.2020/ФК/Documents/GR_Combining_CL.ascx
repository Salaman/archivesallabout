﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="GR_Combining_CL.ascx.vb"
    Inherits="WebApplication.GR_Combining_CL" %>
<table style="width: 1000px;">
    <tr>
        <td>
            Выберите справочник
        </td>
        <td>
            <asp:DropDownList ID="DocType" runat="server" Width="200px">
            </asp:DropDownList>
        </td>
        <td>
            <asp:Button ID="ButtonGetCL" runat="server" Text="Справочник выбран" />
        </td>
    </tr>
</table>
<asp:Panel ID="Panel_Language" runat="server" Visible="false">
    <table style="width: 1000px;">
        <tr>
            <td>
                Объединение значений справочника "Язык документа"
            </td>
        </tr>
        <tr>
            <td>
                Выберите общее значение
            </td>
            <td>
                <asp:DropDownList ID="Language_Name_cl" runat="server" Width="500px">
                </asp:DropDownList>
            </td>
        </tr>
       <%-- <tr>
            <td>
                или введите новое значение
            </td>
            <td>
                <eq:eqTextBox ID="Language_Name_new" runat="server" Width="500px"></eq:eqTextBox>
            </td>
        </tr>--%>
        <tr>
            <td>
                выберите значения для объединения
            </td>
            <td>
                <fieldset>
                    <eq:eqSpecification ID="GR_vClassificator_Language" runat="server" Width="500px"
                        ShowNumering="false">
                        <eq:eqSpecificationColumn HeaderText="Название" Width="500px">
                            <ItemTemplate>
                                <asp:DropDownList ID="ID_VALUE" runat="server" Width="500px">
                                </asp:DropDownList>
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                    </eq:eqSpecification>
                </fieldset>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Panel ID="Panel_Author" runat="server" Visible="false">
    <table style="width: 1000px;">
        <tr>
            <td>
                Объединение значений справочника "Автор документа"
            </td>
        </tr>
        <tr>
            <td>
                Выберите общее значение
            </td>
            <td>
                <asp:DropDownList ID="Author_Name" runat="server" Width="500px">
                </asp:DropDownList>
            </td>
        </tr>
        <%--<tr>
            <td>
                или введите новое значение
            </td>
            <td>
                <eq:eqTextBox ID="Author_Name_new" runat="server" Width="500px"></eq:eqTextBox>
            </td>
        </tr>--%>
        <tr>
            <td>
                выберите значения для объединения
            </td>
            <td>
                <fieldset>
                    <eq:eqSpecification ID="GR_vClassificator_Author" runat="server" Width="500px" ShowNumering="false">
                        <eq:eqSpecificationColumn HeaderText="Название" Width="500px">
                            <ItemTemplate>
                                <asp:DropDownList ID="ID_VALUE" runat="server" Width="500px">
                                </asp:DropDownList>
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                    </eq:eqSpecification>
                </fieldset>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Panel ID="Panel_Kind" runat="server" Visible="false">
    <table style="width: 1000px;">
        <tr>
            <td>
                Объединение значений справочника "Вид документа"
            </td>
        </tr>
        <tr>
            <td>
                Выберите общее значение
            </td>
            <td>
                <asp:DropDownList ID="Kind_Name" runat="server" Width="500px">
                </asp:DropDownList>
            </td>
        </tr>
       <%-- <tr>
            <td>
                или введите новое значение
            </td>
            <td>
                <eq:eqTextBox ID="Kind_Name_new" runat="server" Width="500px"></eq:eqTextBox>
            </td>
        </tr>--%>
        <tr>
            <td>
                выберите значения для объединения
            </td>
            <td>
                <fieldset>
                    <eq:eqSpecification ID="GR_vClassificator_Kind" runat="server" Width="500px" ShowNumering="false">
                        <eq:eqSpecificationColumn HeaderText="Название" Width="500px">
                            <ItemTemplate>
                                <asp:DropDownList ID="ID_VALUE" runat="server" Width="500px">
                                </asp:DropDownList>
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                    </eq:eqSpecification>
                </fieldset>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Panel ID="Panel_Carrier" runat="server" Visible="false">
    <table style="width: 1000px;">
        <tr>
            <td>
                Объединение значений справочника "Вид материального носителя"
            </td>
        </tr>
        <tr>
            <td>
                Выберите общее значение
            </td>
            <td>
                <asp:DropDownList ID="Carrier_Name" runat="server" Width="500px">
                </asp:DropDownList>
            </td>
        </tr>
        <%--<tr>
            <td>
                или введите новое значение
            </td>
            <td>
                <eq:eqTextBox ID="Carrier_Name_New" runat="server" Width="500px"></eq:eqTextBox>
            </td>
        </tr>--%>
        <tr>
            <td>
                выберите значения для объединения
            </td>
            <td>
                <fieldset>
                    <eq:eqSpecification ID="GR_vClassificator_Carrier" runat="server" Width="500px" ShowNumering="false">
                        <eq:eqSpecificationColumn HeaderText="Название" Width="500px">
                            <ItemTemplate>
                                <asp:DropDownList ID="ID_VALUE" runat="server" Width="500px">
                                </asp:DropDownList>
                            </ItemTemplate>
                        </eq:eqSpecificationColumn>
                    </eq:eqSpecification>
                </fieldset>
            </td>
        </tr>
    </table>
</asp:Panel>
<table style="width: 700px;">
    <tr>
        <td>
            <asp:Button ID="Button_Combining" runat="server" Text="Объединить" visible="false"/>
        </td>
    </tr>
</table>

<asp:Panel ID="Panel_CombiningOK" runat="server" Visible="false">
   <table style="width: 700px;">
    <tr>
    <td>
            <asp:Label ID="Label_Message" runat="server" Text="" visible="True"/>
        </td>
        <td>
            <asp:Button ID="Button_NewCombining" runat="server" Text="Сбросить параметры" visible="false"/>
        </td>
    </tr>
</table>
<table style="width: 700px;">
    <tr>
        <td>
            <asp:Button ID="Button_CombiningOK" runat="server" Text="Подтвердить объединение" visible="false"/>
        </td>
    </tr>
</table>
</asp:Panel>