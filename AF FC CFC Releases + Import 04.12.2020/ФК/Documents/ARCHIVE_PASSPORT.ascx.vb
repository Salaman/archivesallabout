﻿Imports System.Data
Imports System.Data.SqlClient

Partial Public Class ARCHIVE_PASSPORT
    Inherits BaseDoc

#Region "События"

    Private Sub Page_PreRender2(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        MultiViewTabs.ActiveViewIndex = CInt(MenuTabs.SelectedValue)

        StatsDisable(vARCHIVE_STATS_P1, vARCHIVE_STATS_A1, vARCHIVE_STATS_E1, vARCHIVE_STATS_M1, vARCHIVE_STATS1)
        StatsDisable(vARCHIVE_STATS_P3, vARCHIVE_STATS_A3, vARCHIVE_STATS_E3, vARCHIVE_STATS_M3, vARCHIVE_STATS3)
    End Sub

#End Region

#Region "Методы"

    Public Sub Calc()
        If SaveDoc() Then
            Dim r As New Recalc(AppSettings)
            Try
                r.Calc(Recalc.Type.Passport, DataSetHeaderDataRow("ISN_PASSPORT"))
            Catch ex As Exception
                AddMessage("Не удалось выполнить расчет: >> " & ex.Message)
            End Try

            LoadDoc(DocID)
        End If
    End Sub

#End Region

#Region "Документ"

    Public Overrides Function NewDoc() As Boolean
        NewDoc = MyBase.NewDoc()
        If NewDoc Then
            If Request.QueryString("ARCHIVE") IsNot Nothing Then
                Crumbs.ISN_ARCHIVE.DocID = New Guid(Request.QueryString("ARCHIVE"))
            End If
        End If
    End Function

    Public Overrides Function SaveDoc() As Boolean
        If Val(TOTAL_SPACE.Text.Trim) = 0 Then
            AddMessage("Общая площадь зданий (помещений) (кв.м.) -> Значение должно быть больше нуля.")
            Return False
        End If

        Return MyBase.SaveDoc()
    End Function

#End Region

#Region "Контролы"

    Public Overrides Sub PutValueToControl(ByVal Value As Object, ByVal TargetControl As System.Web.UI.Control, Optional ByVal FieldFormat As String = "")
        If TypeOf TargetControl Is CheckBox Then
            DirectCast(TargetControl, CheckBox).Checked = (Value Is Nothing OrElse Value.ToString.Trim.ToUpper = "Y")
        Else
            MyBase.PutValueToControl(Value, TargetControl, FieldFormat)
        End If
    End Sub

    Public Overrides Function GetValueFromControl(ByVal SourceControl As System.Web.UI.Control) As Object
        If TypeOf SourceControl Is CheckBox Then
            Return IIf(DirectCast(SourceControl, CheckBox).Checked, "Y", "N")
        Else
            Return MyBase.GetValueFromControl(SourceControl)
        End If
    End Function

#End Region

End Class