﻿Public Partial Class STORAGE_MEDIUM_CL
    Inherits BaseDoc

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Request.QueryString("ParentID") IsNot Nothing Then
                ISN_HIGH_STORAGE_MEDIUM.DocID = New Guid(Request.QueryString("ParentID"))
            End If
        End If
    End Sub

    Private Sub clearISN_HIGH_STORAGE_MEDIUM_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles clearISN_HIGH_STORAGE_MEDIUM.Click
        ISN_HIGH_STORAGE_MEDIUM.DocID = Guid.Empty
    End Sub

End Class