﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ListDocFunction.ascx.vb"
    Inherits="WebApplication.ListDocFunction" %>
<table cellpadding="0" cellspacing="1" style="padding: 0px;" id="tableFunction" runat="server">
    <tr>
        <td style="vertical-align: middle;">
            <asp:LinkButton ID="FunctionLinkButton" runat="server" />
            <asp:Label ID="FunctionNameLabel" runat="server" />
        </td>
        <td style="vertical-align: middle;">
            (
        </td>
        <td style="vertical-align: middle;" id="tdParameter" runat="server">
            <asp:Label ID="FunctionLabel" runat="server" />
            <eq:eqTextBox Visible="false" ID="FunctionTextBox" runat="server" Width="100px" />
            <asp:CheckBox ID="FunctionCheckBox" runat="server" AutoPostBack="true" />
            <asp:DropDownList ID="FunctionDropDownList" runat="server" AutoPostBack="true" />
            <aj:CalendarExtender ID="FunctionCalendarextender" runat="server" TargetControlID="FunctionTextBox" />
            <aj:FilteredTextBoxExtender ID="FunctionFilteredTextBoxExtender" runat="server" TargetControlID="FunctionTextBox" />
        </td>
        <td style="vertical-align: middle;">
            )
        </td>
    </tr>
</table>
