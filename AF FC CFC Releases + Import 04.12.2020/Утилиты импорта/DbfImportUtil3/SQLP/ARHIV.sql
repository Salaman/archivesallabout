﻿-- Архив

/* поля не используются:
[ISN_SUBJECT]
[ARCHIVE_GUID]
[CODE]
[ARCHIVE_LEVEL]
[ARCHIVE_TYPE] */

BEGIN [ARHIV].[tblAuthorizedDep]
	#primary
	"link																		ArchiveID
	1																			[ISN_AuthorizedDep]
	'Без'+char(32)+'имени'														[ShortName]		
	
	$insql			if (@DestID = 2 and not exists(select ISN_AuthorizedDep from tblAuthorizedDep where ISN_AuthorizedDep = @ArchiveID))
	$insql			   or (@DestID = 3 and not exists(select ISN_AuthorizedDep from tblAuthorizedDep))
	$insql			   select @Result = 0 else select @Result = 1

	$outsql			if @DestID = 2
	$outsql			update tblAuthorizedDep set ISN_AuthorizedDep = @ArchiveID where ISN_AuthorizedDep = 1
END

BEGIN [ARHIV].[tblARCHIVE]
	#primary
	"link																		ArchiveID
	$pk																			[ArchiveID]			-- ISN_ARCHIVE
	@ArchiveID																	[ISN_ARCHIVE]		-- PK
	@ANAME																		[NAME_SHORT]		-- СОКРАЩЕННОЕ НАЗВАНИЕ
	@L1																			[NAME]				-- ПОЛНОЕ НАЗВАНИЕ
	@L2																			[ADDRESS]			-- АДРЕС
	@L3																			[AUTHORITY]			-- НАЗВАНИЕ И АДРЕС ОРГАНА УПРАВЛЕНИЯ
	@PAROLE1																	[]
	@PAROLE2																	[]
	@PAROLE3																	[]
	@PAROLE4																	[]
	@PAROLE5																	[]
	@PAROLE6																	[]
	@PAROLE7																	[]
	@PAROLE8																	[]
	@NKOD1																		[]
	@NKOD2																		[]
	@NKOD3																		[]
	@NKOD4																		[]
	@NKOD5																		[]
	@NKOD6																		[]
	@NKOD7																		[]
	
	$outsql			if @DestID = 2
	$outsql			update tblARCHIVE set ISN_AuthorizedDep = @ArchiveID where ISN_ARCHIVE = @ArchiveID
	
	$outsql			if @DestID = 3
	$outsql			update tblARCHIVE set ISN_AuthorizedDep = 1 where ISN_ARCHIVE = @ArchiveID
END
