﻿-- Издания

/* поля не используются:
[PUBLICATION_NUM] */

BEGIN [BIBLIO].[tblPUBLICATION_CL]
	#primary
	"link																		KOD
	$pk																			KOD							-- ISN_PUBLICATION
	@KOD																		[ISN_PUBLICATION]			-- PK
	@B1																			[AUTHORS]					-- АВТОР(Ы)
	@B2																			[PUBLICATION_NAME]			-- НАЗВАНИЕ
	@B3																			[PUBLICATION_PLACE]			-- МЕСТО ИЗДАНИЯ
	@B4																			[PUBLICATION_YEAR]			-- ГОД ИЗДАНИЯ
	@B5																			[SHEET_COUNT]				-- ОБЪЕМ В СТР
	@B6																			[PUBLISHER]					-- ИЗДАТЕЛЬСТВО
	@B7																			[ISN_PUBLICATION_TYPE]		-- ТИП ИЗДАНИЯ ()
END
