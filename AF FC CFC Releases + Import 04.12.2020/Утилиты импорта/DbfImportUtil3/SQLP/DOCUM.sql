﻿-- Документ

/* поля не используются:
[ISN_SECURLEVEL]
[SECURITY_CHAR]
[PAGE_COUNT]
[ADDITIONAL_CLS]
[KEYWORDS] */

BEGIN [DOCUM].[tblREPRODUCTION_METHOD_CL]
	#primary
	"null																		T14	
	$set																		NewSmallID
	$pk																			NewSmallID							-- ISN_REPRODUCTION_METHOD
	@NewSmallID																	[ISN_REPRODUCTION_METHOD]			-- PK
	null																		[CODE]								--
	@T14																		[NAME]								--
	null																		[NOTE]								--
	@KOD																		[]									--
	
	$insql				if not (@T14 in ('рук.','маш.','типогр.','компьют.','ротатор','ротапринт','мимеограф','стеклограф','комп. распечатка','иное'))
	$insql				and not exists(select ISN_REPRODUCTION_METHOD from tblREPRODUCTION_METHOD_CL where [NAME] = @T14 and %ISN_REPRODUCTION_METHOD%)
	$insql				select @Result = 0 else select @Result = 1
END

BEGIN [DOCUM].[tblDOCUMENT]
	#primary
	"link																		DELO,KOD
	$pk																			KOD							-- ISN_DOCUM
	$fk																			DELO						-- ISN_UNIT
	@KOD																		[ISN_DOCUM]					-- PK
	@DELO																		[ISN_UNIT]					-- FK (DELO.KOD) 
	substring(@T1,0,patindex('%-%',@T1))										?[PAGE_FROM]				-- !!! пока не надо !!! НОМЕРА ЛИСТОВ
	substring(@T1,patindex('%-%',@T1)+1,len(@T1))								?[PAGE_TO]					-- !!! пока не надо !!! НОМЕРА ЛИСТОВ
	@T1																			[NOTE]						-- ПРИМЕЧАНИЕ
	@T2																			[NAME]						-- ЗАГОЛОВОК
	@T3																			[ANNOTATE]					-- АННОТАЦИЯ
	format(@T4=yyyyMMdd)														[EVENT_DATE]				-- ДАТА СОБЫТИЯ
	@T5																			[INEXACT_EVENT_DATE]		-- НЕТОЧНАЯ ДАТА СОБЫТИЯ
	@T6																			[EVENT_PLACE]				-- МЕСТО СОБЫТИЯ
	@T7																			{}							-- ВИД ДОКУМЕНТА ()
	format(@T8=yyyyMMdd)														[DOCUM_DATE]				-- ДАТА ДОКУМЕНТА
	@T9																			[INEXACT_DOCUM_DATE]		-- НЕТОЧНАЯ ДАТА ДОКУМЕНТА
	@T10																		[DOC_NUM]					-- НОМЕР ДОКУМЕНТА
	@T11																		{}							-- ЯЗЫК ДОКУМЕНТА
	@T12																		[ENCLOSURES]				-- ПРИЛОЖЕНИЯ
	@T13																		[]							-- ВНЕШНИЕ ОСОБЕННОСТИ
	@T14																		[]							-- СПОСОБ ВОСПРОИЗВЕДЕНИЯ ()
	@T15																		[]							-- ПОДЛИННОСТЬ ()
	@T16																		[]							-- ПРИМЕЧАНИЕ
	@T17																		[]							-- ТЕКСТ ДОКУМЕНТА
	null																		[ISN_DOC_KIND]				--
	null																		[ISN_REPRODUCTION_METHOD]	--

	$insql			if exists(select ISN_UNIT from tblUNIT where ISN_UNIT = @DELO)
	$insql			select @Result = 0 else select @Result = 1

	$outsql			if @T14 in ('рук.','маш.','типогр.','компьют.','ротатор','ротапринт','мимеограф','стеклограф','комп. распечатка','иное')
	$outsql				update tblDOCUMENT set ISN_REPRODUCTION_METHOD = case
	$outsql				when @T14 = 'рук.' then 2
	$outsql				when @T14 = 'маш.' then 3
	$outsql				when @T14 = 'типогр.' then 4
	$outsql				when @T14 = 'компьют.' then 16364
	$outsql				when @T14 = 'ротатор' then 16360
	$outsql				when @T14 = 'ротапринт' then 16361
	$outsql				when @T14 = 'мимеограф' then 16362
	$outsql				when @T14 = 'стеклограф' then 16363
	$outsql				when @T14 = 'комп. распечатка' then 16364
	$outsql				when @T14 = 'иное' then 6
	$outsql				end where ISN_DOCUM = @KOD
	$outsql			else
	$outsql				update tblDOCUMENT set ISN_REPRODUCTION_METHOD =
	$outsql				  (select R.ISN_REPRODUCTION_METHOD from tblREPRODUCTION_METHOD_CL R where R.NAME = @T14 and %R.ISN_REPRODUCTION_METHOD%)
	$outsql				where ISN_DOCUM = @KOD
	
	$outsql			if exists(select tblDOC_KIND_CL.ISN_DOC_KIND from tblDOC_KIND_CL where tblDOC_KIND_CL.NAME = @T7)
	$outsql			update tblDOCUMENT set ISN_DOC_KIND = (select top 1 tblDOC_KIND_CL.ISN_DOC_KIND from tblDOC_KIND_CL where tblDOC_KIND_CL.NAME = @T7)
	$outsql			where ISN_DOCUM = @KOD
END

BEGIN [DOCUM].[tblLANGUAGE_CL]
	#primary
	"null																		T11	
	$set																		NewSmallID2
	$pk																			NewSmallID2							-- ISN_LANGUAGE
	@NewSmallID2																[ISN_LANGUAGE]						-- PK
	@T11																		[NAME]								-- ЯЗЫК
	
	$insql				if exists(select L.ISN_LANGUAGE from tblLANGUAGE_CL L where L.NAME = @T11 and %L.ISN_LANGUAGE%)
	$insql				select @Result = 1 else select @Result = 0
END

BEGIN [DOCUM].[tblREF_LANGUAGE]
	#relative
	"link																		NewID
	"null																		T11	
	$pk																			NewID								-- ISN_REF_LANGUAGE
	$fk																			KOD									-- ISN_OBJ
	@NewID																		[ISN_REF_LANGUAGE]					-- PK
	@KOD																		[ISN_OBJ]							--
	1																			[ISN_LANGUAGE]						--
	705																			[KIND]								-- 'document'
	@T11																		[]									--
	
	$outsql				update tblREF_LANGUAGE set ISN_LANGUAGE =
	$outsql				  (select top 1 L.ISN_LANGUAGE from tblLANGUAGE_CL L where L.NAME = @T11 and %L.ISN_LANGUAGE%)
	$outsql				where ISN_REF_LANGUAGE = @NewID
END

BEGIN
	if not exists(select F.ISN_FEATURE from tblFEATURE F where F.NAME = 'Особенности' and %F.ISN_FEATURE%)
	insert into tblFEATURE
	([ID],[OwnerID],[CreationDateTime],[StatusID],[Deleted],[ISN_FEATURE],[ISN_HIGH_FEATURE],[CODE],[NAME],[NOTE],[FOREST_ELEM],[PROTECTED],[WEIGHT])
	values (@ConstGUID,@OwnerID,GETDATE(),@StatusID,0,@ConstID,null,null,'Особенности',null,'F','Y',1)
	
	if not exists(select F.ISN_FEATURE from tblFEATURE F where F.NAME = 'Подлинность документов' and %F.ISN_FEATURE%)
	begin
		insert into tblFEATURE
		([ID],[OwnerID],[CreationDateTime],[StatusID],[Deleted],[ISN_FEATURE],[ISN_HIGH_FEATURE],[CODE],[NAME],[NOTE],[FOREST_ELEM],[PROTECTED],[WEIGHT])
		values (@ConstGUID3,@OwnerID,GETDATE(),@StatusID,0,@ConstID3,null,null,'Подлинность документов',null,'T','N',1)

		update tblFEATURE set ISN_HIGH_FEATURE =
		(select top 1 F.ISN_FEATURE from tblFEATURE F where F.NAME = 'Особенности' and %F.ISN_FEATURE%)
		where ISN_FEATURE = @ConstID3
	end
END

BEGIN [DOCUM].[tblFEATURE]
	#primary
	"null																		T15	
	$set																		NewID2
	$pk																			NewID2							-- ISN_FEATURE
	@NewID2																		[ISN_FEATURE]					-- PK
	null																		[ISN_HIGH_FEATURE]				--
	null																		[CODE]							--
	@T15																		[NAME]							--
	null																		[NOTE]							--
	'B'																			[FOREST_ELEM]					--
	'N'																			[PROTECTED]						--
	
	$insql			if exists(select F.ISN_FEATURE from tblFEATURE F where F.NAME = @T15 and %F.ISN_FEATURE%)
	$insql			select @Result = 1 else select @Result = 0

	$outsql			update tblFEATURE set ISN_HIGH_FEATURE =
	$outsql			  (select top 1 F.ISN_FEATURE from tblFEATURE F where F.NAME = 'Подлинность документов' and %F.ISN_FEATURE%)
	$outsql			where ISN_FEATURE = @NewID2
END

BEGIN [DOCUM].[tblREF_FEATURE]
	#relative
	"link																		NewID3,KOD
	"null																		T15	
	$pk																			NewID3								-- ISN_REF_LANGUAGE
	$fk																			KOD									-- ISN_OBJ
	@NewID3																		[ISN_REF_FEATURE]					-- PK
	null																		[ISN_FEATURE]						--
	@KOD																		[ISN_OBJ]							--
	705																			[KIND]								-- 'document'
	null																		[ORDER_NUM]							--
	@T15																		[]									--

	$outsql			update tblREF_FEATURE set ISN_FEATURE =
	$outsql			  (select top 1 F.ISN_FEATURE from tblFEATURE F where F.NAME = @T15 and %F.ISN_FEATURE%)
	$outsql			where ISN_REF_FEATURE = @NewID3
END
