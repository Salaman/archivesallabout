﻿-- Биографическая справка

/* поля не используются: - */

BEGIN [FOND4].[tblCITIZEN_CL]
	#primary
	"link																		NewID
	$pk																			NewID					-- ISN_CITIZEN
	$fk																			FOND					-- 
	@NewID																		[ISN_CITIZEN]			-- PK
	@FOND																		[]						-- FK (FOND.KOD)
	@D2																			[]						-- ОСНОВНОЙ ФОНДООБРАЗОВАТЕЛЬ
	rtrim(ltrim(rtrim(@D1)+char(32)+rtrim(@D3)+char(32)+rtrim(@D4)))			[NAME]					-- ФАМИЛИЯ, ИМЯ, ОТЧЕСТВО
	@D5																			[TITLE]					-- ТИТУЛ
	@D6																			[RELATIONSHIP]			-- РОДСТВО
	@D7																			[LAST_FAMILY_NAME]		-- ПРЕДЫДУЩАЯ ФАМИЛИЯ
	@D8																			[NICKNAME]				-- ПСЕВДОНИМ
	format(@D9=yyyyMMdd)														[BIRTH_DATE]			-- НАЧАЛЬНАЯ ДАТА
	format(@D10=yyyyMMdd)														[DEATH_DATE]			-- КОНЕЧНАЯ ДАТА
	@D11																		[PROFESSION]			-- ПРОФЕССИЯ
	@D12																		[POST]					-- ДОЛЖНОСТЬ
	@D13																		[DEGREE]				-- УЧЕНАЯ СТЕПЕНЬ
	@D14																		[MILITARY_RANK]			-- ВОИНСКОЕ ЗВАНИЕ
	@D15																		[HONORARY_RANK]			-- ПОЧЕТНОЕ ЗВАНИЕ
END

BEGIN [FOND4].[tblFUND_CREATOR]
	#relative
	"link																		NewID2,FOND,NewID
	$pk																			NewID2					-- ISN_FUND_CREATOR
	$fk																			FOND					-- ISN_FUND
	@NewID2																		[ISN_FUND_CREATOR]		-- PK
	@NewID																		[ISN_CITIZEN]
	@FOND																		[ISN_FUND]				-- FK
	null																		[ISN_ORGANIZ]
	null																		[KIND]
	@D2																			[]
	
	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @FOND)
	$insql			and (@D2 = 'True')
	$insql			select @Result = 0 else select @Result = 1
END