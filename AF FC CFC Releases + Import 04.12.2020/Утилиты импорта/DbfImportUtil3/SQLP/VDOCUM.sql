﻿-- Вопросы к документам

/* поля не используются: - */

BEGIN [VDOCUM].[tblREF_QUESTION]
	#relative
	"link											NewID,VOPROS,FOND,OPIS,DELO,DOCUM
	$fk												VOPROS
	@NewID											[ISN_REF_QUESTION]
	@VOPROS											[ISN_QUESTION]				-- FK
	case(@TIP=1:701,2:702,3:703,4:705)				[KIND]
	case(@TIP=1:@FOND,2:@OPIS,3:@DELO,4:@DOCUM)		[ISN_OBJ]
	case(@TIP=4:@LIST)								[PAGES]						-- DOCUMENT.PAGE_FROM - DOCUMENT.PAGE_TO (похоже, дублирует DOKUM.T1)

	$insql			if exists(select ISN_QUESTION from tblQUESTION where ISN_QUESTION = @VOPROS)
	$insql			select @Result = 0 else select @Result = 1
END
