﻿-- Дело

/* поля не используются:
[ISN_DOC_KIND]
[ISN_LOCATION]
[SECURITY_REASON]
[ISN_STORAGE_MEDIUM]
[IS_LOST]
[UNIT_CNT]
[BACKUP_COPY_CNT]
[CARDBOARDED]
[ADDITIONAL_CLS]
[ISN_SECURITY_REASON] */

BEGIN [DELO].[tblUNIT]
	#primary
	"link																		KOD,OPIS
	$pk																			KOD							-- ISN_UNIT
	$fk																			OPIS						-- ISN_INVENTORY	
	@KOD																		[ISN_UNIT]					-- PK
	null																		[ISN_HIGH_UNIT]				--
	@OPIS																		[ISN_INVENTORY]				-- FK (OPIS.KOD)
	ltrim(rtrim(substring(@L1,1,8)))											[UNIT_NUM_1]				-- НОМЕР ЕД.ХР
	ltrim(rtrim(substring(@L1,9,2)))											[UNIT_NUM_2]				-- НОМЕР ЕД.ХР
	@L2																			[VOL_NUM]					-- ТОМ ЕД.ХР
	@L3																			[]							-- НОМЕР ЕД.УЧ
	@L4																			[NAME]						-- ЗАГОЛОВОК
	@L5																			[ANNOTATE]					-- АННОТАЦИЯ
	@L6																			[DELO_INDEX]				-- ДЕЛОПРОИЗВОДСТВЕННЫЙ ИНДЕКС
	@L7																			[PRODUCTION_NUM]			-- ПРОИЗВОДСТВЕННЫЙ НОМЕР
	@L8																			[]							-- НАЧАЛЬНАЯ ДАТА
	@L9																			[]							-- КОНЕЧНАЯ ДАТА
	@L11																		[PAGE_COUNT]				-- КОЛ-ВО ЛИСТОВ
	case(@L12=1:'a',2:'c',3:'b')												[UNIT_CATEGORY]				-- КАТЕГОРИЯ ЕД.ХР. ()
	case(@L13=1:1,2:3,3:4,4:2,5:7,6:5,7:6,8:8,9:10,10:9)					    [ISN_DOC_TYPE]				-- ТИП ДОКУМЕНТАЦИИ ()
	isnull(@L14+char(0x0D)+char(0x0A),'')+@L10									[NOTE]						-- ПРИМЕЧАНИЕ
	@L15																		[HAS_TREASURES]				-- ДРАГОЦЕННОСТИ
	@L16																		[IS_MUSEUM_ITEM]			-- МУЗ.ПРЕДМЕТЫ
	@L17																		[]							-- ИЗО.МАТ. ()
	@L18																		[CATALOGUED]				-- ЗАКАТАЛОГИЗИРОВАНО
	@L19																		[IS_IN_SEARCH]				-- РОЗЫСК
	@L20																		[HAS_SF]					-- ИМЕЕТ СФ
	@L21																		[HAS_FP]					-- ИМЕЕТ ФП
	@L22																		[HAS_DEFECTS]				-- НАЛИЧИЕ  ДЕФЕКТОВ
	@L23																		[]							-- ТОМ ОПИСИ
	@L24																		[]							-- ЯЗЫК
	@L25																		[]							-- КОД РУБРИКИ
	1																			[ISN_SECURLEVEL]			--
	'o'																			[SECURITY_CHAR]				--
	703																			[UNIT_KIND]					-- 'unit'
	null																		[ISN_INVENTORY_CLS]			--
	
	$insql				if exists(select ISN_INVENTORY from tblINVENTORY where ISN_INVENTORY = @OPIS)
	$insql				select @Result = 0 else select @Result = 1
	
	$outsql				if exists(select top 1 I.ISN_INVENTORY_CLS from tblINVENTORY_STRUCTURE I where I.NOTE = @L25 and I.ISN_INVENTORY = @OPIS)
	$outsql				update tblUNIT set ISN_INVENTORY_CLS =
	$outsql				  (select top 1 I.ISN_INVENTORY_CLS from tblINVENTORY_STRUCTURE I where I.NOTE = @L25 and I.ISN_INVENTORY = @OPIS)
	$outsql				where ISN_UNIT = @KOD
	
	$outsql				update tblUNIT set MEDIUM_TYPE = 
	$outsql				  (case when @L13 = 10 then 'E' else 'T' end)
	$outsql				where ISN_UNIT = @KOD
	
	$outsql				if @Server = '09'
	$outsql					update tblUNIT set	START_YEAR = left(cast(@L8 as varchar(20)),4), 
	$outsql										end_YEAR = left(cast(@L9 as varchar(20)),4), 
	$outsql										ALL_DATE = cast(@L8 as varchar(20)) + '-' + cast(@L9 as varchar(20))
	$outsql					where ISN_UNIT = @KOD
	$outsql				else
	$outsql					update tblUNIT set	START_YEAR = left(convert(varchar(20),@L8,102),4),
	$outsql										end_YEAR = left(convert(varchar(20),@L9,102),4),
	$outsql										ALL_DATE = convert(varchar(20),@L8,104) + '-' + convert(varchar(20),@L9,104)
	$outsql					where ISN_UNIT = @KOD
	
	$outsql				if (ltrim(isnull(@L10,'')) <> '')
	$outsql				update tblUNIT set START_YEAR_INEXACT = 'Y', end_YEAR_INEXACT = 'Y'
	$outsql				where ISN_UNIT = @KOD
END

BEGIN [DELO].[tblUNIT]
	#primary
	"link																		KOD,OPIS,NewID4	
	"null																		L3
	$pk																			NewID4						-- ISN_UNIT
	$fk																			OPIS						-- ISN_INVENTORY	
	@NewID4																		[ISN_UNIT]					-- PK
	@KOD																		[ISN_HIGH_UNIT]				--
	@OPIS																		[ISN_INVENTORY]				-- FK (OPIS.KOD)
	ltrim(rtrim(replace(replace(@L3,'А',''),'Б','')))															[UNIT_NUM_1]				-- НОМЕР ЕД.УЧ
	@L4																			[NAME]						-- ЗАГОЛОВОК
	@L8																			[]							-- НАЧАЛЬНАЯ ДАТА
	@L9																			[]							-- КОНЕЧНАЯ ДАТА
	@L10																		[]							-- НЕТОЧНАЯ ДАТА
	case(@L13=5:7,6:5,7:6,8:8,9:10)				    	                        [ISN_DOC_TYPE]				-- ТИП ДОКУМЕНТАЦИИ ()
	@L25																		[]							-- КОД РУБРИКИ
	1																			[ISN_SECURLEVEL]			--
	'o'																			[SECURITY_CHAR]				--
	704																			[UNIT_KIND]					-- 'reg_unit'
	null																		[ISN_INVENTORY_CLS]			--
	@L3																			[]							--
		
	$insql				if exists(select ISN_INVENTORY from tblINVENTORY where ISN_INVENTORY = @OPIS)
	$insql				select @Result = 0 else select @Result = 1
	
	$outsql				if exists(select top 1 I.ISN_INVENTORY_CLS from tblINVENTORY_STRUCTURE I where I.NOTE = @L25 and I.ISN_INVENTORY = @OPIS)
	$outsql				update tblUNIT set ISN_INVENTORY_CLS =
	$outsql				  (select top 1 I.ISN_INVENTORY_CLS from tblINVENTORY_STRUCTURE I where I.NOTE = @L25 and I.ISN_INVENTORY = @OPIS)
	$outsql				where ISN_UNIT = @NewID4

	$outsql				if @Server = '09'
	$outsql					update tblUNIT set	START_YEAR = left(cast(@L8 as varchar(20)),4), 
	$outsql										end_YEAR = left(cast(@L9 as varchar(20)),4), 
	$outsql										ALL_DATE = cast(@L8 as varchar(20)) + '-' + cast(@L9 as varchar(20))
	$outsql					where ISN_UNIT = @NewID4
	$outsql				else
	$outsql					update tblUNIT set	START_YEAR = left(convert(varchar(20),@L8,102),4),
	$outsql										end_YEAR = left(convert(varchar(20),@L9,102),4),
	$outsql										ALL_DATE = convert(varchar(20),@L8,104) + '-' + convert(varchar(20),@L9,104)
	$outsql					where ISN_UNIT = @NewID4
	
	$outsql				if (ltrim(isnull(@L10,'')) <> '')
	$outsql				update tblUNIT set START_YEAR_INEXACT = 'Y', end_YEAR_INEXACT = 'Y'
	$outsql				where ISN_UNIT = @NewID4
END

BEGIN [DELO].[tblINVENTORY_DOC_TYPE]
	#relative
	"link																		OPIS
	$fk																			OPIS								-- ISN_INVENTORY
	@OPIS																		[ISN_INVENTORY]						-- FK
	case(@L13=1:1,2:3,3:4,4:2,5:7,6:5,7:6,8:8,9:10,10:9)						[ISN_DOC_TYPE]						--

	$insql				if exists(select ISN_INVENTORY from tblINVENTORY where ISN_INVENTORY = @OPIS)
	$insql					and not exists(select ISN_DOC_TYPE from tblINVENTORY_DOC_TYPE where ISN_DOC_TYPE = 
	$insql						(case @L13 when 1 then 1 when 2 then 3 when 3 then 4 when 4 then 2 when 5 then 7 when 6 then 5 when 7 then 6 when 8 then 8 when 9 then 10 when 10 then 9 end)
	$insql							and ISN_INVENTORY = @OPIS)
	$insql				select @Result = 0 else select @Result = 1
END

BEGIN [DELO].[tblLANGUAGE_CL]
	#primary
	"null																		L24
	$set																		NewSmallID
	$pk																			NewSmallID							-- ISN_LANGUAGE
	@NewSmallID																	[ISN_LANGUAGE]						-- PK
	@L24																		[NAME]								-- ЯЗЫК
	
	$insql				if exists(select L.ISN_LANGUAGE from tblLANGUAGE_CL L where L.NAME = @L24 and %L.ISN_LANGUAGE%)
	$insql				select @Result = 1 else select @Result = 0
END

BEGIN [DELO].[tblREF_LANGUAGE]
	#relative
	"link																		KOD,NewID
	"null																		L24	
	$pk																			NewID								-- ISN_REF_LANGUAGE
	$fk																			KOD									-- ISN_OBJ
	@NewID																		[ISN_REF_LANGUAGE]					-- PK
	@KOD																		[ISN_OBJ]							--
	1																			[ISN_LANGUAGE]						--
	703																			[KIND]								-- 'unit'
	@L24																		[]									--
	
	$insql				if exists(select ISN_REF_LANGUAGE from tblREF_LANGUAGE where ISN_LANGUAGE = 1 and ISN_OBJ = @KOD)
	$insql				select @Result = 1 else select @Result = 0
	
	$outsql				update tblREF_LANGUAGE set ISN_LANGUAGE =
	$outsql				  (select top 1 L.ISN_LANGUAGE from tblLANGUAGE_CL L where L.NAME = @L24 and %L.ISN_LANGUAGE%)
	$outsql				where ISN_REF_LANGUAGE = @NewID
END

BEGIN
	if not exists(select F.ISN_FEATURE from tblFEATURE F where F.NAME = 'Особенности' and %F.ISN_FEATURE%)
	insert into tblFEATURE
	([ID],[OwnerID],[CreationDateTime],[StatusID],[Deleted],[ISN_FEATURE],[ISN_HIGH_FEATURE],[CODE],[NAME],[NOTE],[FOREST_ELEM],[PROTECTED],[WEIGHT])
	values (@ConstGUID,@OwnerID,GETDATE(),@StatusID,0,@ConstID,null,null,'Особенности',null,'F','Y',1)
	
	if not exists(select F.ISN_FEATURE from tblFEATURE F where F.NAME = 'изо. материалы' and %F.ISN_FEATURE%)
	begin
		insert into tblFEATURE
		([ID],[OwnerID],[CreationDateTime],[StatusID],[Deleted],[ISN_FEATURE],[ISN_HIGH_FEATURE],[CODE],[NAME],[NOTE],[FOREST_ELEM],[PROTECTED],[WEIGHT])
		values (@ConstGUID2,@OwnerID,GETDATE(),@StatusID,0,@ConstID2,null,null,'изо. материалы',null,'T','N',1)
		
		update tblFEATURE set ISN_HIGH_FEATURE =
		(select top 1 F.ISN_FEATURE from tblFEATURE F where F.NAME = 'Особенности' and %F.ISN_FEATURE%)
		where ISN_FEATURE = @ConstID2
	end
END

BEGIN [DELO].[tblFEATURE]
	#primary
	"null																		L17	
	$set																		NewID2
	$pk																			NewID2							-- ISN_FEATURE
	@NewID2																		[ISN_FEATURE]					-- PK
	null																		[ISN_HIGH_FEATURE]				--
	null																		[CODE]							--
	@L17																		[NAME]							--
	null																		[NOTE]							--
	'B'																			[FOREST_ELEM]					--
	'N'																			[PROTECTED]						--
	
	$insql			if exists(select F.ISN_FEATURE from tblFEATURE F where F.NAME = @L17 and %F.ISN_FEATURE%)
	$insql			select @Result = 1 else select @Result = 0

	$outsql			update tblFEATURE set ISN_HIGH_FEATURE =
	$outsql			  (select top 1 F.ISN_FEATURE from tblFEATURE F where F.NAME = 'изо. материалы' and %F.ISN_FEATURE%)
	$outsql			where ISN_FEATURE = @NewID2
END

BEGIN [DELO].[tblREF_FEATURE]
	#relative
	"link																		KOD,NewID3
	"null																		L17	
	$fk																			KOD									-- ISN_OBJ
	@NewID3																		[ISN_REF_FEATURE]					-- PK
	null																		[ISN_FEATURE]						--
	@KOD																		[ISN_OBJ]							--
	703																			[KIND]								-- 'unit'
	null																		[ORDER_NUM]							--
	@L17																		[]									--

	$outsql			update tblREF_FEATURE set ISN_FEATURE =
	$outsql			  (select top 1 F.ISN_FEATURE from tblFEATURE F where F.NAME = @L17 and %F.ISN_FEATURE%)
	$outsql			where ISN_REF_FEATURE = @NewID3
END