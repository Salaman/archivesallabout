﻿-- Вопрос

/* поля не используются: - */

BEGIN
	if not exists(select C.ISN_CLS from tblCLS C where C.NAME = 'Межфондовые классификаторы' and %C.ISN_CLS%)
	insert into tblCLS
	([ID],[OwnerID],[CreationDateTime],[StatusID],[Deleted],[ISN_CLS],[ISN_HIGH_CLS],[CODE],[WEIGHT],[NAME],[OBJ_KIND],[MULTISELECT],[NOTE],[FOREST_ELEM],[PROTECTED])
	values (@ConstGUID,@OwnerID,GETDATE(),@StatusID,0,@ConstID,null,null,null,'Межфондовые классификаторы',701,'Y',null,'F','N')

	if not exists(select top 1 C.ISN_CLS from tblCLS C where C.NAME = 'Каталог' and %C.ISN_CLS%)
	begin
		insert into tblCLS
		([ID],[OwnerID],[CreationDateTime],[StatusID],[Deleted],[ISN_CLS],[ISN_HIGH_CLS],[CODE],[WEIGHT],[NAME],[OBJ_KIND],[MULTISELECT],[NOTE],[FOREST_ELEM],[PROTECTED])
		values (@ConstGUID8,@OwnerID,GETDATE(),@StatusID,0,@ConstID8,null,null,null,'Каталог',701,'Y',null,'T','N')

		update tblCLS set ISN_HIGH_CLS =
		(select top 1 C.ISN_CLS from tblCLS C where C.NAME = 'Межфондовые классификаторы' and %C.ISN_CLS%)
		where ISN_CLS = @ConstID8 and [NAME] = 'Каталог'
	end
END

BEGIN [VOPROS].[tblCLS]
	#primary
	"link																		NewID
	$pk																			NewID					-- ISN_CLS
	@NewID																		[ISN_CLS]				-- PK
	@KOD																		[]
	@V1																			[NAME]
	@V3																			[]
	@V4																			[]
	@V5																			[]
	@V6																			[]
	@V7																			[]
	null																		[ISN_HIGH_CLS]
	@KOD																		[CODE]					-- !!! временно, потом чистим !!!
	null																		[WEIGHT]
	701																			[OBJ_KIND]
	'География:'+char(32)														[NOTE]
	'Y'																			[MULTISELECT]
	'B'																			[FOREST_ELEM]
	'N'																			[PROTECTED]
	
	$outsql			update tblCLS set ISN_HIGH_CLS =
	$outsql			  (select top 1 C.ISN_CLS from tblCLS C where C.NAME = 'Каталог' and %C.ISN_CLS%)
	$outsql			where tblCLS.ISN_CLS = @NewID
END

BEGIN [VOPROS].[tblQUESTION]
	#primary
	"link																		KOD
	$pk																			KOD						-- ISN_QUESTION
	@KOD																		[ISN_QUESTION]			-- PK
	@V1																			[NAME]
	@V3																			[]
	@V4																			[]
	@V5																			[]
	@V6																			[]
	@V7																			[]
END

BEGIN [VOPROS].[tblREF_CLS]
	#relative
	"link																		KOD,NewID,NewID2
	$fk																			FOND
	@NewID2																		[ISN_REF_CLS]
	@NewID																		[ISN_CLS]
	null																		[ISN_TREE]
	@KOD																		[ISN_OBJ]
	@V3																			[]
	@V4																			[]
	@V5																			[]
	@V6																			[]
	@V7																			[]
	708																			[KIND]
	
	$insql			if exists(select ISN_CLS from tblCLS where NOTE in (V3, V4, V5, V6, V7))
	$insql			select @Result = 0 else select @Result = 1
	
	$outsql			update tblREF_CLS set ISN_TREE =
	$outsql			  (select top 1 C.ISN_CLS from tblCLS C where C.NOTE in (V3, V4, V5, V6, V7))
	$outsql			where ISN_REF_CLS = @NewID3
END