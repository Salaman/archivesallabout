﻿-- Физическое состояние документов

/* поля не используются:
[ISN_REF_ACT]
[PAGE_NUMS]
[NOTE]
[IS_ACTUAL] */

BEGIN [DELO2].[tblUNIT_STATE]
	#relative
	"link																		DELO,NewID
	"null																		M2
	$pk																			NewID						-- ISN_UNIT_STATE
	$fk																			DELO						-- ISN_UNIT
	@NewID																		[ISN_UNIT_STATE]			-- PK
	@DELO																		[ISN_UNIT]					-- FK
	@M1																			[]							-- ФИЗИЧЕСКОЕ СОСТОЯНИЕ ()
	@M2+16449																	[ISN_STATE]					-- ДЕФЕКТЫ ОСНОВЫ ()
	@M3																			[PAGE_COUNT]				-- КОЛ-ВО ЛИСТОВ
	@M4																			[]							-- ДЕФЕКТЫ ТЕКСТА ()
	@M5																			[]							-- КОЛ-ВО ЛИСТОВ
	@M6																			[]							-- ГОД ОЦЕНКИ ФИЗИЧЕСКОГО СОСТОЯНИЯ

	$insql				if exists(select ISN_UNIT from tblUNIT where ISN_UNIT = @DELO)
	$insql				and (select HAS_DEFECTS from tblUNIT where ISN_UNIT = @DELO) = 'Y'
	$insql				select @Result = 0 else select @Result = 1
END

BEGIN [DELO2].[tblUNIT_STATE]
	#relative
	"link																		DELO,NewID2
	"null																		M4
	$pk																			NewID2						-- ISN_UNIT_STATE
	$fk																			DELO						-- ISN_UNIT
	@NewID2																		[ISN_UNIT_STATE]			-- PK
	@DELO																		[ISN_UNIT]					-- FK
	@M1																			[]							-- ФИЗИЧЕСКОЕ СОСТОЯНИЕ ()
	@M2																			[]							-- ДЕФЕКТЫ ОСНОВЫ ()
	@M3																			[]							-- КОЛ-ВО ЛИСТОВ
	@M4+16506																	[ISN_STATE]					-- ДЕФЕКТЫ ТЕКСТА ()
	@M5																			[PAGE_COUNT]				-- КОЛ-ВО ЛИСТОВ
	@M6																			[]							-- ГОД ОЦЕНКИ ФИЗИЧЕСКОГО СОСТОЯНИЯ

	$insql				if exists(select ISN_UNIT from tblUNIT where ISN_UNIT = @DELO)
	$insql				and (select HAS_DEFECTS from tblUNIT where ISN_UNIT = @DELO) = 'Y'
	$insql				select @Result = 0 else select @Result = 1
END

BEGIN [DELO2].[tblWORK_CL]
	#primary
	"null																		M1
	$set																		NewID3
	$pk																			NewID3						-- ISN_WORK
	$parse																		M1							--
	@NewID3																		[ISN_WORK]					-- PK
	@Parse																		[NAME]						--
	
	$insql				if not (@Parse in ('реставрация','переплет','дезинфекция','дезинсекция','дезинсекция','обложка','н/п','текст','нумерация'))
	$insql				and not exists(select W.ISN_WORK from tblWORK_CL W where W.NAME = @Parse and %W.ISN_WORK%)
	$insql				select @Result = 0 else select @Result = 1
END

BEGIN [DELO2].[tblUNIT_REQUIRED_WORK]
	#relative
	"link																		DELO,NewID4
	"null																		M1
	$pk																			NewID4						-- ISN_UNIT_REQUIRED_WORK
	$fk																			DELO						-- ISN_WORK
	$parse																		M1							--
	@NewID4																		[ISN_UNIT_REQUIRED_WORK]	-- PK
	null																		[ISN_WORK]					--
	@DELO																		[ISN_UNIT]					--
	@M6																			[CHECK_DATE]				--
	'Y'                                                                         [IS_ACTUAL]
	@Parse																		[]							--
	
	$insql				if exists(select ISN_UNIT from tblUNIT where ISN_UNIT = @DELO)
	$insql				select @Result = 0 else select @Result = 1
	
	$outsql				if (@Parse in ('реставрация','переплёт','дезинфекция','дезинсекция','дезинсекция','обложка','н/п','текст','нумерация'))
	$outsql					update tblUNIT_REQUIRED_WORK set ISN_WORK = case
	$outsql					when @Parse = 'реставрация' then 1
	$outsql					when @Parse = 'переплёт' then 2
	$outsql					when @Parse = 'дезинфекция' then 3
	$outsql					when @Parse = 'дезинсекция' then 4
	$outsql					when @Parse = 'шифровка' then 7
	$outsql					when @Parse = 'обложка' then 8
	$outsql					when @Parse = 'н/п' then 11
	$outsql					when @Parse = 'текст' then 16446
	$outsql					when @Parse = 'нумерация' then 16447
	$outsql					end where ISN_UNIT_REQUIRED_WORK = @NewID4
	$outsql				else
	$outsql					update tblUNIT_REQUIRED_WORK set ISN_WORK =
	$outsql					  (select W.ISN_WORK from tblWORK_CL W where W.NAME = @Parse and %W.ISN_WORK%)
	$outsql					where ISN_UNIT_REQUIRED_WORK = @NewID4
END
