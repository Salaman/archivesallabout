﻿-- Характеристика фотодокументов

/* поля не используются:
[STORAGE_INFO]
[PRINT_COUNT]
[DUP_NEGATIVE_COUNT]
[SLIDE_COUNT]
[SHOT_COUNT] */

BEGIN [DELO5].[tblUNIT_FOTO]
	#relative
	"link																		DELO
	$fk																			DELO						-- ISN_UNIT
	@DELO																		[ISN_UNIT]					-- FK (DELO.KOD)
	@O1																			[PLACE]						-- МЕСТО СЪЕМКИ
	@O2																			[AUTHOR]					-- АВТОР СЪЕМКИ
	@O3																			[ORIGINAL]					-- ОРИГИНАЛЬНОСТЬ
	case(@O4=1:'b',2:'c')														[BASE_KIND]					-- ВИД ОСНОВЫ ()
	case(@O5=1:16376,2:16370,3:16369,4:16374,5:16372)							[ISN_DOC_KIND]				-- ВИД ФОТОДОКУМЕНТОВ ()
	case(@O6=1:'b',2:'a',3:'a')													[COLOR]						-- ЦВЕТНОСТЬ ()
	@O7																			[SIZE]						-- РАЗМЕР
	@O8																			[POSITIVE_COUNT]			-- КОЛ-ВО ПОЗИТИВОВ
	@O9																			[NEGATIVE_COUNT]			-- КОЛ-ВО НЕГАТИВОВ
	@O10																		[BACKUP_COUNT]				-- КОЛ-ВО СТРАХ.КОПИЙ
	@O11																		[ACCOMP_DOC]				-- СОСТАВ ТЕКСТОВОЙ СОПРОВОДИТЕЛЬНОЙ ДОКУМЕНТАЦИИ
	@O12																		[FOTO_DATE]					-- ДАТА СЪЕМКИ

	$insql				if exists(select ISN_UNIT from tblUNIT where ISN_UNIT = @DELO)
	$insql				and (select ISN_DOC_TYPE from tblUNIT where ISN_UNIT = @DELO) = 6
	$insql				select @Result = 0 else select @Result = 1
END
