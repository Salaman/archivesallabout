﻿-- Фонд

/* поля не используются:
[WAS_RENAMED]
[ISN_OAF]
[OAF_NOTE]
[HAS_ELECTRONIC_DOCS]
[HAS_TRADITIONAL_DOCS]
[UNDECSRIBED_PAGE_COUNT]
[ADDITIONAL_NSA]
[KEYWORDS]
[NOTE]
[PERSONAL_UNDESCRIBED_DOC_COUNT]
[IS_LOST] */

BEGIN [FOND].[tblFUND]
	#primary
	"link																		KOD,ArchiveID
	$pk																			KOD									-- ISN_FUND
	$fk																			ArchiveID							-- ISN_ARCHIVE
	@ArchiveID																	[ISN_ARCHIVE]						-- FK
	@KOD																		[ISN_FUND]							-- PK
	substring(@FKOD,1,1)														[FUND_NUM_1]						-- НОМЕР ФОНДА
	ltrim(rtrim(substring(@FKOD,3,5)))											[FUND_NUM_2]						-- НОМЕР ФОНДА
	substring(@FKOD,8,1)														[FUND_NUM_3]						-- НОМЕР ФОНДА
	@FNAME																		[FUND_NAME_SHORT]					-- СОКРАЩЕННОЕ НАЗВАНИЕ ФОНДА
	@A1																			[FUND_NAME_FULL]					-- НАЗВАНИЕ ФОНДА
	case(@A2=0:1,1:2,2:3)														[ISN_PERIOD]						-- ИСТОРИЧЕСКИЙ ПЕРИОД (0,1,2)
	case(@A3=1:'a',2:'b',3:'c','d')												[FUND_CATEGORY]						-- КАТЕГОРИЯ ФОНДА (0,1,2,3)
	case(@A4=1:1,2:3,3:4,4:2,5:7,6:5,7:6,8:8,9:10,10:9)							[ISN_DOC_TYPE]						-- ТИП ФОНДА ()
	case(@A4=9:'E','T')															[CARRIER_TYPE]						--
	@A5																			[INVENTORY_COUNT]					-- КОЛИЧЕСТВО ОПИСЕЙ
	@A6																			[AUTO_INVENTORY_COUNT]				-- ВВЕДЕНО ОПИСЕЙ
	@A7																			[DOC_START_YEAR]					-- НАЧАЛЬНАЯ ДАТА ДОКУМЕНТА
	case(@A8='*':'Y','':'N')													[DOC_START_YEAR_INEXACT]			-- НАЧАЛЬНАЯ ДАТА ДОКУМЕНТА ПРИБЛИЗИТЕЛЬНА (*)
	@A9																			[DOC_END_YEAR]						-- КОНЕЧНАЯ ДАТА ДОКУМЕНТА
	case(@A10='*':'Y','':'N')													[DOC_END_YEAR_INEXACT]				-- КОНЕЧНАЯ ДАТА ДОКУМЕНТА ПРИБЛИЗИТЕЛЬНА (*)
	@A11																		[DOC_RECEIPT_YEAR]					-- ДАТА 1-го ПОСТУПЛЕНИЯ ДОКУМЕНТОВ (год)
	@A12																		{}									-- ИСТОЧНИК ПОСТУПЛЕНИЯ (ТИП ИСТОЧНИКА ПОСТУПЛЕНИЯ)
	@A13																		{}									-- ОСНОВАНИЕ  ПОСТУПЛЕНИЯ
	@A14																		[LAST_CHECKED_YEAR]					-- ПОСЛЕДНЯЯ ПРОВЕРКА НАЛИЧИЯ (ДАТА ПОСЛЕДНЕЙ ПРОВЕРКИ НАЛИЧИЯ)
	@A15																		[IS_IN_SEARCH]						-- РОЗЫСК (НАХОДИТСЯ ЛИ В РОЗЫСКЕ)
	@A16																		[]									-----------------------------------
	@A17																		[]									--
	@A18																		[]									--
	@A19																		[]									--
	@A20																		[]									--
	@A21																		[]									--
	@A22																		[]									--
	@A23																		[]									-- ОБЪЕМ ФОНДА
	@A24																		[]									--
	@A25																		[]									--
	@A26																		[]									--
	@A27																		[]									--
	@A28																		[]									--
	@A29																		[]									--
	@A30																		[]									--
	@A31																		[]									-----------------------------------
	@A32																		[]									-----------------------------------
	@A33																		[]									--
	@A34																		[]									--
	@A35																		[]									-- ОБЪЕМ ЕД.ХР. ПО ОПИСЯМ
	@A36																		[]									--
	@A37																		[]									--
	@A38																		[]									-----------------------------------
	@A39																		[]									-----------------------------------
	@A40																		[]									--
	@A41																		[]									--
	@A42																		[]									--
	@A43																		[]									--
	@A44																		[]									-- НЕОПИСАННЫЕ 
	@A45																		[]									-- (КОЛИЧЕСТВО НЕОПИСАННЫХ  ДОКУМЕНТОВ)
	@A46																		[]									--
	@A47																		[]									--
	@A48																		[]									--
	@A49																		[]									-----------------------------------
	@A50																		[]									-----------------------------------
	@A51																		[]									--
	@A52																		[]									--
	@A53																		[]									-- ОСОБО ЦЕННЫЕ ДОКУМЕНТЫ
	@A54																		[]									-- (КОЛИЧЕСТВО УЧТЕННЫХ ОЦД)
	@A55																		[]									--
	@A56																		[]									-----------------------------------
	@A57																		[]									-----------------------------------
	@A58																		[]									--
	@A59																		[]									--
	@A60																		[]									-- ИМЕЮТ СТРАХОВОЙ ФОНД
	@A61																		[]									-- (КОЛИЧЕСТВО ЕД.ХР., ИМЕЮЩИХ СФ)
	@A62																		[]									--
	@A63																		[]									-----------------------------------
	@A64																		[]									-----------------------------------
	@A65																		[]									--
	@A66																		[]									--
	@A67																		[]									-- ИМЕЮТ ФОНД ПОЛЬЗОВАНИЯ
	@A68																		[]									-- (КОЛИЧЕСТВО ЕД.ХР., ИМЕЮЩИХ ФП)
	@A69																		[]									--
	@A70																		[]									-----------------------------------
	@A71																		[]									-----------------------------------
	@A72																		[]									--
	@A73																		[]									--
	@A74																		[]									-- ЗАКАТАЛОГИЗИРОВАННЫЕ
	@A75																		[]									-- (КОЛИЧЕСТВО ЗАКАТАЛОГИЗИРОВАННЫХ ЕД.ХР./ЕД.УЧ.)
	@A76																		[]									--
	@A77																		[]									-----------------------------------
	@A78																		[]									-----------------------------------
	@A79																		[]									--
	@A80																		[]									--
	@A81																		[]									-- НЕОБНАРУЖЕННЫЕ
	@A82																		[]									-- (КОЛИЧЕСТВО НЕОБНАРУЖЕННЫХ ЕД.ХР.)
	@A83																		[]									--
	@A84																		[]									-----------------------------------
	@A85																		[]									-----------------------------------
	@A86																		[]									--
	@A87																		[]									-- СЕКРЕТНЫЕ
	@A88																		[]									-- (КОЛИЧЕСТВО СЕКРЕТНЫХ ЕД.ХР.)
	@A89																		[]									--
	@A90																		[]									-----------------------------------
	@A91																		[UNDESCRIBED_DOC_COUNT]				-- ... НЕОПИСАННЫЕ (КОЛИЧЕСТВО НЕОПИСАННЫХ  ДОКУМЕНТОВ)
	@A92																		[TREASURE_UNITS_COUNT]				-- ДРАГОЦЕННОСТИ (КОЛИЧЕСТВО ЕД.ХР., ИМЕЮЩИХ В СВОЕМ ОФОРМЛЕНИИ ДРАГ. КАМНИ И ДРАГ. МЕТАЛЛЫ)
	@A93																		[HAS_MUSEUM_ITEMS]					-- МУЗ.ПРЕДМЕТЫ (НАЛИЧИЕ МУЗЕЙНЫХ ПРЕДМЕТОВ В СОСТАВЕ ФОНДА
	@A94																		[]									-- ИЗО. МАТ. (НАЛИЧИЕ ИЗОБРАЗИТЕЛЬНЫХ МАТЕРИАЛОВ В СОСТАВЕ  ФОНДА) ()
	@A95																		{}									-- ЯЗЫК (ЯЗЫК ДОКУМЕНТОВ ФОНДА)
	@A96																		[HAS_UNDOCUMENTED_PERIODS]			-- НЕЗАДОК. ПЕРИОДЫ (НАЛИЧИЕ НЕЗАДОКУМЕНТИРОВАННЫХ ПЕРИОДОВ)
	@A97																		[HAS_INCLUSIONS]					-- ФОНДОВЫЕ ВКЛЮЧЕНИЯ (НАЛИЧИЕ ФОНДОВЫХ ВКЛЮЧЕНИЙ)
	case(@A98=1:'a',2:'d',3:'c',0:'a')											[KEEP_PERIOD]						-- СРОК ХРАНЕНИЯ
	case(@A99=1:'a',2:'b',3:'c',4:'d',5:'e',6:'f')								[PROPERTY]							-- СОБСТВЕННОСТЬ ()
	case(@A100=1:'a',2:'b',3:'b',4:'b',5:'b')									[PRESENCE_FLAG]						-- ДВИЖЕНИЕ ()
	case(@A100=1:null,2:'a',3:'b',4:'c',5:'d')									[ABSENCE_REASON]					-- ДВИЖЕНИЕ ()
	@A101																		[MOVEMENT_NOTE]						-- СПРАВКА (СПРАВКА ПО ДВИЖЕНИЮ)
	case(@A102=1:'a',2:'b',3:'d',4:'c')											[FUND_KIND]							-- ВИД (ВИД АРХИВНОГО ФОНДА)
	@A103																		[ISN_SECURLEVEL]					-- ХАРАКТЕРИСТИКА СЕКРЕТНОСТИ ()
	case(@A104=1:'o',2:'p')														[SECURITY_CHAR]						-- ДОСТУП (ДОСТУП К ДОКУМЕНТАМ) ()
	@A105																		{}									-- ПРИЧИНА ОГРАНИЧЕНИЯ ()
	@A106																		[JOIN_REASON]						-- ОСНОВАНИЕ  ОБЪЕДИНЕНИЯ
	@A107																		[]									-- ПРИЗНАКИ (ПРИЗНАКИ СОЗДАНИЯ) ()
	@A109																		[FUND_HISTORY]						-- ИСТОРИЧЕСКАЯ СПРАВКА
	@A110																		[ANNOTATE]							-- АННОТАЦИЯ
	@A111																		[]		 							--Строка содержит номер рубрикатора (1 символ) + номер рубрики (7 символов)
	@A112																		[]
	@A113																		[]
	@A114																		[]
	@A115																		[]
	case(@A116='Y':'a','N':'b')													[INVENTORY_STATE]					-- СОСТОЯНИЕ ОПИСИ
	@A117																		{}									-- ВНУТРИФОНДОВЫЕ ()
	@A118																		{}									-- МЕЖФОНДОВЫЕ ()
	@A119																		{}									-- ВНУТРИФОНДОВЫЕ ()
	@A120																		{}									-- МЕЖФОНДОВЫЕ ()
	@A121																		{}									-- КАТАЛОГИ ()
	@A122																		[CARD_COUNT]						-- Д/ПР.ВЕД.КАРТОТЕКИ ( КОЛИЧЕСТВО ДЕЛОПРОИЗВОДСТВЕННЫХ ВЕДОМСТВЕННЫХ КАРТОТЕК)
	@A123																		{}									-- ОБЗОРЫ
	@A124																		[LIST_COUNT]						-- ПЕРЕЧНИ (КОЛИЧЕСТВО ПЕРЕЧНЕЙ ДОКУМЕНТОВ)
	@A125																		[INNER_DB_COUNT]					-- ВНУТРИФОНД. (ВНУТРИФОНДОВЫЕ)
	@A126																		[FUND_DB_COUNT]						-- МЕЖФОНД. (МЕЖФОНДОВЫЕ)
	@A127																		[ARCHIVE_DB_COUNT]					-- МЕЖАРХ. (МЕЖАРХИВНЫЕ)
	@A128																		[]									-- ОСНОВАНИЕ ОЦЕНКИ ()
	@A129																		[]									-- ЗАКАРТОНИР. (ЗАКАРТОНИРОВАННЫЕ)
	@A130																		[]									-- Н/ПОВРЕЖДЕННЫЕ (НЕИСПРАВИМО ПОВРЕЖДЕННЫЕ)
	@A131																		[]									-- РЕСТАВРАЦИИЯ
	@A132																		[]									-- ПЕРЕПЛЕТ (ПОДШИВКА)
	@A133																		[]									-- ДЕЗИНФЕКЦИЯ
	@A134																		[]									-- ДЕЗИНСЕКЦИЯ
	@A135																		[]									-- ЗАТУХ.ТЕКСТЫ (ЗАТУХАЮЩИЕ ТЕКСТЫ)
	@A136																		[]									-- ШИФРОВКА
	@A137																		[]									-- ОБЛОЖКИ
	@A138																		[]									-- ГОР.ОСНОВА (ГОРЮЧАЯ ОСНОВА)
	@A139																		[]									-- КПО (КОНСЕРВАЦИОННО-ПРОФИЛАКТИЧЕСКАЯ ОБРАБОТКА)
	@A140																		[LAST_DOC_CHECK_YEAR]				-- ГОД ВЫВЕРКИ
	null																		[ISN_SECURITY_REASON]
	
	$outsql			if rtrim(ltrim(@FNAME))=''
	$outsql			update tblFUND set FUND_NAME_SHORT =
	$outsql			  (case when len(@A1)>128 then substring(@A1,1,128)+'...' else @A1 end)
	$outsql			where ISN_FUND = @KOD
		
	$val			A103
	$reg			^(?<R1>[0-3]{1})$
	$log			'Ошибка в разделе ФОНД (Фонд № {FOND_NUM = @KOD}) -> поле ХАРАКТЕРИСТИКА СЕКРЕТНОСТИ: недопустимое значение.'
	$log			'Возможные значения: "откр", "с", "чс"'
END

BEGIN [FOND].[tblFUND_CHECK]
	#relative
	"link																		KOD
	$fk																			KOD									-- ISN_FUND
	@KOD																		[ISN_FUND]							-- PK
	@A129																		[CARDBOARDED]						-- ЗАКАРТОНИР. (ЗАКАРТОНИРОВАННЫЕ)
	null																		[UNITS_NEED_CARDBOARDED]			--
	@A130																		[UNITS_DBR]							-- Н/ПОВРЕЖДЕННЫЕ (НЕИСПРАВИМО ПОВРЕЖДЕННЫЕ)
	@A131																		[UNITS_NEED_RESTORATION]			-- РЕСТАВРАЦИИЯ
	@A132																		[UNITS_NEED_BINDING]				-- ПЕРЕПЛЕТ (ПОДШИВКА)
	@A133																		[UNITS_NEED_DISINFECTION]			-- ДЕЗИНФЕКЦИЯ
	@A134																		[UNITS_NEED_DISINSECTION]			-- ДЕЗИНСЕКЦИЯ
	@A135																		[FADING_PAGES]						-- ЗАТУХ.ТЕКСТЫ (ЗАТУХАЮЩИЕ ТЕКСТЫ)
	@A136																		[UNITS_NEED_ENCIPHERING]			-- ШИФРОВКА
	@A137																		[UNITS_NEED_COVER_CHANGE]			-- ОБЛОЖКИ
	@A138																		[UNITS_INFLAMMABLE]					-- ГОР.ОСНОВА (ГОРЮЧАЯ ОСНОВА)
	@A139																		[UNITS_NEED_KPO]					-- КПО (КОНСЕРВАЦИОННО-ПРОФИЛАКТИЧЕСКАЯ ОБРАБОТКА)

	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @KOD)
	$insql			select @Result = 0 else select @Result = 1
END

BEGIN [FOND].[tblSECURITY_REASON]
	#primary
	"link																		KOD
	"null																		A105
	$pk																			NewID2								-- ISN_SECURITY_REASON
	@NewID2																		[ISN_SECURITY_REASON]				-- PK
	@A105																		[NAME]								-- ПРИЧИНА ОГРАНИЧЕНИЯ ()
	@KOD																		[]									--
	
	$insql			if exists(select S.ISN_SECURITY_REASON from tblSECURITY_REASON S where S.NAME = @A105 and %S.ISN_SECURITY_REASON%)
	$insql			update tblFUND set ISN_SECURITY_REASON = 
	$insql			  (select top 1 S.ISN_SECURITY_REASON from tblSECURITY_REASON S where S.NAME = @A105 and %S.ISN_SECURITY_REASON%)
	$insql			where ISN_FUND = @KOD
	
	$outsql			update tblFUND set ISN_SECURITY_REASON = @NewID2
	$outsql			where ISN_FUND = @KOD
END

BEGIN [FOND].[tblRECEIPT_REASON_CL]
	#primary
	"null																		A13	
	$set																		NewSmallID
	$pk																			NewSmallID					-- ISN_RECEIPT_REASON
	$parse																		A13
	@NewSmallID																	[ISN_RECEIPT_REASON]		-- PK
	@A13																		[]
	@Parse																		[NAME]						--
	
	$insql				if not (@Parse in ('истечение вед. срока хран.','ликв. учр. при правопреемнике','ликв. учр. без правопреемника','дарение','покупка','репарация','реституция','депозит','расп. АО'))
	$insql				and not exists(select R.ISN_RECEIPT_REASON from tblRECEIPT_REASON_CL R where R.NAME = @Parse and %R.ISN_RECEIPT_REASON%)
	$insql				select @Result = 0 else select @Result = 1
END

BEGIN [FOND].[tblFUND_RECEIPT_REASON]
	#relative
	"link																		KOD
	"null																		A13	
	$fk																			KOD									-- ISN_FUND
	$parse																		A13
	@KOD																		[ISN_FUND]							-- FK
	1																			[ISN_RECEIPT_REASON]				--
	1																			[ORDER_NUM]							--
	@A13																		[]
	@Parse																		[]									--
	@ParseID																	[]
	
	$insql				if exists(select ISN_FUND from tblFUND where ISN_FUND = @KOD)
	$insql				select @Result = 0 else select @Result = 1
	
	$outsql				if @Parse in ('истечение вед. срока хран.','ликв. учр. при правопреемнике','ликв. учр. без правопреемника','дарение','покупка','репарация','реституция','депозит','расп. АО')
	$outsql					update tblFUND_RECEIPT_REASON set ISN_RECEIPT_REASON = case
	$outsql					when @Parse = 'истечение вед. срока хран.' then 1
	$outsql					when @Parse = 'ликв. учр. при правопреемнике' then 2
	$outsql					when @Parse = 'ликв. учр. без правопреемника' then 3
	$outsql					when @Parse = 'дарение' then 4
	$outsql					when @Parse = 'покупка' then 5
	$outsql					when @Parse = 'репарация' then 6
	$outsql					when @Parse = 'реституция' then 7
	$outsql					when @Parse = 'депозит' then 8
	$outsql					when @Parse = 'расп. АО' then 9
	$outsql					end where ISN_FUND = @KOD and RowID = @ParseID
	$outsql				else
	$outsql					update tblFUND_RECEIPT_REASON set ISN_RECEIPT_REASON =
	$outsql					  (select top 1 R.ISN_RECEIPT_REASON from tblRECEIPT_REASON_CL R where R.NAME = @Parse and %R.ISN_RECEIPT_REASON%)
	$outsql					where ISN_FUND = @KOD and RowID = @ParseID
END

BEGIN [FOND].[tblRECEIPT_SOURCE_CL]
	#primary
	"null																		A12	
	$set																		NewSmallID2
	$pk																			NewSmallID2					-- ISN_RECEIPT_SOURCE
	$parse																		A12
	@NewSmallID2																[ISN_RECEIPT_SOURCE]		-- PK
	@A12																		[]
	@Parse																		[NAME]						--
	
	$insql				if not (@Parse in ('госуд. структ.','негос. структ.','част. лицо','др. госархив','муниц. архив','др. фонд','иниц. док.','неучт.','др. гос-во','иное','муницип. структ.'))
	$insql				and not exists(select R.ISN_RECEIPT_SOURCE from tblRECEIPT_SOURCE_CL R where R.NAME = @Parse and %R.ISN_RECEIPT_SOURCE%)
	$insql				select @Result = 0 else select @Result = 1
END

BEGIN [FOND].[tblFUND_RECEIPT_SOURCE]
	#relative
	"link																		KOD
	"null																		A12	
	$fk																			KOD									-- ISN_RECEIPT_SOURCE
	$parse																		A12
	@KOD																		[ISN_FUND]							-- FK
	1																			[ISN_RECEIPT_SOURCE]				--
	1																			[ORDER_NUM]							--
	@A12																		[]
	@Parse																		[]									--
	@ParseID																	[]
	
	$insql				if exists(select ISN_FUND from tblFUND where ISN_FUND = @KOD)
	$insql				select @Result = 0 else select @Result = 1
	
	$outsql				if @Parse in ('госуд. структ.','негос. структ.','част. лицо','др. госархив','муниц. архив','др. фонд','иниц. док.','неучт.','др. гос-во','иное','муницип. структ.')
	$outsql					update tblFUND_RECEIPT_SOURCE set ISN_RECEIPT_SOURCE = case
	$outsql					when @Parse = 'госуд. структ.' then 1
	$outsql					when @Parse = 'негос. структ.' then 2
	$outsql					when @Parse = 'част. лицо' then 3
	$outsql					when @Parse = 'др. госархив' then 4
	$outsql					when @Parse = 'муниц. архив' then 5
	$outsql					when @Parse = 'др. фонд' then 6
	$outsql					when @Parse = 'иниц. док.' then 7
	$outsql					when @Parse = 'неучт.' then 8
	$outsql					when @Parse = 'др. гос-во' then 9
	$outsql					when @Parse = 'иное' then 10
	$outsql					when @Parse = 'муницип. структ.' then 11
	$outsql					end where ISN_FUND = @KOD and RowID = @ParseID
	$outsql				else
	$outsql					update tblFUND_RECEIPT_SOURCE set ISN_RECEIPT_SOURCE =
	$outsql					  (select top 1 R.ISN_RECEIPT_SOURCE from tblRECEIPT_SOURCE_CL R where R.NAME = @Parse and %R.ISN_RECEIPT_SOURCE%)
	$outsql					where ISN_FUND = @KOD and RowID = @ParseID
END

BEGIN [FOND].[tblLANGUAGE_CL]
	#primary
	"null																		A95	
	$set																		NewSmallID3
	$pk																			NewSmallID3							-- ISN_LANGUAGE
	@NewSmallID3																[ISN_LANGUAGE]						-- PK
	@A95																		[NAME]								-- ЯЗЫК
	
	$insql				if exists(select L.ISN_LANGUAGE from tblLANGUAGE_CL L where L.NAME = @A95 and %L.ISN_LANGUAGE%)
	$insql				select @Result = 1 else select @Result = 0
END

BEGIN [FOND].[tblREF_LANGUAGE]
	#relative
	"link																		NewID3,KOD
	"null																		A95	
	$pk																			NewID3								-- ISN_REF_LANGUAGE
	$fk																			KOD									-- ISN_OBJ
	@NewID3																		[ISN_REF_LANGUAGE]					-- PK
	@KOD																		[ISN_OBJ]							--
	1																			[ISN_LANGUAGE]						--
	701																			[KIND]								-- 'fund'
	@A95																		[]									--
	
	$insql				if exists(select ISN_FUND from tblFUND where ISN_FUND = @KOD)
	$insql				select @Result = 0 else select @Result = 1
	
	$outsql				update tblREF_LANGUAGE set ISN_LANGUAGE =
	$outsql				  (select top 1 L.ISN_LANGUAGE from tblLANGUAGE_CL L where L.NAME = @A95 and %L.ISN_LANGUAGE%)
	$outsql				where ISN_REF_LANGUAGE = @NewID3
END

BEGIN [FOND].[tblFUND_OAF_REASON]
	#relative
	"link																		KOD
	$fk																			KOD									-- ISN_FUND
	@A107																		[ISN_OAF_REASON]					-- PK
	@KOD																		[ISN_FUND]							-- FK
	@A102																		[]
	
	$insql				if exists(select ISN_FUND from tblFUND where ISN_FUND = @KOD)
	$insql				and @A102 = 2 and @A107 is not null
	$insql				begin
	$insql				  select @ResultLog = 0
	$insql				  if @A107 not like '[1234]'
	$insql				  select @ResultLog =
	$insql				  'Ошибка в разделе ФОНД (Фонд № {FOND_NUM = @KOD}) -> экран 3 -> поле ПРИЗНАКИ (ПРИЗНАКИ СОЗДАНИЯ): недопустимое значение.' +
	$insql				  'Возможные значения: "един. назн. ф/обр.", "подвед. ф/обр.", "смена ф/обр.", "един. объекта деят."'
	$insql				end else select @ResultLog = 1
END

BEGIN [FOND].[tblFUND_COLLECTION_REASONS]
	#relative
	"link																		KOD
	$fk																			KOD									-- ISN_FUND
	@A107																		[ISN_COLLECTION_REASON]				-- PK
	@KOD																		[ISN_FUND]							-- FK
	@A102																		[]
	
	$insql				if exists(select ISN_FUND from tblFUND where ISN_FUND = @KOD)
	$insql				and @A102 = 4 and @A107 is not null
	$insql				begin
	$insql				  select @ResultLog = 0
	$insql				  if @A107 not like '[12345]'
	$insql				  select @ResultLog =
	$insql				'Ошибка в разделе ФОНД (Фонд № {FOND_NUM = @KOD}) -> экран 3 -> поле ПРИЗНАКИ (ПРИЗНАКИ СОЗДАНИЯ): недопустимое значение.' +
	$insql				'Возможные значения: "номин.", "темат.", "хрон.", "объект.", "иное"'
	$insql				end else select @ResultLog = 1
END

BEGIN
	if not exists(select F.ISN_FEATURE from tblFEATURE F where F.NAME = 'Особенности' and %F.ISN_FEATURE%)
	insert into tblFEATURE
	([ID],[OwnerID],[CreationDateTime],[StatusID],[Deleted],[ISN_FEATURE],[ISN_HIGH_FEATURE],[CODE],[NAME],[NOTE],[FOREST_ELEM],[PROTECTED],[WEIGHT])
	values (@ConstGUID,@OwnerID,GETDATE(),@StatusID,0,@ConstID,null,null,'Особенности',null,'F','Y',1)
	
	if not exists(select F.ISN_FEATURE from tblFEATURE F where F.NAME = 'изо. материалы' and %F.ISN_FEATURE%)
	begin
		insert into tblFEATURE
		([ID],[OwnerID],[CreationDateTime],[StatusID],[Deleted],[ISN_FEATURE],[ISN_HIGH_FEATURE],[CODE],[NAME],[NOTE],[FOREST_ELEM],[PROTECTED],[WEIGHT])
		values (@ConstGUID2,@OwnerID,GETDATE(),@StatusID,0,@ConstID2,null,null,'изо. материалы',null,'T','N',1)
	
		update tblFEATURE set ISN_HIGH_FEATURE =
		(select top 1 F.ISN_FEATURE from tblFEATURE F where F.NAME = 'Особенности' and %F.ISN_FEATURE%)
		where ISN_FEATURE = @ConstID2
	end
END

BEGIN [FOND].[tblFEATURE]
	#primary
	"null																		A94	
	$set																		NewSmallID4
	$pk																			NewSmallID4						-- ISN_FEATURE
	@NewSmallID4																[ISN_FEATURE]					-- PK
	null																		[ISN_HIGH_FEATURE]				--
	null																		[CODE]							--
	@A94																		[NAME]							--
	null																		[NOTE]							--
	'B'																			[FOREST_ELEM]					--
	'N'																			[PROTECTED]						--
	
	$insql			if exists(select F.ISN_FEATURE from tblFEATURE F where F.NAME = @A94 and %F.ISN_FEATURE%)
	$insql			select @Result = 1 else select @Result = 0

	$outsql			update tblFEATURE set ISN_HIGH_FEATURE =
	$outsql			  (select top 1 F.ISN_FEATURE from tblFEATURE F where F.NAME = 'изо. материалы' and %F.ISN_FEATURE%)
	$outsql			where ISN_FEATURE = @NewSmallID4
END

BEGIN [FOND].[tblREF_FEATURE]
	#relative
	"link																		NewID4,KOD
	"null																		A94	
	$fk																			KOD									-- ISN_OBJ
	@NewID4																		[ISN_REF_FEATURE]					-- PK
	null																		[ISN_FEATURE]						--
	@KOD																		[ISN_OBJ]							--
	701																			[KIND]								-- 'fund'
	null																		[ORDER_NUM]							--
	@A94																		[]									--

	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @KOD)
	$insql			select @Result = 0 else select @Result = 1

	$outsql			update tblREF_FEATURE set ISN_FEATURE =
	$outsql			  (select top 1 F.ISN_FEATURE from tblFEATURE F where F.NAME = @A94 and %F.ISN_FEATURE%)
	$outsql			where ISN_REF_FEATURE = @NewID4
END

----------------------------------------------------------------------------------
-- CLS
----------------------------------------------------------------------------------

BEGIN [FOND].[tblREF_CLS]
	#relative
	"link																		StepID,KOD
	$fk																			KOD
	@StepID																		[ISN_REF_CLS]
	null																		[ISN_CLS]
	null																		[ISN_TREE]
	@KOD																		[ISN_OBJ]
	701																			[KIND]
	@A111																		[]
	
	$insql			if 1=0
	$insql			and 1=1
	$insql			select @Result = 0 else select @Result = 1
	
	$outsql			update tblREF_CLS set ISN_CLS =
	$outsql			  (select top 1 C.ISN_CLS from tblCLS C where  @A111 like (RTRIM(C.NOTE) +' 0казябра') and %C.ISN_CLS%)
	$outsql			where ISN_REF_CLS = @StepID
	
	$outsql			update tblREF_CLS set ISN_TREE =
	$outsql			  (select top 1 C.ISN_HIGH_CLS from tblCLS C where  @A111 like (RTRIM(C.NOTE) +' 0казябра') and %C.ISN_CLS%)
	$outsql			where ISN_REF_CLS = @StepID
END

BEGIN [FOND].[tblREF_CLS]
	#relative
	"link																		StepID,KOD
	$fk																			KOD
	@StepID																		[ISN_REF_CLS]
	null																		[ISN_CLS]
	null																		[ISN_TREE]
	@KOD																		[ISN_OBJ]
	701																			[KIND]
	@A112																		[]
	
	$insql			if 1=0
	$insql			and 1=1
	$insql			select @Result = 0 else select @Result = 1
	
	$outsql			update tblREF_CLS set ISN_CLS =
	$outsql			  (select top 1 C.ISN_CLS from tblCLS C where @A112 like (RTRIM(C.NOTE) +' 0казябра')  and %C.ISN_CLS%)
	$outsql			where ISN_REF_CLS = @StepID
	
	$outsql			update tblREF_CLS set ISN_TREE =
	$outsql			  (select top 1 C.ISN_HIGH_CLS from tblCLS C where @A112 like (RTRIM(C.NOTE) +' 0казябра')  and %C.ISN_CLS%)
	$outsql			where ISN_REF_CLS = @StepID
END

BEGIN [FOND].[tblREF_CLS]
	#relative
	"link																		StepID,KOD
	$fk																			KOD
	@StepID																		[ISN_REF_CLS]
	null																		[ISN_CLS]
	null																		[ISN_TREE]
	@KOD																		[ISN_OBJ]
	701																			[KIND]
	@A113																		[]
	
	$insql			if 1=0
	$insql			and 1=1
	$insql			select @Result = 0 else select @Result = 1
	
	$outsql			update tblREF_CLS set ISN_CLS =
	$outsql			  (select top 1 C.ISN_CLS from tblCLS C where  @A113 like (RTRIM(C.NOTE) +' 0казябра')  and %C.ISN_CLS%)
	$outsql			where ISN_REF_CLS = @StepID
	
	$outsql			update tblREF_CLS set ISN_TREE =
	$outsql			  (select top 1 C.ISN_HIGH_CLS from tblCLS C where  @A113 like (RTRIM(C.NOTE) +' 0казябра')  and %C.ISN_CLS%)
	$outsql			where ISN_REF_CLS = @StepID
END

BEGIN [FOND].[tblREF_CLS]
	#relative
	"link																		StepID,KOD
	$fk																			KOD
	@StepID																		[ISN_REF_CLS]
	null																		[ISN_CLS]
	null																		[ISN_TREE]
	@KOD																		[ISN_OBJ]
	701																			[KIND]
	@A114																		[]
	
	$insql			if 1=0
	$insql			and 1=1
	$insql			select @Result = 0 else select @Result = 1
	
	$outsql			update tblREF_CLS set ISN_CLS =
	$outsql			  (select top 1 C.ISN_CLS from tblCLS C where  @A114 like (RTRIM(C.NOTE) +' 0казябра')  and %C.ISN_CLS%)
	$outsql			where ISN_REF_CLS = @StepID
	
	$outsql			update tblREF_CLS set ISN_TREE =
	$outsql			  (select top 1 C.ISN_HIGH_CLS from tblCLS C where  @A114 like (RTRIM(C.NOTE) +' 0казябра')  and %C.ISN_CLS%)
	$outsql			where ISN_REF_CLS = @StepID
END

BEGIN [FOND].[tblREF_CLS]
	#relative
	"link																		StepID,KOD
	$fk																			KOD
	@StepID																		[ISN_REF_CLS]
	null																		[ISN_CLS]
	null																		[ISN_TREE]
	@KOD																		[ISN_OBJ]
	701																			[KIND]
	@A115																		[]
	
	$insql			if 1=0
	$insql			and 1=1
	$insql			select @Result = 0 else select @Result = 1
	
	$outsql			update tblREF_CLS set ISN_CLS =
	$outsql			  (select top 1 C.ISN_CLS from tblCLS C where  @A115 like (RTRIM(C.NOTE) +' 0казябра')  and %C.ISN_CLS%)
	$outsql			where ISN_REF_CLS = @StepID
	
	$outsql			update tblREF_CLS set ISN_TREE =
	$outsql			  (select top 1 C.ISN_HIGH_CLS from tblCLS C where  @A115 like (RTRIM(C.NOTE) +' 0казябра')  and %C.ISN_CLS%)
	$outsql			where ISN_REF_CLS = @StepID
END

----------------------------------------------------------------------------------
-- FUND_PAPER
----------------------------------------------------------------------------------

BEGIN [FOND].[tblFUND_PAPER_CLS]
	#relative
	"link																		KOD
	$parse																		A117
	"null																		Parse
	$fk																			KOD						-- ISN_FUND
	@KOD																		[ISN_FUND]				-- FK
	null																		[ISN_PAPER_CLS]			--
	@Parse																		[]						--
	@ParseID																	[]

	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @KOD)
	$insql			select @Result = 0 else select @Result = 1

	$outsql			if @Parse in ('предм.','тем.','темат.','им.','геогр.','хрон.','ист. учр.','видов док.','структ.','отрасл.','перечень вопр.','фонд.','по л/с','опись-указ.','сист.','объект.','предм.-тем.','фильм.','журн.','реж.','опер.','студ.','авт.','исп.')
	$outsql				update tblFUND_PAPER_CLS set ISN_PAPER_CLS =
	$outsql					(select top 1 P.ISN_PAPER_CLS from tblPAPER_CLS P where
	$outsql					P.NAME = case
	$outsql						when @Parse = 'предм.'			then 'предметный'
	$outsql						when @Parse = 'тем.'			then 'тематический'
	$outsql						when @Parse = 'темат.'			then 'тематический'
	$outsql						when @Parse = 'им.'				then 'именной'
	$outsql						when @Parse = 'геогр.'			then 'географический'
	$outsql						when @Parse = 'хрон.'			then 'хронологический'
	$outsql						when @Parse = 'ист. учр.'		then 'по истории организаций'
	$outsql						when @Parse = 'видов док.'		then 'видов документов'
	$outsql						when @Parse = 'структ.'			then 'структурный'
	$outsql						when @Parse = 'отрасл.'			then 'отраслевой'
	$outsql						when @Parse = 'перечень вопр.'	then 'перечень вопросов'
	$outsql						when @Parse = 'фонд.'			then 'фондовый'
	$outsql						when @Parse = 'по л/с'			then 'по личному составу'
	$outsql						when @Parse = 'опись-указ.'		then 'опись-указатель'
	$outsql						when @Parse = 'сист.'			then 'систематический'
	$outsql						when @Parse = 'объект.'			then 'объектный'
	$outsql						when @Parse = 'предм.-тем.'		then 'предметно-тематический'
	$outsql						when @Parse = 'фильм.'			then 'фильмовый'
	$outsql						when @Parse = 'журн.'			then 'журнальный'
	$outsql						when @Parse = 'реж.'			then 'режиссерский'
	$outsql						when @Parse = 'опер.'			then 'операторский'
	$outsql						when @Parse = 'студ.'			then 'студийный'
	$outsql						when @Parse = 'авт.'			then 'авторский'
	$outsql						when @Parse = 'исп.'			then 'исполнительский'
	$outsql					end and P.SCOPE = 'b' and P.CLS_TYPE = 'b' and P.TYPEID = 702 and %P.ISN_PAPER_CLS%)
	$outsql				where ISN_PAPER_CLS is null and ISN_FUND = @KOD and RowID = @ParseID
	$outsql			else
	$outsql				update tblFUND_PAPER_CLS set ISN_PAPER_CLS =
	$outsql					(select top 1 P.ISN_PAPER_CLS from tblPAPER_CLS P where
	$outsql					P.NAME = @Parse and P.SCOPE = 'b' and P.CLS_TYPE = 'b' and P.TYPEID = 702 and %P.ISN_PAPER_CLS%)
	$outsql				where ISN_PAPER_CLS is null and ISN_FUND = @KOD and RowID = @ParseID
END

BEGIN [FOND].[tblFUND_PAPER_CLS]
	#relative
	"link																		KOD
	$parse																		A118
	"null																		Parse
	$fk																			KOD						-- ISN_FUND
	@KOD																		[ISN_FUND]				-- FK
	null																		[ISN_PAPER_CLS]			--
	@Parse																		[]						--
	@ParseID																	[]

	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @KOD)
	$insql			select @Result = 0 else select @Result = 1

	$outsql			if @Parse in ('предм.','тем.','темат.','им.','геогр.','хрон.','ист. учр.','видов док.','структ.','отрасл.','перечень вопр.','фонд.','по л/с','опись-указ.','сист.','объект.','предм.-тем.','фильм.','журн.','реж.','опер.','студ.','авт.','исп.')
	$outsql				update tblFUND_PAPER_CLS set ISN_PAPER_CLS =
	$outsql					(select top 1 P.ISN_PAPER_CLS from tblPAPER_CLS P where
	$outsql					P.NAME = case
	$outsql						when @Parse = 'предм.'			then 'предметный'
	$outsql						when @Parse = 'тем.'			then 'тематический'
	$outsql						when @Parse = 'темат.'			then 'тематический'
	$outsql						when @Parse = 'им.'				then 'именной'
	$outsql						when @Parse = 'геогр.'			then 'географический'
	$outsql						when @Parse = 'хрон.'			then 'хронологический'
	$outsql						when @Parse = 'ист. учр.'		then 'по истории организаций'
	$outsql						when @Parse = 'видов док.'		then 'видов документов'
	$outsql						when @Parse = 'структ.'			then 'структурный'
	$outsql						when @Parse = 'отрасл.'			then 'отраслевой'
	$outsql						when @Parse = 'перечень вопр.'	then 'перечень вопросов'
	$outsql						when @Parse = 'фонд.'			then 'фондовый'
	$outsql						when @Parse = 'по л/с'			then 'по личному составу'
	$outsql						when @Parse = 'опись-указ.'		then 'опись-указатель'
	$outsql						when @Parse = 'сист.'			then 'систематический'
	$outsql						when @Parse = 'объект.'			then 'объектный'
	$outsql						when @Parse = 'предм.-тем.'		then 'предметно-тематический'
	$outsql						when @Parse = 'фильм.'			then 'фильмовый'
	$outsql						when @Parse = 'журн.'			then 'журнальный'
	$outsql						when @Parse = 'реж.'			then 'режиссерский'
	$outsql						when @Parse = 'опер.'			then 'операторский'
	$outsql						when @Parse = 'студ.'			then 'студийный'
	$outsql						when @Parse = 'авт.'			then 'авторский'
	$outsql						when @Parse = 'исп.'			then 'исполнительский'
	$outsql					end and P.SCOPE = 'a' and P.CLS_TYPE = 'b' and P.TYPEID = 702 and %P.ISN_PAPER_CLS%)
	$outsql				where ISN_PAPER_CLS is null and ISN_FUND = @KOD and RowID = @ParseID
	$outsql			else
	$outsql				update tblFUND_PAPER_CLS set ISN_PAPER_CLS =
	$outsql					(select top 1 P.ISN_PAPER_CLS from tblPAPER_CLS P where
	$outsql					P.NAME = @Parse and P.SCOPE = 'a' and P.CLS_TYPE = 'b' and P.TYPEID = 702 and %P.ISN_PAPER_CLS%)
	$outsql				where ISN_PAPER_CLS is null and ISN_FUND = @KOD and RowID = @ParseID
END

BEGIN [FOND].[tblFUND_PAPER_CLS]
	#relative
	"link																		KOD
	$parse																		A119
	"null																		Parse
	$fk																			KOD						-- ISN_FUND
	@KOD																		[ISN_FUND]				-- FK
	null																		[ISN_PAPER_CLS]			--
	@Parse																		[]						--
	@ParseID																	[]

	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @KOD)
	$insql			select @Result = 0 else select @Result = 1

	$outsql			if @Parse in ('предм.','тем.','темат.','им.','геогр.','хрон.','ист. учр.','видов док.','структ.','отрасл.','перечень вопр.','фонд.','по л/с','опись-указ.','сист.','объект.','предм.-тем.','фильм.','журн.','реж.','опер.','студ.','авт.','исп.')
	$outsql				update tblFUND_PAPER_CLS set ISN_PAPER_CLS =
	$outsql					(select top 1 P.ISN_PAPER_CLS from tblPAPER_CLS P where
	$outsql					P.NAME = case
	$outsql						when @Parse = 'предм.'			then 'предметный'
	$outsql						when @Parse = 'тем.'			then 'тематический'
	$outsql						when @Parse = 'темат.'			then 'тематический'
	$outsql						when @Parse = 'им.'				then 'именной'
	$outsql						when @Parse = 'геогр.'			then 'географический'
	$outsql						when @Parse = 'хрон.'			then 'хронологический'
	$outsql						when @Parse = 'ист. учр.'		then 'по истории организаций'
	$outsql						when @Parse = 'видов док.'		then 'видов документов'
	$outsql						when @Parse = 'структ.'			then 'структурный'
	$outsql						when @Parse = 'отрасл.'			then 'отраслевой'
	$outsql						when @Parse = 'перечень вопр.'	then 'перечень вопросов'
	$outsql						when @Parse = 'фонд.'			then 'фондовый'
	$outsql						when @Parse = 'по л/с'			then 'по личному составу'
	$outsql						when @Parse = 'опись-указ.'		then 'опись-указатель'
	$outsql						when @Parse = 'сист.'			then 'систематический'
	$outsql						when @Parse = 'объект.'			then 'объектный'
	$outsql						when @Parse = 'предм.-тем.'		then 'предметно-тематический'
	$outsql						when @Parse = 'фильм.'			then 'фильмовый'
	$outsql						when @Parse = 'журн.'			then 'журнальный'
	$outsql						when @Parse = 'реж.'			then 'режиссерский'
	$outsql						when @Parse = 'опер.'			then 'операторский'
	$outsql						when @Parse = 'студ.'			then 'студийный'
	$outsql						when @Parse = 'авт.'			then 'авторский'
	$outsql						when @Parse = 'исп.'			then 'исполнительский'
	$outsql					end and P.SCOPE = 'b' and P.CLS_TYPE = 'b' and (P.TYPEID = 703 or P.TYPEID = 704) and %P.ISN_PAPER_CLS%)
	$outsql				where ISN_PAPER_CLS is null and ISN_FUND = @KOD and RowID = @ParseID
	$outsql			else
	$outsql				update tblFUND_PAPER_CLS set ISN_PAPER_CLS =
	$outsql					(select top 1 P.ISN_PAPER_CLS from tblPAPER_CLS P where
	$outsql					P.NAME = @Parse and P.SCOPE = 'b' and P.CLS_TYPE = 'b' and (P.TYPEID = 703 or P.TYPEID = 704) and %P.ISN_PAPER_CLS%)
	$outsql				where ISN_PAPER_CLS is null and ISN_FUND = @KOD and RowID = @ParseID
END

BEGIN [FOND].[tblFUND_PAPER_CLS]
	#relative
	"link																		KOD
	$parse																		A120
	"null																		Parse
	$fk																			KOD						-- ISN_FUND
	@KOD																		[ISN_FUND]				-- FK
	null																		[ISN_PAPER_CLS]			--
	@Parse																		[]						--
	@ParseID																	[]

	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @KOD)
	$insql			select @Result = 0 else select @Result = 1

	$outsql			if @Parse in ('предм.','тем.','темат.','им.','геогр.','хрон.','ист. учр.','видов док.','структ.','отрасл.','перечень вопр.','фонд.','по л/с','опись-указ.','сист.','объект.','предм.-тем.','фильм.','журн.','реж.','опер.','студ.','авт.','исп.')
	$outsql				update tblFUND_PAPER_CLS set ISN_PAPER_CLS =
	$outsql					(select top 1 P.ISN_PAPER_CLS from tblPAPER_CLS P where
	$outsql					P.NAME = case
	$outsql						when @Parse = 'предм.'			then 'предметный'
	$outsql						when @Parse = 'тем.'			then 'тематический'
	$outsql						when @Parse = 'темат.'			then 'тематический'
	$outsql						when @Parse = 'им.'				then 'именной'
	$outsql						when @Parse = 'геогр.'			then 'географический'
	$outsql						when @Parse = 'хрон.'			then 'хронологический'
	$outsql						when @Parse = 'ист. учр.'		then 'по истории организаций'
	$outsql						when @Parse = 'видов док.'		then 'видов документов'
	$outsql						when @Parse = 'структ.'			then 'структурный'
	$outsql						when @Parse = 'отрасл.'			then 'отраслевой'
	$outsql						when @Parse = 'перечень вопр.'	then 'перечень вопросов'
	$outsql						when @Parse = 'фонд.'			then 'фондовый'
	$outsql						when @Parse = 'по л/с'			then 'по личному составу'
	$outsql						when @Parse = 'опись-указ.'		then 'опись-указатель'
	$outsql						when @Parse = 'сист.'			then 'систематический'
	$outsql						when @Parse = 'объект.'			then 'объектный'
	$outsql						when @Parse = 'предм.-тем.'		then 'предметно-тематический'
	$outsql						when @Parse = 'фильм.'			then 'фильмовый'
	$outsql						when @Parse = 'журн.'			then 'журнальный'
	$outsql						when @Parse = 'реж.'			then 'режиссерский'
	$outsql						when @Parse = 'опер.'			then 'операторский'
	$outsql						when @Parse = 'студ.'			then 'студийный'
	$outsql						when @Parse = 'авт.'			then 'авторский'
	$outsql						when @Parse = 'исп.'			then 'исполнительский'
	$outsql					end and P.SCOPE = 'a' and P.CLS_TYPE = 'b' and (P.TYPEID = 703 or P.TYPEID = 704) and %P.ISN_PAPER_CLS%)
	$outsql				where ISN_PAPER_CLS is null and ISN_FUND = @KOD and RowID = @ParseID
	$outsql			else
	$outsql				update tblFUND_PAPER_CLS set ISN_PAPER_CLS =
	$outsql					(select top 1 P.ISN_PAPER_CLS from tblPAPER_CLS P where
	$outsql					P.NAME = @Parse and P.SCOPE = 'a' and P.CLS_TYPE = 'b' and (P.TYPEID = 703 or P.TYPEID = 704) and %P.ISN_PAPER_CLS%)
	$outsql				where ISN_PAPER_CLS is null and ISN_FUND = @KOD and RowID = @ParseID
END

BEGIN [FOND].[tblFUND_PAPER_CLS]
	#relative
	"link																		KOD
	$parse																		A121
	"null																		Parse
	$fk																			KOD						-- ISN_FUND
	@KOD																		[ISN_FUND]				-- FK
	null																		[ISN_PAPER_CLS]			--
	@Parse																		[]						--
	@ParseID																	[]

	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @KOD)
	$insql			select @Result = 0 else select @Result = 1

	$outsql			if @Parse in ('предм.','тем.','темат.','им.','геогр.','хрон.','ист. учр.','видов док.','структ.','отрасл.','перечень вопр.','фонд.','по л/с','опись-указ.','сист.','объект.','предм.-тем.','фильм.','журн.','реж.','опер.','студ.','авт.','исп.')
	$outsql				update tblFUND_PAPER_CLS set ISN_PAPER_CLS =
	$outsql					(select top 1 P.ISN_PAPER_CLS from tblPAPER_CLS P where
	$outsql					P.NAME = case
	$outsql						when @Parse = 'предм.'			then 'предметный'
	$outsql						when @Parse = 'тем.'			then 'тематический'
	$outsql						when @Parse = 'темат.'			then 'тематический'
	$outsql						when @Parse = 'им.'				then 'именной'
	$outsql						when @Parse = 'геогр.'			then 'географический'
	$outsql						when @Parse = 'хрон.'			then 'хронологический'
	$outsql						when @Parse = 'ист. учр.'		then 'по истории организаций'
	$outsql						when @Parse = 'видов док.'		then 'видов документов'
	$outsql						when @Parse = 'структ.'			then 'структурный'
	$outsql						when @Parse = 'отрасл.'			then 'отраслевой'
	$outsql						when @Parse = 'перечень вопр.'	then 'перечень вопросов'
	$outsql						when @Parse = 'фонд.'			then 'фондовый'
	$outsql						when @Parse = 'по л/с'			then 'по личному составу'
	$outsql						when @Parse = 'опись-указ.'		then 'опись-указатель'
	$outsql						when @Parse = 'сист.'			then 'систематический'
	$outsql						when @Parse = 'объект.'			then 'объектный'
	$outsql						when @Parse = 'предм.-тем.'		then 'предметно-тематический'
	$outsql						when @Parse = 'фильм.'			then 'фильмовый'
	$outsql						when @Parse = 'журн.'			then 'журнальный'
	$outsql						when @Parse = 'реж.'			then 'режиссерский'
	$outsql						when @Parse = 'опер.'			then 'операторский'
	$outsql						when @Parse = 'студ.'			then 'студийный'
	$outsql						when @Parse = 'авт.'			then 'авторский'
	$outsql						when @Parse = 'исп.'			then 'исполнительский'
	$outsql					end and P.SCOPE = 'a' and P.CLS_TYPE = 'a' and P.TYPEID = 700 and %P.ISN_PAPER_CLS%)
	$outsql				where ISN_PAPER_CLS is null and ISN_FUND = @KOD and RowID = @ParseID
	$outsql			else
	$outsql				update tblFUND_PAPER_CLS set ISN_PAPER_CLS =
	$outsql					(select top 1 P.ISN_PAPER_CLS from tblPAPER_CLS P where
	$outsql					P.NAME = @Parse and P.SCOPE = 'a' and P.CLS_TYPE = 'a' and P.TYPEID = 700 and %P.ISN_PAPER_CLS%)
	$outsql				where ISN_PAPER_CLS is null and ISN_FUND = @KOD and RowID = @ParseID
END

BEGIN [FOND].[tblFUND_PAPER_CLS]
	#relative
	"link																		KOD
	$parse																		A123
	"null																		Parse
	$fk																			KOD						-- ISN_FUND
	@KOD																		[ISN_FUND]				-- FK
	null																		[ISN_PAPER_CLS]			--
	@Parse																		[]						--
	@ParseID																	[]

	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @KOD)
	$insql			select @Result = 0 else select @Result = 1

	$outsql			if @Parse in ('предм.','тем.','темат.','им.','геогр.','хрон.','ист. учр.','видов док.','структ.','отрасл.','перечень вопр.','фонд.','по л/с','опись-указ.','сист.','объект.','предм.-тем.','фильм.','журн.','реж.','опер.','студ.','авт.','исп.')
	$outsql				update tblFUND_PAPER_CLS set ISN_PAPER_CLS =
	$outsql					(select top 1 P.ISN_PAPER_CLS from tblPAPER_CLS P where
	$outsql					P.NAME = case
	$outsql						when @Parse = 'предм.'			then 'предметный'
	$outsql						when @Parse = 'тем.'			then 'тематический'
	$outsql						when @Parse = 'темат.'			then 'тематический'
	$outsql						when @Parse = 'им.'				then 'именной'
	$outsql						when @Parse = 'геогр.'			then 'географический'
	$outsql						when @Parse = 'хрон.'			then 'хронологический'
	$outsql						when @Parse = 'ист. учр.'		then 'по истории организаций'
	$outsql						when @Parse = 'видов док.'		then 'видов документов'
	$outsql						when @Parse = 'структ.'			then 'структурный'
	$outsql						when @Parse = 'отрасл.'			then 'отраслевой'
	$outsql						when @Parse = 'перечень вопр.'	then 'перечень вопросов'
	$outsql						when @Parse = 'фонд.'			then 'фондовый'
	$outsql						when @Parse = 'по л/с'			then 'по личному составу'
	$outsql						when @Parse = 'опись-указ.'		then 'опись-указатель'
	$outsql						when @Parse = 'сист.'			then 'систематический'
	$outsql						when @Parse = 'объект.'			then 'объектный'
	$outsql						when @Parse = 'предм.-тем.'		then 'предметно-тематический'
	$outsql						when @Parse = 'фильм.'			then 'фильмовый'
	$outsql						when @Parse = 'журн.'			then 'журнальный'
	$outsql						when @Parse = 'реж.'			then 'режиссерский'
	$outsql						when @Parse = 'опер.'			then 'операторский'
	$outsql						when @Parse = 'студ.'			then 'студийный'
	$outsql						when @Parse = 'авт.'			then 'авторский'
	$outsql						when @Parse = 'исп.'			then 'исполнительский'
	$outsql					end and P.SCOPE = 'a' and P.CLS_TYPE = 'c' and P.TYPEID = 700 and %P.ISN_PAPER_CLS%)
	$outsql				where ISN_PAPER_CLS is null and ISN_FUND = @KOD and RowID = @ParseID
	$outsql			else
	$outsql				update tblFUND_PAPER_CLS set ISN_PAPER_CLS =
	$outsql					(select top 1 P.ISN_PAPER_CLS from tblPAPER_CLS P where
	$outsql					P.NAME = @Parse and P.SCOPE = 'a' and P.CLS_TYPE = 'c' and P.TYPEID = 700 and %P.ISN_PAPER_CLS%)
	$outsql				where ISN_PAPER_CLS is null and ISN_FUND = @KOD and RowID = @ParseID
END

----------------------------------------------------------------------------------
-- DOCUMENT_STATS
----------------------------------------------------------------------------------

BEGIN [FOND].[tblDOCUMENT_STATS]
	#relative
	"link																		KOD,StepID
	$pk																			StepID						-- ISN_DOCUMENT_STATS
	$fk																			KOD							-- ISN_FUND
	@StepID																		[ISN_DOCUMENT_STATS]
	@KOD																		[ISN_FUND]
	null																		[ISN_DOC_TYPE]
	null																		[ISN_INVENTORY]
	'A'																			[CARRIER_TYPE]
	
	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @KOD)
	$insql			select @Result = 0 else select @Result = 1
END

BEGIN [FOND].[tblDOCUMENT_STATS]
	#relative
	"link																		KOD,StepID
	$pk																			StepID						-- ISN_DOCUMENT_STATS
	$fk																			KOD							-- ISN_FUND
	@StepID																		[ISN_DOCUMENT_STATS]
	@KOD																		[ISN_FUND]
	null																		[ISN_DOC_TYPE]
	null																		[ISN_INVENTORY]
	'P'																			[CARRIER_TYPE]
	@A16																		[UNIT_COUNT]
	@A32																		[UNIT_INVENTORY]
	@A39																		[UNIT_UNDESCRIBED]
	@A50																		[UNIT_OC_COUNT]
	@A57																		[UNIT_HAS_SF]
	@A64																		[UNIT_HAS_FP]
	@A71																		[UNITS_CATALOGUED]
	@A78																		[UNITS_NOT_FOUND]
	@A85																		[SECRET_UNITS]
	
	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @KOD)
	$insql			select @Result = 0 else select @Result = 1
END

BEGIN [FOND].[tblDOCUMENT_STATS]
	#relative
	"link																		KOD,StepID
	$pk																			StepID						-- ISN_DOCUMENT_STATS
	$fk																			KOD							-- ISN_FUND
	@StepID																		[ISN_DOCUMENT_STATS]
	@KOD																		[ISN_FUND]
	null																		[ISN_DOC_TYPE]
	null																		[ISN_INVENTORY]
	'E'																			[CARRIER_TYPE]
	@A25																		[UNIT_COUNT]
	@A37																		[UNIT_INVENTORY]
	@A48																		[UNIT_UNDESCRIBED]
	@A55																		[UNIT_OC_COUNT]
	@A62																		[UNIT_HAS_SF]
	@A69																		[UNIT_HAS_FP]
	@A83																		[UNITS_NOT_FOUND]
	@A90																		[SECRET_UNITS]
	@A30																		[REG_UNIT]	
	@A76																		[REG_UNITS_CTALOGUE]

	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @KOD)
	$insql			select @Result = 0 else select @Result = 1
END

BEGIN [FOND].[tblDOCUMENT_STATS]
	#relative
	"link																		KOD,StepID
	$pk																			StepID						-- ISN_DOCUMENT_STATS
	$fk																			KOD							-- ISN_FUND
	@StepID																		[ISN_DOCUMENT_STATS]
	@KOD																		[ISN_FUND]
	null																		[ISN_DOC_TYPE]
	null																		[ISN_INVENTORY]
	null																		[CARRIER_TYPE]

	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @KOD)
	$insql			select @Result = 0 else select @Result = 1
END

BEGIN [FOND].[tblDOCUMENT_STATS]
	#relative
	"link																		KOD,StepID
	$pk																			StepID						-- ISN_DOCUMENT_STATS
	$fk																			KOD							-- ISN_FUND
	@StepID																		[ISN_DOCUMENT_STATS]
	@KOD																		[ISN_FUND]
	1																			[ISN_DOC_TYPE]
	null																		[ISN_INVENTORY]
	'P'																			[CARRIER_TYPE]

	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @KOD)
	$insql			select @Result = 0 else select @Result = 1
END

BEGIN [FOND].[tblDOCUMENT_STATS]
	#relative
	"link																		KOD,StepID
	$pk																			StepID						-- ISN_DOCUMENT_STATS
	$fk																			KOD							-- ISN_FUND
	@StepID																		[ISN_DOCUMENT_STATS]
	@KOD																		[ISN_FUND]
	2																			[ISN_DOC_TYPE]
	null																		[ISN_INVENTORY]
	'P'																			[CARRIER_TYPE]
	
	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @KOD)
	$insql			select @Result = 0 else select @Result = 1
END

BEGIN [FOND].[tblDOCUMENT_STATS]
	#relative
	"link																		KOD,StepID
	$pk																			StepID						-- ISN_DOCUMENT_STATS
	$fk																			KOD							-- ISN_FUND
	@StepID																		[ISN_DOCUMENT_STATS]
	@KOD																		[ISN_FUND]
	3																			[ISN_DOC_TYPE]
	null																		[ISN_INVENTORY]
	'P'																			[CARRIER_TYPE]

	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @KOD)
	$insql			select @Result = 0 else select @Result = 1
END

BEGIN [FOND].[tblDOCUMENT_STATS]
	#relative
	"link																		KOD,StepID
	$pk																			StepID						-- ISN_DOCUMENT_STATS
	$fk																			KOD							-- ISN_FUND
	@StepID																		[ISN_DOCUMENT_STATS]
	@KOD																		[ISN_FUND]
	4																			[ISN_DOC_TYPE]
	null																		[ISN_INVENTORY]
	'P'																			[CARRIER_TYPE]
	
	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @KOD)
	$insql			select @Result = 0 else select @Result = 1
END

BEGIN [FOND].[tblDOCUMENT_STATS]
	#relative
	"link																		KOD,StepID
	$pk																			StepID						-- ISN_DOCUMENT_STATS
	$fk																			KOD							-- ISN_FUND
	@StepID																		[ISN_DOCUMENT_STATS]
	@KOD																		[ISN_FUND]
	5																			[ISN_DOC_TYPE]
	null																		[ISN_INVENTORY]
	'A'																			[CARRIER_TYPE]
	
	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @KOD)
	$insql			select @Result = 0 else select @Result = 1
END

BEGIN [FOND].[tblDOCUMENT_STATS]
	#relative
	"link																		KOD,StepID
	$pk																			StepID						-- ISN_DOCUMENT_STATS
	$fk																			KOD							-- ISN_FUND
	@StepID																		[ISN_DOCUMENT_STATS]
	@KOD																		[ISN_FUND]
	6																			[ISN_DOC_TYPE]
	null																		[ISN_INVENTORY]
	'A'																			[CARRIER_TYPE]
	
	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @KOD)
	$insql			select @Result = 0 else select @Result = 1
END

BEGIN [FOND].[tblDOCUMENT_STATS]
	#relative
	"link																		KOD,StepID
	$pk																			StepID						-- ISN_DOCUMENT_STATS
	$fk																			KOD							-- ISN_FUND
	@StepID																		[ISN_DOCUMENT_STATS]
	@KOD																		[ISN_FUND]
	7																			[ISN_DOC_TYPE]
	null																		[ISN_INVENTORY]
	'A'																			[CARRIER_TYPE]
	
	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @KOD)
	$insql			select @Result = 0 else select @Result = 1
END

BEGIN [FOND].[tblDOCUMENT_STATS]
	#relative
	"link																		KOD,StepID
	$pk																			StepID						-- ISN_DOCUMENT_STATS
	$fk																			KOD							-- ISN_FUND
	@StepID																		[ISN_DOCUMENT_STATS]
	@KOD																		[ISN_FUND]
	8																			[ISN_DOC_TYPE]
	null																		[ISN_INVENTORY]
	'A'																			[CARRIER_TYPE]
	
	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @KOD)
	$insql			select @Result = 0 else select @Result = 1
END

BEGIN [FOND].[tblDOCUMENT_STATS]
	#relative
	"link																		KOD,StepID
	$pk																			StepID						-- ISN_DOCUMENT_STATS
	$fk																			KOD							-- ISN_FUND
	@StepID																		[ISN_DOCUMENT_STATS]
	@KOD																		[ISN_FUND]
	9																			[ISN_DOC_TYPE]
	null																		[ISN_INVENTORY]
	'M'																			[CARRIER_TYPE]
	
	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @KOD)
	$insql			select @Result = 0 else select @Result = 1
END

BEGIN [FOND].[tblDOCUMENT_STATS]
	#relative
	"link																		KOD,StepID
	$pk																			StepID						-- ISN_DOCUMENT_STATS
	$fk																			KOD							-- ISN_FUND
	@StepID																		[ISN_DOCUMENT_STATS]
	@KOD																		[ISN_FUND]
	4																			[ISN_DOC_TYPE]
	null																		[ISN_INVENTORY]
	'E'																			[CARRIER_TYPE]
	
	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @KOD)
	$insql			select @Result = 0 else select @Result = 1
END

BEGIN [FOND].[tblDOCUMENT_STATS]
	#relative
	"link																		KOD,StepID
	$pk																			StepID						-- ISN_DOCUMENT_STATS
	$fk																			KOD							-- ISN_FUND
	@StepID																		[ISN_DOCUMENT_STATS]
	@KOD																		[ISN_FUND]
	5																			[ISN_DOC_TYPE]
	null																		[ISN_INVENTORY]
	'E'																			[CARRIER_TYPE]
	
	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @KOD)
	$insql			select @Result = 0 else select @Result = 1
END

BEGIN [FOND].[tblDOCUMENT_STATS]
	#relative
	"link																		KOD,StepID
	$pk																			StepID						-- ISN_DOCUMENT_STATS
	$fk																			KOD							-- ISN_FUND
	@StepID																		[ISN_DOCUMENT_STATS]
	@KOD																		[ISN_FUND]
	6																			[ISN_DOC_TYPE]
	null																		[ISN_INVENTORY]
	'E'																			[CARRIER_TYPE]
	
	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @KOD)
	$insql			select @Result = 0 else select @Result = 1
END

BEGIN [FOND].[tblDOCUMENT_STATS]
	#relative
	"link																		KOD,StepID
	$pk																			StepID						-- ISN_DOCUMENT_STATS
	$fk																			KOD							-- ISN_FUND
	@StepID																		[ISN_DOCUMENT_STATS]
	@KOD																		[ISN_FUND]
	7																			[ISN_DOC_TYPE]
	null																		[ISN_INVENTORY]
	'E'																			[CARRIER_TYPE]
	
	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @KOD)
	$insql			select @Result = 0 else select @Result = 1
END

BEGIN [FOND].[tblDOCUMENT_STATS]
	#relative
	"link																		KOD,StepID
	$pk																			StepID						-- ISN_DOCUMENT_STATS
	$fk																			KOD							-- ISN_FUND
	@StepID																		[ISN_DOCUMENT_STATS]
	@KOD																		[ISN_FUND]
	8																			[ISN_DOC_TYPE]
	null																		[ISN_INVENTORY]
	'E'																			[CARRIER_TYPE]
	
	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @KOD)
	$insql			select @Result = 0 else select @Result = 1
END

BEGIN [FOND].[tblDOCUMENT_STATS]
	#relative
	"link																		KOD,StepID
	$pk																			StepID						-- ISN_DOCUMENT_STATS
	$fk																			KOD							-- ISN_FUND
	@StepID																		[ISN_DOCUMENT_STATS]
	@KOD																		[ISN_FUND]
	@A4																			[]
	@A17																		[]
	@A40																		[]
	@A20																		[]
	@A43																		[]
	@A18																		[]
	@A41																		[]
	@A19																		[]
	@A42																		[]
	@A22																		[]
	@A34																		[]
	@A45																		[]
	@A52																		[]
	@A59																		[]
	@A66																		[]
	@A77																		[]
	@A80																		[]
	@A87																		[]
	@A23																		[]
	@A35																		[]
	@A46																		[]
	@A53																		[]
	@A60																		[]
	@A67																		[]
	@A81																		[]
	@A88																		[]
	@A28																		[]
	@A74																		[]
	@A21																		[]
	@A33																		[]
	@A44																		[]
	@A51																		[]
	@A58																		[]
	@A65																		[]
	@A79																		[]
	@A86																		[]
	@A27																		[]
	@A72																		[]
	@A24																		[]
	@A36																		[]
	@A47																		[]
	@A54																		[]
	@A61																		[]
	@A68																		[]
	@A82																		[]
	@A89																		[]
	@A29																		[]
	@A75																		[]
	@A26																		[]
	@A38																		[]
	@A49																		[]
	@A56																		[]
	@A63																		[]
	@A70																		[]
	@A77																		[]
	@A84																		[]
	@A31																		[]
	
	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @KOD)
	$insql			begin
	$insql				update tblDOCUMENT_STATS set
	$insql					ISN_DOC_TYPE = 1,
	$insql					CARRIER_TYPE = 'P',
	$insql					UNIT_COUNT = @A17,
	$insql					UNIT_UNDESCRIBED = @A40
	$insql				where ISN_FUND = @KOD and ISN_INVENTORY is null and ISN_DOC_TYPE = 1 and CARRIER_TYPE = 'P'
	$insql				if (isnull(@A17,0) <> 0 or isnull(@A40,0) <> 0)
	$insql					insert into tblFUND_DOC_TYPE ([ID],[OwnerID],[CreationDateTime],[DocID],[RowID],[ISN_FUND],[ISN_DOC_TYPE])
	$insql					select newid(),@OwnerID,getdate(),ID,0,ISN_FUND,1 from tblFUND where ISN_FUND = @KOD
	$insql				update tblDOCUMENT_STATS set
	$insql					ISN_DOC_TYPE = 2,
	$insql					CARRIER_TYPE = 'P',
	$insql					UNIT_COUNT = @A20,
	$insql					UNIT_UNDESCRIBED = @A43
	$insql				where ISN_FUND = @KOD and ISN_INVENTORY is null and ISN_DOC_TYPE = 2 and CARRIER_TYPE = 'P'
	$insql				if (isnull(@A20,0) <> 0 or isnull(@A43,0) <> 0)
	$insql					insert into tblFUND_DOC_TYPE ([ID],[OwnerID],[CreationDateTime],[DocID],[RowID],[ISN_FUND],[ISN_DOC_TYPE])
	$insql					select newid(),@OwnerID,getdate(),ID,0,ISN_FUND,2 from tblFUND where ISN_FUND = @KOD
	$insql				update tblDOCUMENT_STATS set
	$insql					ISN_DOC_TYPE = 3,
	$insql					CARRIER_TYPE = 'P',
	$insql					UNIT_COUNT = @A18,
	$insql					UNIT_UNDESCRIBED = @A41
	$insql				where ISN_FUND = @KOD and ISN_INVENTORY is null and ISN_DOC_TYPE = 3 and CARRIER_TYPE = 'P'
	$insql				if (isnull(@A18,0) <> 0 or isnull(@A41,0) <> 0)
	$insql					insert into tblFUND_DOC_TYPE ([ID],[OwnerID],[CreationDateTime],[DocID],[RowID],[ISN_FUND],[ISN_DOC_TYPE])
	$insql					select newid(),@OwnerID,getdate(),ID,0,ISN_FUND,3 from tblFUND where ISN_FUND = @KOD
	$insql				update tblDOCUMENT_STATS set
	$insql					ISN_DOC_TYPE = 4,
	$insql					CARRIER_TYPE = 'P',
	$insql					UNIT_COUNT = @A19,
	$insql					UNIT_UNDESCRIBED = @A42
	$insql				where ISN_FUND = @KOD and ISN_INVENTORY is null and ISN_DOC_TYPE = 4 and CARRIER_TYPE = 'P'
	$insql				if (isnull(@A19,0) <> 0 or isnull(@A42,0) <> 0)
	$insql					insert into tblFUND_DOC_TYPE ([ID],[OwnerID],[CreationDateTime],[DocID],[RowID],[ISN_FUND],[ISN_DOC_TYPE])
	$insql					select newid(),@OwnerID,getdate(),ID,0,ISN_FUND,4 from tblFUND where ISN_FUND = @KOD
	$insql				update tblDOCUMENT_STATS set
	$insql					ISN_DOC_TYPE = 5,
	$insql					CARRIER_TYPE = 'A',
	$insql					UNIT_COUNT = @A22,
	$insql					UNIT_UNDESCRIBED = @A45,
	$insql					UNIT_INVENTORY = @A34,
	$insql					UNIT_OC_COUNT = @A52,
	$insql					UNIT_HAS_SF = @A59,
	$insql					UNIT_HAS_FP = @A66,
	$insql					UNITS_CATALOGUED = @A77,
	$insql					UNITS_NOT_FOUND = @A80,
	$insql					SECRET_UNITS = @A87
	$insql				where ISN_FUND = @KOD and ISN_INVENTORY is null and ISN_DOC_TYPE = 5 and CARRIER_TYPE = 'A'
	$insql				if (isnull(@A22,0) <> 0 or isnull(@A45,0) <> 0 or isnull(@A34,0) <> 0 or isnull(@A52,0) <> 0 or isnull(@A59,0) <> 0 or isnull(@A66,0) <> 0 or isnull(@A77,0) <> 0 or isnull(@A80,0) <> 0 or isnull(@A87,0) <> 0)
	$insql					insert into tblFUND_DOC_TYPE ([ID],[OwnerID],[CreationDateTime],[DocID],[RowID],[ISN_FUND],[ISN_DOC_TYPE])
	$insql					select newid(),@OwnerID,getdate(),ID,0,ISN_FUND,5 from tblFUND where ISN_FUND = @KOD
	$insql				update tblDOCUMENT_STATS set
	$insql					ISN_DOC_TYPE = 6,
	$insql					CARRIER_TYPE = 'A',
	$insql					UNIT_COUNT = @A23,
	$insql					UNIT_UNDESCRIBED = @A46,
	$insql					UNIT_INVENTORY = @A35,
	$insql					UNIT_OC_COUNT = @A53,
	$insql					UNIT_HAS_SF = @A60,
	$insql					UNIT_HAS_FP = @A67,
	$insql					UNITS_NOT_FOUND = @A81,
	$insql					SECRET_UNITS = @A88,
	$insql					REG_UNIT = @A28,
	$insql					REG_UNITS_CTALOGUE = @A74
	$insql				where ISN_FUND = @KOD and ISN_INVENTORY is null and ISN_DOC_TYPE = 6 and CARRIER_TYPE = 'A'
	$insql				if (isnull(@A23,0) <> 0 or isnull(@A46,0) <> 0 or isnull(@A35,0) <> 0 or isnull(@A53,0) <> 0 or isnull(@A60,0) <> 0 or isnull(@A67,0) <> 0 or isnull(@A81,0) <> 0 or isnull(@A88,0) <> 0 or isnull(@A28,0) <> 0 or isnull(@A74,0) <> 0)
	$insql					insert into tblFUND_DOC_TYPE ([ID],[OwnerID],[CreationDateTime],[DocID],[RowID],[ISN_FUND],[ISN_DOC_TYPE])
	$insql					select newid(),@OwnerID,getdate(),ID,0,ISN_FUND,6 from tblFUND where ISN_FUND = @KOD
	$insql				update tblDOCUMENT_STATS set
	$insql					ISN_DOC_TYPE = 7,
	$insql					CARRIER_TYPE = 'A',
	$insql					UNIT_COUNT = @A21,
	$insql					UNIT_UNDESCRIBED = @A44,
	$insql					UNIT_INVENTORY = @A33,
	$insql					UNIT_OC_COUNT = @A51,
	$insql					UNIT_HAS_SF = @A58,
	$insql					UNIT_HAS_FP = @A65,
	$insql					UNITS_NOT_FOUND = @A79,
	$insql					SECRET_UNITS = @A86,
	$insql					REG_UNIT = @A27,
	$insql					REG_UNITS_CTALOGUE = @A72
	$insql				where ISN_FUND = @KOD and ISN_INVENTORY is null and ISN_DOC_TYPE = 7 and CARRIER_TYPE = 'A'
	$insql				if (isnull(@A21,0) <> 0 or isnull(@A44,0) <> 0 or isnull(@A33,0) <> 0 or isnull(@A51,0) <> 0 or isnull(@A58,0) <> 0 or isnull(@A65,0) <> 0 or isnull(@A79,0) <> 0 or isnull(@A86,0) <> 0 or isnull(@A27,0) <> 0 or isnull(@A72,0) <> 0)
	$insql					insert into tblFUND_DOC_TYPE ([ID],[OwnerID],[CreationDateTime],[DocID],[RowID],[ISN_FUND],[ISN_DOC_TYPE])
	$insql					select newid(),@OwnerID,getdate(),ID,0,ISN_FUND,7 from tblFUND where ISN_FUND = @KOD
	$insql				update tblDOCUMENT_STATS set
	$insql					ISN_DOC_TYPE = 8,
	$insql					CARRIER_TYPE = 'A',
	$insql					UNIT_COUNT = @A24,
	$insql					UNIT_UNDESCRIBED = @A47,
	$insql					UNIT_INVENTORY = @A36,
	$insql					UNIT_OC_COUNT = @A54,
	$insql					UNIT_HAS_SF = @A61,
	$insql					UNIT_HAS_FP = @A68,
	$insql					UNITS_CATALOGUED = @A82,
	$insql					SECRET_UNITS = @A89,
	$insql					REG_UNIT = @A29,
	$insql					REG_UNITS_CTALOGUE = @A75
	$insql				where ISN_FUND = @KOD and ISN_INVENTORY is null and ISN_DOC_TYPE = 8 and CARRIER_TYPE = 'A'
	$insql				if (isnull(@A24,0) <> 0 or isnull(@A47,0) <> 0 or isnull(@A36,0) <> 0 or isnull(@A54,0) <> 0 or isnull(@A61,0) <> 0 or isnull(@A68,0) <> 0 or isnull(@A82,0) <> 0 or isnull(@A89,0) <> 0 or isnull(@A26,0) <> 0 or isnull(@A75,0) <> 0)
	$insql					insert into tblFUND_DOC_TYPE ([ID],[OwnerID],[CreationDateTime],[DocID],[RowID],[ISN_FUND],[ISN_DOC_TYPE])
	$insql					select newid(),@OwnerID,getdate(),ID,0,ISN_FUND,8 from tblFUND where ISN_FUND = @KOD
	$insql				update tblDOCUMENT_STATS set
	$insql					ISN_DOC_TYPE = 9,
	$insql					CARRIER_TYPE = 'M',
	$insql					UNIT_COUNT = @A26,
	$insql					UNIT_UNDESCRIBED = @A49,
	$insql					UNIT_INVENTORY = @A38,
	$insql					UNIT_OC_COUNT = @A56,
	$insql					UNIT_HAS_SF = @A63,
	$insql					UNIT_HAS_FP = @A70,
	$insql					UNITS_CATALOGUED = @A77,
	$insql					UNITS_NOT_FOUND = @A84,
	$insql					REG_UNIT = @A31
	$insql				where ISN_FUND = @KOD and ISN_INVENTORY is null and ISN_DOC_TYPE = 9 and CARRIER_TYPE = 'M'
	$insql				if (isnull(@A26,0) <> 0 or isnull(@A49,0) <> 0 or isnull(@A38,0) <> 0 or isnull(@A56,0) <> 0 or isnull(@A63,0) <> 0 or isnull(@A70,0) <> 0 or isnull(@A77,0) <> 0 or isnull(@A84,0) <> 0 or isnull(@A31,0) <> 0)
	$insql					insert into tblFUND_DOC_TYPE ([ID],[OwnerID],[CreationDateTime],[DocID],[RowID],[ISN_FUND],[ISN_DOC_TYPE])
	$insql					select newid(),@OwnerID,getdate(),ID,0,ISN_FUND,9 from tblFUND where ISN_FUND = @KOD
	$insql				select @Result = 1
	$insql			end
END
