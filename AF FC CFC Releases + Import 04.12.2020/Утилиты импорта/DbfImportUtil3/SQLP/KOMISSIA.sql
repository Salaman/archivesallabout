﻿-- Названия комиссий (рассекр.)

/* поля не используются: 
[NOTE]
[PROTECTED]
[WEIGHT] */

BEGIN [KOMISSIA].[tblDECL_COMMISSION_CL]
	#primary
	"link																		KOD
	$pk																			KOD					-- ISN_COMMISSION
	@KOD																		[ISN_COMMISSION]	-- PK
	@NAME																		[NAME_SHORT]		-- НАЗВАНИЕ СОКРАЩЕННОЕ
	@K1																			[NAME]				-- НАЗВАНИЕ ПОЛНОЕ
	@K2																			[CREATE_DATE]		-- НАЧАЛЬНАЯ ДАТА
	@K3																			[DELETE_DATE]		-- КОНЕЧНАЯ ДАТА
END
