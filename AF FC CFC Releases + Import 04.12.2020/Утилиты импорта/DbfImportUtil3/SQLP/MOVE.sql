﻿-- Движение

/* поля не используются:
[ISN_COMMISSION]
[ISN_ESTIMATE_REASON]
[ACT_NAME]
[ACT_PERSONS]
[ACT_WORK]
[PAGE_NUMBERS] */

BEGIN [MOVE].[tblACT_TYPE_CL]
	#primary
	"null																		I8	
	$set																		NewSmallID
	$pk																			NewSmallID				-- ISN_ACT_TYPE
	@NewSmallID																	[ISN_ACT_TYPE]			-- PK
	@I8																			[NAME]					-- АКТ
	
	$insql				if exists(select A.ISN_ACT_TYPE from tblACT_TYPE_CL A where A.NAME = @I8 and %A.ISN_ACT_TYPE%)
	$insql				select @Result = 1 else select @Result = 0
END

BEGIN [MOVE].[tblDEPOSIT]
	#primary
	"link																		NewID3,FOND,OPIS
	$pk																			NewID3					-- ISN_DEPOSIT
	@NewID3																		[ISN_DEPOSIT]			-- PK
	@FOND																		[ISN_FUND]				-- FK (FOND.KOD)
	'Россыпь'																	[DEPOSIT_NAME]			-- 
	@I3																			[UNIT_COUNT]			--
	@OPIS																		[]						--
	@I9																			[DOC_COUNT]				--
	0																			[PROCESSED_DOC]			--
	
	$insql				if exists(select ISN_FUND from tblFUND where ISN_FUND = @FOND)
	$insql				and (isnull(rtrim(@OPIS), '') = '' or @OPIS = '0' or not exists(select ISN_INVENTORY from tblINVENTORY where ISN_INVENTORY = @OPIS))
	$insql				select @Result = 0 else select @Result = 1
	
	$outsql				update tblDEPOSIT set DEPOSIT_NAME =
	$outsql				  ('Россыпь ' + cast((select count(D.ISN_DEPOSIT) from tblDEPOSIT D where D.ISN_FUND = @FOND) as varchar(10)))
	$outsql				where ISN_DEPOSIT = @NewID3
	
	$outsql				if (select ISN_DOC_TYPE from tblFUND where ISN_FUND = @FOND and Deleted = 0) = 3
	$outsql				update tblDEPOSIT set PERSONAL_DOC_COUNT = @I9, PROCESSED_PERSONAL_DOC = 0 where ISN_DEPOSIT = @NewID3
END

BEGIN [MOVE].[tblACT]
	#primary
	"link																		NewID,FOND,OPIS
	$pk																			NewID					-- ISN_ACT
	@NewID																		[ISN_ACT]				-- PK
	@FOND																		[ISN_FUND]				-- FK (FOND.KOD)
	702																			[ACT_OBJ]				-- FK (OPIS.KOD), N ОПИСИ
	@I1																			[]						-- ГОД
	case(@I2=1:0,2:2,3:1)														[MOVEMENT_FLAG]			-- ДВИЖЕНИЕ (ТИП ДВИЖЕНИЯ) ()
	@I3																			?[UNIT_COUNT]			-- ОБЪЕМ: ед.хр
	@I3																			[]						-- ОБЪЕМ: ед.хр
	@I4																			[]						-- ОБЪЕМ: ед.уч.
	@I5																			[DOC_DATES]				-- ДАТЫ ДОКУМЕНТОВ
	@I6																			[NOTE]					-- ПРИМЕЧАНИЕ
	0																			[ACT_NUM]				-- N и ДАТА АКТА
	null																		[ACT_DATE]				-- N и ДАТА АКТА
	@I7																			[]						--	
	@I8																			{}						-- АКТ
	@I9																			[]						-- ОБЪЕМ: док-ов.
	null																		[ISN_ACT_TYPE]			--
	@OPIS																		[]
	@ZZ																			[]
	@NN																			[]
	@DD																			[]
	
	$insql				if exists(select ISN_FUND from tblFUND where ISN_FUND = @FOND)
	$insql				select @ResultLog = 0 else select @ResultLog = 1
	
	$insql				if isnull(ltrim(@NN), '') = '' and isnull(ltrim(@DD), '') = ''
	$insql				begin
	$insql					select @ResultLog = 'PASS:Отсутствует номер и дата акта в разделе ДВИЖЕНИЕ ДОКУМЕНТОВ (Фонд № {FOND_NUM = @FOND}, Опись № {INVENTORY_NUM =? @OPIS}) -> экран 16 -> поле № и ДАТА АКТА: Акт импортирован с номером "без номера" и без даты'
	$insql				end
	$insql				if isnull(ltrim(@NN), '') <> '' and isnull(ltrim(@DD), '') = ''
	$insql				begin
	$insql					select @ResultLog = 'PASS:Отсутствует дата акта в разделе ДВИЖЕНИЕ ДОКУМЕНТОВ (Фонд № {FOND_NUM = @FOND}, Опись № {INVENTORY_NUM =? @OPIS}) -> экран 16 -> поле № и ДАТА АКТА: Акт ' + isnull(@NN, '') + ' импортирован без даты'
	$insql				end
	$insql				if isnull(ltrim(@DD), '') <> ''
	$insql				begin
	$insql					begin try select convert(datetime, @DD, 104) end try
	$insql					begin catch select @ResultLog = 'Ошибка в разделе ДВИЖЕНИЕ ДОКУМЕНТОВ (Фонд № {FOND_NUM = @FOND}, Опись № {INVENTORY_NUM =? @OPIS}) -> экран 16 -> поле № и ДАТА АКТА: недопустимое значение даты' end catch
	$insql				end
	
	$outsql				update tblACT set
	$outsql					ACT_NUM = (case
	$outsql						when isnull(ltrim(@ZZ), '0') = '0' then 'без номера'
	$outsql						else isnull(ltrim(@NN), '')
	$outsql					end),
	$outsql					ACT_DATE = (case
	$outsql						when isnull(ltrim(@DD), '') = '' then null
	$outsql						else convert(datetime, @DD, 104)
	$outsql					end)
	$outsql				where ISN_ACT = @NewID
	
	$outsql				update tblACT set UNIT_COUNT =
	$outsql				  (case when rtrim(@I3) = '' then 0 else @I3 end)
	$outsql				where ISN_ACT = @NewID
	
	$outsql				update tblACT set ISN_ACT_TYPE =
	$outsql				  (select top 1 A.ISN_ACT_TYPE from tblACT_TYPE_CL A where A.NAME = @I8 and %A.ISN_ACT_TYPE%)
	$outsql				where ISN_ACT = @NewID
	
	$outsql				if isnull(ltrim(@OPIS), '') = '' or @OPIS = '0' or not exists(select ISN_INVENTORY from tblINVENTORY where ISN_INVENTORY = @OPIS)
	$outsql				update tblACT set ACT_OBJ = 707
	$outsql				where ISN_ACT = @NewID
	
	$val				I7
	$reg				^(?<ZZ>(?<NN>^[^\d\,\.]*?\d+[^\s\,от]*)[\s\,от]*)?(?<DD>([0-9][0-9]|[1-9])\.([0-9][0-9]|[1-9])\.(\d{4}))?$
	$log				'Ошибка в разделе ДВИЖЕНИЕ ДОКУМЕНТОВ (Фонд № {FOND_NUM = @FOND}, Опись № {INVENTORY_NUM =? @OPIS}) -> экран 16 -> поле № и ДАТА АКТА: недопустимый формат номера и даты'
	$log				'Возможный формат: "10,01.02.2003" или "10, 01.02.2003" или "10 от 01.02.2003"'

	$val				I2
	$reg				^([123])$
	$log				'Ошибка в разделе ДВИЖЕНИЕ ДОКУМЕНТОВ (Фонд № {FOND_NUM = @FOND}, Опись № {INVENTORY_NUM =? @OPIS}) -> экран 16 -> поле ДВИЖЕНИЕ: недопустимое значение'
	$log				'Возможные значения: "поступление", "выбытие", "без движения"'
END

BEGIN [MOVE].[tblREF_ACT]
	#relative
	"link																		NewID2,NewID,NewID3,OPIS
	$pk																			NewID2					-- ISN_REF_ACT
	$fk																			NewID2					-- ISN_OBJ
	@NewID2																		[ISN_REF_ACT]			-- PK
	@NewID																		[ISN_ACT]				--
	707																			[KIND]					--
	@NewID3																		[ISN_OBJ]				--
	@OPIS																		[]						--
	
	$insql				if exists(select ISN_ACT from tblACT where ISN_ACT = @NewID)
	$insql				select @Result = 0 else select @Result = 1
	
	$outsql				if (isnull(ltrim(@OPIS), '') = '' or @OPIS = '0' or not exists(select ISN_INVENTORY from tblINVENTORY where ISN_INVENTORY = @OPIS))
	$outsql				update tblREF_ACT set DocID =
	$outsql				  (select top 1 D.ID from tblDEPOSIT D where D.ISN_DEPOSIT = @NewID3)
	$outsql				where ISN_REF_ACT = @NewID2
	$outsql				else
	$outsql				update tblREF_ACT set KIND = 702, ISN_OBJ = @OPIS, DocID =
	$outsql				  (select top 1 I.ID from tblINVENTORY I where I.ISN_INVENTORY = @OPIS)
	$outsql				where ISN_REF_ACT = @NewID2
END
