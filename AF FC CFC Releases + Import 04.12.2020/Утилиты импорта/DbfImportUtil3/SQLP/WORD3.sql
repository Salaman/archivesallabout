﻿-- Ключевые слова к вопросу

/* поля не используются: - */

BEGIN [WORD3].[tblCLS]
	#primary
	"link																		NewID
	$pk																			NewID					-- ISN_CLS
	@NewID																		[ISN_CLS]				-- PK
	@VOPROS																		[]
	@WORD																		[]
	
	$insql			update tblCLS set NOTE =
	$insql			  (NOTE + @WORD)
	$insql			where CODE = @VOPROS and %ISN_CLS%
	
	$insql			update tblCLS set CODE = null where CODE = @VOPROS and %ISN_CLS%
	
	$insql			select @Result = 1
END

BEGIN [WORD3].[tblCLS]
	#primary
	"link																		NewID
	$pk																			NewID					-- ISN_CLS
	@NewID																		[ISN_CLS]				-- PK
	@VOPROS																		[]
	@WORD																		[NAME]
	null																		[ISN_HIGH_CLS]
	null																		[CODE]
	null																		[WEIGHT]
	701																			[OBJ_KIND]
	null																		[NOTE]
	'Y'																			[MULTISELECT]
	'B'																			[FOREST_ELEM]
	'N'																			[PROTECTED]
	
	$insql			if not exists(select C.ISN_CLS from tblCLS C where C.NAME = @WORD and C.ISN_HIGH_CLS =
	$insql				(select top 1 CC.ISN_CLS from tblCLS CC where CC.NAME = 'Ключевые слова к вопросу' and %CC.ISN_CLS%) )
	$insql			select @Result = 0 else select @Result = 1
	
	$outsql			update tblCLS set ISN_HIGH_CLS =
	$outsql			  (select top 1 C.ISN_CLS from tblCLS C where C.NAME = 'Ключевые слова к вопросу' and %C.ISN_CLS%)
	$outsql			where tblCLS.ISN_CLS = @NewID
END

BEGIN [WORD3].[tblREF_CLS]
	#relative
	"link																		NewID,NewID2,VOPROS
	$fk																			VOPROS
	@NewID2																		[ISN_REF_CLS]
	@NewID																		[ISN_CLS]
	null																		[ISN_TREE]
	@VOPROS																		[ISN_OBJ]
	708																			[KIND]
	@WORD																		[]
	
	$insql			if exists(select ISN_QUESTION from tblQUESTION where ISN_QUESTION = @VOPROS) and exists(select ISN_CLS from tblCLS where ISN_CLS = @NewID)
	$insql			select @Result = 0 else select @Result = 1
	
	$outsql			update tblREF_CLS set ISN_TREE =
	$outsql			  (select C.ISN_HIGH_CLS from tblCLS C where C.ISN_CLS = @NewID)
	$outsql			where ISN_REF_CLS = @NewID2
END
