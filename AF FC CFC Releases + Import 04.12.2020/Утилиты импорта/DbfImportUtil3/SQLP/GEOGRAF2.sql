﻿-- География к делу

/* поля не используются: - */

BEGIN
	if not exists(select C.ISN_CLS from tblCLS C where C.NAME = 'Межфондовые классификаторы' and %C.ISN_CLS%)
	insert into tblCLS
	([ID],[OwnerID],[CreationDateTime],[StatusID],[Deleted],[ISN_CLS],[ISN_HIGH_CLS],[CODE],[WEIGHT],[NAME],[OBJ_KIND],[MULTISELECT],[NOTE],[FOREST_ELEM],[PROTECTED])
	values (@ConstGUID,@OwnerID,GETDATE(),@StatusID,0,@ConstID,null,null,null,'Межфондовые классификаторы',701,'Y',null,'F','N')

	if not exists(select C.ISN_CLS from tblCLS C where C.NAME = 'География единиц хранения' and %C.ISN_CLS%)
	begin
		insert into tblCLS
		([ID],[OwnerID],[CreationDateTime],[StatusID],[Deleted],[ISN_CLS],[ISN_HIGH_CLS],[CODE],[WEIGHT],[NAME],[OBJ_KIND],[MULTISELECT],[NOTE],[FOREST_ELEM],[PROTECTED])
		values (@ConstGUID3,@OwnerID,GETDATE(),@StatusID,0,@ConstID3,null,null,null,'География единиц хранения',701,'Y',null,'T','N')

		update tblCLS set ISN_HIGH_CLS =
		(select top 1 C.ISN_CLS from tblCLS C where C.NAME = 'Межфондовые классификаторы' and %C.ISN_CLS%)
		where ISN_CLS = @ConstID3 and [NAME] = 'География единиц хранения'
	end
END

BEGIN [GEOGRAF2].[tblCLS]
	#primary
	"link																		NewID
	$pk																			NewID					-- ISN_CLS
	@NewID																		[ISN_CLS]				-- PK
	@DELO																		[]
	@WORD																		[NAME]
	null																		[ISN_HIGH_CLS]
	null																		[CODE]
	null																		[WEIGHT]
	701		 														     		[OBJ_KIND]
	null																		[NOTE]
	'Y'																			[MULTISELECT]
	'B'																			[FOREST_ELEM]
	'N'																			[PROTECTED]
	
	$insql			if not exists(select C.ISN_CLS from tblCLS C where C.NAME = @WORD and C.ISN_HIGH_CLS =
	$insql				(select top 1 CC.ISN_CLS from tblCLS CC where CC.NAME = 'География единиц хранения' and %CC.ISN_CLS%) )
	$insql			select @Result = 0 else select @Result = 1
	
	$outsql			update tblCLS set ISN_HIGH_CLS =
	$outsql			  (select top 1 C.ISN_CLS from tblCLS C where C.NAME = 'География единиц хранения' and %C.ISN_CLS%)
	$outsql			where ISN_CLS = @NewID
END

BEGIN [GEOGRAF2].[tblREF_CLS]
	#relative
	"link																		NewID,NewID2,DELO
	$fk																			DELO
	@NewID2																		[ISN_REF_CLS]
	@NewID																		[ISN_CLS]
	null																		[ISN_TREE]
	@DELO																		[ISN_OBJ]
	703																			[KIND]
	@WORD																		[]
	
	$insql			if exists(select ISN_UNIT from tblUNIT where ISN_UNIT = @DELO) and exists(select ISN_CLS from tblCLS where ISN_CLS = @NewID)
	$insql			select @Result = 0 else select @Result = 1
	
	$outsql			update tblREF_CLS set ISN_TREE =
	$outsql			  (select C.ISN_HIGH_CLS from tblCLS C where C.ISN_CLS = @NewID)
	$outsql			where ISN_REF_CLS = @NewID2
	
	$outsql			update tblUNIT set ADDITIONAL_CLS =
	$outsql				(isnull(ADDITIONAL_CLS, '') + 'География: ' + @WORD + char(13) + char(10))
	$outsql			where ISN_UNIT = @DELO
END