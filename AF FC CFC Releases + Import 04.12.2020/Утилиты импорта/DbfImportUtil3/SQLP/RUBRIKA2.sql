﻿-- Рубрикатор (структура описи)

/* поля не используются:
[CODE] */

BEGIN
	if not exists(select I.ISN_INVENTORY_CLS from tblINVENTORY_STRUCTURE I where I.NAME = 'Структура описи' and %I.ISN_INVENTORY_CLS%)
	insert into tblINVENTORY_STRUCTURE
	([ID],[OwnerID],[CreationDateTime],[DocID],[RowID],[StatusID],[Deleted],[ISN_INVENTORY_CLS],[ISN_HIGH_INVENTORY_CLS],[ISN_INVENTORY],[CODE],[NAME],[NOTE],[FOREST_ELEM],[PROTECTED],[WEIGHT])
	values (@ConstGUID,@OwnerID,GETDATE(),@StatusID,0,@StatusID,0,@ConstID,null,null,null,'Структура описи',null,'F','Y',1)
END

BEGIN [OPIS].[tblINVENTORY_STRUCTURE]
	#primary
	"link																		NewID,KOD
	$pk																			NewID							-- ISN_INVENTORY_CLS
	$fk																			KOD								-- ISN_INVENTORY
	0																			[RowID]							-- RowID
	@ZeroID																		[DocID]							-- DocID
	@NewID																		[ISN_INVENTORY_CLS]				-- PK
	@KOD																		[ISN_INVENTORY]					-- FK
	null																		[ISN_HIGH_INVENTORY_CLS]		--
	null																		[CODE]							--
	null																		[NOTE]							-- Первая, вторая, третья цифра номера рубрики
	'Разделы'+char(32)+'описи'+char(32)+rtrim(ltrim(@OKOD))						[NAME]							-- Название рубрики.
	'T'																			[FOREST_ELEM]					--
	'N'																			[PROTECTED]						--
	
	$insql			if exists(select ISN_INVENTORY from tblINVENTORY where ISN_INVENTORY = @KOD)
	$insql			select @Result = 0 else select @Result = 1
	
	$outsql			update tblINVENTORY_STRUCTURE set ISN_HIGH_INVENTORY_CLS =
	$outsql			  (select top 1 I.ISN_INVENTORY_CLS from tblINVENTORY_STRUCTURE I where I.NAME = 'Структура описи' and %I.ISN_INVENTORY_CLS%)
	$outsql			where ISN_INVENTORY_CLS = @NewID
	
	$outsql			update tblINVENTORY_STRUCTURE set DocID =
	$outsql			  (select top 1 I.ID from tblINVENTORY I where I.ISN_INVENTORY = @KOD)
	$outsql			where ISN_INVENTORY_CLS = @NewID
END

BEGIN [FOND].[tblINVENTORY_STRUCTURE]
	#primary
	"link																		KOD
	$pk																			NewID3
	NewID3																		[]
	@KOD																		[]
	@FKOD																		[]

	$insql			update tblINVENTORY_STRUCTURE set [NAME] =
	$insql			  ([NAME] + ' фонда ' + @FKOD)
	$insql			where ISN_INVENTORY in
	$insql			(select I.ISN_INVENTORY from tblINVENTORY I where I.ISN_FUND = @KOD)
	
	$insql			select @Result = 1
END

BEGIN [RUBRIKA2].[tblINVENTORY_STRUCTURE]
	#primary
	"link																		NewID2,OPIS
	$pk																			NewID2							-- ISN_INVENTORY_CLS
	$fk																			OPIS							-- ISN_HIGH_INVENTORY_CLS
	0																			[RowID]							-- RowID
	@ZeroID																		[DocID]							-- DocID
	@NewID2																		[ISN_INVENTORY_CLS]				-- PK
	@OPIS																		[ISN_INVENTORY]					-- 
	null																		[ISN_HIGH_INVENTORY_CLS]		-- FK
	null																		[CODE]							--
	ltrim(str(@R1))+char(32)+ltrim(str(@R2))+char(32)+ltrim(str(@R3))			[NOTE]							-- Первая, вторая, третья цифра номера рубрики
	@RUBRIKA																	[NAME]							-- Название рубрики.	
	'B'																			[FOREST_ELEM]					--
	'N'																			[PROTECTED]						--
	
	$insql			if exists(select ISN_INVENTORY from tblINVENTORY where ISN_INVENTORY = @OPIS)
	$insql			select @Result = 0 else select @Result = 1
	
	$outsql			update tblINVENTORY_STRUCTURE set ISN_HIGH_INVENTORY_CLS =
	$outsql			  (select top 1 I.ISN_INVENTORY_CLS from tblINVENTORY_STRUCTURE I where I.ISN_INVENTORY = @OPIS)
	$outsql			where ISN_INVENTORY_CLS = @NewID2
	
	$outsql			update tblINVENTORY_STRUCTURE set DocID =
	$outsql			  (select I.ID from tblINVENTORY I where I.ISN_INVENTORY = @OPIS)
	$outsql			where ISN_INVENTORY_CLS = @NewID2
END
