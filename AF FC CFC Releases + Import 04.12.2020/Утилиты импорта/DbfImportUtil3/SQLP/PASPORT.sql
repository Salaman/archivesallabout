﻿-- Паспорт

/* поля не используются:
[GUIDE_LS]
[INVENTORY_COUNT]
[CATALOUGUE_COUNT]
[INDEX_COUNT]
[REVIEW_COUNT]
[TOTAL_SPACE]
[STORAGE_SPACE]
[SPACE_WITHOUT_SECUR]
[SPACE_WITHOU_SECURITY_PROC]
[SPACE_WITHOUT_ALARM]
[SPACE_WITHOUT_ALARM_PROC]
[FREE_SHELF_LENGTH]
[LOAD_LEVEL] */

BEGIN [PASPORT].[tblARCHIVE_PASSPORT]
	#primary
	"link																		NewID,ArchiveID
	$pk																			NewID					-- ISN_PASSPORT
	$fk																			ArchiveID				-- ISN_ARCHIVE
	0																			[RowID]					-- RowID
	'106FD655-C47B-4ec8-A683-14F2E3F160DD'										[DocID]					-- DocID
	@NewID																		[ISN_PASSPORT]			-- PK
	@ArchiveID																	[ISN_ARCHIVE]			-- FK
	@GOD																		[PASS_YEAR]
	@XA1																		[]
	@XA2																		[]
	@XA3																		[]
	@XA4																		[]
	@X1																			[]
	@X2																			[]
	@X3																			[]
	@X4																			[]
	@X5																			[]
	@X6																			[]
	@X7																			[]
	@X8																			[]
	@X9																			[]
	@X10																		[]
	@X11																		[]
	@X12																		[]
	@X13																		[]
	@X14																		[]
	@X15																		[]
	@X16																		[]
	@X17																		[]
	@X18																		[]
	@X19																		[]
	@X20																		[]
	@X21																		[]
	@X22																		[]
	@X23																		[]
	@X24																		[]
	@X25																		[]
	@X26																		[]
	@X27																		[]
	@X28																		[]
	@X29																		[]
	@X30																		[]
	@X31																		[]
	@X32																		[]
	@X33 																		[]
	@X34																		[]
	@X35 																		[]
	@X36																		[]
	@X37																		[]
	@X38																		[]
	@X39																		[]
	@X40																		[]
	@X41																		[]
	@X42																		[]
	@X43																		[]
	@X44																		[]
	@X45																		[]
	@X46																		[]
	@X47																		[]
	@X48																		[]
	@X49																		[]
	@X50																		[]
	@X51																		[]
	@X52																		[]
	@X53																		[]
	@X54																		[]
	@X55																		[]
	@X56																		[]
	@X57																		[]
	@X58																		[]
	@X59																		[]
	@X60																		[]
	@X61																		[]
	@X62																		[]
	@X63																		[]
	@X64																		[]
	@X65																		[]
	@X66																		[]
	@X67																		[]
	@X68																		[]
	@X69																		[]
	@X70																		[]
	@X71																		[]
	@X72																		[]
	@X73																		[]
	@X74																		[]
	@X75																		[]
	@X76																		[]
	@X77																		[]
	@X78																		[MICROFORM_FRAME_COUNT]
	@X79																		[LP_DOC_COUNT]
	@X80																		[]
	@X81																		[]
	@X82																		[]
	@X83																		[]
	@X84																		[]
	@X85																		[]
	@X86																		[]
	@X87																		[]
	@X88																		[]
	@X89																		[]
	@X90																		[]
	@X91																		[]
	@X92																		[]
	@X93																		[]
	@X94																		[]
	@X95																		[]
	@X96																		[]
	@X97																		[]
	@X98																		[]
	@X99																		[]
	@X100																		[]
	@X101																		[]
	@X102																		[]
	@X103																		[]
	@X104																		[]
	@X105																		[]
	@X106																		[]
	@X107																		[]
	@X108																		[]
	@X109																		[]
	@X110																		[]
	@X111																		[]
	@X112																		[]
	@X113																		[]
	@X114																		[]
	@X115																		[]
	@X116																		[]
	@X117																		[]
	@X118																		[]
	@X119																		[]
	@X120																		[]
	@X121																		[]
	@X122																		[]
	@X123																		[]
	@X124																		[]
	@X125																		[]
	@X126																		[]
	@X127																		[]
	@X128																		[]
	@X129																		[]
	@X130																		[]
	@X131																		[]
	@X132 																		[]
	@X133																		[]
	@X134																		[]
	@X135																		[]
	@X136																		[]
	@X137																		[]
	@X138																		[]
	@X139																		[]
	@X140																		[]
	@X141																		[]
	@X142																		[]
	@X143																		[]
	@X144																		[]
	@X145																		[]
	@X146																		[]
	@X147																		[]
	@X148																		[]
	@X149																		[]
	@X150																		[]
	@X151																		[]
	@X152																		[]
	@X153																		[]
	@X154																		[]
	@X155																		[]
	@X156																		[]
	@X157																		[]
	@X158																		[]
	@X159																		[]
	@X160																		[]
	@X161																		[]
	@X162																		[]
	@X163																		[]
	@X164																		[]
	@X165																		[]
	@X166																		[]
	@X167																		[]
	@X168																		[]
	@X169																		[]
	@X170																		[]
	@X171																		[]
	@X172																		[]
	@X173																		[]
	@X174																		[]
	@X175																		[]
	@X176																		[]
	@X177																		[]
	@X178																		[]
	@X179																		[]
	@X180																		[]
	@X181																		[]
	@X182																		[]
	@X183																		[]
	@X184																		[]
	@X185																		[]
	@X186																		[]
	@X187																		[]
	@X188																		[]
	@X189																		[]
	@X190																		[]
	@X191																		[]
	@X192																		[]
	@X193																		[]
	@X194																		[]
	@X195																		[]
	@X196																		[]
	@X197																		[]
	@X198																		[]
	@X199																		[]
	@X200																		[]
	@X201																		[]
	@X202																		[]
	@X203																		[]
	@X204																		[]
	@X205																		[]
	@X206																		[]
	@X207																		[]
	@X208																		[]
	@X209																		[]
	@X210																		[]
	@X211																		[]
	@X212																		[]
	@X213																		[]
	@X214																		[]
	@X215																		[]
	@X216																		[]
	@X217																		[ALL_SII_COUNT]
	@X218																		[GUIDE_COUNT]
	@X219																		[]
	@X220																		[ATD_COUNT]
	@X221																		[HISTORY_COUNT]
	@X222																		[BOOK_COUNT]
	@X223																		[NEWSPAPER_COUNT]
	@X224																		[MAGAZINE_COUNT]
	@X225																		[OTHER_PRINT_COUNT]
	@X226																		[ARH_BUILDING_COUNT]
	@X227																		[SPEC_BUILDING_COUNT]
	@X228																		[PRISP_BUILDING_COUNT]
	@X229																		[SHELF_LENGTH]
	@X230																		[METAL_SHELF_LENGTH]
	@X231																		[]
	@X232																		[]
	@X233																		[]
	@X234																		[]
	@X235																		[]
	@X236																		[UNITS_CARDBOARDED]
	0																			[TOTAL_SPACE]
	
	$outsql				update tblARCHIVE_PASSPORT set DocID =
	$outsql				  (select top 1 A.ID from tblARCHIVE A where A.ISN_ARCHIVE = @ArchiveID)
	$outsql				where ISN_PASSPORT = @NewID
END

----------------------------------------------------------------------------------
-- ARCHIVE_STATS
----------------------------------------------------------------------------------

BEGIN [PASPORT].[tblARCHIVE_STATS]
	#relative
	"link																		StepID,NewID
	$pk																			StepID					-- ISN_ARCHIVE_STATS
	$fk																			NewID					-- ISN_PASSPORT
	@StepID																		[ISN_ARCHIVE_STATS]		-- PK
	@NewID																		[ISN_PASSPORT]			-- FK
	1																			[ISN_DOC_TYPE]			--
	'P'																			[CARRIER_TYPE]			--
	@X7																			[FUND_COUNT]
	@X8																			[UNIT_COUNT]
	@X9																			[UNIT_INVENTORY]
	@X10																		[UNIT_SECRET]
	@X11																		[UNIT_OC]
	@X12																		[UNIT_DEP]
	@X84																		[UNIT_HAS_SF]
	@X85																		[UNIT_HAS_FP]
	@X86																		[NEGATIVE_COUNT]
	@X87																		[SF_COUNT]
	@X128																		[INVENTORY_COUNT]
	@X129																		[INVENTORY_COUNT_FULL]
	@X131																		[UNIT_CATALOGUED]
	@X133																		[PAPER_CARD_COUNT]
	@X134																		[DB_COUNT]
	@X135																		[DB_VOLUME]
END

BEGIN [PASPORT].[tblARCHIVE_STATS]
	#relative
	"link																		StepID,NewID
	$pk																			StepID					-- ISN_ARCHIVE_STATS
	$fk																			NewID					-- ISN_PASSPORT
	@StepID																		[ISN_ARCHIVE_STATS]		-- PK
	@NewID																		[ISN_PASSPORT]			-- FK
	2																			[ISN_DOC_TYPE]			--
	'P'																			[CARRIER_TYPE]			--
	@X24																		[FUND_COUNT]
	@X25																		[UNIT_COUNT]
	@X26																		[UNIT_INVENTORY]
	@X27																		[UNIT_SECRET]
	@X28																		[UNIT_DEP]
	@X96																		[UNIT_HAS_FP]
	@X152																		[INVENTORY_COUNT]
	@X153																		[INVENTORY_COUNT_FULL]
	@X155																		[UNIT_CATALOGUED]
	@X157																		[PAPER_CARD_COUNT]
	@X158																		[DB_COUNT]
	@X159																		[DB_VOLUME]
END

BEGIN [PASPORT].[tblARCHIVE_STATS]
	#relative
	"link																		StepID,NewID
	$pk																			StepID					-- ISN_ARCHIVE_STATS
	$fk																			NewID					-- ISN_PASSPORT
	@StepID																		[ISN_ARCHIVE_STATS]		-- PK
	@NewID																		[ISN_PASSPORT]			-- FK
	3																			[ISN_DOC_TYPE]			--
	'P'																			[CARRIER_TYPE]			--
	@X13																		[FUND_COUNT]
	@X14																		[UNIT_COUNT]
	@X15																		[UNIT_INVENTORY]
	@X16																		[UNIT_OC]
	@X17																		[UNIT_DEP]
	@X88																		[UNIT_HAS_SF]
	@X89																		[UNIT_HAS_FP]
	@X90																		[NEGATIVE_COUNT]
	@X91																		[SF_COUNT]
	@X136																		[INVENTORY_COUNT]
	@X137																		[INVENTORY_COUNT_FULL]
	@X139																		[UNIT_CATALOGUED]
	@X141																		[PAPER_CARD_COUNT]
	@X142																		[DB_COUNT]
	@X143																		[DB_VOLUME]
END

BEGIN [PASPORT].[tblARCHIVE_STATS]
	#relative
	"link																		StepID,NewID
	$pk																			StepID					-- ISN_ARCHIVE_STATS
	$fk																			NewID					-- ISN_PASSPORT
	@StepID																		[ISN_ARCHIVE_STATS]		-- PK
	@NewID																		[ISN_PASSPORT]			-- FK
	4																			[ISN_DOC_TYPE]			--
	'P'																			[CARRIER_TYPE]			--
	@X18																		[FUND_COUNT]
	@X19																		[UNIT_COUNT]
	@X20																		[UNIT_INVENTORY]
	@X21																		[UNIT_SECRET]
	@X22																		[UNIT_OC]
	@X23																		[UNIT_DEP]
	@X92																		[UNIT_HAS_SF]
	@X93																		[UNIT_HAS_FP]
	@X94																		[NEGATIVE_COUNT]
	@X95																		[SF_COUNT]
	@X144																		[INVENTORY_COUNT]
	@X145																		[INVENTORY_COUNT_FULL]
	@X147																		[UNIT_CATALOGUED]
	@X149																		[PAPER_CARD_COUNT]
	@X150																		[DB_COUNT]
	@X151																		[DB_VOLUME]
END

BEGIN [PASPORT].[tblARCHIVE_STATS]
	#relative
	"link																		StepID,NewID
	$pk																			StepID					-- ISN_ARCHIVE_STATS
	$fk																			NewID					-- ISN_PASSPORT
	@StepID																		[ISN_ARCHIVE_STATS]		-- PK
	@NewID																		[ISN_PASSPORT]			-- FK
	5																			[ISN_DOC_TYPE]			--
	'A'																			[CARRIER_TYPE]			--
	@X35																		[FUND_COUNT]
	@X36																		[UNIT_COUNT]
	@X37																		[UNIT_INVENTORY]
	@X38																		[UNIT_SECRET]
	@X39																		[UNIT_OC]
	@X40																		[UNIT_DEP]
	@X100																		[UNIT_HAS_SF]
	@X101																		[UNIT_HAS_FP]
	@X102																		[SF_COUNT]
	@X168																		[INVENTORY_COUNT]
	@X169																		[INVENTORY_COUNT_FULL]
	@X171																		[UNIT_CATALOGUED]
	@X173																		[PAPER_CARD_COUNT]
	@X174																		[DB_COUNT]
	@X175																		[DB_VOLUME]

END

BEGIN [PASPORT].[tblARCHIVE_STATS]
	#relative
	"link																		StepID,NewID
	$pk																			StepID					-- ISN_ARCHIVE_STATS
	$fk																			NewID					-- ISN_PASSPORT
	@StepID																		[ISN_ARCHIVE_STATS]		-- PK
	@NewID																		[ISN_PASSPORT]			-- FK
	6																			[ISN_DOC_TYPE]			--
	'A'																			[CARRIER_TYPE]			--
	@X41																		[FUND_COUNT]
	@X42																		[UNIT_COUNT]
	@X43																		[UNIT_INVENTORY]
	@X44																		[UNIT_SECRET]
	@X45																		[UNIT_OC]
	@X46																		[UNIT_DEP]
	@X72																		[REG_UNIT_COUNT]
	@X73																		[REG_UNIT_INVENTORY]
	@X103																		[UNIT_HAS_SF]
	@X104																		[UNIT_HAS_FP]
	@X105																		[SF_COUNT]
	@X176																		[INVENTORY_COUNT]
	@X177																		[INVENTORY_COUNT_FULL]
	@X179																		[UNIT_CATALOGUED]
	@X181																		[PAPER_CARD_COUNT]
	@X182																		[DB_COUNT]
	@X183																		[DB_VOLUME]
END

BEGIN [PASPORT].[tblARCHIVE_STATS]
	#relative
	"link																		StepID,NewID
	$pk																			StepID					-- ISN_ARCHIVE_STATS
	$fk																			NewID					-- ISN_PASSPORT
	@StepID																		[ISN_ARCHIVE_STATS]		-- PK
	@NewID																		[ISN_PASSPORT]			-- FK
	7																			[ISN_DOC_TYPE]			--
	'A'																			[CARRIER_TYPE]			--
	@X29																		[FUND_COUNT]
	@X30																		[UNIT_COUNT]
	@X31																		[UNIT_INVENTORY]	
	@X32																		[UNIT_SECRET]
	@X33																		[UNIT_OC]
	@X34																		[UNIT_DEP]
	@X70																		[REG_UNIT_COUNT]
	@X71																		[REG_UNIT_INVENTORY]
	@X97																		[UNIT_HAS_SF]
	@X98																		[UNIT_HAS_FP]
	@X99																		[SF_COUNT]
	@X160																		[INVENTORY_COUNT]
	@X161																		[INVENTORY_COUNT_FULL]
	@X163																		[UNIT_CATALOGUED]
	@X165																		[PAPER_CARD_COUNT]
	@X166																		[DB_COUNT]
	@X167																		[DB_VOLUME]
END

BEGIN [PASPORT].[tblARCHIVE_STATS]
	#relative
	"link																		StepID,NewID
	$pk																			StepID					-- ISN_ARCHIVE_STATS
	$fk																			NewID					-- ISN_PASSPORT
	@StepID																		[ISN_ARCHIVE_STATS]		-- PK
	@NewID																		[ISN_PASSPORT]			-- FK
	8																			[ISN_DOC_TYPE]			--
	'A'																			[CARRIER_TYPE]			--
	@X47																		[FUND_COUNT]
	@X48																		[UNIT_COUNT]
	@X49																		[UNIT_INVENTORY]
	@X50																		[UNIT_SECRET]
	@X51																		[UNIT_OC]
	@X52																		[UNIT_DEP]
	@X74																		[REG_UNIT_COUNT]
	@X75																		[REG_UNIT_INVENTORY]
	@X106																		[UNIT_HAS_SF]
	@X107																		[UNIT_HAS_FP]
	@X108																		[SF_COUNT]
	@X184																		[INVENTORY_COUNT]
	@X185																		[INVENTORY_COUNT_FULL]
	@X187																		[UNIT_CATALOGUED]
	@X189																		[PAPER_CARD_COUNT]
	@X190																		[DB_COUNT]
	@X191																		[DB_VOLUME]
END

BEGIN [PASPORT].[tblARCHIVE_STATS]
	#relative
	"link																		StepID,NewID
	$pk																			StepID					-- ISN_ARCHIVE_STATS
	$fk																			NewID					-- ISN_PASSPORT
	@StepID																		[ISN_ARCHIVE_STATS]		-- PK
	@NewID																		[ISN_PASSPORT]			-- FK
	9																			[ISN_DOC_TYPE]			--
	'M'																			[CARRIER_TYPE]			--
	@X59																		[FUND_COUNT]
	@X60																		[UNIT_COUNT]
	@X61																		[UNIT_INVENTORY]
	@X62																		[UNIT_OC]
	@X63																		[UNIT_DEP]
	@X112																		[UNIT_HAS_SF]
	@X113																		[UNIT_HAS_FP]
	@X114																		[NEGATIVE_COUNT]
	@X115																		[SF_COUNT]
	@X200																		[INVENTORY_COUNT]
	@X201																		[INVENTORY_COUNT_FULL]
	@X203																		[UNIT_CATALOGUED]
	@X205																		[PAPER_CARD_COUNT]
	@X206																		[DB_COUNT]
	@X207																		[DB_VOLUME]
END

BEGIN [PASPORT].[tblARCHIVE_STATS]
	#relative
	"link																		StepID,NewID
	$pk																			StepID					-- ISN_ARCHIVE_STATS
	$fk																			NewID					-- ISN_PASSPORT
	@StepID																		[ISN_ARCHIVE_STATS]		-- PK
	@NewID																		[ISN_PASSPORT]			-- FK
	null																		[ISN_DOC_TYPE]			--
	'A'																			[CARRIER_TYPE]			--
END

BEGIN [PASPORT].[tblARCHIVE_STATS]
	#relative
	"link																		StepID,NewID
	$pk																			StepID					-- ISN_ARCHIVE_STATS
	$fk																			NewID					-- ISN_PASSPORT
	@StepID																		[ISN_ARCHIVE_STATS]		-- PK
	@NewID																		[ISN_PASSPORT]			-- FK
	null																		[ISN_DOC_TYPE]			--
	'P'																			[CARRIER_TYPE]			--
	@X1																			[FUND_COUNT]
	@X2																			[UNIT_COUNT]
	@X3																			[UNIT_INVENTORY]
	@X4																			[UNIT_SECRET]
	@X5																			[UNIT_OC]
	@X6																			[UNIT_DEP]
	@X80																		[UNIT_HAS_SF]
	@X81																		[UNIT_HAS_FP]
	@X82																		[NEGATIVE_COUNT]
	@X83																		[SF_COUNT]
	@X120																		[INVENTORY_COUNT]
	@X121																		[INVENTORY_COUNT_FULL]
	@X123																		[UNIT_CATALOGUED]
	@X125																		[PAPER_CARD_COUNT]
	@X126																		[DB_COUNT]
	@X127																		[DB_VOLUME]
END

BEGIN [PASPORT].[tblARCHIVE_STATS]
	#relative
	"link																		StepID,NewID
	$pk																			StepID					-- ISN_ARCHIVE_STATS
	$fk																			NewID					-- ISN_PASSPORT
	@StepID																		[ISN_ARCHIVE_STATS]		-- PK
	@NewID																		[ISN_PASSPORT]			-- FK
	null																		[ISN_DOC_TYPE]			--
	'E'																			[CARRIER_TYPE]			--
	@X53																		[FUND_COUNT]
	@X54																		[UNIT_COUNT]
	@X55																		[UNIT_INVENTORY]
	@X56																		[UNIT_SECRET]
	@X57																		[UNIT_OC]
	@X58																		[UNIT_DEP]
	@X76																		[REG_UNIT_COUNT]
	@X77																		[REG_UNIT_INVENTORY]
	@X109																		[UNIT_HAS_SF]
	@X110																		[UNIT_HAS_FP]
	@X111																		[SF_COUNT]
	@X192																		[INVENTORY_COUNT]
	@X193																		[INVENTORY_COUNT_FULL]
	@X195																		[UNIT_CATALOGUED]
	@X197																		[PAPER_CARD_COUNT]
	@X198																		[DB_COUNT]
	@X199																		[DB_VOLUME]
END

BEGIN [PASPORT].[tblARCHIVE_STATS]
	#relative
	"link																		StepID,NewID
	$pk																			StepID					-- ISN_ARCHIVE_STATS
	$fk																			NewID					-- ISN_PASSPORT
	@StepID																		[ISN_ARCHIVE_STATS]		-- PK
	@NewID																		[ISN_PASSPORT]			-- FK
	null																		[ISN_DOC_TYPE]			--
	null																		[CARRIER_TYPE]			--
	@X64																		[FUND_COUNT]
	@X65																		[UNIT_COUNT]
	@X66																		[UNIT_INVENTORY]
	@X67																		[UNIT_SECRET]
	@X68																		[UNIT_OC]
	@X69																		[UNIT_DEP]
	@X116																		[UNIT_HAS_SF]
	@X117																		[UNIT_HAS_FP]
	@X118																		[NEGATIVE_COUNT]
	@X119																		[SF_COUNT]
	@X208																		[INVENTORY_COUNT]
	@X209																		[INVENTORY_COUNT_FULL]
	@X211																		[UNIT_CATALOGUED]
	@X213																		[PAPER_CARD_COUNT]
	@X214																		[DB_COUNT]
	@X215																		[DB_VOLUME]
	@X216																		[REG_UNIT_CATALOGUED]
END

BEGIN [PASPORT].[tblARCHIVE_STATS]
	#relative
	"link																		StepID,NewID
	$pk																			StepID					-- ISN_ARCHIVE_STATS
	$fk																			NewID					-- ISN_PASSPORT
	@StepID																		[ISN_ARCHIVE_STATS]		-- PK
	@NewID																		[ISN_PASSPORT]			-- FK
	4																			[ISN_DOC_TYPE]			--
	'E'																			[CARRIER_TYPE]			--
END

BEGIN [PASPORT].[tblARCHIVE_STATS]
	#relative
	"link																		StepID,NewID
	$pk																			StepID					-- ISN_ARCHIVE_STATS
	$fk																			NewID					-- ISN_PASSPORT
	@StepID																		[ISN_ARCHIVE_STATS]		-- PK
	@NewID																		[ISN_PASSPORT]			-- FK
	5																			[ISN_DOC_TYPE]			--
	'E'																			[CARRIER_TYPE]			--
END

BEGIN [PASPORT].[tblARCHIVE_STATS]
	#relative
	"link																		StepID,NewID
	$pk																			StepID					-- ISN_ARCHIVE_STATS
	$fk																			NewID					-- ISN_PASSPORT
	@StepID																		[ISN_ARCHIVE_STATS]		-- PK
	@NewID																		[ISN_PASSPORT]			-- FK
	6																			[ISN_DOC_TYPE]			--
	'E'																			[CARRIER_TYPE]			--
END

BEGIN [PASPORT].[tblARCHIVE_STATS]
	#relative
	"link																		StepID,NewID
	$pk																			StepID					-- ISN_ARCHIVE_STATS
	$fk																			NewID					-- ISN_PASSPORT
	@StepID																		[ISN_ARCHIVE_STATS]		-- PK
	@NewID																		[ISN_PASSPORT]			-- FK
	7																			[ISN_DOC_TYPE]			--
	'E'																			[CARRIER_TYPE]			--
END

BEGIN [PASPORT].[tblARCHIVE_STATS]
	#relative
	"link																		StepID,NewID
	$pk																			StepID					-- ISN_ARCHIVE_STATS
	$fk																			NewID					-- ISN_PASSPORT
	@StepID																		[ISN_ARCHIVE_STATS]		-- PK
	@NewID																		[ISN_PASSPORT]			-- FK
	8																			[ISN_DOC_TYPE]			--
	'E'																			[CARRIER_TYPE]			--
END
