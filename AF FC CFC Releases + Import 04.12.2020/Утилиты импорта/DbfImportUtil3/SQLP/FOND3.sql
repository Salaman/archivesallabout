﻿-- Переименования

/* поля не используются: - */

BEGIN [FOND3].[tblFUND_RENAME]
	#relative
	"link																		FOND,NewID
	$fk																			FOND						-- ISN_FUND
	@FOND																		[ISN_FUND]					-- FK(FOND.KOD)
	@NewID																		[ISN_FUND_RENAME]			-- PK
	@C1																			[FUND_NAME_FULL]			-- НАЗВАНИЕ ФОНДА
	@C2																			[FUND_NAME_SHORT]			-- СОКРАЩЕННОЕ НАЗВАНИЕ
	format(@C3=yyyyMMdd)														[CREATE_DATE]				-- НАЧАЛЬНАЯ ДАТА
	case(@C4='*':'Y','':'N')													[CREATE_DATE_INEXACT]		-- НАЧАЛЬНАЯ ДАТА (*)
	format(@C5=yyyyMMdd)														[DELETE_DATE]				-- КОНЕЧНАЯ ДАТА
	case(@C6='*':'Y','':'N')													[DELETE_DATE_INEXACT]		-- КОНЕЧНАЯ ДАТА (*)

	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @FOND)
	$insql			select @Result = 0 else select @Result = 1
		
	$outsql			update tblFUND set tblFUND.WAS_RENAMED = 'Y'
	$outsql			where ISN_FUND = @FOND
END

BEGIN [FOND3].[tblFUND_RENAME]
	#relative
	"link																		FOND
	@FOND																		[]

	$insql			update tblFUND_RENAME set tblFUND_RENAME.FUND_NUM_1 = 
	$insql			  (select top 1 F.FUND_NUM_1 from tblFUND F where F.ISN_FUND = @FOND)
	$insql			where ISN_FUND = @FOND
	
	$insql			update tblFUND_RENAME set tblFUND_RENAME.FUND_NUM_2 = 
	$insql			  (select top 1 F.FUND_NUM_2 from tblFUND F where F.ISN_FUND = @FOND)
	$insql			where ISN_FUND = @FOND
	
	$insql			update tblFUND_RENAME set tblFUND_RENAME.FUND_NUM_2 = 
	$insql			  (select top 1 F.FUND_NUM_2 from tblFUND F where F.ISN_FUND = @FOND)
	$insql			where ISN_FUND = @FOND
	
	$insql			select @Result = 1
	
END
