﻿-- Незадокументированные периоды

/* поля не используются: - */

BEGIN [FOND6].[tblUNDOCUMENTED_PERIOD]
	#relative
	"link																		NewID,FOND
	$pk																			NewID					-- ISN_PERIOD
	$fk																			FOND					-- ISN_FUND
	@NewID																		[ISN_PERIOD]			-- PK
	@FOND																		[ISN_FUND]				-- FK
	@F1																			[PERIOD_START_YEAR]		-- НАЧАЛЬНАЯ ДАТА
	@F2																			[PERIOD_END_YEAR]		-- КОНЕЧНАЯ ДАТА
	@F3																			[ISN_ABSENCE_REASON]	-- ПРИЧИНА ОТСУТСТВИЯ (ПРИЧИНА ОТСУТСТВИЯ ДОКУМЕНТОВ В ФОНДЕ) ()
	@F4																			[INFO_PLACE]			-- МЕСТО НАХОЖДЕНИЯ
	
	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @FOND)
	$insql			select @Result = 0 else select @Result = 1
END
