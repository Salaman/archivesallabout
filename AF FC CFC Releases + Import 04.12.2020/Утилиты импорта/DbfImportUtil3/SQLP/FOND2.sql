﻿-- Библиография

/* поля не использутся: -*/

BEGIN [FOND2].[tblFUND_PUBLICATION]
	#relative
	"link																		FOND,BIBLIO,NewID
	$pk																			NewID						-- 
	$fk																			FOND						-- ISN_FUND
	@BIBLIO																		[ISN_PUBLICATION]			-- FK
	@FOND																		[ISN_FUND]					-- FK
	@NewID																		[]
	
	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @FOND)
	$insql			select @Result = 0 else select @Result = 1		
END
