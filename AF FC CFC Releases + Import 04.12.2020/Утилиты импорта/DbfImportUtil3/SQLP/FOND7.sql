﻿-- Состав ОАФ

/* поля не используются:
[ISN_CHILD_FUND] */

BEGIN [FOND7].[tblFUND_OAF]
	#relative
	"link																		NewID,FOND
	$pk																			NewID					-- ISN_OAF
	$fk																			FOND					-- ISN_FUND
	@NewID																		[ISN_OAF]				-- PK
	@FOND																		[ISN_FUND]				-- FK
	@R1																			[FUND_NUM_1]			-- ПРЕЖНИЙ НОМЕР ФОНДА
	@R2																			[FUND_NUM_2]			-- ПРЕЖНИЙ НОМЕР ФОНДА
	@R3																			[FUND_NUM_3]			-- ПРЕЖНИЙ НОМЕР ФОНДА
	@Q2																			[FUND_NAME_FULL]		-- НАЗВАНИЕ ФОНДА
	@Q3																			[FUND_NAME_SHORT]		-- СОКРАЩЕННОЕ НАЗВАНИЕ ФОНДА
	
	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @FOND)
	$insql			select @Result = 0 else select @Result = 1
	
	$val			Q1
	$reg			^((?<R1>[а-я]{0,1}?)([-]*)(?<R2>\d{0,5}?)([-]*)(?<R3>[а-я]{0,1}?))$
	$log			'Ошибка в разделе СОСТАВ ОАФ (Фонд № {FOND_NUM = @FOND}) -> поле ПРЕЖНИЙ НОМЕР ФОНДА: недопустимый формат'
END
