﻿-- Характеристика НТД

/* поля не используются:
[COMPLEX_NUM_2]
[DEV_STAGE]
[ORGANIZ]
[REQUEST_DATE]
[REQUEST_NUM]
[PATENT_NUM]
[COPYRIGHT_NUM] */

BEGIN [DELO4].[tblUNIT_NTD]
	#relative
	"link																																					DELO
	$fk																																						DELO					-- ISN_UNIT
	@DELO																																					[ISN_UNIT]				-- FK (DELO.KOD)
	substring(@N1,1,2)																																		[COMPLEX_NUM_1]			-- НОМЕР КОМПЛЕКСА
	substring(@N1,3,1)																																		[COMPLEX_NUM_2]			-- НОМЕР КОМПЛЕКСА
	@N2																																						[AUTHOR]				-- АВТОР
	case(@N3='НД':16436,'КД':16437,'ТД':16438,'ПД':16439,'атентная':16440,'изобретательство':16441,'рационализация':16442,'картографическая':16443)			[ISN_NTD_KIND]			-- ВИД ДОКУМЕНТАЦИИ ()

	$insql				if exists(select ISN_UNIT from tblUNIT where ISN_UNIT = @DELO)
	$insql				and (select ISN_DOC_TYPE from tblUNIT where ISN_UNIT = @DELO) = 3
	$insql				select @Result = 0 else select @Result = 1
END
