﻿-- Топография

/* поля не используются: - */

BEGIN
	if not exists(select L.ISN_LOCATION from tblLOCATION L where L.NAME = 'Архивохранилище' and %L.ISN_LOCATION%)
	insert into tblLOCATION
	([ID],[OwnerID],[CreationDateTime],[StatusID],[Deleted],[ISN_LOCATION],[ISN_HIGH_LOCATION],[ISN_ARCHIVE],[CODE],[NAME],[NOTE],[FOREST_ELEM],[PROTECTED],[WEIGHT])
	values (@ConstGUID,@OwnerID,GETDATE(),@StatusID,0,@ConstID,null,@SharedArchiveID,null,'Архивохранилище',null,'T','Y',1)
END

BEGIN [OPIS2].[tblLOCATION]
	#primary
	"link																		NewSmallID,ArchiveID
	"null																		H3	
	$pk																			NewSmallID					-- ISN_LOCATION
	@NewSmallID																	[ISN_LOCATION]				-- PK
	@OPIS																		[]							-- FK(OPIS.KOD), N ОПИСИ
	@H1																			[]							-- НАЧ. (НАЧАЛЬНЫЙ) НОМЕР
	@H2																			[]							-- КОН. (КОНЕЧНЫЙ) НОМЕР
	'Хранилище'+char(32)+@H3													[NAME]						-- ХРАНИЛИЩЕ
	rtrim(@H3)+',,,,,'															[NOTE]						-- ХРАНИЛИЩЕ
	@H4																			[]							-- ЭТАЖ (НОМЕР ЭТАЖА)
	@H5																			[]							-- ПОМЕЩЕНИЕ (НОМЕР ПОМЕЩЕНИЯ ХРАНИЛИЩА)
	@H6																			[]							-- СТЕЛЛАЖ (НОМЕР СТЕЛЛАЖА)
	@H7																			[]							-- ШКАФ (НОМЕР ШКАФА)
	@H8																			[]							-- ПОЛКА (НОМЕР ПОЛКИ)
	null																		[ISN_HIGH_LOCATION]
    @ArchiveID																	[ISN_ARCHIVE]
    null																		[CODE]
    'B'																			[FOREST_ELEM]
    'N'																			[PROTECTED]
    null																		[WEIGHT]
    
    $insql			if not exists(select L.ISN_LOCATION from tblLOCATION L where L.NAME = 'Хранилище'+char(32)+@H3 and %L.ISN_LOCATION%)
    $insql			select @Result = 0 else select @Result = 1

	$outsql			update tblLOCATION set ISN_HIGH_LOCATION =
	$outsql			  (select top 1 L.ISN_LOCATION from tblLOCATION L where L.NAME = 'Архивохранилище' and %L.ISN_LOCATION%)
	$outsql			where tblLOCATION.ISN_LOCATION = @NewSmallID
END

BEGIN [OPIS2].[tblLOCATION]
	#primary
	"link																		NewSmallID2,ArchiveID
	"null																		H4	
	$pk																			NewSmallID2					-- ISN_LOCATION
    @NewSmallID																	[]							--
	@NewSmallID2																[ISN_LOCATION]				-- PK
	@OPIS																		[]							-- FK(OPIS.KOD), N ОПИСИ
	@H1																			[]							-- НАЧ. (НАЧАЛЬНЫЙ) НОМЕР
	@H2																			[]							-- КОН. (КОНЕЧНЫЙ) НОМЕР
	rtrim(@H3)+','+rtrim(@H4)+',,,,'											[NOTE]						-- ХРАНИЛИЩЕ
	'Этаж'+char(32)+@H4															[NAME]						-- ЭТАЖ (НОМЕР ЭТАЖА)
	@H5																			[]							-- ПОМЕЩЕНИЕ (НОМЕР ПОМЕЩЕНИЯ ХРАНИЛИЩА)
	@H6																			[]							-- СТЕЛЛАЖ (НОМЕР СТЕЛЛАЖА)
	@H7																			[]							-- ШКАФ (НОМЕР ШКАФА)
	@H8																			[]							-- ПОЛКА (НОМЕР ПОЛКИ)
	null																		[ISN_HIGH_LOCATION]
    @ArchiveID																	[ISN_ARCHIVE]
    null																		[CODE]
    'B'																			[FOREST_ELEM]
    'N'																			[PROTECTED]
    null																		[WEIGHT]

    $insql			if not exists(select L.ISN_LOCATION from tblLOCATION L where L.NAME = 'Этаж'+char(32)+@H4 and %L.ISN_LOCATION%)
    $insql			select @Result = 0 else select @Result = 1

	$outsql			update tblLOCATION set ISN_HIGH_LOCATION =
	$outsql			  (select top 1 L.ISN_LOCATION from tblLOCATION L where L.NAME = 'Хранилище'+char(32)+@H3 and %L.ISN_LOCATION%)
	$outsql			where ISN_LOCATION = @NewSmallID2
END

BEGIN [OPIS2].[tblLOCATION]
	#primary
	"link																		NewSmallID3,ArchiveID
	"null																		H5	
	$pk																			NewSmallID3					-- ISN_LOCATION
    @NewSmallID2																[]							--
	@NewSmallID3																[ISN_LOCATION]				-- PK
	@OPIS																		[]							-- FK(OPIS.KOD), N ОПИСИ
	@H1																			[]							-- НАЧ. (НАЧАЛЬНЫЙ) НОМЕР
	@H2																			[]							-- КОН. (КОНЕЧНЫЙ) НОМЕР
	rtrim(@H3)+','+rtrim(@H4)+','+rtrim(@H5)+',,,'								[NOTE]						-- ХРАНИЛИЩЕ
	'Помещение'+char(32)+@H5													[NAME]						-- ПОМЕЩЕНИЕ (НОМЕР ПОМЕЩЕНИЯ ХРАНИЛИЩА)
	@H6																			[]							-- СТЕЛЛАЖ (НОМЕР СТЕЛЛАЖА)
	@H7																			[]							-- ШКАФ (НОМЕР ШКАФА)
	@H8																			[]							-- ПОЛКА (НОМЕР ПОЛКИ)
	null																		[ISN_HIGH_LOCATION]
    @ArchiveID																	[ISN_ARCHIVE]
    null																		[CODE]
    'B'																			[FOREST_ELEM]
    'N'																			[PROTECTED]
    null																		[WEIGHT]
    
    $insql			if not exists(select L.ISN_LOCATION from tblLOCATION L where L.NAME = 'Помещение'+char(32)+@H5 and %L.ISN_LOCATION%)
    $insql			select @Result = 0 else select @Result = 1
    
	$outsql			update tblLOCATION set ISN_HIGH_LOCATION =
	$outsql			  (select top 1 L.ISN_LOCATION from tblLOCATION L where L.NAME = 'Этаж'+char(32)+@H4 and %L.ISN_LOCATION%)
	$outsql			where ISN_LOCATION = @NewSmallID3
END

BEGIN [OPIS2].[tblLOCATION]
	#primary
	"link																		NewSmallID4,ArchiveID
	"null																		H6	
	$pk																			NewSmallID4					-- ISN_LOCATION
    @NewSmallID3																[]							--
	@NewSmallID4																[ISN_LOCATION]				-- PK
	@OPIS																		[]							-- FK(OPIS.KOD), N ОПИСИ
	@H1																			[]							-- НАЧ. (НАЧАЛЬНЫЙ) НОМЕР
	@H2																			[]							-- КОН. (КОНЕЧНЫЙ) НОМЕР
	rtrim(@H3)+','+rtrim(@H4)+','+rtrim(@H5)+','+rtrim(@H6)+',,'				[NOTE]						-- ХРАНИЛИЩЕ
	'Стеллаж'+char(32)+@H6														[NAME]						-- СТЕЛЛАЖ (НОМЕР СТЕЛЛАЖА)
	@H7																			[]							-- ШКАФ (НОМЕР ШКАФА)
	@H8																			[]							-- ПОЛКА (НОМЕР ПОЛКИ)
	null																		[ISN_HIGH_LOCATION]
    @ArchiveID																	[ISN_ARCHIVE]
    null																		[CODE]
    'B'																			[FOREST_ELEM]
    'N'																			[PROTECTED]
    null																		[WEIGHT]
    
    $insql			if not exists(select L.ISN_LOCATION from tblLOCATION L where L.NAME = 'Стеллаж'+char(32)+@H6 and %L.ISN_LOCATION%)
    $insql			select @Result = 0 else select @Result = 1
    
	$outsql			update tblLOCATION set ISN_HIGH_LOCATION =
	$outsql			  (select top 1 L.ISN_LOCATION from tblLOCATION L where L.NAME = 'Помещение'+char(32)+@H5 and %L.ISN_LOCATION%)
	$outsql			where ISN_LOCATION = @NewSmallID4
END

BEGIN [OPIS2].[tblLOCATION]
	#primary
	"link																		NewID,ArchiveID
	"null																		H7	
	$pk																			NewID						-- ISN_LOCATION
    @NewSmallID4																[]							--
	@NewID																		[ISN_LOCATION]				-- PK
	@OPIS																		[]							-- FK(OPIS.KOD), N ОПИСИ
	@H1																			[]							-- НАЧ. (НАЧАЛЬНЫЙ) НОМЕР
	@H2																			[]							-- КОН. (КОНЕЧНЫЙ) НОМЕР
	rtrim(@H3)+','+rtrim(@H4)+','+rtrim(@H5)+','+rtrim(@H6)+','+rtrim(@H7)+','	[NOTE]						-- ХРАНИЛИЩЕ
	'Шкаф'+char(32)+@H7															[NAME]						-- ШКАФ (НОМЕР ШКАФА)
	@H8																			[]							-- ПОЛКА (НОМЕР ПОЛКИ)
	null																		[ISN_HIGH_LOCATION]
    @ArchiveID																	[ISN_ARCHIVE]
    null																		[CODE]
    'B'																			[FOREST_ELEM]
    'N'																			[PROTECTED]
    null																		[WEIGHT]
    
    $insql			if not exists(select L.ISN_LOCATION from tblLOCATION L where L.NAME = 'Шкаф'+char(32)+@H7 and %L.ISN_LOCATION%)
    $insql			select @Result = 0 else select @Result = 1
    
	$outsql			update tblLOCATION set ISN_HIGH_LOCATION =
	$outsql			  (select top 1 L.ISN_LOCATION from tblLOCATION L where L.NAME = 'Стеллаж'+char(32)+@H6 and %L.ISN_LOCATION%)
	$outsql			where ISN_LOCATION = @NewID
END

BEGIN [OPIS2].[tblLOCATION]
	#primary
	"link																					NewID2,ArchiveID
	"null																					H8	
	$pk																						NewID2						-- ISN_LOCATION
    @NewID																					[]							--
	@NewID2																					[ISN_LOCATION]				-- PK
	@OPIS																					[]							-- FK(OPIS.KOD), N ОПИСИ
	@H1																						[]							-- НАЧ. (НАЧАЛЬНЫЙ) НОМЕР
	@H2																						[]							-- КОН. (КОНЕЧНЫЙ) НОМЕР
	rtrim(@H3)+','+rtrim(@H4)+','+rtrim(@H5)+','+rtrim(@H6)+','+rtrim(@H7)+','+rtrim(@H8)	[NOTE]						-- ХРАНИЛИЩЕ
	'Полка'+char(32)+@H8																	[NAME]						-- ПОЛКА (НОМЕР ПОЛКИ)
	null																					[ISN_HIGH_LOCATION]
    @ArchiveID																				[ISN_ARCHIVE]
    null																					[CODE]
    'B'																						[FOREST_ELEM]
    'N'																						[PROTECTED]
    null																					[WEIGHT]
    
    $insql			if not exists(select L.ISN_LOCATION from tblLOCATION L where L.NAME = 'Полка'+char(32)+@H8 and %L.ISN_LOCATION%)
    $insql			select @Result = 0 else select @Result = 1
    
	$outsql			update tblLOCATION set ISN_HIGH_LOCATION =
	$outsql			  (select top 1 L.ISN_LOCATION from tblLOCATION L where L.NAME = 'Шкаф'+char(32)+@H7 and %L.ISN_LOCATION%)
	$outsql			where ISN_LOCATION = @NewID2
END

BEGIN [OPIS2].[tblREF_LOCATION]
	#relative
	"link																		NewID3,NewID2,OPIS
	"null																		H3	
	$pk																			NewID3						-- ISN_REF_LOCATION
	$fk																			OPIS						--
	@NewID3																		[ISN_REF_LOCATION]			-- PK
	@NewID2																		[ISN_LOCATION]				--
	@H1																			[UNIT_NUM_FROM]				-- НАЧ. (НАЧАЛЬНЫЙ) НОМЕР
	@H2																			[UNIT_NUM_TO]				-- КОН. (КОНЕЧНЫЙ) НОМЕР
	@OPIS																		[ISN_OBJ]					--
	702																			[KIND]						-- 702
	null																		[NOTE]						--
	@H3																			[]							--
	@H8																			[]							--
	
    $insql			if exists(select ISN_LOCATION from tblLOCATION where ISN_LOCATION = @NewID2)
    $insql			select @Result = 0 else select @Result = 1
END
