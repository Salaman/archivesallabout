﻿-- Опись

/* поля не используются:
[ISN_REPRODUCTION_METHOD]
[ISN_DOC_KIND]
[ISN_REQUIRED_WORK]
[UNITS_WITH_TREASURES_COUNT]
[MUSEUM_UNITS_COUNT]
[WEIGHT]
[NOTE]
[VOL_NUM]
[HAS_TRADITIONAL_DOCS]
[HAS_ELECTRONIC_DOCS]
[ADDITIONAL_NSA]
[KEYWORDS] */

BEGIN [OPIS].[tblREPRODUCTION_METHOD_CL]
	#primary
	"null																		G27	
	$set																		NewSmallID
	$pk																			NewSmallID							-- ISN_REPRODUCTION_METHOD
	@NewSmallID																	[ISN_REPRODUCTION_METHOD]			-- PK
	null																		[CODE]								--
	@G27																		[NAME]								--
	null																		[NOTE]								--
	@KOD																		[]									--
	
	$insql				if not (@G27 in ('рук.','маш.','типогр.','компьют.','ротатор','ротапринт','мимеограф','стеклограф','комп. распечатка','иное'))
	$insql				and not exists(select R.ISN_REPRODUCTION_METHOD from tblREPRODUCTION_METHOD_CL R where R.NAME = @G27 and %R.ISN_REPRODUCTION_METHOD%)
	$insql				select @Result = 0 else select @Result = 1
END

BEGIN [OPIS].[tblINVENTORY]
	#primary
	"link																		KOD,FOND
	$pk																			KOD								-- ISN_INVENTORY
	$fk																			FOND							-- ISN_FUND
	@KOD																		[ISN_INVENTORY]					-- PK
	@FOND																		[ISN_FUND]						-- FK (FOND.KOD)
	rtrim(ltrim(substring(@OKOD,1,7)))											[INVENTORY_NUM_1]				-- N ОПИСИ
	rtrim(ltrim(substring(@OKOD,8,7)))											[INVENTORY_NUM_2]				-- N ОПИСИ
	rtrim(ltrim(substring(@OKOD,15,7)))											[INVENTORY_NUM_3]				-- N ОПИСИ
	@ONAME																		[INVENTORY_NAME]				-- НАЗВАНИЕ ОПИСИ
	case(@G1=1:1,2:3,3:4,4:2,5:7,6:5,7:6,8:8,9:10,10:9)							[ISN_INVENTORY_TYPE]			-- ТИП ОПИСИ
	case(@G2=1:'a',2:'b',3:'c')													[INVENTORY_KIND]				-- КАТЕГОРИЯ ОПИСИ (0,1,2,3)
	@G3																			[DOC_START_YEAR]				-- НАЧАЛЬНАЯ ДАТА
	case(@G4='*':'Y','':'N')													[DOC_START_YEAR_INEXACT]		-- НАЧАЛЬНАЯ ДАТА (*)
	@G5																			[DOC_END_YEAR]					-- КОНЕЧНАЯ ДАТА
	case(@G6='*':'Y','':'N')													[DOC_END_YEAR_INEXACT]			-- КОНЕЧНАЯ ДАТА (*)
	@G7																			[]								-- ОБЪЕМ ОПИСИ (в ед.хр.)
	@G8																			[]								-- ОБЪЕМ ОПИСИ (в ед.уч.)
	@G9																			[]								-- ОЦД (КОЛИЧЕСТВО УЧТЕННЫХ ОЦД)
	@G10																		[]								-- ИМЕЮТ СФ (КОЛИЧЕСТВО ЕД.ХР., ИМЕЮЩИХ СФ)
	@G11																		[]								-- ИМЕЮТ ФП (КОЛИЧЕСТВО ЕД.ХР., ИМЕЮЩИХ ФП)
	@G12																		[]								-- ЗАКАТАЛОГ. (КОЛИЧЕСТВО ЗАКАТАЛОГИЗИРОВАННЫХ ЕД.ХР./ЕД.УЧ.)
	@G13																		[]								-- НЕОБНАРУЖЕННЫЕ (КОЛИЧЕСТВО НЕОБНАРУЖЕННЫХ ДЕЛ)
	@G14																		[]								-- СЕКРЕТНЫЕ (КОЛИЧЕСТВО СЕКРЕТНЫХ ДЕЛ)
	case(@G15=1:'a',2:'b',3:'b',4:'b',5:'b')									[PRESENCE_FLAG]					-- ДВИЖЕНИЕ ()
	case(@G15=1:null,2:'a',3:'b',4:'c',5:'d')									[ABSENCE_REASON]				-- ДВИЖЕНИЕ ()
	@G16																		[MOVEMENT_NOTE]					-- СПРАВКА (СПРАВКА ПО ДВИЖЕНИЮ)
	case(@G17<>0:@G17)															[ISN_RECEIPT_SOURCE]			-- ИСТОЧНИК ПОСТУПЛЕНИЯ (ТИП ИСТОЧНИКА ПОСТУПЛЕНИЯ) ()
	case(@G18<>0:@G18)															[ISN_RECEIPT_REASON]			-- ОСНОВАНИЕ ПОСТУПЛЕНИЯ ()
	case(@G19=1:'a',2:'b')														[INVENTORY_KEEP_PERIOD]			-- СРОК ХРАНЕНИ ()
	case(@G20<>0:@G20)															[ISN_SECURLEVEL]				-- Х-КА СЕКРЕТН. (ХАРАКТЕРИСТИКА СЕКРЕТНОСТИ) ()
	case(@G21=1:'o',2:'p')														[SECURITY_CHAR]					-- ДОСТУП (ДОСТУП К ДОКУМЕНТАМ) ()
	@G22																		{}								-- ПРИЧИНА ОГРАНИЧЕНИЯ ()
	@G23																		[INVENTORY_DOC_WORK]			-- ВИДЫ РАБОТ
	@G24																		[]								-- СПРАВОЧНЫЙ АППАРАТ (НАЛИЧИЕ СПРАВОЧНОГО АППАРАТА ОПИСИ) ()
	@G25																		[CATALOGUING]					-- КАТАЛОГИЗАЦИЯ
	@G26																		[COPY_COUNT]					-- ЭКЗ.  (КОЛИЧЕСТВО ЭКЗЕМПЛЯРОВ ОПИСИ)
	@G27																		[]								-- СП. ВОСПРОИЗВЕДЕНИЯ (СПОСОБ ВОСПРОИЗВЕДЕНИЯ) ()
	@G28																		[]								-- ТРЕБУЕМЫЕ ВИДЫ РАБОТ (ТРЕБУЕМЫЕ ВИДЫ РАБОТ С ОПИСЬЮ) ()
	@G29																		[]								-- ГРУПП. ДЕЛ (ПРИЗНАКИ ГРУППИРОВКИ ДЕЛ В ОПИСИ) ()
	@G30																		[]								-- ПОДСИСТЕМЫ НТД ()
	@G31																		[]								-- ВИДОВОЙ СОСТАВ (ВИДОВОЙ СОСТАВ ДОКУМЕНТОВ) ()
	@G32																		[]								-- НОСИТЕЛЬ (НОСИТЕЛЬ ДОКУМЕНТОВ) ()
	@G33																		[]								-- ЗАКАРТОНИРОВАНО
	@G34																		[]								-- Н\ПОВРЕЖДЕННЫЕ
	@G35																		[]								-- РЕСТАВРАЦИЯ (КОЛИЧЕСТВО ЕД.ХР., ТРЕБУЮЩИХ РЕСТАВРАЦИИ)
	@G36																		[]								-- ПЕРЕПЛЕТ (КОЛИЧЕСТВО ДЕЛ, ТРЕБУЮЩИХ ПЕРЕПЛЕТА ИЛИ ПОДШИВКИ)
	@G37																		[]								-- ДЕЗИНФЕКЦИЯ (КОЛИЧЕСТВО  ЕД.ХР,, ТРЕБУЮЩИХ ДЕЗИНФЕКЦИИ)
	@G38																		[]								-- ДЕЗИНСЕКЦИЯ (КОЛИЧЕСТВО ЕД.ХР., ТРЕБУЮЩИХ ДЕЗИНСЕКЦИИ)
	@G39																		[]								-- ЗАТУХ.ТЕКСТЫ (КОЛИЧЕСТВО ЛИСТОВ ЗАТУХАЮЩИХ ТЕКСТОВ)
	@G40																		[]								-- ШИФРОВКА (КОЛИЧЕСТВО ДЕЛ, ТРЕБУЮЩИХ ШИФРОВКИ)
	@G41																		[]								-- ОБЛОЖКИ  (КОЛИЧЕСТВО ДЕЛ, ТРЕБУЮЩИХ ЗАМЕНЫ ОБЛОЖЕК)
	@G42																		[]								-- ГОР. ОСНОВА (КОЛИЧЕСТВО ЕД.ХР. НА ГОРЮЧЕЙ ОСНОВЕ)
	@G43																		[]								-- КПО (КОЛИЧЕСТВО ЕД.ХР., ТРЕБУЮЩИХ КОНСЕРВАЦИОННО-ПРОФИЛАКТИЧЕСКОЙ ОБРАБОТКИ)
	@G44																		[]								-- ДАТА ПРОВЕРКИ
	@G45																		[ANNOTATE]						-- АННОТАЦИЯ
	@G46																		[]								-- ОБЪЕМ (ЕД.ХР.)
	@G47																		[]								-- ОБЪЕМ (ЕД.УЧ.)
	@G48																		[]								-- ОБЪЕМ ДОК-ОВ ПО Л/С
	@G49																		[]								-- ОБЪЕМ ДОК-ОВ ПО Л/С
	'T'																			[CARRIER_TYPE]
	null																		[ISN_INVENTORY_STORAGE]
	null																		[ISN_SECURITY_REASON]

	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @FOND)
	$insql			select @Result = 0 else select @Result = 1

	$outsql			update tblINVENTORY set ISN_DOC_KIND = case
	$outsql			when @G31 = 1 and @G1 = 5 then 10
	$outsql			when @G31 = 2 and @G1 = 5 then 11
	$outsql			when @G31 = 3 and @G1 = 5 then 12
	$outsql			when @G31 = 4 and @G1 = 5 then 13
	$outsql			when @G31 = 5 and @G1 = 5 then 14
	$outsql			when @G31 = 6 and @G1 = 5 then 16366
	$outsql			when @G31 = 1 and @G1 = 6 then 16367
	$outsql			when @G31 = 2 and @G1 = 6 then 16369
	$outsql			when @G31 = 3 and @G1 = 6 then 16370
	$outsql			when @G31 = 1 and @G1 = 7 then 15
	$outsql			when @G31 = 2 and @G1 = 7 then 16379
	$outsql			when @G31 = 3 and @G1 = 7 then 16380
	$outsql			when @G31 = 1 and @G1 = 8 then 16382
	$outsql			when @G31 = 2 and @G1 = 8 then 16383
	$outsql			when @G31 = 3 and @G1 = 8 then 16384
	$outsql			when @G31 = 4 and @G1 = 8 then 16385
	$outsql			when @G31 = 5 and @G1 = 8 then 16386
	$outsql			when @G31 = 6 and @G1 = 8 then 16375
	$outsql			end where ISN_INVENTORY = @KOD
	
	$outsql			if @G27 in ('рук.','маш.','типогр.','компьют.','ротатор','ротапринт','мимеограф','стеклограф','комп. распечатка','иное')
	$outsql				update tblINVENTORY set ISN_REPRODUCTION_METHOD = case
	$outsql				when @G27 = 'рук.' then 2
	$outsql				when @G27 = 'маш.' then 3
	$outsql				when @G27 = 'типогр.' then 4
	$outsql				when @G27 = 'компьют.' then 16364
	$outsql				when @G27 = 'ротатор' then 16360
	$outsql				when @G27 = 'ротапринт' then 16361
	$outsql				when @G27 = 'мимеограф' then 16362
	$outsql				when @G27 = 'стеклограф' then 16363
	$outsql				when @G27 = 'комп. распечатка' then 16364
	$outsql				when @G27 = 'иное' then 6
	$outsql				end where ISN_INVENTORY = @KOD
	$outsql			else
	$outsql				update tblINVENTORY set ISN_REPRODUCTION_METHOD =
	$outsql				  (select top 1 R.ISN_REPRODUCTION_METHOD from tblREPRODUCTION_METHOD_CL R where R.NAME = @G27 and %R.ISN_REPRODUCTION_METHOD%)
	$outsql				where ISN_INVENTORY = @KOD	
	
	$val			G1
	$reg			^([1-9]|10)$
	$log			'Ошибка в разделе ОПИСЬ (Фонд № {FOND_NUM = @FOND}, Опись № {INVENTORY_NUM = @KOD}) -> экран 11 -> поле ТИП ОПИСИ: недопустимое значение'
	$log			'Возможные значения: "упр.", "л/п", "НТД", "л/с", "кино", "фото", "фоно", "видео", "МЧД", "м/ф-подл."'
	
	$val			G18
	$reg			^([0-9])$
	$log			'Ошибка в разделе ОПИСЬ (Фонд № {FOND_NUM = @FOND}, Опись № {INVENTORY_NUM = @KOD}) -> экран 12 -> поле ОСНОВАНИЯ ПОСТУПЛЕНИЯ: недопустимое значение'
	$log			'Возможные значения: "истечение вед. срока хран.", "ликв. учр. при правопреемнике", "ликв. учр. без правопреемника", "дарение", "покупка", "репарация", "реституция", "депозит", "расп. АО "'
END

BEGIN [OPIS].[tblINVENTORY_DOC_TYPE]
	#relative
	"link																		KOD
	"null																		G1
	$fk																			KOD									-- ISN_INVENTORY
	@KOD																		[ISN_INVENTORY]						-- 
	case(@G1=1:1,2:3,3:4,4:2,5:7,6:5,7:6,8:8,9:10,10:9)							[ISN_DOC_TYPE]						--
	
	$insql			if exists(select ISN_INVENTORY from tblINVENTORY where ISN_INVENTORY = @KOD)
	$insql			select @Result = 0 else select @Result = 1
END

BEGIN [OPIS].[tblINVENTORY_DOC_TYPE]
	#relative
	"link																		KOD
	$fk																			KOD									-- ISN_INVENTORY
	@KOD																		[ISN_INVENTORY]						-- 
	2																			[ISN_DOC_TYPE]						-- 
	@G48																		[]

	$insql			if (exists(select ISN_INVENTORY from tblINVENTORY where ISN_INVENTORY = @KOD) and isnull(@G48,0) > 0)
	$insql			select @Result = 0 else select @Result = 1
END

BEGIN [OPIS].[tblSECURITY_REASON]
	#primary
	"link																		KOD
	"null																		G22	
	$pk																			NewID2								-- ISN_SECURITY_REASON
	@NewID2																		[ISN_SECURITY_REASON]				-- PK
	@G22																		[NAME]								-- ПРИЧИНА ОГРАНИЧЕНИЯ ()
	@KOD																		[]									--
	
	$insql			if exists(select S.ISN_SECURITY_REASON from tblSECURITY_REASON S where S.NAME = @G22 and %S.ISN_SECURITY_REASON%)
	$insql			update tblINVENTORY set ISN_SECURITY_REASON =
	$insql			  (select top 1 S.ISN_SECURITY_REASON from tblSECURITY_REASON S where S.NAME = @G22 and %S.ISN_SECURITY_REASON%)
	$insql			where tblINVENTORY.ISN_INVENTORY = @KOD
	
	$outsql			update tblINVENTORY set ISN_SECURITY_REASON = @NewID2
	$outsql			where tblINVENTORY.ISN_INVENTORY = @KOD
END

BEGIN [OPIS].[tblINVENTORY_CHECK]
	#relative
	"link																		KOD
	$fk																			KOD										-- ISN_INVENTORY
	@KOD																		[ISN_INVENTORY]							-- PK
	@G33																		[CARDBOARDED]							-- ЗАКАРТОНИРОВАНО
	@G34																		[UNITS_DBR]								-- Н\ПОВРЕЖДЕННЫЕ
	@G35																		[UNITS_NEED_RESTORATION]				-- РЕСТАВРАЦИЯ (КОЛИЧЕСТВО ЕД.ХР., ТРЕБУЮЩИХ РЕСТАВРАЦИИ)
	@G36																		[UNITS_NEED_BINDING]					-- ПЕРЕПЛЕТ (КОЛИЧЕСТВО ДЕЛ, ТРЕБУЮЩИХ ПЕРЕПЛЕТА ИЛИ ПОДШИВКИ)
	@G37																		[UNITS_NEED_DISINFECTION]				-- ДЕЗИНФЕКЦИЯ (КОЛИЧЕСТВО  ЕД.ХР,, ТРЕБУЮЩИХ ДЕЗИНФЕКЦИИ)
	@G38																		[UNITS_NEED_DISINSECTION]				-- ДЕЗИНСЕКЦИЯ (КОЛИЧЕСТВО ЕД.ХР., ТРЕБУЮЩИХ ДЕЗИНСЕКЦИИ)
	@G39																		[FADING_PAGES]							-- ЗАТУХ.ТЕКСТЫ (КОЛИЧЕСТВО ЛИСТОВ ЗАТУХАЮЩИХ ТЕКСТОВ)
	@G40																		[UNITS_NEED_ENCIPHERING]				-- ШИФРОВКА (КОЛИЧЕСТВО ДЕЛ, ТРЕБУЮЩИХ ШИФРОВКИ)
	@G41																		[UNITS_NEED_COVER_CHANGE]				-- ОБЛОЖКИ  (КОЛИЧЕСТВО ДЕЛ, ТРЕБУЮЩИХ ЗАМЕНЫ ОБЛОЖЕК)
	@G42																		[UNITS_INFLAMMABLE]						-- ГОР. ОСНОВА (КОЛИЧЕСТВО ЕД.ХР. НА ГОРЮЧЕЙ ОСНОВЕ)
	@G43																		[UNITS_NEED_KPO]						-- КПО (КОЛИЧЕСТВО ЕД.ХР., ТРЕБУЮЩИХ КОНСЕРВАЦИОННО-ПРОФИЛАКТИЧЕСКОЙ ОБРАБОТКИ)

	$insql			if exists(select ISN_INVENTORY from tblINVENTORY where ISN_INVENTORY = @KOD)
	$insql			select @Result = 0 else select @Result = 1
END

BEGIN [OPIS].[tblINV_REQUIRED_WORK_CL]
	#primary
	"null																		G28	
	$set																		NewSmallID2
	$pk																			NewSmallID2							-- ISN_REQUIRED_WORK
	$parse																		G28
	@NewSmallID2																[ISN_REQUIRED_WORK]					-- PK
	null																		[CODE]								--
	@Parse																		[NAME]								--
	null																		[NOTE]								--
	@KOD																		[]									--
	@G28																		[]
	@Parse																		[]
	
	$insql			if not (@Parse in ('н/описание','редактир.','тит. лист','оглав.','предисл.','сп. сокр.','указатели','перераб.','кат-ция','перепеч.','ксерокоп.','форм. тома','переплет'))
	$insql			and not exists(select I.ISN_REQUIRED_WORK from tblINV_REQUIRED_WORK_CL I where I.NAME = @Parse and %I.ISN_REQUIRED_WORK%)
	$insql			select @Result = 0 else select @Result = 1
END

BEGIN [OPIS].[tblINVENTORY_REQUIRED_WORK]
	#relative
	"link																		KOD
	"null																		G28	
	$fk																			KOD								--
	$parse																		G28
	@KOD																		[ISN_INVENTORY]					--
	1																			[ISN_REQUIRED_WORK]				--
	@G28																		[]
	@Parse																		[]								--
	@ParseID																	[]
	
	$insql			if exists(select ISN_INVENTORY from tblINVENTORY where ISN_INVENTORY = @KOD)
	$insql			select @Result = 0 else select @Result = 1
	
	$outsql			if @Parse in ('н/описание','редактир.','тит. лист','оглав.','предисл.','сп. сокр.','указатели','перераб.','кат-ция','перепеч.','ксерокоп.','форм. тома','переплет')
	$outsql				update tblINVENTORY_REQUIRED_WORK set ISN_REQUIRED_WORK = case
	$outsql				when @Parse = 'н/описание' then 1
	$outsql				when @Parse = 'редактир.' then 2
	$outsql				when @Parse = 'тит. лист' then 3
	$outsql				when @Parse = 'оглав.' then 4
	$outsql				when @Parse = 'предисл.' then 5
	$outsql				when @Parse = 'сп. сокр.' then 6
	$outsql				when @Parse = 'указатели' then 7
	$outsql				when @Parse = 'перераб.' then 8
	$outsql				when @Parse = 'кат-ция' then 9
	$outsql				when @Parse = 'перепеч.' then 10
	$outsql				when @Parse = 'ксерокоп.' then 11
	$outsql				when @Parse = 'форм. тома' then 12
	$outsql				when @Parse = 'переплет' then 13
	$outsql				end where ISN_INVENTORY = @KOD and RowID = @ParseID
	$outsql			else
	$outsql				update tblINVENTORY_REQUIRED_WORK set ISN_REQUIRED_WORK =
	$outsql				  (select top 1 I.ISN_REQUIRED_WORK from tblINV_REQUIRED_WORK_CL I where I.NAME = @Parse and %I.ISN_REQUIRED_WORK%)
	$outsql				where ISN_INVENTORY = @KOD and RowID = @ParseID
END

BEGIN [OPIS].[tblGROUPING_ATTRIBUTE_CL]
	#primary
	"null																		G29	
	$set																		NewSmallID3
	$pk																			NewSmallID3							-- ISN_GROUPING_ATTRIBUTE
	@NewSmallID3																[ISN_GROUPING_ATTRIBUTE]			-- PK
	null																		[CODE]								--
	@G29																		[NAME]								--
	null																		[NOTE]								--
	@KOD																		[]									--
	
	$insql			if not (@G29 in ('хрон.','функц.','отрасл.','тем.','предм.-вопр.','ном.','корр.','геогр.','авт.','алф.','вид.','жанр.','структ.','значимость'))
	$insql			and not exists(select G.ISN_GROUPING_ATTRIBUTE from tblGROUPING_ATTRIBUTE_CL G where G.NAME = @G29 and %G.ISN_GROUPING_ATTRIBUTE%)
	$insql			select @Result = 0 else select @Result = 1
END

BEGIN [OPIS].[tblINVENTORY_GROUPING_ATTRIBUTE]
	#relative
	"link																		KOD
	"null																		G29	
	$fk																			KOD								--
	@KOD																		[ISN_INVENTORY]					--
	1																			[ISN_GROUPING_ATTRIBUTE]		--
	@G29																		[]								--
	
	$insql			if exists(select ISN_INVENTORY from tblINVENTORY where ISN_INVENTORY = @KOD)
	$insql			select @Result = 0 else select @Result = 1
	
	$outsql			if @G29 in ('хрон.','функц.','отрасл.','тем.','предм.-вопр.','ном.','корр.','геогр.','авт.','алф.','вид.','жанр.','структ.','значимость')
	$outsql				update tblINVENTORY_GROUPING_ATTRIBUTE set ISN_GROUPING_ATTRIBUTE = case
	$outsql				when @G29 = 'хрон.' then 1
	$outsql				when @G29 = 'функц.' then 2
	$outsql				when @G29 = 'отрасл.' then 3
	$outsql				when @G29 = 'тем.' then 4
	$outsql				when @G29 = 'предм.-вопр.' then 5
	$outsql				when @G29 = 'ном.' then 6
	$outsql				when @G29 = 'корр.' then 7
	$outsql				when @G29 = 'геогр.' then 8
	$outsql				when @G29 = 'авт.' then 9
	$outsql				when @G29 = 'алф.' then 10
	$outsql				when @G29 = 'вид.' then 11
	$outsql				when @G29 = 'жанр.' then 12
	$outsql				when @G29 = 'структ.' then 16434
	$outsql				when @G29 = 'значимость' then 16435
	$outsql				end where ISN_INVENTORY = @KOD
	$outsql			else
	$outsql				update tblINVENTORY_GROUPING_ATTRIBUTE set ISN_GROUPING_ATTRIBUTE =
	$outsql				  (select top 1 G.ISN_GROUPING_ATTRIBUTE from tblGROUPING_ATTRIBUTE_CL G where G.NAME = @G29 and %G.ISN_GROUPING_ATTRIBUTE%)
	$outsql				where ISN_INVENTORY = @KOD
END

BEGIN [OPIS].[tblSTORAGE_MEDIUM_CL]
	#primary
	"link																		KOD
	"null																		G32	
	$pk																			NewID3								-- ISN_REQUIRED_WORK
	$parse																		G32
	@NewID3																        [ISN_STORAGE_MEDIUM]	
	null																		[ISN_HIGH_STORAGE_MEDIUM]				-- PK
	@Parse																		[NAME]								--
	'B'																		    [FOREST_ELEM]								--
	'N'																		    [PROTECTED]	
	@KOD																		[]									--
	@G32																		[]
	@Parse																		[]
	
	$insql			if not (@Parse in ('dddddd'))
	$insql			and not exists(select I.ISN_STORAGE_MEDIUM from tblSTORAGE_MEDIUM_CL I where I.NAME = @Parse and %I.ISN_STORAGE_MEDIUM%)
	$insql			select @Result = 0 else select @Result = 1

	$outsql			update tblSTORAGE_MEDIUM_CL set ISN_HIGH_STORAGE_MEDIUM =
	$outsql				(select top 1 S.ISN_STORAGE_MEDIUM from tblSTORAGE_MEDIUM_CL S where ltrim(rtrim(S.NAME)) = 'Носители документов' and %S.ISN_STORAGE_MEDIUM%)
	$outsql			where ISN_STORAGE_MEDIUM = @NewID3
	
	$outsql			update tblINVENTORY set ISN_INVENTORY_STORAGE = @NewID3
	$outsql			where ISN_INVENTORY = @KOD
END

BEGIN [OPIS].[tblINVENTORY_DOC_STORAGE]
	#relative
	"link																		KOD
	"null																		G32	
	$fk																			KOD								--
	$parse																		G32
	@KOD																		[ISN_INVENTORY]					--
	766																			[ISN_STORAGE_MEDIUM]				--
	@G32																		[]
	@Parse																		[]								--
	@ParseID																	[]
	
	$insql			if exists(select ISN_INVENTORY from tblINVENTORY where ISN_INVENTORY = @KOD)  and @G32 <> ''
	$insql			select @Result = 0 else select @Result = 1
	
	$outsql				update tblINVENTORY_DOC_STORAGE set ISN_STORAGE_MEDIUM =
	$outsql				  (select top 1 I.ISN_STORAGE_MEDIUM from tblSTORAGE_MEDIUM_CL I where I.NAME = @Parse and %I.ISN_STORAGE_MEDIUM%)
	$outsql				where ISN_INVENTORY = @KOD and RowID = @ParseID
END



BEGIN [OPIS].[tblPAPER_CLS_INV]
	#primary
	"link																		KOD,NewID4
	"null																		G24	
	$pk																			NewID4	
	$parse                                                                      G24						
	@NewID4																		[ISN_PAPER_CLS_INV]			
	@Parse                                                                      [NAME]	
    'N'                                                                         [PROTECTED]
	100                                                                         [WEIGHT]
	@G24																		[]
	@KOD																		[]
	
	$insql			if exists(select ISN_INVENTORY from tblINVENTORY where ISN_INVENTORY = @KOD) and @G24 <> ''
	$insql			and not exists (select [NAME] from tblPAPER_CLS_INV where [NAME]=@Parse)
	$insql          and not exists (select [NAME] from tblPAPER_CLS_INV where ISN_PAPER_CLS_INV = case 
	$insql					when @Parse = 'тит. лист' then 1
	$insql					when @Parse = 'оглавл.' then 2
	$insql					when @Parse = 'предисл.' then 3
	$insql					when @Parse = 'сп. сокр.' then 4
	$insql					when @Parse = 'перевод. табл.' then 5
	$insql					when @Parse = 'указ. предмет.' then 6
	$insql					when @Parse = 'указ. предмет.-тем.' then 7
	$insql					when @Parse = 'указ. именной' then 8
	$insql					when @Parse = 'указ. геогр.' then 9
	$insql					when @Parse = 'указ. хрон.' then 10
	$insql					when @Parse = 'указ. ист. учр.' then 11
	$insql					when @Parse = 'указ. вид. док.' then 12
	$insql					when @Parse = 'указ. структ.' then 13
	$insql					when @Parse = 'указ. отрасл.' then 14
	$insql					when @Parse = 'перечень вопр.' then 15 end)
	$insql			select @Result = 0 else select @Result = 1
END

BEGIN [OPIS].[tblINVENTORY_PAPER_CLS]
	#relative
	"link																		KOD,NewID4
	$fk																			NewID4									
	$parse																		G24								
	@KOD																	    [ISN_INVENTORY]
	1                                                                           [ISN_PAPER_CLS_INV]
	@Parse																		[]	
	@G24																	    []
	@NewID4                                                                     []
	@ParseID                                                                    []
	
	$insql			if exists(select ISN_INVENTORY from tblINVENTORY where ISN_INVENTORY = @KOD) and @G24 <> ''
	$insql			select @Result = 0 else select @Result = 1
	
	$outsql				if (@Parse in ('тит. лист','оглавл.','предисл.','сп. сокр.',
	$outsql                            'перевод. табл.','указ. предмет.','указ. предмет.-тем.','указ. именной',
	$outsql                            'указ. геогр.', 'указ. хрон.','указ. ист. учр.','указ. вид. док.',
	$outsql                            'указ. структ.','указ. отрасл.','перечень вопр.'))
	$outsql					update tblINVENTORY_PAPER_CLS set ISN_PAPER_CLS_INV = case
	$outsql					when @Parse = 'тит. лист' then 1
	$outsql					when @Parse = 'оглавл.' then 2
	$outsql					when @Parse = 'предисл.' then 3
	$outsql					when @Parse = 'сп. сокр.' then 4
	$outsql					when @Parse = 'перевод. табл.' then 5
	$outsql					when @Parse = 'указ. предмет.' then 6
	$outsql					when @Parse = 'указ. предмет.-тем.' then 7
	$outsql					when @Parse = 'указ. именной' then 8
	$outsql					when @Parse = 'указ. геогр.' then 9
	$outsql					when @Parse = 'указ. хрон.' then 10
	$outsql					when @Parse = 'указ. ист. учр.' then 11
	$outsql					when @Parse = 'указ. вид. док.' then 12
	$outsql					when @Parse = 'указ. структ.' then 13
	$outsql					when @Parse = 'указ. отрасл.' then 14
	$outsql					when @Parse = 'перечень вопр.' then 15
	$outsql					end  where ISN_INVENTORY = @KOD  and RowID = @ParseID
	$outsql             else 
	$outsql                    update tblINVENTORY_PAPER_CLS set ISN_PAPER_CLS_INV =(SELECT ISN_PAPER_CLS_INV FROM tblPAPER_CLS_INV WHERE [NAME]=@Parse) where ISN_INVENTORY = @KOD  and RowID = @ParseID
	$outsql                    update tblINVENTORY_PAPER_CLS set DocID =(SELECT ID FROM tblINVENTORY WHERE ISN_INVENTORY = @KOD) where ISN_INVENTORY = @KOD  and RowID = @ParseID

END

----------------------------------------------------------------------------------
-- DOCUMENT_STATS
----------------------------------------------------------------------------------

BEGIN [OPIS].[tblDOCUMENT_STATS]
	#relative
	"link																		KOD,StepID,FOND
	$pk																			StepID						-- ISN_DOCUMENT_STATS
	$fk																			KOD							-- ISN_INVENTORY
	@StepID																		[ISN_DOCUMENT_STATS]
	@FOND																		[ISN_FUND]
	@KOD																		[ISN_INVENTORY]
	null																		[ISN_DOC_TYPE]
	'A'																			[CARRIER_TYPE]
		
	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @FOND)
	$insql			and exists(select ISN_INVENTORY from tblINVENTORY where ISN_INVENTORY = @KOD)
	$insql			select @Result = 0 else select @Result = 1
END

BEGIN [OPIS].[tblDOCUMENT_STATS]
	#relative
	"link																		KOD,StepID,FOND
	$pk																			StepID						-- ISN_DOCUMENT_STATS
	$fk																			KOD							-- ISN_INVENTORY
	@StepID																		[ISN_DOCUMENT_STATS]
	@FOND																		[ISN_FUND]
	@KOD																		[ISN_INVENTORY]
	null																		[ISN_DOC_TYPE]
	'P'																			[CARRIER_TYPE]
	
	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @FOND)
	$insql			and exists(select ISN_INVENTORY from tblINVENTORY where ISN_INVENTORY = @KOD)
	$insql			select @Result = 0 else select @Result = 1
END

BEGIN [OPIS].[tblDOCUMENT_STATS]
	#relative
	"link																		KOD,StepID,FOND
	$pk																			StepID						-- ISN_DOCUMENT_STATS
	$fk																			KOD							-- ISN_INVENTORY
	@StepID																		[ISN_DOCUMENT_STATS]
	@FOND																		[ISN_FUND]
	@KOD																		[ISN_INVENTORY]
	null																		[ISN_DOC_TYPE]
	'E'																			[CARRIER_TYPE]
	
	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @FOND)
	$insql			and exists(select ISN_INVENTORY from tblINVENTORY where ISN_INVENTORY = @KOD)
	$insql			select @Result = 0 else select @Result = 1
END

BEGIN [OPIS].[tblDOCUMENT_STATS]
	#relative
	"link																		KOD,StepID,FOND
	$pk																			StepID						-- ISN_DOCUMENT_STATS
	$fk																			KOD							-- ISN_INVENTORY
	@StepID																		[ISN_DOCUMENT_STATS]
	@FOND																		[ISN_FUND]
	@KOD																		[ISN_INVENTORY]
	null																		[ISN_DOC_TYPE]
	null																		[CARRIER_TYPE]
	
	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @FOND)
	$insql			and exists(select ISN_INVENTORY from tblINVENTORY where ISN_INVENTORY = @KOD)
	$insql			select @Result = 0 else select @Result = 1
END

BEGIN [OPIS].[tblDOCUMENT_STATS]
	#relative
	"link																		KOD,StepID,FOND
	$pk																			StepID						-- ISN_DOCUMENT_STATS
	$fk																			KOD							-- ISN_INVENTORY
	@StepID																		[ISN_DOCUMENT_STATS]
	@FOND																		[ISN_FUND]
	@KOD																		[ISN_INVENTORY]
	1																			[ISN_DOC_TYPE]
	'P'																			[CARRIER_TYPE]
	
	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @FOND)
	$insql			and exists(select ISN_INVENTORY from tblINVENTORY where ISN_INVENTORY = @KOD)
	$insql			select @Result = 0 else select @Result = 1
END

BEGIN [OPIS].[tblDOCUMENT_STATS]
	#relative
	"link																		KOD,StepID,FOND
	$pk																			StepID						-- ISN_DOCUMENT_STATS
	$fk																			KOD							-- ISN_INVENTORY
	@StepID																		[ISN_DOCUMENT_STATS]
	@FOND																		[ISN_FUND]
	@KOD																		[ISN_INVENTORY]
	2																			[ISN_DOC_TYPE]
	'P'																			[CARRIER_TYPE]
	
	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @FOND)
	$insql			and exists(select ISN_INVENTORY from tblINVENTORY where ISN_INVENTORY = @KOD)
	$insql			select @Result = 0 else select @Result = 1
END

BEGIN [OPIS].[tblDOCUMENT_STATS]
	#relative
	"link																		KOD,StepID,FOND
	$pk																			StepID						-- ISN_DOCUMENT_STATS
	$fk																			KOD							-- ISN_INVENTORY
	@StepID																		[ISN_DOCUMENT_STATS]
	@FOND																		[ISN_FUND]
	@KOD																		[ISN_INVENTORY]
	3																			[ISN_DOC_TYPE]
	'P'																			[CARRIER_TYPE]
	
	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @FOND)
	$insql			and exists(select ISN_INVENTORY from tblINVENTORY where ISN_INVENTORY = @KOD)
	$insql			select @Result = 0 else select @Result = 1
END

BEGIN [OPIS].[tblDOCUMENT_STATS]
	#relative
	"link																		KOD,StepID,FOND
	$pk																			StepID						-- ISN_DOCUMENT_STATS
	$fk																			KOD							-- ISN_INVENTORY
	@StepID																		[ISN_DOCUMENT_STATS]
	@FOND																		[ISN_FUND]
	@KOD																		[ISN_INVENTORY]
	4																			[ISN_DOC_TYPE]
	'P'																			[CARRIER_TYPE]
	
	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @FOND)
	$insql			and exists(select ISN_INVENTORY from tblINVENTORY where ISN_INVENTORY = @KOD)
	$insql			select @Result = 0 else select @Result = 1
END

BEGIN [OPIS].[tblDOCUMENT_STATS]
	#relative
	"link																		KOD,StepID,FOND
	$pk																			StepID						-- ISN_DOCUMENT_STATS
	$fk																			KOD							-- ISN_INVENTORY
	@StepID																		[ISN_DOCUMENT_STATS]
	@FOND																		[ISN_FUND]
	@KOD																		[ISN_INVENTORY]
	5																			[ISN_DOC_TYPE]
	'A'																			[CARRIER_TYPE]
	
	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @FOND)
	$insql			and exists(select ISN_INVENTORY from tblINVENTORY where ISN_INVENTORY = @KOD)
	$insql			select @Result = 0 else select @Result = 1
END

BEGIN [OPIS].[tblDOCUMENT_STATS]
	#relative
	"link																		KOD,StepID,FOND
	$pk																			StepID						-- ISN_DOCUMENT_STATS
	$fk																			KOD							-- ISN_INVENTORY
	@StepID																		[ISN_DOCUMENT_STATS]
	@FOND																		[ISN_FUND]
	@KOD																		[ISN_INVENTORY]
	6																			[ISN_DOC_TYPE]
	'A'																			[CARRIER_TYPE]
	
	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @FOND)
	$insql			and exists(select ISN_INVENTORY from tblINVENTORY where ISN_INVENTORY = @KOD)
	$insql			select @Result = 0 else select @Result = 1
END

BEGIN [OPIS].[tblDOCUMENT_STATS]
	#relative
	"link																		KOD,StepID,FOND
	$pk																			StepID						-- ISN_DOCUMENT_STATS
	$fk																			KOD							-- ISN_INVENTORY
	@StepID																		[ISN_DOCUMENT_STATS]
	@FOND																		[ISN_FUND]
	@KOD																		[ISN_INVENTORY]
	7																			[ISN_DOC_TYPE]
	'A'																			[CARRIER_TYPE]
	
	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @FOND)
	$insql			and exists(select ISN_INVENTORY from tblINVENTORY where ISN_INVENTORY = @KOD)
	$insql			select @Result = 0 else select @Result = 1
END

BEGIN [OPIS].[tblDOCUMENT_STATS]
	#relative
	"link																		KOD,StepID,FOND
	$pk																			StepID						-- ISN_DOCUMENT_STATS
	$fk																			KOD							-- ISN_INVENTORY
	@StepID																		[ISN_DOCUMENT_STATS]
	@FOND																		[ISN_FUND]
	@KOD																		[ISN_INVENTORY]
	8																			[ISN_DOC_TYPE]
	'A'																			[CARRIER_TYPE]
	
	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @FOND)
	$insql			and exists(select ISN_INVENTORY from tblINVENTORY where ISN_INVENTORY = @KOD)
	$insql			select @Result = 0 else select @Result = 1
END

BEGIN [OPIS].[tblDOCUMENT_STATS]
	#relative
	"link																		KOD,StepID,FOND
	$pk																			StepID						-- ISN_DOCUMENT_STATS
	$fk																			KOD							-- ISN_INVENTORY
	@StepID																		[ISN_DOCUMENT_STATS]
	@FOND																		[ISN_FUND]
	@KOD																		[ISN_INVENTORY]
	9																			[ISN_DOC_TYPE]
	'M'																			[CARRIER_TYPE]
	
	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @FOND)
	$insql			and exists(select ISN_INVENTORY from tblINVENTORY where ISN_INVENTORY = @KOD)
	$insql			select @Result = 0 else select @Result = 1
END

BEGIN [OPIS].[tblDOCUMENT_STATS]
	#relative
	"link																		KOD,StepID,FOND
	$pk																			StepID						-- ISN_DOCUMENT_STATS
	$fk																			KOD							-- ISN_INVENTORY
	@StepID																		[ISN_DOCUMENT_STATS]
	@FOND																		[ISN_FUND]
	@KOD																		[ISN_INVENTORY]
	4																			[ISN_DOC_TYPE]
	'E'																			[CARRIER_TYPE]
	
	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @FOND)
	$insql			and exists(select ISN_INVENTORY from tblINVENTORY where ISN_INVENTORY = @KOD)
	$insql			select @Result = 0 else select @Result = 1
END

BEGIN [OPIS].[tblDOCUMENT_STATS]
	#relative
	"link																		KOD,StepID,FOND
	$pk																			StepID						-- ISN_DOCUMENT_STATS
	$fk																			KOD							-- ISN_INVENTORY
	@StepID																		[ISN_DOCUMENT_STATS]
	@FOND																		[ISN_FUND]
	@KOD																		[ISN_INVENTORY]
	5																			[ISN_DOC_TYPE]
	'E'																			[CARRIER_TYPE]
	
	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @FOND)
	$insql			and exists(select ISN_INVENTORY from tblINVENTORY where ISN_INVENTORY = @KOD)
	$insql			select @Result = 0 else select @Result = 1
END

BEGIN [OPIS].[tblDOCUMENT_STATS]
	#relative
	"link																		KOD,StepID,FOND
	$pk																			StepID						-- ISN_DOCUMENT_STATS
	$fk																			KOD							-- ISN_INVENTORY
	@StepID																		[ISN_DOCUMENT_STATS]
	@FOND																		[ISN_FUND]
	@KOD																		[ISN_INVENTORY]
	6																			[ISN_DOC_TYPE]
	'E'																			[CARRIER_TYPE]
	
	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @FOND)
	$insql			and exists(select ISN_INVENTORY from tblINVENTORY where ISN_INVENTORY = @KOD)
	$insql			select @Result = 0 else select @Result = 1
END

BEGIN [OPIS].[tblDOCUMENT_STATS]
	#relative
	"link																		KOD,StepID,FOND
	$pk																			StepID						-- ISN_DOCUMENT_STATS
	$fk																			KOD							-- ISN_INVENTORY
	@StepID																		[ISN_DOCUMENT_STATS]
	@FOND																		[ISN_FUND]
	@KOD																		[ISN_INVENTORY]
	7																			[ISN_DOC_TYPE]
	'E'																			[CARRIER_TYPE]
	
	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @FOND)
	$insql			and exists(select ISN_INVENTORY from tblINVENTORY where ISN_INVENTORY = @KOD)
	$insql			select @Result = 0 else select @Result = 1
END

BEGIN [OPIS].[tblDOCUMENT_STATS]
	#relative
	"link																		KOD,StepID,FOND
	$pk																			StepID						-- ISN_DOCUMENT_STATS
	$fk																			KOD							-- ISN_INVENTORY
	@StepID																		[ISN_DOCUMENT_STATS]
	@FOND																		[ISN_FUND]
	@KOD																		[ISN_INVENTORY]
	8																			[ISN_DOC_TYPE]
	'E'																			[CARRIER_TYPE]
	
	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @FOND)
	$insql			and exists(select ISN_INVENTORY from tblINVENTORY where ISN_INVENTORY = @KOD)
	$insql			select @Result = 0 else select @Result = 1
END

BEGIN [OPIS].[tblDOCUMENT_STATS]
	#relative
	"link																		KOD,StepID,FOND
	$pk																			StepID						-- ISN_DOCUMENT_STATS
	$fk																			KOD							-- ISN_INVENTORY
	@StepID																		[ISN_DOCUMENT_STATS]
	@FOND																		[ISN_FUND]
	@KOD																		[ISN_INVENTORY]
	@G1																			[]
	@G7																			[]
	@G8																			[]
	@G9																			[]
	@G10																		[]
	@G11																		[]
	@G12																		[]
	@G13																		[]
	@G14																		[]
	@G47																		[]
	@G46																		[]
	@G48																		[]
	@G49																		[]

	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @FOND)
	$insql			and exists(select ISN_INVENTORY from tblINVENTORY where ISN_INVENTORY = @KOD)
	$insql			begin
	$insql				update tblDOCUMENT_STATS set
	$insql					ISN_DOC_TYPE = null,
	$insql					CARRIER_TYPE = null,
	$insql					UNIT_COUNT = cast(isnull(@G7, 0) as float),
	$insql					REG_UNIT = @G8,	
	$insql					UNIT_OC_COUNT = @G9,
	$insql					UNITS_CATALOGUED = @G12,
	$insql					UNITS_NOT_FOUND = @G13,
	$insql					--SECRET_UNITS = @G14,
	$insql					UNIT_HAS_SF = @G10,
	$insql					UNIT_HAS_FP = @G11,
	$insql					REG_UNIT_REGISTERED = @G47,
	$insql					UNIT_REGISTERED = @G46
	$insql				where ISN_FUND = @FOND and ISN_INVENTORY = @KOD
	$insql					and ISN_DOC_TYPE is null
	$insql					and CARRIER_TYPE is null
	$insql				update tblDOCUMENT_STATS set
	$insql					ISN_DOC_TYPE = null,
	$insql					CARRIER_TYPE = case when @G1 in (1,2,3,4) then 'P' when @G1 in (5,6,7,8) then 'A' when @G1 = 10 then 'M' end,
	$insql					UNIT_COUNT = cast(isnull(@G7, 0) as float),
	$insql					REG_UNIT = @G8,
	$insql					UNIT_OC_COUNT = @G9,
	$insql					UNITS_CATALOGUED = @G12,
	$insql					UNITS_NOT_FOUND = @G13,
	$insql					--SECRET_UNITS = @G14,
	$insql					UNIT_HAS_SF = @G10,
	$insql					UNIT_HAS_FP = @G11,
	$insql					REG_UNIT_REGISTERED = @G47,
	$insql					UNIT_REGISTERED = @G46
	$insql				where ISN_FUND = @FOND and ISN_INVENTORY = @KOD
	$insql					and ISN_DOC_TYPE is null
	$insql					and CARRIER_TYPE = case when @G1 in (1,2,3,4) then 'P' when @G1 in (5,6,7,8) then 'A' when @G1 = 10 then 'M' end
	$insql				if (@G1 = 1)
	$insql					begin
	$insql						if (@G48 > 0)
	$insql							update tblDOCUMENT_STATS set
	$insql								ISN_DOC_TYPE = case ISN_DOC_TYPE when 2 then 2 when 1 then 1 end,
	$insql								CARRIER_TYPE = 'P',
	$insql								UNIT_COUNT = case ISN_DOC_TYPE when 2 then @G48 when 1 then cast(isnull(@G7, 0) as float)- @G48 end ,
	$insql								UNIT_REGISTERED = @G49
	$insql							where ISN_FUND = @FOND and ISN_INVENTORY = @KOD and ISN_DOC_TYPE IN (1,2) and CARRIER_TYPE = 'P'
	$insql						else
	$insql							update tblDOCUMENT_STATS set
	$insql								ISN_DOC_TYPE = 1,
	$insql								CARRIER_TYPE = 'P',
	$insql								UNIT_COUNT = @G7,
	$insql						        REG_UNIT = @G8,
	$insql						        UNIT_OC_COUNT = @G9,
	$insql						        UNITS_CATALOGUED = @G12,
	$insql						        UNITS_NOT_FOUND = @G13,
	$insql						       --SECRET_UNITS = @G14,
	$insql						        UNIT_HAS_SF = @G10,
	$insql						        UNIT_HAS_FP = @G11,
	$insql						        REG_UNIT_REGISTERED = @G47,
	$insql						        UNIT_REGISTERED = @G46
	$insql							where ISN_FUND = @FOND and ISN_INVENTORY = @KOD and ISN_DOC_TYPE = 1 and CARRIER_TYPE = 'P'
	$insql					end
	$insql				else
	$insql					update tblDOCUMENT_STATS set
	$insql						ISN_DOC_TYPE = case @G1 when 4 then 2 when 2 then 3 when 3 then 4 when 5 then 7 when 6 then 5 when 7 then 6 when 8 then 8 when 10 then 9 end,
	$insql						CARRIER_TYPE = case @G1 when 4 then 'P' when 2 then 'P' when 3 then 'P' when 5 then 'A' when 6 then 'A' when 7 then 'A' when 8 then 'A' when 10 then 'M' end,
	$insql						UNIT_COUNT = @G7,
	$insql						REG_UNIT = @G8,
	$insql						UNIT_OC_COUNT = @G9,
	$insql						UNITS_CATALOGUED = @G12,
	$insql						UNITS_NOT_FOUND = @G13,
	$insql						--SECRET_UNITS = @G14,
	$insql						UNIT_HAS_SF = @G10,
	$insql						UNIT_HAS_FP = @G11,
	$insql						REG_UNIT_REGISTERED = @G47,
	$insql						UNIT_REGISTERED = @G46
	$insql					where ISN_FUND = @FOND and ISN_INVENTORY = @KOD
	$insql						and ISN_DOC_TYPE = case @G1 when 4 then 2 when 2 then 3 when 3 then 4 when 5 then 7 when 6 then 5 when 7 then 6 when 8 then 8 when 10 then 9 end
	$insql						and CARRIER_TYPE = case @G1 when 4 then 'P' when 2 then 'P' when 3 then 'P' when 5 then 'A' when 6 then 'A' when 7 then 'A' when 8 then 'A' when 10 then 'M' end
	$insql			end	
	$insql			select @Result = 1
END


