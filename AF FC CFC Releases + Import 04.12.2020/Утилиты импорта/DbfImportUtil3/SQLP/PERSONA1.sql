﻿-- Персоналии к фонду

/* поля не используются: - */

BEGIN
	if not exists(select C.ISN_CLS from tblCLS C where C.NAME = 'Межфондовые классификаторы' and %C.ISN_CLS%)
	insert into tblCLS
	([ID],[OwnerID],[CreationDateTime],[StatusID],[Deleted],[ISN_CLS],[ISN_HIGH_CLS],[CODE],[WEIGHT],[NAME],[OBJ_KIND],[MULTISELECT],[NOTE],[FOREST_ELEM],[PROTECTED])
	values (@ConstGUID,@OwnerID,GETDATE(),@StatusID,0,@ConstID,null,null,null,'Межфондовые классификаторы',701,'Y',null,'F','N')

	if not exists(select C.ISN_CLS from tblCLS C where C.NAME = 'Персоналии фондов' and %C.ISN_CLS%)
	begin
		insert into tblCLS
		([ID],[OwnerID],[CreationDateTime],[StatusID],[Deleted],[ISN_CLS],[ISN_HIGH_CLS],[CODE],[WEIGHT],[NAME],[OBJ_KIND],[MULTISELECT],[NOTE],[FOREST_ELEM],[PROTECTED])
		values (@ConstGUID4,@OwnerID,GETDATE(),@StatusID,0,@ConstID4,null,null,null,'Персоналии фондов',701,'Y',null,'T','N')

		update tblCLS set ISN_HIGH_CLS =
		(select top 1 C.ISN_CLS from tblCLS C where C.NAME = 'Межфондовые классификаторы' and %C.ISN_CLS%)
		where ISN_CLS = @ConstID4 and [NAME] = 'Персоналии фондов'
	end
END

BEGIN [PERSONA1].[tblCLS]
	#primary
	"link																		NewID
	$pk																			NewID					-- ISN_CLS
	@NewID																		[ISN_CLS]				-- PK
	@FOND																		[]
	@WORD																		[NAME]
	null																		[ISN_HIGH_CLS]
	null																		[CODE]
	null																		[WEIGHT]
	701																			[OBJ_KIND]
	null																		[NOTE]
	'Y'																			[MULTISELECT]
	'B'																			[FOREST_ELEM]
	'N'																			[PROTECTED]
	
	$insql			if not exists(select C.ISN_CLS from tblCLS C where C.NAME = @WORD and C.ISN_HIGH_CLS =
	$insql				(select top 1 CC.ISN_CLS from tblCLS CC where CC.NAME = 'Персоналии фондов' and %CC.ISN_CLS%) )
	$insql			select @Result = 0 else select @Result = 1
	
	$outsql			update tblCLS set ISN_HIGH_CLS =
	$outsql			  (select top 1 C.ISN_CLS from tblCLS C where C.NAME = 'Персоналии фондов' and %C.ISN_CLS%)
	$outsql			where tblCLS.ISN_CLS = @NewID
END

BEGIN [PERSONA1].[tblREF_CLS]
	#relative
	"link																		NewID,NewID2,FOND
	$fk																			FOND
	@NewID2																		[ISN_REF_CLS]
	@NewID																		[ISN_CLS]
	null																		[ISN_TREE]
	@FOND																		[ISN_OBJ]
	701																			[KIND]
	
	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @FOND) and exists(select ISN_CLS from tblCLS where ISN_CLS = @NewID)
	$insql			select @Result = 0 else select @Result = 1
	
	$outsql			update tblREF_CLS set ISN_TREE =
	$outsql			  (select C.ISN_HIGH_CLS from tblCLS C where C.ISN_CLS = @NewID)
	$outsql			where ISN_REF_CLS = @NewID2
END
