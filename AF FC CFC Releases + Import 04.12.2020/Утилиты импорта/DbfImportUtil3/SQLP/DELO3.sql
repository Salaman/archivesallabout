﻿-- Рассекречивание

/* поля не используются:
[ISN_ESTIMATE_REASON]
[ACT_PERSONS]
[NOTE]
[ACT_WORK]
[UNIT_COUNT] */

BEGIN [DELO3].[tblACT]
	#primary
	"link																		NewID,DELO,KOMISSIA
	$pk																			NewID					-- ISN_ACT
	@NewID																		[ISN_ACT]				-- PK
	@DELO																		[ISN_FUND]				-- FK
	@KOMISSIA																	[ISN_COMMISSION]		-- FK
	0																			[ACT_NUM]				-- Номер протокола
	null																		[ACT_DATE] 				-- Дата протокола
	@J3																			[ACT_NAME]				-- Заголовок рассекречивания
	@J4																			[PAGE_NUMBERS]			-- №№ ЛИСТОВ
	@J5																			[DOC_DATES]				-- Даты документов
	7																			[ISN_ACT_TYPE]			-- Комиссия всегда занимается рассекречиванием
	1																			[MOVEMENT_FLAG]			--
	703																			[ACT_OBJ]				-- 'unit'
	@J1																			[]
	@J2																			[]
	
	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @DELO)
	$insql			select @ResultLog = 0 else select @ResultLog = 1
	
	$insql			if isnull(ltrim(@J1), '') = ''
	$insql				select @ResultLog = 'PASS:Отсутствует номер в разделе РАССЕКРЕЧИВАНИЕ (Фонд № {FOND_NUM = @DELO}) -> экран 16 -> поле № и ДАТА АКТА: Акт импортирован с номером "без номера"'
	$insql			if isnull(ltrim(@J2), '') <> ''
	$insql				select @ResultLog = 'PASS:Отсутствует дата акта в разделе РАССЕКРЕЧИВАНИЕ (Фонд № {FOND_NUM = @DELO}) -> экран 16 -> поле № и ДАТА АКТА: Акт импортирован без даты'
	$insql			else
	$insql			begin
	$insql				begin try select convert(datetime, @J2, 104) end try
	$insql				begin catch select @ResultLog = 'Ошибка в разделе РАССЕКРЕЧИВАНИЕ (Фонд № {FOND_NUM = @DELO}) -> экран 16 -> поле № и ДАТА АКТА: недопустимое значение даты' end catch
	$insql			end
	
	$outsql			update tblACT set
	$outsql				ACT_NUM = (case
	$outsql					when isnull(ltrim(@J1), '0') = '0' then 'без номера'
	$outsql					else isnull(ltrim(@J1), '')
	$outsql				end),
	$outsql				ACT_DATE = (case
	$outsql					when isnull(ltrim(@J2), '') = '' then null
	$outsql					else convert(datetime, @J2, 104)
	$outsql				end)
	$outsql			where ISN_ACT = @NewID
END
