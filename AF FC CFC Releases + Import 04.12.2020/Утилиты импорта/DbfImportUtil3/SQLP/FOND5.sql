﻿-- Фондовые включения

/* поля не используются:
[ISN_CITIZEN] */

BEGIN [FOND5].[tblORGANIZ_CL]
	#primary
	"link																		NewID
	$pk																			NewID					-- ISN_ORGANIZ
	@NewID																		[ISN_ORGANIZ]			-- PK
	@E1																			[NAME]					-- ФОНДООБРАЗОВАТЕЛЬ
	@E2																			[]						-- ВИДЫ ДОКУМЕНТОВ
	@E3																			[]						-- НАЧАЛЬНАЯ ДАТА
	@E4																			[]						-- НАЧАЛЬНАЯ ДАТА (*)
	@E5																			[]						-- КОНЕЧНАЯ ДАТА
	@E6																			[]						-- КОНЕЧНАЯ ДАТА (*)
END

BEGIN [FOND5].[tblFUND_INCLUSION]
	#relative
	"link																		NewID2,FOND,NewID
	"null																		E1,E2,E3,E4,E5,E6
	$pk																			NewID2					-- ISN_INCLUSION
	$fk																			FOND					-- ISN_FUND
	@NewID2																		[ISN_INCLUSION]			-- PK
	@FOND																		[ISN_FUND]				-- FK
	@E1																			[]						-- ФОНДООБРАЗОВАТЕЛЬ
	null																		[ISN_CITIZEN]			--
	@NewID																		[ISN_ORGANIZ]			--
	@E2																			[DOC_TYPES]				-- ВИДЫ ДОКУМЕНТОВ
	@E3																			[START_YEAR]			-- НАЧАЛЬНАЯ ДАТА
	case(@E4='*':'Y','':'N')													[START_YEAR_INEXACT]	-- НАЧАЛЬНАЯ ДАТА (*)
	@E5																			[END_YEAR]				-- КОНЕЧНАЯ ДАТА
	case(@E6='*':'Y','':'N')													[END_YEAR_INEXACT]		-- КОНЕЧНАЯ ДАТА (*)
	
	$insql			if exists(select ISN_FUND from tblFUND where ISN_FUND = @FOND)
	$insql			select @Result = 0 else select @Result = 1
END
