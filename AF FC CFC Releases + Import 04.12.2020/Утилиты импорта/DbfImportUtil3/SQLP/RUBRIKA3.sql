﻿-- Рубрикатор (продолжение)

/* поля не используются: - */

BEGIN [RUBRIKA3].[tblCLS]
	#primary
	"link																		NewID
	$pk																			NewID
	
	@RID																		[]
	@R1																			[]
	@R2																			[]
	@R3																			[]
	@R4																			[]
	@R5																			[]
	@R6																			[]
	
	@NewID																		[ISN_CLS]
	null																		[ISN_HIGH_CLS]
	'UPDATE_CODE'																[CODE]
	null																		[WEIGHT]
	@RUBRIKA																	[NAME]
	701																			[OBJ_KIND]
	'Y'																			[MULTISELECT]
	null																		[NOTE]
	'B'																			[FOREST_ELEM]
	'N'																			[PROTECTED]
	
	$insql				if exists(select C.ISN_CLS from tblCLS C where C.NAME = @RUBRIKA and %C.ISN_CLS%  and C.PROTECTED='N'
	$insql              and C.NOTE=cast(@RID as varchar) + ' ' + 
	$insql              cast(isnull(@R1,'')as varchar)+' '+
	$insql              cast(isnull(@R2,'')as varchar)+' '+
	$insql              cast(isnull(@R3,'')as varchar)+' '+
	$insql              cast(isnull(@R4,'')as varchar)+' '+
	$insql              cast(isnull(@R5,'')as varchar)+' '+
	$insql              cast(isnull(@R6,'')as varchar))
	$insql				select @Result = 1 else select @Result = 0	

	$outsql				update tblCLS set NOTE =
	$outsql				cast(@RID as varchar) + ' ' + 
	$outsql             cast(isnull(@R1,'')as varchar)+' '+
	$outsql             cast(isnull(@R2,'')as varchar)+' '+
	$outsql             cast(isnull(@R3,'')as varchar)+' '+
	$outsql             cast(isnull(@R4,'')as varchar)+' '+
	$outsql             cast(isnull(@R5,'')as varchar)+' '+
	$outsql             cast(isnull(@R6,'')as varchar)+' '
	$outsql				where ISN_CLS = @NewID and [NAME] = @RUBRIKA

END

BEGIN
	update b set b.ISN_HIGH_CLS = a.ISN_CLS
	from tblCLS b
	join tblCLS a on
	replace(a.NOTE,'0 ','') = substring(b.NOTE,1,len(replace(a.NOTE,'0 ','')))
	and a.NOTE <> b.NOTE
	and len(replace(a.NOTE,'0 ',''))+2 >= len(replace(b.NOTE,'0 ',''))
	where a.CODE = 'UPDATE_CODE' and b.CODE = 'UPDATE_CODE' and %b.ISN_CLS%
END

BEGIN
	update tblCLS set ISN_HIGH_CLS =
	  (select top 1 a.ISN_CLS from tblCLS a where a.NAME = 'Каталог' and %a.ISN_CLS%)
	where len(rtrim(replace(NOTE,'0 ',''))) < 3
	and CODE = 'UPDATE_CODE' and %ISN_CLS%
END


BEGIN
	update tblCLS set CODE = null
	where CODE = 'UPDATE_CODE' and %ISN_CLS%
END


