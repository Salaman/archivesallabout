merge AF506.dbo.eqUsers as t --target �������
using gakk.dbo.eqUsers as s --source �������
on t.ID = s.ID
when matched then
	update set Deleted = s.Deleted, AccessGranted = s.AccessGranted, Pass = s.Pass
when not matched then
	insert values (s.[ID],s.[Login],s.[Department],s.[Deleted],s.[OwnerID],s.[CreationDateTime],s.[StatusID],s.[Email],s.[patronymic],
				   s.[Position], s.[Phone],s.[Room_Number],s.[Description],s.[surname],s.[AccessGranted],s.[Supervisor],s.[FirstName],
				   s.[BUILD_IN_ACCOUNT], s.[Pass],s.[Cookie],s.[UserTheme]);


merge AF506.dbo.eqRoles as t 
using gakk.dbo.eqRoles as s
on t.ID = s.ID
when not matched then
	insert values (s.[ID], s.[RoleName],s.[Description],s.[OwnerID],s.[CreationDateTime],s.[Deleted],s.[StatusID],s.[BUILD_IN_ROLE],
	s.[StartPage]);

merge AF506.dbo.eqGroups as t
using gakk.dbo.eqGroups as s
on t.ID = s.ID
when not matched then
	insert values(s.[ID],s.[GroupName],s.[OwnerID],s.[CreationDateTime],s.[Deleted],s.[StatusID]);

merge AF506.dbo.eqGroupMembers as t
using gakk.dbo.eqGroupMembers as s
on t.ID = s.ID
when matched then
	update set DocID = s.DocID, RowID = s.RowID, Type = s.Type, UserOrGroupID = s.UserOrGroupID, Name = s.Name,
			   OwnerID = s.OwnerID, CreationDateTime = s.CreationDateTime
when not matched then
	insert ([DocID], [RowID], [Type], [UserOrGroupID], [Name], [ID], [OwnerID], [CreationDateTime])
	values (s.[DocID], s.[RowID], s.[Type], s.[UserOrGroupID], s.[Name], s.[ID], s.[OwnerID], s.[CreationDateTime]);


merge AF506.dbo.eqGroupsRoles as t
using gakk.dbo.eqGroupsRoles as s
on t.ID = s.ID
when matched then
	update set DocID = s.DocID, OwnerID = s.OwnerID, CreationDateTime = s.CreationDateTime, RoleName = s.RoleName,
			   RowID = s.RowID, RoleID = s.RoleID
when not matched then 
	insert ([DocID], [ID], [OwnerID], [CreationDateTime], [RoleName], [RowID], [RoleID])
	values (s.[DocID], s.[ID], s.[OwnerID], s.[CreationDateTime], s.[RoleName], s.[RowID], s.[RoleID]);

merge AF506.dbo.eqRolesDocumentsAccess as t
using gakk.dbo.eqRolesDocumentsAccess as s
on t.ID = s.ID
when matched then
	update set [DocID] = s.[DocID], [RowID] = s.[RowID], [DocTypeID] = s.[DocTypeID], [DocTypeName] = s.[DocTypeName],
	           [OwnerID] = s.[OwnerID], [CreationDateTime] = s.[CreationDateTime], [Menu] = s.[Menu], [List] = s.[List],
			   [Open] = s.[Open], [New] = s.[New], [Priority] = s.[Priority]
when not matched then
	insert ([ID], [DocID], [RowID], [DocTypeID], [DocTypeName], [OwnerID], [CreationDateTime], [Menu], [List], [Open], [New], [Priority])
	values (s.[ID], s.[DocID], s.[RowID], s.[DocTypeID], s.[DocTypeName], s.[OwnerID], s.[CreationDateTime],s.[Menu],s.[List],s.[Open],
	        s.[New], s.[Priority]);



merge AF506.dbo.eqRolesToolBarDenied as t
using gakk.dbo.eqRolesToolBarDenied as s
on t.ID = s.ID
when matched then
	update set [DocID] = s.[DocID], [RowID] = s.[RowID], [DocTypeID] = s.[DocTypeID], [IDButton] = s.[IDButton], [Document] = s.[Document],
			   [Caption] = s.[Caption], [OwnerID] = s.[OwnerID], [CreationDateTime] = s.[CreationDateTime], [StatusID] = s.[StatusID],
			   [Picture] = s.[Picture], [Hide] = s.[Hide], [Priority] = s.[Priority]
when not matched then
	insert ([ID], [DocID], [RowID], [DocTypeID], [IDButton], [Document], [Caption], [OwnerID], [CreationDateTime], [StatusID], [Picture], 
			[Hide], [Priority])
	values (s.[ID], s.[DocID], s.[RowID], s.[DocTypeID], s.[IDButton], s.[Document], s.[Caption],s.[OwnerID],s.[CreationDateTime],s.[StatusID],
	        s.[Picture], s.[Hide], s.[Priority]);


merge AF506.dbo.eqRolesToolBarDeniedList as t
using gakk.dbo.eqRolesToolBarDeniedList as s
on t.ID = s.ID
when not matched then
	insert values (s.[ID],s.[OwnerID],s.[CreationDateTime],s.[DocID],s.[RowID],s.[DocTypeID],s.[IDButton],s.[Priority]);


