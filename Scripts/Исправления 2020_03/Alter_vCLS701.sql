USE [ArchiveFund__test_util_import]
GO

/****** Object:  View [dbo].[vCLS701]    Script Date: 05.03.2020 9:59:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- AW: 2012-10-23
ALTER VIEW [dbo].[vCLS701]
AS
SELECT		C.ID, C.OwnerID, C.CreationDateTime, C.StatusID, C.Deleted,
			C.ISN_CLS, C.ISN_HIGH_CLS, C.CODE, C.[WEIGHT], C.NAME, C.OBJ_KIND,
			C.MULTISELECT, C.NOTE, C.FOREST_ELEM, C.PROTECTED, [dbo].[fn_Level2](C.ISN_CLS, C.ISN_HIGH_CLS) 'Level2'
FROM		tblCLS C
WHERE		C.OBJ_KIND = 701 AND C.ISN_CLS>1000000000
GO


