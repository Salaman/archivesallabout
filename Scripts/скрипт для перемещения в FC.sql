alter table GAKO1.dbo.tblFUND drop column ForbidRecalc
alter table GAKO1.dbo.tblINVENTORY drop column ForbidRecalc
alter table GAKO1.dbo.tblREF_LOCATION drop column UNIT_NUM_OLD_FROM
alter table GAKO1.dbo.tblREF_LOCATION drop column UNIT_NUM_OLD_TO

alter table GAKO1.dbo.tblFUND add ForbidRecalc varchar(1) NULL
alter table GAKO1.dbo.tblINVENTORY add ForbidRecalc varchar(1) NULL
alter table GAKO1.dbo.tblREF_LOCATION add UNIT_NUM_OLD_FROM varchar(50) NULL
alter table GAKO1.dbo.tblREF_LOCATION add UNIT_NUM_OLD_TO varchar(50) NULL

