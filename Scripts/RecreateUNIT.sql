insert tblUNIT 
  select 
  newid(),
  OwnerID,
  CreationDateTime,
  StatusID,
  Deleted,
  100015284690,
  ISN_HIGH_UNIT,
  100000482520,
  7,
  ISN_LOCATION,
  ISN_SECURLEVEL,
  SECURITY_CHAR,
  SECURITY_REASON,
  ISN_INVENTORY_CLS,
  100000100330,
  ISN_DOC_KIND,
  704,
  2,
  UNIT_NUM_2,
  VOL_NUM,
  'Хроникально-документальный фильм «Праздник Красноярского вольно-пожарного общества». Парад пожарных дружин,  показательное тушение пожара с применением средств пожаротушения и спасения пострадавших. На празднике присутствуют члены Исполнительного комитета Красноярского Совета рабочих и солдатских депутатов Яков Федорович Дубровинский, Григорий Спиридонович Вейнбаум и командующий отрядами Красной Гвардии Тихон Павлович Марковский',
  ANNOTATE,
  DELO_INDEX,
  PRODUCTION_NUM,
  UNIT_CATEGORY,
  NOTE,
  IS_IN_SEARCH,
  IS_LOST,
  HAS_SF,
  HAS_FP,
  HAS_DEFECTS,
  ARCHIVE_CODE,
  CATALOGUED,
  WEIGHT,
  UNIT_CNT,
  1918,
  START_YEAR_INEXACT,
  1918,
  END_YEAR_INEXACT,
  'E',
  BACKUP_COPY_CNT,
  HAS_TREASURES,
  IS_MUSEUM_ITEM,
  null,
  CARDBOARDED,
  ADDITIONAL_CLS,
  '1918-1919',
  ISN_SECURITY_REASON,
  UNIT_NUM_TXT
  from tblUNIT where ISN_UNIT = 10000611424

  delete from tblUNIT where ID = 'B3609CA7-F443-4E59-BE4A-1579D7827C4B'

  select * from tblUNIT where ISN_UNIT = 10000611424 or NAME like '%Хроникально-документальный фильм «Праздник Красноярского вольно-пожарного общества»%'
  select * from tblUNIT where ISN_UNIT = 10000611424 or ISN_UNIT = 10001528469 or ISN_UNIT = 10001528469

  update tblUNIT set ID = 'B3609CA7-F443-4E59-BE4A-1579D7827C4B', 
  ISN_UNIT = 10001528469,
  ISN_INVENTORY = 10000048252,
  ISN_STORAGE_MEDIUM = 10000010033
  where ISN_UNIT = 100015284690