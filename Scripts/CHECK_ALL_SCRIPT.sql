use CFCTemp 
declare @ISN bigint
set @ISN = (select top 1 ISN_ARCHIVE from tblARCHIVE where CreationDateTime = 

'2020-01-31 11:03:56.837'

)

select * from
(
	select a.CreationDateTime, a.NAME, '' as Empty from tblARCHIVE as a
	where a.ISN_ARCHIVE = @ISN
) as "r",
(
	select count(f.ID) as FUNDS from tblFUND as f 
	right join tblARCHIVE as a on a.ISN_ARCHIVE = f.ISN_ARCHIVE and a.Deleted = 0
	where
	f.Deleted = 0 and
	a.ISN_ARCHIVE = @ISN 
) as "r1",
(
	select count(i.ID) as OPISES from tblINVENTORY as i 
	right join tblFUND as f on i.ISN_FUND = f.ISN_FUND and f.Deleted = 0
	right join tblARCHIVE as a on a.ISN_ARCHIVE = f.ISN_ARCHIVE and a.Deleted = 0
	where 
	i.Deleted = 0 and 
	f.ISN_ARCHIVE = @ISN
) as "r2",
(
	select count(u.ID) as ED_HRAN from tblUNIT as u
	right join tblINVENTORY as i on i.ISN_INVENTORY = u.ISN_INVENTORY and i.Deleted = 0
	right join tblFUND as f on f.ISN_FUND = i.ISN_FUND and f.Deleted = 0
	right join tblARCHIVE as a on a.ISN_ARCHIVE = f.ISN_ARCHIVE and a.Deleted = 0
	where 
	u.Deleted = 0 and
	a.ISN_ARCHIVE = @ISN
) as "r3";
use FC
select CreationDateTime, Deleted, ISN_ARCHIVE, NAME from tblARCHIVE

